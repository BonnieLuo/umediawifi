* 專案建置說明:
1. 執行 copyFileFromSample.bat, 產生下列檔案:
	(1) local.properties
	(2) app/build.gradle
2. 修改檔案裡的參數以適應自己電腦的編譯環境:
	(1) local.properties
		- sdk.dir 修改為自己電腦裡安裝的 Android SDK 路徑
	(2) build.gradle
		- signingConfigs 裡的 keystore file 存放路徑修改為自己電腦裡的路徑
			(keystore file OnyxWifi_1234567890.jks 儲存在專案目錄裡)
3. 以後若 app/build.gradle 有修改, 例如: dependencies 新增 library, 需將 app/build.gradle 內容複製到 app/build.gradle.sample,
	並上傳至 source code version control server, 其他人同步 code 後需再執行一次 copyFileFromSample.bat, 得到新的 app/build.gradle, 才不會 compile error


* Cloud Server URL 說明:
- 目前專案使用的 Cloud Server URL 共有四種:
	(1) For developer: http://test.onyxwificloud.com
	(2) For developer: https://stage.onyxwificloud.com
	(3) For RDQA: http://rdqa.onyxwificloud.com
	(4) For production: https://api.onyxwificloud.com
- 若將打 Cloud API 的 base server URL 直接以明文方式寫在程式內, 當 App 被反組譯時可能較容易被看出來 server URL
	為了不想在程式內留下明文的 server URL, 故將加密 server URL 得到的密文寫在程式內, 交給程式解密回真正的 server URL
- test server 和 stage server 都是給 developer 用的, 差別在一個是 http, 一個是 https
- Cloud team 發佈 API 會優先上在 test server, 故 developer 自行測試時, 使用 test server 即可
- 若發佈版本給 RDQA 測試, 需使用 RDQA server
- 若要發佈對外的正式版本 (要上架到 Google Play 的版本, 真實使用者會下載來使用), 需使用 production server
- ***** 非常重要!!! 發佈版本的規則請見專案 source code MainApp.java 裡面的說明
			

* 放置 Android 圖檔到專案內的 res/drawable-xxxx 資料夾 (xxxx 代表解析度) 時的注意事項:
- UI 設計師提供過來的 Android 圖檔包, 通常分成五個資料夾, 圖片已依照解析度放好:
  drawable-hdpi, drawable-mdpi, drawable-xhdpi, drawable-xxhdpi, drawable-xxxhdpi
- 若偶爾只需要一個圖檔時, UI 設計師提供過來的檔案不會分資料夾, 而是在檔名後面帶有解析度, 例如: bg_login_xxxh.png
  這時需要重新命名, 將檔名內的 "_解析度" 去掉後再放置到對應解析度的資料夾
- App Logo 圖檔處理方法請參考文章: https://mp.weixin.qq.com/s/WxgHJ1stBjokPi6lTUd1Mg
	(1) 將設計師提供的圖檔 (圖片尺寸是 512*512, 一張而已) 放在 res/mipmap-anydpi-v26 底下 (注意圖檔名稱不能有空白)
	(2) 在 Android Studio 按下快捷鍵: Ctrl+Shift+A, 搜尋並打開 "image asset"
	(3) Icon Type 和 Name 保持預設值即可
	(4) Foreground Layer -> Path 選擇剛剛步驟 (1) 的圖檔
	(5) Trim 選擇 No, Resize 成合適的大小
	(6) Background Layer -> Color 選擇白色 #FFFFFF