package com.onyx.wifi.utility;

import com.onyx.wifi.MainApp;
import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.BnbNetworkSetting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {
    public static final long mDayInMilliseconds = 1000 * 60 * 60 * 24;
    public static final long mHourInMilliseconds = 1000 * 60 * 60;
    public static final long mMinuteInMilliseconds = 1000 * 60;

    public static final long mDayInSeconds = 60 * 60 * 24;
    public static final long mHourInSeconds = 60 * 60;
    public static final long mMinuteInSeconds = 60;

    public static final long mDayInMinutes = 60 * 24;
    public static final long mHourInMinutes = 60;

    public static TimeZone getTimezoneByDefinedString(String definedTimezoneString) {
        // 由於定義好的字串是將手機系統字串的底線 "_" 取代成空白 " ", 故要先取代回去才能比對
        String timezone = definedTimezoneString.replace(" ", "_");
        TimeZone systemTimezone = TimeZone.getTimeZone(timezone);
        if (!timezone.equals(systemTimezone.getID())) {
            LogUtils.trace("defined timezone string cant't match Android system timezone");
            // TimeZone.getAvailableIDs() 可取得 Android 系統所有的 Timezone
            // Android 系統版本不同, available TimeZone 可能會不同 (因為隨著時間演進, 全世界所有的時區定義會更新)
        }
        return systemTimezone;
    }

    // 跟 Router 時區相關的操作, 例如 BnB Network Setting, 使用到 Calendar 時需設置 timezone, 範例如下:
    // (1) Calendar calendar = Calendar.getInstance(DateUtils.getRouterTimezone());
    // (2) Calendar calendar = Calendar.getInstance();
    //     calendar.setTimeZone(DateUtils.getRouterTimezone());
    public static TimeZone getRouterTimezone() {
        String routerTimezoneString = DataManager.getInstance().getDeviceInfo().getCity();
        return getTimezoneByDefinedString(routerTimezoneString);
    }

    // 將 milliseconds timestamp 轉換成 GMT 的時間字串
    public static String getDateStringWithGMTTimestamp(long timeInMillis) {
        // format example: "2020-02-12T13:41:33.698Z"
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(new Date(timeInMillis));
    }

    // 將 milliseconds timestamp 根據時區轉換成時間字串
    public static String getDateStringByTimezone(long timeInMillis, TimeZone timeZone) {
        // format example: "2020-02-12T13:34:23.466+0800"
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
        sdf.setTimeZone(timeZone);
        return sdf.format(new Date(timeInMillis));
    }

    // 測試依據 timezone 顯示 timestamp 字串
    public static void testTimezone() {
        String[] timezones = MainApp.getInstance().getResources().getStringArray(R.array.timezone);
        long testTimestamp = 1581465600000L;    // 2020-02-12T00:00:00.000+0000
        for (int i = 0; i < timezones.length; i++) {
            TimeZone timezoneId = DateUtils.getTimezoneByDefinedString(timezones[i]);
            LogUtils.trace("test timestamp: " + testTimestamp + ", timezone: " + timezones[i] + ", timezoneId: " + timezoneId.getID() + ", now date in timezoneId: " + DateUtils.getDateStringByTimezone(testTimestamp, timezoneId));
        }
    }

    public static String getSpeedTestDiffTimeString(String speedTestTimestamp) {
        if (StringUtils.isNullOrEmpty(speedTestTimestamp)) {
            return "-- hrs ago";
        }

        // https://developer.android.com/reference/java/text/SimpleDateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        // 從 cloud 拿到的 speed test 時間 (ex: "2019-09-09T12:08:48.838Z") 代表的是 GMT 時間, 故要將 date format 的時區設定為 GMT
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date speedTestTime = null;
        try {
            speedTestTime = dateFormat.parse(speedTestTimestamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String diffTimeString = "-- hrs ago";
        if (speedTestTime != null) {
            Date nowTime = Calendar.getInstance().getTime(); // 手機現在的時間 (以手機系統現在的時區為準)
            // 將 speed test time 及手機現在的時間都換成 timestamp 來比較, 計算出時間差
            long diffMinutes = (nowTime.getTime() - speedTestTime.getTime()) / 1000 / 60;
            long hours = diffMinutes / 60;
            long minutes = diffMinutes % 60;
            LogUtils.trace("now time: " + dateFormat.format(nowTime) + ", speed test time: " + dateFormat.format(speedTestTime));
            LogUtils.trace("diffMinutes = " + diffMinutes + ", hours = " + hours + ", minutes = " + minutes);

            // 兩種顯示格式:
            // (1) "%d hours ago"
            // (2) "%d hours %d minutes ago"
            String time = hours + ((hours > 1) ? " hrs" : " hr"); // ----- (1)
            /////////// ----- (2) Begin
//            String time = "";
//            if (Math.abs(hours) >= 1) {
//                time = hours + ((Math.abs(hours) > 1) ? " hrs" : " hr") + " ";
//            }
//            time += minutes + ((minutes > 1) ? " minutes" : " minute");
            /////////// ----- (2) End
            diffTimeString = time + " ago";
        }

        return diffTimeString;
    }

    // client list
    // off line
    public static String milliSecToLastOnlineDescription(long timeDiffWithCurrent, long lastOnlineTime) {
//        long seconds = timeDiffWithCurrent / 1000;
//        long minutes = seconds / 60;才會call到此function
//        long hours = minutes / 60;
//        long days = hours / 24;

        long days, hours, minutes, seconds, tmp = 0;
        tmp = timeDiffWithCurrent;
        days = timeDiffWithCurrent / mDayInMilliseconds;
        tmp -= (days * mDayInMilliseconds);
        hours = tmp / mHourInMilliseconds;
        tmp -= (hours * mHourInMilliseconds);
        minutes = tmp / mMinuteInMilliseconds;
        tmp -= (minutes * mMinuteInMilliseconds);
        seconds = tmp / 1000;

        String time;// = days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;

        ///////////////////////////////////////////
//        Calendar currentTime = Calendar.getInstance();
//        Calendar lastOnline = Calendar.getInstance();
//        lastOnline.setTimeInMillis(lastOnlineTime);
//        //DateFormat df = new SimpleDateFormat("dd/MM/yy/HH/mm/ss");
//        LogUtils.trace("Timediff", "current: " +
//                //df.format(currentTime.getTime()) + "\n" +
//                currentTime.get(Calendar.YEAR) + " " +
//                (currentTime.get(Calendar.MONTH) + 1) + " " +
//                currentTime.get(Calendar.DATE) + " " +
//                currentTime.get(Calendar.HOUR) + " " +
//                currentTime.get(Calendar.HOUR_OF_DAY) + " " +
//                currentTime.get(Calendar.MINUTE) + " ");
//        LogUtils.trace("Timediff", "lastOnline: " +
//                //df.format(lastOnline.getTime()) + "\n" +
//                lastOnline.get(Calendar.YEAR) + " " +
//                (lastOnline.get(Calendar.MONTH) + 1) + " " +
//                lastOnline.get(Calendar.DATE) + " " +
//                lastOnline.get(Calendar.HOUR) + " " +
//                lastOnline.get(Calendar.HOUR_OF_DAY) + " " +
//                lastOnline.get(Calendar.MINUTE) + " ");
        ///////////////////////////////////////////
        if (days > 1) {//||
            // ((days == 1) && ((hours > 0) || (minutes > 0) || (seconds > 0)))) {// (lastOnline - currentTime) >= 1 day
            return "Last online " + days + "days ago";
        } else if ((days == 1) && ((hours > 0) || (minutes > 0) || (seconds > 0))) {

            return "Last online " + days + "day ago";
        } else { // (lastOnline - currentTime) < 1 day
            Calendar currentTime = Calendar.getInstance();
            Calendar lastOnline = Calendar.getInstance();
            lastOnline.setTimeInMillis(lastOnlineTime);
            int currentDate = currentTime.get(Calendar.DATE);
            int currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
            int currentMinute = currentTime.get(Calendar.MINUTE);
            int lastOnlineDate = lastOnline.get(Calendar.DATE);
            int lastOnlineHour = lastOnline.get(Calendar.HOUR_OF_DAY);
            int lastOnlineMinute = lastOnline.get(Calendar.MINUTE);

            LogUtils.trace("Timediff", "current: " +
                    //df.format(currentTime.getTime()) + "\n" +
                    currentTime.get(Calendar.YEAR) + " " +
                    (currentTime.get(Calendar.MONTH) + 1) + " " +
                    currentTime.get(Calendar.DATE) + " " +
                    currentTime.get(Calendar.HOUR) + " " +
                    currentTime.get(Calendar.HOUR_OF_DAY) + " " +
                    currentTime.get(Calendar.MINUTE) + " ");
            LogUtils.trace("Timediff", "lastOnline: " +
                    //df.format(lastOnline.getTime()) + "\n" +
                    lastOnline.get(Calendar.YEAR) + " " +
                    (lastOnline.get(Calendar.MONTH) + 1) + " " +
                    lastOnline.get(Calendar.DATE) + " " +
                    lastOnline.get(Calendar.HOUR) + " " +
                    lastOnline.get(Calendar.HOUR_OF_DAY) + " " +
                    lastOnline.get(Calendar.MINUTE) + " ");
            if (lastOnlineDate != currentDate) {
                if (lastOnlineHour > 12) {
                    time = "Last online at " +//"Online since " +
                            String.valueOf(lastOnlineHour - 12) + ":" +
                            String.valueOf(lastOnlineMinute) +
                            " PM, Yesterday";
                } else {
                    time = "Last online at " +//"Online since " +
                            String.valueOf(lastOnlineHour) + ":" +
                            String.valueOf(lastOnlineMinute) +
                            " AM, Yesterday";
                }
            } else {
                if (lastOnlineHour > 12) {
                    time = "Last online at " +//"Online since " +
                            String.valueOf(lastOnlineHour - 12) + ":" +
                            String.valueOf(lastOnlineMinute) +
                            " PM, Today";
                } else {
                    time = "Last online at " +//"Online since " +
                            String.valueOf(lastOnlineHour) + ":" +
                            String.valueOf(lastOnlineMinute) +
                            " AM, Today";
                }

            }
        }

        return time;
    }

    public static String getTimeDiffToCurrent(long lastOnlineTime) {
        Calendar currentTime = Calendar.getInstance();
        long current = currentTime.getTimeInMillis();

        if (lastOnlineTime > currentTime.getTimeInMillis()) {  //<=不合理的狀況
            return null;
        }

        return milliSecToLastOnlineDescription(currentTime.getTimeInMillis() - lastOnlineTime, lastOnlineTime);
    }

    // disallowed timestamp
    public static String getDisallowedTimeDescription(long disallowedTimestamp) {

        Calendar lastOnline = Calendar.getInstance();
        lastOnline.setTimeInMillis(disallowedTimestamp);

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//        int date = lastOnline.get(Calendar.DATE);
//        int month = lastOnline.get(Calendar.MONTH) + 1;
//        int year = lastOnline.get(Calendar.YEAR);
//
//        String description = "Blocked on ";
//
//        if (date < 10)
//            description += "0";
//
//        description += date + "/";
//
//        if (month <10)
//            description += "0";
//
//        description += month + "/" + year;
//
//        return description;
        return "Blocked on " + sdf.format(disallowedTimestamp);
    }

    public static boolean IsLongInactiveClient(long lastOnlineTime) {
        long mJudgeTimeInMillis = 4 * 24 * 60 * 60 * 1000; // 判斷為long inactive的時間標準

        Calendar currentTime = Calendar.getInstance();
        long current = currentTime.getTimeInMillis();
        if (lastOnlineTime > currentTime.getTimeInMillis()) {  //<=不合理的狀況
            return false;
        }

        if ((currentTime.getTimeInMillis() - lastOnlineTime) > mJudgeTimeInMillis) {
            return true;
        } else {
            return false;
        }
    }

    //
    // For client list
    //
    public static String minToTimeFormat(int timeInMinutes) {
        long days, hours, minutes, seconds, tmp = 0;
        tmp = timeInMinutes;
        days = timeInMinutes / mDayInMinutes;
        tmp -= (days * mDayInMinutes);
        hours = tmp / mHourInMinutes;
        tmp -= (hours * mHourInMinutes);
        minutes = tmp;

        String sDay = " Days";
        String sHour = " Hours";
        String sMinue = " Minutes";
        String timeString = new String();

        if (timeInMinutes < 0) return "";

        if (days == 1) sDay = " Day";
        if (hours == 1) sHour = " Hour";
        if (minutes == 1) sMinue = " Minute";

        if (days > 0) {
            timeString += days + sDay;
        }
        if (hours > 0) {
            if (days > 0) {
                timeString += ", ";
            }
            timeString += hours + sHour;
        }
        if (minutes > 0) {
            if (days > 0 || hours > 0) {
                timeString += ", ";
            }
            timeString += minutes + sMinue;
        }

        if (days == 0 && hours == 0 && minutes == 0) {
            timeString += "0 Minute";
        }
        return timeString;
    }

    public static String secToTimeFormat(int timeInSeconds) {
        long days, hours, minutes, tmp = 0;
        tmp = timeInSeconds;
        days = timeInSeconds / mDayInSeconds;
        tmp -= (days * mDayInSeconds);
        hours = tmp / mHourInSeconds;
        tmp -= (hours * mHourInSeconds);
        minutes = tmp / mMinuteInSeconds;
        tmp -= (minutes * mMinuteInSeconds);

        String sDay = " Days";
        String sHour = " Hours";
        String sMinue = " Minutes";
        String timeString = new String();

        if (timeInSeconds < 0) return "";

        if (days == 1) sDay = " Day";
        if (hours == 1) sHour = " Hour";
        if (minutes == 1) sMinue = " Minute";

        if (days > 0) {
            timeString += days + sDay;
        }
        if (hours > 0) {
            if (days > 0) {
                timeString += ", ";
            }
            timeString += hours + sHour;
        }
        if (minutes > 0) {
            if (days > 0 || hours > 0) {
                timeString += ", ";
            }
            timeString += minutes + sMinue;
        }

        if (days == 0 && hours == 0 && minutes == 0) {
            timeString += "0 Minute";
        }
        return timeString;
    }

    public static String secToClientLeaseTimeFormat(int timeInSeconds) {
        long days, hours, minutes, tmp = 0;
        tmp = timeInSeconds;
        days = timeInSeconds / mDayInSeconds;
        tmp -= (days * mDayInSeconds);
        hours = tmp / mHourInSeconds;
        tmp -= (hours * mHourInSeconds);
        minutes = tmp / mMinuteInSeconds;
        tmp -= (minutes * mMinuteInSeconds);

        String timeString = new String();

        if (timeInSeconds < 0) return "";

        if (hours < 10) {
            timeString += "0";
        }

        timeString += hours + "h:";

        if (minutes < 10) {
            timeString += "0";
        }

        timeString += minutes + "m:";

        if (tmp < 10) {
            timeString += "0";
        }

        timeString += tmp + "s";

        return timeString;
    }

    private static String DigitToMonth(int month) {
        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sep";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";
            default:
                return "unknown";
        }
    }

    //
    // For BnB Network's start time and end time
    //
    public static String secToTimeDescription(long timeInSeconds) { // long -> int 會出錯，因為要x1000
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(timeInSeconds * 1000);

//        return DigitToMonth(time.get(Calendar.MONTH)) + " " +
//                time.get(Calendar.DATE) + "th, " +
//                time.get(Calendar.YEAR) + " " +
//                time.get(Calendar.HOUR_OF_DAY) + ":" +
//                time.get(Calendar.MINUTE) + ":" +
//                time.get(Calendar.SECOND);
        SimpleDateFormat sf = new SimpleDateFormat("HH:mm:ss");//new SimpleDateFormat("hh:mm:ss a");
        return DigitToMonth(time.get(Calendar.MONTH) + 1) + " " +
                time.get(Calendar.DATE) + "th, " +
                time.get(Calendar.YEAR);// + " " +
//                time.get(Calendar.HOUR_OF_DAY) + ":" +
//                time.get(Calendar.MINUTE) + ":" +
//                time.get(Calendar.SECOND) + " " +
        //sf.format(time.getTime());
    }

    public static Calendar secToCalendar(long timeInSeconds) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(timeInSeconds * 1000);
        return time;
    }

    public static int datePickerToStartTimeInSeconds(int year, int month, int day) {
        Calendar pickTime = Calendar.getInstance();

        pickTime.set(
                year,
                month,
                day,
                0,
                0,
                0);

        return (int) (pickTime.getTimeInMillis() / 1000);
    }

    public static int datePickerToEndTimeInSeconds(int year, int month, int day) {
        Calendar pickTime = Calendar.getInstance();

        pickTime.set(
                year,
                month,
                day,
                23,
                59,
                59);

        return (int) (pickTime.getTimeInMillis() / 1000);
    }

    public static boolean isBnBActive(BnbNetworkSetting bnbNetworkSetting) {
        Calendar current = Calendar.getInstance();
        long currentInMillis = current.getTimeInMillis();
        long startInMillis = ((long) bnbNetworkSetting.getStart()) * 1000;
        long endInMillis = ((long) bnbNetworkSetting.getEnd()) * 1000;

        // if ((current >= start) and (current <= end)) return true, else return false
        if ((currentInMillis >= startInMillis) && (currentInMillis <= endInMillis)) {
            return true;
        } else {
            return false;
        }
    }

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }
}
