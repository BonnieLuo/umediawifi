package com.onyx.wifi.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.onyx.wifi.MainApp;

public class CommonUtils {

    private static final String SHARED_PREF_NAME = "app_info";

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = (connectivityManager == null) ? null : connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isInternetAccessible() {
        Runtime runtime = Runtime.getRuntime();
        try {
            // 上面的 function: isInternetAvailable() 若回傳 true, 代表手機有開啟行動網路或是有連線上 WIFI, 但實際對外的網路不一定有通
            // 這個 function 用 ping Google Public DNS (8.8.8.8) 的方式確認對外網路是否有通
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void toast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void setSharedPrefData(String key, String value) {
        SharedPreferences pref = MainApp.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        pref.edit().putString(key, value).apply();
    }

    public static void setSharedPrefFloat(String key, float value) {
        SharedPreferences pref = MainApp.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        pref.edit().putFloat(key, value).apply();
    }

    public static void setSharedPrefInt(String key, int value) {
        SharedPreferences pref = MainApp.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        pref.edit().putFloat(key, value).apply();
    }

    public static String getSharedPrefData(String key) {
        SharedPreferences pref = MainApp.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString(key, "");
    }

    public static Float getSharedPrefFloat(String key) {
        SharedPreferences pref = MainApp.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getFloat(key, 0.0f);
    }

    public static int getSharedPrefInt(String key) {
        SharedPreferences pref = MainApp.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getInt(key, -1);
    }

    public static int getColor(Context context, int colorId) {
        return ContextCompat.getColor(context, colorId);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        // Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        // If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    // view: 表示當前要接收軟鍵盤輸入的 View, 必須是 EditText, 或者 EditText 的子類
    // 參考資料: https://blog.csdn.net/ccpat/article/details/46717573
    public static void showKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && view != null) {
            view.requestFocus();
            imm.showSoftInput(view, 0);
        }
    }

    public static String getMasterKey() {
        return "zBD" + (84 * 6 / 2 + 733 - 17) + "j2Gn0mj31";
    }
}
