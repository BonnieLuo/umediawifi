package com.onyx.wifi.utility;

import android.os.Build;
import android.util.Log;

import com.onyx.wifi.BuildConfig;
import com.onyx.wifi.MainApp;
import com.onyx.wifi.model.DataManager;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class LogUtils {

    private static String TAG = "D_UMEDIA";

    public static void trace(final String msg) {
        if (BuildConfig.DEBUG || BuildConfig.IS_RDQA) {
            // 只有 Production Release 版本不符合判斷式, 不會寫 log
            String fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

            String log = getServerName() + " #" + lineNumber + " " + className + "." + methodName + "() : " + msg;
            writeLogToFile(log);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, log);
            }
        }
    }

    public static void trace(String tag, final String msg) {
        if (BuildConfig.DEBUG || BuildConfig.IS_RDQA) {
            // 只有 Production Release 版本不符合判斷式, 不會寫 log
            if (tag != null && tag.isEmpty()) {
                tag = TAG;
            }
            String fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

            String log = getServerName() + " #" + lineNumber + " " + className + "." + methodName + "() : " + msg;
            writeLogToFile(log);
            if (BuildConfig.DEBUG) {
                Log.d(tag, log);
            }
        }
    }

    public static void traceOnlyDebug(final String msg) {
        if (BuildConfig.DEBUG) {
            String fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

            String log = getServerName() + " #" + lineNumber + " " + className + "." + methodName + "() : " + msg;
            Log.d(TAG, log);
        }
    }

    public static void traceOnlyFile(final String msg) {
        if (BuildConfig.IS_RDQA && !BuildConfig.DEBUG) {
            String fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

            String log = getServerName() + " #" + lineNumber + " " + className + "." + methodName + "() : " + msg;
            writeLogToFile(log);
        }
    }

    private static String getServerName() {
        return "[" + DataManager.getInstance().getCloudServerMode().name() + "]";
    }

    private static final long FILE_MAX_SIZE = 5 * 1024 * 1024; // 5MB

    public static File getLogFile() {
        String fileName = Build.MODEL + "_" + Build.VERSION.RELEASE + "_log.txt";
        File logFile = new File(MainApp.getInstance().getExternalFilesDir(null), fileName);
        return logFile;
    }

    private static void writeLogToFile(String log) {
        File logFile = getLogFile();
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            String newLine = System.getProperty("line.separator");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            log = sdf.format(Calendar.getInstance().getTime()) + " " + log + newLine;

            RandomAccessFile file = new RandomAccessFile(logFile, "rw");
            long lastTimeFileIndex = DataManager.getInstance().getLogFileIndex();
            long newFileIndex = lastTimeFileIndex + log.length();
            if (file.length() == 0 || newFileIndex > FILE_MAX_SIZE) {
                // 以下情況會從檔案最一開始的地方寫 log:
                // (1) 剛建立出 log 檔案, 還沒有任何內容時
                // (2) log 檔案已有內容, 新增的文字加上去會超過檔案大小限制, 就從頭開始覆寫
                file.seek(0);
                newFileIndex = log.length();
            } else {
                // 從上次的地方繼續往下寫
                file.seek(lastTimeFileIndex);
            }
            file.write(log.getBytes());
            file.write(newLine.getBytes());
            file.close();

            // 記住 log file 檔案內容寫到哪個位置了, 下次從這個地方接著往下寫
            DataManager.getInstance().setLogFileIndex(newFileIndex);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
