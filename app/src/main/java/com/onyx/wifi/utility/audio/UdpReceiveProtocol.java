package com.onyx.wifi.utility.audio;

import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.DeviceInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.PortUnreachableException;

public class UdpReceiveProtocol extends Thread {

    private DatagramSocket ds = null;
    private Boolean running = false;

    private Integer port = 0;
    private AudioDeviceInfo audioDeviceInfo = null;

    //public UdpReceiveProtocol(Integer port, DeviceInfo deviceInfo) throws IOException {
    public UdpReceiveProtocol(Integer port, AudioDeviceInfo audioDeviceInfo) throws IOException {

        ds = new DatagramSocket(AppConstants.UDP_PORT);
        running = true;
        this.audioDeviceInfo = audioDeviceInfo;
        this.port = port;
        LogUtils.trace("UdpReceiveProtocol");
    }

    public void setRunning(Boolean running) {
        this.running = running;
        if (!running)
            this.ds.close();
    }

    public void run() {

        byte[] buf = new byte[256];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        DeviceInfo deviceInfo = new DeviceInfo("","","","","","","");
        int i = 0;

        try {
            while (this.running) {
                //byte[] buf = new byte[256];

                //DatagramPacket packet = new DatagramPacket(buf, buf.length);
                ds.receive(packet);
                LogUtils.trace("UdpReceiveProtocol - running");
                String text = new String(buf, 0, packet.getLength());
                LogUtils.trace("packet length:"+packet.getLength());

                JSONObject json = new JSONObject(text);
                JSONArray keys = json.names();
                JSONObject newJson = new JSONObject();
                LogUtils.trace("keys #:"+keys.length());

                for (i = 0; i < keys.length(); ++i) {
                    newJson.put(keys.get(i).toString().toLowerCase(),
                            json.getString(keys.get(i).toString()));
                    LogUtils.trace("packet: " + keys.get(i).toString().toLowerCase() + ", " + json.getString(keys.get(i).toString()));
                }


                //this.deviceInfo.clear();
                deviceInfo.clear();
                this.audioDeviceInfo.updateAudioDeviceInfo(deviceInfo);
//              (productID: String, dsn: String?, codeChallenge: String?, state: String, address: String?, status: String, metadata: String?){
                if (newJson.has(AppConstants.DEVICE_PRODUCT_ID) && newJson.has(AppConstants.DEVICE_DSN) && newJson.has(AppConstants.DEVICE_CODE_CHALLENGE)) {
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_PRODUCT_ID, newJson.getString(AppConstants.DEVICE_PRODUCT_ID));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_DSN, newJson.getString(AppConstants.DEVICE_DSN));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_CODE_CHALLENGE, newJson.getString(AppConstants.DEVICE_CODE_CHALLENGE));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_ADDRESS, packet.getAddress().toString());

                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, "");

                    deviceInfo.setProductID(newJson.getString(AppConstants.DEVICE_PRODUCT_ID));// deviceName
                    deviceInfo.setDsn(newJson.getString(AppConstants.DEVICE_DSN));// serialNumber
                    deviceInfo.setCodeChallenge(newJson.getString(AppConstants.DEVICE_CODE_CHALLENGE));// codeChallenge
                    deviceInfo.setDeviceAddress(packet.getAddress().toString());// address
                    LogUtils.trace(deviceInfo.toString());
                    this.audioDeviceInfo.updateAudioDeviceInfo(deviceInfo);
                } else if (newJson.has(AppConstants.DEVICE_PRODUCT_ID) && newJson.has(AppConstants.DEVICE_STATE)) {
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_PRODUCT_ID, newJson.getString(AppConstants.DEVICE_PRODUCT_ID));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATE, newJson.getString(AppConstants.DEVICE_STATE));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_ADDRESS, packet.getAddress().toString());

                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, "");

                    deviceInfo.setProductID(newJson.getString(AppConstants.DEVICE_PRODUCT_ID));// deviceName
                    deviceInfo.setState(newJson.getString(AppConstants.DEVICE_STATE));// state
                    deviceInfo.setDeviceAddress(packet.getAddress().toString());// address
                    deviceInfo.setDeviceStatus(newJson.getString(AppConstants.DEVICE_STATUS));// status
                    LogUtils.trace(deviceInfo.toString());
                    this.audioDeviceInfo.updateAudioDeviceInfo(deviceInfo);
                } else if (newJson.has(AppConstants.DEVICE_STATUS)) {
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, newJson.getString(AppConstants.DEVICE_STATUS));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_ADDRESS, packet.getAddress().toString());

                    deviceInfo.setDeviceAddress(packet.getAddress().toString());// address
                    deviceInfo.setDeviceStatus(newJson.getString(AppConstants.DEVICE_STATUS));// status
                    LogUtils.trace(deviceInfo.toString());
                    this.audioDeviceInfo.updateAudioDeviceInfo(deviceInfo);
                } else if (newJson.has(AppConstants.DEVICE_DSN) && newJson.has(AppConstants.DEVICE_STATE)) {
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_DSN, newJson.getString(AppConstants.DEVICE_DSN));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATE, newJson.getString(AppConstants.DEVICE_STATE));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_ADDRESS, packet.getAddress().toString());

                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, "");

                    deviceInfo.setDsn(newJson.getString(AppConstants.DEVICE_DSN));// serialNumber
                    deviceInfo.setState(newJson.getString(AppConstants.DEVICE_STATE));// state
                    deviceInfo.setDeviceAddress(packet.getAddress().toString());// address
                    LogUtils.trace(deviceInfo.toString());
                    this.audioDeviceInfo.updateAudioDeviceInfo(deviceInfo);
                } else if (newJson.has(AppConstants.DEVICE_DSN) && newJson.has(AppConstants.DEVICE_METADATE)) {
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_DSN, newJson.getString(AppConstants.DEVICE_DSN));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_METADATE, newJson.getString(AppConstants.DEVICE_METADATE));
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_ADDRESS, packet.getAddress().toString());

                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, "");

                    deviceInfo.setDsn(newJson.getString(AppConstants.DEVICE_DSN));// serialNumber
                    deviceInfo.setDeviceAddress(packet.getAddress().toString());// address
                    deviceInfo.setMetadata(newJson.getString(AppConstants.DEVICE_METADATE));// metadata
                    LogUtils.trace(deviceInfo.toString());
                    this.audioDeviceInfo.updateAudioDeviceInfo(deviceInfo);
                } else {
                    LogUtils.trace("UnExpected Response");
                }
            }
        }catch (PortUnreachableException e) {
            LogUtils.trace("PortUnreachableException~~~~~~~");
        } catch (IOException e) {
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}