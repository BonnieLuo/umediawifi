package com.onyx.wifi.utility.encrypt;

import com.onyx.wifi.utility.CommonUtils;

import tgio.rncryptor.RNCryptorNative;

/* Utility of RNCryptorNative Library (https://github.com/TGIO/RNCryptorNative#rncryptornative) */
public class RNCryptorUtils {

    public static String getMasterKey() {
        // master key 為自定義的亂碼, 使用各種程式運算惡搞組成 master key, 越亂越安全, 萬一程式被反組譯時越不容易看懂
        return "mzb48ac" + new StringBuilder(CommonUtils.getMasterKey()).reverse().toString() + "jc2zAHD45H" + (4 * 6 / 2);
    }

    // [Note] RNCryptorNative Library 的行為:
    // 給定同樣的 plain text 和 master key, 每次加密出來的 cipher text 會不同, 但都可以解密回同樣的 plain text
    public static String encryptString(String plainText, String key) {
        return new String(new RNCryptorNative().encrypt(plainText, key));
    }

    private static String decryptString(String cipherText, String key) {
        return new RNCryptorNative().decrypt(cipherText, key);
    }

    // 若將打 Cloud API 的 base server URL 直接以明文方式寫在程式內, 當 App 被反組譯時可能較容易被看出來 server URL
    // 為了不想在程式內留下明文的 server URL, 故將加密 server URL 得到的密文寫在程式內, 交給程式解密回真正的 server URL
    // 當初是以 getMasterKey() 的 key 去加密 server URL 得到下面那些加密字串的, 故要解密也是傳入 getMasterKey()
    // 詳見專案的 readme.txt -> Cloud Server URL 說明
    public static String getTestServerUrl() {
        return decryptString("AgGKerNRIxmfjZydkJbstFwKzxaLb2XJTpEKSwcNLYLouw+vHxJjDRZ8JZglH+5P9JOBHYsYgIn52intWM1/h/W9W33naAfymHN5Zmbgjq8izLx0oUDbd7jOznpW5//k7RM=", getMasterKey());
    }

//    public static String getStageServerUrl() {
//        return decryptString("AgEeehtyxGeKGJVb0a45qZ5X5/0dxeW2X+uOANfYiotf7mnUWc8/7r1Qfte4dNj21Vhab6CeQZGhzFJDtaoLiugZmuFj31QTiIu26eGDJswdw1WhkBsiimchNCDzcv1ilybZrG7GFxfOkNRqqeV8vk20", getMasterKey());
//    }

    public static String getRdqaServerUrl() {
        return decryptString("AgFoAo0Ai6O68sJbDerG7LyN6i8EYu9zgwCNTok717hAIjh3TCvaL/tPfKiZanMPeaNya+4nQKS7QxDf4cnbHCH+fXIq3hMi8byaXfE5pV2lKFOPPT4aKejxJ5PD+6xwKzk=", getMasterKey());
    }

    public static String getProductionServerUrl() {
        return decryptString("AgHJkAV1rSpcXZTs94xGGCnUrpe4bNFcWxAvcc0k1OFC6la22bHpFa+NcLDdfHq51LO8XwPflydr4T6syOZmsObYK6Pl2d91AozO+ip0SSfDecS5CpoULyXubIARiR7Yhes=", getMasterKey());
    }
}
