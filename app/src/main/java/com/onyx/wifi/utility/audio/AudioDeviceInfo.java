package com.onyx.wifi.utility.audio;

import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.DeviceInfo;

public interface AudioDeviceInfo {
        void updateAudioDeviceInfo(DeviceInfo deviceInfo);
}
