package com.onyx.wifi.utility;

public class StringUtils {

    public static boolean isNullOrEmpty(String str) {
        return (str == null || str.isEmpty());
    }

    /**
     * Convert byte[] to hex string
     *
     * @param src byte[] data
     * @return hex string
     */
    public static String bytes2HexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return "";
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * Convert hex string to byte[]
     *
     * @param hexString the hex string
     * @return byte[]
     */
    public static byte[] hexString2Bytes(String hexString) {
        if (hexString == null) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (char2Byte(hexChars[pos]) << 4 | char2Byte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c char
     * @return byte
     */
    private static byte char2Byte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    // encode plain text string to hex string, ex: "abc123" -> "616263313233"
    public static String encodeToHexString(String plainText) {
        if (plainText == null) {
            return "";
        }
        return bytes2HexString(plainText.getBytes());
    }

    // decode hex string to plain text string , ex: "616263313233" -> "abc123"
    public static String decodeHexString(String hexString) {
        byte[] bytes = hexString2Bytes(hexString);
        if (bytes == null) {
            return "";
        }
        return new String(bytes);
    }

    public static int stringToInt(String str) {
        int value = -1;
        if (isNullOrEmpty(str)) {
            return value;
        }
        try {
            value = Integer.valueOf(str);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return value;
    }
}
