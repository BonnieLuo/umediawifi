package com.onyx.wifi.utility;

public class AppConstants {

    // AppConstants 宣告不變的常數或是字串/型態定義, 通常會被跨頁面使用

    public static final String FIREBASE_PROJECT_NAME_TEST = "TEST";
    public static final String FIREBASE_PROJECT_NAME_RDQA = "RDQA";

    public static final String CLOUD_API_HEADER_NAME = "Authorization";

    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_TEMP_NEW_EMAIL = "tmp_new_email";
    public static final String KEY_DATA = "data";

    // Device Response
    public static final String KEY_STATUS = "status";
    public static final String VALUE_IN_PROGRESS = "InProgress";
    public static final String VALUE_COMPLETED = "Completed";
    public static final String VALUE_ACTIVATED = "Activated";
    public static final String VALUE_CANCEL = "Cancel";
    public static final String VALUE_PENDING = "Pending";
    public static final String VALUE_TIMEOUT = "Timeout";
    public static final String VALUE_FAILED = "Failed";

    // Network Setting
    public static final String KEY_TIMEZONE = "timezone";

    // Voice Assistant
    public static final String KEY_COORDINATE_Y = "coordinate_y";
    public static final String KEY_AUDIO_DEVICE_IP = "audio_device_ip";
    public static final String KEY_AUDIO_DEVICE_DEVICE_IP = "audio_device_device_ip";
    public static final String KEY_AUDIO_DEVICE_UDID = "audio_device_udid";
    public static final String KEY_AUDIO_DEVICE_DID = "audio_device_did";
    public static final String KEY_UNREGISTER_ALEXA = "register_alexa";
    public static final String KEY_SELECTED_INDEX = "selected_index";

    // Sign Up / Sign In / Email Verification
    public static final String EXTRA_USER_EMAIL = "extra_user_email";
    public static final String EXTRA_USER_PASSWORD = "extra_user_password";
    public static final String EXTRA_AUTO_RESEND_VERIFY_CODE = "extra_auto_resend_verify_code";
    public static final String IS_SOCIAL_SIGN_IN = "is_social_sign_in";

    // Dashboard
    public static final String EXTRA_MAIN_MENU_ITEM = "extra_main_menu_item";
    public static final String EXTRA_DID = "extra_did";

    // Node Setting
    public static final String EXTRA_REMOVE_DEVICE = "extra_remove_device";

    // Firmware Update
    public static final String EXTRA_DEVICE_DID = "extra_device_did";
    public static final String EXTRA_DEVICE_FW_STATUS = "extra_device_fw_status";

    // Device Setup
    public static final String EXTRA_SETUP_FLOW = "extra_setup_flow";
    public static final String EXTRA_SETUP_DEVICE = "extra_setup_device";
    public static final String EXTRA_FROM_DEVICE_SETUP = "extra_from_device_setup";
    public static final String EXTRA_ADD_ANOTHER_DEVICE = "extra_add_another_device";
    public static final String EXTRA_RESETUP_ROUTER = "extra_resetup_router";

    // BLE
    public static final String EXTRA_IS_DISCOVERED = "extra_is_discovered";
    public static final String EXTRA_IS_ENABLE = "extra_is_enable";
    public static final String EXTRA_BLE_DATA = "extra_ble_data";

    public static final String INTENT_BLE_ACTION_GATT_CONNECTED = "com.onyx.wifi.ble.ACTION_GATT_CONNECTED";
    public static final String INTENT_BLE_ACTION_GATT_DISCONNECTED = "com.onyx.wifi.ble.ACTION_GATT_DISCONNECTED";
    public static final String INTENT_BLE_ACTION_GATT_DISCOVERED = "com.onyx.wifi.ble.ACTION_GATT_DISCOVERED";
    public static final String INTENT_BLE_ACTION_GATT_NOTIFY = "com.onyx.wifi.ble.ACTION_GATT_NOTIFY";
    public static final String INTENT_BLE_ACTION_DATA_CHANGE = "com.onyx.wifi.ble.ACTION_DATA_CHANGE";

    // Wireless Setting
    public static final String EXTRA_WIRELESS_INFO = "extra_wireless_info";

    // Login (check privacy) (TBD)
    public static final String URL_TERMS_OF_SERVICE = "";
    public static final String URL_PRIVACY_POLICY = "";

    //
    // Audio (Voice Assistant)
    //
    public static final Integer UDP_PORT = 8888;

    // audio device info
    public static final String DEVICE_PRODUCT_ID = "productid";
    public static final String DEVICE_DSN = "dsn";
    public static final String DEVICE_CODE_CHALLENGE = "codechallenge";
    public static final String DEVICE_STATE = "state";
    public static final String DEVICE_ADDRESS = "address";
    public static final String DEVICE_STATUS = "status";
    public static final String DEVICE_METADATE = "metadata";
    // request Auth Code
    public static final String CODE_CHALLENGE_METHOD = "S256";

    // receive and then send Auth code to audio device
    public static final String DEVICE_AUTH_CODE = "AuthCode";
    public static final String DEVICE_CLIENT_ID = "ClientID";
    public static final String DEVICE_REDIRECT_URL = "RedirectURI";

    // item shown in UI
    public static final String DEVICE_STATUS_LINKING = "Linking Account..."; // UI
    public static final String DEVICE_STATUS_COMPLETE = "Complete"; // UI
    public static final String DEVICE_STATUS_CONFIRM = "Confirm";// UI
    public static final String DEVICE_STATUS_ERROR = "Error occurred.Try Again..."; // UI

    // audio device status content
    public static final String DEVICE_AMAZON_REQUEST = "amazon request";
    public static final String DEVICE_AMAZON_RESPONSE = "amazon response";
    public static final String DEVICE_SEND_AUTH_TO_PRODUCT = "sent Auth to Product";
    public static final String DEVICE_STATUS_CONNECTION_COMPLETE = "connection complete";// from device
    public static final String DEVICE_STATUS_REGISTER_REQUEST = "registering request";// from device
    public static final String DEVICE_STATUS_REGISTER_RESPONSE = "registering response";// from device
    public static final String DEVICE_STATUS_MQTT_SUBSCRIBED = "mqtt subscribed";// from device
    public static final String DEVICE_STATUS_CONNECTION_ERROR = "error in connection";
    public static final String DEVICE_STATE_CONFIRM_RESPONSE = "ConfirmationReply"; // X
    public static final String DEVICE_STATE_ALREADY_CONNECTED = "already connected";// from device?

    public static final String SHARED_KEY_LINKING_PROCESS = "Linking process";

    // Broadcast Event
    // (1) Notify events (Firebase Cloud Messaging)
    // (2) Other events triggered by sendBroadcast()
    public static final String EVENT_DASHBOARD_INFO_UPDATE = "event_dashboard_info_update";
    public static final String EVENT_WIRELESS_SETTING_UPDATE = "event_wireless_setting_update";
    public static final String EVENT_SPEED_TEST_UPDATE = "event_speed_test_update";
    public static final String EVENT_GLOBAL_PAUSE_INFO_UPDATE = "event_global_pause_info_update";
    public static final String EVENT_CLIENT_COUNT = "event_client_count";
    public static final String EVENT_DEVICE_INTERNET_STATUS = "event_device_internet_status";
    public static final String EVENT_DEVICE_SPEED_TEST = "event_device_speed_test";
    public static final String EVENT_DEVICE_FIRMWARE_UPDATE = "event_device_firmware_update";
    public static final String EVENT_DEVICE_FIRMWARE_UPDATE_COMPLETED = "event_device_firmware_update_completed";
    public static final String EVENT_DEVICE_ALL_NOTIFY = "event_device_all_notify";

    // Request Code
    public static final int REQUEST_CODE_NODE_SETTING = 1;

    public enum CloudServerMode {
        NONE,
        TEST,
        //        STAGE,
        RDQA,
        PRODUCTION
    }

    public enum SetupMode {
        NONE,
        ROUTER,
        ROUTER_RESETUP,
        REPEATER
    }

    public enum SetupFlow {
        NONE,
        BLE,
        WIFI
    }

    public enum DeviceType {
        // 1:Desktop, 2:Plug, 3:Tower
        NONE("None"),
        DESKTOP("UMEDIA Desk"),
        PLUG("UMEDIA Plug"),
        TOWER("UMEDIA Tower");

        private final String name;

        DeviceType(String s) {
            name = s;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
}
