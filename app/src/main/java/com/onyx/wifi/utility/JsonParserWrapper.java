package com.onyx.wifi.utility;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

public class JsonParserWrapper {

    private final String TAG = this.getClass().getSimpleName();

    private Gson mGson;
    private JsonParser mJsonParser;

    public JsonParserWrapper() {
        mGson = new Gson();
        mJsonParser = new JsonParser();
    }

    public JsonObject parseJson(String json) {
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject = (JsonObject) mJsonParser.parse(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public <T> T jsonToObject(String json, Class<T> classOfT) {
        return mGson.fromJson(json, classOfT);
    }

    public <T> T jsonToObject(String json, Type type) {
        return mGson.fromJson(json, type);
    }

    public <T> T jsonToObject(JsonObject json, Class<T> classOfT) {
        return mGson.fromJson(json, classOfT);
    }

    public <T> T jsonToObject(JsonObject json, Type type) {
        return mGson.fromJson(json, type);
    }

    public String jsonGetString(JsonObject jsonObject, String name) {
        String data = "";
        try {
            data = jsonObject.get(name).getAsString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public JsonArray jsonGetJsonArray(JsonObject jsonObject, String name) {
        JsonArray data = new JsonArray();
        try {
            data = jsonObject.get(name).getAsJsonArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public JsonObject jsonGetJsonObject(JsonObject jsonObject, String name) {
        JsonObject data = new JsonObject();
        try {
            data = jsonObject.get(name).getAsJsonObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
