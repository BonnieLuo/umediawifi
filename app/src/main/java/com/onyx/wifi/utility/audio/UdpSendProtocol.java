package com.onyx.wifi.utility.audio;


import android.os.AsyncTask;

import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpSendProtocol extends AsyncTask <String, Integer, Void>{
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String... params) { // params: (msg: String, address: String)
        String msg = params[0];
        String addr = params[1];
        DatagramSocket ds = null;
        try {
            ds = new DatagramSocket();
            InetAddress sendAddress = InetAddress.getByName(addr);
            DatagramPacket dp;
            dp = new DatagramPacket(msg.getBytes(), msg.length(), sendAddress, AppConstants.UDP_PORT);
            ds.send(dp);
            LogUtils.trace("addr: " + sendAddress + "port: " + AppConstants.UDP_PORT);
            } catch (Exception e) {
            e.printStackTrace();
        }finally {
            ds.close(); // ?
        }
        return null;
    }

    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }
}