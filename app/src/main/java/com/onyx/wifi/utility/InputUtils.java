package com.onyx.wifi.utility;

import android.util.Patterns;

import com.onyx.wifi.MainApp;
import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;

import java.util.Random;
import java.util.regex.Pattern;

public class InputUtils {

    public static boolean isNotEmpty(String str) {
        if (str == null) {
            return false;
        }
        return str.trim().length() > 0;
    }

    public static boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        }
        boolean isValid = Patterns.EMAIL_ADDRESS.matcher(email).matches();
        LogUtils.trace("string: " + email + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isPasswordLengthEnough(String password) {
        if (password == null) {
            return false;
        }
        return password.length() >= 8;
    }

    public static boolean isIpValid(String ip) {
        if (ip == null) {
            return false;
        }
        boolean isValid = Patterns.IP_ADDRESS.matcher(ip).matches();
        LogUtils.trace("string: " + ip + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isNameValid(String name) {
        if (!isNotEmpty(name)) {
            return false;
        }
        String regex = "^[a-zA-Z0-9'-.,_+\\s]{1,20}$";
        boolean isValid = Pattern.compile(regex).matcher(name).matches();
        LogUtils.trace("string: " + name + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isUrlValid(String url) {
        if (url == null) {
            return false;
        }
        String regex = "^[a-zA-Z0-9-_]+(\\.[a-zA-Z0-9-_]+){1,}$";
        boolean isValid = Pattern.compile(regex).matcher(url).matches();
        LogUtils.trace("string: " + url + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isKeywordValid(String keyword) {
        if (keyword == null) {
            return false;
        }
        String regex = "^[a-zA-Z0-9-_]{1,}$";
        boolean isValid = Pattern.compile(regex).matcher(keyword).matches();
        LogUtils.trace("string: " + keyword + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isWifiSsidValid(String ssid) {
        boolean isValid = isCharacterValid(ssid, 1, 32);
        LogUtils.trace("string: " + ssid + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isWifiPasswordValid(String password) {
        boolean isValid = isCharacterValid(password, 8, 63);
        LogUtils.trace("string: " + password + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isPppoeUsernameValid(String username) {
        boolean isValid = isCharacterValid(username, 1, 48);
        LogUtils.trace("string: " + username + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isPppoePasswordValid(String password) {
        boolean isValid = isCharacterValid(password, 1, 48);
        LogUtils.trace("string: " + password + ", is valid: " + isValid);
        return isValid;
    }

    public static boolean isCharacterValid(String input, int minLength, int maxLength) {
        if (input == null) {
            return false;
        }
        // \s: 任何空白字元(空白, 換行, tab), 等同 [ \t\n\x0B\f\r]
        // length: minLength ~ maxLength
        // ex: regex = ^[\sa-zA-Z0-9!#$%&(){|}\[\]\\~:;<=>?@*+,./^_`'"-]{1,48}$
        // 變成 String type, 跳脫字元 '\' 需要再加一次 '\'
        String regex = "^[\\sa-zA-Z0-9!#$%&(){|}\\[\\]\\\\~:;<=>?@*+,./^_`'\"-]{" + String.valueOf(minLength) + "," + String.valueOf(maxLength) + "}$";
        return Pattern.compile(regex).matcher(input).matches();
    }

    public static boolean containsWhitespace(String input) {
        if (input == null) {
            return false;
        }
        String regex = "\\s+";
        return Pattern.compile(regex).matcher(input).find();
    }

    public static String getInvalidNameMessage(String itemName) {
        return getInvalidCharacterMessage(itemName, R.string.err_length_1_20);
    }

    public static String getInvalidWifiSsidMessage(String itemName) {
        return getInvalidCharacterMessage(itemName, R.string.err_length_1_32);
    }

    public static String getInvalidWifiPasswordMessage(String itemName) {
        return getInvalidCharacterMessage(itemName, R.string.err_length_8_63);
    }

    public static String getInvalidPppoeUsernameMessage() {
        return getInvalidCharacterMessage("PPPoE username", R.string.err_length_1_48);
    }

    public static String getInvalidPppoePasswordMessage() {
        return getInvalidCharacterMessage("PPPoE password", R.string.err_length_1_48);
    }

    private static String getInvalidCharacterMessage(String item, int stringId) {
        // item: 請使用者輸入的項目的名稱
        // stringId: 長度限制的 string ID
        return "The " + item + " should use " + MainApp.getInstance().getString(stringId) + " characters!";
    }

    // [Product Design Specification]
    // SSID is randomly select and combine two words together.
    // (1) one place + one object
    // (2) one object + one place
    public static String generateNetworkSsid() {
        Random random = new Random();
        String[] places = MainApp.getInstance().getResources().getStringArray(R.array.place);
        String[] objects = MainApp.getInstance().getResources().getStringArray(R.array.object);
        int indexPlace = random.nextInt(places.length);
        int indexObject = random.nextInt(objects.length);
        int order = random.nextInt(2);
        String ssid;
        if (order == 0) {
            ssid = places[indexPlace] + objects[indexObject];
        } else {
            ssid = objects[indexObject] + places[indexPlace];
        }
        return ssid;
    }

    // [Product Design Specification]
    // Randomly generate password with eight (8) characters long that contains following characters.
    // (1) One Uppercase letter
    // (2) Six Lowercase letters
    // (3) One Number
    // The uppercase letter and number SHOULD be randomly placed within 8 characters.
    public static String generateNetworkPassword() {
        int totalLength = 8;
        Random random = new Random();
        // 產生兩個數字做為 uppercase letter 和 number 的擺放位置
        int indexUppercase = random.nextInt(totalLength);
        int indexNumber = random.nextInt(totalLength);
        while (indexNumber == indexUppercase) {
            indexNumber = random.nextInt(totalLength);
        }
        // 產生每個字元
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < totalLength; i++) {
            if (i == indexUppercase) {
                sb.append(randomUppercaseLetter());
            } else if (i == indexNumber) {
                sb.append(randomNumber());
            } else {
                sb.append(randomLowercaseLetter());
            }
        }
        return sb.toString();
    }

    private static char randomUppercaseLetter() {
        Random random = new Random();
        String upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int index = random.nextInt(upperChars.length());
        return upperChars.charAt(index);
    }

    private static char randomLowercaseLetter() {
        Random random = new Random();
        String lowerChars = "abcdefghijklmnopqrstuvwxyz";
        int index = random.nextInt(lowerChars.length());
        return lowerChars.charAt(index);
    }

    private static char randomNumber() {
        Random random = new Random();
        String number = "0123456789";
        int index = random.nextInt(number.length());
        return number.charAt(index);
    }

    public static String getDefault5gNetworkSsid(String baseNetworkSsid) {
        // max length of SSID is 32
        String ssid5g;
        if (baseNetworkSsid.length() <= 27) {
            ssid5g = baseNetworkSsid + "_5GHz";
        } else {
            ssid5g = baseNetworkSsid.substring(0, 27) + "_5GHz";
        }
        return ssid5g;
    }

    public static String getDefaultGuestNetworkSsid() {
        // max length of SSID is 32
        String ssidGuest;
        String baseNetworkSsid = DataManager.getInstance().getNetworkName();
        if (baseNetworkSsid.length() <= 26) {
            ssidGuest = baseNetworkSsid + "_Guest";
        } else {
            ssidGuest = baseNetworkSsid.substring(0, 26) + "_Guest";
        }
        return ssidGuest;
    }

    public static String getDefaultBnbNetworkSsid() {
        // max length of SSID is 32
        String ssidBnb;
        String baseNetworkSsid = DataManager.getInstance().getNetworkName();
        if (baseNetworkSsid.length() <= 28) {
            ssidBnb = baseNetworkSsid + "_BnB";
        } else {
            ssidBnb = baseNetworkSsid.substring(0, 28) + "_BnB";
        }
        return ssidBnb;
    }
}
