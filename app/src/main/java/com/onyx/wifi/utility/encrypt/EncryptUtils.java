package com.onyx.wifi.utility.encrypt;

import com.onyx.wifi.utility.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtils {

    /**
     * SHA加密
     *
     * @param strSrc 明文
     * @return 加密之後的密文
     */
    public static String shaEncrypt(String strSrc) {
        MessageDigest messageDigest;
        String strDes = "";
        byte[] bt = strSrc.getBytes();
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(bt);
            strDes = StringUtils.bytes2HexString(messageDigest.digest()); // to HexString
        } catch (NoSuchAlgorithmException e) {
            return strDes;
        }
        return strDes;
    }

    /**
     * MD5加密
     *
     * @param strSrc 明文
     * @return 加密之後的密文
     */
    public static String md5Encrypt(String strSrc) {
        MessageDigest messageDigest;
        String strDes = "";
        byte[] bt = strSrc.getBytes();
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(bt);
            strDes = StringUtils.bytes2HexString(messageDigest.digest()); // to HexString
        } catch (NoSuchAlgorithmException e) {
            return strDes;
        }
        return strDes;
    }
}
