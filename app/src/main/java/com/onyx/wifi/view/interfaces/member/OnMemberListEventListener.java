package com.onyx.wifi.view.interfaces.member;

import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Member;

public interface OnMemberListEventListener {
    void onManageMemberClick(Member member);

    void onMemberClick(Member member);

    void onPauseMemberClick(Member member);

    void onClientClick(ClientModel clientModel);
}
