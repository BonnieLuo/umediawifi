package com.onyx.wifi.view.fragment.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentCreatePasswordBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.customized.NoUnderlineSpan;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.login.SignUpListener;
import com.onyx.wifi.viewmodel.login.SignUpViewModel;

public class CreatePasswordFragment extends BaseFragment {

    private FragmentCreatePasswordBinding mBinding;
    private SignUpViewModel mViewModel;

    private SignUpListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignUpListener) {
            mEventListener = (SignUpListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement SignUpListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_password, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        mBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignUpUser().setPassword(editable.toString());
                checkPasswordDataFilled();
            }
        });
        mBinding.etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignUpUser().setConfirmPassword(editable.toString());
                checkPasswordDataFilled();
            }
        });
        mBinding.cbPrivacy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mViewModel.getSignUpUser().setCheckPrivacy(b);
                checkPasswordDataFilled();
            }
        });
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onBack();
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mViewModel.isPasswordDataValid()) {
                    mEventListener.onPasswordDataValid();
                } else {
                    showMessageDialog("Create Password", mViewModel.getInputErrorMsg());
                }
            }
        });

        setClickMessage();
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(SignUpViewModel.class);
    }

    private void setClickMessage() {
        SpannableString msgCheck = new SpannableString(getString(R.string.check_privacy_policy));
        String msgService = getString(R.string.terms_of_service);
        String msgPrivacy = getString(R.string.privacy_policy);
        int startIndex, endIndex;
        NoUnderlineSpan urlSpan;

        // set hyperlink for terms of service
        startIndex = msgCheck.toString().indexOf(msgService);
        endIndex = startIndex + msgService.length();
        urlSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
//                Uri uri = Uri.parse(AppConstants.URL_TERMS_OF_SERVICE);
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
            }
        };
        msgCheck.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgCheck.setSpan(new ForegroundColorSpan(CommonUtils.getColor(mActivity, R.color.white)), startIndex, endIndex, 0);

        // set hyperlink for privacy policy
        startIndex = msgCheck.toString().indexOf(msgPrivacy);
        endIndex = startIndex + msgPrivacy.length();
        urlSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
//                Uri uri = Uri.parse(AppConstants.URL_PRIVACY_POLICY);
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
            }
        };
        msgCheck.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgCheck.setSpan(new ForegroundColorSpan(CommonUtils.getColor(mActivity, R.color.white)), startIndex, endIndex, 0);

        mBinding.tvPrivacy.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvPrivacy.setText(msgCheck, TextView.BufferType.SPANNABLE);
        mBinding.tvPrivacy.setHighlightColor(Color.TRANSPARENT);
    }

    private void checkPasswordDataFilled() {
        if (mViewModel.isPasswordDataFilled()) {
            mBinding.btnContinue.setVisibility(View.VISIBLE);
        } else {
            mBinding.btnContinue.setVisibility(View.INVISIBLE);
        }
    }

    private void setData() {
        mBinding.etPassword.setText(mViewModel.getSignUpUser().getPassword());
        mBinding.etConfirmPassword.setText(mViewModel.getSignUpUser().getConfirmPassword());
        mBinding.cbPrivacy.setChecked(mViewModel.getSignUpUser().getCheckPrivacy());
        checkPasswordDataFilled();
    }
}
