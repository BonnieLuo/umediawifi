package com.onyx.wifi.view.activity.menubar.pause;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityGlobalPauseBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.model.item.member.PauseInfoModel;
import com.onyx.wifi.model.item.pause.PauseProfile;
import com.onyx.wifi.model.item.pause.PauseProfileModel;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.pause.GlobalPauseViewModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class GlobalPauseActivity extends BaseActivity {

    private ActivityGlobalPauseBinding mBinding;

    private ImageView mCloseImageView;

    private List<PauseProfileModel> mPauseProfileList = new ArrayList<>();

    private List<PauseInfoModel> mPauseInfoList = new ArrayList<>();

    private GlobalPauseViewModel mViewModel;

    private double mDuration = 0;
    private String mDurationString = null;

    private CountDownTimer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //android O fix bug orientation
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_global_pause);

        mCloseImageView = findViewById(R.id.closeImageView);
        mCloseImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBinding.createArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(GlobalPauseActivity.this, EditPauseProfileActivity.class));
                Intent intent = new Intent(mContext, EditPauseProfileActivity.class);
                intent.putExtra("is_create_new_profile", true);
                startActivity(intent);
            }
        });

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mViewModel.getGlobalPauseList();
        mBinding.createLayout.setVisibility(View.GONE);
    }

    void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(GlobalPauseViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Pause.GET_GLOBAL_PAUSE_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Global Pause", resultStatus.errorMsg);

                            return;
                        }

                        mPauseProfileList = new ArrayList<>();
                        mPauseInfoList = new ArrayList<>();

                        JsonObject json = (JsonObject) resultStatus.data;

                        JsonObject dataJSON = json.getAsJsonObject("data");
                        JsonArray pauseProfileArray = dataJSON.getAsJsonArray("pause_profile");

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<PauseProfileModel>>() {
                        }.getType();
                        List<PauseProfileModel> pauseProfileList = new ArrayList<>();

                        if (pauseProfileArray != null) {
                            pauseProfileList = gson.fromJson(pauseProfileArray.toString(), listType);
                        }

                        if (pauseProfileList == null) {
                            return;
                        }

                        JsonElement jsonElement = dataJSON.get("pause_status");

                        if (jsonElement != null && !jsonElement.isJsonNull()) {
                            JsonArray pauseStatusArray = jsonElement.getAsJsonArray();

                            if (pauseStatusArray != null) {
                                Type pauseInfoListType = new TypeToken<List<PauseInfoModel>>() {
                                }.getType();
                                mPauseInfoList = gson.fromJson(pauseStatusArray.toString(), pauseInfoListType);
                            }
                        } else {
                            mPauseInfoList = null;
                        }

                        if (mPauseProfileList == null) {
                            return;
                        }

                        if (pauseProfileList.isEmpty()) {
                            mBinding.vNone.setVisibility(View.GONE);
                            mBinding.vHasAtLeastOneProfile.setVisibility(View.GONE);
                            mBinding.pause1Layout.setVisibility(View.GONE);
                            mBinding.pause2Layout.setVisibility(View.GONE);
                            mBinding.createLayout.setVisibility(View.VISIBLE);

                            return;
                        }

                        for (PauseProfileModel profileModel : pauseProfileList) {
                            String type = profileModel.getType();
                            if (type.equalsIgnoreCase("1")) {
                                mPauseProfileList.add(profileModel);
                            }
                        }

                        if (mPauseProfileList.size() >= 2) {
                            mBinding.pause1Layout.setVisibility(View.VISIBLE);
                            mBinding.pause2Layout.setVisibility(View.VISIBLE);
                            mBinding.createLayout.setVisibility(View.GONE);
                            mBinding.vNone.setVisibility(View.VISIBLE);
                            mBinding.vHasAtLeastOneProfile.setVisibility(View.VISIBLE);

                            setupPause1();
                            setupPause2();
                        } else if (mPauseProfileList.size() == 1) {
                            mBinding.pause1Layout.setVisibility(View.VISIBLE);
                            mBinding.pause2Layout.setVisibility(View.GONE);
                            mBinding.createLayout.setVisibility(View.VISIBLE);
                            mBinding.vNone.setVisibility(View.VISIBLE);
                            mBinding.vHasAtLeastOneProfile.setVisibility(View.VISIBLE);

                            setupPause1();
                        } else {
                            mBinding.pause1Layout.setVisibility(View.GONE);
                            mBinding.pause2Layout.setVisibility(View.GONE);
                            mBinding.createLayout.setVisibility(View.VISIBLE);
                            mBinding.vNone.setVisibility(View.VISIBLE);
                            mBinding.vHasAtLeastOneProfile.setVisibility(View.GONE);
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setPauseProfileList(mPauseProfileList);
                        dataManager.setEditPauseProfile(null);

                        // Dashboard 需要顯示 global pause 的狀態, 故先將整理後的資料存到 share preference
                        boolean find = false;
                        for (PauseInfoModel pauseInfoModel : mPauseInfoList) {
                            PauseInfo pauseInfo = new PauseInfo(pauseInfoModel);
                            String type = pauseInfo.getType();
                            boolean isEnabled = pauseInfo.isEnabled();
                            int minuteLeft = pauseInfo.getMinuteLeft();
                            // type = 1 是 global pause
                            if ("1".equals(type) && isEnabled && minuteLeft > 0) {
                                // 找到第一筆 enable = 1 且 left minute > 0 的 profile, 存到 share preference
                                DataManager.getInstance().setPauseInfoModel(pauseInfoModel);
                                find = true;
                                break;
                            }
                        }
                        if (!find) {
                            // 如果沒有找到符合上述條件的 pause info, 代表 global pause 是 disable 的狀態
                            // 儲存 enable = 0 的資料到 share preference
                            DataManager.getInstance().setPauseInfoModel(new PauseInfoModel());
                        }

                        return;
                    }

                    if (resultStatus.actionCode == Code.Action.Pause.ENABLE_GLOBAL_PAUSE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Enable Global Pause", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;

                        JsonElement statusJson = json.get("status");
                        String status = statusJson.getAsString();

                        if (status.equalsIgnoreCase("Failed")) {
                            showMessageDialog("Device", "Command fail");

                            return;
                        }

                        JsonObject dataJSON = json.getAsJsonObject("data");
                        if (dataJSON != null) {
                            JsonElement ruleIdJson = dataJSON.get("rule_id");
                            String ruleId = ruleIdJson.getAsString();
                            JsonElement enableJson = dataJSON.get("enable");
                            String enable = enableJson.getAsString();

                            if (mPauseInfoList != null && !mPauseInfoList.isEmpty()) {
                                for (PauseInfoModel pauseInfoModel : mPauseInfoList) {
                                    String modelRuleId = pauseInfoModel.getRuleId();
                                    if (modelRuleId.equalsIgnoreCase(ruleId)) {
                                        pauseInfoModel.setEnable(enable);
                                    }
                                }
                            }
                        }
                        else { // the case that all client devices are excluded or there exists no client
                            CommonUtils.toast(mContext, "No client is included in this profile.");
                        }
                        mViewModel.getGlobalPauseList();
                    }

                    if (resultStatus.actionCode == Code.Action.Pause.DISABLE_GLOBAL_PAUSE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Disable Global Pause", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;

                        JsonElement statusJson = json.get("status");
                        String status = statusJson.getAsString();

                        if (status.equalsIgnoreCase("Failed")) {
                            showMessageDialog("Device", "Command fail");

                            return;
                        }

                        JsonObject dataJSON = json.getAsJsonObject("data");
                        JsonElement ruleIdJson = dataJSON.get("rule_id");
                        String ruleId = ruleIdJson.getAsString();
                        JsonElement enableJson = dataJSON.get("enable");
                        String enable = enableJson.getAsString();

                        if (mPauseInfoList != null && !mPauseInfoList.isEmpty()) {
                            for (PauseInfoModel pauseInfoModel : mPauseInfoList) {
                                String modelRuleId = pauseInfoModel.getRuleId();
                                if (modelRuleId.equalsIgnoreCase(ruleId)) {
                                    pauseInfoModel.setEnable(enable);
                                }
                            }
                        }

                        mViewModel.getGlobalPauseList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Member List", resultStatus.errorMsg);

                            return;
                        }
                    }

                }
            }
        });
    }

    private void setupPause1() {
        PauseProfileModel pauseProfile = mPauseProfileList.get(0);
        String name = pauseProfile.getName();
        mBinding.pause1TitleTextView.setText(name);

        mBinding.clToggleP1EnableStatus.setOnClickListener(new View.OnClickListener() { //pause1Layout
            @Override
            public void onClick(View v) {
                PauseProfileModel pauseProfile = mPauseProfileList.get(0);
                String profileId = pauseProfile.getProfileId();
                String durationString = pauseProfile.getDurationString();

                if (mPauseInfoList != null && !mPauseInfoList.isEmpty()) {
                    for (PauseInfoModel pauseInfoModel : mPauseInfoList) {
                        String modelProfileId = pauseInfoModel.getProfileId();//pauseProfile.getProfileId();

                        if (modelProfileId.equalsIgnoreCase(profileId)) { // Bonnie <-原本要walk around 先找enable == "1"
                            String enable = pauseInfoModel.getEnable();
                            if (enable.equalsIgnoreCase("0")) {
                                setupPauseProfile1PauseState(0, durationString);

                                if (mPauseProfileList.size() > 1) {
                                    mBinding.pause2Container.setVisibility(View.INVISIBLE);
                                    mBinding.pause2ArrowImageView.setVisibility(View.VISIBLE);
                                }

                                mViewModel.enableGlobalPause(profileId);

                                return;
                            } else {
                                setupPauseProfile1CancelState();
                                setupPauseProfile2CancelState();
                                mViewModel.disableGlobalPause(profileId, pauseInfoModel.getRuleId());
                                cancelTimer();

                                return;
                            }
                        }
                    }
                }

                setupPauseProfile1PauseState(0, durationString);

                if (mPauseProfileList.size() > 1) {
                    mBinding.pause2Container.setVisibility(View.INVISIBLE);
                    mBinding.pause2ArrowImageView.setVisibility(View.VISIBLE);
                }

                mViewModel.enableGlobalPause(profileId);

            }
        });

        mBinding.pause1ArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseProfileModel model = mPauseProfileList.get(0);
                PauseProfile pauseProfile = new PauseProfile(model);

                DataManager dataManager = DataManager.getInstance();
                dataManager.setEditPauseProfile(pauseProfile);

                startActivity(new Intent(GlobalPauseActivity.this, EditPauseProfileActivity.class));
            }
        });

        setupPause1Status();
    }

    private void reDrawProgresBar(boolean isProfile1, long minuteLeftInMillis) {
        cancelTimer();

        mTimer = new CountDownTimer(minuteLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //vertifyBtn.setText((millisUntilFinished / 1000)   " second");

                double value = mDuration - (double)millisUntilFinished / 1000 / 60;
                double normalizeValue = normalize(value, 0, mDuration);
                int progress = (int) (normalizeValue * 85);

                if (isProfile1)
                    setupPauseProfile1PauseState(progress, mDurationString);
                else
                    setupPauseProfile2PauseState(progress, mDurationString);
            }

            @Override
            public void onFinish() {
                setupPauseProfile1CancelState();
                setupPauseProfile2CancelState();
            }
        };
        mTimer.start();
    }

    private void setupPause1Status() {
        if (mPauseInfoList == null) {
            mBinding.pause1ImageView.setImageResource(R.drawable.ic_global_pause);
            mBinding.pause1ImageView.setVisibility(View.VISIBLE);

            mBinding.progressBar1.setProgress(0);
            mBinding.progressBar1.setVisibility(View.INVISIBLE);

            mBinding.durationTextView1.setText("");
            mBinding.durationTextView1.setVisibility(View.INVISIBLE);

            return;
        }

        if (mPauseInfoList.isEmpty()) {
            setupPauseProfile1CancelState();

            return;
        }

        if (mPauseProfileList.size() > 1) {
            PauseProfileModel pauseProfile = mPauseProfileList.get(1);
            String profileId = pauseProfile.getProfileId();

            for (PauseInfoModel model : mPauseInfoList) {
                String modelProfileId = model.getProfileId();
                String pause2Enable = model.getEnable();
                PauseInfo info = new PauseInfo(model);
                int minuteLeft = info.getMinuteLeft();
                if (modelProfileId != null) {
                    if (profileId.equalsIgnoreCase(modelProfileId)) {
                        if (pause2Enable.equalsIgnoreCase("1") &&
                                minuteLeft > 0) {
                            mBinding.pause1Container.setVisibility(View.INVISIBLE);
                            mBinding.pause1ArrowImageView.setVisibility(View.VISIBLE);

                            return;
                        }
                    }
                }
            }
        }

        PauseProfileModel pauseProfile = mPauseProfileList.get(0);
        String profileId = pauseProfile.getProfileId();

        for (PauseInfoModel model : mPauseInfoList) {
            String modelProfileId = model.getProfileId();
            if (modelProfileId != null) {
                if (profileId.equalsIgnoreCase(modelProfileId)){// && model.getEnable().equalsIgnoreCase("1")) {// Bonnie
                    PauseInfo info = new PauseInfo(model);
                    int minuteLeft = info.getMinuteLeft();

                    double duration = (double) info.getDuration();

                    if (minuteLeft <= 0 || !info.isEnabled()) { // 解決有時候global pause開起來沒有enabled的profile的pause icon會消失
                        mBinding.pause1ImageView.setImageResource(R.drawable.ic_global_pause);
                        mBinding.pause1ImageView.setVisibility(View.VISIBLE);

                        mBinding.progressBar1.setProgress(0);
                        mBinding.progressBar1.setVisibility(View.INVISIBLE);

                        mBinding.durationTextView1.setText("");
                        mBinding.durationTextView1.setVisibility(View.INVISIBLE);

                        return; //Bonnie 出現profile_id重複的狀況
                    } else {
                        mBinding.pause1ImageView.setImageResource(0);
                        mBinding.pause1ImageView.setVisibility(View.INVISIBLE);

                        if (minuteLeft != 0 && duration != 0) {
                            double value = duration - minuteLeft;
                            double normalizeValue = normalize(value, 0, duration);
                            int progress = (int) (normalizeValue * 85);

                            String durationString = info.getDurationString();

                            //setupPauseProfile1PauseState(progress, durationString);

                            //////////////////////////////////////
                            mDuration = duration;
                            mDurationString = durationString;

                            reDrawProgresBar(true, minuteLeft*60*1000);
                        }

                        return; //Bonnie 出現profile_id重複的狀況
                    }
                }
            }
        }

        setupPauseProfile1CancelState();
    }

    private void setupPauseProfile1PauseState(int progress, String durationString) {
        mBinding.pause2Container.setVisibility(View.INVISIBLE);

        mBinding.pause2ImageView.setVisibility(View.INVISIBLE);
        mBinding.progressBar2.setVisibility(View.INVISIBLE);
        mBinding.durationTextView2.setVisibility(View.INVISIBLE);
        mBinding.pause2ArrowImageView.setVisibility(View.VISIBLE);

        mBinding.pause1ImageView.setVisibility(View.INVISIBLE);
        mBinding.progressBar1.setSecondaryProgress(85);
        mBinding.progressBar1.setProgress(progress);
        mBinding.progressBar1.setVisibility(View.VISIBLE);

        mBinding.durationTextView1.setText(durationString);
        mBinding.durationTextView1.setVisibility(View.VISIBLE);

        mBinding.pause1ArrowImageView.setVisibility(View.INVISIBLE);
    }

    private void setupPauseProfile1CancelState() {
        mBinding.pause2Container.setVisibility(View.VISIBLE);

        mBinding.pause1TitleTextView.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));

        mBinding.pause1ImageView.setImageResource(R.drawable.ic_global_pause);
        mBinding.pause1ImageView.setVisibility(View.VISIBLE);

        mBinding.progressBar1.setProgress(0);
        mBinding.progressBar1.setVisibility(View.INVISIBLE);

        mBinding.durationTextView1.setText("");
        mBinding.durationTextView1.setVisibility(View.INVISIBLE);

        mBinding.pause1ArrowImageView.setVisibility(View.VISIBLE);
    }

    private void setupPause2() {
        PauseProfileModel pauseProfile = mPauseProfileList.get(1);
        String name = pauseProfile.getName();
        mBinding.pause2TitleTextView.setText(name);

        mBinding.clToggleP2EnableStatus.setOnClickListener(new View.OnClickListener() { //pause2Layout
            @Override
            public void onClick(View v) {
                PauseProfileModel pauseProfile = mPauseProfileList.get(1);
                String profileId = pauseProfile.getProfileId();
                String durationString = pauseProfile.getDurationString();

                if (mPauseInfoList != null && !mPauseInfoList.isEmpty()) {
                    for (PauseInfoModel pauseInfoModel : mPauseInfoList) {
                        String modelProfileId = pauseInfoModel.getProfileId();
                        if (modelProfileId.equalsIgnoreCase(profileId)) { // Bonnie <-原本要walk around 先找enable == "1"
                            String enable = pauseInfoModel.getEnable();
                            if (enable.equalsIgnoreCase("0")) {

                                setupPauseProfile2PauseState(0, durationString);

                                if (mPauseProfileList.size() > 1) {
                                    mBinding.pause1Container.setVisibility(View.INVISIBLE);
                                    mBinding.pause1ArrowImageView.setVisibility(View.VISIBLE);
                                }

                                mViewModel.enableGlobalPause(profileId);

                                return;
                            } else {
                                setupPauseProfile1CancelState();
                                setupPauseProfile2CancelState();
                                mViewModel.disableGlobalPause(profileId, pauseInfoModel.getRuleId());
                                cancelTimer();
                                return;
                            }
                        }
                    }
                }

                setupPauseProfile2PauseState(0, durationString);

                if (mPauseProfileList.size() > 1) {
                    mBinding.pause1Container.setVisibility(View.INVISIBLE);
                    mBinding.pause1ArrowImageView.setVisibility(View.VISIBLE);
                }

                mViewModel.enableGlobalPause(profileId);
            }
        });

        mBinding.pause2ArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseProfileModel model = mPauseProfileList.get(1);
                PauseProfile pauseProfile = new PauseProfile(model);

                DataManager dataManager = DataManager.getInstance();
                dataManager.setEditPauseProfile(pauseProfile);

                startActivity(new Intent(GlobalPauseActivity.this, EditPauseProfileActivity.class));
            }
        });

        setupPause2Status();
    }

    private void setupPause2Status() {
        if (mPauseInfoList == null) {
            mBinding.pause2ImageView.setImageResource(R.drawable.ic_global_pause);
            mBinding.pause2ImageView.setVisibility(View.VISIBLE);

            mBinding.progressBar2.setProgress(0);
            mBinding.progressBar2.setVisibility(View.INVISIBLE);

            mBinding.durationTextView2.setText("");
            mBinding.durationTextView2.setVisibility(View.INVISIBLE);

            return;
        }

        if (mPauseInfoList.isEmpty()) {
            setupPauseProfile2CancelState();

            return;
        }

        if (mPauseProfileList.size() > 1) {
            PauseProfileModel pauseProfile = mPauseProfileList.get(0);
            String profileId = pauseProfile.getProfileId();

            for (PauseInfoModel model : mPauseInfoList) {
                String modelProfileId = model.getProfileId();
                String pause1Enable = model.getEnable();
                PauseInfo info = new PauseInfo(model);
                int minuteLeft = info.getMinuteLeft();
                if (modelProfileId != null) {
                    if (profileId.equalsIgnoreCase(modelProfileId)) {
                        if (pause1Enable.equalsIgnoreCase("1") &&
                                minuteLeft > 0) {
                            mBinding.pause2Container.setVisibility(View.INVISIBLE);
                            mBinding.pause2ArrowImageView.setVisibility(View.VISIBLE);

                            return;
                        }
                    }
                }
            }
        }

        PauseProfileModel pauseProfile = mPauseProfileList.get(1);
        String profileId = pauseProfile.getProfileId();

        for (PauseInfoModel model : mPauseInfoList) {
            String modelProfileId = model.getProfileId();
            if (modelProfileId != null) {
                if (profileId.equalsIgnoreCase(modelProfileId)){// && model.getEnable().equalsIgnoreCase("1")) { // Bonnie
                    PauseInfo info = new PauseInfo(model);
                    int minuteLeft = info.getMinuteLeft();

                    double duration = (double) info.getDuration();

                    if (minuteLeft <= 0 || !info.isEnabled()) { // 解決有時候global pause開起來沒有enabled的profile的pause icon會消失
                        mBinding.pause2ImageView.setImageResource(R.drawable.ic_global_pause);
                        mBinding.pause2ImageView.setVisibility(View.VISIBLE);

                        mBinding.progressBar2.setProgress(0);
                        mBinding.progressBar2.setVisibility(View.INVISIBLE);

                        mBinding.durationTextView2.setText("");
                        mBinding.durationTextView2.setVisibility(View.INVISIBLE);

                        return;
                    } else {
                        mBinding.pause2ImageView.setImageResource(0);
                        mBinding.pause2ImageView.setVisibility(View.INVISIBLE);

                        if (minuteLeft != 0 && duration != 0) {
                            double value = duration - minuteLeft;
                            double normalizeValue = normalize(value, 0, duration);
                            int progress = (int) (normalizeValue * 85);

                            String durationString = info.getDurationString();

                            //setupPauseProfile2PauseState(progress, durationString);

                            //////////////////////////////////////
                            mDuration = duration;
                            mDurationString = durationString;

                            reDrawProgresBar(false, minuteLeft*60*1000);
                        }

                        return;
                    }
                }
            }
        }

        setupPauseProfile2CancelState();
    }

    private void setupPauseProfile2PauseState(int progress, String durationString) {
        mBinding.pause1Container.setVisibility(View.INVISIBLE);

        mBinding.pause1ImageView.setVisibility(View.INVISIBLE);
        mBinding.progressBar1.setVisibility(View.INVISIBLE);
        mBinding.durationTextView1.setVisibility(View.INVISIBLE);
        mBinding.pause1ArrowImageView.setVisibility(View.VISIBLE);

        mBinding.pause2TitleTextView.setTextColor(CommonUtils.getColor(mContext, R.color.purple_4e1393));
        mBinding.pause2ImageView.setVisibility((View.INVISIBLE));
        mBinding.progressBar2.setSecondaryProgress(85);
        mBinding.progressBar2.setProgress(progress);
        mBinding.progressBar2.setVisibility(View.VISIBLE);

        mBinding.durationTextView2.setText(durationString);
        mBinding.durationTextView2.setVisibility(View.VISIBLE);

        mBinding.pause2ArrowImageView.setVisibility(View.INVISIBLE);
    }

    private void setupPauseProfile2CancelState() {
        mBinding.pause1Container.setVisibility(View.VISIBLE);

        mBinding.pause2TitleTextView.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
        mBinding.pause2ImageView.setImageResource(R.drawable.ic_global_pause);
        mBinding.pause2ImageView.setVisibility(View.VISIBLE);

        mBinding.progressBar2.setProgress(0);
        mBinding.progressBar2.setVisibility(View.INVISIBLE);

        mBinding.durationTextView2.setText("");
        mBinding.durationTextView2.setVisibility(View.INVISIBLE);

        mBinding.pause2ArrowImageView.setVisibility(View.VISIBLE);
    }

    double normalize(double value, double min, double max) {
        return ((value - min) / (max - min));
    }

}