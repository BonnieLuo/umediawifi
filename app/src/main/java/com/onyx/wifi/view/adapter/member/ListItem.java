package com.onyx.wifi.view.adapter.member;

public class ListItem<T, I> {
    private T type;

    private I item;

    public ListItem(T type, I item) {
        this.type = type;
        this.item = item;
    }

    public T getType() {
        return type;
    }

    public I getItem() {
        return item;
    }
}
