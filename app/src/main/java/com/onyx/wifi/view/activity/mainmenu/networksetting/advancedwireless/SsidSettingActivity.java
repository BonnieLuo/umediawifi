package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessSsidSettingsBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless2G;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless2GModel;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless5G;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless5GModel;
import com.onyx.wifi.model.item.advancewireless.ssid.WirelessModel;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.SsidSettingsViewModel;

import java.util.ArrayList;
import java.util.Arrays;

public class SsidSettingActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessSsidSettingsBinding mBinding;

    private SsidSettingsViewModel mViewModel;

    private Wireless5G mWireless5G;

    private Wireless2G mWireless2G;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_ssid_settings);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            setData();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.editChannel5gImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();

                String[] channelDisplayArray = {"Auto", "CH 149", "CH 153", "CH 157", "CH 161", "CH 165"};
                String[] channelArray = {"Auto", "149", "153", "157", "161", "165"};
                final ArrayList<String> options = new ArrayList<String>(Arrays.asList(channelDisplayArray));

                optionListDialog.setOptionString(options);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        if (position == 0) {
                            mWireless5G.setChannel("0");
                        } else {
                            String channel = channelArray[position];//options.get(position);
                            mWireless5G.setChannel(channel);

                            if (channel.equalsIgnoreCase("165")) {
                                mWireless5G.setBandwidth("0");
                            }
                        }

                        setWirelss5G();

                        checkDiff();

                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.editBandwidth5gImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();

                String[] bandwidthArray = {"20", "20/40", "20/40/80"};

                String currentChannel = mWireless5G.getChannel();
                if (currentChannel.equalsIgnoreCase("165")) {
                    bandwidthArray = new String[]{"20"};
                }

                final ArrayList<String> options = new ArrayList<String>(Arrays.asList(bandwidthArray));

                optionListDialog.setOptionString(options);
                String[] finalBandwidthArray = bandwidthArray;
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        String bandwidth = String.valueOf(position);
                        mWireless5G.setBandwidth(bandwidth);

                        setWirelss5G();

                        checkDiff();

                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.ssid5gBroadcastImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWireless5G.toggleSsidBroadcast();

                setWirelss5G();

                checkDiff();
            }
        });

        mBinding.editChannelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();

                String[] channelDisplayArray = {"Auto", "CH 1", "CH 2", "CH 3", "CH 4", "CH 5", "CH 6", "CH 7", "CH 8", "CH 9", "CH 10", "CH 11"};
                String[] channelArray = {"Auto", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
                final ArrayList<String> options = new ArrayList<String>(Arrays.asList(channelDisplayArray));

                optionListDialog.setOptionString(options);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        if (position == 0) {
                            mWireless2G.setChannel("0");
                        } else {
                            String channel = channelArray[position];//options.get(position);
                            mWireless2G.setChannel(channel);
                        }

                        setWirelss2G();

                        checkDiff();

                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.editBandwidthImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();

                String[] bandwidthArray = {"20", "20/40"};
                final ArrayList<String> options = new ArrayList<String>(Arrays.asList(bandwidthArray));

                optionListDialog.setOptionString(options);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        String bandwidth = String.valueOf(position);
                        mWireless2G.setBandwidth(bandwidth);

                        setWirelss2G();

                        checkDiff();

                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.ssidBroadcastImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWireless2G.toggleSsidBroadcast();

                setWirelss2G();

                checkDiff();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Advanced Wireless");

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSettings();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SsidSettingsViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.SET_SSID) {
                        if (!resultStatus.success) {
                            showMessageDialog("Setup SSID", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }
                }
            }
        });
    }

    private void setData(){
        DataManager dataManager = DataManager.getInstance();
        AdvanceWirelessSettings advanceWirelessSettings = dataManager.getAdvanceWirelessSettings();

        if (advanceWirelessSettings == null) {
            return;
        }

        WirelessModel wirelessModel = advanceWirelessSettings.getWireless();

        if (wirelessModel == null) {
            return;
        }

        // 5G

        Wireless5GModel wireless5GModel = wirelessModel.getWirelss5G();
        mWireless5G = new Wireless5G(wireless5GModel);

        if (mWireless5G == null) {
            return;
        }
        setWirelss5G();

        // 2.4G

        Wireless2GModel wirelss2G = wirelessModel.getWirelss2G();
        mWireless2G = new Wireless2G(wirelss2G);

        if (mWireless2G == null) {
            return;
        }

        setWirelss2G();
    }

    private void setWirelss5G() {
        String channel = mWireless5G.getChannel();
        if (channel != null) {
            if (channel.equalsIgnoreCase("0")) {
                mBinding.channel5gTextView.setText("Auto");
            } else {
                mBinding.channel5gTextView.setText("CH " + channel);
            }
        }/* else { // Bug #8546
            mBinding.channel5gTextView.setText("Auto");
        }*/

        String bandwidth = mWireless5G.getBandwidth();
        if (bandwidth != null) {
            if (bandwidth.equalsIgnoreCase("0")) {
                mBinding.bandwidth5gTextView.setText("20");
            }

            if (bandwidth.equalsIgnoreCase("1")) {
                mBinding.bandwidth5gTextView.setText("20/40");
            }

            if (bandwidth.equalsIgnoreCase("2")) {
                mBinding.bandwidth5gTextView.setText("20/40/80");
            }
        }

        boolean isSsidBroadcast = mWireless5G.isSsidBroadcast();
        mBinding.ssid5gBroadcastImageView.setSelected(isSsidBroadcast);

        if (isSsidBroadcast) {
            mBinding.ssid5gTextView.setText("Broadcast");
        } else {
            mBinding.ssid5gTextView.setText("Disabled");
        }
    }

    private void setWirelss2G() {
        String channel = mWireless2G.getChannel();
        if (channel != null) {
            if (channel.equalsIgnoreCase("0")) {
                mBinding.channelTextView.setText("Auto");
            } else {
                mBinding.channelTextView.setText("CH " + channel);
            }
        } /*else { // Bug #8546
            mBinding.channelTextView.setText("Auto");
        }*/

        String bandwidth = mWireless2G.getBandwidth();
        if (bandwidth != null) {
            if (bandwidth.equalsIgnoreCase("0")) {
                mBinding.bandwidthTextView.setText("20");
            }

            if (bandwidth.equalsIgnoreCase("1")) {
                mBinding.bandwidthTextView.setText("20/40");
            }

            if (bandwidth.equalsIgnoreCase("2")) {
                mBinding.bandwidthTextView.setText("20/40/80");
            }
        }

        boolean isSsidBroadcast = mWireless2G.isSsidBroadcast();
        mBinding.ssidBroadcastImageView.setSelected(isSsidBroadcast);

        if (isSsidBroadcast) {
            mBinding.ssidTextView.setText("Broadcast");
        } else {
            mBinding.ssidTextView.setText("Disabled");
        }
    }

    private void checkDiff() {
        boolean is5GChanged = mWireless5G.isChanged();
        boolean is2GChanged = mWireless2G.isChanged();

        if (is5GChanged || is2GChanged) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void updateSettings() {
        mViewModel.updateSSID(mWireless5G, mWireless2G);
    }

}
