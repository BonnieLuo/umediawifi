package com.onyx.wifi.view.activity.setup.router;

import android.arch.lifecycle.Observer;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityRouterProvisionBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.activity.setup.repeater.RepeaterSetupActivity;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.view.fragment.setup.DeviceSetLabelFragment;
import com.onyx.wifi.view.fragment.setup.router.CheckCableFragment;
import com.onyx.wifi.view.fragment.setup.router.CheckCableRetryFragment;
import com.onyx.wifi.view.fragment.setup.router.CreateNetworkFragment;
import com.onyx.wifi.view.fragment.setup.router.RouterSetupSuccessFragment;
import com.onyx.wifi.view.fragment.setup.router.SelectConnectionTypeFragment;
import com.onyx.wifi.view.fragment.setup.router.SetPppoeFragment;
import com.onyx.wifi.view.fragment.setup.router.SetStaticIpFragment;
import com.onyx.wifi.view.fragment.setup.router.SetWifiFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

public class RouterProvisionActivity extends BaseDeviceSetupActivity implements DeviceSetupListener {

    private CheckCableFragment mCheckCableFragment;
    private CheckCableRetryFragment mCheckCableRetryFragment;
    private SelectConnectionTypeFragment mSelectConnectionTypeFragment;
    private SetPppoeFragment mSetPppoeFragment;
    private SetStaticIpFragment mSetStaticIpFragment;
    private DeviceSetLabelFragment mRouterSetLabelFragment;
    private SetWifiFragment mSetWifiFragment;
    private CreateNetworkFragment mCreateNetworkFragment;
    private RouterSetupSuccessFragment mRouterSetupSuccessFragment;

    private ActivityRouterProvisionBinding mBinding;

    private AppConstants.SetupFlow mSetupFlow;
    private SetupDevice mSetupDevice;

    private boolean mIsNeedListenBleState = false;  // for BLE setup flow
    private boolean mIsNeedDetectWanType = false;
    private boolean mIsNeedGetProvisionState = false;
    private int mGetProvisionStateCount = 0;

    private JsonParserWrapper mJsonParser;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSetupFlow = (AppConstants.SetupFlow) getIntent().getSerializableExtra(AppConstants.EXTRA_SETUP_FLOW);
        DataManager.getInstance().setSetupFlow(mSetupFlow);
        mSetupDevice = getIntent().getParcelableExtra(AppConstants.EXTRA_SETUP_DEVICE);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_router_provision);
        setView();
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mCheckCableFragment = (CheckCableFragment) mFragmentManager.findFragmentByTag(CheckCableFragment.class.getName());
            mCheckCableRetryFragment = (CheckCableRetryFragment) mFragmentManager.findFragmentByTag(CheckCableRetryFragment.class.getName());
            mSelectConnectionTypeFragment = (SelectConnectionTypeFragment) mFragmentManager.findFragmentByTag(SelectConnectionTypeFragment.class.getName());
            mSetPppoeFragment = (SetPppoeFragment) mFragmentManager.findFragmentByTag(SetPppoeFragment.class.getName());
            mSetStaticIpFragment = (SetStaticIpFragment) mFragmentManager.findFragmentByTag(SetStaticIpFragment.class.getName());
            mRouterSetLabelFragment = (DeviceSetLabelFragment) mFragmentManager.findFragmentByTag(DeviceSetLabelFragment.class.getName());
            mSetWifiFragment = (SetWifiFragment) mFragmentManager.findFragmentByTag(SetWifiFragment.class.getName());
            mCreateNetworkFragment = (CreateNetworkFragment) mFragmentManager.findFragmentByTag(CreateNetworkFragment.class.getName());
            mRouterSetupSuccessFragment = (RouterSetupSuccessFragment) mFragmentManager.findFragmentByTag(RouterSetupSuccessFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            // 顯示第一頁, 隱藏其他頁
            // 該頁面第一頁的 layout 不是寫成 Fragment 而直寫在 Activity 內, 故沒有 fragmentTransaction.show()
            if (mCheckCableFragment != null) {
                fragmentTransaction.hide(mCheckCableFragment);
            }
            if (mCheckCableRetryFragment != null) {
                fragmentTransaction.hide(mCheckCableRetryFragment);
            }
            if (mSelectConnectionTypeFragment != null) {
                fragmentTransaction.hide(mSelectConnectionTypeFragment);
            }
            if (mSetPppoeFragment != null) {
                fragmentTransaction.hide(mSetPppoeFragment);
            }
            if (mSetStaticIpFragment != null) {
                fragmentTransaction.hide(mSetStaticIpFragment);
            }
            if (mRouterSetLabelFragment != null) {
                fragmentTransaction.hide(mRouterSetLabelFragment);
            }
            if (mSetWifiFragment != null) {
                fragmentTransaction.hide(mSetWifiFragment);
            }
            if (mCreateNetworkFragment != null) {
                fragmentTransaction.hide(mCreateNetworkFragment);
            }
            if (mRouterSetupSuccessFragment != null) {
                fragmentTransaction.hide(mRouterSetupSuccessFragment);
            }
            fragmentTransaction.commit();
            setDeviceLoadingView();
        } else {
            setDeviceLoadingView();
        }

        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            // 是否需要監聽藍牙開關狀態
            mIsNeedListenBleState = true;
        } else {
            // WIFI setup 流程不需要監聽藍牙開關狀態
            mIsNeedListenBleState = false;

            // 若是走 WIFI setup, 會先確定 WIFI 已經連線上 device setup SSID 才會進入這頁, 故進入這頁時就可以直接開始 setup 了
            mDeviceSetupViewModel.startDeviceSetup();
        }

        // 進入這頁時 App 才要開始設定 router, 並且會需要偵測 WAN type
        mIsNeedDetectWanType = true;
        // 要等到 App 與 device 的 setup 流程完成, device 開始向 cloud 進行 provision 程序時, 此 flag 才會被設定起來
        mIsNeedGetProvisionState = false;

        mJsonParser = new JsonParserWrapper();
        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            if (mIsNeedListenBleState) {
                IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                registerReceiver(mBleStateListener, filter);
            }

            // 註冊 BLE action receiver 以透過 broadcast receiver 收到 connection status
            mDeviceSetupViewModel.registerBleActionReceiver();

            if (mIsNeedDetectWanType) {
                // onResume() 時符合該情境有兩種:
                // (1) 一開始進入 BLE setup 流程, 還沒偵測過 WAN type, 要偵測 WAN type 需先跟 device 連上線
                // (2) 已經有跟 device 連上線, 但在得到 WAN type 結果前畫面被 pause 了
                //      由於 onPause() 時會斷開與 device 的 BLE 連線, 所以再回來 App 時需要再次跟 device 連線
                LogUtils.trace("mIsNeedDetectWanType = true, connect device");
                mDeviceSetupViewModel.connectDevice();
            }
        }

        if (mIsNeedGetProvisionState) {
            LogUtils.trace("Activity is resumed and need to get provision state.");
            getProvisionState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            // 若是還沒偵測到 WAN type, 畫面 pause 時需要斷開裝置連線, 回來時再重新連線 -> 設定 router -> 偵測 WAN type
            if (mIsNeedDetectWanType) {
                LogUtils.trace("mIsNeedDetectWanType = true, disconnect device");
                mDeviceSetupViewModel.disconnectDevice();
            }

            mDeviceSetupViewModel.unregisterBleActionReceiver();
            try {
                // BLE state listener 有可能已經先被 unregister 了, 再次 unregister 就會產生 exception
                unregisterReceiver(mBleStateListener);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        if (mIsNeedGetProvisionState) {
            mHandler.removeCallbacks(mGetProvisionStateRunnable);
        }
    }

    @Override
    protected void onDestroy() {
        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            mDeviceSetupViewModel.disconnectDevice();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        /* 有兩個地方會呼叫該 function: (1) 標題列的 back 按鈕, (2) Android 系統的 back 鍵.
           此流程的第一頁不會加入 back stack, 第二頁開始才會加入, 例如: 第二頁的 back stack entry count = 1.
           如果不是在流程的第一頁的話, 標題列會有 back 按鈕, 按 Android back 鍵也可以回到上一頁.
           但如果是在第一頁, 標題列的 back 按鈕要隱藏, 按 Android back 鍵也不能結束頁面, 意即不能跳過此流程. */

        // detect WAN type 時若沒有偵測到 cable 會出現提示使用者檢查 cable 的畫面
        // 此時按下 back 鍵要能讓使用者回到一開始的說明頁面, 重新掃描 device
        // 應用場景: 掃描到兩台 device, 一台有接 cable, 一台沒有, 使用者先選了沒有 cable 的那台, 要能讓他回去選另外一台
        // Bug #8665 (https://umedia.plan.io/issues/8665)
        if (mFragmentManager.getBackStackEntryCount() == 1) {
            String fragmentTag = mFragmentManager.getBackStackEntryAt(0).getName();
            Fragment currentFragment = mFragmentManager.findFragmentByTag(fragmentTag);
            if (currentFragment instanceof CheckCableFragment) {
                //
                LogUtils.trace("press back in check cable page, finish page");
                finish();
                return;
            }
        }

        // back stack entry count > 0 代表不在流程的第一頁, 可以返回上一頁
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            super.onBackPressed();

            if (mFragmentManager.getBackStackEntryCount() == 0) {
                // 回到上一頁之後再判斷一次 back stack entry count, 若等於 0 代表已回到了第一頁, 此時要隱藏 UI 上的 back 鍵.
                mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
            }
        }
    }

    private void setDeviceLoadingView() {
        setDeviceImage(mSetupDevice, mBinding.imgDevice);
    }

    private void setView() {
        setTitleBar();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.initial_setup_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        // 流程的第一頁不能返回, 故先隱藏按鈕
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mDeviceSetupViewModel.setSetupFlow(DataManager.getInstance().getSetupFlow());
        mDeviceSetupViewModel.setSetupMode(DataManager.getInstance().getSetupMode());
        mDeviceSetupViewModel.setSetupDevice(mSetupDevice);
        mDeviceSetupViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.success) {
                        // 動作 success 後, 根據現在的 action code 判斷下一步動作
                        // 大部份動作 success 的情況在 view model/model 內已串接完成, 一步步往下做 (期間 UI 不需要更新)
                        // 這裡判斷的情況是需要 UI 更新畫面或由 UI 決定流程
                        if (resultStatus.actionCode == Code.Action.DeviceSetup.CONNECT_DEVICE) {
                            // device 連線成功後即可開始下 command, 進入 Firmware 的 device setup/provision 流程
                            mDeviceSetupViewModel.startDeviceSetup();
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_WAN_TYPE) {
                            String type = (resultStatus.data == null) ? "" : (String) resultStatus.data;
                            if (type.equals("dhcp")) {
                                onContinue(Code.Action.DeviceSetup.SET_DHCP);
                            } else if (type.equals("pppoe")) {
                                onContinue(Code.Action.DeviceSetup.SELECT_PPPOE);
                            } else if (type.equals("unknown")) {
                                onContinue(Code.Action.DeviceSetup.SELECT_STATIC_IP);
                            }

                            // 有偵測到 WAN type 了, 清除 flag
                            mIsNeedDetectWanType = false;
                            LogUtils.trace("mIsNeedDetectWanType = false");
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_DEVICE_TOKEN_STATUS) {
                            onContinue(Code.Action.DeviceSetup.SETUP_LOCATION);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SETUP_COMPLETE) {
                            // 對 device 設定完 setup complete 指令後, 開始去跟 Cloud 詢問 device provision 的狀態
                            // (先前的步驟已將 device 設定好可以連上 Internet 了, device 會開始跟 Cloud 進行 provision 程序)
                            onContinue(Code.Action.DeviceSetup.GET_PROVISION_STATE);
                        } else if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_PROVISION_STATE) {
                            String state = mJsonParser.jsonGetString((JsonObject) resultStatus.data, "state");
                            // Cloud 回覆的 device provision state 共有四種:
                            // (1) PROVISION_BEGIN : 開始佈署
                            // (2) REGISTER_IOT_HUB_DEVICE : 註冊 IoT Hub 裝置中
                            // (3) MQTT_CONNECTED : MQTT 已連線
                            // (4) PROVISION_SUCCESS : 佈署完成
                            if (state.equals("PROVISION_SUCCESS")) {
                                String did = mJsonParser.jsonGetString((JsonObject) resultStatus.data, "device_id");
                                mDeviceSetupViewModel.deviceConnectionTest(did);

                                // 收到 provision 成功後, 會顯示 setup 成功的畫面, 之後回到 dashboard 會去打 cloud API 拿資料
                                // 等待幾秒才顯示成功的畫面, 是要等 cloud 的資料穩定了, 才讓使用者可以回到 dashboard
                                // 因為若馬上打 cloud API 拿取資料, 有時會發生 dashboard 拿到的資料是空的, 或是 get node info 收到 Http status 500
                                // 但過一下子再打 API 又沒問題了, 推測可能有時 cloud 的資料尚未處理完成才會發生錯誤, 故等待幾秒再打 cloud API 拿資料
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // 最後一個動作, 不論成功或失敗, 都將 progress 改成 100%, 表示結束
                                        mCreateNetworkFragment.updateProgress(100);
                                        onContinue(Code.Action.DeviceSetup.PROVISION_SUCCESS);
                                    }
                                }, 10000);
                            } else {
                                getProvisionStateAgain();
                            }
                            updateProvisionProgress();
                        }
                    } else {
                        // command fail 時, view model 將 FW 回傳的 error code (status) 放在 resultStatus.data
                        // 有多個動作 fail 的原因都有 "cable unplugged" 這項, 先統一判斷
                        String status = (resultStatus.data == null) ? "" : (String) resultStatus.data;
                        if (isCableUnpluggedError(status)) {
                            return;
                        }

                        // 若不是 "cable unplugged", 再以 action code 來判斷及顯示訊息
                        String message = resultStatus.errorMsg;
                        if (resultStatus.actionCode == Code.Action.DeviceSetup.CONNECT_DEVICE) {
                            showErrorMessageAndRetry("Bluetooth", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_USER_ID) {
                            showErrorMessageAndRetry("Set User ID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_TIMEZONE) {
                            showErrorMessageAndRetry("Set Timezone", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_PROVISION_TOKEN) {
                            showErrorMessageAndRetry("Set Provision Token", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_DEVICE_BOARD_ID) {
                            showErrorMessageAndRetry("Get Device Board ID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_DEVICE_OPERATION_MODE) {
                            showErrorMessageAndRetry("Set Device Operation Mode", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.DETECT_WAN_TYPE) {
                            showErrorMessageAndRetry("Detect WAN Type", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_WAN_TYPE) {
                            showErrorMessageAndRetry("Get WAN Type", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_WAN_DHCP) {
                            String title = "Set WAN DHCP";
                            if (isNeedReselectConnectionType(status, title, message)) {
                                // do nothing, action is done in the if statement
                            } else {
                                showErrorMessageAndRetry(title, message);
                            }
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_WAN_PPPOE) {
                            String title = "Set PPPoE";
                            if (isNeedReselectConnectionType(status, title, message)) {
                                // do nothing, action is done in the if statement
                            } else if (status.equals(Code.Error.DeviceSetup.STATUS_651)
                                    || status.equals(Code.Error.DeviceSetup.STATUS_751)) {
                                // PPPoE username or password 輸入錯誤, 再導回到 PPPoE 輸入畫面, 讓使用者可以修改
                                final MessageDialog messageDialog = new MessageDialog();
                                messageDialog.setTitle(title);
                                messageDialog.setContent(message);
                                messageDialog.setOkButton(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        messageDialog.dismiss();
                                        onContinue(Code.Action.DeviceSetup.SELECT_PPPOE);
                                    }
                                });
                                showDialog(messageDialog);
                            } else {
                                showErrorMessageAndRetry(title, message);
                            }
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_WAN_STATIC_IP) {
                            showErrorMessageAndRetry("Set Static Ip", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_INTERNET_CONN_STATUS) {
                            String title = "Get Internet Connection Status";
                            if (isNeedReselectConnectionType(status, title, message)) {
                                // do nothing, action is done in the if statement
                            } else {
                                showErrorMessageAndRetry(title, message);
                            }
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_LOCATION) {
                            showErrorMessageAndRetry("Set Location", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_2G_WIFI) {
                            showErrorMessageAndRetry("Set 2.4G Wi-Fi SSID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_2G_WIFI_KEY) {
                            showErrorMessageAndRetry("Set 2.4G Wi-Fi Password", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_WIFI) {
                            showErrorMessageAndRetry("Set 5G Wi-Fi SSID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_WIFI_KEY) {
                            showErrorMessageAndRetry("Set 5G Wi-Fi Password", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_BLE_ENABLE) {
                            showErrorMessageAndRetry("Set BLE Disable", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SETUP_COMPLETE) {
                            showErrorMessageAndRetry("Set Setup Complete", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_PROVISION_STATE) {
                            if (mDeviceSetupViewModel.getSetupFlow() == AppConstants.SetupFlow.WIFI && mGetProvisionStateCount <= 3) {
                                // WIFI provision 時手機會連線到 device 預設的 SSID, 開始 provision 後因為預設的 SSID 失效了所以手機 WIFI 會斷線
                                // 而打 cloud API get provision state 時就會出現網路不通的提示訊息, 但實際上過一下子手機網路可能就通了
                                // (例如: 有開行動數據的話, WIFI 斷線後就會自動切換去使用行動網路, 但需要幾秒鐘的時間)
                                // => 原本第一次就會提示訊息, 改為三次失敗後才提示
                                getProvisionStateAgain();
                            } else {
                                if (message.equals(getString(R.string.err_internet_not_available))
                                        || message.equals(getString(R.string.err_internet_connection_fail))
                                        || message.equals(getString(R.string.err_internet_timeout))
                                        || message.equals(getString(R.string.err_socket_exception))) {
                                    final MessageDialog messageDialog = new MessageDialog();
                                    messageDialog.setTitle("Get Provision State");
                                    messageDialog.setContent("Please check your phone's Internet connection in order to get device setup result.");
                                    messageDialog.setOkButton(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            messageDialog.dismiss();
                                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    });
                                    showDialog(messageDialog);
                                } else {
                                    getProvisionStateAgain();
                                }
                            }
                            updateProvisionProgress();
                        } else {
                            showErrorMessageAndRetry("Device Setup", "Action code: " + resultStatus.actionCode + "\n Message: " + message);
                        }
                    }
                }
            }
        });
    }

    // 判斷是不是 cable unplugged error, 是的話導到 cable unplugged UI
    private boolean isCableUnpluggedError(String status) {
        if (status.equals(Code.Error.DeviceSetup.STATUS_615)
                || status.equals(Code.Error.DeviceSetup.STATUS_715)) {
            mIsNeedDetectWanType = true;
            LogUtils.trace("mIsNeedDetectWanType = true");
            onContinue(Code.Action.DeviceSetup.CHECK_CABLE);
            return true;
        }
        return false;
    }

    // 判斷是不是特定某些 error, 是的話顯示 error message 並導回 select connection type UI
    private boolean isNeedReselectConnectionType(String status, String title, String message) {
        if (status.equals(Code.Error.DeviceSetup.STATUS_641)
                || status.equals(Code.Error.DeviceSetup.STATUS_651)
                || status.equals(Code.Error.DeviceSetup.STATUS_652)
                || status.equals(Code.Error.DeviceSetup.STATUS_653)
                || status.equals(Code.Error.DeviceSetup.STATUS_654)
                || status.equals(Code.Error.DeviceSetup.STATUS_661)
                || status.equals(Code.Error.DeviceSetup.STATUS_741)
                || status.equals(Code.Error.DeviceSetup.STATUS_751)
                || status.equals(Code.Error.DeviceSetup.STATUS_752)
                || status.equals(Code.Error.DeviceSetup.STATUS_753)
                || status.equals(Code.Error.DeviceSetup.STATUS_754)
                || status.equals(Code.Error.DeviceSetup.STATUS_761)) {
            final MessageDialog messageDialog = new MessageDialog();
            messageDialog.setTitle(title);
            messageDialog.setContent(message);
            messageDialog.setOkButton(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messageDialog.dismiss();
                    onContinue(Code.Action.DeviceSetup.CHANGE_CONNECTION_TYPE);
                }
            });
            showDialog(messageDialog);
            return true;
        }
        return false;
    }

    @Override
    public void onContinue(int nextStep) {
        LogUtils.trace("next step = " + nextStep);
        switch (nextStep) {
            case Code.Action.DeviceSetup.MORE_HELP:
                startMoreHelpActivity();
                break;

            case Code.Action.DeviceSetup.CHECK_CABLE:
                mCheckCableFragment = new CheckCableFragment();
                addFragment(R.id.clContainer, mCheckCableFragment, CheckCableFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case Code.Action.DeviceSetup.CHECK_CABLE_RETRY:
                mCheckCableRetryFragment = new CheckCableRetryFragment();
                addFragment(R.id.clContainer, mCheckCableRetryFragment, CheckCableRetryFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case Code.Action.DeviceSetup.RETRY:
                backToFirstPage();
                mDeviceSetupViewModel.detectWanType();
                break;

            case Code.Action.DeviceSetup.CHANGE_CONNECTION_TYPE:
                if (mSelectConnectionTypeFragment != null && mSelectConnectionTypeFragment.isAdded()) {
                    // 已經有存在 select connection type 頁面, 它是第一頁, 所以返回到第一頁
                    backToFirstPage();
                } else {
                    addSelectConnectionTypeView();
                }
                break;

            case Code.Action.DeviceSetup.SELECT_PPPOE:
                if (mSelectConnectionTypeFragment == null) {
                    addSelectConnectionTypeView();
                }
                mSetPppoeFragment = new SetPppoeFragment();
                addFragment(R.id.clContainer, mSetPppoeFragment, SetPppoeFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case Code.Action.DeviceSetup.SELECT_STATIC_IP:
                if (mSelectConnectionTypeFragment == null) {
                    addSelectConnectionTypeView();
                }
                mSetStaticIpFragment = new SetStaticIpFragment();
                addFragment(R.id.clContainer, mSetStaticIpFragment, SetStaticIpFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case Code.Action.DeviceSetup.SET_DHCP:
                // 有兩種方式進入該情境:
                // (1) get WAN type 結果是 DHCP, view model 通知 UI, UI 再次呼叫 view model 執行 setWanDhcp()
                //      (其實 view model 也可以在偵測到 WAN type 是 DHCP 後就自己執行 setWanDhcp(), 不需要繞到 UI 再回去,
                //      但因 UI 需要清除 mIsNeedDetectWanType flag, 所以要通知 UI)
                // (2) 在 select connection type 頁面選擇 "Auto Config", 也是要呼叫 view model 執行 setWanDhcp()
                // 以 (1) 的方式進來時, UI 畫面本來就在 loading 畫面了, 不需要更新
                // 以 (2) 的方式進來時, UI 畫面在 select connection type 頁面, 需要更新 UI 回到 loading 畫面
                removeSelectConnectionTypeView();
                mDeviceSetupViewModel.setWanDhcp();
                break;

            case Code.Action.DeviceSetup.SET_PPPOE:
                removeSelectConnectionTypeView();
                mDeviceSetupViewModel.setPppoe();
                break;

            case Code.Action.DeviceSetup.SET_STATIC_IP:
                removeSelectConnectionTypeView();
                mDeviceSetupViewModel.setStaticIp();
                break;

            case Code.Action.DeviceSetup.SETUP_LOCATION:
                mRouterSetLabelFragment = new DeviceSetLabelFragment();
                // set label 頁面不允許返回, 直接讓它覆蓋 loading 畫面, 成為新的第一頁, 不用加入 back stack
                addFragmentAsFirstPage(R.id.clContainer, mRouterSetLabelFragment, DeviceSetLabelFragment.class.getName());
                break;

            case Code.Action.DeviceSetup.SETUP_WIFI:
                mSetWifiFragment = new SetWifiFragment();
                addFragment(R.id.clContainer, mSetWifiFragment, SetWifiFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case Code.Action.DeviceSetup.CREATE_NETWORK:
                // 繼續設定 device, 需顯示 create network 的 loading 畫面

                // popup 所有頁面, 回到第一頁 (因 loading 畫面需成為新的第一頁, 且頁面不允許返回)
                backToFirstPage();

                mCreateNetworkFragment = new CreateNetworkFragment();
                // 移除前面的頁面並新增 create network 的 loading 畫面
                mFragmentManager.beginTransaction()
                        .remove(mRouterSetLabelFragment)
                        .remove(mSetWifiFragment)
                        .add(R.id.clContainer, mCreateNetworkFragment, CreateNetworkFragment.class.getName())
                        .commit();

                mDeviceSetupViewModel.setLocation();
                break;

            case Code.Action.DeviceSetup.GET_PROVISION_STATE:
                if (mSetupFlow == AppConstants.SetupFlow.BLE) {
                    // App 所有要透過藍牙傳送給 device 的指令都已完成, 即使藍牙在這時候被關閉了也不影響結果, 故不用再監聽藍牙開關狀態了
                    mIsNeedListenBleState = false;
                    unregisterReceiver(mBleStateListener);
                }
                mIsNeedGetProvisionState = true;
                mGetProvisionStateCount = 0;
                getProvisionState();
                // 使用者已完成所有設定, App 開始等待 provision 結果, 顯示進度
                mCreateNetworkFragment.updateProgress(0);
                break;

            case Code.Action.DeviceSetup.PROVISION_SUCCESS:
                mRouterSetupSuccessFragment = new RouterSetupSuccessFragment();
                // 新增 router setup success 頁面, 直接覆蓋原本的 create network 的 loading 畫面
                addFragmentAsFirstPage(R.id.clContainer, mRouterSetupSuccessFragment, RouterSetupSuccessFragment.class.getName());

                // router provision 成功, 回到 dashboard 更新資料時需要顯示 loading 畫面
                // 因為 router provision 成功後可能先會經過其他頁面 (ex: 接著做 extender provision) 才回到 dashboard
                // 所以用 flag 紀錄, 回到 dashboard 時判斷此 flag 來決定是否顯示 loading 畫面
                DataManager.getInstance().setAfterInitialSetup(true);
                break;

            case Code.Action.DeviceSetup.ADD_ANOTHER_DEVICE:
                startActivity(new Intent(mContext, RepeaterSetupActivity.class));
                finish();
                break;

            case Code.Action.DeviceSetup.FINISH_SETUP:
                startDashboardActivity(RouterProvisionActivity.this);
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void setSupportView(TextView textView) {
        setSupportClickMessage(textView);
    }

    private void backToFirstPage() {
        // popup 所有頁面, 回到第一頁 (讓流程需要的畫面成為新的第一頁, 且頁面不允許返回)
        // 第一頁可能是 loading 畫面或是 select connection type, 依據流程進行到哪個步驟而有所不同
        int count = mFragmentManager.getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            mFragmentManager.popBackStackImmediate();
        }
        // 頁面不允許返回, 隱藏畫面上的返回鍵
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
    }

    private void addSelectConnectionTypeView() {
        // 偵測 WAN type 根據結果可能需要自動導到 PPPoE/Static IP 頁面, 而這兩頁都可以 back 回到 select connection type 頁,
        // 所以在添加 PPPoE/Static IP 頁面前需先添加 select connection type 頁
        mSelectConnectionTypeFragment = new SelectConnectionTypeFragment();
        // select connection type 頁面沒有 back 功能, 不用加入 back stack, 讓 select connection type 覆蓋原本的 loading 畫面, 成為新的第一頁
        addFragmentAsFirstPage(R.id.clContainer, mSelectConnectionTypeFragment, SelectConnectionTypeFragment.class.getName());
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
    }

    private void removeSelectConnectionTypeView() {
        // 送 command 給 device 開始設定 DHCP/PPPoE/Static IP 後, 需要一些時間, 後續也還有別的步驟需設定, 故顯示 loading 畫面
        // loading 畫面在第一頁, 若現在是在 select connection type 頁面, 需移除並回到第一頁, 原本被覆蓋的 loading 畫面就會再顯示出來
        if (mSelectConnectionTypeFragment != null) {
            removeFragment(mSelectConnectionTypeFragment);
            mSelectConnectionTypeFragment = null;
            backToFirstPage();
        }
    }

    private Runnable mGetProvisionStateRunnable = new Runnable() {
        @Override
        public void run() {
            mGetProvisionStateCount++;
            LogUtils.trace("mGetProvisionStateCount = " + mGetProvisionStateCount);
            mDeviceSetupViewModel.getProvisionState();
        }
    };

    private void getProvisionState() {
        // device 向 Cloud 進行 provision 中, 需花費一些時間, 等待 10 秒後再詢問狀態
        mHandler.postDelayed(mGetProvisionStateRunnable, 10000);
    }

    private void getProvisionStateAgain() {
        if (mGetProvisionStateCount >= mGetProvisionStateMaxCount) {
            // 將 progress 改成 100%, 表示結束
            mCreateNetworkFragment.updateProgress(100);
            // 嘗試 get provision state 多次了都還沒拿到 provision 成功的狀態, 判定為 provision 失敗
            LogUtils.trace("mGetProvisionStateCount = " + mGetProvisionStateCount);
            String message = "Device provision failed. Please try again.";
            showErrorMessageAndRetry("Provision Timeout", message);
        } else {
            // 重新詢問一次 provision 狀態
            getProvisionState();
        }
    }

    private void updateProvisionProgress() {
        int progress = (int) ((100.0 / mGetProvisionStateMaxCount) * mGetProvisionStateCount);
        mCreateNetworkFragment.updateProgress(progress);
    }
}
