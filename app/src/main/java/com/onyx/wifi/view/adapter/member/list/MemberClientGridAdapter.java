package com.onyx.wifi.view.adapter.member.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.view.interfaces.member.OnMemberListEventListener;

import java.util.ArrayList;
import java.util.List;

public class MemberClientGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Client> mClientList = new ArrayList<Client>();

    private OnMemberListEventListener mEventListener;

    public void setClientList(List<Client> clientList) {
        mClientList = clientList;

        notifyDataSetChanged();
    }

    public void setMemberListEventListener(OnMemberListEventListener listener) {
        mEventListener = listener;
    }

    @NonNull
    @Override
    public ClientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_client_grid_item, parent, false);

        ClientViewHolder memberViewHolder = new ClientViewHolder(view);
        return memberViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ClientViewHolder clientViewHolder = (ClientViewHolder)viewHolder;

        Client client = mClientList.get(position);

        clientViewHolder.setClient(client);
        clientViewHolder.setOnMemberListEventListener(mEventListener);
    }

    @Override
    public int getItemCount() {
        return  mClientList.size();
    }

    class ClientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Client mClient;

        private ImageView mLightImageView;

        private ImageView mIconImageView;

        private TextView mNameTextView;

        private OnMemberListEventListener mOnMemberListEventListener;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);

            this.itemView.setOnClickListener(this);

            mLightImageView = this.itemView.findViewById(R.id.lightImageView);

            mIconImageView = this.itemView.findViewById(R.id.iconImageView);

            mNameTextView = this.itemView.findViewById(R.id.nameTextView);
        }

        public void setOnMemberListEventListener(OnMemberListEventListener listener) {
            mOnMemberListEventListener = listener;
        }

        public void setClient(Client client) {
            mClient = client;

            setupLight();

            setupIcon();

            setupName();
        }

        private void setupLight() {
            ConnectionStatus status = mClient.getConnectionStatus();

            switch (status) {
                case OFFLINE:
                    mLightImageView.setImageResource(R.drawable.ic_internet_status_offline_gray);
                    break;
                case WEAK:
                    mLightImageView.setImageResource(R.drawable.ic_internet_status_weak);
                    break;
                case ONLINE:
                    mLightImageView.setImageResource(R.drawable.ic_internet_status_online);
                    break;
            }
        }

        private void setupIcon() {
            int iconId = mClient.getClientIcon();

            mIconImageView.setImageResource(iconId);
        }

        private void setupName() {
            String name = mClient.getName();
            mNameTextView.setText(name);
        }

        @Override
        public void onClick(View v) {
            ClientModel clientModel = mClient.getClientModel();

            if (mOnMemberListEventListener != null) {
                mOnMemberListEventListener.onClientClick(clientModel);
            }
        }

    }
}
