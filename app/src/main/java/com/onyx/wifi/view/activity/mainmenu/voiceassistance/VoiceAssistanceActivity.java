package com.onyx.wifi.view.activity.mainmenu.voiceassistance;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityVoiceAssistanceBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.ConnectAlexaAdapter;
import com.onyx.wifi.view.adapter.DisconnectAlexaAdapter;
import com.onyx.wifi.view.adapter.VoiceDeviceAdapter;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.AudioDevice;
import com.onyx.wifi.viewmodel.mainmenu.VoiceAssistantViewModel;

import java.util.Timer;
import java.util.TimerTask;

// Bonnie 1

public class VoiceAssistanceActivity extends BaseMenuActivity implements VoiceDeviceAdapter.OnItemClickHandler, DisconnectAlexaAdapter.OnItemClickHandler, ConnectAlexaAdapter.OnItemClickHandler {

    private ActivityVoiceAssistanceBinding mBinding;
    //private ArrayList<AudioDevice> mAudioDevice = new ArrayList<>();
    private VoiceDeviceAdapter mAdapter;
    private ConnectAlexaAdapter mConnectAlexaAdapter;
    private DisconnectAlexaAdapter mDisconnectAlexaAdapter;
    private Boolean mIsConnectAlexaList = false;
    private Boolean mIsDisconnectAlexaList = false;
    private static float mDisconnectPosX = 0.0f;
    private static float mDisconnectPosY = 0.0f;
    private VoiceAssistantViewModel mViewModel;
    private Timer mTimer;

    //public UdpReceiveProtocol mUdpReceiveProtocol; // 不可以宣告成static!!


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_voice_assistance);


        setViewModel();
        initVoiceDeviceList();
        setView();

        //移到打cloud api successfully
//        if (CommonUtils.getSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA).equals("true")){
//            LogUtils.trace("Bonnie_reset", "[unregister]" + index);
//            mViewModel.setUnRegister("0", mViewModel.getmAudioDeviceList().get(index).getDid());
//            CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "false");
//        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //
        // Get the coordinates of the original Disconnect buttom here (once)
        //
        if (hasFocus) {
            LogUtils.trace("Bonnie_xy",
                    "X:" + mBinding.btnDisconnect.getX() + " " + mBinding.btnDisconnect.getY() + " " + mBinding.btnConnect.getX() + " " + mBinding.btnConnect.getY());

            LogUtils.trace("Bonnie_xy",
                    "X(m):" + mDisconnectPosX + " " + mDisconnectPosY + " " + mBinding.btnConnect.getX() + " " + mBinding.btnConnect.getY());
            if (mDisconnectPosX == 0.0f && mDisconnectPosY == 0.0f) {
                //if (mBinding.btnDisconnect.getY() != mBinding.btnConnect.g)
                if (mBinding.btnDisconnect.getY() < mBinding.btnConnect.getY()) {
                    mDisconnectPosX = mBinding.btnDisconnect.getX();
                    mDisconnectPosY = mBinding.btnDisconnect.getY();
                }
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        initVoiceDeviceList();
        //swapBackToVoiceDeviceRecycler();
        LogUtils.trace("Bonnie_xy", "[onResume()]X:" + mBinding.btnDisconnect.getX() + " " + mBinding.btnDisconnect.getY());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(VoiceAssistantViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.VoiceAssistant.GET_AUDIO_DEVICE_INFO_LIST) {
                        if (resultStatus.success) {
                            // 處理linking timeout dialog, user 按下OK後，紀錄flog,回到主頁做deregister的動作=>deregister的動作移到dialog出現之前
//                            int index = Integer.valueOf(CommonUtils.getSharedPrefData(AppConstants.KEY_SELECTED_INDEX)); //有取得list才執行unregister的動作
//
//                            if (CommonUtils.getSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA).equals("true")) {
//                                LogUtils.trace("Bonnie_reset", "[unregister]" + " " + index + " " + mViewModel.getmAudioDeviceList().get(index).getIP());
//
//                                mViewModel.setUnRegister("0", mViewModel.getmAudioDeviceList().get(index).getDid());
//                                CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "false");
//                            }
                            updateVoiceDevicesList();
                        } else {
                            showMessageDialog("Get Audio Device", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.VoiceAssistant.SET_UNREGISTER) {
                        if (resultStatus.success) {
                            //mViewModel.getAudioDeviceInfoList();
                            updateVoiceDevicesList();
                        } else {
                            showMessageDialog("Unregister", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.VoiceAssistant.SET_MUTE_UNMUTE) {
                        if (resultStatus.success) {
                            //mViewModel.getAudioDeviceInfoList();
                            LogUtils.trace("Bonnie_m", "[SET_MUTE_UNMUTE][Success]");
                            //updateMuteUnMute();

                            updateVoiceDevicesList();
                            //initVoiceDeviceList();
                        } else {
                            showMessageDialog("Mute/Unmute", resultStatus.errorMsg);
                        }
                        startTimer();
                    }
                }
            }
        });
    }

    private void setUpVoiceDeviceRecycler() {
        LogUtils.trace("Bonnie_xy", "[setUpVoiceDeviceRecycler]");
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding.rvVoiceDevice.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding.rvVoiceDevice.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        if (mAdapter == null) {
            mAdapter = new VoiceDeviceAdapter(mContext, mViewModel.getmAudioDeviceList(), this);
        }
        mBinding.rvVoiceDevice.setAdapter(mAdapter);


        LogUtils.trace("status", "[setUpVoiceDeviceRecycler] mAdapter" + System.identityHashCode(mAdapter));
    }

    private void swapBackToVoiceDeviceRecycler() {
        LogUtils.trace("Bonnie_xy",
                "[Back]X:" + mBinding.btnDisconnect.getX() + " " + mBinding.btnDisconnect.getY() + " " + mBinding.btnConnect.getX() + " " + mBinding.btnConnect.getY());

        mBinding.btnDisconnect.setX(mDisconnectPosX);
        mBinding.btnDisconnect.setY(mDisconnectPosY);

        mBinding.btnDisconnect.setVisibility(View.VISIBLE);
        mBinding.btnConnect.setVisibility(View.VISIBLE);

        DisplayButtonInStartPage();

        LogUtils.trace("Bonnie_xy", "[Back] x(m): " + mDisconnectPosX + " y:" + mDisconnectPosY);

        mBinding.tvSelectDevice.setVisibility(View.INVISIBLE);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mIsConnectAlexaList = false;
        mIsDisconnectAlexaList = false;
        if (mAdapter == null) {
            mAdapter = new VoiceDeviceAdapter(mContext, mViewModel.getmAudioDeviceList(), this);
        }
        mBinding.rvVoiceDevice.setAdapter(mAdapter);

        //updateVoiceDevicesList();
        LogUtils.trace("status", "[swapBackToVoiceDeviceRecycler] mAdapter" + System.identityHashCode(mAdapter));

    }

    private void swapToConnectAlexaRecycler() {
        mBinding.btnDisconnect.setVisibility(View.INVISIBLE);
        grayOutButton(mBinding.btnConnect);
        mBinding.tvSelectDevice.setText("Select a device to register");
        mBinding.tvSelectDevice.setVisibility(View.VISIBLE);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mIsConnectAlexaList = true;
        mIsDisconnectAlexaList = false;
        if (mConnectAlexaAdapter == null) {
            mConnectAlexaAdapter = new ConnectAlexaAdapter(mContext, mViewModel.getmAudioDeviceList(), this);
        }
        //updateVoiceDevicesList();
        mBinding.rvVoiceDevice.setAdapter(mConnectAlexaAdapter);

        LogUtils.trace("status", "[swapToConnectAlexaRecycler] mConnectAlexaAdapter" + System.identityHashCode(mConnectAlexaAdapter));

    }

    private void swapToDisconnectAlexaRecycler() {
        float posX = mBinding.btnConnect.getX();
        float posY = mBinding.btnConnect.getY();
        mBinding.btnDisconnect.setX(posX);
        mBinding.btnDisconnect.setY(posY);
        mBinding.btnDisconnect.setVisibility(View.VISIBLE);
        grayOutButton(mBinding.btnDisconnect);
        mBinding.btnConnect.setVisibility(View.INVISIBLE);
        mBinding.tvSelectDevice.setText("Select a device to deregister");
        mBinding.tvSelectDevice.setVisibility(View.VISIBLE);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mIsDisconnectAlexaList = true;
        mIsConnectAlexaList = false;
        if (mDisconnectAlexaAdapter == null) {
            mDisconnectAlexaAdapter = new DisconnectAlexaAdapter(mContext, mViewModel.getmAudioDeviceList(), this);
        }
        //updateVoiceDevicesList();
        mBinding.rvVoiceDevice.setAdapter(mDisconnectAlexaAdapter); // 不可用swapAdapter, 因為共用ViewHolder
        LogUtils.trace("status", "[swapToDisconnectAlexaRecycler] mConnectAlexaAdapter" + System.identityHashCode(mDisconnectAlexaAdapter));

    }
    /////////////////////////////////////

    void startTimer() {
        if (mTimer == null) {
            GetListTimerTask getListTimerTask = new GetListTimerTask();
            mTimer = new Timer();
            mTimer.schedule(getListTimerTask, 10L, 5000L);
        }
    }

    void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    /////////////////////////////////////
    private void initVoiceDeviceList() {

//        if (mTimer == null) {
//            GetListTimerTask getListTimerTask = new GetListTimerTask();
//            mTimer = new Timer();
//            mTimer.schedule(getListTimerTask, 10L, 5000L);
//        }
        startTimer();
        DisplayButtonInStartPage();
    }

    private void updateMuteUnMute() {
        if (mIsDisconnectAlexaList) {
            AudioDevice audioDevice = mViewModel.getmAudioDeviceList().get(mDisconnectAlexaAdapter.getMuteUnMutePos());
            LogUtils.trace("Bonnie_m", "[mIsDisconnectAlexaList]\n" + mDisconnectAlexaAdapter.getMuteUnMutePos() + " " + mViewModel.getmAudioDeviceList().get(mDisconnectAlexaAdapter.getMuteUnMutePos()).getIsMute());

            if (audioDevice.getIsMute())
                audioDevice.setIsMute(false);
            else
                audioDevice.setIsMute(true);
            LogUtils.trace("Bonnie_m", "[mIsDisconnectAlexaList]\n" + mDisconnectAlexaAdapter.getMuteUnMutePos() + " " + mViewModel.getmAudioDeviceList().get(mDisconnectAlexaAdapter.getMuteUnMutePos()).getIsMute());

            mViewModel.getmAudioDeviceList().set(mDisconnectAlexaAdapter.getMuteUnMutePos(), audioDevice);
            mDisconnectAlexaAdapter.setDevice(mViewModel.getmAudioDeviceList());
            mBinding.rvVoiceDevice.setAdapter(mDisconnectAlexaAdapter);
        } else if (mIsConnectAlexaList) {

            AudioDevice audioDevice = mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getMuteUnMutePos());
            LogUtils.trace("Bonnie_m", "[mIsConnectAlexaList]\n" + mConnectAlexaAdapter.getMuteUnMutePos() + " " + mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getMuteUnMutePos()).getIsMute());
            if (audioDevice.getIsMute())
                audioDevice.setIsMute(false);
            else
                audioDevice.setIsMute(true);
            LogUtils.trace("Bonnie_m", "[mIsConnectAlexaList]\n" + mConnectAlexaAdapter.getMuteUnMutePos() + " " + mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getMuteUnMutePos()).getIsMute());

            mViewModel.getmAudioDeviceList().set(mConnectAlexaAdapter.getMuteUnMutePos(), audioDevice);
            mConnectAlexaAdapter.setDevice(mViewModel.getmAudioDeviceList());
            mBinding.rvVoiceDevice.setAdapter(mConnectAlexaAdapter);
        } else {
            AudioDevice audioDevice = mViewModel.getmAudioDeviceList().get(mAdapter.getMuteUnMutePos());
            LogUtils.trace("Bonnie_m", "[mainAlexaList]\n" + mAdapter.getMuteUnMutePos() + " " + mViewModel.getmAudioDeviceList().get(mAdapter.getMuteUnMutePos()).getIsMute());

            if (audioDevice.getIsMute())
                audioDevice.setIsMute(false);
            else
                audioDevice.setIsMute(true);
            LogUtils.trace("Bonnie_m", "[mainAlexaList]\n" + mAdapter.getMuteUnMutePos() + " " + mViewModel.getmAudioDeviceList().get(mAdapter.getMuteUnMutePos()).getIsMute());

            mViewModel.getmAudioDeviceList().set(mAdapter.getMuteUnMutePos(), audioDevice);
            mAdapter.setDevice(mViewModel.getmAudioDeviceList());
            mBinding.rvVoiceDevice.setAdapter(mAdapter);
        }
    }


    private void updateVoiceDevicesList() {
        if (mViewModel.isListTheSame())
            return;

        LogUtils.trace("UpdateUI", "ReDraw List");
        if (!mIsConnectAlexaList && !mIsDisconnectAlexaList) {
            DisplayButtonInStartPage();
        }

        if (mIsDisconnectAlexaList) {
            LogUtils.trace("status", "[mIsDisconnectAlexaList]");
            mDisconnectAlexaAdapter.setDevice(mViewModel.getmAudioDeviceList());
            mBinding.rvVoiceDevice.setAdapter(mDisconnectAlexaAdapter);
        } else if (mIsConnectAlexaList) {
            LogUtils.trace("status", "[mIsConnectAlexaList]");
            mConnectAlexaAdapter.setDevice(mViewModel.getmAudioDeviceList());
            mBinding.rvVoiceDevice.setAdapter(mConnectAlexaAdapter);
        } else {
            LogUtils.trace("status", "[mainAlexaList]");
            mAdapter.setDevice(mViewModel.getmAudioDeviceList());
            mBinding.rvVoiceDevice.setAdapter(mAdapter);
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        //
        // Set up default voice device recycler view
        //
        setUpVoiceDeviceRecycler();

        mBinding.btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsDisconnectAlexaList) {
                    swapToDisconnectAlexaRecycler();
                } else {
                    final ConfirmDialog confirmDialog = new ConfirmDialog();
                    confirmDialog.setTitle("Disconnect Amazon Alexa");
                    confirmDialog.setContent("Do you wish to deregister Amazon Alexa voice assistant from the following device: " + mViewModel.getmAudioDeviceList().get(mDisconnectAlexaAdapter.getSelectedPos()).getLocation()); // needs to be modified
                    confirmDialog.setPositiveButton("Deregister", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mViewModel.getmAudioDeviceList().size() > mDisconnectAlexaAdapter.getSelectedPos()) {
                                mViewModel.setUnRegister("alexa_register", mViewModel.getmAudioDeviceList().get(mDisconnectAlexaAdapter.getSelectedPos()).getDid());
                                mDisconnectAlexaAdapter.resetSelectedRecord();
                            }
//                           initVoiceDeviceList();
                            confirmDialog.dismiss();
                            mViewModel.getAudioDeviceInfoList();
                            swapBackToVoiceDeviceRecycler();

                        }
                    });
                    confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            confirmDialog.dismiss();
                        }
                    });
                    showDialog(confirmDialog);
                }
            }
        });

        mBinding.btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsConnectAlexaList) {
                    swapToConnectAlexaRecycler();

                } else {

                    if (mTimer != null) {
                        mTimer.cancel();
                        mTimer = null;
                    }
                    CommonUtils.setSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP, mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getSelectedPos()).getIP());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_DEVICE_IP, mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getSelectedPos()).getDeviceIP());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_UDID, mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getSelectedPos()).getUdid());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_DID, mViewModel.getmAudioDeviceList().get(mConnectAlexaAdapter.getSelectedPos()).getDid());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_SELECTED_INDEX, String.valueOf(mConnectAlexaAdapter.getSelectedPos()));
                    LogUtils.trace("Bonnie_ip", "(" + CommonUtils.getSharedPrefData(AppConstants.KEY_SELECTED_INDEX) + ") \n" +
                            CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP) +
                            CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_UDID));

                    Intent intent = new Intent(mContext, NetworkConnectConfirmActivity.class);
                    startActivity(intent);
                }
            }
        });

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // initialize the status of checkbox
                if (mIsDisconnectAlexaList && (mDisconnectAlexaAdapter != null)) {
                    mDisconnectAlexaAdapter.setInit();
                } else if (mIsConnectAlexaList && (mConnectAlexaAdapter != null)) {
                    mConnectAlexaAdapter.setInit();
                }

                swapBackToVoiceDeviceRecycler();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.voice_assistance_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    // 3. 點擊事件
    @Override
    public void onItemClick(AudioDevice audioDevice) {
        //Toast.makeText(this,
        //        "click " + audioDevice.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMuteUnMuteClick(String muteUnMute, AudioDevice audioDevice) {
        cancelTimer();

        if (audioDevice.getIsMute()) {
            mViewModel.setMuteUnMute("set_audio", muteUnMute, audioDevice.getDid());
        } else {
            mViewModel.setMuteUnMute("set_audio", muteUnMute, audioDevice.getDid());
        }

        //mViewModel.getAudioDeviceInfoList();
    }

    private void showOutButton(Button button, boolean isStartPage) {
        if (button == mBinding.btnConnect) {
            button.setEnabled(true);
        } else {
            button.setEnabled(true);
            button.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
        }
    }

    private void grayOutButton(Button button) {
        if (button == mBinding.btnConnect) {
            button.setEnabled(false);
        } else {
            button.setEnabled(false);
            button.setTextColor(ContextCompat.getColor(mContext, R.color.gray_cccccc));
        }
    }

    private boolean IsAnyOnlineAndConnectToAlexa() {
        for (AudioDevice audioDevice : mViewModel.getmAudioDeviceList()) {
            if (audioDevice.getIsDeviceConnect() && audioDevice.getIsAlexaConnect()) {
                return true;
            }
        }
        return false;
    }

    private boolean IsAnyOnlineAndNotConnectToAlexa() {
        for (AudioDevice audioDevice : mViewModel.getmAudioDeviceList()) {
            if (audioDevice.getIsDeviceConnect() && (!audioDevice.getIsAlexaConnect())) {
                return true;
            }
        }
        return false;
    }

    private void DisplayButtonInStartPage() {
        if (IsAnyOnlineAndConnectToAlexa()) {
            showOutButton(mBinding.btnDisconnect, true);
        } else {
            grayOutButton(mBinding.btnDisconnect);
        }

        if (IsAnyOnlineAndNotConnectToAlexa()) {
            showOutButton(mBinding.btnConnect, true);
        } else {
            grayOutButton(mBinding.btnConnect);
        }

    }

    //
    // enable Connection button
    //
    @Override
    public void enableButton(int position) {
        if (mIsConnectAlexaList) {
            mBinding.btnConnect.setEnabled(true);

        } else {
            showOutButton(mBinding.btnDisconnect, false);
        }
    }

    //
    // disable Disconnection button
    //
    @Override
    public void diableButton() {
        if (mIsConnectAlexaList) {
            grayOutButton(mBinding.btnConnect);
        } else {
            grayOutButton(mBinding.btnDisconnect);
        }
    }

    private class GetListTimerTask extends TimerTask {

        @Override
        public void run() {

            //if (mIsConnectAlexaList || mIsDisconnectAlexaList)
            //  return;

            mViewModel.getAudioDeviceInfoList();
        }
    }

}
