package com.onyx.wifi.view.adapter.member.blocking;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.interfaces.member.OnBlacklistChangedListener;

import java.util.ArrayList;
import java.util.List;

public class BlockingSiteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListItem> mListItems = new ArrayList<>();

    private OnBlacklistChangedListener mOnBlacklistChangedListener;

    public BlockingSiteListAdapter(OnBlacklistChangedListener listener) {
        super();

        mOnBlacklistChangedListener = listener;
    }

    public void setListItems(List<ListItem> listItems) {
        mListItems = listItems;

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<BlockingListItemType, Object> listItem = mListItems.get(position);

        BlockingListItemType viewType = listItem.getType();

        switch (viewType) {
            case HEADER:
                return BlockingListItemType.HEADER.ordinal();
            case FOOTER:
                return BlockingListItemType.FOOTER.ordinal();
            case ITEM:
            default:
                return BlockingListItemType.ITEM.ordinal();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == BlockingListItemType.HEADER.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_blocking_site_list_header, parent, false);

            HeaderViewHolder viewHolder = new HeaderViewHolder(view);
            return viewHolder;
        }

        if (viewType == BlockingListItemType.FOOTER.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_blocking_site_list_footer, parent, false);

            FooterViewHolder viewHolder = new FooterViewHolder(view);
            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_blocking_site_list_item, parent, false);

        ItemViewHolder viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<BlockingListItemType, Object> listItem = mListItems.get(position);

        BlockingListItemType viewType = listItem.getType();

        if (viewType == BlockingListItemType.ITEM) {
            String site = (String) listItem.getItem();

            ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
            itemViewHolder.setSite(site);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIconImageView;

        private TextView mSiteTextView;

        private String mSite;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = itemView.findViewById(R.id.iconImageView);

            mIconImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnBlacklistChangedListener != null) {
                        mOnBlacklistChangedListener.onRemoveWebsiteBlocking(mSite);
                    }
                }
            });

            mSiteTextView = itemView.findViewById(R.id.textView);
        }

        public void setSite(String newSite) {
            mSite = newSite;

            mSiteTextView.setText(mSite);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIconImageView;

        public FooterViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = itemView.findViewById(R.id.iconImageView);

            mIconImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnBlacklistChangedListener != null) {
                        mOnBlacklistChangedListener.onAddWebsiteBlocking();
                    }
                }
            });
        }
    }

}
