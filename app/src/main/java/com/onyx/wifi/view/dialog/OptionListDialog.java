package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewOptionListDialogBinding;
import com.onyx.wifi.view.adapter.OptionListAdapter;

import java.util.ArrayList;

public class OptionListDialog extends BaseDialog {

    private ViewOptionListDialogBinding mBinding;

    private ArrayList<String> mOptionStrings = null;
    private ArrayList<Integer> mOptionIconIds = null;

    private AdapterView.OnItemClickListener mItemClickListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.OptionListDialog);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_option_list_dialog, null, false);

        ListView listView = mBinding.listView;
        OptionListAdapter listAdapter = new OptionListAdapter(getContext(), mOptionStrings, mOptionIconIds);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(mItemClickListener);

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        if (mOptionStrings.size() > 4) {
            // 當項目個數太多時, 固定 list view 高度, 超過的項目需滾動 scroll bar 才看得到
            // 在程式裡設定 LayoutParams 時, 必須將想要的 dp 值換算成 pixel
            // pixels = dp * context.getResources().getDisplayMetrics().density;
            float factor = getContext().getResources().getDisplayMetrics().density;
            // 4 個項目的高度大約是 220dp, 設定 list view 的高度再多一些, 露出一點下面的項目, 讓使用者知道還有更多項目
            params.height = (int) (250 * factor);
        } else {
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        listView.setLayoutParams(params);

        builder.setView(mBinding.getRoot());

        Dialog dialog = builder.create();
        // 設置寬度為螢幕寬度、靠近螢幕底部
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams layoutParams = window.getAttributes();
            layoutParams.gravity = Gravity.BOTTOM;
            window.setAttributes(layoutParams);
        }

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (null != dialog) {
            Window window = dialog.getWindow();
            if (window != null) {
                // 為解決 DialogFragment 彈出的時候, 左右兩邊會留白問題
                window.setLayout(-1, -2);
            }
        }
    }

    // 設定 option list 所有項目的文字 (儲存成 ArrayList 格式)
    public void setOptionString(ArrayList<String> optionStrings) {
        mOptionStrings = optionStrings;
    }

    // 設定 option list 所有項目文字前的的 icon (儲存成 ArrayList 格式)
    // icon 可有可無, 若沒有設定 icon 時, adapter 即以純文字方式顯示 option list
    // 若不需要 icon 時不用特別呼叫此 function, 因 mOptionIconIds 有預設值 null, adapter 判斷到 null 即可正確以純文字方式顯示
    public void setOptionIconIds(ArrayList<Integer> optionIconIds) {
        mOptionIconIds = optionIconIds;
    }

    public void setOptionClickListener(AdapterView.OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }
}
