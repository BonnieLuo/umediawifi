package com.onyx.wifi.view.adapter.member.list;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.Member;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.view.interfaces.member.OnMemberListEventListener;

import java.util.List;

public class MemberViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;

    private ImageView mAvatarImageView;

    private ImageView mPhotoImageView;

    private TextView mNameTextView;

    private TextView mPauseStatus;

    private ConstraintLayout mMemberButton;

    private ImageButton mManageButton;

    private FrameLayout mPauseLayout;

    private ImageView mPauseImageView;

    private Button mExtendButton;

    private RecyclerView mRecyclerView;

    private MemberClientGridAdapter mClientGridAdapter;

    private Member mMember;

    private OnMemberListEventListener mEventListener;

    public MemberViewHolder(@NonNull View itemView) {
        super(itemView);

        mContext = this.itemView.getContext();

        mAvatarImageView = this.itemView.findViewById(R.id.avatarImageView);

        mPhotoImageView = this.itemView.findViewById(R.id.photoImageView);

        mNameTextView = this.itemView.findViewById(R.id.nameTextView);

        mManageButton = this.itemView.findViewById(R.id.manageButton);
        mManageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onManageMemberClick(mMember);
                }
            }
        });

        mMemberButton = this.itemView.findViewById(R.id.clMember);
        mMemberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onMemberClick(mMember);
                }
            }
        });


        mPauseLayout = this.itemView.findViewById(R.id.pauseLayout);
        mPauseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMember instanceof FamilyMember != true) {
                    return;
                }

                FamilyMember familyMember = (FamilyMember) mMember;

                int pauseDuration = familyMember.getPauseDuration();

                if (pauseDuration == 0) {
                    pauseDuration = 30;
                } else if (pauseDuration == 30) {
                    pauseDuration = 60;
                } else if (pauseDuration == 60) {
                    pauseDuration = 120;
                } else if (pauseDuration == 120) {
                    pauseDuration = 480;
                } else if (pauseDuration == 480) {
                    pauseDuration = 525600;
                } else if (pauseDuration == 525600) {
                    pauseDuration = 0;
                }

                familyMember.setPauseDuration(pauseDuration);

                if (mEventListener != null) {
                    mEventListener.onPauseMemberClick(mMember);
                }

            }
        });

        mPauseStatus = this.itemView.findViewById(R.id.tvPauseStatus);

        mPauseImageView = this.itemView.findViewById(R.id.pauseImageView);

        mExtendButton = this.itemView.findViewById(R.id.extendButton);
        mExtendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button)v;

                if (mMember.extend()) {
                    String title = mContext.getString(R.string.member_display_hide_device);
                    button.setText(title);

                    button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_hide, 0);

                    List<Client> allClientList = mMember.getAllAllowedClient();//mMember.getAllClients();

                    mClientGridAdapter.setClientList(allClientList);
                } else {
                    String title = mContext.getString(R.string.member_display_more_device);
                    button.setText(title);

                    button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);

                    List<Client>  firstTreeClients = mMember.getFirstThreeAllowedClients();//mMember.getFirstTreeClients();

                    mClientGridAdapter.setClientList(firstTreeClients);
                }
            }
        });

        mClientGridAdapter = new MemberClientGridAdapter();

        mRecyclerView = this.itemView.findViewById(R.id.recyclerView);

        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(false);

            mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));

            int spacingInPixels = mContext.getResources().getDimensionPixelSize(R.dimen.recyclerview_member_device_spacing);
            mRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

            mRecyclerView.setAdapter(mClientGridAdapter);
        }
    }

    public void setMemberListEventListener(OnMemberListEventListener eventListener) {
        mEventListener = eventListener;
        mClientGridAdapter.setMemberListEventListener(mEventListener);
    }

    public void setMember(Member member) {
        mMember = member;

        setupAvatar();

        String name = mMember.getName();
        mNameTextView.setText(name);

        setupPhoto();

        setupPauseInfo();

        setupManagerButton();

        setupExtendButton();

        List<Client> clientList;

        if (mMember.needExtend()) {
            clientList = mMember.getAllAllowedClient();//mMember.getAllClients();
        } else {
            clientList = mMember.getFirstThreeAllowedClients();//mMember.getFirstTreeClients();
        }

        mClientGridAdapter.setClientList(clientList);
    }

    private void setupAvatar() {
        Member.MemberType memberType = mMember.getType();

        switch (memberType) {
            case FAMILY:
                mAvatarImageView.setImageResource(R.drawable.ic_member_default);
                break;
            case HOME:
                mAvatarImageView.setImageResource(R.drawable.ic_member_home);
                break;
            case GUEST:
                mAvatarImageView.setImageResource(R.drawable.ic_member_guest);
                break;
            case BNB:
                mAvatarImageView.setImageResource(R.drawable.ic_member_bnb);
                break;
        }
    }

    private void setupPhoto() {
        Context context = this.itemView.getContext();
        Bitmap bitmap = mMember.getPhoto(context);
        mPhotoImageView.setImageBitmap(bitmap);
    }

    private void setupPauseInfo() {
        Member.MemberType memberType = mMember.getType();

        if (memberType != Member.MemberType.FAMILY) {
            mPauseImageView.setVisibility(View.INVISIBLE);

            return;
        }

        FamilyMember familyMember = (FamilyMember) mMember;
        List<PauseInfo> pauseInfoList = familyMember.getPauseInfoList();

        if (!pauseInfoList.isEmpty()) {
            mPauseImageView.setImageResource(R.drawable.ic_pause_1);
            mPauseImageView.setVisibility(View.VISIBLE);
            mPauseStatus.setVisibility(View.VISIBLE);
            return;
        }

        mPauseImageView.setVisibility(View.VISIBLE);

        mPauseImageView.setImageResource(R.drawable.ic_pause);

        mPauseStatus.setVisibility(View.INVISIBLE);
    }

    private void setupManagerButton() {
        Member.MemberType memberType = mMember.getType();

        if (memberType != Member.MemberType.FAMILY) {
            mManageButton.setVisibility(View.INVISIBLE);

            return;
        }

        mManageButton.setVisibility(View.VISIBLE);
    }

    private void setupExtendButton() {
        int clientListSize = mMember.getClientListSize();

        // init the state of moreButton
        String title = mContext.getString(R.string.member_display_more_device);
        mExtendButton.setText(title);
        mExtendButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);

        if (clientListSize > 3) {
            mExtendButton.setVisibility(View.VISIBLE);
        } else {
            mExtendButton.setVisibility(View.GONE);
        }
    }

    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.top = space;
            outRect.right = space;
            outRect.bottom = space;
        }
    }

}
