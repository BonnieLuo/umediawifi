package com.onyx.wifi.view.activity.menubar.dashboard;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNodeSettingBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.menubar.member.client.ClientSettingsActivity;
import com.onyx.wifi.view.activity.setup.router.RouterSetupActivity;
import com.onyx.wifi.view.adapter.ClientAdapter;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.InputDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.ListDisplayType;
import com.onyx.wifi.viewmodel.menubar.NodeSettingViewModel;

import java.util.ArrayList;
import java.util.Arrays;

public class NodeSettingActivity extends BaseMenuActivity implements ClientAdapter.OnItemClickHandler {

    private ActivityNodeSettingBinding mBinding;
    private NodeSettingViewModel mViewModel;

    private ClientAdapter mOnlineClientAdapter;
    private ArrayList<ClientModel> mOnlineClients = new ArrayList<>();
    private ListDisplayType mClientListDisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;

    private String mDid;
    private DeviceInfo mNodeInfo;

    private String mOriginalDeviceLabel;
    private String mNewDeviceLabel;

    private boolean mIsRouter = false;
    private boolean mRemoveDevice = false;

    private int DEFAULT_ITEM_NUM = 4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDid = getIntent().getStringExtra(AppConstants.EXTRA_DID);
        mIsRouter = mDid.equals(DataManager.getInstance().getControllerDid());

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_node_setting);
        setViewModel();
        setView();

        // 先顯示 share preference 儲存的資料
        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.EVENT_DEVICE_INTERNET_STATUS);
        intentFilter.addAction(AppConstants.EVENT_DEVICE_SPEED_TEST);
        intentFilter.addAction(AppConstants.EVENT_CLIENT_COUNT);
        registerReceiver(mEventReceiver, intentFilter);

        // 打 cloud API 以取得最新的資料
        mViewModel.getNodeInfo();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mMenuBar != null && mMenuBar.isMainMenuOpened()) {
            // 若有開啟 main menu 時, 按返回鍵關閉 main menu 就好, 不要結束頁面
            mMenuBar.closeMainMenu();
        } else {
            Intent data = new Intent();

            // 針對 remove device 的情況, network map 頁面會需要特別處理, 故將結果回傳回去
            if (mRemoveDevice) {
                data.putExtra(AppConstants.EXTRA_DID, mDid);
                data.putExtra(AppConstants.EXTRA_REMOVE_DEVICE, mRemoveDevice);
                setResult(RESULT_OK, data);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // button: device picture 旁邊的 "..." icon
        mBinding.btnManageNode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final OptionListDialog optionListDialog = new OptionListDialog();
                ArrayList<String> optionList = new ArrayList<>();
                ArrayList<Integer> optionIconIdList = new ArrayList<>();
                // Task #8583 (https://umedia.plan.io/issues/8583)
                // remove "Resetup" function/button
//                if (mIsRouter) {
//                    optionList.add(getResources().getString(R.string.resetup));
//                    optionList.add(getResources().getString(R.string.remove_device));
//                    optionIconIdList.add(R.drawable.ic_sheet_resetup);
//                    optionIconIdList.add(R.drawable.ic_sheet_device);
//                } else {
                    optionList.add(getResources().getString(R.string.remove_device));
                    optionIconIdList.add(R.drawable.ic_sheet_device);
//                }
                optionListDialog.setOptionString(optionList);
                optionListDialog.setOptionIconIds(optionIconIdList);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String option = optionList.get(i);
                        if (option.equals(getResources().getString(R.string.resetup))) {
                            Intent intent = new Intent(mContext, RouterSetupActivity.class);
                            intent.putExtra(AppConstants.EXTRA_RESETUP_ROUTER, true);
                            startActivity(intent);
                            finish();
                        } else if (option.equals(getResources().getString(R.string.remove_device))) {
                            final ConfirmDialog confirmDialog = new ConfirmDialog();
                            confirmDialog.setTitle(getString(R.string.remove_device_dialog_title));
                            confirmDialog.setContent(getRemoveDeviceDialogContent());
                            confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mViewModel.removeDevice();
                                    confirmDialog.dismiss();
                                }
                            });
                            confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    confirmDialog.dismiss();
                                }
                            });
                            showDialog(confirmDialog);
                        }
                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        // button: choose label
        mBinding.btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final OptionListDialog optionListDialog = new OptionListDialog();
                String[] options = getResources().getStringArray(R.array.location_label);
                final ArrayList<String> optionList = new ArrayList<>(Arrays.asList(options));
                optionListDialog.setOptionString(optionList);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i == 0) {
                            // Custom - Specify your own
                            final InputDialog inputDialog = new InputDialog();
                            inputDialog.setTitle(getString(R.string.dialog_title_input_label));
                            inputDialog.setInputHint(getString(R.string.dialog_hint_input_label));
                            inputDialog.setPositiveListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    setLabel(inputDialog.getInput());
                                    inputDialog.dismiss();
                                }
                            });
                            inputDialog.setNegativeListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    inputDialog.dismiss();
                                }
                            });
                            showDialog(inputDialog);
                        } else {
                            setLabel(optionList.get(i));
                        }
                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        // button: "Reboot Now"
        mBinding.tvRebootNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.rebootDevice();
            }
        });

        // online client devices

        // 設置 layout manager, 決定 recycler view 的排列方式
        mBinding.rvOnlineClient.setLayoutManager(new LinearLayoutManager(this));
        // 設置分隔線
        mBinding.rvOnlineClient.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mOnlineClients = mViewModel.getOnlineClients();
        mOnlineClientAdapter = new ClientAdapter(mContext, mOnlineClients, this, false);
        mBinding.rvOnlineClient.setAdapter(mOnlineClientAdapter);

        mBinding.tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClientListDisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mClientListDisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                    updateClientListData();
                } else if (mClientListDisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mClientListDisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                    updateClientListData();
                }
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("");
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.updateDeviceLabel(mNewDeviceLabel);
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NodeSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.NodeSetting.GET_NODE_INFO) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Node Info", resultStatus.errorMsg);
                        }
                        mViewModel.getSpeedTest();
                    } else if (resultStatus.actionCode == Code.Action.NodeSetting.GET_SPEED_TEST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Speed Test", resultStatus.errorMsg);
                        }
                        mViewModel.getClientList();
                    } else if (resultStatus.actionCode == Code.Action.NodeSetting.GET_CLIENT_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Client List", resultStatus.errorMsg);
                        }
                        setData();
                    } else if (resultStatus.actionCode == Code.Action.NodeSetting.UPDATE_DEVICE_LABEL) {
                        if (resultStatus.success) {
                            // 更新資料及勾勾狀態
                            mOriginalDeviceLabel = mNewDeviceLabel;
                            checkIfDataChanged();

                            // 將更新後的值寫回 share preference
                            DataManager.getInstance().updateDeviceLabel(mDid, mNewDeviceLabel);
                        } else {
                            showMessageDialog("Update Device Label", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.NodeSetting.REMOVE_DEVICE) {
                        if (resultStatus.success) {
                            if (mIsRouter) {
                                // 如果刪除的 device 是 router, 需要重新 initial setup
                                Intent intent = new Intent(mContext, RouterSetupActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                // remove device 成功, 當前頁面的 device 已不存在, 結束頁面
                                mRemoveDevice = true;
                                onBackPressed();
                            }
                        } else {
                            showMessageDialog("Remove Device", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.NodeSetting.REBOOT_DEVICE) {
                        if (resultStatus.success) {
                            setData();
                            showMessageDialog("Reboot Device", "Send command successfully.");
                        } else {
                            showMessageDialog("Reboot Device", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });

        mViewModel.setDeviceDid(mDid);
    }

    private void setData() {
        mNodeInfo = mViewModel.getSavedNodeInfo(mDid);
        mOriginalDeviceLabel = mNodeInfo.getDeviceLabel();
        mNewDeviceLabel = mNodeInfo.getDeviceLabel();

        // device label
        mBinding.titleBar.setTitle(mOriginalDeviceLabel);
        mBinding.tvLabel.setText(mOriginalDeviceLabel);

        // status
        if (mNodeInfo.getOnline()) {
            mBinding.tvValueStatus.setText(R.string.online);
            mBinding.imgStatus.setImageResource(R.drawable.ic_internet_status_online);
        } else {
            mBinding.tvValueStatus.setText(R.string.offline);
            mBinding.imgStatus.setImageResource(R.drawable.ic_internet_status_offline_gray);
        }

        // ip address
        mBinding.tvValueIpAddress.setText(mNodeInfo.getIp());

        // network speed
        // 目前 cloud API 只有支援 router 的 speed test, 如果是 router 才顯示 speed test 的值
        // (extender 會顯示 UI 預設值: "--Mbps Down, --Mbps Up")
        // TODO: 顯示 extender speed test 的值
        if (mIsRouter) {
            SpeedTest speedTest = DataManager.getInstance().getSpeedTest(mDid);
            int status = speedTest.getStatus();
            if (status != 0) {   // not in progress
                int download = speedTest.getDownload();
                int upload = speedTest.getUpload();
                String down = (download == -1) ? "--" : String.valueOf(download);
                String up = (upload == -1) ? "--" : String.valueOf(upload);
                String result = down + "Mbps Down, " + up + "Mbps Up";
                mBinding.tvValueNetworkSpeed.setText(result);
                mBinding.pbSpeedTest.setVisibility(View.INVISIBLE);
            } else {    // in progress
                mBinding.tvValueNetworkSpeed.setText(getText(R.string.speed_in_progress));
                mBinding.pbSpeedTest.setVisibility(View.VISIBLE);
            }
        }

        // 更新勾勾狀態
        checkIfDataChanged();

        // online client devices
        updateClientListData();
    }

    private void updateClientListData() {
        mOnlineClients = mViewModel.getOnlineClients();
        int clientCount = mOnlineClients.size();
        if (clientCount > 0) {
            mBinding.clOnlineClient.setVisibility(View.VISIBLE);
            if (clientCount <= DEFAULT_ITEM_NUM) {
                mBinding.tvMore.setVisibility(View.INVISIBLE);

                mOnlineClientAdapter.setDevice(mOnlineClients);
            } else {
                mBinding.tvMore.setVisibility(View.VISIBLE);

                if (mClientListDisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mBinding.tvMore.setText(R.string.more);
                    mBinding.tvMore.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);

                    mOnlineClientAdapter.setDevice(new ArrayList<>(mOnlineClients.subList(0, DEFAULT_ITEM_NUM)));
                } else if (mClientListDisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mBinding.tvMore.setText(R.string.hide);
                    mBinding.tvMore.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_hide, 0);

                    mOnlineClientAdapter.setDevice(mOnlineClients);
                }
            }
        } else {
            mBinding.clOnlineClient.setVisibility(View.GONE);
        }
    }

    private void setLabel(String label) {
        mNewDeviceLabel = label;
        mBinding.tvLabel.setText(mNewDeviceLabel);
        mBinding.titleBar.setTitle(mNewDeviceLabel);
        checkIfDataChanged();
    }

    // 更新勾勾狀態
    private void checkIfDataChanged() {
        boolean isChanged = false;
        if (!mNewDeviceLabel.equals(mOriginalDeviceLabel)) {
            isChanged = true;
        }
        mBinding.titleBar.setRightButtonVisibility((isChanged) ? View.VISIBLE : View.INVISIBLE);
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace(action);
            if (action.equals(AppConstants.EVENT_DEVICE_INTERNET_STATUS) ||
                    action.equals(AppConstants.EVENT_DEVICE_SPEED_TEST)) {
                // FcmService 收到 device online/offline/speed test notify 時會將新的值儲存到 share preference
                // setData() 從 share preference 拿資料出來顯示, 不用重打 API 拿資料
                setData();
            } else if (action.equals(AppConstants.EVENT_CLIENT_COUNT)) {
                // 重打 API 刷新 client list
                mViewModel.getClientList();
            }
        }
    };

    private String getRemoveDeviceDialogContent() {
        boolean isOnline = mNodeInfo.getOnline();
        if (mIsRouter) {
            if (isOnline) {
                return getString(R.string.remove_device_dialog_content_online_router);
            } else {
                return getString(R.string.remove_device_dialog_content_offline_router);
            }
        } else {
            if (isOnline) {
                return getString(R.string.remove_device_dialog_content_online_extender);
            } else {
                return getString(R.string.remove_device_dialog_content_offline_extender);
            }
        }
    }

    @Override
    public void onItemClick(ClientModel clientModel) {
        Client client = new Client(clientModel);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setManageClient(client);

        Intent intent = new Intent(NodeSettingActivity.this, ClientSettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onViewClick(int pos) {

    }

    @Override
    public void onClientSelected() {

    }
}
