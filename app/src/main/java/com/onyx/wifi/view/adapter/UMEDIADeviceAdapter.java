package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.UMEDIADevice;

import java.util.List;

public class UMEDIADeviceAdapter extends RecyclerView.Adapter<UMEDIADeviceAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(UMEDIADevice umediaDevice);
        // 提供onItemRemove做為移除項目的事件
        //void onItemRemove(int dataCount, UserType userType, int position);
    }

    private Context mContext;
    public static List<UMEDIADevice> mData; // "public static": global data for related pages(activities)
    //
    // 2. 宣告interface
    //
    private UMEDIADeviceAdapter.OnItemClickHandler mClickHandler;

    public UMEDIADeviceAdapter(
            Context context,
            List<UMEDIADevice> data,
            UMEDIADeviceAdapter.OnItemClickHandler clickHandler) {
        mContext = context;
        mData = data;
        mClickHandler = clickHandler;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private ImageView imgUMEDIADevice;
        private TextView tvName;
        private TextView tvFWVersion;
        private TextView tvDevStatus;

        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            imgUMEDIADevice = (ImageView) itemView.findViewById(R.id.imgUMEDIADevice);// 從view(list item)中取得TextView實體
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvFWVersion = (TextView) itemView.findViewById(R.id.tvFWVersion);
            tvDevStatus = (TextView) itemView.findViewById(R.id.tvDevStatus);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UMEDIADevice umediaDevice = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(umediaDevice);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // binding with layout
    @Override // required
    public UMEDIADeviceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_onyx_device_list_item, viewGroup, false); //取得list的view
        return new UMEDIADeviceAdapter.ViewHolder(view);
    }

    // set data (according to the status of each device)
    @Override // required
    public void onBindViewHolder(@NonNull UMEDIADeviceAdapter.ViewHolder viewHolder, int i) {
        //
        // device type
        //
        if (mData.get(i).getDeviceType() == AppConstants.DeviceType.DESKTOP) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hrn22ac); // image needs to be update
        }
        if (mData.get(i).getDeviceType() == AppConstants.DeviceType.PLUG) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hex22acp); // image needs to be update
        }
        if (mData.get(i).getDeviceType() == AppConstants.DeviceType.TOWER) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
        }

        //
        //  Name
        //
        viewHolder.tvName.setText(mData.get(i).getName());

        //
        // FW version
        //
        viewHolder.tvFWVersion.setText(mData.get(i).getFWVersion());

        //
        // Status
        //
        if (mData.get(i).getIsHasIssue()) {
            viewHolder.tvDevStatus.setText("Failed");
            viewHolder.tvDevStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_cf0c0a));
        } else {
            viewHolder.tvDevStatus.setText("Passed");
            viewHolder.tvDevStatus.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
        }
    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public void setDevice(List<UMEDIADevice> data) {
        mData = data;
        notifyDataSetChanged();
    }

}
