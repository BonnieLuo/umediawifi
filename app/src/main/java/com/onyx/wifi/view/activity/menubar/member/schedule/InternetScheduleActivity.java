package com.onyx.wifi.view.activity.menubar.member.schedule;

import android.app.TimePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TimePicker;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityEditInternetScheduleBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.ScheduleConfiguration;
import com.onyx.wifi.model.item.member.ScheduleConfigurationModel;
import com.onyx.wifi.model.item.member.ScheduleFrequency;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.schedule.ScheduleConfigurationViewModel;

import java.util.Calendar;
import java.util.Date;

public class InternetScheduleActivity extends BaseMenuActivity {

    private ActivityEditInternetScheduleBinding mBinding;

    private ScheduleConfigurationViewModel mViewModel;

    private ScheduleConfiguration mScheduleConfig;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_internet_schedule);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            setData();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        mBinding.customEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mScheduleConfig != null) {
                    String name = mBinding.customEditText.getText().toString().trim();
                    mScheduleConfig.setName(name);
                }

                checkDiff();
            }
        });

        mBinding.enableImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    mScheduleConfig.toggleEnable();

                    setScheduleEnable();
                }

                checkDiff();
            }
        });

        mBinding.editStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Time
                Calendar calendar = Calendar.getInstance();

                if (mScheduleConfig != null) {
                    Date startTime = mScheduleConfig.getStartTime();

                    if (startTime != null) {
                        calendar.setTime(startTime);
                    }
                }

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, 0);

                                if (mScheduleConfig != null) {
                                    Date newStartTime = calendar.getTime();
                                    mScheduleConfig.setStartTime(newStartTime);

                                    setScheduleStartTime();

                                    checkDiff();
                                }

                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        mBinding.editEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Time
                final Calendar calendar = Calendar.getInstance();

                if (mScheduleConfig != null) {
                    Date endTime = mScheduleConfig.getEndTime();

                    if (endTime != null) {
                        calendar.setTime(endTime);
                    }
                }

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, 0);

                                if (mScheduleConfig != null) {
                                    Date newStartTime = calendar.getTime();
                                    mScheduleConfig.setEndTime(newStartTime);

                                    setScheduleEndTime();

                                    checkDiff();
                                }
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        mBinding.sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleSunday();

                    updateSunday();
                }

                checkDiff();
            }
        });

        mBinding.mon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleMonday();

                    updateMonday();
                }

                checkDiff();
            }
        });

        mBinding.tue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleTuesday();

                    updateTuesday();
                }

                checkDiff();
            }
        });

        mBinding.wed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleWednesday();

                    updateWednesday();
                }

                checkDiff();
            }
        });

        mBinding.thu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleThursday();

                    updateThursday();
                }

                checkDiff();
            }
        });

        mBinding.fri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleFriday();

                    updateFriday();
                }

                checkDiff();
            }
        });

        mBinding.sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScheduleConfig != null) {
                    ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                    frequency.toggleSaturday();

                    updateSaturday();
                }

                checkDiff();
            }
        });

        mBinding.eraseScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog confirmDialog = new ConfirmDialog();

                confirmDialog.setTitle("Remove Schedule");
                confirmDialog.setContent("Are you sure you want to remove this schedule?");
                confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataManager dataManager = DataManager.getInstance();
                        FamilyMember member = dataManager.getManageMember();
                        String memberId = member.getId();
                        String scheduleId = mScheduleConfig.getId();

                        mViewModel.removeSchedule(memberId, scheduleId);
                    }
                });
                confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmDialog.dismiss();
                    }
                });

                showDialog(confirmDialog);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Manage Schedule");

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mScheduleConfig.getName();
                if (!InputUtils.isNameValid(name)) {
                    String title = getString(R.string.member_name_invalid_name);
                    String message = getString(R.string.member_name_rule);

                    showMessageDialog(title, message);

                    return;
                }

                Date startTime = mScheduleConfig.getStartTime();
                if (startTime == null) {
                    String title = getString(R.string.internet_invalid_start_time_title);
                    String message = getString(R.string.internet_invalid_start_time);

                    showMessageDialog(title, message);

                    return;
                }

                Date endTime = mScheduleConfig.getEndTime();
                if (endTime == null) {
                    String title = getString(R.string.internet_invalid_end_time_title);
                    String message = getString(R.string.internet_invalid_end_time);

                    showMessageDialog(title, message);

                    return;
                }

                if (startTime.after(endTime)) {
                    String title = getString(R.string.internet_invalid_time_title);
                    String message = getString(R.string.internet_invalid_time);

                    showMessageDialog(title, message);

                    return;
                }

                ScheduleFrequency frequency = mScheduleConfig.getFrequency();
                if (frequency.isEmpty()) {
                    String title = getString(R.string.internet_invalid_frequency_title);
                    String message = getString(R.string.internet_invalid_frequency);

                    showMessageDialog(title, message);

                    return;
                }

                DataManager dataManager = DataManager.getInstance();
                FamilyMember member = dataManager.getManageMember();
                String memberId = member.getId();

                ScheduleConfiguration scheduleConfiguration = dataManager.getScheduleConfiguration();

                if (scheduleConfiguration != null) {
                    mViewModel.updateSchedule(memberId, mScheduleConfig);
                } else {
                    mViewModel.createSchedule(memberId, mScheduleConfig);
                }
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ScheduleConfigurationViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Member.CREATE_MEMBER_SCHEDULE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Create Schedule", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.UPDATE_MEMBER_SCHEDULE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Schedule", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.REMOVE_MEMBER_SCHEDULE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Schedule", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }
                }
            }
        });
    }

    private void setData() {
        DataManager dataManager = DataManager.getInstance();

        mScheduleConfig = dataManager.getScheduleConfiguration();

        if (mScheduleConfig == null) {
            mScheduleConfig = new ScheduleConfiguration(new ScheduleConfigurationModel());
        }

        setScheduleName();

        setScheduleEnable();

        setScheduleStartTime();

        setScheduleEndTime();

        updateSunday();
        updateMonday();
        updateTuesday();
        updateWednesday();
        updateThursday();
        updateFriday();
        updateSaturday();

        setRemoveButton();
    }

    private void setScheduleName() {
        String name = mScheduleConfig.getName();
        mBinding.customEditText.setText(name);
    }

    private void setScheduleEnable() {
        boolean enable = mScheduleConfig.isEnabled();

        if (enable) {
            mBinding.enableTextView.setText("Enabled");
            mBinding.enableImageView.setSelected(true);
        } else {
            mBinding.enableTextView.setText("Disabled");
            mBinding.enableImageView.setSelected(false);
        }
    }

    private void setScheduleStartTime() {
        String startTime = mScheduleConfig.getStartTimeString("hh:mm a");
        mBinding.starTimeTextView.setText(startTime);
    }

    private void setScheduleEndTime() {
        String endTime = mScheduleConfig.getEndTimeString("hh:mm a");
        mBinding.endTimeTextView.setText(endTime);
    }

    private void updateSunday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isSundayEnable()) {
            mBinding.sun.setImageResource(R.drawable.ic_schedule_7_on);
        } else {
            mBinding.sun.setImageResource(R.drawable.ic_schedule_7_off);
        }
    }

    private void updateMonday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isMondayEnable()) {
            mBinding.mon.setImageResource(R.drawable.ic_schedule_1_on);
        } else {
            mBinding.mon.setImageResource(R.drawable.ic_schedule_1_off);
        }
    }

    private void updateTuesday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isTuesdayEnable()) {
            mBinding.tue.setImageResource(R.drawable.ic_schedule_2_on);
        } else {
            mBinding.tue.setImageResource(R.drawable.ic_schedule_2_off);
        }
    }

    private void updateWednesday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isWednesdayEnable()) {
            mBinding.wed.setImageResource(R.drawable.ic_schedule_3_on);
        } else {
            mBinding.wed.setImageResource(R.drawable.ic_schedule_3_off);
        }
    }

    private void updateThursday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isThursdayEnable()) {
            mBinding.thu.setImageResource(R.drawable.ic_schedule_4_on);
        } else {
            mBinding.thu.setImageResource(R.drawable.ic_schedule_4_off);
        }
    }

    private void updateFriday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isFridayEnable()) {
            mBinding.fri.setImageResource(R.drawable.ic_schedule_5_on);
        } else {
            mBinding.fri.setImageResource(R.drawable.ic_schedule_5_off);
        }
    }

    private void updateSaturday() {
        ScheduleFrequency frequency = mScheduleConfig.getFrequency();

        if (frequency.isSaturdayEnable()) {
            mBinding.sat.setImageResource(R.drawable.ic_schedule_6_on);
        } else {
            mBinding.sat.setImageResource(R.drawable.ic_schedule_6_off);
        }
    }

    private void setRemoveButton() {
        String scheduleId = mScheduleConfig.getId();

        if (scheduleId == null) {
            mBinding.eraseScheduleButton.setEnabled(false);

            return;
        }

        if (scheduleId.isEmpty() || mScheduleConfig.isEnabled()) {
            mBinding.eraseScheduleButton.setEnabled(false);
        } else {
            mBinding.eraseScheduleButton.setEnabled(true);
        }
    }

    private void checkDiff() {
        boolean diff = mScheduleConfig.checkDiff();

        if (diff) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

}
