package com.onyx.wifi.view.activity.mainmenu.wirelesssetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityBlockedDevicesBinding;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.BlockedClientAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class BlockedDevicesActivity extends BaseMenuActivity implements BlockedClientAdapter.OnItemClickHandler {
    private ActivityBlockedDevicesBinding mBinding;
    private NetworkSettingViewModel mViewModel;
    private BlockedClientAdapter mAdapter;
    private Timer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_blocked_devices);
        setViewModel();
        mViewModel.getClientInfoList();
        initClientList();
        setView();
    }

    private void setUpBlockedClientRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding.rvBlockedDevicesList.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding.rvBlockedDevicesList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        if (mAdapter == null) {
            mAdapter = new BlockedClientAdapter(mContext, mViewModel.getmBlockedClient(), this);
        }
        mBinding.rvBlockedDevicesList.setAdapter(mAdapter);
    }

    private void updateClientList() {
        ArrayList<ClientModel> data = new ArrayList<ClientModel>();
        //////////////////////////////////////////////////
        if (mAdapter != null) {

            data.addAll(mViewModel.getmBlockedClient());
            mAdapter.setDevice(data); //在此處直接用mViewModel.getFWStatusList()，在setDevice裡會變成null

            mBinding.rvBlockedDevicesList.setAdapter(mAdapter);
        }

        // show/hide "No Device!"
        if (mViewModel.getmBlockedClient().size() == 0) {
            mBinding.groupNoDevice.setVisibility(View.VISIBLE);
        } else {
            mBinding.groupNoDevice.setVisibility(View.INVISIBLE);
        }
    }

    void startTimer() {
        if (mTimer == null) {
            BlockedDevicesActivity.GetListTimerTask getListTimerTask = new BlockedDevicesActivity.GetListTimerTask();
            mTimer = new Timer();
            mTimer.schedule(getListTimerTask, 10L, 5000L);
        }
    }

    void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void initClientList() {
        startTimer();
        updateClientList();//之前放在startTimer()之前
    }

    @Override
    protected void onResume() {
        super.onResume();
        initClientList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        setUpBlockedClientRecycler();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Blocked Devices");
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus.actionCode == Code.Action.NetworkSetting.GET_CLIENT_INFO_LIST) {
                    if (resultStatus.success) {

                        updateClientList();
                    } else {
                        showMessageDialog("Get Client List", resultStatus.errorMsg);
                    }
                } else if (resultStatus.actionCode == Code.Action.Member.UNBLOCK_CLIENT) {
                    if (resultStatus.success) {

                        updateClientList();
                    } else {
                        showMessageDialog("UnBlock Client List", resultStatus.errorMsg);
                    }
                }
            }
        });
    }

    @Override
    public void onItemClick(ClientModel clientModel) {

    }

    @Override
    public void onDelete(ClientModel clientModel) {
        mViewModel.unblockClient(clientModel);
    }

    @Override
    public void onViewClick(int pos) {

    }

    @Override
    public void onClientSelected() {

    }

    private class GetListTimerTask extends TimerTask {

        @Override
        public void run() {

            mViewModel.getClientInfoList();
        }
    }
}
