package com.onyx.wifi.view.adapter.member.client;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.view.interfaces.member.OnClientTypeClickListener;

import java.util.Map;

class ClientIconGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String[] mClientTypeList;

    private String mSelectedClientType;

    private OnClientTypeClickListener mOnClientTypeClickListener;

    public ClientIconGridAdapter(String[] clientTypeList, String selectedClientType, OnClientTypeClickListener listener) {
        super();

        mClientTypeList = clientTypeList;

        mSelectedClientType = selectedClientType;

        mOnClientTypeClickListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_device_icon, parent, false);

        IconViewHolder viewHolder = new IconViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        IconViewHolder iconViewHolder = (IconViewHolder)viewHolder;

        String clientType = mClientTypeList[position];
        iconViewHolder.setClientType(clientType, mSelectedClientType);
    }

    @Override
    public int getItemCount() {
        return  mClientTypeList.length;
    }

    class IconViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private String mClientType;

        private ImageView mIconImageView;

        private ImageView mSelectedImageView;

        public IconViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mIconImageView = this.itemView.findViewById(R.id.iconImageView);

            mSelectedImageView = this.itemView.findViewById(R.id.selectedImageView);
        }

        public void setClientType(String clientType, String mSelectedClientType) {
            mClientType = clientType;

            DataManager dataManager = DataManager.getInstance();
            Map<String, Integer> devicePictureMap = dataManager.getClientPictureMap();

            String lowercaseDeviceType = mClientType.toLowerCase();
            Integer devicePictureId = devicePictureMap.get(lowercaseDeviceType);

            mIconImageView.setImageResource(devicePictureId);

            mSelectedImageView.setVisibility(View.INVISIBLE);

            if (mSelectedClientType != null && mClientType.equalsIgnoreCase(mSelectedClientType)) {
                mSelectedImageView.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View v) {
            if (mOnClientTypeClickListener != null) {
                mOnClientTypeClickListener.onClientTypeClick(mClientType);
            }
        }
    }
}
