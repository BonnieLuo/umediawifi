package com.onyx.wifi.view.activity.mainmenu.wirelesssetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityWirelessInfoSettingBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.wirelesssetting.WirelessSettingData;
import com.onyx.wifi.viewmodel.mainmenu.WirelessSettingViewModel;

public class WirelessInfoSettingActivity extends BaseWirelessSettingActivity {

    private ActivityWirelessInfoSettingBinding mBinding;
    private WirelessSettingViewModel mViewModel;

    private WirelessSettingData mOriginalWirelessSettingData;
    private WirelessSettingData mTempWirelessSettingData;

    private String mInputErrorMsg = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String wirelessInfo = getIntent().getStringExtra(AppConstants.EXTRA_WIRELESS_INFO);
        mOriginalWirelessSettingData = new JsonParserWrapper().jsonToObject(wirelessInfo, WirelessSettingData.class);
        mTempWirelessSettingData = new JsonParserWrapper().jsonToObject(wirelessInfo, WirelessSettingData.class);
        LogUtils.trace(mOriginalWirelessSettingData.toString());

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_wireless_info_setting);
        setView();
        setViewModel();

        setData();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // [按鈕] SSID 的筆型圖示
        mBinding.btnEditSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEditMode(mBinding.etValueNetworkSsid);
            }
        });

        // [文字元件] SSID, 設置各項事件
        mBinding.etValueNetworkSsid.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            // 監聽 EditText 的焦點以判斷是否需要結束編輯模式
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkFocusChange(mBinding.etValueNetworkSsid, hasFocus);
            }
        });
        mBinding.etValueNetworkSsid.addTextChangedListener(new TextWatcher() {
            // 監聽 EditText 的文字改變
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // 當使用者輸入完要將值儲存到 temp 物件, 並判斷是否需要顯示勾勾
                String ssid = s.toString().trim();
                switch (mOriginalWirelessSettingData.getSettingType()) {
                    case INFO_2G:
                        mTempWirelessSettingData.setSsid2g(ssid);
                        break;

                    case INFO_5G:
                        mTempWirelessSettingData.setSsid5g(ssid);
                        break;

                    case BOTH:
                        mTempWirelessSettingData.setSsid2g(ssid);
                        mTempWirelessSettingData.setSsid5g(ssid);
                        break;
                }
                checkIfDataChanged();
            }
        });
        mBinding.etValueNetworkSsid.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            // 監聽鍵盤 enter 鍵的事件
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // 在 layout 裡有設定 EditText 的 imeOptions="actionDone", 故這裡是判斷 IME_ACTION_DONE
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    exitEditMode(mBinding.etValueNetworkSsid);
                }
                return false;
            }
        });

        // [按鈕] password 的筆型圖示
        mBinding.btnEditPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEditMode(mBinding.etValueNetworkPwd);
            }
        });

        // [文字元件] password, 設置各項事件 (同上述 SSID 文字元件的說明)
        mBinding.etValueNetworkPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkFocusChange(mBinding.etValueNetworkPwd, hasFocus);
            }
        });
        mBinding.etValueNetworkPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String password = s.toString().trim();
                switch (mOriginalWirelessSettingData.getSettingType()) {
                    case INFO_2G:
                        mTempWirelessSettingData.setPassword2g(password);
                        break;

                    case INFO_5G:
                        mTempWirelessSettingData.setPassword5g(password);
                        break;

                    case BOTH:
                        mTempWirelessSettingData.setPassword2g(password);
                        mTempWirelessSettingData.setPassword5g(password);
                        break;
                }
                checkIfDataChanged();
            }
        });
        mBinding.etValueNetworkPwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    exitEditMode(mBinding.etValueNetworkPwd);
                }
                return false;
            }
        });
        // [按鈕] password 的眼睛圖示
        mBinding.btnShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setPasswordVisible(mBinding.etValueNetworkPwd, isChecked);
            }
        });
        // [按鈕] Generate Random Password
        mBinding.btnGeneratePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.etValueNetworkPwd.setText(InputUtils.generateNetworkPassword());
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        switch (mOriginalWirelessSettingData.getSettingType()) {
            case INFO_2G:
                mBinding.titleBar.setTitle("2.4GHz " + getString(R.string.wireless_setting_title));
                break;

            case INFO_5G:
                mBinding.titleBar.setTitle("5GHz " + getString(R.string.wireless_setting_title));
                break;

            case BOTH:
            default:
                mBinding.titleBar.setTitle(R.string.wireless_setting_title);
                break;
        }
        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDataValid()) {
                    exitEditMode(mBinding.etValueNetworkSsid);
                    exitEditMode(mBinding.etValueNetworkPwd);
                    mViewModel.updateWirelessSetting(mTempWirelessSettingData, true);
                } else {
                    showMessageDialog("Invalid Input", mInputErrorMsg);
                }
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(WirelessSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.WirelessSetting.UPDATE_WIRELESS_SETTING) {
                        if (resultStatus.success) {
                            finish();
                        } else {
                            if (resultStatus.errorMsg.equals(getString(R.string.err_socket_exception))) {
                                showMessageDialog("Update Wireless Setting", "Your phone disconnected from Wi-Fi because you change " +
                                        "the wireless setting.\nPlease reconnect to network and try again in order to sync the setting.");
                            } else {
                                showMessageDialog("Update Wireless Setting", resultStatus.errorMsg);
                            }
                        }
                    }
                }
            }
        });
    }

    private void setData() {
        switch (mOriginalWirelessSettingData.getSettingType()) {
            case INFO_2G:
                mBinding.tvNameNetworkSsid.setText(R.string.item_2g_wireless_network_ssid);
                mBinding.etValueNetworkSsid.setText(mOriginalWirelessSettingData.getSsid2g());
                mBinding.tvNameNetworkPwd.setText(R.string.item_2g_wireless_network_password);
                mBinding.etValueNetworkPwd.setText(mOriginalWirelessSettingData.getPassword2g());
                break;

            case INFO_5G:
                mBinding.tvNameNetworkSsid.setText(R.string.item_5g_wireless_network_ssid);
                mBinding.etValueNetworkSsid.setText(mOriginalWirelessSettingData.getSsid5g());
                mBinding.tvNameNetworkPwd.setText(R.string.item_5g_wireless_network_password);
                mBinding.etValueNetworkPwd.setText(mOriginalWirelessSettingData.getPassword5g());
                break;

            case BOTH:
            default:
                mBinding.tvNameNetworkSsid.setText(R.string.item_wireless_network_ssid);
                mBinding.etValueNetworkSsid.setText(mOriginalWirelessSettingData.getSsid2g());
                mBinding.tvNameNetworkPwd.setText(R.string.item_wireless_network_password);
                mBinding.etValueNetworkPwd.setText(mOriginalWirelessSettingData.getPassword2g());
                break;
        }
        setMultiLineDisplay(mBinding.etValueNetworkSsid);
        setMultiLineDisplay(mBinding.etValueNetworkPwd);
    }

    private void enterEditMode(EditText editText) {
        editText.setEnabled(true);
        editText.requestFocus();
        showKeyboard(editText);
        showKeyboard(editText);
        editText.setSelection(editText.getEditableText().length());
    }

    private void exitEditMode(EditText editText) {
        editText.setEnabled(false);
    }

    private void checkFocusChange(EditText editText, boolean hasFocus) {
        // EditText 失去焦點時 (例如: 點擊 EditText 以外的區域) 要結束編輯模式
        if (!hasFocus) {
            exitEditMode(editText);
        }
    }

    private void checkIfDataChanged() {
        boolean isChanged = false;
        switch (mTempWirelessSettingData.getSettingType()) {
            case INFO_2G:
                // 檢查 2.4G SSID, password 是否有更改
                if (!mTempWirelessSettingData.getSsid2g().equals(mOriginalWirelessSettingData.getSsid2g())) {
                    isChanged = true;
                }
                if (!mTempWirelessSettingData.getPassword2g().equals(mOriginalWirelessSettingData.getPassword2g())) {
                    isChanged = true;
                }
                break;

            case INFO_5G:
                // 檢查 5G SSID, password 是否有更改
                if (!mTempWirelessSettingData.getSsid5g().equals(mOriginalWirelessSettingData.getSsid5g())) {
                    isChanged = true;
                }
                if (!mTempWirelessSettingData.getPassword5g().equals(mOriginalWirelessSettingData.getPassword5g())) {
                    isChanged = true;
                }
                break;

            case BOTH:
                // 2.4G 和 5G 都檢查
                if (!mTempWirelessSettingData.getSsid2g().equals(mOriginalWirelessSettingData.getSsid2g())) {
                    isChanged = true;
                }
                if (!mTempWirelessSettingData.getPassword2g().equals(mOriginalWirelessSettingData.getPassword2g())) {
                    isChanged = true;
                }
                if (!mTempWirelessSettingData.getSsid5g().equals(mOriginalWirelessSettingData.getSsid5g())) {
                    isChanged = true;
                }
                if (!mTempWirelessSettingData.getPassword5g().equals(mOriginalWirelessSettingData.getPassword5g())) {
                    isChanged = true;
                }
                break;
        }
        mBinding.titleBar.setRightButtonVisibility((isChanged) ? View.VISIBLE : View.INVISIBLE);
    }

    private boolean isDataValid() {
        String msgSsidInvalid = InputUtils.getInvalidWifiSsidMessage("SSID");
        String msgPwdInvalid = InputUtils.getInvalidWifiPasswordMessage("password");
        String msgWhitespace = mContext.getString(R.string.err_ssid_invalid_space);
        switch (mTempWirelessSettingData.getSettingType()) {
            case INFO_2G:
                // 檢查 2.4G SSID, password 是否合法
                if (!InputUtils.isWifiSsidValid(mTempWirelessSettingData.getSsid2g())) {
                    mInputErrorMsg = msgSsidInvalid;
                    return false;
                } else if (!InputUtils.isWifiPasswordValid(mTempWirelessSettingData.getPassword2g())) {
                    mInputErrorMsg = msgPwdInvalid;
                    return false;
                } else if (InputUtils.containsWhitespace(mTempWirelessSettingData.getSsid2g())) {
                    // TODO: constraint to be removed if Bug #7076 is solved
                    // https://umedia.plan.io/issues/7076?pn=1#change-28750
                    mInputErrorMsg = msgWhitespace;
                    return false;
                }
                break;

            case INFO_5G:
                // 檢查 5G SSID, password 是否合法
                if (!InputUtils.isWifiSsidValid(mTempWirelessSettingData.getSsid5g())) {
                    mInputErrorMsg = msgSsidInvalid;
                    return false;
                } else if (!InputUtils.isWifiPasswordValid(mTempWirelessSettingData.getPassword5g())) {
                    mInputErrorMsg = msgPwdInvalid;
                    return false;
                } else if (InputUtils.containsWhitespace(mTempWirelessSettingData.getSsid5g())) {
                    // TODO: constraint to be removed if Bug #7076 is solved
                    // https://umedia.plan.io/issues/7076?pn=1#change-28750
                    mInputErrorMsg = msgWhitespace;
                    return false;
                }
                break;

            case BOTH:
                // 2.4G 和 5G 都檢查
                if (!InputUtils.isWifiSsidValid(mTempWirelessSettingData.getSsid2g())) {
                    mInputErrorMsg = msgSsidInvalid;
                    return false;
                } else if (!InputUtils.isWifiPasswordValid(mTempWirelessSettingData.getPassword2g())) {
                    mInputErrorMsg = msgPwdInvalid;
                    return false;
                } else if (!InputUtils.isWifiSsidValid(mTempWirelessSettingData.getSsid5g())) {
                    mInputErrorMsg = msgSsidInvalid;
                    return false;
                } else if (!InputUtils.isWifiPasswordValid(mTempWirelessSettingData.getPassword5g())) {
                    mInputErrorMsg = msgPwdInvalid;
                    return false;
                } else if (InputUtils.containsWhitespace(mTempWirelessSettingData.getSsid2g())) {
                    // TODO: constraint to be removed if Bug #7076 is solved
                    // https://umedia.plan.io/issues/7076?pn=1#change-28750
                    mInputErrorMsg = msgWhitespace;
                    return false;
                } else if (InputUtils.containsWhitespace(mTempWirelessSettingData.getSsid5g())) {
                    // TODO: constraint to be removed if Bug #7076 is solved
                    // https://umedia.plan.io/issues/7076?pn=1#change-28750
                    mInputErrorMsg = msgWhitespace;
                    return false;
                }
                break;
        }
        return true;
    }
}
