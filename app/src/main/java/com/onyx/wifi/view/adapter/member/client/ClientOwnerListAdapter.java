package com.onyx.wifi.view.adapter.member.client;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.interfaces.member.OnClientOwnerChangedListener;

import java.util.ArrayList;
import java.util.List;

public class ClientOwnerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MemberModel> mMemberList = new ArrayList<MemberModel>();

    private OnClientOwnerChangedListener mOnClientOwnerChangedListener;

    public ClientOwnerListAdapter() {
        super();

        DataManager dataManager = DataManager.getInstance();

        MemberList memberList = dataManager.getMemberList();

        if (memberList != null)
            mMemberList = memberList.getFamilyMemberList();

    }

    public void setDevice(List<MemberModel> data) {
        //通过System.identityHashCode(object)方法来间接的获取内存地址；
        //创建出来的对象，只要没被销毁，内存地址始终不变。
        //Adapter绑定的数据源集合要为同一个集合，notifyDataSetChanged()方法才有效，否则需要重新设置数据源
        //mData = data; <-錯誤寫法，address可能會變更

        mMemberList.clear();
        mMemberList.addAll(data);
        LogUtils.trace("status", "address: " + System.identityHashCode(mMemberList));
        notifyDataSetChanged();
    }

    public void setOnClientOwnerChangedListener(OnClientOwnerChangedListener listener) {
        mOnClientOwnerChangedListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_member_select_list_item, parent, false);

        MemberViewHolder memberViewHolder = new MemberViewHolder(view);

        return memberViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (mMemberList != null) {
            MemberModel memberModel = mMemberList.get(position);
            MemberViewHolder memberViewHolder = (MemberViewHolder) viewHolder;
            memberViewHolder.setMemberModel(memberModel);
        }
    }

    @Override
    public int getItemCount() {
        if (mMemberList != null)
            return mMemberList.size();
        else
            return 0;
    }

    class MemberViewHolder extends RecyclerView.ViewHolder {

        FamilyMember mMember;

        Client mClient;

        ImageView userImageView;
        TextView userNameTextView;
        ImageButton assignButton;

        public MemberViewHolder(@NonNull View itemView) {
            super(itemView);

            DataManager dataManager = DataManager.getInstance();

            mClient = dataManager.getManageClient();

            userImageView = itemView.findViewById(R.id.userImageView);

            userNameTextView = itemView.findViewById(R.id.textView);

            assignButton = itemView.findViewById(R.id.assignButton);
            assignButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String ownerId = mClient.getTemporaryOwnerId();

                    String memberId = mMember.getId();

                    if (ownerId == null) {
                        mClient.setTemporaryOwnerId(memberId);

                        notifyDataSetChanged();

                        if (mOnClientOwnerChangedListener != null) {
                            boolean isOwnerChanged = mClient.isOwnerChanged();
                            mOnClientOwnerChangedListener.onClientOwnerChanged(isOwnerChanged);
                        }

                        return;
                    }

                    if (memberId.equalsIgnoreCase(ownerId)) {
                        mClient.setTemporaryOwnerId(null);
                    } else {
                        mClient.setTemporaryOwnerId(memberId);
                    }

                    notifyDataSetChanged();

                    if (mOnClientOwnerChangedListener != null) {
                        boolean isOwnerChanged = mClient.isOwnerChanged();
                        mOnClientOwnerChangedListener.onClientOwnerChanged(isOwnerChanged);
                    }
                }
            });
        }

        public void setMemberModel(MemberModel memberModel) {
            mMember = new FamilyMember(memberModel);

            Context context = this.itemView.getContext();
            Bitmap pictureBitmap = mMember.getPhoto(context);

            if (pictureBitmap != null) {
                userImageView.setImageBitmap(pictureBitmap);
            } else {
                userImageView.setImageResource(0);
            }

            String name = mMember.getName();
            userNameTextView.setText(name);
            userNameTextView.setTextColor(context.getResources().getColor(R.color.gray_676767));

            isSelected();
        }

        public boolean isSelected() {
            String memberId = mMember.getId();

            String ownerId = mClient.getTemporaryOwnerId();

            if (ownerId != null) {
                Context context = this.itemView.getContext();

                if (memberId.equalsIgnoreCase(ownerId)) {
                    userNameTextView.setTextColor(context.getResources().getColor(R.color.purple_4e1393));
                    assignButton.setSelected(true);

                    return true;
                }
            }

            Context context = this.itemView.getContext();
            userNameTextView.setTextColor(context.getResources().getColor(R.color.gray_676767));
            assignButton.setSelected(false);

            return false;
        }

    }
}
