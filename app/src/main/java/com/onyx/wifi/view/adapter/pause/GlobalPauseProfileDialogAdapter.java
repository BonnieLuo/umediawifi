package com.onyx.wifi.view.adapter.pause;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.pause.PauseProfileModel;

import java.util.ArrayList;
import java.util.List;

public class GlobalPauseProfileDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface RecyclerViewClickListener {
        void onPauseProfileClick(PauseProfileModel pauseProfile);

        void onPauseClick(PauseProfileModel pauseProfile);

        void onCreatePauseProfileClick();
    }

    private List<PauseProfileViewModel> mProfileList = new ArrayList<PauseProfileViewModel>();

    private RecyclerViewClickListener mItemClickListener;

    @Override
    public int getItemViewType(int position) {
        PauseProfileViewModel viewModel = mProfileList.get(position);

        PauseProfileType pauseProfileType= viewModel.getPauseProfileType();

        return pauseProfileType.ordinal();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == PauseProfileType.FOOTER.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_global_pause_create_profile, parent, false);

            FooterViewHolder footer = new FooterViewHolder(view);
            return footer;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_global_pause_profile, parent, false);

        PauseProfileViewHolder viewHolder = new PauseProfileViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        PauseProfileViewModel viewModel = mProfileList.get(position);

        PauseProfileType pauseProfileType = viewModel.getPauseProfileType();

        if (pauseProfileType == PauseProfileType.PROFILE) {
            setProfile(viewModel, viewHolder);
        }

    }

    @Override
    public int getItemCount() {
        return mProfileList.size();
    }

    private void setProfile(PauseProfileViewModel viewModel, RecyclerView.ViewHolder viewHolder){
        PauseProfileModel pauseProfile = (PauseProfileModel) viewModel.getData();

        if (pauseProfile != null) {
            PauseProfileViewHolder pauseProfileViewHolder = (PauseProfileViewHolder) viewHolder;
            pauseProfileViewHolder.setPauseProfile(pauseProfile);
        }
    }

    public void setProfileList(List<PauseProfileViewModel> profileList) {
        mProfileList = profileList;
        notifyDataSetChanged();
    }

    public void setItemClickListener(RecyclerViewClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    class PauseProfileViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        private PauseProfileModel mPauseProfile;

        private TextView titleTextView;

        private ImageView mArrorImageView;

        public PauseProfileViewHolder(@NonNull View itemView) {
            super(itemView);

            mContext = this.itemView.getContext();

            titleTextView = this.itemView.findViewById(R.id.titleTextView);

            mArrorImageView = this.itemView.findViewById(R.id.arrow);

            mArrorImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onPauseProfileClick(mPauseProfile);
                    }
                }
            });
        }

        public void setPauseProfile(PauseProfileModel pauseProfile) {
            mPauseProfile = pauseProfile;

            String name = mPauseProfile.getName();
            titleTextView.setText(name);
        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        private ImageView mArrorImageView;

        public FooterViewHolder(@NonNull View itemView) {
            super(itemView);

            mContext = this.itemView.getContext();

            mArrorImageView = this.itemView.findViewById(R.id.arrow);

            mArrorImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onCreatePauseProfileClick();
                    }
                }
            });
        }

    }

}
