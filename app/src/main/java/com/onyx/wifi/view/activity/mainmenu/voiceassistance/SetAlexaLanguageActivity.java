package com.onyx.wifi.view.activity.mainmenu.voiceassistance;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySetAlexaLanguageBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.adapter.LanguageAdapter;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.Language;

import java.util.ArrayList;

public class SetAlexaLanguageActivity extends BaseMenuActivity implements LanguageAdapter.OnItemClickHandler{
    private ActivitySetAlexaLanguageBinding mBinding;
    private ArrayList<Language> mLangList = new ArrayList<>();
    private PopupWindow langPopupWindow;
    private RecyclerView mRvLangList = null;
    private LanguageAdapter mAdapter;
    private View popLangView = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_set_alexa_language);
        loadLangList();
        showLangPopwindow();
        setView();
    }

    private void setUpLanguageRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        if (mRvLangList != null) {

            mRvLangList.setLayoutManager(new LinearLayoutManager(SetAlexaLanguageActivity.this));
            // 設置格線
            mRvLangList.addItemDecoration(new DividerItemDecoration(SetAlexaLanguageActivity.this, DividerItemDecoration.VERTICAL));
            // 將資料交給adapter
            // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
            mAdapter = new LanguageAdapter(SetAlexaLanguageActivity.this, mLangList, this);
            mRvLangList.setAdapter(mAdapter);
        }

    }

    /**
     * 显示popupWindow
     */
    private void showLangPopwindow() {
        //加载弹出框的布局
        popLangView = LayoutInflater.from(SetAlexaLanguageActivity.this).inflate(
                R.layout.view_pop_language, null);
        mRvLangList = popLangView.findViewById(R.id.rvLangList);
        setUpLanguageRecycler();
        langPopupWindow = new PopupWindow(popLangView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        langPopupWindow.setFocusable(true);// 取得焦点
        //注意  要是点击外部空白处弹框消息  那么必须给弹框设置一个背景色  不然是不起作用的
        langPopupWindow.setBackgroundDrawable(null);
        //点击外部消失
        langPopupWindow.setOutsideTouchable(true);
        //设置可以点击
        langPopupWindow.setTouchable(true);
        //进入退出的动画，指定刚才定义的style
        langPopupWindow.setAnimationStyle(R.style.popwindow_anim_style);

    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.btnLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //从底部显示
                langPopupWindow.showAtLocation(popLangView, Gravity.BOTTOM, 100, 0);
            }
        });

        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AlexaSetupFinishActivity.class);
                startActivity(intent);
            }
        });

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadLangList() {
        mLangList.add(new Language("English (United States)"));
        mLangList.add(new Language("English (Canada)"));
        mLangList.add(new Language("English (India)"));
        mLangList.add(new Language("English (Australla)"));
        mLangList.add(new Language("German"));
        mLangList.add(new Language("Japanese"));
        mLangList.add(new Language("French"));
        mLangList.add(new Language("Italian"));
        mLangList.add(new Language("Spanish"));
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.voice_assistance_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemClick(Language lang) {
        CommonUtils.toast(mContext, lang.getLang());
    }
}
