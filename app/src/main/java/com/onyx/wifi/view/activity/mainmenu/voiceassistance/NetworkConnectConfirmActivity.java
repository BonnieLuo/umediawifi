package com.onyx.wifi.view.activity.mainmenu.voiceassistance;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNotConnectedToHomeNetworkBinding;
import com.onyx.wifi.databinding.ActivityWirelessSettingBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.VoiceAssistantViewModel;


public class NetworkConnectConfirmActivity extends BaseMenuActivity {

    private ActivityNotConnectedToHomeNetworkBinding mBinding;
    private VoiceAssistantViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_not_connected_to_home_network);
        setViewModel();
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        if (DataManager.getInstance().getWirelessSetting().getWifiSameEnabled()) {
            mBinding.tvName1.setText(DataManager.getInstance().getWirelessSetting().getWireless2g().getSsid());
            mBinding.tvOR.setVisibility(View.INVISIBLE);
            mBinding.tvName2.setVisibility(View.INVISIBLE);
        } else {
            mBinding.tvName1.setText(DataManager.getInstance().getWirelessSetting().getWireless2g().getSsid());
            mBinding.tvOR.setVisibility(View.VISIBLE);
            mBinding.tvName2.setText(DataManager.getInstance().getWirelessInfo5gFronthaul().getSsid());
        }

        mBinding.btnConnectionConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.checkIsHomeNetwork();
            }
        });

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VoiceAssistanceActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(VoiceAssistantViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.VoiceAssistant.CHECK_IF_HOME_NETWORK) {
                        if (resultStatus.success) {  // in home network
                            Intent intent = new Intent(mContext, SetupAlexaActivity.class);
                            startActivity(intent);
                        } else {                     // not in home network
                            final ConfirmDialog confirmDialog = new ConfirmDialog();
                            confirmDialog.setTitle("Change Wifi");
                            confirmDialog.setContent("You are not connected to the home network. Please check Android \"Settings\" of your phone.");
                            confirmDialog.setPositiveButton("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    confirmDialog.dismiss();
                                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });
                            confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    confirmDialog.dismiss();
                                }
                            });
                            showDialog(confirmDialog);
                            //showMessageDialog("Get Audio Device", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }


    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.voice_assistance_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}

