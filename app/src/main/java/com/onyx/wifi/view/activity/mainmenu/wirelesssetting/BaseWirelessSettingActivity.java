package com.onyx.wifi.view.activity.mainmenu.wirelesssetting;

import android.support.v4.content.res.ResourcesCompat;
import android.text.InputType;
import android.widget.EditText;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;

public class BaseWirelessSettingActivity extends BaseMenuActivity {

    // 用途: 眼睛圖示按鈕的點擊事件, 設定顯示/不顯示 password
    protected void setPasswordVisible(EditText etPassword, boolean isVisible) {
        if (isVisible) {
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        // 上述用 input type 的方式更改文字的型態後, 字型會跑掉, 故重新設定
        etPassword.setTypeface(ResourcesCompat.getFont(mContext, R.font.gotham_medium));

        setMultiLineDisplay(etPassword);
    }

    // 設定讓文字可以多行顯示, 若字串太長, 系統會自動換行
    // 若直接在 layout 為 EditText 設定 inputType="textMultiLine" 屬性, 雖然文字可以多行顯示, 但會造成輸入時也可以輸入換行字元
    // 我們需要 "可以多行顯示, 但不可以輸入換行字元", 故參考下面文章的解法 (綠色勾勾的才是正解)
    // https://stackoverflow.com/questions/36338563/how-to-implement-multiline-edittext-with-actiondone-button-without-enter-button/41566987
    protected void setMultiLineDisplay(TextView textView) {
        // 傳入的參數宣告為 TextView 即可通用 TextView or EditText (因為 EditText 是繼承 TextView 來的, 也是一種 TextView)
        textView.setHorizontallyScrolling(false);
        textView.setMaxLines(Integer.MAX_VALUE);
    }
}
