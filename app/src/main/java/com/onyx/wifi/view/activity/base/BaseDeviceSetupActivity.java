package com.onyx.wifi.view.activity.base;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.mainmenu.troubleshooting.MoreHelpActivity;
import com.onyx.wifi.view.activity.mainmenu.troubleshooting.SupportActivity;
import com.onyx.wifi.view.activity.menubar.dashboard.DashboardActivity;
import com.onyx.wifi.view.activity.setup.AlternativeSetupActivity;
import com.onyx.wifi.view.activity.setup.BluetoothIsRequiredActivity;
import com.onyx.wifi.view.activity.setup.repeater.RepeaterSetupActivity;
import com.onyx.wifi.view.activity.setup.router.RouterSetupActivity;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;
import com.onyx.wifi.viewmodel.setup.DeviceSetupViewModel;

public class BaseDeviceSetupActivity extends BaseTitleActivity {

    private final int REQUEST_CODE_PERMISSION = 1;

    protected final int mGetProvisionStateMaxCount = 12;  // 嘗試 get provision state 最多幾次

    protected DeviceSetupViewModel mDeviceSetupViewModel;

    // 在開始 device setup 前會檢查 App 需要的權限是否都有了, 若沒有會顯示訊息告知使用者
    protected MessageDialog mLocationServiceMsgDialog;
    protected MessageDialog mBluetoothPermissionMsgDialog;
    protected MessageDialog mGrantPermissionMsgDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setViewModel();

        initExplanationMessageDialog();
    }

    private void setViewModel() {
        mDeviceSetupViewModel = ViewModelProviders.of(this).get(DeviceSetupViewModel.class);
    }

    private void initExplanationMessageDialog() {
        mLocationServiceMsgDialog = new MessageDialog();
        mLocationServiceMsgDialog.setTitle("Location Service");
        mLocationServiceMsgDialog.setContent(getString(R.string.location_service_explanation));
        mLocationServiceMsgDialog.setOkButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLocationServiceMsgDialog.dismiss();
                guideToSettingsOfLocation();
            }
        });

        mBluetoothPermissionMsgDialog = new MessageDialog();
        mBluetoothPermissionMsgDialog.setTitle("Permission");
        mBluetoothPermissionMsgDialog.setContent(getString(R.string.bluetooth_and_location_permission_explanation));
        mBluetoothPermissionMsgDialog.setOkButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBluetoothPermissionMsgDialog.dismiss();
                requestAllPermission();
            }
        });

        mGrantPermissionMsgDialog = new MessageDialog();
        mGrantPermissionMsgDialog.setTitle("Permission");
        mGrantPermissionMsgDialog.setContent(getString(R.string.permission_grant_guide));
        mGrantPermissionMsgDialog.setOkButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGrantPermissionMsgDialog.dismiss();
                guideToSettingsOfApp();
            }
        });
    }

    protected void setSupportClickMessage(TextView textView) {
        textView.setClickable(true);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SupportActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    protected boolean isAllPermissionGranted() {
        return isPermissionGranted(Manifest.permission.BLUETOOTH)
                && isPermissionGranted(Manifest.permission.BLUETOOTH_ADMIN)
                && isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    protected void requestAllPermission() {
        requestPermission(new String[]{
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
                    boolean anyPermissionNeedShowGuide = false;
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED && isNeedShowPermissionGuide(permissions[i])) {
                            anyPermissionNeedShowGuide = true;
                        }
                    }
                    if (anyPermissionNeedShowGuide) {
                        if (mGrantPermissionMsgDialog != null && !mGrantPermissionMsgDialog.isAdded()) {
                            showDialog(mGrantPermissionMsgDialog);
                        }
                    }
                }
                break;
        }
    }

    protected boolean isLocationEnabled() {
        LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (manager != null) {
            return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        return false;
    }

    protected void guideToSettingsOfLocation() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected final BroadcastReceiver mBleStateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        LogUtils.trace("Bluetooth is turned off.");
                        startActivity(new Intent(mContext, BluetoothIsRequiredActivity.class));
                        finish();
                        break;

                    case BluetoothAdapter.STATE_TURNING_OFF:
                    case BluetoothAdapter.STATE_ON:
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    protected void retryDeviceSetup() {
        Intent intent;
        switch (DataManager.getInstance().getSetupMode()) {
            case ROUTER:
                if (DataManager.getInstance().getSetupFlow() == AppConstants.SetupFlow.BLE) {
                    intent = new Intent(mContext, RouterSetupActivity.class);
                } else {
                    intent = new Intent(mContext, AlternativeSetupActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            case ROUTER_RESETUP:
                // resetup router 時只能透過 BLE 的方式
                intent = new Intent(mContext, RouterSetupActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            case REPEATER:
            default:
                if (DataManager.getInstance().getSetupFlow() == AppConstants.SetupFlow.BLE) {
                    intent = new Intent(mContext, RepeaterSetupActivity.class);
                } else {
                    intent = new Intent(mContext, AlternativeSetupActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    // 整個 setup 流程重來, 回到 setup 步驟說明頁面
    public void showErrorMessageAndRetry(String title, String message) {
        final MessageDialog messageDialog = new MessageDialog();
        messageDialog.setTitle(title);
        messageDialog.setContent(message);
        messageDialog.setOkButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeviceSetupViewModel.disconnectDevice();
                messageDialog.dismiss();
                retryDeviceSetup();
                finish();
            }
        });
        showDialog(messageDialog);
    }

    public void setDeviceImage(SetupDevice setupDevice, ImageView imgDevice) {
        // 根據 device type 判斷要顯示哪一張圖片
        if (setupDevice != null) {
            switch (setupDevice.getDeviceType()) {
                case DESKTOP:
                    imgDevice.setImageResource(R.drawable.pic_hrn22ac);
                    break;

                case PLUG:
                    imgDevice.setImageResource(R.drawable.pic_hex22acp);
                    break;

                case TOWER:
                default:
                    imgDevice.setImageResource(R.drawable.pic_hna22ac);
                    break;
            }
        }
    }

    protected void getProvisionToken() {
        // 事先向 cloud 取得 device provision token (BLE/WIFI setup 皆需要此 token), 再進行後續的掃描藍牙、連線等流程
        // 因如果 BLE setup 失敗可以走 WIFI setup 流程, 而 WIFI setup 會造成手機暫時連不上 Internet, 到時候會無法打 cloud API, 所以現在先打
        mDeviceSetupViewModel.getProvisionToken(true);
    }

    @Override
    protected void startDashboardActivity(Activity activity) {
        Intent intent = new Intent(mContext, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    protected void startMoreHelpActivity() {
        Intent intent = new Intent(mContext, MoreHelpActivity.class);
        intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, true);
        startActivity(intent);
    }
}
