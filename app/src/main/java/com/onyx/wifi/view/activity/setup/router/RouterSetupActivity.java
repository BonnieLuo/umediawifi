package com.onyx.wifi.view.activity.setup.router;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityRouterSetupBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.activity.setup.DeviceSetupSearchActivity;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.fragment.setup.WaitForLedFragment;
import com.onyx.wifi.view.fragment.setup.router.ConnectToModemFragment;
import com.onyx.wifi.view.fragment.setup.router.FindTheButtonFragment;
import com.onyx.wifi.view.fragment.setup.router.PlugInModemFragment;
import com.onyx.wifi.view.fragment.setup.router.UnplugModemFragment;
import com.onyx.wifi.view.fragment.setup.router.WhatYouNeedFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;

public class RouterSetupActivity extends BaseDeviceSetupActivity implements DeviceSetupListener {

    private WhatYouNeedFragment mWhatYouNeedFragment;
    private UnplugModemFragment mUnplugModemFragment;
    private ConnectToModemFragment mConnectToModemFragment;
    private PlugInModemFragment mPlugInModemFragment;
    private WaitForLedFragment mWaitForLedFragment;
    private FindTheButtonFragment mFindTheButtonFragment;

    private ActivityRouterSetupBinding mBinding;

    private boolean mIsResetup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsResetup = getIntent().getBooleanExtra(AppConstants.EXTRA_RESETUP_ROUTER, false);
        LogUtils.trace("is resetup router = " + mIsResetup);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setSetupFlow(AppConstants.SetupFlow.BLE);
        dataManager.setSetupMode((mIsResetup) ? AppConstants.SetupMode.ROUTER_RESETUP : AppConstants.SetupMode.ROUTER);
        dataManager.setSetupFailCount(0);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_router_setup);
        setView();
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mWhatYouNeedFragment = (WhatYouNeedFragment) mFragmentManager.findFragmentByTag(WhatYouNeedFragment.class.getName());
            mUnplugModemFragment = (UnplugModemFragment) mFragmentManager.findFragmentByTag(UnplugModemFragment.class.getName());
            mConnectToModemFragment = (ConnectToModemFragment) mFragmentManager.findFragmentByTag(ConnectToModemFragment.class.getName());
            mPlugInModemFragment = (PlugInModemFragment) mFragmentManager.findFragmentByTag(PlugInModemFragment.class.getName());
            mWaitForLedFragment = (WaitForLedFragment) mFragmentManager.findFragmentByTag(WaitForLedFragment.class.getName());
            mFindTheButtonFragment = (FindTheButtonFragment) mFragmentManager.findFragmentByTag(FindTheButtonFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            // 顯示第一頁, 隱藏其他頁
            if (mUnplugModemFragment != null) {
                fragmentTransaction.hide(mUnplugModemFragment);
            }
            if (mConnectToModemFragment != null) {
                fragmentTransaction.hide(mConnectToModemFragment);
            }
            if (mPlugInModemFragment != null) {
                fragmentTransaction.hide(mPlugInModemFragment);
            }
            if (mWaitForLedFragment != null) {
                fragmentTransaction.hide(mWaitForLedFragment);
            }
            if (mFindTheButtonFragment != null) {
                fragmentTransaction.hide(mFindTheButtonFragment);
            }
            fragmentTransaction.show(mWhatYouNeedFragment);
            fragmentTransaction.commit();
        } else {
            mWhatYouNeedFragment = new WhatYouNeedFragment();
            addFragmentAsFirstPage(R.id.flContainer, mWhatYouNeedFragment, WhatYouNeedFragment.class.getName());
        }
    }

    private void setView() {
        setTitleBar();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.initial_setup_title);
        setFirstPageLeftButton();
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        setLoadingObserve(mDeviceSetupViewModel, this, mBinding.loading);
        mDeviceSetupViewModel.setSetupMode(DataManager.getInstance().getSetupMode());
        mDeviceSetupViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_PROVISION_TOKEN) {
                        if (resultStatus.success) {
                            startSearchDeviceActivity();
                        } else {
                            showMessageDialog("Get Provision Token", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        // 有兩個地方會呼叫該 function: (1) 標題列的 back 按鈕, (2) Android 系統的 back 鍵.
        // 此流程的第一頁不會加入 back stack, 第二頁開始才會加入, 例如: 第二頁的 back stack entry count = 1.
        // 如果不是在流程的第一頁的話, 標題列會有 back 按鈕, 按 Android back 鍵也可以回到上一頁.
        // 但如果是在第一頁, 會有兩種情況:
        //  (1) 第一次 setup, 標題列的左上角按鈕功能是 sign out, 且按 Android back 鍵不能結束頁面 (意即不能跳過此流程)
        //  (2) resetup, 標題列的左上角按鈕功能是回到 dashboard 頁面, 且 Android back 鍵功能可以作用

        if (!mIsResetup && mFragmentManager.getBackStackEntryCount() == 0) {
            return;
        }

        super.onBackPressed();

        if (mFragmentManager.getBackStackEntryCount() == 0) {
            // 回到上一頁之後再判斷一次 back stack entry count, 若等於 0 代表已回到了第一頁
            setFirstPageLeftButton();
        }
    }

    @Override
    public void onContinue(int nextStep) {
        LogUtils.trace("next step = " + nextStep);
        switch (nextStep) {
            case Code.Action.DeviceSetup.MORE_ABOUT_MODEM:
                startActivity(new Intent(mContext, ModemBasicActivity.class));
                break;

            case Code.Action.DeviceSetup.MORE_HELP:
                startMoreHelpActivity();
                break;

            case Code.Action.DeviceSetup.UNPLUG_MODEM:
                mUnplugModemFragment = new UnplugModemFragment();
                addFragment(R.id.flContainer, mUnplugModemFragment, UnplugModemFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                setLeftButtonBack();
                break;

            case Code.Action.DeviceSetup.CONNECT_TO_MODEM:
                mConnectToModemFragment = new ConnectToModemFragment();
                addFragment(R.id.flContainer, mConnectToModemFragment, ConnectToModemFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                setLeftButtonBack();
                break;

            case Code.Action.DeviceSetup.PLUG_IN_MODEM:
                mPlugInModemFragment = new PlugInModemFragment();
                addFragment(R.id.flContainer, mPlugInModemFragment, PlugInModemFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                setLeftButtonBack();
                break;

            case Code.Action.DeviceSetup.WAIT_FOR_LED:
                mWaitForLedFragment = new WaitForLedFragment();
                addFragment(R.id.flContainer, mWaitForLedFragment, WaitForLedFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                setLeftButtonBack();
                break;

            case Code.Action.DeviceSetup.FIND_THE_BUTTON:
                mFindTheButtonFragment = new FindTheButtonFragment();
                addFragment(R.id.flContainer, mFindTheButtonFragment, FindTheButtonFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                setLeftButtonBack();
                break;

            case Code.Action.DeviceSetup.FIND_THE_BUTTON_GOT_IT:
                onBackPressed();
                break;

            case Code.Action.DeviceSetup.SEARCH_DEVICE:
                if (isAllPermissionGranted()) {
                    LogUtils.trace("all permission granted");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // Android 6.0 以上需開啟手機的 GPS 定位, 否則收不到藍牙掃描的結果
                        if (isLocationEnabled()) {
                            getProvisionToken();
                        } else {
                            if (mLocationServiceMsgDialog != null && !mLocationServiceMsgDialog.isAdded()) {
                                showDialog(mLocationServiceMsgDialog);
                            }
                        }
                    } else {
                        getProvisionToken();
                    }
                } else {
                    if (mBluetoothPermissionMsgDialog != null && !mBluetoothPermissionMsgDialog.isAdded()) {
                        showDialog(mBluetoothPermissionMsgDialog);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void setSupportView(TextView textView) {
        setSupportClickMessage(textView);
    }

    private void startSearchDeviceActivity() {
        startActivity(new Intent(mContext, DeviceSetupSearchActivity.class));
        // 這裡不用呼叫 finish()
        // 因 device setup 頁面包含多個步驟說明, 到最後一步並 continue 進入後續流程 (藍牙掃描, 連接裝置...) 之後,
        // 若過程中有失敗需要重來時, 都需要回到這頁步驟說明的最後一步(wait for LED), 故進入後續流程時不需要將這頁關閉
    }

    private void setFirstPageLeftButton() {
        if (mIsResetup) {
            // resetup 時, 流程第一頁左上角的按鈕是 home 圖示, 按下去回到 dashboard 頁面
            mBinding.titleBar.setLeftButton(R.drawable.ic_home, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startDashboardActivity(RouterSetupActivity.this);
                    finish();
                }
            });

            // 左上角的 sign out 按鈕設定為不可見
            mBinding.titleBar.setLeftButtonSignOutInvisible();
        } else {
            // 第一次 setup 時, 流程第一頁左上角是 sign out 按鈕
            mBinding.titleBar.setLeftButtonSignOut(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ConfirmDialog confirmDialog = new ConfirmDialog();
                    confirmDialog.setTitle("Sign out of UMEDIA");
                    confirmDialog.setContent("Are you sure you want to sign out?");
                    confirmDialog.setPositiveButton("Sign out", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAccountManager.signOut();
                            startLoginHomeActivity(RouterSetupActivity.this);
                            finish();
                        }
                    });
                    confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            confirmDialog.dismiss();
                        }
                    });
                    showDialog(confirmDialog);
                }
            });

            // 原本的左上角按鈕設定為不可見
            mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        }
    }

    private void setLeftButtonBack() {
        // 若非流程第一頁, 不論是第一次 setup 或是 resetup, 左上角的按鈕都是 back 圖示, 按下去是返回上一頁
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // 左上角的 sign out 按鈕設定為不可見
        mBinding.titleBar.setLeftButtonSignOutInvisible();
    }
}
