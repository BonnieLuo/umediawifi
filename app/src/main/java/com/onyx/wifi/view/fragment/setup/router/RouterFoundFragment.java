package com.onyx.wifi.view.fragment.setup.router;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentRouterFoundBinding;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;
import com.onyx.wifi.viewmodel.setup.BleScanViewModel;

public class RouterFoundFragment extends BaseFragment {

    private FragmentRouterFoundBinding mBinding;
    private BleScanViewModel mViewModel;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_router_found, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.setSelectedIndex(0);
                mEventListener.onContinue(Code.Action.DeviceSetup.SETUP_ROUTER);
            }
        });
        mEventListener.setSupportView(mBinding.tvSupport);
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(BleScanViewModel.class);
    }

    private void setData() {
        if (mViewModel.getDeviceList() != null && mViewModel.getDeviceList().size() > 0) {
            SetupDevice setupDevice = mViewModel.getDeviceList().get(0);
            mBinding.tvDeviceName.setText(setupDevice.getName());
            if (mActivity instanceof BaseDeviceSetupActivity) {
                ((BaseDeviceSetupActivity) mActivity).setDeviceImage(setupDevice, mBinding.imgDevice);
            }
        }
    }
}
