package com.onyx.wifi.view.adapter.advancedwireless;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRule;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRuleModel;
import com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless.PortForwardingRuleActivity;

import java.util.ArrayList;
import java.util.List;

public class PortForwardingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<PortForwardingRule> mRuleList = new ArrayList<>();

    public void setRuleModelList(List<PortForwardingRuleModel> ruleModelList) {
        mRuleList.clear();

        for (PortForwardingRuleModel model : ruleModelList) {
            PortForwardingRule rule = new PortForwardingRule(model);
            mRuleList.add(rule);
        }

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_port_forwarding_list_item, parent, false);

        RuleViewHolder viewHolder = new RuleViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        PortForwardingRule rule = mRuleList.get(position);

        RuleViewHolder ruleViewHolder = (RuleViewHolder) viewHolder;
        ruleViewHolder.setRule(rule);
    }

    @Override
    public int getItemCount() {
        return mRuleList.size();
    }

    class RuleViewHolder extends RecyclerView.ViewHolder {

        PortForwardingRule mRule;

        TextView mRuleNameTextView;

        TextView mClientIpTextView;

        TextView mRuleTextView;

        TextView mEnableTextView;

        ImageView mArrowImageView;

        public RuleViewHolder(@NonNull View itemView) {
            super(itemView);

            mRuleNameTextView = itemView.findViewById(R.id.ruleNameTextView);

            mClientIpTextView = itemView.findViewById(R.id.clientIpTextView);

            mRuleTextView = itemView.findViewById(R.id.ruleTextView);

            mEnableTextView = itemView.findViewById(R.id.enableTextView);

            mArrowImageView = itemView.findViewById(R.id.arrowImageView);
            mArrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataManager dataManager = DataManager.getInstance();
                    dataManager.setPortForwardingRule(mRule);

                    Context context = itemView.getContext();
                    context.startActivity(new Intent(context, PortForwardingRuleActivity.class));
                }
            });
        }

        public void setRule(PortForwardingRule rule) {
            mRule = rule;

            String ruleName = mRule.getName();
            mRuleNameTextView.setText(ruleName);

            String ip = mRule.getIp();
            mClientIpTextView.setText(ip);

            String ruleDescription = mRule.getRuleDescription();
            mRuleTextView.setText(ruleDescription);

            if (mRule.isEnabled()) {
                mEnableTextView.setText("Enabled");
            } else {
                mEnableTextView.setText("Disabled");
            }
        }

    }
}
