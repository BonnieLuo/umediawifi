package com.onyx.wifi.view.activity.mainmenu.voiceassistance;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAlexaSetupFinishedBinding;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.NoUnderlineSpan;
import com.onyx.wifi.view.dialog.WarningDialog;

public class AlexaSetupFinishActivity extends BaseMenuActivity {
    private ActivityAlexaSetupFinishedBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_alexa_setup_finished);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.btnFinishSetup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VoiceAssistanceActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // Set hyperlink of forgot password
        SpannableString msgDownloadApp = new SpannableString("To learn more about Alexa and access additional features, download the Amazon Alexa App.");
        String msgApp = "Amazon Alexa App";
        int startIndex = msgDownloadApp.toString().indexOf(msgApp);
        int endIndex = startIndex + msgApp.length();
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                introduceToGooglePlay();
            }
        };
        msgDownloadApp.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgDownloadApp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.gray_676767)), startIndex, endIndex, 0);
        mBinding.tvComment.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvComment.setText(msgDownloadApp, TextView.BufferType.SPANNABLE);
        mBinding.tvComment.setHighlightColor(Color.TRANSPARENT);
    }

    private void introduceToGooglePlay() {
        String appPackageName = "com.amazon.dee.app";
        Intent intent = null;
        try
        {
            // Open app with Google Play app
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            startActivity(intent);
        }
        catch (ActivityNotFoundException anfe)
        {
            // Open Google Play website
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
            startActivity(intent);
        }

    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.voice_assistance_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
