package com.onyx.wifi.view.adapter.member.client;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.SectionViewHolder;
import com.onyx.wifi.view.interfaces.member.OnClientTypeClickListener;

import java.util.ArrayList;
import java.util.List;

public class ClientTypeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private String mSelectedClientType;

    private List<ListItem> mListItems;

    private OnClientTypeClickListener mOnClientTypeClickListener;

    public ClientTypeListAdapter(Context context, String selectedClientType, OnClientTypeClickListener listener) {
        mContext = context;

        mSelectedClientType = selectedClientType;
        
        mListItems = setupListItems();

        mOnClientTypeClickListener = listener;
    }

    public void setSelectedClientType(String selectedClientType) {
        mSelectedClientType = selectedClientType;

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<IconViewType, Object> listItem = mListItems.get(position);

        IconViewType viewType = listItem.getType();

        switch (viewType) {
            case SECTION:
                return IconViewType.SECTION.ordinal();
            case COPYRIGHT:
                return IconViewType.COPYRIGHT.ordinal();
            case ITEM:
            default:
                return IconViewType.ITEM.ordinal();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == IconViewType.SECTION.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_section, parent, false);

            SectionViewHolder viewHolder = new SectionViewHolder(view);
            return viewHolder;
        }

        if (viewType == IconViewType.COPYRIGHT.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_client_icon_copyright, parent, false);

            CopyrightViewHolder viewHolder = new CopyrightViewHolder(view);
            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_device_icon_grid, parent, false);

        ClientIconGridViewHolder viewHolder = new ClientIconGridViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<IconViewType, Object> listItem = mListItems.get(position);

        IconViewType viewType = listItem.getType();

        switch (viewType) {
            case SECTION:
                setSection(listItem, viewHolder);

                break;
            case ITEM:
                setItem(listItem, viewHolder);

                break;
        }
    }

    private void setSection(ListItem listItem, RecyclerView.ViewHolder viewHolder){
        String title = (String) listItem.getItem();

        if (title != null) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) viewHolder;

            sectionViewHolder.setTitle(title);
        }
    }

    private void setItem(ListItem listItem, RecyclerView.ViewHolder viewHolder){
        String[] clientTypeList = (String[]) listItem.getItem();

        if (clientTypeList != null) {
            ClientIconGridViewHolder clientIconGridViewHolder = (ClientIconGridViewHolder) viewHolder;

            clientIconGridViewHolder.setData(clientTypeList, mSelectedClientType, mOnClientTypeClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    private List<ListItem> setupListItems() {
        List<ListItem> listItems = new ArrayList<>();

        // Mobile
        ListItem sectionMobile = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_mobile));
        listItems.add(sectionMobile);

        ListItem itemMobile = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.mobile));
        listItems.add(itemMobile);

        // Audio & Video
        ListItem sectionAudioAndVideo = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_audio_and_video));
        listItems.add(sectionAudioAndVideo);

        ListItem itemAudioAndVideo = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.audio_and_video));
        listItems.add(itemAudioAndVideo);

        //Home & Office
        ListItem sectionHomeAndOffice = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_home_and_office));
        listItems.add(sectionHomeAndOffice);

        ListItem itemHomeAndOffice = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.home_and_office));
        listItems.add(itemHomeAndOffice);

        //Smart Home
        ListItem sectionSmartHome = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_smart_home));
        listItems.add(sectionSmartHome);

        ListItem itemSmartHome = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.smart_home));
        listItems.add(itemSmartHome);

        //Network
        ListItem sectionNetwork = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_network));
        listItems.add(sectionNetwork);

        ListItem itemNetwork = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.network));
        listItems.add(itemNetwork);

        //Server
        ListItem sectionServer = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_server));
        listItems.add(sectionServer);

        ListItem itemServer = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.server));
        listItems.add(itemServer);

        //Engineering
        ListItem sectionEngineering = new ListItem<>(IconViewType.SECTION, mContext.getString(R.string.device_icon_engineering));
        listItems.add(sectionEngineering);

        ListItem itemEngineering = new ListItem<>(IconViewType.ITEM, mContext.getResources().getStringArray(R.array.engineering));
        listItems.add(itemEngineering);

        ListItem copyright = new ListItem<>(IconViewType.COPYRIGHT, null);
        listItems.add(copyright);

        return listItems;
    }

    class CopyrightViewHolder extends RecyclerView.ViewHolder {

        public CopyrightViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
