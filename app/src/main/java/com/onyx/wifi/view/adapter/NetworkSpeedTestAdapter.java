package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.NodeSpeedTestInfo;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;

public class NetworkSpeedTestAdapter extends RecyclerView.Adapter<NetworkSpeedTestAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(NodeSpeedTestInfo nodeSpeedTestInfo);
    }

    private Context mContext;
    public static ArrayList<NodeSpeedTestInfo> mData = new ArrayList<NodeSpeedTestInfo>(); // "public static": global data for related pages(activities)
    //
    // 2. 宣告interface
    //
    private NetworkSpeedTestAdapter.OnItemClickHandler mClickHandler;

    public NetworkSpeedTestAdapter(
            Context context,
            ArrayList<NodeSpeedTestInfo> data,
            NetworkSpeedTestAdapter.OnItemClickHandler clickHandler) {
        mContext = context;
        mData = data;
        mClickHandler = clickHandler;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private ImageView imgUMEDIADevice;
        private ImageView imgInternetStatus;
        private TextView tvName;
        private TextView tvNodeSpeedTestInfo;

        private GifImageView pbRunning;
        private TextView tvRerunTest;

        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            imgUMEDIADevice = (ImageView) itemView.findViewById(R.id.imgUMEDIADevice);// 從view(list item)中取得TextView實體
            imgInternetStatus = (ImageView) itemView.findViewById(R.id.imgInternetStatus);

            tvName = (TextView) itemView.findViewById(R.id.tvName);

            tvNodeSpeedTestInfo = (TextView) itemView.findViewById(R.id.tvNodeSpeedTestInfo);

            pbRunning = (GifImageView) itemView.findViewById(R.id.pbRunning);
            tvRerunTest = (TextView) itemView.findViewById(R.id.tvUpdating);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NodeSpeedTestInfo umediafwStatus = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(umediafwStatus);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // binding with layout
    @Override // required
    public NetworkSpeedTestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_network_speed_list_item, viewGroup, false); //取得list的view
        return new NetworkSpeedTestAdapter.ViewHolder(view);
    }

    // set data (according to the status of each device)
    @Override // required
    public void onBindViewHolder(@NonNull NetworkSpeedTestAdapter.ViewHolder viewHolder, int i) {
        //
        // device type
        //
        if (mData.get(i).getDeviceInfo().getDeviceType() == AppConstants.DeviceType.DESKTOP) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hrn22ac); // image needs to be update
        }
        if (mData.get(i).getDeviceInfo().getDeviceType() == AppConstants.DeviceType.PLUG) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hex22acp); // image needs to be update
        }
        if (mData.get(i).getDeviceInfo().getDeviceType() == AppConstants.DeviceType.TOWER) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
        }

        //
        //  Name
        //
        viewHolder.tvName.setText(mData.get(i).getDeviceInfo().getDeviceLabel());


        //
        // Network Speed Test info
        //

    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public void setDevice(ArrayList<NodeSpeedTestInfo> data) {
        //通过System.identityHashCode(object)方法来间接的获取内存地址；
        //创建出来的对象，只要没被销毁，内存地址始终不变。
        //Adapter绑定的数据源集合要为同一个集合，notifyDataSetChanged()方法才有效，否则需要重新设置数据源
        //mData = data; <-錯誤寫法，address可能會變更
        mData.clear();
        mData.addAll(data);
        //LogUtils.trace("NotifyMsg", "[Adapter]:" + data.size() + " " + mData.size());
        for (NodeSpeedTestInfo umediafwStatus : mData) {
            //LogUtils.trace("NotifyMsg",umediafwStatus.toString());
            //System.out.println(umediafwStatus);
        }
        //LogUtils.trace("status", "address: " + System.identityHashCode(mData));
        notifyDataSetChanged();
    }

}
