package com.onyx.wifi.view.adapter.member.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Member;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.SectionViewHolder;
import com.onyx.wifi.view.interfaces.member.OnMemberListEventListener;

import java.util.ArrayList;
import java.util.List;

public class MemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnMemberListEventListener {

    private List<ListItem> mListItems = new ArrayList<ListItem>();

    private OnMemberListEventListener mEventListener;

    public void setListItems(List<ListItem> listItems) {
        mListItems = listItems;

        notifyDataSetChanged();
    }

    public void setMemberListEventListener(OnMemberListEventListener listener) {
        mEventListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<MemberListItemType, Object> listItem = mListItems.get(position);

        MemberListItemType type = listItem.getType();

        switch (type) {
            case SECTION:
                return MemberListItemType.SECTION.ordinal();
            case ITEM:
            default:
                return MemberListItemType.ITEM.ordinal();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == MemberListItemType.SECTION.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_section, parent, false);

            SectionViewHolder viewHolder = new SectionViewHolder(view);
            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_member_list_item, parent, false);

        MemberViewHolder viewHolder = new MemberViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<MemberListItemType, Object> listItem = mListItems.get(position);

        MemberListItemType type = listItem.getType();

        switch (type) {
            case SECTION:
                setSection(listItem, viewHolder);

                break;
            case ITEM:
            default:
                setItem(listItem, viewHolder);

                break;
        }
    }

    private void setSection(ListItem listItem, RecyclerView.ViewHolder viewHolder){
        String title = (String) listItem.getItem();

        if (title != null) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) viewHolder;

            sectionViewHolder.setTitle(title);
        }
    }

    private void setItem(ListItem listItem, RecyclerView.ViewHolder viewHolder){
        Member member = (Member) listItem.getItem();

        if (member != null) {
            MemberViewHolder membeViewHolder = (MemberViewHolder) viewHolder;

            membeViewHolder.setMember(member);
            membeViewHolder.setMemberListEventListener(this);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    @Override
    public void onManageMemberClick(Member member) {
        if (mEventListener != null) {
            mEventListener.onManageMemberClick(member);
        }
    }

    @Override
    public void onMemberClick(Member member) {
        if (mEventListener != null) {
            mEventListener.onMemberClick(member);
        }
    }

    @Override
    public void onPauseMemberClick(Member member) {
        if (mEventListener != null) {
            mEventListener.onPauseMemberClick(member);
        }

        notifyDataSetChanged();
    }

    @Override
    public void onClientClick(ClientModel clientModel) {
        if (mEventListener != null) {
            mEventListener.onClientClick(clientModel);
        }
    }
}