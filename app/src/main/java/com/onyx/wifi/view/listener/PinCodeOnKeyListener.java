package com.onyx.wifi.view.listener;

import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class PinCodeOnKeyListener implements View.OnKeyListener {
    private EditText[] editTexts;
    private int currentIndex;

    public PinCodeOnKeyListener(EditText[] editTexts, int currentIndex) {
        this.currentIndex = currentIndex;
        this.editTexts = editTexts;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (editTexts[currentIndex].getText().toString().isEmpty() && currentIndex != 0) {
                editTexts[currentIndex - 1].requestFocus();
            }
        }
        return false;
    }
}
