package com.onyx.wifi.view.activity.menubar.member.schedule;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMemberInternetScheduleBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.ScheduleConfigurationModel;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.member.schedule.InternetScheduleListAdapter;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.dialog.InfoDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.schedule.ScheduleConfigurationListViewModel;

import java.lang.reflect.Type;
import java.util.List;

public class InternetScheduleListActivity extends BaseMenuActivity {

    private ActivityMemberInternetScheduleBinding mBinding;

    private ScheduleConfigurationListViewModel mViewModel;

    private InternetScheduleListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_internet_schedule);

        mAdapter = new InternetScheduleListAdapter();

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            DataManager dataManager = DataManager.getInstance();
            dataManager.setScheduleConfiguration(null);

            FamilyMember member = dataManager.getManageMember();
            String memberId = member.getId();
            mViewModel.getMemberScheduleList(memberId);
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        mBinding.timezoneInfoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoDialog infoDialog = new InfoDialog();
                String title = mContext.getString(R.string.internet_schedule_timezone);
                infoDialog.setTitle(title);

                String content = mContext.getString(R.string.internet_schedule_timezone_info);
                infoDialog.setContent(content);

                BaseActivity baseActivity = (BaseActivity) mContext;
                baseActivity.showDialog(infoDialog);
            }
        });

        // TODO: Timezone
        DataManager dataManager = DataManager.getInstance();
        String currentTimezone = dataManager.getDeviceInfo().getCity();
        mBinding.timezoneTextView.setText(currentTimezone);


        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Manage Schedule");

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setRightButton(R.drawable.ic_add_rule, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InternetScheduleListActivity.this, InternetScheduleActivity.class);
                startActivity(intent);
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ScheduleConfigurationListViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_SCHEDULE_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Schedule List", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            setData(json);
                        }
                    }
                }
            }
        });
    }

    private void setData(JsonObject json){
        JsonArray scheduleArray = json.getAsJsonArray("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<List<ScheduleConfigurationModel>>(){}.getType();
        List<ScheduleConfigurationModel> scheduleListList =  gson.fromJson(scheduleArray.toString(), listType);
        mAdapter.setScheduleListList(scheduleListList);
    }
}
