package com.onyx.wifi.view.activity.mainmenu.accountsetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAccountSettingBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.CustomEditText;
import com.onyx.wifi.view.customized.NoUnderlineSpan;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.WarningDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.AccountSettingViewModel;

public class AccountSettingActivity extends BaseMenuActivity implements CustomEditText.DrawableRightListener {

    private ActivityAccountSettingBinding mBinding;
    private AccountSettingViewModel mViewModel;
    View view_accountSetting = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //mBinding = DataBindingUtil.setContentView(this, R.layout.activity_account_setting);
        LayoutInflater inflater = LayoutInflater.from(this);
        view_accountSetting = inflater.inflate(R.layout.activity_account_setting, null);

        //
        // 透過View來取得binding (為了解決Custom EditText點擊屏幕應該取消焦點和關掉鍵盤的問題)
        //
        mBinding = DataBindingUtil.bind(view_accountSetting);
        setContentView(view_accountSetting);
        setViewModel();
        setView(); // the initial value of UI is set in onResume (setData())
    }

    @Override
    protected void onResume() { // being at foreground
        super.onResume();
        setData();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(AccountSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.UPDATE_USER_INFO) {
                        if (resultStatus.success) {
                            CommonUtils.toast(mContext, getString(R.string.msg_update_user_info_success));
                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                        } else {
                            showMessageDialog("Update User Info", resultStatus.errorMsg);
                        }
                    }

                    if (resultStatus.actionCode == Code.Action.UserAccount.DELETE_USER) {
                        if (resultStatus.success) {
                            CommonUtils.toast(mContext, getString(R.string.msg_delete_user_success));
                            mAccountManager.signOut();
                            startLoginHomeActivity(AccountSettingActivity.this);
                            finish();
                        } else {
                            showMessageDialog("Delete User", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void checkNameDataFilled() {
        if (mViewModel.isNameDataFilled() && mViewModel.isNameChanged()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void hideKeyboardAndClearCustomEditTextFocus(View v) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        mBinding.tilFirstName.setFocusable(false);
        mBinding.tilFirstName.setFocusableInTouchMode(false);
        mBinding.etFirstName.setNotEditable();
        mBinding.etFirstName.setLongClickable(false);

        mBinding.tilLastName.setFocusable(false);
        mBinding.tilLastName.setFocusableInTouchMode(false);
        mBinding.etLastName.setNotEditable();
        mBinding.etLastName.setLongClickable(false);

    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        //
        // onTouch點擊Custom EditText外一次就可以解決，onClick點擊Custom EditText外第二次才會收起鍵盤和取消Custom EditText的focus
        //
        view_accountSetting.findViewById(R.id.accountSettingRootView).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        switch (v.getId()) {
                            case R.id.accountSettingRootView:
                                hideKeyboardAndClearCustomEditTextFocus(v);
                                break;
                        }
                        break;
                }
                return false;
            }
        });

        // check if it is social sign in, if yes, hide some views not going to be shown
        if (mAccountManager.isSocialAccount()) {
            mBinding.group.setVisibility(View.INVISIBLE);
        }

        mBinding.etFirstName.setDrawableRightListener(this);
        mBinding.etLastName.setDrawableRightListener(this);

        mBinding.etFirstName.setLongClickable(false); // to solve the issue that long click shall not display any response before clicking the pen
        mBinding.etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Set the user input to mAccountSettingUser in ViewModel
                mViewModel.getAccountSettingUser().setFirstName(editable.toString().trim());
                // Set first name value to Share Preference through mAccountSettingUser
                checkNameDataFilled();
            }
        });

        mBinding.etLastName.setLongClickable(false);
        mBinding.etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Set the user input to mAccountSettingUser in ViewModel
                mViewModel.getAccountSettingUser().setLastName(editable.toString().trim());
                // Set last name value to Share Preference through mAccountSettingUser
                checkNameDataFilled();
            }
        });

        //mBinding.btnEditEmail.setOnClickListener(new View.OnClickListener() {
        if (!mAccountManager.isSocialAccount()) {
            mBinding.clEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ChangeEmailActivity.class);
                    startActivity(intent);
                    //finish(); // Don't finish this main page here
                }
            });

            //mBinding.btnEditPassword.setOnClickListener(new View.OnClickListener() {
            mBinding.clPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ChangePasswordActivity.class);
                    startActivity(intent);
                    //finish();
                }
            });
        }

        mBinding.btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ConfirmDialog confirmDialog = new ConfirmDialog();
                confirmDialog.setTitle("Sign out of UMEDIA");
                confirmDialog.setContent("Are you sure you want to sign out?");
                confirmDialog.setPositiveButton("Sign out", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAccountManager.signOut();
                        startLoginHomeActivity(AccountSettingActivity.this);
                        finish();
                    }
                });
                confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmDialog.dismiss();
                    }
                });
                showDialog(confirmDialog);
            }
        });

        // Set hyperlink of forgot password
        SpannableString msgDeleteUser = new SpannableString(getString(R.string.terminate_account));
        String msgTerminateAccount = "Want to terminate your account?";
        int startIndex = msgDeleteUser.toString().indexOf(msgTerminateAccount);
        int endIndex = startIndex + msgTerminateAccount.length();
        NoUnderlineSpan clickableSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
                final ConfirmDialog confirmDialog = new ConfirmDialog();
                confirmDialog.setTitle("Terminate account");
                confirmDialog.setContent("Are you sure you want to terminate your UMEDIA account?");
                confirmDialog.setPositiveButton("Terminate", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmDialog.dismiss();
                        mViewModel.deleteUser();
                    }
                });
                confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmDialog.dismiss();
                    }
                });
                showDialog(confirmDialog);
            }
        };
        msgDeleteUser.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgDeleteUser.setSpan(new ForegroundColorSpan(Color.RED), startIndex, endIndex, 0);
        mBinding.tvTerminate.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvTerminate.setText(msgDeleteUser, TextView.BufferType.SPANNABLE);
        mBinding.tvTerminate.setHighlightColor(Color.TRANSPARENT);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.account_setting_title);
//        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyboardAndClearCustomEditTextFocus(v);
                    mViewModel.updateUserInfo();
                }
            });

    }

    private void setData() {
        mBinding.etFirstName.setText(CommonUtils.getSharedPrefData(AppConstants.KEY_FIRST_NAME));
        mViewModel.getAccountSettingUser().setFirstName(CommonUtils.getSharedPrefData(AppConstants.KEY_FIRST_NAME));
        mBinding.etLastName.setText(CommonUtils.getSharedPrefData(AppConstants.KEY_LAST_NAME));
        mViewModel.getAccountSettingUser().setLastName(CommonUtils.getSharedPrefData(AppConstants.KEY_LAST_NAME));
        mBinding.etEditEmail.setText(CommonUtils.getSharedPrefData(AppConstants.KEY_EMAIL));
        mViewModel.getAccountSettingUser().setEmail(CommonUtils.getSharedPrefData(AppConstants.KEY_EMAIL));

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onDrawableRightClick(View view) {
        switch (view.getId()) {
            case R.id.etFirstName:
                mBinding.etFirstName.setEditable();
                mBinding.etFirstName.setLongClickable(true);

                mBinding.tilLastName.setFocusable(false);
                mBinding.tilLastName.setFocusableInTouchMode(false);
                mBinding.etLastName.setNotEditable();
                mBinding.etLastName.setLongClickable(false);
                //CommonUtils.toast(mContext, "Please input first name.");
                break;
            case R.id.etLastName:
                mBinding.tilFirstName.setFocusable(false);
                mBinding.tilFirstName.setFocusableInTouchMode(false);
                mBinding.etFirstName.setNotEditable();
                mBinding.etFirstName.setLongClickable(false);

                mBinding.etLastName.setEditable();
                mBinding.etLastName.setLongClickable(true);
                //CommonUtils.toast(mContext, "Please input last name.");
                break;
        }
    }
}
