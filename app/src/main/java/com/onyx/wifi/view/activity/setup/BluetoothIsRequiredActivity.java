package com.onyx.wifi.view.activity.setup;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityBluetoothIsRequiredBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;

public class BluetoothIsRequiredActivity extends BaseDeviceSetupActivity {

    private ActivityBluetoothIsRequiredBinding mBinding;

    private AppConstants.SetupMode mSetupMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSetupMode = DataManager.getInstance().getSetupMode();

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bluetooth_is_required);
        setView();
    }

    private void setView() {
        setTitleBar();

        String content = getString(R.string.bluetooth_is_required_content);
        if (mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
            mBinding.tvContent.setText(content);
            // resetup router 時不能透過 WIFI 的方式
            mBinding.btnAlternative.setVisibility(View.GONE);
        } else {
            // 除了 resetup router 之外, 都可以透過 WIFI setup
            content = content + " " + getString(R.string.bluetooth_is_required_tip);
            mBinding.tvContent.setText(content);
            mBinding.btnAlternative.setVisibility(View.VISIBLE);
            mBinding.btnAlternative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(mContext, AlternativeSetupActivity.class));
                    finish();
                }
            });
        }
        mBinding.btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 自動開啟藍牙並回到 setup 說明頁面
                BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
                if (!bluetoothAdapter.isEnabled()) {
                    LogUtils.trace("enable bluetooth");
                    bluetoothAdapter.enable();
                }

                retryDeviceSetup();
            }
        });
        setSupportClickMessage(mBinding.tvSupport);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);

        switch (mSetupMode) {
            case ROUTER:
            case ROUTER_RESETUP:
                mBinding.titleBar.setTitle(R.string.initial_setup_title);
                mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                });
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case REPEATER:
            default:
                mBinding.titleBar.setTitle(R.string.umedia_device_setup_title);
                mBinding.titleBar.setLeftButton(R.drawable.ic_home, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startDashboardActivity(BluetoothIsRequiredActivity.this);
                        finish();
                    }
                });
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;
        }

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        retryDeviceSetup();
    }
}
