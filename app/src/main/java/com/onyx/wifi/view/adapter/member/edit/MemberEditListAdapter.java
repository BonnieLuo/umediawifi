package com.onyx.wifi.view.adapter.member.edit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.MemberHeaderViewHolder;
import com.onyx.wifi.view.adapter.member.SectionViewHolder;
import com.onyx.wifi.view.interfaces.member.OnMemberChangedListener;

import java.util.ArrayList;
import java.util.List;

public class MemberEditListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListItem> mListItems = new ArrayList<>();

    private FamilyMember mMember;

    private View.OnClickListener mOnButtonClickListener;

    private OnMemberChangedListener mOnMemberChangedListener;

    public MemberEditListAdapter(FamilyMember member, List<ListItem> listItems, View.OnClickListener listener, OnMemberChangedListener memberChangedListener) {
        super();

        mMember = member;

        mListItems = listItems;

        mOnButtonClickListener = listener;

        mOnMemberChangedListener = memberChangedListener;
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<MemberEditItemType, Object> listItem = mListItems.get(position);

        MemberEditItemType type = listItem.getType();

        switch (type) {
            case HEADER:
                return MemberEditItemType.HEADER.ordinal();
            case SECTION:
                return MemberEditItemType.SECTION.ordinal();
            case ITEM:
            default:
                return MemberEditItemType.ITEM.ordinal();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == MemberEditItemType.HEADER.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_editor_header, parent, false);

            MemberHeaderViewHolder viewHolder = new MemberHeaderViewHolder(view);
            return viewHolder;
        }

        if (viewType == MemberEditItemType.SECTION.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_section, parent, false);

            SectionViewHolder viewHolder = new SectionViewHolder(view);
            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_member_assign_device_list, parent, false);

        ClientViewHolder viewHolder = new ClientViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<MemberEditItemType, Object> listItem = mListItems.get(position);

        MemberEditItemType type = listItem.getType();

        switch (type) {
            case HEADER:
                setHeader(viewHolder);

                break;
            case SECTION:
                setSection(listItem, viewHolder);

                break;
            case ITEM:
            default:
                setItem(listItem, viewHolder);

                break;
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    public void updateImage(String imageFilePath) {
        mMember.setTemporaryPhotoPath(imageFilePath);

        notifyItemChanged(0);

        if (mOnMemberChangedListener != null) {
            mOnMemberChangedListener.onMemberChanged();
        }
    }

    private void setHeader(RecyclerView.ViewHolder viewHolder){
        MemberHeaderViewHolder headerViewHolder = (MemberHeaderViewHolder) viewHolder;

        headerViewHolder.setFamilyMember(mMember);
        headerViewHolder.setIconCamera();
        headerViewHolder.setOnButtonClickListener(mOnButtonClickListener);
        headerViewHolder.setOnMemberChangedListener(mOnMemberChangedListener);
    }

    private void setSection(ListItem listItem, RecyclerView.ViewHolder viewHolder){
        String title = (String) listItem.getItem();

        if (title != null) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) viewHolder;

            sectionViewHolder.setTitle(title);
        }
    }

    private void setItem(ListItem listItem, RecyclerView.ViewHolder viewHolder){
        Client client = (Client) listItem.getItem();

        if (client != null) {
            ClientViewHolder clientViewHolder = (ClientViewHolder) viewHolder;

            clientViewHolder.setClient(client);
        }
    }

    private class ClientViewHolder extends RecyclerView.ViewHolder {

        private Client mClient;

        private ImageView mIconImageView;

        private TextView mNameTextView;

        private TextView mLastOnlineTextView;

        private ImageView mLightImageView;

        private ImageButton mAssignButton;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = this.itemView.findViewById(R.id.iconImageView);

            mNameTextView = this.itemView.findViewById(R.id.nameTextView);

            mLastOnlineTextView = this.itemView.findViewById(R.id.lastOnlineTextView);

            mLightImageView = this.itemView.findViewById(R.id.lightImageView);

            mAssignButton = this.itemView.findViewById(R.id.assignButton);
            mAssignButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Client> clientList = mMember.getTemporaryClientList();

                    boolean selected = false;

                    // mClient:當下要處理的list item(client)
                    if (clientList.contains(mClient)) { // 原本已有
                        clientList.remove(mClient); //<= ??

                        selected = false;
                    } else {                            // 原本的設定沒有
                        clientList.add(mClient);

                        selected = true;
                    }

                    setSelected(selected);
                }
            });
        }

        public void setClient(Client client) {
            mClient = client;

            setupIcon();

            setupName();

            setupConnectionLight();

            setupConnectionMessage();

            List<Client> clientList = mMember.getAllAllowedClient();////mMember.getAllClients();

            if (clientList.contains(mClient)) {
                setSelected(true);
            } else {
                setSelected(false);
            }
        }

        private void setSelected(boolean selected) {

            if (selected) {
                int iconId = mClient.getClientIcon(ConnectionStatus.ONLINE);
                mIconImageView.setImageResource(iconId);
            } else {
                int iconId = mClient.getClientIcon(ConnectionStatus.OFFLINE);
                mIconImageView.setImageResource(iconId);
            }

            mNameTextView.setSelected(selected);
            mAssignButton.setSelected(selected);

            boolean isChanged = mMember.checkClientListChanged();

            if (isChanged) {
                if (mOnMemberChangedListener != null) {
                    mOnMemberChangedListener.onMemberChanged();
                }
            } else {
                if (mOnMemberChangedListener != null) {
                    mOnMemberChangedListener.onMemberRollback();
                }
            }
        }

        private void setupIcon() {
            int iconId = mClient.getClientIcon();
            mIconImageView.setImageResource(iconId);
        }

        private void setupName() {
            String name = mClient.getName();
            mNameTextView.setText(name);
        }

        private void setupConnectionLight() {
            int lightId = mClient.getConnectionLight();
            mLightImageView.setImageResource(lightId);
        }

        private void setupConnectionMessage() {
            Context context = this.itemView.getContext();
            String message = mClient.getLastOnlineTimeInfo();//.getConnectionMessage(context);
            mLastOnlineTextView.setText(message);
        }

    }
}
