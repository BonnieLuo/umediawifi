package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNetworkTimezoneBinding;
import com.onyx.wifi.databinding.ActivityNetworksettingsTimezoneBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.mainmenu.voiceassistance.SetAlexaLanguageActivity;
import com.onyx.wifi.view.adapter.LanguageAdapter;
import com.onyx.wifi.view.adapter.NetworkSpeedTestAdapter;
import com.onyx.wifi.view.adapter.SearchTimezoneAdapter;
import com.onyx.wifi.view.adapter.TimezoneAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

public class TimezoneActivity extends BaseMenuActivity implements SearchTimezoneAdapter.OnItemClickHandler{
    private ActivityNetworksettingsTimezoneBinding mBinding;
    private NetworkSettingViewModel mViewModel;

    private PopupWindow timezonePopupWindow;
    private RecyclerView mRvTimezoneList = null;
    private SearchTimezoneAdapter mAdapter;
    private View popTimezoneView = null;
    private String mSelectedTimezone = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_networksettings_timezone);

        setViewModel();
        //showTimezonePopwindow(); //需透過viewmodel取得timezone array，所以必須在setViewModel()之後
        setView();
    }

//    private void setUpTimezoneRecycler() {
//        //
//        // Set up Recycler View for Voice Devices
//        //
//        if (mRvTimezoneList != null) {
//
//            mRvTimezoneList.setLayoutManager(new LinearLayoutManager(NetworkTimezoneActivity.this));
//            // 設置格線
//            //1.用系統提供的高度和顏色,不做自定義
//            //mRvTimezoneList.addItemDecoration(new DividerItemDecoration(NetworkTimezoneActivity.this, DividerItemDecoration.VERTICAL));
//            DividerItemDecoration dec = new DividerItemDecoration(NetworkTimezoneActivity.this, DividerItemDecoration.VERTICAL);
//            Drawable drawable = ContextCompat.getDrawable(mContext,R.drawable.shape_list_divider);
//            dec.setDrawable(drawable);
//
//            mRvTimezoneList.addItemDecoration(dec);
//
//            // 將資料交給adapter
//            // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
//            mAdapter = new SearchTimezoneAdapter(NetworkTimezoneActivity.this, mViewModel.getTimezoneList(), this);
//            mRvTimezoneList.setAdapter(mAdapter);
//        }
//    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.NetworkSetting.POST_HOME_NETWORK_SETTING) {
                        if (resultStatus.success) {
                            // update Share Preference (Device Info)
                            // TODO: 改成getDashboardInfo(主頁原本背景就會定期call)or什麼都不做，但最兩行要留著
                            DeviceInfo deviceInfo = DataManager.getInstance().getDeviceInfo();
                            deviceInfo.setCity(mSelectedTimezone);
                            DataManager.getInstance().updateDeviceInfo(deviceInfo);

                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                            finish();
                        } else {
                            showMessageDialog("Post Home Network Setting", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void setUpTimezoneRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding.rvTimezoneList.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding.rvTimezoneList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        if (mAdapter == null) {
            //LogUtils.trace("AdapterList", "new Adapter" + mViewModel.getFWStatusList().size());
            mAdapter = new SearchTimezoneAdapter(mContext, mViewModel.getTimezoneList(), this);
        }
        mBinding.rvTimezoneList.setAdapter(mAdapter);
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        //
        // List
        //
        setUpTimezoneRecycler();

//        mBinding.btnSelectTimezone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                timezonePopupWindow.showAtLocation(popTimezoneView, Gravity.BOTTOM, 100, 0);
//                setBackgroundAlpha(NetworkTimezoneActivity.this, 0.6f);
//            }
//        });

        // default timezone
        //mBinding.tvSelectedTimezoneValue.setText(DataManager.getInstance().getDeviceInfo().getCity());

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update timezone to cloud - 成功後更改SharePreference
                mViewModel.postHomeNetworkSetting(mSelectedTimezone, DataManager.getInstance().getNetworkSetting());
            }
        });

        mBinding.svTimezoneSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mBinding.svTimezoneSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                // 得到輸入管理物件
 //               InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                if (imm != null) {
//                    // 這將讓鍵盤在所有的情況下都被隱藏，但是一般我們在點選搜尋按鈕後，輸入法都會乖乖的自動隱藏的。
//                    imm.hideSoftInputFromWindow(mBinding.svTimezoneSearch.getWindowToken(), 0); // 輸入法如果是顯示狀態，那麼就隱藏輸入法
//                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mAdapter.getFilter().filter(s);
                return false;
            }
        });
        mBinding.svTimezoneSearch.findViewById(android.support.v7.appcompat.R.id.search_plate).setBackground(null);
        mBinding.svTimezoneSearch.findViewById(android.support.v7.appcompat.R.id.submit_area).setBackground(null);

        ImageView icon = mBinding.svTimezoneSearch.findViewById(android.support.v7.appcompat.R.id.search_button);
        icon.setColorFilter(CommonUtils.getColor(mContext, R.color.yellow_dfc231));
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.network_timezone_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }


    @Override
    public void onItemClick(boolean anySelected, String selectedTimezone) {
        // 與sharepreference裡的值比較，不同時會在右上角顯示勾號
        if (anySelected) {
            if (!DataManager.getInstance().getDeviceInfo().getCity().equals(selectedTimezone)) {
                mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
                mSelectedTimezone = selectedTimezone;
            } else {
                mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
            }
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
        //TODO
        //mBinding.tvSelectedTimezoneValue.setText(mSelectedTimezone);
        //timezonePopupWindow.dismiss();



        //CommonUtils.toast(mContext, selectedTimezone);
    }
}
