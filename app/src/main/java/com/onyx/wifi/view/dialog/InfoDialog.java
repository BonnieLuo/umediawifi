package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewInfoDialogBinding;

public class InfoDialog extends BaseDialog {

    private ViewInfoDialogBinding mBinding;

    private String mContent;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_info_dialog, null, false);
        mBinding.tvTitle.setText(mTitle);
        mBinding.tvContent.setText(mContent);
        mBinding.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        builder.setView(mBinding.getRoot());
        return builder.create();
    }

    @Override
    public String toString() {
        return "InfoDialog{ " +
                "title = \"" + mTitle + "\"" +
                ", content = \"" + mContent + "\"" +
                " }";
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContent(String content) {
        mContent = content;
    }
}
