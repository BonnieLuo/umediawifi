package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewMessageDialogBinding;

public class MessageDialog extends BaseDialog {

    private ViewMessageDialogBinding mBinding;

    private String mContent;
    private String mOkText;
    private View.OnClickListener mOkListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_message_dialog, null, false);
        mBinding.tvTitle.setText(mTitle);
        mBinding.tvContent.setText(mContent);
        if (mOkText == null) {
            mBinding.btnOk.setText(getString(R.string.btn_ok));
        } else {
            mBinding.btnOk.setText(mOkText);
        }
        if (mOkListener == null) {
            mBinding.btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        } else {
            mBinding.btnOk.setOnClickListener(mOkListener);
        }
        builder.setView(mBinding.getRoot());
        return builder.create();
    }

    @Override
    public String toString() {
        return "MessageDialog{ " +
                "title = \"" + mTitle + "\"" +
                ", content = \"" + mContent + "\"" +
                " }";
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setOkButton(View.OnClickListener listener) {
        mOkListener = listener;
    }

    public void setOkButton(String text, View.OnClickListener listener) {
        mOkText = text;
        mOkListener = listener;
    }
}
