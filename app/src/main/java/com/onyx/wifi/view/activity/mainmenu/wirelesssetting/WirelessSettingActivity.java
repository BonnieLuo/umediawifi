package com.onyx.wifi.view.activity.mainmenu.wirelesssetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityWirelessSettingBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.BnbNetworkSetting;
import com.onyx.wifi.model.item.GuestNetworkSetting;
import com.onyx.wifi.model.item.WirelessSetting;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.wirelesssetting.WirelessSettingData;
import com.onyx.wifi.viewmodel.mainmenu.WirelessSettingViewModel;

public class WirelessSettingActivity extends BaseWirelessSettingActivity {

    private ActivityWirelessSettingBinding mBinding;
    private WirelessSettingViewModel mViewModel;

    private WirelessSettingData mOriginalWirelessSettingData;
    private WirelessSettingData mTempWirelessSettingData;

    private GuestNetworkSetting mOriginalGuestNetworkSetting;
    private BnbNetworkSetting mOriginalBnbNetworkSetting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_wireless_setting);
        setView();
        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // 先顯示 share preference 儲存的資料
        setData();

        // 打 cloud API 以取得最新的資料
        mViewModel.getWirelessSetting();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // [CheckBox] Same SSID & Password
        mBinding.cbSameSsidPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTempWirelessSettingData.setWifiSame(isChecked);
                if (isChecked) {
                    // 設定 5G 使用和 2.4G 相同的 SSID/password
                    mTempWirelessSettingData.setSsid5g(mOriginalWirelessSettingData.getSsid2g());
                    mTempWirelessSettingData.setPassword5g(mOriginalWirelessSettingData.getPassword2g());
                    mBinding.etValue5gSsid.setText(mTempWirelessSettingData.getSsid5g());
                    mBinding.etValue5gPwd.setText(mTempWirelessSettingData.getPassword5g());
                } else {
                    // 設定 2.4G 和 5G 使用分開的 SSID/password, 有兩種可能
                    // (1) 原本設定是使用相同的 SSID/password (checked)
                    // (2) 原本設定就已經是分開的的 SSID/password 了 (unchecked)
                    if (mOriginalWirelessSettingData.getWifiSame()) {
                        // (1) checkbox 狀態: checked -> unchecked
                        // => 要為 5G 預設 SSID/password
                        mTempWirelessSettingData.setSsid5g(InputUtils.getDefault5gNetworkSsid(mOriginalWirelessSettingData.getSsid2g()));
                        mTempWirelessSettingData.setPassword5g(mOriginalWirelessSettingData.getPassword2g());
                    } else {
                        // (2) checkbox 狀態: unchecked -> checked -> unchecked
                        // => 由於中間變成 checked 狀態時, 5G 的 SSID/password 會被設定成和 2.4G 相同, 所以要再設定回原本的值
                        mTempWirelessSettingData.setSsid5g(mOriginalWirelessSettingData.getSsid5g());
                        mTempWirelessSettingData.setPassword5g(mOriginalWirelessSettingData.getPassword5g());
                    }
                    mBinding.etValue5gSsid.setText(mTempWirelessSettingData.getSsid5g());
                    mBinding.etValue5gPwd.setText(mTempWirelessSettingData.getPassword5g());
                }

                // 更新 UI
                setWirelessLayoutVisible(isChecked);
                checkIfDataChanged();
            }
        });

        // [按鈕] 2.4G password 的眼睛圖示
        mBinding.btnShow2gPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setPasswordVisible(mBinding.etValue2gPwd, isChecked);
            }
        });

        // [按鈕] 5G password 的眼睛圖示
        mBinding.btnShow5gPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setPasswordVisible(mBinding.etValue5gPwd, isChecked);
            }
        });

        // [Layout] 2.4G SSID 整列區塊設置點擊事件
        mBinding.cl2gSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWirelessInfo(WirelessSettingData.SettingType.INFO_2G);
            }
        });

        // [Layout] 2.4G password 整列區塊設置點擊事件
        mBinding.cl2gPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWirelessInfo(WirelessSettingData.SettingType.INFO_2G);
            }
        });

        // [Layout] 5G SSID 整列區塊設置點擊事件
        mBinding.cl5gSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWirelessInfo(WirelessSettingData.SettingType.INFO_5G);
            }
        });

        // [Layout] 5G password 整列區塊設置點擊事件
        mBinding.cl5gPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWirelessInfo(WirelessSettingData.SettingType.INFO_5G);
            }
        });

        // [按鈕] "Device Online Notification" 的 info
        mBinding.btnInfoDeviceOnlineNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfoDialog(getString(R.string.info_device_online_notification_title), getString(R.string.info_device_online_notification_content));
            }
        });

        // [CheckBox] Device Online Notification
        mBinding.cbDeviceOnlineNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTempWirelessSettingData.setClientOnlineNotification(isChecked);
                mBinding.tvValueDeviceOnlineNotification.setText((isChecked) ? R.string.enabled : R.string.disabled);
                checkIfDataChanged();
            }
        });

        // [Layout] Guest Network 整列區塊設置點擊事件
        mBinding.clBlockedDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, BlockedDevicesActivity.class);
                startActivity(intent);
            }
        });

        // [Layout] Guest Network 整列區塊設置點擊事件
        mBinding.clGuestNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GuestNetworkActivity.class);
                startActivity(intent);
            }
        });

        // [Layout] BnB Network 整列區塊設置點擊事件
        mBinding.clBnbNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, BnbNetworkActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.wireless_setting_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.updateWirelessSetting(mTempWirelessSettingData, false);
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(WirelessSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.WirelessSetting.GET_WIRELESS_SETTING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Wireless Setting", resultStatus.errorMsg);
                        }
                        mViewModel.getGuestNetworkSetting();
                    } else if (resultStatus.actionCode == Code.Action.WirelessSetting.GET_GUEST_NETWORK_SETTING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Guest Network Setting", resultStatus.errorMsg);
                        }
                        mViewModel.getBnbNetworkSetting();
                    } else if (resultStatus.actionCode == Code.Action.WirelessSetting.GET_BNB_NETWORK_SETTING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get BnB Network Setting", resultStatus.errorMsg);
                        }
                        setData();
                    } else if (resultStatus.actionCode == Code.Action.WirelessSetting.UPDATE_WIRELESS_SETTING) {
                        if (resultStatus.success) {
                            // 更新資料及勾勾狀態
                            mOriginalWirelessSettingData.setWifiSame(mTempWirelessSettingData.getWifiSame());
                            mOriginalWirelessSettingData.setClientOnlineNotification(mTempWirelessSettingData.getClientOnlineNotification());
                            checkIfDataChanged();

                            // 將更新後的值寫回 share preference
                            DataManager dataManager = DataManager.getInstance();
                            WirelessSetting wirelessSetting = dataManager.getWirelessSetting();
                            wirelessSetting.setWifiSameEnabled(mTempWirelessSettingData.getWifiSame());
                            wirelessSetting.setClientOnlineNotification(mTempWirelessSettingData.getClientOnlineNotification());
                            if (wirelessSetting.getWireless5g().getBh().equals("0")) {
                                wirelessSetting.getWireless5g().setSsid(mTempWirelessSettingData.getSsid5g());
                                wirelessSetting.getWireless5g().setPwd(mTempWirelessSettingData.getPassword5g());
                            } else {
                                wirelessSetting.getWireless5g2().setSsid(mTempWirelessSettingData.getSsid5g());
                                wirelessSetting.getWireless5g2().setPwd(mTempWirelessSettingData.getPassword5g());
                            }
                            dataManager.setWirelessSetting(wirelessSetting);
                        } else {
                            showMessageDialog("Update Wireless Setting", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    // (1) get data from shared preference
    // (2) set data to UI
    private void setData() {
        if (mMenuBar != null) {
            mMenuBar.setData();
        }

        // get data from shared preference
        mOriginalWirelessSettingData = mViewModel.getWirelessSettingData(); // 原來的設定值
        mTempWirelessSettingData = mViewModel.getWirelessSettingData(); // temp 物件, 儲存使用者在這頁更新的值, 一開始和 original 的值是相同的
        mOriginalGuestNetworkSetting = mViewModel.getGuestNetworkSettingData();
        mOriginalBnbNetworkSetting = mViewModel.getBnbNetworkSettingData();

        // wireless setting
        setWirelessLayoutVisible(mTempWirelessSettingData.getWifiSame());
        mBinding.etValue5gSsid.setText(mTempWirelessSettingData.getSsid5g());
        mBinding.etValue5gPwd.setText(mTempWirelessSettingData.getPassword5g());
        mBinding.etValue2gSsid.setText(mTempWirelessSettingData.getSsid2g());
        mBinding.etValue2gPwd.setText(mTempWirelessSettingData.getPassword2g());
        setMultiLineDisplay(mBinding.etValue5gSsid);
        setMultiLineDisplay(mBinding.etValue5gPwd);
        setMultiLineDisplay(mBinding.etValue2gSsid);
        setMultiLineDisplay(mBinding.etValue2gPwd);

        // device online notification
        boolean isEnabled = mTempWirelessSettingData.getClientOnlineNotification();
        mBinding.cbDeviceOnlineNotification.setChecked(isEnabled);
        mBinding.tvValueDeviceOnlineNotification.setText(isEnabled ? R.string.enabled : R.string.disabled);

        // guest network
        if (mOriginalGuestNetworkSetting != null && mOriginalGuestNetworkSetting.getEnable()) {
            mBinding.tvValueGuestNetwork.setText(R.string.enabled);
        } else {
            mBinding.tvValueGuestNetwork.setText(R.string.disabled);
        }

        // BnB network
        if (mOriginalBnbNetworkSetting != null &&
                mOriginalBnbNetworkSetting.getEnable() &&
                DateUtils.isBnBActive(mOriginalBnbNetworkSetting)) {
            // 判斷現在時間若在排程時間內, 才是 active, 否則是 inactive
            mBinding.tvValueBnbNetwork.setText(R.string.active);
            mBinding.tvValueExpire.setText(DateUtils.secToTimeDescription(mOriginalBnbNetworkSetting.getEnd()));
            mBinding.groupExpire.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvValueBnbNetwork.setText(R.string.inactive);
            mBinding.groupExpire.setVisibility(View.GONE);
        }

        // 更新勾勾狀態
        checkIfDataChanged();
    }

    private void setWirelessLayoutVisible(boolean sameSsidPwd) {
        if (sameSsidPwd) {
            mBinding.cbSameSsidPwd.setChecked(true);
            mBinding.group5g.setVisibility(View.GONE);
            mBinding.tvName2gSsid.setText(R.string.item_family_network_ssid);
            mBinding.tvName2gPwd.setText(R.string.item_family_network_password);
        } else {
            mBinding.cbSameSsidPwd.setChecked(false);
            mBinding.group5g.setVisibility(View.VISIBLE);
            mBinding.tvName2gSsid.setText(R.string.item_2g_family_network_ssid);
            mBinding.tvName2gPwd.setText(R.string.item_2g_family_network_password);
        }
    }

    private void checkIfDataChanged() {
        boolean isChanged = false;
        if (mTempWirelessSettingData.getWifiSame() != mOriginalWirelessSettingData.getWifiSame()) {
            isChanged = true;
        }
        if (mTempWirelessSettingData.getClientOnlineNotification() != mOriginalWirelessSettingData.getClientOnlineNotification()) {
            isChanged = true;
        }
        mBinding.titleBar.setRightButtonVisibility((isChanged) ? View.VISIBLE : View.INVISIBLE);
    }

    private void editWirelessInfo(WirelessSettingData.SettingType settingType) {
        // 產生一個新的 WirelessSettingData 物件, 將目前的設定資訊傳給下一頁 WirelessInfoSettingActivity 去做設定
        // 下一頁編輯的資訊是 2.4G/5G SSID 和 password
        WirelessSettingData wirelessSettingData = new WirelessSettingData();

        // wifi same flag 和下一頁設定的資訊有關, 在下一頁按勾勾按鈕打 cloud API 時要一起送出, 故將待更新的 temp 值傳給下一頁
        wirelessSettingData.setWifiSame(mTempWirelessSettingData.getWifiSame());

        wirelessSettingData.setSsid2g(mTempWirelessSettingData.getSsid2g());
        wirelessSettingData.setPassword2g(mTempWirelessSettingData.getPassword2g());
        wirelessSettingData.setSsid5g(mTempWirelessSettingData.getSsid5g());
        wirelessSettingData.setPassword5g(mTempWirelessSettingData.getPassword5g());
        if (mTempWirelessSettingData.getWifiSame()) {
            wirelessSettingData.setSettingType(WirelessSettingData.SettingType.BOTH);
        } else {
            wirelessSettingData.setSettingType(settingType);
        }

        // device online notification 是在這頁設定的, 下一頁不會更新到這項值, 故將原本的設定值傳給下一頁
        wirelessSettingData.setClientOnlineNotification(mOriginalWirelessSettingData.getClientOnlineNotification());

        Intent intent = new Intent(mContext, WirelessInfoSettingActivity.class);
        intent.putExtra(AppConstants.EXTRA_WIRELESS_INFO, wirelessSettingData.toJsonString());
        startActivity(intent);
    }
}
