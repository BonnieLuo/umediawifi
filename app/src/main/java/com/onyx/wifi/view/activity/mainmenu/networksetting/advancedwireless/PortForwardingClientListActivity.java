package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityClientListBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRule;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.advancedwireless.PortForwardingClientListAdapter;
import com.onyx.wifi.viewmodel.ClientListViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;

import java.lang.reflect.Type;
import java.util.List;

public class PortForwardingClientListActivity extends BaseMenuActivity implements PortForwardingClientListAdapter.OnClientListChangedListener {

    private ActivityClientListBinding mBinding;

    private ClientListViewModel mViewModel;

    private PortForwardingClientListAdapter mAdapter;

    private String mTitle;

    private String mIp;

    private boolean mIsMultiSelectEnabled;

    private ClientModel mSelectedClientModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        if (intent != null) {
            Bundle bundle = intent.getExtras();
            mTitle = bundle.getString("title");
            mIsMultiSelectEnabled = bundle.getBoolean("multi_select_enabled");

            mIp = bundle.getString("ip", "");
        }

        DataManager dataManager = DataManager.getInstance();
        dataManager.setSelectedClientIp(mIp);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_client_list);

        mAdapter = new PortForwardingClientListAdapter(this);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            mViewModel.getAllClients();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(mTitle);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedClientModel != null) {
                    String newIp = mSelectedClientModel.getIp();

                    DataManager dataManager = DataManager.getInstance();

                    PortForwardingRule rule = dataManager.getPortForwardingRule();
                    rule.setIp(newIp);
                }

                finish();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
}

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ClientListViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.GET_ALL_CLIENTS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get All Clients", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            JsonObject dataJson = json.getAsJsonObject("data");

                            JsonArray clientListJSON = dataJson.getAsJsonArray("client_list");

                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<ClientModel>>() {
                            }.getType();
                            List<ClientModel> clientModelList = gson.fromJson(clientListJSON.toString(), listType);
                            setData(clientModelList);
                        }
                    }
                }
            }
        });
    }

    private void setData(List<ClientModel> clientModelList) {
        if (clientModelList == null) {
            return;
        }

        mAdapter.setData(clientModelList, mIp);
    }

    @Override
    public void onClientListChanged(ClientModel selectedClientModel) {
        mSelectedClientModel = selectedClientModel;

        if (checkClientListChanged()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    public boolean checkClientListChanged() {
        if (mSelectedClientModel != null) {
            String ip = mSelectedClientModel.getIp();
            if (!mIp.equalsIgnoreCase(ip)) {
                return true;
            }
        }

        return false;
    }
}
