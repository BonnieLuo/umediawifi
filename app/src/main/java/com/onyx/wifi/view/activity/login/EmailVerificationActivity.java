package com.onyx.wifi.view.activity.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityEmailVerificationBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.interfaces.TextChangedListener;
import com.onyx.wifi.view.listener.PinCodeOnKeyListener;
import com.onyx.wifi.view.listener.PinCodeTextWatcher;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.login.EmailVerificationViewModel;

public class EmailVerificationActivity extends BaseActivity {

    private ActivityEmailVerificationBinding mBinding;
    private EmailVerificationViewModel mViewModel;

    private EditText[] mEtDigits;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean autoResendCode = getIntent().getBooleanExtra(AppConstants.EXTRA_AUTO_RESEND_VERIFY_CODE, true);
        LogUtils.trace("auto resend verify code = " + autoResendCode);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_email_verification);
        setView();
        setViewModel();

        if (autoResendCode) {
            // autoResendCode = true 發生的情境:
            // App 使用 Firebase SDK 登入成功, 打 get user info 的 cloud API 時收到 4013 (unverified email)
            // 此時 App 需引導至 email 認證頁並自動重送一組驗證碼

            mViewModel.resendCode();

            // App 已經使用 Firebase SDK 登入成功了, 不需要再登入
            mViewModel.setIsNeedAutoSignIn(false);
        } else {
            // autoResendCode = false 發生時是註冊的情境:
            // (1) App 打了註冊的 cloud API 後, cloud 會傳送一組驗證碼到使用者的 email, App 不需要自動重送
            // (2) email 驗證成功後, App 要自動幫使用者登入

            String email = getIntent().getStringExtra(AppConstants.EXTRA_USER_EMAIL);
            String password = getIntent().getStringExtra(AppConstants.EXTRA_USER_PASSWORD);
            mViewModel.setSignInUser(email, password);

            mViewModel.setIsNeedAutoSignIn(true);
        }
    }

    private void setView() {
        mEtDigits = new EditText[]{mBinding.etDigit1, mBinding.etDigit2, mBinding.etDigit3, mBinding.etDigit4};
        mBinding.etDigit1.addTextChangedListener(new PinCodeTextWatcher(EmailVerificationActivity.this, mEtDigits, 0, mTextChangedListener));
        mBinding.etDigit2.addTextChangedListener(new PinCodeTextWatcher(EmailVerificationActivity.this, mEtDigits, 1, mTextChangedListener));
        mBinding.etDigit3.addTextChangedListener(new PinCodeTextWatcher(EmailVerificationActivity.this, mEtDigits, 2, mTextChangedListener));
        mBinding.etDigit4.addTextChangedListener(new PinCodeTextWatcher(EmailVerificationActivity.this, mEtDigits, 3, mTextChangedListener));
        mBinding.etDigit1.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 0));
        mBinding.etDigit2.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 1));
        mBinding.etDigit3.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 2));
        mBinding.etDigit4.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 3));

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.resendCode();
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.verifyCode();
            }
        });
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(EmailVerificationViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.VERIFY_EMAIL_RESEND_CODE) {
                        if (resultStatus.success) {
                            String msg = String.format(mContext.getString(R.string.msg_resend_verify_code_success));
                            CommonUtils.toast(mContext, msg);
                        } else {
                            showMessageDialog("Email Verification - Resend Code", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.UserAccount.VERIFY_EMAIL_VERIFY_CODE) {
                        if (resultStatus.success) {
                            // email 認證成功, 刷新 token
                            mAccountManager.refreshUserIdToken(true);
                            afterSignInDone(EmailVerificationActivity.this);
                        } else {
                            showMessageDialog("Email Verification - Verify Email", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private TextChangedListener mTextChangedListener = new TextChangedListener() {
        @Override
        public void afterTextChangedDone() {
            String verificationCode = "";
            for (EditText etDigit : mEtDigits) {
                verificationCode = verificationCode + etDigit.getText().toString();
            }
            if (verificationCode.length() == 4) {
                mBinding.btnResend.setVisibility(View.INVISIBLE);
                mBinding.btnContinue.setVisibility(View.VISIBLE);

                mViewModel.setVerificationCode(verificationCode);
                LogUtils.trace("verification code = " + verificationCode);
            } else {
                mBinding.btnResend.setVisibility(View.VISIBLE);
                mBinding.btnContinue.setVisibility(View.INVISIBLE);
            }
        }
    };
}
