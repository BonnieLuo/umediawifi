package com.onyx.wifi.view.activity.menubar.member.client;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAttachedProfileListBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.member.client.ClientOwnerListAdapter;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.interfaces.member.OnClientOwnerChangedListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.client.ClientOwnerViewModel;

public class ClientOwnerActivity extends BaseMenuActivity implements OnClientOwnerChangedListener {

    private ActivityAttachedProfileListBinding mBinding;

    private ClientOwnerViewModel mViewModel;

    private ClientOwnerListAdapter mAdapter;
    private boolean bIsInit = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ClientOwnerListAdapter();
        mAdapter.setOnClientOwnerChangedListener(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_attached_profile_list);

        setView();

        setViewModel();

        DataManager dataManager = DataManager.getInstance();
        MemberList memberList = dataManager.getMemberList();

        if (memberList == null) {
            bIsInit = true;
            mViewModel.getMemberList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        mBinding.membersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.membersRecyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Profile Attached");
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateClientOwner();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ClientOwnerViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Member.ASSIGN_CLIENT_TO_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Assign Client To Member", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.REMOVE_CLIENT_FROM_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Client From Member", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Member List", resultStatus.errorMsg);

                            return;
                        }

                        if (bIsInit) {
                            DataManager dataManager = DataManager.getInstance();
                            MemberList memberList = dataManager.getMemberList();

                            if (memberList != null) {
                                mAdapter.setDevice(memberList.getFamilyMemberList());
                                mBinding.membersRecyclerView.setAdapter(mAdapter);
                            } else { // client 有binding member但member list 沒有東西，不合理
                                showMessageDialog("Get Member List", "client has binding member, but the member list is empty!");
                            }

                            bIsInit = false;
                        } else {
                            finish();
                        }
                    }
                }
            }
        });
    }

    private void updateClientOwner() {
        DataManager dataManager = DataManager.getInstance();

        Client client = dataManager.getManageClient();

        String newOwnerId = client.getTemporaryOwnerId();

        if (newOwnerId == null) {
            String ownerId = client.getOwnerId();
            mViewModel.removeClientFromMember(ownerId, client);

            return;
        }

        mViewModel.assignClientToMember(newOwnerId, client);
    }

    @Override
    public void onClientOwnerChanged(boolean isOwnerChanged) {
        if (isOwnerChanged) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }
}

