package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySetupTroubleshootingBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.mainmenu.wirelesssetting.BnbNetworkActivity;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.ListDisplayType;

import java.util.ArrayList;

public class SetupTroubleshootingActivity extends BaseMoreHelpActivity {
    private ActivitySetupTroubleshootingBinding mBinding;

    private ListDisplayType mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ3DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ4DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ5DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ6DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ7DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ8DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ9DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_troubleshooting);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        //
        // set partial color of some TextView content
        //
        setTextPartialColor(mBinding.tv3Solution, mBinding.tv3Solution.getText().toString(), "“Yellow”", R.color.yellow_c8ac1f);
        setTextPartialColor(mBinding.tv4Question, mBinding.tv4Question.getText().toString(), "blue", R.color.blue_1e88ff);
        setTextPartialColor(mBinding.tv5Question, mBinding.tv5Question.getText().toString(), "blue", R.color.blue_1e88ff);

        //
        // onclick handler
        //
        mBinding.ctv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ3DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ3DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ3DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ3DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ4DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ4DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ4DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ4DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ5DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ5DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ5DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ5DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ6DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ6DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ6DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ6DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ7DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ7DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ7DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ7DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ8DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ8DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ8DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ8DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ9DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ9DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ9DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ9DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        setBottomLayout(mBinding.btnReturnToSetup, mBinding.tvTrouble, mBinding.tvSupport, mBinding.menuBar, mBinding.clContent);
    }

    private void showByDisplayType() {
        // 1
        if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("+");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl1Solution.setVisibility(View.GONE);
        }else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("-");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl1Solution.setVisibility(View.VISIBLE);
        }

        // 2
        if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("+");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl2Solution.setVisibility(View.GONE);
        }else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("-");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl2Solution.setVisibility(View.VISIBLE);
        }

        // 3
        if (mQ3DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv3.setText("+");
            mBinding.tv3Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl3Solution.setVisibility(View.GONE);
        }else if (mQ3DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv3.setText("-");
            mBinding.tv3Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl3Solution.setVisibility(View.VISIBLE);
        }

        // 4
        if (mQ4DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv4.setText("+");
            mBinding.tv4Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl4Solution.setVisibility(View.GONE);
        }else if (mQ4DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv4.setText("-");
            mBinding.tv4Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl4Solution.setVisibility(View.VISIBLE);
        }

        // 5
        if (mQ5DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv5.setText("+");
            mBinding.tv5Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl5Solution.setVisibility(View.GONE);
        }else if (mQ5DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv5.setText("-");
            mBinding.tv5Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl5Solution.setVisibility(View.VISIBLE);
        }

        // 6
        if (mQ6DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv6.setText("+");
            mBinding.tv6Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl6Solution.setVisibility(View.GONE);
        }else if (mQ6DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv6.setText("-");
            mBinding.tv6Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl6Solution.setVisibility(View.VISIBLE);
        }

        // 7
        if (mQ7DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv7.setText("+");
            mBinding.tv7Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl7Solution.setVisibility(View.GONE);
        }else if (mQ7DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv7.setText("-");
            mBinding.tv7Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl7Solution.setVisibility(View.VISIBLE);
        }

        // 8
        if (mQ8DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv8.setText("+");
            mBinding.tv8Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl8Solution.setVisibility(View.GONE);
        }else if (mQ8DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv8.setText("-");
            mBinding.tv8Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl8Solution.setVisibility(View.VISIBLE);
        }

        // 9
        if (mQ9DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv9.setText("+");
            mBinding.tv9Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl9Solution.setVisibility(View.GONE);
        }else if (mQ9DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv9.setText("-");
            mBinding.tv9Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl9Solution.setVisibility(View.VISIBLE);
        }
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.more_help_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
