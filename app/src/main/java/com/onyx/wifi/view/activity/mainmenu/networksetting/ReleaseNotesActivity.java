package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityReleaseNotesBinding;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;

public class ReleaseNotesActivity extends BaseMenuActivity {
    private ActivityReleaseNotesBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_release_notes);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        mBinding.tvReleaseNotes.setMovementMethod(ScrollingMovementMethod.getInstance());
        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.firmware_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
