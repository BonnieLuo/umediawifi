package com.onyx.wifi.view.listener;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.onyx.wifi.view.interfaces.TextChangedListener;

public class PinCodeTextWatcher implements TextWatcher {
    private Activity activity;
    private EditText[] editTexts;
    private int currentIndex;
    private boolean isFirst = false, isLast = false;
    private String newTypedString = "";
    private TextChangedListener listener;

    public PinCodeTextWatcher(Activity activity, EditText[] editTexts, int currentIndex, TextChangedListener listener) {
        this.activity = activity;
        this.currentIndex = currentIndex;
        this.editTexts = editTexts;
        this.listener = listener;

        if (currentIndex == 0) {
            this.isFirst = true;
        } else if (currentIndex == editTexts.length - 1) {
            this.isLast = true;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        newTypedString = s.subSequence(start, start + count).toString().trim();
    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = newTypedString;

        /* Detect paste event and set first char */
        if (text.length() > 1) {
            text = String.valueOf(text.charAt(0));
        }

        editTexts[currentIndex].removeTextChangedListener(this);
        editTexts[currentIndex].setText(text);
        editTexts[currentIndex].setSelection(text.length());
        editTexts[currentIndex].addTextChangedListener(this);

        if (text.length() == 1) {
            moveToNext();
        } else if (text.length() == 0) {
            moveToPrevious();
        }

        listener.afterTextChangedDone();
    }

    private void moveToNext() {
        if (!isLast) {
            editTexts[currentIndex + 1].requestFocus();
        }

        if (isAllEditTextsFilled() && isLast) { // isLast is optional
            editTexts[currentIndex].clearFocus();
            hideKeyboard();
        }
    }

    private void moveToPrevious() {
        if (!isFirst) {
            editTexts[currentIndex - 1].requestFocus();
        }
    }

    private boolean isAllEditTextsFilled() {
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().length() == 0) {
                return false;
            }
        }
        return true;
    }

    private void hideKeyboard() {
        if (activity != null && activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
