package com.onyx.wifi.view.activity.menubar.dashboard;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNetworkMapBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.NetworkMap;
import com.onyx.wifi.model.item.NodeSetInfo;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.menubar.member.client.ClientSettingsActivity;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.NetworkMapViewModel;

import java.util.ArrayList;
import java.util.Map;

public class NetworkMapActivity extends BaseMenuActivity {

    private ActivityNetworkMapBinding mBinding;
    private NetworkMapViewModel mViewModel;

    // network map 頁面正中間的 item 是 root, 旁邊是連到它的 child
    // - root item 一定是 node (router or repeater), 點擊 root item 會開啟 node setting 頁面
    // - child item 可以是 repeater or client, 點擊 child item 的行為會不同
    //   (1) 若是 repeater, 點擊了會進入下一層 network map, 以剛剛點擊的 repeater 為 root item, 顯示它的資料
    //   (2) 若是 client, 點擊了會開啟 client setting 頁面

    private final int mItemsPerPage = 7;    // 針對一個 root item, 每一頁可顯示的資料 (child item) 數量有限, 超過的顯示在下一頁

    private NetworkMapLayer mCurrentNetworkMapLayer;   // 現在顯示的那一層 network map
    private ArrayList<NetworkMapLayer> mNetworkMapLayerList = new ArrayList<>();    // 所有的 network map layer

    // 為了方便顯示 child item 的資料, 以 array list 存取所有會使用到的 UI 元件
    private ArrayList<ImageView> mChildImgBackgroundList = new ArrayList<>(); // layout 上所有 child 的 background
    private ArrayList<ImageView> mChildIconList = new ArrayList<>(); // layout 上所有 child icon
    private ArrayList<ImageView> mChildStatusList = new ArrayList<>();  // layout 上所有 child internet status
    private ArrayList<TextView> mChildNameList = new ArrayList<>(); // layout 上所有 child name
    private ArrayList<TextView> mChildClientCountList = new ArrayList<>(); // layout 上所有 child client count
    private ArrayList<Group> mChildGroupList = new ArrayList<>();   // layout 上所有 child group

    private Handler mHandler;
    private int REFRESH_DATA_PERIOD = 3 * 60 * 1000;    // milliseconds => 3 minutes

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_network_map);
        setView();
        setViewModel();

        setInitialData();

        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.EVENT_DEVICE_INTERNET_STATUS);
        intentFilter.addAction(AppConstants.EVENT_CLIENT_COUNT);
        registerReceiver(mEventReceiver, intentFilter);

        // 打 cloud API 以取得最新的資料
        refreshData(true);

        mHandler.postDelayed(mGetNetworkMapRunnable, REFRESH_DATA_PERIOD);
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        mHandler.removeCallbacks(mGetNetworkMapRunnable);
    }

    // 定期打 API 更新資料
    private Runnable mGetNetworkMapRunnable = new Runnable() {
        @Override
        public void run() {
            LogUtils.trace("polling network map data");
            // 在背景執行就好, 不需要顯示 loading 狀態
            refreshData(false);
            mHandler.postDelayed(mGetNetworkMapRunnable, REFRESH_DATA_PERIOD);
        }
    };

    // 呼叫 refresh data 時, 若在前景就顯示 loading 狀態, 在背景就不用
    private void refreshData(boolean showLoadingStatus) {
        // 更新資料的流程包含三步驟:
        // (1) 打 get node list 取得有哪些 device
        // (2) 對所有 device 都打 connection test API (如果 device 的連線狀態有錯, 可以透過 connection test API 去修復)
        // (3) 再打 get network map 取得資料
        mViewModel.getNodeListForConnTest(showLoadingStatus);
    }

    private void getNetworkMap() {
        mCurrentNetworkMapLayer = mNetworkMapLayerList.get(mNetworkMapLayerList.size() - 1);
        DeviceInfo rootItem = mCurrentNetworkMapLayer.getRootItem();
        mViewModel.getNetworkMap(rootItem.getDid());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LogUtils.trace("requestCode = " + requestCode + ", resultCode = " + requestCode);
        if (requestCode == AppConstants.REQUEST_CODE_NODE_SETTING) {
            if (resultCode == RESULT_OK && data != null) {
                String did = data.getStringExtra(AppConstants.EXTRA_DID);
                boolean removeDevice = data.getBooleanExtra(AppConstants.EXTRA_REMOVE_DEVICE, false);
                LogUtils.trace("remove device = " + removeDevice + ", did = " + did + ", root item did = " + mCurrentNetworkMapLayer.rootItem.getDid());

                // 在 node setting 頁面執行的操作 (remove device or update device label), 回到這頁 network map 後需要重打 cloud API 去取得最新的資料
                // 由於 onActivityResult() 執行完之後會再經過 onResume(), 裡面就會呼叫 refreshData() 去打 cloud API 取得最新的資料
                // 所以在 onActivityResult() 裡面就不需要呼叫 refreshData()
                if (removeDevice) {
                    if (mCurrentNetworkMapLayer.rootItem.getDid().equals(did)) {
                        LogUtils.trace("case: remove root device");

                        // did 比對相同: 被移除的 device 是 network map 中心的 root item
                        // 因為 root item 不存在了, 故將 network map 返回上一層
                        // => array list 最後一個 item 是當前的 layer, 因為要返回上一層, 所以移除它
                        mNetworkMapLayerList.remove(mNetworkMapLayerList.size() - 1);

                        // 重畫 UI, 畫出上一層的資料
                        setData();
                    } else {
                        LogUtils.trace("case: remove child device");
                    }
                }
            }
        }
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace(action);
            if (action.equals(AppConstants.EVENT_DEVICE_INTERNET_STATUS) ||
                    action.equals(AppConstants.EVENT_CLIENT_COUNT)) {
                getNetworkMap();
            }
        }
    };

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mChildImgBackgroundList.add(mBinding.imgChild1Background);
        mChildImgBackgroundList.add(mBinding.imgChild2Background);
        mChildImgBackgroundList.add(mBinding.imgChild3Background);
        mChildImgBackgroundList.add(mBinding.imgChild4Background);
        mChildImgBackgroundList.add(mBinding.imgChild5Background);
        mChildImgBackgroundList.add(mBinding.imgChild6Background);
        mChildImgBackgroundList.add(mBinding.imgChild7Background);

        mChildIconList.add(mBinding.imgChild1);
        mChildIconList.add(mBinding.imgChild2);
        mChildIconList.add(mBinding.imgChild3);
        mChildIconList.add(mBinding.imgChild4);
        mChildIconList.add(mBinding.imgChild5);
        mChildIconList.add(mBinding.imgChild6);
        mChildIconList.add(mBinding.imgChild7);

        mChildStatusList.add(mBinding.imgChild1Status);
        mChildStatusList.add(mBinding.imgChild2Status);
        mChildStatusList.add(mBinding.imgChild3Status);
        mChildStatusList.add(mBinding.imgChild4Status);
        mChildStatusList.add(mBinding.imgChild5Status);
        mChildStatusList.add(mBinding.imgChild6Status);
        mChildStatusList.add(mBinding.imgChild7Status);

        mChildNameList.add(mBinding.tvChild1Name);
        mChildNameList.add(mBinding.tvChild2Name);
        mChildNameList.add(mBinding.tvChild3Name);
        mChildNameList.add(mBinding.tvChild4Name);
        mChildNameList.add(mBinding.tvChild5Name);
        mChildNameList.add(mBinding.tvChild6Name);
        mChildNameList.add(mBinding.tvChild7Name);

        mChildClientCountList.add(mBinding.tvChild1ClientCount);
        mChildClientCountList.add(mBinding.tvChild2ClientCount);
        mChildClientCountList.add(mBinding.tvChild3ClientCount);
        mChildClientCountList.add(mBinding.tvChild4ClientCount);
        mChildClientCountList.add(mBinding.tvChild5ClientCount);
        mChildClientCountList.add(mBinding.tvChild6ClientCount);
        mChildClientCountList.add(mBinding.tvChild7ClientCount);

        mChildGroupList.add(mBinding.groupChild1);
        mChildGroupList.add(mBinding.groupChild2);
        mChildGroupList.add(mBinding.groupChild3);
        mChildGroupList.add(mBinding.groupChild4);
        mChildGroupList.add(mBinding.groupChild5);
        mChildGroupList.add(mBinding.groupChild6);
        mChildGroupList.add(mBinding.groupChild7);
        mChildGroupList.add(mBinding.groupChild8);

        mBinding.imgChild8Background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNextPage();
            }
        });
        mBinding.tvRemainingItemCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNextPage();
            }
        });
        mBinding.tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNextPage();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setDashboardTitle();
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkMapViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Dashboard.GET_NODE_LIST_FOR_CONN_TEST) {
                        if (resultStatus.success) {
                            // 從 node list 取得所有 node did 去打 connection test
                            // (如果 device 的連線狀態有錯, 可以透過 connection test API 去修復)
                            DataManager dataManager = DataManager.getInstance();
                            ArrayList<DeviceInfo> nodeList = dataManager.getNodeList();
                            for (DeviceInfo node : nodeList) {
                                mViewModel.deviceConnectionTest(node.getDid());
                            }

                            // 打完 connection test 後等一下再去 get 資料
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getNetworkMap();
                                }
                            }, 3000);
                        } else {
                            // 打 get node list 失敗, 不繼續打 connection test, 直接 get 資料
                            getNetworkMap();
                        }
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.GET_NETWORK_MAP) {
                        if (resultStatus.success) {
                            // 取得打 API 拿到的資料並更新 UI
                            NetworkMap networkMap = mViewModel.getNetworkMapData();
                            mCurrentNetworkMapLayer.setRootItem(networkMap.getRootDevice());
                            mCurrentNetworkMapLayer.setChildData(convertToChildData(networkMap));
                            mCurrentNetworkMapLayer.setDisplayedPageIndex(0);
                            setData();
                        } else {
                            showMessageDialog("Get Network Map", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    // 顯示當前這層 network map 的資料
    private void setData() {
        mCurrentNetworkMapLayer = mNetworkMapLayerList.get(mNetworkMapLayerList.size() - 1);

        //
        // root item
        //

        DeviceInfo rootItem = mCurrentNetworkMapLayer.getRootItem();
        if (rootItem != null) {
            // name
            mBinding.tvRootName.setText(rootItem.getDeviceLabel());

            // internet status
            if (rootItem.getOnline()) {
                mBinding.imgRootStatus.setImageResource(R.drawable.ic_internet_status_online);
            } else {    // 0 or others
                mBinding.imgRootStatus.setImageResource(R.drawable.ic_internet_status_offline_purple);
            }

            // click event for icon
            mBinding.imgRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, NodeSettingActivity.class);
                    intent.putExtra(AppConstants.EXTRA_DID, rootItem.getDid());
                    startActivityForResult(intent, AppConstants.REQUEST_CODE_NODE_SETTING);
                }
            });
        }

        //
        // child items
        //

        // child item 可以是 node or client, 一頁最多顯示 7 個
        // 若 node + client 超過 7 個, 就會用第 8 個 child item 來表示還有更多 item

        // 先隱藏所有的 child item, 再一個一個畫
        setAllChildItemInvisible();

        ArrayList<ChildItem> childData = mCurrentNetworkMapLayer.getChildData();
        if (childData != null && childData.size() > 0) {
            int displayedPageIndex = mCurrentNetworkMapLayer.getDisplayedPageIndex();
            int startIndex = displayedPageIndex * mItemsPerPage;
            int endIndex = startIndex + (mItemsPerPage - 1);
            if (endIndex > (childData.size() - 1)) {
                endIndex = childData.size() - 1;
            }
            // 畫這一頁的 child item 1 ~ 7
            for (int currentIndex = startIndex; currentIndex <= endIndex; currentIndex++) {
                setChildItemData(currentIndex, childData.get(currentIndex));
            }

            int remainingItemCount = (childData.size() - 1) - endIndex;
            // 當剩餘的 item > 0 時, 代表需要下一頁才能顯示剩餘的 item
            if (remainingItemCount > 0) {
                // 設定 child item 8 為可見, 顯示剩餘的 item 數
                mChildGroupList.get(7).setVisibility(View.VISIBLE);
                mBinding.tvRemainingItemCount.setText("+" + String.valueOf(remainingItemCount));
            } else {
                mChildGroupList.get(7).setVisibility(View.INVISIBLE);
            }
        }
    }

    // 一開始顯示的是第一層 network map, root item 是 router
    private void setInitialData() {
        NetworkMapLayer networkMapLayer = new NetworkMapLayer();

        DeviceInfo rootItem = new DeviceInfo();
        NodeSetInfo nodeSetInfo = mViewModel.getSavedNodeSetInfo();
        if (nodeSetInfo != null) {
            rootItem = nodeSetInfo.getDeviceInfo();
            rootItem.setOnline(nodeSetInfo.getControllerOnline());
            rootItem.setClientCount(0);
        }

        networkMapLayer.setRootItem(rootItem);
        networkMapLayer.setChildData(null);
        networkMapLayer.setDisplayedPageIndex(0);

        mNetworkMapLayerList.add(networkMapLayer);

        setData();
    }

    private void setAllChildItemInvisible() {
        for (int i = 0; i < mChildGroupList.size(); i++) {
            mChildGroupList.get(i).setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < mChildClientCountList.size(); i++) {
            mChildClientCountList.get(i).setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < mChildIconList.size(); i++) {
            mChildIconList.get(i).setOnClickListener(null);
        }
    }

    // 設定單一個 child item 的資料
    private void setChildItemData(int index, ChildItem childItem) {
        int childIndex = index % mItemsPerPage;

        if (childItem.getItemType() == ItemType.NODE) {
            DeviceInfo deviceInfo = (DeviceInfo) childItem.getItemData();

            // set group visible
            mChildGroupList.get(childIndex).setVisibility(View.VISIBLE);

            // image background (用 node count 判斷)
            if (deviceInfo.getNodeCount() > 0) {
                mChildImgBackgroundList.get(childIndex).setImageResource(R.drawable.hexagon_chain);
            } else {
                mChildImgBackgroundList.get(childIndex).setImageResource(R.drawable.hexagon);
            }

            // icon
            switch (deviceInfo.getDeviceType()) {
                case DESKTOP:
                    mChildIconList.get(childIndex).setImageResource(R.drawable.pic_hrn22ac);
                    break;

                case PLUG:
                    mChildIconList.get(childIndex).setImageResource(R.drawable.pic_hex22acp);
                    break;

                case TOWER:
                default:
                    mChildIconList.get(childIndex).setImageResource(R.drawable.pic_hna22ac);
                    break;
            }

            // click event for icon
            mChildIconList.get(childIndex).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NetworkMapLayer nextNetworkMapLayer = new NetworkMapLayer();
                    nextNetworkMapLayer.setRootItem(deviceInfo);
                    nextNetworkMapLayer.setChildData(null);
                    nextNetworkMapLayer.setDisplayedPageIndex(0);
                    mNetworkMapLayerList.add(nextNetworkMapLayer);
                    setData();
                    refreshData(true);
                }
            });

            // internet status
            if (deviceInfo.getOnline()) {
                mChildStatusList.get(childIndex).setImageResource(R.drawable.ic_internet_status_online);
            } else {    // 0 or others
                mChildStatusList.get(childIndex).setImageResource(R.drawable.ic_internet_status_offline_purple);
            }

            // name
            mChildNameList.get(childIndex).setText(deviceInfo.getDeviceLabel());

            // client count
            if (deviceInfo.getClientCount() > 0) {
                mChildClientCountList.get(childIndex).setVisibility(View.VISIBLE);
                mChildClientCountList.get(childIndex).setText(String.valueOf(deviceInfo.getClientCount()));
            } else {
                mChildClientCountList.get(childIndex).setVisibility(View.INVISIBLE);
            }
        } else if (childItem.getItemType() == ItemType.CLIENT) {
            ClientModel clientInfo = (ClientModel) childItem.getItemData();

            // set group visible
            mChildGroupList.get(childIndex).setVisibility(View.VISIBLE);

            // image background
            mChildImgBackgroundList.get(childIndex).setImageResource(R.drawable.hexagon);

            // icon
            String clientType = clientInfo.getClientType();
            if (clientType != null) {
                String lowerCaseClientType = clientType.toLowerCase();
                Map<String, Integer> deviceIconMap = DataManager.getInstance().getOfflineClientIconMap();
                if (deviceIconMap.containsKey(lowerCaseClientType)) {
                    int picId = deviceIconMap.get(lowerCaseClientType);
                    mChildIconList.get(childIndex).setImageResource(picId);
                } else {
                    String connectionType = clientInfo.getConnectionType();
                    if (connectionType.equalsIgnoreCase("Wireless")) {
                        mChildIconList.get(childIndex).setImageResource(R.drawable.ic_generic_wifi_offline);
                    } else if (connectionType.equalsIgnoreCase("Ethernet")) {
                        mChildIconList.get(childIndex).setImageResource(R.drawable.ic_generic_lan_offline);
                    } else {
                        mChildIconList.get(childIndex).setImageResource(R.drawable.ic_generic_wifi_offline);
                    }
                }
            }

            // click event for icon
            mChildIconList.get(childIndex).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Client client = new Client(clientInfo);

                    DataManager dataManager = DataManager.getInstance();
                    dataManager.setManageClient(client);

                    Intent intent = new Intent(NetworkMapActivity.this, ClientSettingsActivity.class);
                    startActivity(intent);
                }
            });

            // internet status
            if (clientInfo.getOnlineStatus() == ConnectionStatus.ONLINE) {
                mChildStatusList.get(childIndex).setImageResource(R.drawable.ic_internet_status_online);
            } else {
                mChildStatusList.get(childIndex).setImageResource(R.drawable.ic_internet_status_offline_purple);
            }

            // name
            mChildNameList.get(childIndex).setText(clientInfo.getName());

            // client count
            mChildClientCountList.get(childIndex).setVisibility(View.INVISIBLE);
        }
    }

    // 當前 root item 的資料不只一頁, 顯示下一頁
    private void showNextPage() {
        mCurrentNetworkMapLayer.setDisplayedPageIndex(mCurrentNetworkMapLayer.getDisplayedPageIndex() + 1);
        setData();
    }

    @Override
    public void onBackPressed() {
        if (mMenuBar != null && mMenuBar.isMainMenuOpened()) {
            // 若有開啟 main menu 時, 按返回鍵關閉 main menu 就好, 不要結束頁面
            mMenuBar.closeMainMenu();
        } else {
            // 打 cloud API 時會處於 loading 狀態, 當使用者按下 back 鍵時, 頁面要不處於 loading 狀態, back 鍵才能有作用
            if (isNotInLoadingStatus()) {
                if (mCurrentNetworkMapLayer.getDisplayedPageIndex() > 0) {
                    // 若當前頁面不是第一頁, 就回到上一頁
                    mCurrentNetworkMapLayer.setDisplayedPageIndex(mCurrentNetworkMapLayer.getDisplayedPageIndex() - 1);
                    setData();
                } else {    // current page index == 0
                    // 當前頁面已經是第一頁, 判斷是要回到上一層 network map 或是要結束頁面

                    if (mNetworkMapLayerList.size() == 1) {
                        // layer array list 現在只有一個 item, 代表已經是在最一開始那一層 => 結束頁面
                        finish();
                    } else {
                        // layer array list size > 1, 代表是在其他層 => 返回上一層 network map

                        // array list 最後一個 item 是當前的 layer, 因為要返回上一層, 所以移除它
                        mNetworkMapLayerList.remove(mNetworkMapLayerList.size() - 1);

                        // 重畫 UI
                        setData();
                        refreshData(true);
                    }
                }
            }
        }
    }

    private ArrayList<ChildItem> convertToChildData(NetworkMap networkMap) {
        ArrayList<ChildItem> childData = new ArrayList<>();
        if (networkMap != null) {
            ArrayList<DeviceInfo> nodeList = networkMap.getNodeList();
            ArrayList<ClientModel> clientList = networkMap.getClientList();

            if (nodeList != null && nodeList.size() > 0) {
                for (int i = 0; i < nodeList.size(); i++) {
                    ChildItem childItem = new ChildItem();
                    childItem.setItemType(ItemType.NODE);
                    childItem.setItemData(nodeList.get(i));

                    childData.add(childItem);
                }
            }
            if (clientList != null && clientList.size() > 0) {
                for (int i = 0; i < clientList.size(); i++) {
                    ClientModel client = clientList.get(i);
                    if (!client.getDisallowed() && client.getOnlineStatus() == ConnectionStatus.ONLINE) {
                        // display only online clients (Bug #8176: https://umedia.plan.io/issues/8176)
                        ChildItem childItem = new ChildItem();
                        childItem.setItemType(ItemType.CLIENT);
                        childItem.setItemData(client);

                        childData.add(childItem);
                    }
                }
            }
        }
        return childData;
    }

    private class NetworkMapLayer {

        // 當前的 root item
        private DeviceInfo rootItem;

        // 所有 child item 的資料 (不論 node or client)
        private ArrayList<ChildItem> childData = new ArrayList<>();

        // 現在顯示到當前 root item 的第幾頁資料
        private int displayedPageIndex = 0;

        DeviceInfo getRootItem() {
            return rootItem;
        }

        void setRootItem(DeviceInfo rootItem) {
            this.rootItem = rootItem;
        }

        ArrayList<ChildItem> getChildData() {
            return childData;
        }

        void setChildData(ArrayList<ChildItem> childData) {
            this.childData = childData;
        }

        int getDisplayedPageIndex() {
            return displayedPageIndex;
        }

        void setDisplayedPageIndex(int displayedPageIndex) {
            this.displayedPageIndex = displayedPageIndex;
        }
    }

    private enum ItemType {
        NODE, CLIENT
    }

    private class ChildItem {

        private ItemType itemType;
        private Object itemData;

        ItemType getItemType() {
            return itemType;
        }

        void setItemType(ItemType itemType) {
            this.itemType = itemType;
        }

        Object getItemData() {
            return itemData;
        }

        void setItemData(Object itemData) {
            this.itemData = itemData;
        }
    }
}