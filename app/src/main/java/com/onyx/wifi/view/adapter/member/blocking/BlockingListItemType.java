package com.onyx.wifi.view.adapter.member.blocking;

public enum BlockingListItemType {
    HEADER,
    ITEM,
    FOOTER
}
