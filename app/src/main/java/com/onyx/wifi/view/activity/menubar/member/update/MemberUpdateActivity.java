package com.onyx.wifi.view.activity.menubar.member.update;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.JsonObject;
import com.onyx.wifi.BuildConfig;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMemberEditorBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.PhotoUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.edit.MemberEditItemType;
import com.onyx.wifi.view.adapter.member.edit.MemberEditListAdapter;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.view.interfaces.member.OnMemberChangedListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.update.MemberUpdateViewModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.onyx.wifi.utility.PhotoUtils.PERMISSIONS_STORAGE;
import static com.onyx.wifi.utility.PhotoUtils.PICK_FROM_CAMERA;
import static com.onyx.wifi.utility.PhotoUtils.PICK_FROM_GALLARY;
import static com.onyx.wifi.utility.PhotoUtils.REQUEST_CAMERA;
import static com.onyx.wifi.utility.PhotoUtils.REQUEST_EXTERNAL_STORAGE;
import static com.onyx.wifi.utility.PhotoUtils.pickFromGallary;

public class MemberUpdateActivity extends BaseMenuActivity implements OnMemberChangedListener, View.OnClickListener {

    private ActivityMemberEditorBinding mBinding;

    private MemberUpdateViewModel mViewModel;

    private MemberEditListAdapter mAdapter;

    private FamilyMember mMember;

    private Uri mOutputFileUri;

    private String mOriginalName = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_editor);

        DataManager dataManager = DataManager.getInstance();
        mMember = dataManager.getManageMember();
        mOriginalName = mMember.getName();

        setupAdapter();

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    //pic coming from camera
                    File imageFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "family_camera.jpg");
                    String imageFilePath = imageFile.getAbsolutePath();
                    mAdapter.updateImage(imageFilePath);
                }
                break;

            case PICK_FROM_GALLARY:
                if (resultCode == Activity.RESULT_OK) {
                    //pick image from gallery
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imageFilePath = cursor.getString(columnIndex);
                    cursor.close();
                    mAdapter.updateImage(imageFilePath);
                }
                break;
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.member_manage_family_member_title);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mMember.getTemporaryName();
                if (!InputUtils.isNameValid(name)) {
                    String title = getString(R.string.member_name_invalid_name);
                    String message = getString(R.string.member_name_rule);

                    showMessageDialog(title, message);

                    return;
                }

                DataManager dataManager = DataManager.getInstance();
                MemberList memberList = dataManager.getMemberList();
                List<MemberModel> familyMemberList = memberList.getFamilyMemberList();

                for(MemberModel model: familyMemberList) {
                    String modelName = model.getName();
                    if (modelName.equalsIgnoreCase(name) &&
                        modelName != mOriginalName) {
                        String title = getString(R.string.member_warning_title);
                        String message = getString(R.string.member_name_duplicate_message);

                        showMessageDialog(title, message);

                        return;
                    }
                }

                List<Client> clientList = mMember.getTemporaryClientList();
                int size = clientList.size();

                if (size < 1) {
                    String title = getString(R.string.member_warning_title);
                    String message = getString(R.string.member_client_required_message);

                    showMessageDialog(title, message);

                    return;
                }

                if (size > 16) {
                    String title = getString(R.string.member_warning_title);
                    String message = getString(R.string.member_client_limit_message);

                    showMessageDialog(title, message);

                    return;
                }

                mViewModel.updateMember(mMember);
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(MemberUpdateViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {

                    if (resultStatus.actionCode == Code.Action.Member.UPDATE_FAMILY_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Family Member", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject response = (JsonObject) resultStatus.data;
                        String memberId = response.get("member_id").getAsString();
                        String fileName = memberId + ".jpg";
                        File avatarFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName);

                        String tempImagePath = mMember.getTemporaryPhotoPath();

                        if (tempImagePath != null) {
                            File tempFile = new File(tempImagePath);
                            if (tempFile != null && tempFile.exists()) {
                                try {
                                    PhotoUtils.copyAvatarImage(tempFile, avatarFile);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Member List", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }
                }
            }
        });
    }

    private void setupAdapter() {
        List<ListItem> listItems = new ArrayList();

        ListItem header = new ListItem<>(MemberEditItemType.HEADER, null);
        listItems.add(header);

        String title = getString(R.string.member_recently_section_title);

        ListItem section = new ListItem<>(MemberEditItemType.SECTION, title);
        listItems.add(section);

        // this member's assigned_client_liet
        List<Client> clientList =  mMember.getAllAllowedClient();//mMember.getAllClients();
        for (Client client : clientList) {
            ListItem item = new ListItem<>(MemberEditItemType.ITEM, client);
            listItems.add(item);
        }

        DataManager dataManager = DataManager.getInstance();

        // all clients from Home Network
        List<Client> recentlyActiveClients = dataManager.getRecentlyActiveAllowedClients();
        for (Client client : recentlyActiveClients) {
            ConnectionStatus connectionStatus = client.getConnectionStatus();

            if (connectionStatus == ConnectionStatus.ONLINE) {
                ListItem item = new ListItem<>(MemberEditItemType.ITEM, client);
                listItems.add(item);
            } else {
                long lastOnlineTimestamp = client.getLastOnlineTimestamp();
                if (!DateUtils.IsLongInactiveClient(lastOnlineTimestamp)) {
                    ListItem item = new ListItem<>(MemberEditItemType.ITEM, client);
                    listItems.add(item);
                }
            }
        }

        String longInactiveTitle = getString(R.string.member_long_inactive_section_title);

        ListItem longInactiveSection = new ListItem<>(MemberEditItemType.SECTION, longInactiveTitle);
        listItems.add(longInactiveSection);

        for (Client client : recentlyActiveClients) {
            long lastOnlineTimestamp = client.getLastOnlineTimestamp();
            if (DateUtils.IsLongInactiveClient(lastOnlineTimestamp)) {
                ListItem item = new ListItem<>(MemberEditItemType.ITEM, client);
                listItems.add(item);
            }
        }

        mAdapter = new MemberEditListAdapter(mMember, listItems, this, this);
    }

    private void showOptionListDialog() {
        OptionListDialog optionListDialog = new OptionListDialog();

        final ArrayList<String> options = new ArrayList<>();
        options.add(getString(R.string.member_new_family_take_photo));
        options.add(getString(R.string.member_new_family_choose_photo));

        ArrayList<Integer> icons = new ArrayList<>();
        icons.add(R.drawable.ic_sheet_camera);
        icons.add(R.drawable.ic_sheet_picture);

        optionListDialog.setOptionString(options);
        optionListDialog.setOptionIconIds(icons);
        optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    if (ActivityCompat.checkSelfPermission(MemberUpdateActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Callback onRequestPermissionsResult interceptadona Activity MainActivity
                        ActivityCompat.requestPermissions(MemberUpdateActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    } else {
                        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "family_camera.jpg");
                        if (Build.VERSION.SDK_INT >= 24) {//android 7.0以上
                            mOutputFileUri = FileProvider.getUriForFile(MemberUpdateActivity.this, BuildConfig.APPLICATION_ID.concat(".provider"), file);
                        } else {
                            mOutputFileUri = Uri.fromFile(file);
                        }
                        PhotoUtils.launchCamera(MemberUpdateActivity.this, mOutputFileUri);
                    }
                }

                if (position == 1) {
                    pickFromGallary(MemberUpdateActivity.this);
                }


                optionListDialog.dismiss();
            }
        });
        showDialog(optionListDialog);
    }

    private boolean isMemberChanged() {
        return mMember.getTemporaryPhotoPath() != null ||
                !mMember.getTemporaryName().equalsIgnoreCase(mMember.getName()) ||
                mMember.checkClientListChanged();
    }

    @Override
    public void onMemberRollback() {
        if (isMemberChanged()) {
            return;
        }

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onMemberChanged() {
        if (!isMemberChanged()) {
            return;
        }

        mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        int permission = ActivityCompat.checkSelfPermission(MemberUpdateActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    MemberUpdateActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }

        showOptionListDialog();
    }
}
