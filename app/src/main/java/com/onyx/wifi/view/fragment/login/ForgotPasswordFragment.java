package com.onyx.wifi.view.fragment.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentForgotPasswordBinding;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.login.ForgotPasswordListener;
import com.onyx.wifi.viewmodel.login.ForgotPasswordViewModel;

public class ForgotPasswordFragment extends BaseFragment {

    private FragmentForgotPasswordBinding mBinding;
    private ForgotPasswordViewModel mViewModel;

    private ForgotPasswordListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ForgotPasswordListener) {
            mEventListener = (ForgotPasswordListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ForgotPasswordListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        mBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getForgetPasswordUser().setEmail(editable.toString().trim());
                checkEmailDataFilled();
            }
        });
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onBack();
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mViewModel.isEmailDataValid()) {
                    mViewModel.sendCode();
                } else {
                    showMessageDialog("Forgot Password", mViewModel.getInputErrorMsg());
                }
            }
        });
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(ForgotPasswordViewModel.class);
    }

    private void checkEmailDataFilled() {
        if (mViewModel.isEmailDataFilled()) {
            mBinding.btnContinue.setVisibility(View.VISIBLE);
        } else {
            mBinding.btnContinue.setVisibility(View.INVISIBLE);
        }
    }

    private void setData() {
        mBinding.etEmail.setText(mViewModel.getForgetPasswordUser().getEmail());
        checkEmailDataFilled();
    }
}
