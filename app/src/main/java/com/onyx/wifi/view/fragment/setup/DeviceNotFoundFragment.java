package com.onyx.wifi.view.fragment.setup;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentDeviceNotFoundBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;

public class DeviceNotFoundFragment extends BaseFragment {

    private FragmentDeviceNotFoundBinding mBinding;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_device_not_found, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
    }

    private void setView() {
        if (DataManager.getInstance().getSetupMode() == AppConstants.SetupMode.ROUTER_RESETUP) {
            // resetup router 時不能透過 WIFI 的方式
            mBinding.btnAlternative.setVisibility(View.GONE);
        } else {
            // alternative setup 按鈕先隱藏, 若 BLE scan 失敗兩次再顯示
            if (DataManager.getInstance().getSetupFailCount() < 2) {
                mBinding.btnAlternative.setVisibility(View.GONE);
            } else {
                mBinding.btnAlternative.setVisibility(View.VISIBLE);
            }
            mBinding.btnAlternative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mEventListener.onContinue(Code.Action.DeviceSetup.ALTERNATIVE_SETUP);
                }
            });
        }

        mBinding.btnMoreHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onContinue(Code.Action.DeviceSetup.MORE_HELP);
            }
        });
        mBinding.btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onContinue(Code.Action.DeviceSetup.RETRY);
            }
        });
        mEventListener.setSupportView(mBinding.tvSupport);
    }
}
