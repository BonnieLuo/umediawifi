package com.onyx.wifi.view.fragment.setup.router;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentCreateNetworkBinding;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.setup.DeviceSetupViewModel;

public class CreateNetworkFragment extends BaseFragment {

    private FragmentCreateNetworkBinding mBinding;
    private DeviceSetupViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_network, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewModel();
        setView();

        // 一開始看不到進度, 到開始 get provision state 時再顯示進度
        mBinding.tvProgress.setText("");
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(DeviceSetupViewModel.class);
    }

    private void setView() {
        if (mActivity instanceof BaseDeviceSetupActivity) {
            ((BaseDeviceSetupActivity) mActivity).setDeviceImage(mViewModel.getSetupDevice(), mBinding.imgDevice);
        }
    }

    public void updateProgress(int progress) {
        LogUtils.trace(progress + "%");
        mBinding.tvProgress.setText(progress + "%");
    }
}
