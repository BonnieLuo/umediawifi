package com.onyx.wifi.view.customized;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class NoUnderlineSpan extends ClickableSpan {

    public NoUnderlineSpan() {
        super();
    }

    public void onClick(View view) {

    }

    // override updateDrawState
    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(false); // set to false to remove underline
    }
}
