package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityLanIpInformationBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

public class LANIPInformationActivity extends BaseMenuActivity {
    private ActivityLanIpInformationBinding mBinding;
    private NetworkSetting mOriginalNetworkSetting;
    private NetworkSettingViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_lan_ip_information);
        mOriginalNetworkSetting = DataManager.getInstance().getNetworkSetting();

        setViewModel();
        setView();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {

            }
        });
    }


    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // MAC Address
        mBinding.tvMACAddrValue.setText(mOriginalNetworkSetting.getDevMac());
        // LAN IP Address
        mBinding.tvLANIPAddrValue.setText(mOriginalNetworkSetting.getLanIp());
        // Subnet Mask??
        mBinding.tvSubnetMaskValue.setText(mOriginalNetworkSetting.getNetMask());
        // DHCP IP Address Range
        mBinding.tvDHCPIPAddrRangeValue.setText(mOriginalNetworkSetting.getDhcpRange());
        // DHCP Client Lease Time
        String clientLeaseTime = mOriginalNetworkSetting.getClientLeaseTime();
        if (StringUtils.isNullOrEmpty(clientLeaseTime)) {
            mBinding.tvDHCPClientLeaseTimeValue.setText("");
        }else {
            int leaseTimeInSeconds = Integer.parseInt(clientLeaseTime);
            mBinding.tvDHCPClientLeaseTimeValue.setText(DateUtils.secToTimeFormat(leaseTimeInSeconds));
        }

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.lan_ip_information_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
