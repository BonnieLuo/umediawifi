package com.onyx.wifi.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;

import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.dialog.BaseDialog;

public class BaseFragment extends Fragment {

    protected FragmentActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final View rootView = view;
        /* Fragment 實作時可能運用的場景: 將多個 Fragment 加入一個 Activity 的 FrameLayout 中(Ex: 帳號登入相關頁面).
           由於使用了 FrameLayout, Fragment 之間會完全重疊, 使用者看不到疊在下面的 Fragment, 但實測時卻發生了「可以點擊到疊在下面的 Fragment 的 EditText」的情況.
           解法: 將 Fragment root view 設定為可點擊, 可將點擊事件攔截在該 Fragment 自身而不繼續往下傳遞, 就不會觸發到疊在下面的 Fragment 了. */
        rootView.setClickable(true);

        /* 一進入一個頁面, 若有 EditText 則會自動獲取焦點並彈出鍵盤(系統預設行為會自動尋找第一個可獲取焦點的元件).
           解法: 為了讓 EditText 不獲取焦點及不彈出鍵盤, 將最外層的 root view 設定為可獲取焦點, 讓 root view 早於 EditText 先取得焦點.
           該解法寫在這裡 (BaseFragment) 有效用, 但同樣的解法寫在 BaseActivity 裡面不知道為何會沒效用, 因此若需為 Activity 做同樣設定 (讓 EditText 不獲取焦點及不彈出鍵盤),
           必須從 xml 裡面設定, 將 EditText 的 parent layout 設定 android:focusable="true" 及 android:focusableInTouchMode="true", 例如 activity_sign_in.xml */
        rootView.setFocusable(true);
        rootView.setFocusableInTouchMode(true);

        // 為最外層的 root view 設定 touch 事件, 實現「點選 EditText 以外的地方即自動隱藏鍵盤」.
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                rootView.requestFocus();
                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        CommonUtils.hideKeyboard(mActivity);
    }

    protected void showKeyboard(View view) {
        CommonUtils.showKeyboard(mActivity, view);
    }

    protected void showMessageDialog(String title, String msg) {
        if (mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).showMessageDialog(title, msg);
        }
    }

    protected void showDialog(BaseDialog dialog) {
        if (mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).showDialog(dialog);
        }
    }
}
