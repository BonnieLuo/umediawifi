package com.onyx.wifi.view.fragment.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentPasswordResetCodeBinding;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.view.interfaces.TextChangedListener;
import com.onyx.wifi.view.listener.PinCodeOnKeyListener;
import com.onyx.wifi.view.listener.PinCodeTextWatcher;
import com.onyx.wifi.viewmodel.interfaces.login.ForgotPasswordListener;
import com.onyx.wifi.viewmodel.login.ForgotPasswordViewModel;

public class PasswordResetCodeFragment extends BaseFragment {

    private FragmentPasswordResetCodeBinding mBinding;
    private ForgotPasswordViewModel mViewModel;

    private ForgotPasswordListener mEventListener;

    private EditText[] mEtDigits;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ForgotPasswordListener) {
            mEventListener = (ForgotPasswordListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ForgotPasswordListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_password_reset_code, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
    }

    private void setView() {
        mEtDigits = new EditText[]{mBinding.etDigit1, mBinding.etDigit2, mBinding.etDigit3, mBinding.etDigit4};
        mBinding.etDigit1.addTextChangedListener(new PinCodeTextWatcher(mActivity, mEtDigits, 0, mTextChangedListener));
        mBinding.etDigit2.addTextChangedListener(new PinCodeTextWatcher(mActivity, mEtDigits, 1, mTextChangedListener));
        mBinding.etDigit3.addTextChangedListener(new PinCodeTextWatcher(mActivity, mEtDigits, 2, mTextChangedListener));
        mBinding.etDigit4.addTextChangedListener(new PinCodeTextWatcher(mActivity, mEtDigits, 3, mTextChangedListener));
        mBinding.etDigit1.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 0));
        mBinding.etDigit2.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 1));
        mBinding.etDigit3.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 2));
        mBinding.etDigit4.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 3));

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onBack();
            }
        });
        mBinding.btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.resendCode();
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onResetCodeContinue();
            }
        });
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(ForgotPasswordViewModel.class);
    }

    private TextChangedListener mTextChangedListener = new TextChangedListener() {
        @Override
        public void afterTextChangedDone() {
            String resetCode = "";
            for (EditText etDigit : mEtDigits) {
                resetCode = resetCode + etDigit.getText().toString();
            }
            if (resetCode.length() == 4) {
                mBinding.btnResend.setVisibility(View.INVISIBLE);
                mBinding.btnContinue.setVisibility(View.VISIBLE);

                mViewModel.getForgetPasswordUser().setResetCode(resetCode);
                LogUtils.trace("reset code = " + resetCode);
            } else {
                mBinding.btnResend.setVisibility(View.VISIBLE);
                mBinding.btnContinue.setVisibility(View.INVISIBLE);
            }
        }
    };
}
