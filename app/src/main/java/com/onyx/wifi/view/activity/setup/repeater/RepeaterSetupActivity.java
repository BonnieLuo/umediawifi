package com.onyx.wifi.view.activity.setup.repeater;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityRepeaterSetupBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.activity.mainmenu.troubleshooting.PlacementGuideActivity;
import com.onyx.wifi.view.activity.setup.DeviceSetupSearchActivity;
import com.onyx.wifi.view.fragment.setup.WaitForLedFragment;
import com.onyx.wifi.view.fragment.setup.repeater.PlugInDeviceFragment;
import com.onyx.wifi.view.fragment.setup.router.FindTheButtonFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;

public class RepeaterSetupActivity extends BaseDeviceSetupActivity implements DeviceSetupListener {

    private PlugInDeviceFragment mPlugInDeviceFragment;
    private WaitForLedFragment mWaitForLedFragment;
    private FindTheButtonFragment mFindTheButtonFragment;

    private ActivityRepeaterSetupBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setSetupFlow(AppConstants.SetupFlow.BLE);
        dataManager.setSetupMode(AppConstants.SetupMode.REPEATER);
        dataManager.setSetupFailCount(0);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_repeater_setup);
        setView();
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mWaitForLedFragment = (WaitForLedFragment) mFragmentManager.findFragmentByTag(WaitForLedFragment.class.getName());
            mFindTheButtonFragment = (FindTheButtonFragment) mFragmentManager.findFragmentByTag(FindTheButtonFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            // 顯示第一頁, 隱藏其他頁
            if (mWaitForLedFragment != null) {
                fragmentTransaction.hide(mWaitForLedFragment);
            }
            if (mFindTheButtonFragment != null) {
                fragmentTransaction.hide(mFindTheButtonFragment);
            }
            fragmentTransaction.show(mPlugInDeviceFragment);
            fragmentTransaction.commit();
        } else {
            mPlugInDeviceFragment = new PlugInDeviceFragment();
            addFragmentAsFirstPage(R.id.flContainer, mPlugInDeviceFragment, PlugInDeviceFragment.class.getName());
        }
    }

    private void setView() {
        setTitleBar();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.umedia_device_setup_title);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        setLeftButtonHome();
    }

    private void setViewModel() {
        setLoadingObserve(mDeviceSetupViewModel, this, mBinding.loading);
        mDeviceSetupViewModel.setSetupMode(DataManager.getInstance().getSetupMode());
        mDeviceSetupViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_PROVISION_TOKEN) {
                        if (resultStatus.success) {
                            // setup repeater 時會需要 router 的 wireless setting
                            // 為免資料不一致造成 setup repeater 失敗, 故再打一次 API 向 cloud 拿最新的資料
                            mDeviceSetupViewModel.getWirelessSetting();
                        } else {
                            showMessageDialog("Get Provision Token", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_WIRELESS_SETTING) {
                        if (resultStatus.success) {
                            startSearchDeviceActivity();
                        } else {
                            showMessageDialog("Get Wireless Setting", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // repeater setup 過程中, 這頁 RepeaterSetupActivity 不會關閉, 且會停留在步驟說明的最後一個畫面, 若過程中有失敗就會回到這頁重來
        // 而 repeater 新增成功時, 這頁還在, 若按了 add another device 按鈕, 會開啟現在這頁, 以 onNewIntent() 的方式回來
        // 此時會看到頁面停留在步驟說明的最後一個畫面, 但因為是要 add another device, 包含說明頁面的整個流程都要重來才對
        // 所以若判斷到 add another device flag = true, 需回到所有步驟說明的第一個畫面
        boolean addAnotherDevice = intent.getBooleanExtra(AppConstants.EXTRA_ADD_ANOTHER_DEVICE, false);
        if (addAnotherDevice) {
            // back to first page
            int count = mFragmentManager.getBackStackEntryCount();
            for (int i = 0; i < count; i++) {
                onBackPressed();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mFragmentManager.getBackStackEntryCount() == 0) {   // 第一個畫面
            // 在第一個畫面按 Android back 鍵的行為跟按左上角 home icon 的行為一樣, 都是要回到 Dashboard
            // 因為這一頁的前一頁有可能是 RouterSetupActivity (這種情況發生在: 當 Initial Setup 成功後直接按 add another device)
            // 假設此時按了 Android back 鍵時是預設行為 (super.onBackPressed(), 會把這頁關閉), 就會回到 Initial Setup 頁面
            // 這會形成錯誤的流程, 應該要回到 Dashboard 才對
            startDashboardActivity(RepeaterSetupActivity.this);
            finish();
        } else {
            super.onBackPressed();
        }

        if (mFragmentManager.getBackStackEntryCount() == 0) {
            // 回到上一頁之後再判斷一次 back stack entry count, 若等於 0 代表已回到了第一頁, 此時左上角的按鈕要變回 home 圖示.
            setLeftButtonHome();
        }
    }

    @Override
    public void onContinue(int nextStep) {
        LogUtils.trace("next step = " + nextStep);
        switch (nextStep) {
            case Code.Action.DeviceSetup.PLACEMENT_ASSISTANT:
                Intent intent = new Intent(mContext, PlacementGuideActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, true);
                startActivity(intent);
                break;

            case Code.Action.DeviceSetup.WAIT_FOR_LED:
                mWaitForLedFragment = new WaitForLedFragment();
                addFragment(R.id.flContainer, mWaitForLedFragment, WaitForLedFragment.class.getName());
                setLeftButtonBack();
                break;

            case Code.Action.DeviceSetup.FIND_THE_BUTTON:
                mFindTheButtonFragment = new FindTheButtonFragment();
                addFragment(R.id.flContainer, mFindTheButtonFragment, FindTheButtonFragment.class.getName());
                break;

            case Code.Action.DeviceSetup.FIND_THE_BUTTON_GOT_IT:
                onBackPressed();
                break;

            case Code.Action.DeviceSetup.SEARCH_DEVICE:
                if (isAllPermissionGranted()) {
                    LogUtils.trace("all permission granted");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // Android 6.0 以上需開啟手機的 GPS 定位, 否則收不到藍牙掃描的結果
                        if (isLocationEnabled()) {
                            getProvisionToken();
                        } else {
                            if (mLocationServiceMsgDialog != null && !mLocationServiceMsgDialog.isAdded()) {
                                showDialog(mLocationServiceMsgDialog);
                            }
                        }
                    } else {
                        getProvisionToken();
                    }
                } else {
                    if (mBluetoothPermissionMsgDialog != null && !mBluetoothPermissionMsgDialog.isAdded()) {
                        showDialog(mBluetoothPermissionMsgDialog);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void setSupportView(TextView textView) {
        setSupportClickMessage(textView);
    }

    private void startSearchDeviceActivity() {
        startActivity(new Intent(mContext, DeviceSetupSearchActivity.class));
        // 這裡不用呼叫 finish()
        // 因 device setup 頁面包含多個步驟說明, 到最後一步並 continue 進入後續流程 (藍牙掃描, 連接裝置...) 之後,
        // 若過程中有失敗需要重來時, 都需要回到這頁步驟說明的最後一步(wait for LED), 故進入後續流程時不需要將這頁關閉
    }

    private void setLeftButtonHome() {
        // 流程第一頁, 左上角的按鈕是 home 圖示, 按下去回到 dashboard 頁面
        mBinding.titleBar.setLeftButton(R.drawable.ic_home, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDashboardActivity(RepeaterSetupActivity.this);
                finish();
            }
        });
    }

    private void setLeftButtonBack() {
        // 若非流程第一頁, 左上角的按鈕是 back 圖示, 按下去是返回上一頁
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
