package com.onyx.wifi.view.activity.mainmenu.accountsetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityChangePasswordBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.AccountSettingViewModel;

public class ChangePasswordActivity extends BaseMenuActivity {
    private ActivityChangePasswordBinding mBinding;
    private AccountSettingViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        setViewModel();
        setView();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(AccountSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.UPDATE_PASSWORD) {
                        if (resultStatus.success) {
                            CommonUtils.toast(mContext, getString(R.string.msg_update_password_success));
                            final MessageDialog messageDialog = new MessageDialog();
                            messageDialog.setTitle("Change Password Successfully");
                            messageDialog.setContent("Please sign in again.");
                            messageDialog.setOkButton(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    messageDialog.dismiss();
                                    mAccountManager.signOut();
                                    startLoginHomeActivity(ChangePasswordActivity.this);
                                    finish();
                                }
                            });
                            showDialog(messageDialog);

                        } else {
                            showMessageDialog("Update Password", resultStatus.errorMsg);
                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        });
    }

    private void checkPasswordDataFilled() {
        if (mViewModel.isPasswordDataFilled()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getAccountSettingUser().setPassword(editable.toString());
                checkPasswordDataFilled();
            }
        });
        mBinding.etConfirmNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getAccountSettingUser().setConfirmPassword(editable.toString());
                checkPasswordDataFilled();
            }
        });
    }

//    @Override
//    public void onBackPressed() {
//        startAccountSettingActivity(this);
//        finish();
//    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.account_setting_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.isPasswordDataValid()) {
                    mViewModel.updatePassword();
                } else {
                    //CommonUtils.toast(mContext, mViewModel.getInputErrorMsg());
                    showMessageDialog("Update Password", mViewModel.getInputErrorMsg());
                    mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
