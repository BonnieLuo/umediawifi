package com.onyx.wifi.view.activity.mainmenu.wirelesssetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityGuestNetworkBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.GuestNetworkSetting;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.WirelessSettingViewModel;

public class GuestNetworkActivity extends BaseWirelessSettingActivity {

    private ActivityGuestNetworkBinding mBinding;
    private WirelessSettingViewModel mViewModel;

    private GuestNetworkSetting mOriginalGuestNetworkData;
    private GuestNetworkSetting mTempGuestNetworkData;

    private String mInputErrorMsg = "";

    private void initGuestNetworkData(GuestNetworkSetting guestNetworkSetting) {
        guestNetworkSetting.setEnable(false);
        guestNetworkSetting.setSsid(InputUtils.getDefaultGuestNetworkSsid());
        guestNetworkSetting.setPwd("");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOriginalGuestNetworkData = DataManager.getInstance().getGuestNetworkSetting();
        mTempGuestNetworkData = DataManager.getInstance().getGuestNetworkSetting();
        if (mOriginalGuestNetworkData == null || !InputUtils.isNotEmpty(mOriginalGuestNetworkData.getSsid())) {
            // 若資料為 null 或 SSID 沒有值時才自動產生預設資料, 否則 UI 顯示的就是 cloud 回傳的資料
            // (若 enable -> disable -> 要再次 enable 時就會自動帶入之前設定過的資料)
            mOriginalGuestNetworkData = new GuestNetworkSetting();
            mTempGuestNetworkData = new GuestNetworkSetting();
            initGuestNetworkData(mOriginalGuestNetworkData);
            initGuestNetworkData(mTempGuestNetworkData);
        }
        LogUtils.trace("GuestNetwork", mOriginalGuestNetworkData.toString());

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_guest_network);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // Enabled/Disabled:
        mBinding.cbEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mTempGuestNetworkData.setEnable(isChecked);
                mBinding.tvGuestEnable.setText((isChecked) ? R.string.enabled : R.string.disabled);
                mBinding.groupEnable.setVisibility((isChecked) ? View.VISIBLE : View.INVISIBLE);

                checkIfDataChanged();
            }
        });

        // [按鈕] SSID 的筆型圖示
        mBinding.btnEditGuestSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEditMode(mBinding.etGuestSsidValue);
            }
        });

        // [文字元件] SSID, 設置各項事件
        mBinding.etGuestSsidValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            // 監聽 EditText 的焦點以判斷是否需要結束編輯模式
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkFocusChange(mBinding.etGuestSsidValue, hasFocus);
            }
        });

        mBinding.etGuestSsidValue.addTextChangedListener(new TextWatcher() {
            // 監聽 EditText 的文字改變
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // 當使用者輸入完要將值儲存到 temp 物件, 並判斷是否需要顯示勾勾
                String ssid = s.toString().trim();
                mTempGuestNetworkData.setSsid(ssid);
                checkIfDataChanged();
            }
        });

        mBinding.etGuestSsidValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            // 監聽鍵盤 enter 鍵的事件
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // 在 layout 裡有設定 EditText 的 imeOptions="actionDone", 故這裡是判斷 IME_ACTION_DONE
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    exitEditMode(mBinding.etGuestSsidValue);
                }
                return false;
            }
        });

        // [按鈕] password 的筆型圖示
        mBinding.btnEditGuestPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEditMode(mBinding.etGuestPwdValue);
            }
        });

        // [文字元件] password, 設置各項事件 (同上述 SSID 文字元件的說明)
        mBinding.etGuestPwdValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkFocusChange(mBinding.etGuestPwdValue, hasFocus);
            }
        });

        mBinding.etGuestPwdValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String password = s.toString().trim();
                mTempGuestNetworkData.setPwd(password);
                checkIfDataChanged();
            }
        });

        mBinding.etGuestPwdValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    exitEditMode(mBinding.etGuestPwdValue);
                }
                return false;
            }
        });
        // [按鈕] password 的眼睛圖示
        mBinding.btnShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setPasswordVisible(mBinding.etGuestPwdValue, isChecked);
            }
        });
        // [按鈕] Generate Random Password
        mBinding.btnGeneratePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.etGuestPwdValue.setText(InputUtils.generateNetworkPassword());
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Guest Network");
        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDataValid()) {
                    exitEditMode(mBinding.etGuestSsidValue);
                    exitEditMode(mBinding.etGuestPwdValue);
                    mViewModel.updateGuestNetwork(mTempGuestNetworkData);
                    LogUtils.trace("guestNetwork", mTempGuestNetworkData.toString());
                } else {
                    showMessageDialog("Invalid Input", mInputErrorMsg);
                }
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(WirelessSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.WirelessSetting.UPDATE_GUEST_NETWORK_SETTING) {
                        if (resultStatus.success) {
                            finish();
                        } else {
                            showMessageDialog("Update Guest Network", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void setData() {

        mBinding.cbEnable.setChecked(mOriginalGuestNetworkData.getEnable());

        // set original ssid
        mBinding.etGuestSsidValue.setText(mOriginalGuestNetworkData.getSsid());

        // set original password
        mBinding.etGuestPwdValue.setText(mOriginalGuestNetworkData.getPwd());

        // set as multi-line display mode
        setMultiLineDisplay(mBinding.etGuestSsidValue);
        setMultiLineDisplay(mBinding.etGuestPwdValue);

        // set original enable state
        if (mOriginalGuestNetworkData.getEnable()) {
            mBinding.tvGuestEnable.setText(getString(R.string.enabled));
            mBinding.groupEnable.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvGuestEnable.setText(getString(R.string.disabled));
            mBinding.groupEnable.setVisibility(View.INVISIBLE);
        }
    }

    private void enterEditMode(EditText editText) {
        editText.setEnabled(true);
        editText.requestFocus();
        showKeyboard(editText);
        showKeyboard(editText);
        editText.setSelection(editText.getEditableText().length());
    }

    private void exitEditMode(EditText editText) {
        editText.setEnabled(false);
    }

    private void checkFocusChange(EditText editText, boolean hasFocus) {
        // EditText 失去焦點時 (例如: 點擊 EditText 以外的區域) 要結束編輯模式
        if (!hasFocus) {
            exitEditMode(editText);
        }
    }

    private void checkIfDataChanged() {
        boolean isChanged = false;

        if (mTempGuestNetworkData.getEnable() != mOriginalGuestNetworkData.getEnable()) {
            isChanged = true;
        }

        if (!mTempGuestNetworkData.getSsid().equals(mOriginalGuestNetworkData.getSsid())) {
            isChanged = true;
        }

        if (!mTempGuestNetworkData.getPwd().equals(mOriginalGuestNetworkData.getPwd())) {
            isChanged = true;
        }

        mBinding.titleBar.setRightButtonVisibility((isChanged) ? View.VISIBLE : View.INVISIBLE);
    }

    private boolean isDataValid() {
        String msgSsidInvalid = InputUtils.getInvalidWifiSsidMessage("SSID");
        String msgPwdInvalid = InputUtils.getInvalidWifiPasswordMessage("password");
        String msgWhitespace = mContext.getString(R.string.err_ssid_invalid_space);

        //
        //  check for ssid and password
        //
        if (!InputUtils.isWifiSsidValid(mTempGuestNetworkData.getSsid())) {
            mInputErrorMsg = msgSsidInvalid;
            return false;
        } else if (!InputUtils.isWifiPasswordValid(mTempGuestNetworkData.getPwd())) {
            mInputErrorMsg = msgPwdInvalid;
            return false;
        } else if (InputUtils.containsWhitespace(mTempGuestNetworkData.getSsid())) {
            // TODO: constraint to be removed if Bug #7076 is solved
            // https://umedia.plan.io/issues/7076?pn=1#change-28750
            mInputErrorMsg = msgWhitespace;
            return false;
        }
        return true;
    }
}
