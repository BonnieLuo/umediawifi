package com.onyx.wifi.view.activity.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySignInBinding;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.customized.NoUnderlineSpan;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.login.SignInViewModel;

public class SignInActivity extends BaseActivity {

    private ActivitySignInBinding mBinding;
    private SignInViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignInUser().setEmail(editable.toString().trim());
                checkDataFilled();
            }
        });
        mBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignInUser().setPassword(editable.toString());
                checkDataFilled();
            }
        });
        mBinding.btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternetStatus()) {
                    mViewModel.signIn();
                }
            }
        });

        // Set hyperlink of forgot password
        SpannableString msgForgetPassword = new SpannableString(getString(R.string.sign_in_forget_password));
        String msgClickHere = "Click here.";
        int startIndex = msgForgetPassword.toString().indexOf(msgClickHere);
        int endIndex = startIndex + msgClickHere.length();
        NoUnderlineSpan clickableSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, ForgotPasswordActivity.class));
            }
        };
        msgForgetPassword.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgForgetPassword.setSpan(new ForegroundColorSpan(Color.WHITE), startIndex, endIndex, 0);
        mBinding.tvForgetPassword.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvForgetPassword.setText(msgForgetPassword, TextView.BufferType.SPANNABLE);
        mBinding.tvForgetPassword.setHighlightColor(Color.TRANSPARENT);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SignInViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.SIGN_IN) {
                        if (resultStatus.success) {
                            afterSignInDone(SignInActivity.this);
                        } else {
                            /* 登入分成兩步驟:
                                (1) Firebase 登入.
                                (2) Firebase 登入成功取得 user token 後, 打 Cloud API 取得使用者資訊.
                                兩者皆成功後才算登入成功, LoginHomeActivity 頁面會將使用者引導至 Dashboard 畫面.
                                LoginHomeActivity 判斷到 Firebase user 是登入狀態就會將使用者引導至 Dashboard 畫面,
                                故若步驟 (2) 打 Cloud API 失敗, 需將 Firebase user 登出, 否則會被誤判斷. */
                            mAccountManager.signOut();

                            showMessageDialog("Sign in", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void checkDataFilled() {
        if (mViewModel.isDataFilled()) {
            mBinding.btnSignIn.setVisibility(View.VISIBLE);
        } else {
            mBinding.btnSignIn.setVisibility(View.INVISIBLE);
        }
    }

    private void setData() {
        mBinding.etEmail.setText(mViewModel.getSignInUser().getEmail());
        mBinding.etPassword.setText(mViewModel.getSignInUser().getPassword());
        checkDataFilled();
    }
}
