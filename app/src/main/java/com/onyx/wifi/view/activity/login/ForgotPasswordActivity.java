package com.onyx.wifi.view.activity.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityForgotPasswordBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.fragment.login.ForgotPasswordFragment;
import com.onyx.wifi.view.fragment.login.PasswordResetCodeFragment;
import com.onyx.wifi.view.fragment.login.ResetPasswordFragment;
import com.onyx.wifi.viewmodel.interfaces.login.ForgotPasswordListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.login.ForgotPasswordViewModel;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordListener {

    private ForgotPasswordFragment mForgotPasswordFragment;
    private PasswordResetCodeFragment mPasswordResetCodeFragment;
    private ResetPasswordFragment mResetPasswordFragment;

    private ActivityForgotPasswordBinding mBinding;
    private ForgotPasswordViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mForgotPasswordFragment = (ForgotPasswordFragment) mFragmentManager.findFragmentByTag(ForgotPasswordFragment.class.getName());
            mPasswordResetCodeFragment = (PasswordResetCodeFragment) mFragmentManager.findFragmentByTag(PasswordResetCodeFragment.class.getName());
            mResetPasswordFragment = (ResetPasswordFragment) mFragmentManager.findFragmentByTag(ResetPasswordFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            // 顯示第一頁, 隱藏其他頁
            if (mPasswordResetCodeFragment != null) {
                fragmentTransaction.hide(mPasswordResetCodeFragment);
            }
            if (mResetPasswordFragment != null) {
                fragmentTransaction.hide(mResetPasswordFragment);
            }
            fragmentTransaction.show(mForgotPasswordFragment);
            fragmentTransaction.commit();
        } else {
            mForgotPasswordFragment = new ForgotPasswordFragment();
            addFragmentAsFirstPage(R.id.flContainer, mForgotPasswordFragment, ForgotPasswordFragment.class.getName());
        }
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.FORGOT_PASSWORD_SEND_CODE) {
                        if (resultStatus.success) {
                            sendCodeSuccess();
                        } else {
                            showMessageDialog("Forgot Password - Send Code", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.UserAccount.FORGOT_PASSWORD_RESEND_CODE) {
                        if (resultStatus.success) {
                            CommonUtils.toast(mContext, getString(R.string.msg_resend_reset_pwd_code_success));
                        } else {
                            showMessageDialog("Forgot Password - Resend Code", resultStatus.errorMsg);
                        }
                    } else if (resultStatus.actionCode == Code.Action.UserAccount.FORGOT_PASSWORD_VERIFY_RESET_PASSWORD) {
                        if (resultStatus.success) {
                            afterSignInDone(ForgotPasswordActivity.this);
                        } else {
                            showMessageDialog("Forgot Password - Reset Password", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onResetCodeContinue() {
        mResetPasswordFragment = new ResetPasswordFragment();
        addFragment(R.id.flContainer, mResetPasswordFragment, ResetPasswordFragment.class.getName());
    }

    public void sendCodeSuccess() {
        mPasswordResetCodeFragment = new PasswordResetCodeFragment();
        addFragment(R.id.flContainer, mPasswordResetCodeFragment, PasswordResetCodeFragment.class.getName());
    }
}
