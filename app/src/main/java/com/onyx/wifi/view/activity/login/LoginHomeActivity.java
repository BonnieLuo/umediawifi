package com.onyx.wifi.view.activity.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;

import com.google.firebase.auth.FirebaseUser;
import com.onyx.wifi.BuildConfig;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.CloudManager;
import com.onyx.wifi.databinding.ActivityLoginHomeBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;

public class LoginHomeActivity extends BaseActivity {

    private ActivityLoginHomeBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_home);
        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        FirebaseUser user = mAccountManager.getUser();
        // 判斷 Firebase 和 UMEDIA cloud 都有登入才算登入
        if (user != null && DataManager.getInstance().getUserInfo() != null) {
            /**
             * LoginHomeActivity 是 launcher 頁面
             * 1. 使用者在登入 App 的狀態下離開 App, 再次開啟 App 時, 由於已經是登入狀態, 不需再登入, 直接進入 Dashboard 頁面.
             * 2. 登入頁面/email 認證頁面動作完成後, 會結束自身頁面, 疊在下面的 LoginHomeActivity 會回到前景.
             *      此時若判斷到是登入狀態, 自然會進入 Dashboard 頁面, 不需要由登入頁面/email 認證頁面主動導引.
             *      (錯誤行為: 若由登入頁面/email 認證頁面主動導引進入 Dashboard 頁面, LoginHomeActivity 仍保存在 stack 裡,
             *      此時若在 Dashboard 頁面按 Android back 鍵, 會退回到 LoginHomeActivity, 然後由於 user 不為 null, 又會再次開啟 Dashboard 頁面.)
             **/
            startDashboardActivity(LoginHomeActivity.this);
            finish();
        } else {
            LogUtils.trace("Firebase user is null: " + (user == null) + ", user info is null: " + (DataManager.getInstance().getUserInfo() == null));
            mAccountManager.signOut();
        }
    }

    private void setView() {
        mBinding.btnSocialSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SocialSignInActivity.class));
                DataManager.getInstance().setIsSocialSignIn(true);
                //CommonUtils.setSharedPrefData(AppConstants.IS_SOCIAL_SIGN_IN, "true");
            }
        });
        mBinding.btnSignInUmedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SignInActivity.class));
                DataManager.getInstance().setIsSocialSignIn(false);
                //CommonUtils.setSharedPrefData(AppConstants.IS_SOCIAL_SIGN_IN, "false");
            }
        });
        mBinding.btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SignUpActivity.class));
                DataManager.getInstance().setIsSocialSignIn(false);
                //CommonUtils.setSharedPrefData(AppConstants.IS_SOCIAL_SIGN_IN, "false");
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (BuildConfig.IS_RDQA) {  // RDQA build
                // 只有 RDQA build 可以動態切換 cloud server, 在 TEST 與 RDQA 之間切換
                AppConstants.CloudServerMode cloudServerMode = DataManager.getInstance().getCloudServerMode();
                if (cloudServerMode == AppConstants.CloudServerMode.TEST) {
                    cloudServerMode = AppConstants.CloudServerMode.RDQA;
                } else if (cloudServerMode == AppConstants.CloudServerMode.RDQA) {
                    cloudServerMode = AppConstants.CloudServerMode.TEST;
                }
                // 將 cloud server mode 儲存到 share preference, 並重新設定 cloud API 的 base URL
                DataManager.getInstance().setCloudServerMode(cloudServerMode);
                CloudManager.getInstance().initialCloudApiBaseUrl();
                mAccountManager.reinitialize();
                showMessageDialog("Change Cloud Server", "Cloud server URL change to: " + cloudServerMode.name());
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            return showAppInfo();
        }
        return super.onKeyDown(keyCode, event);
    }
}
