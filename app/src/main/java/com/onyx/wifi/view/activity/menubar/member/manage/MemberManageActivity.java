package com.onyx.wifi.view.activity.menubar.member.manage;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMemberManageBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.Member;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.menubar.member.update.MemberUpdateActivity;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.manage.MemberManageItemType;
import com.onyx.wifi.view.adapter.member.manage.MemberManageListAdapter;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.view.interfaces.member.OnMemberChangedListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.manage.MemberManageViewModel;

import java.util.ArrayList;
import java.util.List;

public class MemberManageActivity extends BaseMenuActivity implements OnMemberChangedListener, View.OnClickListener {

    private ActivityMemberManageBinding mBinding;

    private MemberManageViewModel mViewModel;

    private MemberManageListAdapter mAdapter;

    private FamilyMember mMember;

    private Handler mHandler = new Handler();

    class PauseTask implements Runnable {
        private Client mClient;

        public void setClient(Client client) {
            mClient = client;
        }

        @Override
        public void run() {
            if (mViewModel == null) {
                return;
            }

            if (mClient == null) {
                return;
            }

            String cid = mClient.getCid();
            int duration = mClient.getPauseDuration();

            mViewModel.pauseClient(cid, duration);
        }
    }

    private PauseTask pauseTask = new PauseTask();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_manage);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        }

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_manage);
        setView();
    }

    @Override
    public void onBackPressed() {
        DataManager dataManager = DataManager.getInstance();
        dataManager.setManageMember(null);
        dataManager.setManageClient(null);

        super.onBackPressed();
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        //mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        setupAdapter();

        mBinding.memberUserManageRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.memberUserManageRecyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.member_manage_family_member);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mMember.getTemporaryName();

                if (!InputUtils.isNameValid(name)) {
                    String title = getString(R.string.member_name_invalid_name);
                    String message = getString(R.string.member_name_rule);

                    showMessageDialog(title, message);

                    return;
                }

                mViewModel.updateMember(mMember);
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(MemberManageViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {

                    if (resultStatus.actionCode == Code.Action.Member.UPDATE_FAMILY_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Family Member", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.REMOVE_FAMILY_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Family Member", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.PAUSE_CLIENT) {
                        if (!resultStatus.success) {
                            showMessageDialog("Pause Client", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.RESUME_CLIENT) {
                        if (!resultStatus.success) {
                            showMessageDialog("Resume Client", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Member List", resultStatus.errorMsg);

                            return;
                        }

                        setupAdapter();

                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void setupAdapter() {
        DataManager dataManager = DataManager.getInstance();
        mMember = dataManager.getManageMember();

        List<ListItem> listItems = new ArrayList();

        setupHeader(mMember, listItems);

        setupClientList(mMember, listItems);

        setupContentFilters(listItems);

        setupInternetSchedule(listItems);

        mAdapter = new MemberManageListAdapter(mMember, listItems, this, this);
    }

    private boolean isMemberChanged() {
        return mMember.getTemporaryName() != "";
    }

    @Override
    public void onMemberRollback() {
        if (isMemberChanged()) {
            return;
        }

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onMemberChanged() {
        if (!isMemberChanged()) {
            return;
        }

        mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
    }

    private void setupHeader(Member member, List<ListItem> listItems) {
        ListItem header = new ListItem<>(MemberManageItemType.HEADER, member);
        listItems.add(header);
    }

    private void setupClientList(Member member, List<ListItem> listItems) {
        String title = getString(R.string.member_manage_assigned_devices);

        ListItem section = new ListItem<>(MemberManageItemType.SECTION, title);
        listItems.add(section);

        List<Client> clientList = member.getAllAllowedClient();//member.getAllClients();

        if (clientList.size() == 0) {
            ListItem emptySection = new ListItem<>(MemberManageItemType.EMPTY, "No Device!");
            listItems.add(emptySection);
            return;
        }

        for (Client client : clientList) {
            ListItem item = new ListItem<>(MemberManageItemType.ITEM, client);
            listItems.add(item);
        }
    }

    private void setupContentFilters(List<ListItem> listItems) {
        String title = "Content Filters";

        ListItem section = new ListItem<>(MemberManageItemType.SECTION, title);
        listItems.add(section);

        ListItem item = new ListItem<>(MemberManageItemType.CONTENT_FILTERS, null);
        listItems.add(item);
    }

    private void setupInternetSchedule(List<ListItem> listItems) {
        String title = "Internet Schedule";

        ListItem section = new ListItem<>(MemberManageItemType.SECTION, title);
        listItems.add(section);

        ListItem item = new ListItem<>(MemberManageItemType.INTERNET_SCHEDULE, null);
        listItems.add(item);
    }

    @Override
    public void onClick(View v) {
        OptionListDialog optionListDialog = new OptionListDialog();
        final ArrayList<String> options = new ArrayList<>();
        options.add(getString(R.string.member_manage_edit_profile));
        options.add(getString(R.string.member_remove_family_member));

        ArrayList<Integer> icons = new ArrayList<>();
        icons.add(R.drawable.ic_sheet_client);
        icons.add(R.drawable.ic_sheet_remove);

        optionListDialog.setOptionString(options);
        optionListDialog.setOptionIconIds(icons);
        optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(MemberManageActivity.this, MemberUpdateActivity.class);
                    startActivity(intent);

                    optionListDialog.dismiss();
                }

                if (position == 1) {
                    final ConfirmDialog confirmDialog = new ConfirmDialog();
                    confirmDialog.setTitle(getString(R.string.member_remove_family_member_dialog_title));
                    confirmDialog.setContent(getString(R.string.member_remove_family_member_desc));
                    confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DataManager dataManager = DataManager.getInstance();
                            FamilyMember member = dataManager.getManageMember();

                            if (member != null) {
                                String memberId = member.getId();
                                mViewModel.removeMember(memberId);
                            }

                            confirmDialog.dismiss();
                        }
                    });
                    confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            confirmDialog.dismiss();
                        }
                    });
                    showDialog(confirmDialog);
                }


                optionListDialog.dismiss();
            }
        });
        showDialog(optionListDialog);
    }

}
