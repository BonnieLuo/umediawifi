package com.onyx.wifi.view.adapter.advancedwireless;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.adapter.member.ListItem;

import java.util.ArrayList;
import java.util.List;

public class PortForwardingClientListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnClientListChangedListener {

        void onClientListChanged(ClientModel selectedClientModel);

    }

    private enum ListItemType {
        RECENTLY_SECTION,
        LONG_INACTIVE_SECTION,
        NO_DEVICE,
        ITEM
    }

    private List<ListItem> mListItems = new ArrayList<>();

    private List<ClientModel> mClientModelList = new ArrayList<>();

    private List<ClientModel> mRecentlyClientModelList = new ArrayList<>();

    //private List<ClientModel> mLongInactiveClientModelList = new ArrayList<>();

    private ClientModel mSelectedClientModel;
    private int mSelectIndex = -1;

    private String mSelectedIp;

    private boolean mIsRecentlyExtend = false;
    private boolean mIsLongInactiveExtend = false;
    private boolean[] flag = new boolean[1000];//此處新增一個boolean型別的陣列

    private OnClientListChangedListener mListener;

    public PortForwardingClientListAdapter(OnClientListChangedListener listener) {
        mListener = listener;

        setupListItems();
    }

    public void setData(List<ClientModel> clientModelList, String ip) {
        mClientModelList = clientModelList;

        mSelectedIp = ip;

        for (ClientModel clientModel: mClientModelList) {
            String modelIp = clientModel.getIp();
            if (modelIp.equalsIgnoreCase(mSelectedIp)) {
                mSelectedClientModel = clientModel;
            }

        }

        setupClientModelList();

        setupListItems();
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<ListItemType, Object> listItem = mListItems.get(position);
        ListItemType type = listItem.getType();

        return type.ordinal();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        if (viewType != ListItemType.ITEM.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_client_list_section, parent, false);

            SectionViewHolder viewHolder = new SectionViewHolder(view);

            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_client_list_item, parent, false);

        ClientViewHolder viewHolder = new ClientViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<ListItemType, Object> listItem = mListItems.get(position);
        ListItemType type = listItem.getType();

        if (type != ListItemType.ITEM) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder)viewHolder;
            sectionViewHolder.setListItem(listItem);
            return;
        }

        ClientModel clientModel = (ClientModel) listItem.getItem();

        ClientViewHolder clientViewHolder = (ClientViewHolder)viewHolder;
        clientViewHolder.setClientModel(clientModel, position == mSelectIndex);

        //////////////////////////////////////////////////////////////////////////
        // 解決checkbox因為timer定期重畫造成勾選消失的問題
//        viewHolder.cbIsAlexaConnect.setOnCheckedChangeListener(null);//先設定一次CheckBox的選中監聽器，傳入引數null
//        viewHolder.cbIsAlexaConnect.setChecked(flag[position]);//用陣列中的值設定CheckBox的選中狀態
//        viewHolder.cbIsAlexaConnect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                flag[position] = b;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    private void setupClientModelList() {
        mRecentlyClientModelList.clear();
        //mLongInactiveClientModelList.clear();

        for (ClientModel model : mClientModelList) {
            ConnectionStatus connectionStatus = model.getOnlineStatus();
            if (connectionStatus == ConnectionStatus.ONLINE) {
                mRecentlyClientModelList.add(model);
            } //else {
//                mLongInactiveClientModelList.add(model);
//            }
        }

        mIsRecentlyExtend = mRecentlyClientModelList.size() < 4;

        //mIsLongInactiveExtend = mLongInactiveClientModelList.size() < 4;
    }

    private void setupListItems() {
        mListItems.clear();

//        ListItem<ListItemType, String> recentlySection = new ListItem(ListItemType.RECENTLY_SECTION, "Recently Active Devices");
//        mListItems.add(recentlySection);

        for (ClientModel model: getRecentlyClientModelList()) {
            ListItem<ListItemType, ClientModel> item = new ListItem(ListItemType.ITEM, model);
            mListItems.add(item);
        }

        if (getRecentlyClientModelList().size() == 0) {
            ListItem<ListItemType, String> NoDeviceSection = new ListItem(ListItemType.NO_DEVICE, "No Device!");
            mListItems.add(NoDeviceSection);
        }

//        if (getLongInactiveClientModelList().size() > 0) {
//            ListItem<ListItemType, String> longInactiveSection = new ListItem(ListItemType.LONG_INACTIVE_SECTION, "Long Inactive Devices");
//            mListItems.add(longInactiveSection);
//
//            for (ClientModel model : getLongInactiveClientModelList()) {
//                ListItem<ListItemType, ClientModel> item = new ListItem(ListItemType.ITEM, model);
//                mListItems.add(item);
//            }
//        }

        notifyDataSetChanged();
    }

    private List<ClientModel> getRecentlyClientModelList() {
        if (mRecentlyClientModelList.size() <= 3) {
            return mRecentlyClientModelList;
        }

        if (!mIsRecentlyExtend) {
            return mRecentlyClientModelList.subList(0, 3);
        }

        return mRecentlyClientModelList;
    }

//    private List<ClientModel> getLongInactiveClientModelList() {
//        if (mLongInactiveClientModelList.size() <= 3) {
//            return mLongInactiveClientModelList;
//        }
//
//        if (!mIsLongInactiveExtend) {
//            return mLongInactiveClientModelList.subList(0, 3);
//        }
//
//        return mLongInactiveClientModelList;
//    }

    class SectionViewHolder extends RecyclerView.ViewHolder {

        ListItem<ListItemType, Object> mListItem;

        TextView mTitleTextView;

        TextView mNoDevice;

        Button mButton;

        public SectionViewHolder(@NonNull View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.titleTextView);

            mNoDevice = itemView.findViewById(R.id.tvNoDevice);

            mButton = itemView.findViewById(R.id.extendButton);

            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListItemType type = mListItem.getType();

                    if (type == ListItemType.RECENTLY_SECTION) {
                        mIsRecentlyExtend = !mIsRecentlyExtend;
                    }

                    if (type == ListItemType.LONG_INACTIVE_SECTION) {
                        mIsLongInactiveExtend = !mIsLongInactiveExtend;
                    }

                    setupListItems();
                }
            });
        }

        public void setListItem(ListItem<ListItemType, Object> listItem) {
            mListItem = listItem;

            String title = (String) mListItem.getItem();
            mTitleTextView.setVisibility(View.VISIBLE);
            mTitleTextView.setText(title);

            ListItemType type = mListItem.getType();

            if (type == ListItemType.RECENTLY_SECTION) {
                if (mIsRecentlyExtend) {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_hide_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_hide, 0);
                } else {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_more_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);
                }

                if (mRecentlyClientModelList.size() > 3) {
                    mButton.setVisibility(View.VISIBLE);
                } else {
                    mButton.setVisibility(View.INVISIBLE);
                }
            }

            if (type == ListItemType.NO_DEVICE) {
                mTitleTextView.setVisibility(View.INVISIBLE);
                mNoDevice.setVisibility(View.VISIBLE);
//                mTitleTextView.setTextColor(CommonUtils.getColor(itemView.getContext(), R.color.gray_cccccc));
//                mTitleTextView.setGravity(View.TEXT_ALIGNMENT_CENTER);
            }

            if (type == ListItemType.LONG_INACTIVE_SECTION) {
                if (mIsLongInactiveExtend) {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_hide_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_hide, 0);
                } else {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_more_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);
                }

//                if (mLongInactiveClientModelList.size() > 3) {
//                    mButton.setVisibility(View.VISIBLE);
//                } else {
//                    mButton.setVisibility(View.INVISIBLE);
//                }
            }
        }
    }

    class ClientViewHolder extends RecyclerView.ViewHolder {

        Client mClient;

        ImageView mIconImageView;
        TextView mNameTextView;
        TextView mDescriptionTextView;
        ImageView mDefaultUserImageView;
        CardView mCardView;
        ImageView mPhotoImageView;
        ImageView mLightImageView;
        ImageView mSelectedImageView;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = itemView.findViewById(R.id.iconImageView);

            mNameTextView = itemView.findViewById(R.id.nameTextView);

            mDescriptionTextView = itemView.findViewById(R.id.descriptionTextView);

            mDefaultUserImageView = itemView.findViewById(R.id.defaultUserImageView);
            mCardView = itemView.findViewById(R.id.userCardView);
            mPhotoImageView = itemView.findViewById(R.id.photoImageView);

            mLightImageView = itemView.findViewById(R.id.lightImageView);

            mSelectedImageView = itemView.findViewById(R.id.selectedImageView);

            mSelectedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedClientModel = mClient.getClientModel();
                    mSelectIndex = getAdapterPosition();
                    notifyDataSetChanged();

                    if (mListener != null) {
                        mListener.onClientListChanged(mSelectedClientModel);
                    }
                }
            });
        }

        public void setClientModel(ClientModel clientModel, boolean isSelected) {
            mClient = new Client(clientModel);

            setClientType(isSelected);

            String name = mClient.getName();

            // 以下判斷only for進此畫面後user有對check box做點按動作後
            if (isSelected)
                mNameTextView.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.purple_4e1393));
            else
                mNameTextView.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.gray_828282));

            mNameTextView.setText(name);

            Context context = itemView.getContext();
            //String connectionMessage = mClient.getConnectionMessage(context);
            //String connectionMessage = mClient.getLastOnlineTimeInfo();//.getConnectionMessage(context);

            mDescriptionTextView.setText(mClient.getIp());

            setOwnerPhoto();

            setStatus();

            setSelected();
        }

        public void setClientType(boolean isSelected) {

            if (isSelected) {
                int iconId = mClient.getClientIcon(ConnectionStatus.ONLINE);
                mIconImageView.setImageResource(iconId);
            } else {
                int iconId = mClient.getClientIcon(ConnectionStatus.OFFLINE);
                mIconImageView.setImageResource(iconId);
            }
//            int iconId = mClient.getClientIcon();
//
//            mIconImageView.setImageResource(iconId);
        }

        public void setOwnerPhoto() {
            String ownerId = mClient.getOwnerId();

            if (ownerId == null) {
                mDefaultUserImageView.setVisibility(View.INVISIBLE);
                mCardView.setVisibility(View.INVISIBLE);
                mPhotoImageView.setVisibility(View.INVISIBLE);

                return;
            }

            mDefaultUserImageView.setVisibility(View.VISIBLE);
            mCardView.setVisibility(View.VISIBLE);
            mPhotoImageView.setVisibility(View.VISIBLE);
            mPhotoImageView.setImageResource(0);

            Context context = itemView.getContext();
            Bitmap bitmap = mClient.getOwnerPhoto(context, ownerId);

            if (bitmap != null) {
                mPhotoImageView.setImageBitmap(bitmap);
            }
        }

        public void setStatus() {
            int light = mClient.getConnectionLight();

            mLightImageView.setImageResource(light);
        }

        public void setSelected() {
            mSelectedImageView.setSelected(false);

            String currentMac = mClient.getCid();

            if (mSelectedClientModel != null) {
                String cid = mSelectedClientModel.getCid();

                if (currentMac.equalsIgnoreCase(cid)) {
                    mSelectedImageView.setSelected(true);

                    ////////////////////////////////
                    int iconId = mClient.getClientIcon(ConnectionStatus.ONLINE);
                    mIconImageView.setImageResource(iconId);

                    mNameTextView.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.purple_4e1393));
                    return;
                }
            }
        }
    }
}
