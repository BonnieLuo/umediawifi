package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityInternetIpInformationBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

public class InternetIPInfoActivity extends BaseMenuActivity {
    private ActivityInternetIpInformationBinding mBinding;
    private NetworkSetting mOriginalNetworkSetting;
    private NetworkSettingViewModel mViewModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_internet_ip_information);
        mOriginalNetworkSetting = DataManager.getInstance().getNetworkSetting();

        setViewModel();
        setView();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {

            }
        });
    }


    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // MAC address
        mBinding.tvMACAddrValue.setText(mOriginalNetworkSetting.getWanMac());
        // Internet Type
        if (mOriginalNetworkSetting.getWanType().equals("1")) {
            mBinding.tvInternetTypeValue.setText("DHCP");
        } else if (mOriginalNetworkSetting.getWanType().equals("2")) {
            mBinding.tvInternetTypeValue.setText("PPPoE");
        } else if (mOriginalNetworkSetting.getWanType().equals("3")) {
            mBinding.tvInternetTypeValue.setText("Static IP");
        } else {
            mBinding.tvInternetTypeValue.setText("Unknown");
        }

        if (mOriginalNetworkSetting.getWanType().equals("1") ||
                mOriginalNetworkSetting.getWanType().equals("2") ) {
            // Internet IP Address
            mBinding.tvInternetIPAddrValue.setText(mOriginalNetworkSetting.getWanIp());
            // Subnet Mask
            mBinding.tvSubnetMaskValue.setText(mOriginalNetworkSetting.getWanMask());
            // Default Gateway
            mBinding.tvDefaultGatewayValue.setText(mOriginalNetworkSetting.getWanGw());
            // DNSI
            mBinding.tvDNS1Value.setText(mOriginalNetworkSetting.getWanDns1());
            // DNS2
            mBinding.tvDNS2Value.setText(mOriginalNetworkSetting.getWanDns2());
        } else if (mOriginalNetworkSetting.getWanType().equals("3")) {
            // Internet IP Address
            mBinding.tvInternetIPAddrValue.setText(mOriginalNetworkSetting.getWanStaticIp());
            // Subnet Mask
            mBinding.tvSubnetMaskValue.setText(mOriginalNetworkSetting.getWanStaticMask());
            // Default Gateway
            mBinding.tvDefaultGatewayValue.setText(mOriginalNetworkSetting.getWanStaticGw());
            // DNSI
            mBinding.tvDNS1Value.setText(mOriginalNetworkSetting.getWanStaticDns1());
            // DNS2
            mBinding.tvDNS2Value.setText(mOriginalNetworkSetting.getWanStaticDns2());
        } else {
            // Internet IP Address
            mBinding.tvInternetIPAddrValue.setText("");
            // Subnet Mask
            mBinding.tvSubnetMaskValue.setText("");
            // Default Gateway
            mBinding.tvDefaultGatewayValue.setText("");
            // DNSI
            mBinding.tvDNS1Value.setText("");
            // DNS2
            mBinding.tvDNS2Value.setText("");
        }

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.internet_ip_information_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

}
