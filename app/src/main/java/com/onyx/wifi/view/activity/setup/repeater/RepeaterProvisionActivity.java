package com.onyx.wifi.view.activity.setup.repeater;

import android.arch.lifecycle.Observer;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityRepeaterProvisionBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.view.fragment.setup.DeviceSetLabelFragment;
import com.onyx.wifi.view.fragment.setup.repeater.ConfigureDeviceFragment;
import com.onyx.wifi.view.fragment.setup.repeater.RepeaterSetupSuccessFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

public class RepeaterProvisionActivity extends BaseDeviceSetupActivity implements DeviceSetupListener {

    private DeviceSetLabelFragment mRepeaterSetLabelFragment;
    private ConfigureDeviceFragment mConfigureDeviceFragment;
    private RepeaterSetupSuccessFragment mRepeaterSetupSuccessFragment;

    private ActivityRepeaterProvisionBinding mBinding;

    private AppConstants.SetupFlow mSetupFlow;
    private SetupDevice mSetupDevice;

    private boolean mIsNeedListenBleState = false;  // for BLE setup flow
    private boolean mIsNeedGetProvisionState = false;
    private int mGetProvisionStateCount = 0;

    private JsonParserWrapper mJsonParser;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSetupFlow = (AppConstants.SetupFlow) getIntent().getSerializableExtra(AppConstants.EXTRA_SETUP_FLOW);
        DataManager.getInstance().setSetupFlow(mSetupFlow);
        mSetupDevice = getIntent().getParcelableExtra(AppConstants.EXTRA_SETUP_DEVICE);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_repeater_provision);
        setView();
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mConfigureDeviceFragment = (ConfigureDeviceFragment) mFragmentManager.findFragmentByTag(ConfigureDeviceFragment.class.getName());
            mRepeaterSetupSuccessFragment = (RepeaterSetupSuccessFragment) mFragmentManager.findFragmentByTag(RepeaterSetupSuccessFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            if (mConfigureDeviceFragment != null) {
                fragmentTransaction.hide(mConfigureDeviceFragment);
            }
            if (mRepeaterSetupSuccessFragment != null) {
                fragmentTransaction.hide(mRepeaterSetupSuccessFragment);
            }
            fragmentTransaction.show(mRepeaterSetLabelFragment);
            fragmentTransaction.commit();
        } else {
            mRepeaterSetLabelFragment = new DeviceSetLabelFragment();
            addFragmentAsFirstPage(R.id.clContainer, mRepeaterSetLabelFragment, DeviceSetLabelFragment.class.getName());
        }

        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            // 是否需要監聽藍牙開關狀態
            mIsNeedListenBleState = true;
        } else {
            // WIFI setup 流程不需要監聽藍牙開關狀態
            mIsNeedListenBleState = false;
        }

        // 要等到 App 與 device 的 setup 流程完成, device 開始向 cloud 進行 provision 程序時, 此 flag 才會被設定起來
        mIsNeedGetProvisionState = false;

        mJsonParser = new JsonParserWrapper();
        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            if (mIsNeedListenBleState) {
                IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                registerReceiver(mBleStateListener, filter);
            }

            // 註冊 BLE action receiver 以透過 broadcast receiver 收到 connection status
            mDeviceSetupViewModel.registerBleActionReceiver();
        }

        if (mIsNeedGetProvisionState) {
            LogUtils.trace("Activity is resumed and need to get provision state.");
            getProvisionState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            mDeviceSetupViewModel.unregisterBleActionReceiver();
            try {
                // BLE state listener 有可能已經先被 unregister 了, 再次 unregister 就會產生 exception
                unregisterReceiver(mBleStateListener);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        if (mIsNeedGetProvisionState) {
            mHandler.removeCallbacks(mGetProvisionStateRunnable);
        }
    }

    @Override
    protected void onDestroy() {
        if (mSetupFlow == AppConstants.SetupFlow.BLE) {
            mDeviceSetupViewModel.disconnectDevice();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mConfigureDeviceFragment != null && mConfigureDeviceFragment.isVisible()) {
            // do nothing
            // 在 search device 頁面時不允許執行返回功能退出頁面, 以免藍牙掃描流程出現問題
            return;
        }

        // 若是在其他頁面 (ex: 找不到裝置、找到一個/多個裝置...), 允許頁面返回功能
        super.onBackPressed();
    }

    private void setView() {
        setTitleBar();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.umedia_device_setup_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mDeviceSetupViewModel.setSetupFlow(DataManager.getInstance().getSetupFlow());
        mDeviceSetupViewModel.setSetupMode(DataManager.getInstance().getSetupMode());
        mDeviceSetupViewModel.setSetupDevice(mSetupDevice);
        mDeviceSetupViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.success) {
                        // 動作 success 後, 根據現在的 action code 判斷下一步動作
                        // 大部份動作 success 的情況在 view model/model 內已串接完成, 一步步往下做 (期間 UI 不需要更新)
                        // 這裡判斷的情況是需要 UI 更新畫面或由 UI 決定流程
                        if (resultStatus.actionCode == Code.Action.DeviceSetup.CONNECT_DEVICE) {
                            // device 連線成功後即可開始下 command, 進入 Firmware 的 device setup/provision 流程
                            mDeviceSetupViewModel.startDeviceSetup();
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SETUP_COMPLETE) {
                            // 對 device 設定完 setup complete 指令後, 開始去跟 Cloud 詢問 device provision 的狀態
                            onContinue(Code.Action.DeviceSetup.GET_PROVISION_STATE);
                        } else if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_PROVISION_STATE) {
                            String state = mJsonParser.jsonGetString((JsonObject) resultStatus.data, "state");
                            // Cloud 回覆的 device provision state 共有四種:
                            // (1) PROVISION_BEGIN : 開始佈署
                            // (2) REGISTER_IOT_HUB_DEVICE : 註冊 IoT Hub 裝置中
                            // (3) MQTT_CONNECTED : MQTT 已連線
                            // (4) PROVISION_SUCCESS : 佈署完成
                            if (state.equals("PROVISION_SUCCESS")) {
                                String did = mJsonParser.jsonGetString((JsonObject) resultStatus.data, "device_id");
                                mDeviceSetupViewModel.deviceConnectionTest(did);

                                // 收到 provision 成功後, 會顯示 setup 成功的畫面, 之後回到 dashboard 會去打 cloud API 拿資料
                                // 等待幾秒才顯示成功的畫面, 是要等 cloud 的資料穩定了, 才讓使用者可以回到 dashboard
                                // 因為若馬上打 cloud API 拿取資料, 有時會發生 dashboard 拿到的資料是空的, 或是 get node info 收到 Http status 500
                                // 但過一下子再打 API 又沒問題了, 推測可能有時 cloud 的資料尚未處理完成才會發生錯誤, 故等待幾秒再打 cloud API 拿資料
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // 最後一個動作, 不論成功或失敗, 都將 progress 改成 100%, 表示結束
                                        mConfigureDeviceFragment.updateProgress(100);
                                        onContinue(Code.Action.DeviceSetup.PROVISION_SUCCESS);
                                    }
                                }, 10000);
                            } else {
                                getProvisionStateAgain();
                            }
                            updateProvisionProgress();
                        }
                    } else {
                        // command fail 時, 以 action code 來判斷及顯示訊息
                        String message = resultStatus.errorMsg;
                        if (resultStatus.actionCode == Code.Action.DeviceSetup.CONNECT_DEVICE) {
                            showErrorMessageAndRetry("Bluetooth", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_USER_ID) {
                            showErrorMessageAndRetry("Set User ID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_TIMEZONE) {
                            showErrorMessageAndRetry("Set Timezone", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_PROVISION_TOKEN) {
                            showErrorMessageAndRetry("Set Provision Token", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_DEVICE_BOARD_ID) {
                            showErrorMessageAndRetry("Get Device Board ID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_DEVICE_OPERATION_MODE) {
                            showErrorMessageAndRetry("Set Device Operation Mode", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_LOCATION) {
                            showErrorMessageAndRetry("Set Location", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI) {
                            showErrorMessageAndRetry("Set 5G ApClient Wi-Fi SSID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI_KEY) {
                            showErrorMessageAndRetry("Set 5G ApClient Wi-Fi Password", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI_SEC) {
                            showErrorMessageAndRetry("Set 5G ApClient Wi-Fi Security", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_AP_CLIENT_WIFI_ACTION) {
                            showErrorMessageAndRetry("Set ApClient Wi-Fi Action", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.GET_5G_AP_CLIENT_CONN_STATUS) {
                            showErrorMessageAndRetry("Set 5G ApClient Connection Status", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_2G_WIFI) {
                            showErrorMessageAndRetry("Set 2.4G Wi-Fi SSID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_2G_WIFI_KEY) {
                            showErrorMessageAndRetry("Set 2.4G Wi-Fi Password", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_WIFI) {
                            showErrorMessageAndRetry("Set 5G Wi-Fi SSID", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_5G_WIFI_KEY) {
                            showErrorMessageAndRetry("Set 5G Wi-Fi Password", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SET_BLE_ENABLE) {
                            showErrorMessageAndRetry("Set BLE Disable", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceFirmwareCommand.SETUP_COMPLETE) {
                            showErrorMessageAndRetry("Set Setup Complete", message);
                        } else if (resultStatus.actionCode == Code.Action.DeviceSetup.GET_PROVISION_STATE) {
                            if (mDeviceSetupViewModel.getSetupFlow() == AppConstants.SetupFlow.WIFI && mGetProvisionStateCount <= 3) {
                                // WIFI provision 時手機會連線到 device 預設的 SSID, 開始 provision 後因為預設的 SSID 失效了所以手機 WIFI 會斷線
                                // 而打 cloud API get provision state 時就會出現網路不通的提示訊息, 但實際上過一下子手機網路可能就通了
                                // (例如: 有開行動數據的話, WIFI 斷線後就會自動切換去使用行動網路, 但需要幾秒鐘的時間)
                                // => 原本第一次就會提示訊息, 改為三次失敗後才提示
                                getProvisionStateAgain();
                            } else {
                                if (message.equals(getString(R.string.err_internet_not_available))
                                        || message.equals(getString(R.string.err_internet_connection_fail))
                                        || message.equals(getString(R.string.err_internet_timeout))
                                        || message.equals(getString(R.string.err_socket_exception))) {
                                    final MessageDialog messageDialog = new MessageDialog();
                                    messageDialog.setTitle("Get Provision State");
                                    messageDialog.setContent("Please check your phone's Internet connection in order to get device setup result.");
                                    messageDialog.setOkButton(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            messageDialog.dismiss();
                                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    });
                                    showDialog(messageDialog);
                                } else {
                                    getProvisionStateAgain();
                                }
                            }
                            updateProvisionProgress();
                        } else {
                            showErrorMessageAndRetry("Device Setup", "Action code: " + resultStatus.actionCode + "\n Message: " + message);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onContinue(int nextStep) {
        LogUtils.trace("next step = " + nextStep);
        switch (nextStep) {
            case Code.Action.DeviceSetup.CONNECT_DEVICE:
                mConfigureDeviceFragment = new ConfigureDeviceFragment();
                addFragment(R.id.clContainer, mConfigureDeviceFragment, ConfigureDeviceFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);

                // 選好 device label 按下 continue, 根據 setup flow 決定下一個動作
                if (mSetupFlow == AppConstants.SetupFlow.BLE) {
                    // BLE 需要先 connect device
                    mDeviceSetupViewModel.connectDevice();
                } else {
                    // 若是走 WIFI setup, 進入這頁時是確定 WIFI 已經連線上 device setup SSID 了, 所以此時可以直接開始 setup
                    mDeviceSetupViewModel.startDeviceSetup();
                }
                break;

            case Code.Action.DeviceSetup.GET_PROVISION_STATE:
                if (mSetupFlow == AppConstants.SetupFlow.BLE) {
                    // App 所有要透過藍牙傳送給 device 的指令都已完成, 即使藍牙在這時候被關閉了也不影響結果, 故不用再監聽藍牙開關狀態了
                    mIsNeedListenBleState = false;
                    unregisterReceiver(mBleStateListener);
                }
                mIsNeedGetProvisionState = true;
                mGetProvisionStateCount = 0;
                getProvisionState();
                // 使用者已完成所有設定, App 開始等待 provision 結果, 顯示進度
                mConfigureDeviceFragment.updateProgress(0);
                break;

            case Code.Action.DeviceSetup.PROVISION_SUCCESS:
                mRepeaterSetupSuccessFragment = new RepeaterSetupSuccessFragment();
                // 新增 repeater setup success 頁面, 直接覆蓋原本的 configure device 的 loading 畫面
                addFragmentAsFirstPage(R.id.clContainer, mRepeaterSetupSuccessFragment, RepeaterSetupSuccessFragment.class.getName());
                break;

            case Code.Action.DeviceSetup.ADD_ANOTHER_DEVICE:
                Intent intent = new Intent(mContext, RepeaterSetupActivity.class);
                intent.putExtra(AppConstants.EXTRA_ADD_ANOTHER_DEVICE, true);
                startActivity(intent);
                finish();
                break;

            case Code.Action.DeviceSetup.FINISH_SETUP:
                startDashboardActivity(RepeaterProvisionActivity.this);
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void setSupportView(TextView textView) {
        setSupportClickMessage(textView);
    }

    private Runnable mGetProvisionStateRunnable = new Runnable() {
        @Override
        public void run() {
            mGetProvisionStateCount++;
            LogUtils.trace("mGetProvisionStateCount = " + mGetProvisionStateCount);
            mDeviceSetupViewModel.getProvisionState();
        }
    };

    private void getProvisionState() {
        // device 向 Cloud 進行 provision 中, 需花費一些時間, 等待 10 秒後再詢問狀態
        mHandler.postDelayed(mGetProvisionStateRunnable, 10000);
    }

    private void getProvisionStateAgain() {
        if (mGetProvisionStateCount >= mGetProvisionStateMaxCount) {
            // 將 progress 改成 100%, 表示結束
            mConfigureDeviceFragment.updateProgress(100);
            // 嘗試 get provision state 多次了都還沒拿到 provision 成功的狀態, 判定為 provision 失敗
            LogUtils.trace("mGetProvisionStateCount = " + mGetProvisionStateCount);
            String message = "Device provision failed. Please try again.";
            showErrorMessageAndRetry("Provision Timeout", message);
        } else {
            // 重新詢問一次 provision 狀態
            getProvisionState();
        }
    }

    private void updateProvisionProgress() {
        int progress = (int) ((100.0 / mGetProvisionStateMaxCount) * mGetProvisionStateCount);
        mConfigureDeviceFragment.updateProgress(progress);
    }
}
