package com.onyx.wifi.view.adapter.member;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;

public class SectionViewHolder extends RecyclerView.ViewHolder {

    private TextView mTitleTextView;

    public SectionViewHolder(@NonNull View itemView) {
        super(itemView);

        mTitleTextView = this.itemView.findViewById(R.id.title);
    }

    public void setTitle(String title) {
        if (title.equalsIgnoreCase("No Device!")) {
            Context context = itemView.getContext();
            int colorId = context.getResources().getColor(R.color.gray_676767);
            mTitleTextView.setTextColor(colorId);
        } else {
            Context context = itemView.getContext();
            int colorId = context.getResources().getColor(R.color.purple_4e1393);
            mTitleTextView.setTextColor(colorId);
        }

        mTitleTextView.setText(title);
    }
}
