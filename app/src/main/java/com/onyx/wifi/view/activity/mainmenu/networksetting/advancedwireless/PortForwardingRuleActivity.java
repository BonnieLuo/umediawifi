package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessPortForwardingRuleBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingList;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRule;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRuleModel;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.CustomEditText;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.PortForwardingRuleViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PortForwardingRuleActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessPortForwardingRuleBinding mBinding;

    private PortForwardingRuleViewModel mViewModel;

    private PortForwardingRule mRule;

    List<PortForwardingRuleModel> mRuleModelList = new ArrayList<PortForwardingRuleModel>();

    OptionListDialog mOptionListDialog = new OptionListDialog();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_port_forwarding_rule);

        DataManager dataManager = DataManager.getInstance();
        mRule = dataManager.getPortForwardingRule();

        if (mRule == null) {
            PortForwardingRuleModel model = new PortForwardingRuleModel();
            mRule = new PortForwardingRule(model);
            dataManager.setPortForwardingRule(mRule);
        }

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            DataManager dataManager = DataManager.getInstance();
            mRule = dataManager.getPortForwardingRule();

            setData();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        DataManager dataManager = DataManager.getInstance();
        dataManager.setPortForwardingRule(null);
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.customEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newName = s.toString().trim();

                mRule.setName(newName);

                checkDiff();
            }
        });

        mBinding.ipEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (InputUtils.isIpValid(editable.toString().trim())) {
                    mRule.setIp(editable.toString().trim());
                } else {
                    mRule.setManualIp(editable.toString().trim());
                }
                checkDiff();
            }
        });

        mBinding.enableImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRule.toggleEnable();

                setEnable();

                checkDiff();
            }
        });

        mBinding.clientEditArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PortForwardingClientListActivity.class);
                intent.putExtra("title", "Select a Device");
                intent.putExtra("multi_select_enabled", false);
                intent.putExtra("ip", mRule.getIp());

                startActivity(intent);
            }
        });

        mBinding.externalPortStartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setExternalPortStart(port);

                checkDiff();
            }
        });

        mBinding.externalPortEndEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setExternalPortEnd(port);

                checkDiff();
            }
        });

        mBinding.internalPortStartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setInternalPortStart(port);

                checkDiff();
            }
        });

        mBinding.internalPortEndEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setInternalPortEnd(port);

                checkDiff();
            }
        });



        final String[] protocolArray = {"TCP", "UDP", "TCP & UDP"};
        final ArrayList<String> options = new ArrayList<String>(Arrays.asList(protocolArray));

        mOptionListDialog.setOptionString(options);
        mOptionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                mRule.setProtocol(position);

                setProtocol();

                checkDiff();

                mOptionListDialog.dismiss();
            }
        });

        mBinding.imgSelectProtocol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(mOptionListDialog);
            }
        });

//        mBinding.portocolEditText.setDrawableRightListener(new CustomEditText.DrawableRightListener() {
//            @Override
//            public void onDrawableRightClick(View view) {
//
//                showDialog(mOptionListDialog);
//            }
//        });

        mBinding.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRule != null) {
                    final ConfirmDialog confirmDialog = new ConfirmDialog();
                    confirmDialog.setTitle("Remove Rule");
                    confirmDialog.setContent("Are you sure you want to remove this port forwarding rule?");
                    confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewModel.removePortForwardingRule(mRule);
                            confirmDialog.dismiss();
                        }
                    });
                    confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            confirmDialog.dismiss();
                        }
                    });
                    showDialog(confirmDialog);
                }
            }
        });

    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Port Forwarding Rule");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataManager dataManager = DataManager.getInstance();
                dataManager.setPortForwardingRule(null);

                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRule();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void getNewestRuleList() {
        DataManager dataManager = DataManager.getInstance();
        AdvanceWirelessSettings settings = dataManager.getAdvanceWirelessSettings();
        PortForwardingList portForwardingList = settings.getPortForwardingList();

        mRuleModelList.clear();
        mRuleModelList.addAll(portForwardingList.getPortForwardingRule());
    }

    private boolean isNameDuplicated(String name) {
        for (PortForwardingRuleModel ruleModel : mRuleModelList) {
            if (!ruleModel.getId().equals(mRule.getId())) {
                if (name.equals(StringUtils.decodeHexString(ruleModel.getName()))) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isExternalPortCoverageDuplicated(PortForwardingRule rule) {
        for (PortForwardingRuleModel ruleModel : mRuleModelList) {
            if ("1".equals(ruleModel.getEnable()) &&
                    !(ruleModel.getId().equals(mRule.getId()))) {
                String externalPort = ruleModel.getExternalPort();
                String[] separatedExternalPort = externalPort.split("-");
                String externalPortStart = null;
                String externalPortEnd = null;
                int exPortStart = 0;
                int exPortEnd = 0;

                if (separatedExternalPort.length == 2) {
                    externalPortStart = separatedExternalPort[0];
                    externalPortEnd = separatedExternalPort[1];
                }
                exPortStart = Integer.parseInt(externalPortStart);
                exPortEnd = Integer.parseInt(externalPortEnd);

                if (Math.max(Integer.parseInt(mRule.getExternalPortStart()), exPortStart) <=
                        Math.min(Integer.parseInt(mRule.getExternalPortEnd()), exPortEnd))
                    return true;
//                if ((Integer.parseInt(mRule.getExternalPortStart()) >= exPortStart) &&
//                        (Integer.parseInt(mRule.getExternalPortStart()) <= exPortEnd)) {
//                    return true;
//                } else if ((Integer.parseInt(mRule.getExternalPortEnd()) >= exPortStart) &&
//                        (Integer.parseInt(mRule.getExternalPortEnd()) <= exPortEnd)) {
//                    return true;
//                }
            }
        }

        return false;
    }

    private boolean isInternalPortCoverageDuplicated(PortForwardingRule rule) {
        for (PortForwardingRuleModel ruleModel : mRuleModelList) {
            if ("1".equals(ruleModel.getEnable()) &&
                    !(ruleModel.getId().equals(mRule.getId()))) {
                String internalPort = ruleModel.getInternalPort();
                String[] separatedInternalPort = internalPort.split("-");
                String internalPortStart = null;
                String internalPortEnd = null;
                int inPortStart = 0;
                int inPortEnd = 0;

                if (separatedInternalPort.length == 2) {
                    internalPortStart = separatedInternalPort[0];
                    internalPortEnd = separatedInternalPort[1];
                }
                inPortStart = Integer.parseInt(internalPortStart);
                inPortEnd = Integer.parseInt(internalPortEnd);

                if (Math.max(Integer.parseInt(mRule.getInternalPortStart()), inPortStart) <=
                        Math.min(Integer.parseInt(mRule.getInternalPortEnd()), inPortEnd))
                    return true;
//                if ((Integer.parseInt(mRule.getInternalPortStart()) >= inPortStart) &&
//                        (Integer.parseInt(mRule.getInternalPortStart()) <= inPortEnd)) {
//                    return true;
//                } else if ((Integer.parseInt(mRule.getInternalPortEnd()) >= inPortStart) &&
//                        (Integer.parseInt(mRule.getInternalPortEnd()) <= inPortEnd)) {
//                    return true;
//                }
            }
        }

        return false;
    }

    private boolean isRuleValid() {
        if (!mRule.isNameValid()) {
            String title = getString(R.string.invalid_rule_name_title);
            String message = getString(R.string.invalid_rule_name);

            showMessageDialog(title, message);

            return false;
        }

        if (isNameDuplicated(mRule.getName())) {
            String title = getString(R.string.invalid_rule_name_title);
            String message = "The rule with this name has already existed";

            showMessageDialog(title, message);

            return false;
        }

        // 判斷手動輸入的ip
        if (!InputUtils.isNotEmpty(mBinding.ipEditText.getEditableText().toString().trim()) ||
                !InputUtils.isIpValid(mBinding.ipEditText.getEditableText().toString().trim())) {
            String title = "Invaid IP Address";
            String message = "Please set IP address for this rule.";

            showMessageDialog(title, message);
            mBinding.ipEditText.setText(mRule.getIp());
            mBinding.ipEditText.setSelection(mRule.getIp().length());
            return false;
        }

//        if (!InputUtils.isIpValid(mBinding.ipEditText.getEditableText().toString().trim())) {
//            String title = "Invaid IP Address";
//            String message = "The input IP address is invalid.";
//
//            showMessageDialog(title, message);
//            mBinding.ipEditText.setText(mRule.getIp());
//            mBinding.ipEditText.setSelection(mRule.getIp().length());
//            return false;
//        }


        if (!mRule.isDeviceValid()) {
            String title = getString(R.string.invalid_device_title);
            String message = getString(R.string.invalid_device);

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isExternalPortRangeValid()) {
            String title = getString(R.string.invalid_external_port_title);
            String message = getString(R.string.invalid_external_port_range);

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isExternalPortRuleValid()) {
            String title = getString(R.string.invalid_external_port_title);
            String message = getString(R.string.invalid_external_port_rule);

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isInternalPortRangeValid()) {
            String title = getString(R.string.invalid_internal_port_title);
            String message = getString(R.string.invalid_internal_port_range);

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isInternalPortRuleValid()) {
            String title = getString(R.string.invalid_internal_port_title);
            String message = getString(R.string.invalid_internal_port_rule);

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isPortNumberValid()) {
            String title = getString(R.string.invalid_port_number_title);
            String message = getString(R.string.invalid_port_number);

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isProtocolValid()) {
            String title = getString(R.string.invalid_protocol_title);
            String message = getString(R.string.invalid_protocol);

            showMessageDialog(title, message);

            return false;
        }


        if (mRule.isEnabled()) {
            if (isExternalPortCoverageDuplicated(mRule)) {
                String title = "Invalid External Port";
                String message = "The External Port interval is overlapped with other enabled rule.";

                showMessageDialog(title, message);

                return false;
            }

            if (isInternalPortCoverageDuplicated(mRule)) {
                String title = "Invalid Internal Port";
                String message = "The Internal Port interval is overlapped with other enabled rule.";

                showMessageDialog(title, message);

                return false;
            }
        }

        return true;
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(PortForwardingRuleViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.GET_SETTINGS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Advance Wireless Settings", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            JsonObject jsonObject = json.getAsJsonObject("data");

                            Gson gson = new Gson();

                            AdvanceWirelessSettings advanceWirelessSettings = gson.fromJson(jsonObject, AdvanceWirelessSettings.class);

                            DataManager dataManager = DataManager.getInstance();
                            dataManager.setAdvanceWirelessSettings(advanceWirelessSettings);

                            // get newest rule list before update
                            getNewestRuleList();

                            // check rule
                            if (isRuleValid()) {

                                // execute update
                                mViewModel.updatePortForwardingRule(mRule);
                            }
                        }
                    }
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.UPDATE_PORT_FORWARDING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Port Forwarding", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setPortForwardingRule(null);

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.REMOVE_PORT_FORWARDING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Port Forwarding", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setPortForwardingRule(null);

                        finish();
                    }
                }
            }
        });
    }

    private void setData() {
        setName();

        setEnable();

        setIp();

        setExternalPort();

        setInternetPort();

        setProtocol();

        setRemoveButton();
    }

    private void setName() {
        String name = mRule.getName();
        mBinding.customEditText.setText(name);
    }

    private void setEnable() {
        boolean isEnabled = mRule.isEnabled();

        if (isEnabled) {
            mBinding.enableTextView.setText("Enabled");
            mBinding.enableImageView.setSelected(true);
        } else {
            mBinding.enableTextView.setText("Disabled");
            mBinding.enableImageView.setSelected(false);
        }
    }

    private void setIp() {
        String ip = mRule.getIp();

        if (ip == null) {
            return;
        }

        mBinding.ipEditText.setText(ip);
    }

    private void setExternalPort() {
        String startPort = mRule.getExternalPortStart();

        mBinding.externalPortStartEditText.setText(startPort);

        String endPort = mRule.getExternalPortEnd();

        mBinding.externalPortEndEditText.setText(endPort);
    }

    private void setInternetPort() {
        String startPort = mRule.getInternalPortStart();

        mBinding.internalPortStartEditText.setText(startPort);

        String endPort = mRule.getInternalPortEnd();

        mBinding.internalPortEndEditText.setText(endPort);
    }

    private void setProtocol() {
        String protocol = mRule.getProtocol();

        //mBinding.portocolEditText.setText(protocol);
        mBinding.tvProtocol.setText(protocol);
    }

    private void setRemoveButton() {
        String id = mRule.getId();
        if (id == null) {
            mBinding.removeButton.setEnabled(false);

            return;
        }

        mBinding.removeButton.setEnabled(true);
    }

    private void checkDiff() {
        if (mRule.isChanged()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void sendRule() {

        mViewModel.getAdvanceWirelessSettings();
        //mViewModel.updatePortForwardingRule(mRule);
    }

}
