package com.onyx.wifi.view.activity.setup;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAlternativeSetupBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.activity.setup.repeater.RepeaterProvisionActivity;
import com.onyx.wifi.view.activity.setup.router.RouterProvisionActivity;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

public class AlternativeSetupActivity extends BaseDeviceSetupActivity {

    private ActivityAlternativeSetupBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_alternative_setup);
        setView();
        setViewModel();
    }

    private void setView() {
        setTitleBar();

        switch (DataManager.getInstance().getSetupMode()) {
            case ROUTER:
                mBinding.tvContent.setText(R.string.alternative_setup_content_initial_setup);
                mBinding.tvStep1Title.setText(R.string.alternative_setup_step1_title_initial_setup);
                mBinding.tvStep1Content.setText(R.string.alternative_setup_step1_content_initial_setup);
                mBinding.tvStep2Title.setText(R.string.alternative_setup_step2_title_initial_setup);
                mBinding.tvStep2Content.setText(R.string.alternative_setup_step2_content_initial_setup);
                mBinding.tvStep3Title.setText(R.string.alternative_setup_step3_title_initial_setup);
                mBinding.tvStep3Content.setText(R.string.alternative_setup_step3_content_initial_setup);
                break;

            case REPEATER:
            default:
                mBinding.tvContent.setText(R.string.alternative_setup_content_add_device);
                mBinding.tvStep1Title.setText(R.string.alternative_setup_step1_title_add_device);
                mBinding.tvStep1Content.setText(R.string.alternative_setup_step1_content_add_device);
                mBinding.tvStep2Title.setText(R.string.alternative_setup_step2_title_add_device);
                mBinding.tvStep2Content.setText(R.string.alternative_setup_step2_content_add_device);
                mBinding.tvStep3Title.setText(R.string.alternative_setup_step3_title_add_device);
                mBinding.tvStep3Content.setText(R.string.alternative_setup_step3_content_add_device);
                break;
        }

        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 按下 continue 要進入 setup & provision 流程, 但要先確定手機有連線上 device 預設的 setup WIFI SSID
                // 由於下一個頁面需根據 device type 顯示圖片, 故打 get board ID WIFI API
                // 若有成功取得 board ID 即代表手機有連線上 device, 順便也把 device type 的資訊傳給下一頁 (見下方 observe result status)
                mDeviceSetupViewModel.checkIsConnectedToDevice();
            }
        });
        setSupportClickMessage(mBinding.tvSupport);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);

        switch (DataManager.getInstance().getSetupMode()) {
            // 這裡不用判斷 resetup router 的 case, 因為流程不會走到這頁
            // 此頁 alternative setup 是透過 WIFI setup, 而 router 經過 initial setup 成功後, 預設的 WIFI SSID 就不存在了
            // 因此 resetup router 時不能透過 WIFI, 只能透過 BLE
            case ROUTER:
                mBinding.titleBar.setTitle(R.string.initial_setup_title);
                mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                });
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case REPEATER:
            default:
                mBinding.titleBar.setTitle(R.string.umedia_device_setup_title);
                mBinding.titleBar.setLeftButton(R.drawable.ic_home, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startDashboardActivity(AlternativeSetupActivity.this);
                        finish();
                    }
                });
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;
        }

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        setLoadingObserve(mDeviceSetupViewModel, this, mBinding.loading);
        mDeviceSetupViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.DeviceSetup.CHECK_SETUP_WIFI) {
                        if (resultStatus.success) {
                            SetupDevice setupDevice = new SetupDevice();
                            // 流程進入下一頁面 (xxxxxProvisionActivity) 時, UI 需依據產品型號顯示圖片, 故將資訊傳遞下去
                            setupDevice.setDeviceType((AppConstants.DeviceType) resultStatus.data);
                            Intent intent = new Intent();
                            if (DataManager.getInstance().getSetupMode() == AppConstants.SetupMode.ROUTER) {
                                intent.setClass(mContext, RouterProvisionActivity.class);
                            } else {
                                intent.setClass(mContext, RepeaterProvisionActivity.class);
                            }
                            intent.putExtra(AppConstants.EXTRA_SETUP_FLOW, AppConstants.SetupFlow.WIFI);
                            intent.putExtra(AppConstants.EXTRA_SETUP_DEVICE, setupDevice);
                            startActivity(intent);
                        } else {
                            String message = resultStatus.errorMsg;
                            final MessageDialog messageDialog = new MessageDialog();
                            messageDialog.setTitle("Check Setup WiFi");
                            messageDialog.setContent(message);
                            messageDialog.setOkButton(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    messageDialog.dismiss();
                                }
                            });
                            showDialog(messageDialog);
                        }
                    }
                }
            }
        });
    }

    // 從這頁進入 ProvisionActivity 時, setup flow 會被設定成 WIFI, 並且這頁不會關閉
    // 若 WIFI setup 過程中失敗, 要 retry 時會判斷到 setup flow 是 WIFI, 就會導回到這頁
    // 但若這時再退出這頁, 視為退出 alternative setup (WIFI setup) 流程, 需將 setup flow 回復成 BLE 流程
    @Override
    protected void onDestroy() {
        // 退出這頁時會被執行到
        DataManager.getInstance().setSetupFlow(AppConstants.SetupFlow.BLE);
        super.onDestroy();
    }
}
