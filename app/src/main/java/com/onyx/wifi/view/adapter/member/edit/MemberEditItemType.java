package com.onyx.wifi.view.adapter.member.edit;

public enum MemberEditItemType {
    HEADER,
    SECTION,
    ITEM
}
