package com.onyx.wifi.view.interfaces;

import com.onyx.wifi.model.item.advancewireless.dmz.DestinationClient;

import java.util.List;

public interface OnClientListChangedListener {

    void onClientListChanged(List<DestinationClient> selectedCidList);

}
