package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.R;

import com.onyx.wifi.databinding.ActivityAdvancedWirelessPortTriggeringRuleBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringList;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRule;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRuleModel;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.PortTriggeringRuleViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PortTriggeringRuleActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessPortTriggeringRuleBinding mBinding;

    private PortTriggeringRuleViewModel mViewModel;

    private PortTriggeringRule mRule;

    List<PortTriggeringRuleModel> mRuleModelList = new ArrayList<PortTriggeringRuleModel>();

    OptionListDialog mMatchedProtocolDialog = new OptionListDialog();
    OptionListDialog TriggeredProtocolDialog = new OptionListDialog();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_port_triggering_rule);

        DataManager dataManager = DataManager.getInstance();
        mRule = dataManager.getPortTriggeringRule();

        if (mRule == null) {
            PortTriggeringRuleModel model = new PortTriggeringRuleModel();
            mRule = new PortTriggeringRule(model);
            dataManager.setPortTriggeringRule(mRule);
        }

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            DataManager dataManager = DataManager.getInstance();
            mRule = dataManager.getPortTriggeringRule();

            setData();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        DataManager dataManager = DataManager.getInstance();
        dataManager.setPortTriggeringRule(null);
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.customEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newName = s.toString().trim();
                mRule.setName(newName);

                checkDiff();
            }
        });

        mBinding.enableImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRule.toggleEnable();

                setEnable();

                checkDiff();
            }
        });

        mBinding.matchedPortStartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setMatchedPortStart(port);

                checkDiff();
            }
        });

        mBinding.matchedPortEndEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setMatchedPortEnd(port);

                checkDiff();
            }
        });

        final String[] protocolArray = {"TCP", "UDP", "TCP & UDP"};
        final ArrayList<String> options = new ArrayList<String>(Arrays.asList(protocolArray));

        mMatchedProtocolDialog.setOptionString(options);
        mMatchedProtocolDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                mRule.setMatchedProtocol(position);

                setMatchedProtocol();

                checkDiff();

                mMatchedProtocolDialog.dismiss();
            }
        });
        mBinding.imgMatchedProtocol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(mMatchedProtocolDialog);
            }
        });
//        mBinding.matchedProtocolEditText.setDrawableRightListener(new CustomEditText.DrawableRightListener() {
//            @Override
//            public void onDrawableRightClick(View view) {
//
//                showDialog(optionListDialog);
//            }
//        });

        mBinding.triggeredPortStartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setTriggeredPortStart(port);

                checkDiff();
            }
        });

        mBinding.triggeredPortEndEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String port = s.toString();

                mRule.setTriggeredPortEnd(port);

                checkDiff();
            }
        });

        TriggeredProtocolDialog.setOptionString(options);
        TriggeredProtocolDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                mRule.setTriggeredProtocol(position);

                setTriggeredProtocol();

                checkDiff();

                TriggeredProtocolDialog.dismiss();
            }
        });

        mBinding.imgTriggeredProtocol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(TriggeredProtocolDialog);
            }
        });
//        mBinding.triggeredProtocolEditText.setDrawableRightListener(new CustomEditText.DrawableRightListener() {
//            @Override
//            public void onDrawableRightClick(View view) {
//
//                showDialog(TriggeredProtocolDialog);
//            }
//        });

        mBinding.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRule != null) {
                    final ConfirmDialog confirmDialog = new ConfirmDialog();
                    confirmDialog.setTitle("Remove Rule");
                    confirmDialog.setContent("Are you sure you want to remove this port triggering rule?");
                    confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewModel.removePortTriggeringRule(mRule);
                            confirmDialog.dismiss();
                        }
                    });
                    confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            confirmDialog.dismiss();
                        }
                    });
                    showDialog(confirmDialog);
                }
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Port Triggering Rule");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataManager dataManager = DataManager.getInstance();
                dataManager.setPortTriggeringRule(null);

                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRule();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void getNewestRuleList() {
        DataManager dataManager = DataManager.getInstance();
        AdvanceWirelessSettings settings = dataManager.getAdvanceWirelessSettings();
        PortTriggeringList portForwardingList = settings.getPortTriggeringList();

        mRuleModelList.clear();
        mRuleModelList.addAll(portForwardingList.getPortTriggeringRule());
    }

    private boolean isNameDuplicated(String name) {
        for (PortTriggeringRuleModel ruleModel : mRuleModelList) {
            if (!ruleModel.getRuleId().equals(mRule.getId())) {
                if (name.equals(StringUtils.decodeHexString(ruleModel.getRuleName()))) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isMatchedPortCoverageDuplicated(PortTriggeringRule rule) {
        for (PortTriggeringRuleModel ruleModel : mRuleModelList) {
            if ("1".equals(ruleModel.getEnable()) &&
                    !(ruleModel.getRuleId().equals(mRule.getId()))) {
                String matchPort = ruleModel.getMatchPort();
                String[] separatedMatchPort = matchPort.split("-");
                String matchPortStart = null;
                String matchPortEnd = null;
                int matchStart = 0;
                int matchEnd = 0;

                if (separatedMatchPort.length == 2) {
                    matchPortStart = separatedMatchPort[0];
                    matchPortEnd = separatedMatchPort[1];
                }
                matchStart = Integer.parseInt(matchPortStart);
                matchEnd = Integer.parseInt(matchPortEnd);

                if (Math.max(Integer.parseInt(mRule.getMatchedPortStart()), matchStart) <=
                        Math.min(Integer.parseInt(mRule.getMatchedPortEnd()), matchEnd))
                    return true;
            }
        }

        return false;
    }

    private boolean isTriggeredPortCoverageDuplicated(PortTriggeringRule rule) {
        for (PortTriggeringRuleModel ruleModel : mRuleModelList) {
            if ("1".equals(ruleModel.getEnable()) &&
                    !(ruleModel.getRuleId().equals(mRule.getId()))) {
                String TriggerPort = ruleModel.getTriggerPort();
                String[] separatedTriggerPort = TriggerPort.split("-");
                String triggerPortStart = null;
                String triggerPortEnd = null;
                int triggerStart = 0;
                int triggerEnd = 0;

                if (separatedTriggerPort.length == 2) {
                    triggerPortStart = separatedTriggerPort[0];
                    triggerPortEnd = separatedTriggerPort[1];
                }
                triggerStart = Integer.parseInt(triggerPortStart);
                triggerEnd = Integer.parseInt(triggerPortEnd);

                if (Math.max(Integer.parseInt(mRule.getTriggeredPortStart()), triggerStart) <=
                        Math.min(Integer.parseInt(mRule.getTriggeredPortEnd()), triggerEnd))
                    return true;
            }
        }

        return false;
    }

    private boolean isRuleValid() {
        if (!mRule.isNameValid()) {
            String title = getString(R.string.invalid_rule_name_title);
            String message = getString(R.string.invalid_rule_name);

            showMessageDialog(title, message);

            return false;
        }

        if (isNameDuplicated(mRule.getName())) {
            String title = getString(R.string.invalid_rule_name_title);
            String message = "The rule with this name has already existed.";

            showMessageDialog(title, message);

            return false;
        }

        if (!mRule.isMatchedPortRangeValid()) {
            String title=getString(R.string.invalid_match_port_title);
            String message=getString(R.string.invalid_match_port_range);

            showMessageDialog(title,message);

            return false;
        }

        if (!mRule.isMatchedPortRuleValid()) {
            String title=getString(R.string.invalid_match_port_title);
            String message=getString(R.string.invalid_match_port_rule);

            showMessageDialog(title,message);

            return false;
        }

        if(!mRule.isMatchedProtocolValid()){
            String title=getString(R.string.invalid_protocol_title);
            String message=getString(R.string.invalid_matched_protocol);

            showMessageDialog(title,message);

            return false;
        }

        if(!mRule.isTriggeredRangeValid()){
            String title=getString(R.string.invalid_trigger_port_title);
            String message=getString(R.string.invalid_trigger_port_range);

            showMessageDialog(title,message);

            return false;
        }

        if(!mRule.isTriggeredRuleValid()){
            String title=getString(R.string.invalid_trigger_port_title);
            String message=getString(R.string.invalid_trigger_port_rule);

            showMessageDialog(title,message);

            return false;
        }

        if(!mRule.isTriggeredProtocolValid()){
            String title=getString(R.string.invalid_protocol_title);
            String message=getString(R.string.invalid_triggered_protocol);

            showMessageDialog(title,message);

            return false;
        }


        if (mRule.isEnabled()) {
            if (isMatchedPortCoverageDuplicated(mRule)) {
                String title = "Invalid Matched Port";
                String message = "The Matched Port interval is overlapped with other enabled rule.";

                showMessageDialog(title, message);

                return false;
            }

            if (isTriggeredPortCoverageDuplicated(mRule)) {
                String title = "Invalid Triggered Port";
                String message = "The Triggered Port interval is overlapped with other enabled rule.";

                showMessageDialog(title, message);

                return false;
            }
        }

        return true;
    }


    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(PortTriggeringRuleViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.GET_SETTINGS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Advance Wireless Settings", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            JsonObject jsonObject = json.getAsJsonObject("data");

                            Gson gson = new Gson();

                            AdvanceWirelessSettings advanceWirelessSettings = gson.fromJson(jsonObject, AdvanceWirelessSettings.class);

                            DataManager dataManager = DataManager.getInstance();
                            dataManager.setAdvanceWirelessSettings(advanceWirelessSettings);

                            // get newest rule list before update
                            getNewestRuleList();

                            // check rule
                            if (isRuleValid()) {

                                // execute update
                                mViewModel.updatePortTriggeringRule(mRule);
                            }
                        }
                    }
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.UPDATE_PORT_TRIGGERING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Port Triggering", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setPortTriggeringRule(null);

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.REMOVE_PORT_TRIGGERING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Port Triggering", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setPortTriggeringRule(null);

                        finish();
                    }
                }
            }
        });
    }

    private void setData(){
        setName();

        setEnable();

        setMatchedPort();

        setMatchedProtocol();

        setTriggeredPort();

        setTriggeredProtocol();

        setRemoveButton();
    }

    private void setName(){
        String name = mRule.getName();
        mBinding.customEditText.setText(name);
    }

    private void setEnable(){
        boolean isEnabled = mRule.isEnabled();

        if (isEnabled) {
            mBinding.enableTextView.setText("Enabled");
            mBinding.enableImageView.setSelected(true);
        } else {
            mBinding.enableTextView.setText("Disabled");
            mBinding.enableImageView.setSelected(false);
        }
    }

    private void setMatchedPort(){
        String startPort = mRule.getMatchedPortStart();

        mBinding.matchedPortStartEditText.setText(startPort);

        String endPort = mRule.getMatchedPortEnd();

        mBinding.matchedPortEndEditText.setText(endPort);
    }

    private void setMatchedProtocol(){
        String protocol = mRule.getDisplayMatchedProtocol();

        //mBinding.matchedProtocolEditText.setText(protocol);
        mBinding.tvMatchedProtocol.setText(protocol);
    }

    private void setTriggeredPort(){
        String startPort = mRule.getTriggeredPortStart();

        mBinding.triggeredPortStartEditText.setText(startPort);

        String endPort = mRule.getTriggeredPortEnd();

        mBinding.triggeredPortEndEditText.setText(endPort);
    }

    private void setTriggeredProtocol(){
        String protocol = mRule.getDisplayTriggeredProtocol();

        //mBinding.triggeredProtocolEditText.setText(protocol);
        mBinding.tvTriggeredProtocol.setText(protocol);
    }

    private void setRemoveButton(){
        String id = mRule.getId();
        if (id == null) {
            mBinding.removeButton.setEnabled(false);

            return;
        }

        mBinding.removeButton.setEnabled(true);
    }

    private void checkDiff() {
        if (mRule.isChanged()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void sendRule() {
//        if (!mRule.isNameValid()) {
//            String title = getString(R.string.invalid_rule_name_title);
//            String message = getString(R.string.invalid_rule_name);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        if (!mRule.isMatchedPortRangeValid()) {
//            String title = getString(R.string.invalid_match_port_title);
//            String message = getString(R.string.invalid_match_port_range);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        if (!mRule.isMatchedPortRuleValid()) {
//            String title = getString(R.string.invalid_match_port_title);
//            String message = getString(R.string.invalid_match_port_rule);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        if (!mRule.isMatchedProtocolValid()) {
//            String title = getString(R.string.invalid_protocol_title);
//            String message = getString(R.string.invalid_protocol);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        if (!mRule.isTriggeredRangeValid()) {
//            String title = getString(R.string.invalid_trigger_port_title);
//            String message = getString(R.string.invalid_trigger_port_range);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        if (!mRule.isTriggeredRuleValid()) {
//            String title = getString(R.string.invalid_trigger_port_title);
//            String message = getString(R.string.invalid_trigger_port_rule);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        if (!mRule.isTriggeredProtocolValid()) {
//            String title = getString(R.string.invalid_protocol_title);
//            String message = getString(R.string.invalid_protocol);
//
//            showMessageDialog(title, message);
//
//            return;
//        }
//
//        mViewModel.updatePortTriggeringRule(mRule);

        mViewModel.getAdvanceWirelessSettings();

    }

}
