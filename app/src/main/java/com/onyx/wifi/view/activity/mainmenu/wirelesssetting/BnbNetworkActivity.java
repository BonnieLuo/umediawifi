package com.onyx.wifi.view.activity.mainmenu.wirelesssetting;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityBnbNetworkBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.BnbNetworkSetting;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.WirelessSettingViewModel;

import java.util.Calendar;

public class BnbNetworkActivity extends BaseWirelessSettingActivity {

    private ActivityBnbNetworkBinding mBinding;
    private WirelessSettingViewModel mViewModel;

    private BnbNetworkSetting mOriginalBnbNetworkData;
    private BnbNetworkSetting mTempBnbNetworkData;
    private DatePickerDialog mStartDatePickerDialog;
    private DatePickerDialog mEndDatePickerDialog;
    private SettingStatus mSettingStatus = SettingStatus.FIRST_TIME;

    private String mInputErrorMsg = "";

    public enum SettingStatus {
        FIRST_TIME, ENABLE, NOT_ENABLE
    }

// sample:
//    bnbNetworkSetting =             {
//        enabled = 1;
//        end = 1567526399;
//        pwd = 4f316a6269797469;
//        ssid = 4c796e6e54657374426f6172645f426e42;
//        start = 1567440000;
//    };

    private void correctBnBStartTime(BnbNetworkSetting bnbNetworkSetting) {
        Calendar currentTime = Calendar.getInstance();

        currentTime.set(
                currentTime.get(Calendar.YEAR),
                currentTime.get(Calendar.MONTH),
                currentTime.get(Calendar.DATE),
                0,
                0,
                0);

        bnbNetworkSetting.setStart((int) (currentTime.getTimeInMillis() / 1000));
    }

    private void correctBnBEndTime(BnbNetworkSetting bnbNetworkSetting) {
        Calendar currentTime = Calendar.getInstance();

        currentTime.set(
                currentTime.get(Calendar.YEAR),
                currentTime.get(Calendar.MONTH),
                currentTime.get(Calendar.DATE),
                23,
                59,
                59);

        bnbNetworkSetting.setEnd((int) (currentTime.getTimeInMillis() / 1000));

    }

    private void initBnbNetworkData(BnbNetworkSetting bnbNetworkSetting) {
        Calendar currentTime = Calendar.getInstance();
        bnbNetworkSetting.setEnable(false);
        bnbNetworkSetting.setSsid(InputUtils.getDefaultBnbNetworkSsid());
        bnbNetworkSetting.setPwd("");

        currentTime.set(
                currentTime.get(Calendar.YEAR),
                currentTime.get(Calendar.MONTH),
                currentTime.get(Calendar.DATE),
                0,
                0,
                0);
        bnbNetworkSetting.setStart((int) (currentTime.getTimeInMillis() / 1000));

        currentTime.set(
                currentTime.get(Calendar.YEAR),
                currentTime.get(Calendar.MONTH),
                currentTime.get(Calendar.DATE),
                23,
                59,
                59);

        bnbNetworkSetting.setEnd((int) (currentTime.getTimeInMillis() / 1000));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOriginalBnbNetworkData = DataManager.getInstance().getBnbNetworkSetting();
        mTempBnbNetworkData = DataManager.getInstance().getBnbNetworkSetting();

        if (mOriginalBnbNetworkData == null) {
            mOriginalBnbNetworkData = new BnbNetworkSetting();
            mTempBnbNetworkData = new BnbNetworkSetting();
            initBnbNetworkData(mOriginalBnbNetworkData);
            initBnbNetworkData(mTempBnbNetworkData);
            mSettingStatus = SettingStatus.FIRST_TIME;
        } else if (!mOriginalBnbNetworkData.getEnable()) {
            initBnbNetworkData(mOriginalBnbNetworkData);
            initBnbNetworkData(mTempBnbNetworkData);
            mSettingStatus = SettingStatus.NOT_ENABLE;
        } else {
            mSettingStatus = SettingStatus.ENABLE;
        }

        // error handling: 防止cloud傳來start:0 end: 0的狀況
        if (mOriginalBnbNetworkData.getStart() == 0 || mOriginalBnbNetworkData.getEnd() == 0) {
            if (mOriginalBnbNetworkData.getStart() == 0) {
                correctBnBStartTime(mOriginalBnbNetworkData);
                correctBnBStartTime(mTempBnbNetworkData);
            }

            if (mOriginalBnbNetworkData.getEnd() == 0) {
                correctBnBEndTime(mOriginalBnbNetworkData);
                correctBnBEndTime(mTempBnbNetworkData);
            }
        }

        LogUtils.trace("BnbNetwork", mOriginalBnbNetworkData.toString());

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bnb_network);
        setView();
        setViewModel();
        setData();
    }

    private void setLimitationOfStartPicker(long minDateInMillis, long maxDateInMillis) {
        mStartDatePickerDialog.getDatePicker().setMinDate(0);
        mStartDatePickerDialog.getDatePicker().setMinDate(minDateInMillis);
        if (maxDateInMillis != -1) {
            mStartDatePickerDialog.getDatePicker().setMaxDate(12345678L);
            mStartDatePickerDialog.getDatePicker().setMaxDate(maxDateInMillis);
        }
    }

    private void setLimitationOfEndPicker(long minDateInMillis) {
        mEndDatePickerDialog.getDatePicker().setMinDate(0);
        mEndDatePickerDialog.getDatePicker().setMinDate(minDateInMillis);

    }

    private void setFocusOfStartDatePickerDialog() {
        Calendar start = DateUtils.secToCalendar(mTempBnbNetworkData.getStart());
//        CommonUtils.toast(mContext, "" +
//                start.get(Calendar.YEAR) +
//                start.get(Calendar.MONTH) +
//                start.get(Calendar.DAY_OF_MONTH));

        mStartDatePickerDialog.getDatePicker().updateDate(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH));

    }

    private void setFocusOfEndDatePickerDialog() {
        Calendar end = DateUtils.secToCalendar(mTempBnbNetworkData.getEnd());
//        CommonUtils.toast(mContext, "" +
//                end.get(Calendar.YEAR) +
//                end.get(Calendar.MONTH) +
//                end.get(Calendar.DAY_OF_MONTH));

        mEndDatePickerDialog.getDatePicker().updateDate(end.get(Calendar.YEAR), end.get(Calendar.MONTH), end.get(Calendar.DAY_OF_MONTH));

    }

    private void initStartDatePickerDialog() {
        Calendar startCal = DateUtils.secToCalendar(mTempBnbNetworkData.getStart());
        mStartDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                // 1. 存至mTempBnbNetworkData的start
                mTempBnbNetworkData.setStart(DateUtils.datePickerToStartTimeInSeconds(year, month, dayOfMonth));

                // 2. 顯示
                mBinding.tvBnbStarts.setText("Starts:   " + DateUtils.secToTimeDescription(mTempBnbNetworkData.getStart()));

                // 3. 重新設置mEndDatePickerDialog
                initEndDatePickerDialog();
                setLimitationOfEndPicker(((long) (mTempBnbNetworkData.getStart())) * 1000);

                // 4. check if data change
                checkIfDataChanged();
            }
        }, startCal.get(Calendar.YEAR), startCal.get(Calendar.MONTH), startCal.get(Calendar.DAY_OF_MONTH));
    }

    private void initEndDatePickerDialog() {
        Calendar endCal = DateUtils.secToCalendar(mTempBnbNetworkData.getEnd());
        mEndDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                // 1. 存至mTempBnbNetworkData的End
                mTempBnbNetworkData.setEnd(DateUtils.datePickerToEndTimeInSeconds(year, month, dayOfMonth));

                // 2. 顯示
                mBinding.tvBnbExpires.setText("Expires: " + DateUtils.secToTimeDescription(mTempBnbNetworkData.getEnd()));

                // 3. 重新設置mStartDatePickerDialog
                initStartDatePickerDialog();
                setLimitationOfStartPicker(System.currentTimeMillis(), -1);//((long) (mTempBnbNetworkData.getEnd())) * 1000);

                // 4. check if data change
                checkIfDataChanged();
            }
        }, endCal.get(Calendar.YEAR), endCal.get(Calendar.MONTH), endCal.get(Calendar.DAY_OF_MONTH));

    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // Starts:
        // 1. (1) new dialog
        //    (2) define onDateSet: 選好日期(startDate)後對mEndDatePickerDialog做以下動作
        //        -　重新new
        //        -  重新設定limitataion(minDate: startDate)
        initStartDatePickerDialog();

        // 2. (1) set the limitation of mStartDatePickerDialog
        //          -  minDate: current date
        //          -  maxDate: endDate
        setLimitationOfStartPicker(System.currentTimeMillis(), -1);//((long) (mTempBnbNetworkData.getEnd())) * 1000); // minDate, maxDate
        //    (2) set focus date of mStartDatePickerDialog
        setFocusOfStartDatePickerDialog();

        mBinding.btnEditBnbStarts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitEditMode(mBinding.etBnbPwdValue);
                exitEditMode(mBinding.etBnbSsidValue);
                setFocusOfStartDatePickerDialog();
                mStartDatePickerDialog.show();
            }
        });


        // Expires:
        // 1. (1) new dialog
        //    (2) define onDateSet: 選好日期(endDate)後對mStartDatePickerDialog做以下動作
        //        -　重新new
        //        -  重新設定limitataion (minDate: current date, maxDate: endDate)
        initEndDatePickerDialog();

        // 2. (1) set the limitation of mEndDatePickerDialog
        //          -  minDate: current date
        setLimitationOfEndPicker(((long) (mTempBnbNetworkData.getStart())) * 1000);//System.currentTimeMillis()); // minDate
        //    (2) set focus date of mEndDatePickerDialog
        setFocusOfEndDatePickerDialog();

        mBinding.btnEditBnbExpires.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitEditMode(mBinding.etBnbPwdValue);
                exitEditMode(mBinding.etBnbSsidValue);
                setFocusOfEndDatePickerDialog();
                mEndDatePickerDialog.show();
            }
        });


        // [按鈕] SSID 的筆型圖示
        mBinding.btnEditBnbSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEditMode(mBinding.etBnbSsidValue);
            }
        });

        // [文字元件] SSID, 設置各項事件
        mBinding.etBnbSsidValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            // 監聽 EditText 的焦點以判斷是否需要結束編輯模式
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkFocusChange(mBinding.etBnbSsidValue, hasFocus);
            }
        });

        mBinding.etBnbSsidValue.addTextChangedListener(new TextWatcher() {
            // 監聽 EditText 的文字改變
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // 當使用者輸入完要將值儲存到 temp 物件, 並判斷是否需要顯示勾勾
                String ssid = s.toString().trim();
                mTempBnbNetworkData.setSsid(ssid);
                checkIfDataChanged();
            }
        });

        mBinding.etBnbSsidValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            // 監聽鍵盤 enter 鍵的事件
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // 在 layout 裡有設定 EditText 的 imeOptions="actionDone", 故這裡是判斷 IME_ACTION_DONE
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    exitEditMode(mBinding.etBnbSsidValue);
                }
                return false;
            }
        });

        // [按鈕] password 的筆型圖示
        mBinding.btnEditBnbPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEditMode(mBinding.etBnbPwdValue);
            }
        });

        // [文字元件] password, 設置各項事件 (同上述 SSID 文字元件的說明)
        mBinding.etBnbPwdValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkFocusChange(mBinding.etBnbPwdValue, hasFocus);
            }
        });

        mBinding.etBnbPwdValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String password = s.toString().trim();
                mTempBnbNetworkData.setPwd(password);
                checkIfDataChanged();
            }
        });

        mBinding.etBnbPwdValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    exitEditMode(mBinding.etBnbPwdValue);
                }
                return false;
            }
        });
        // [按鈕] password 的眼睛圖示
        mBinding.btnShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setPasswordVisible(mBinding.etBnbPwdValue, isChecked);
            }
        });
        // [按鈕] Generate Random Password
        mBinding.btnGeneratePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.etBnbPwdValue.setText(InputUtils.generateNetworkPassword());
            }
        });

        // Delete BnB Network Instance
        if (mSettingStatus == SettingStatus.FIRST_TIME ||
                mSettingStatus == SettingStatus.NOT_ENABLE) {
            mBinding.btnDeleteBnbNetworkInstance.setEnabled(false);
        } else {
            mBinding.btnDeleteBnbNetworkInstance.setEnabled(true);
        }

        mBinding.btnDeleteBnbNetworkInstance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update bnb network (2):
                // enable is false
                mTempBnbNetworkData.setEnable(false);
                mViewModel.updateBnbNetwork(mTempBnbNetworkData);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("BnB Network");
        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitEditMode(mBinding.etBnbSsidValue);
                exitEditMode(mBinding.etBnbPwdValue);
                if (isDataValid() && isDateSettingValid()) {
                    // update bnb network (1):
                    // enable is true
                    mTempBnbNetworkData.setEnable(true);
                    mViewModel.updateBnbNetwork(mTempBnbNetworkData);
                } else {
                    if (!isDataValid()) {
                        showMessageDialog("Invalid Input", mInputErrorMsg);
                    } else if (!isDateSettingValid()) {
                        showMessageDialog("Invalid Input", "Expire time SHALL NOT be larger than start time.");
                    }
                }
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(WirelessSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.WirelessSetting.UPDATE_BNB_NETWORK_SETTING) {
                        if (resultStatus.success) {
                            finish();
                        } else {
                            showMessageDialog("Update BnB Network", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void setData() {
        // set original start
        mBinding.tvBnbStarts.setText("Starts:   " + DateUtils.secToTimeDescription(mOriginalBnbNetworkData.getStart()));

        // set original expires
        mBinding.tvBnbExpires.setText("Expires: " + DateUtils.secToTimeDescription(mOriginalBnbNetworkData.getEnd()));

        // set original ssid
        mBinding.etBnbSsidValue.setText(mOriginalBnbNetworkData.getSsid());

        // set original password
        mBinding.etBnbPwdValue.setText(mOriginalBnbNetworkData.getPwd());

        // set as multi-line display mode
        setMultiLineDisplay(mBinding.etBnbSsidValue);
        setMultiLineDisplay(mBinding.etBnbPwdValue);
    }

    private void enterEditMode(EditText editText) {
        editText.setEnabled(true);
        editText.requestFocus();
        showKeyboard(editText);
        showKeyboard(editText);
        editText.setSelection(editText.getEditableText().length());
    }

    private void exitEditMode(EditText editText) {
        editText.setEnabled(false);
    }

    private void checkFocusChange(EditText editText, boolean hasFocus) {
        // EditText 失去焦點時 (例如: 點擊 EditText 以外的區域) 要結束編輯模式
        if (!hasFocus) {
            exitEditMode(editText);
        }
    }

    private void checkIfDataChanged() {
        boolean isChanged = false;

        if (mTempBnbNetworkData.getStart() != mOriginalBnbNetworkData.getStart()) {
            isChanged = true;
        }

        if (mTempBnbNetworkData.getEnd() != mOriginalBnbNetworkData.getEnd()) {
            isChanged = true;
        }

        if (!mTempBnbNetworkData.getSsid().equals(mOriginalBnbNetworkData.getSsid())) {
            isChanged = true;
        }

        if (!mTempBnbNetworkData.getPwd().equals(mOriginalBnbNetworkData.getPwd())) {
            isChanged = true;
        }

        mBinding.titleBar.setRightButtonVisibility((isChanged) ? View.VISIBLE : View.INVISIBLE);
    }

    private boolean isDateSettingValid() {
        Calendar start = DateUtils.secToCalendar(mTempBnbNetworkData.getStart());
        Calendar end = DateUtils.secToCalendar(mTempBnbNetworkData.getEnd());
        if ((end.get(Calendar.YEAR) < start.get(Calendar.YEAR)) ||
                ((end.get(Calendar.YEAR) == start.get(Calendar.YEAR)) && (end.get(Calendar.MONTH) < start.get(Calendar.MONTH))) ||
                ((end.get(Calendar.YEAR) == start.get(Calendar.YEAR)) && (end.get(Calendar.MONTH) == start.get(Calendar.MONTH)) && (end.get(Calendar.DAY_OF_MONTH) < start.get(Calendar.DAY_OF_MONTH))))
            return false;
        else
            return true;
    }

    private boolean isDataValid() {
        String msgSsidInvalid = InputUtils.getInvalidWifiSsidMessage("SSID");
        String msgPwdInvalid = InputUtils.getInvalidWifiPasswordMessage("password");
        String msgWhitespace = mContext.getString(R.string.err_ssid_invalid_space);

        //
        //  check for ssid and password ( start and end time are checked in corresponding Datapickerdialog
        //
        if (!InputUtils.isWifiSsidValid(mTempBnbNetworkData.getSsid())) {
            mInputErrorMsg = msgSsidInvalid;
            return false;
        } else if (!InputUtils.isWifiPasswordValid(mTempBnbNetworkData.getPwd())) {
            mInputErrorMsg = msgPwdInvalid;
            return false;
        } else if (InputUtils.containsWhitespace(mTempBnbNetworkData.getSsid())) {
            // TODO: constraint to be removed if Bug #7076 is solved
            // https://umedia.plan.io/issues/7076?pn=1#change-28750
            mInputErrorMsg = msgWhitespace;
            return false;
        }
        return true;
    }

}
