package com.onyx.wifi.view.interfaces;

public interface TextChangedListener {
    void afterTextChangedDone();
}
