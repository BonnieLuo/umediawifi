package com.onyx.wifi.view.activity.base;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.mainmenu.AnalyticsActivity;
import com.onyx.wifi.view.activity.mainmenu.SecurityActivity;
import com.onyx.wifi.view.activity.mainmenu.accountsetting.AccountSettingActivity;
import com.onyx.wifi.view.activity.mainmenu.networksetting.NetworkSettingActivity;
import com.onyx.wifi.view.activity.mainmenu.troubleshooting.TroubleshootingActivity;
import com.onyx.wifi.view.activity.mainmenu.voiceassistance.VoiceAssistanceActivity;
import com.onyx.wifi.view.activity.mainmenu.wirelesssetting.WirelessSettingActivity;
import com.onyx.wifi.view.activity.menubar.dashboard.DashboardActivity;
import com.onyx.wifi.view.activity.menubar.member.list.MemberListActivity;
import com.onyx.wifi.view.activity.menubar.pause.GlobalPauseActivity;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.pause.GlobalPauseViewModel;

import java.lang.reflect.Type;
import java.util.List;

public class BaseMenuActivity extends BaseTitleActivity {

    protected MenuBar mMenuBar;
    private GlobalPauseViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.EVENT_CLIENT_COUNT);
        registerReceiver(mEventReceiver, intentFilter);

        if (mMenuBar != null) {
            mMenuBar.setData();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace(action);
            if (action.equals(AppConstants.EVENT_CLIENT_COUNT)) {
                if (mMenuBar != null) {
                    mMenuBar.setData();
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (mMenuBar != null && mMenuBar.isMainMenuOpened()) {
            // 若有開啟 main menu 時, 按返回鍵關閉 main menu 就好, 不要結束頁面
            mMenuBar.closeMainMenu();
        } else {
            super.onBackPressed();
        }
    }

    protected void setMenuBar(MenuBar menuBar) {
        mMenuBar = menuBar;
        setImmerseLayout(mMenuBar.getMenuTitleBar());

        setMenuBarItem();
        setMainMenuItem();
    }

    private void setMenuBarItem() {
        setHomeItem();
        setMemberItem();
        setMenuItem();
        setPauseItem();
        setNotificationItem();
    }

    private void setHomeItem() {
        mMenuBar.setMenuBarItemClickListener(MenuBar.MenuBarItem.HOME, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMenuBar.getMenuBarFocusItem() != MenuBar.MenuBarItem.HOME) {
                    startDashboardActivity(BaseMenuActivity.this);
                    finish();
                } else {
                    // focus 的項目已是自身, 若 main menu 是開啟狀態, 只要關閉 main menu, 畫面就回到自身頁面
                    if (mMenuBar.isMainMenuOpened()) {
                        mMenuBar.closeMainMenu();
                    }
                }
            }
        });
    }

    private void setMemberItem() {
        mMenuBar.setMenuBarItemClickListener(MenuBar.MenuBarItem.MEMBER, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMenuBar.getMenuBarFocusItem() != MenuBar.MenuBarItem.MEMBER) {
                    Intent intent = new Intent(BaseMenuActivity.this, MemberListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    if (!(BaseMenuActivity.this instanceof DashboardActivity)) {
                        finish();
                    }
                } else {
                    // focus 的項目已是自身, 若 main menu 是開啟狀態, 只要關閉 main menu, 畫面就回到自身頁面
                    if (mMenuBar.isMainMenuOpened()) {
                        mMenuBar.closeMainMenu();
                    }
                }
            }
        });
    }

    private void setMenuItem() {
        mMenuBar.setMenuBarItemClickListener(MenuBar.MenuBarItem.MENU, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMenuBar.toggleMainMenu();
            }
        });
    }

    private void setPauseItem() {
        mMenuBar.setMenuBarItemClickListener(MenuBar.MenuBarItem.PAUSE, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel = ViewModelProviders.of(BaseMenuActivity.this).get(GlobalPauseViewModel.class);
                setLoadingObserve(mViewModel, BaseMenuActivity.this, findViewById(R.id.loading));
                mViewModel.getResultStatus().observe(BaseMenuActivity.this, new Observer<ResultStatus>() {
                    @Override
                    public void onChanged(@Nullable ResultStatus resultStatus) {
                        if (resultStatus != null) {
                            if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                                if (!resultStatus.success) {
                                    showMessageDialog("Get Member List", resultStatus.errorMsg);

                                    return;
                                }

                                mViewModel.getAllClients();
                            }

                            if (resultStatus.actionCode == Code.Action.UserAccount.GET_ALL_CLIENTS) {
                                if (!resultStatus.success) {
                                    showMessageDialog("Get All Clients", resultStatus.errorMsg);

                                    return;
                                }

                                JsonObject json = (JsonObject) resultStatus.data;
                                if (json != null) {
                                    JsonObject dataJson = json.getAsJsonObject("data");

                                    JsonArray clientListJSON = dataJson.getAsJsonArray("client_list");

                                    if (clientListJSON != null) {
                                        Gson gson = new Gson();
                                        Type listType = new TypeToken<List<ClientModel>>() {
                                        }.getType();
                                        List<ClientModel> allClientModelList = gson.fromJson(clientListJSON.toString(), listType);

                                        DataManager dataManager = DataManager.getInstance();
                                        dataManager.setAllClientModelList(allClientModelList);
                                    }

                                }

                                //startActivity(new Intent(mContext, GlobalPauseActivity.class));
                            }
                        }
                    }
                });
                mViewModel.getMemberList();
                startActivity(new Intent(mContext, GlobalPauseActivity.class));// Bonnie
            }
        });
    }


    private void setNotificationItem() {
        mMenuBar.setMenuBarItemClickListener(MenuBar.MenuBarItem.NOTIFICATION, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void setMainMenuItem() {
        setNetworkSettingItem();
        setWirelessSettingItem();
        // device 尚未支援 Analytics 功能
//        setAnalyticsItem();
        setParentalControlItem();
        setVoiceAssistanceItem();
        // device 尚未支援 Security 功能
//        setSecurityItem();
        setAccountItem();
        setTroubleshootingItem();
    }

    private void setNetworkSettingItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.NETWORK_SETTING, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, NetworkSettingActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof NetworkSettingActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.NETWORK_SETTING);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setWirelessSettingItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.WIRELESS_SETTING, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, WirelessSettingActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof WirelessSettingActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.WIRELESS_SETTING);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setAnalyticsItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.ANALYTICS, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, AnalyticsActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof AnalyticsActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.ANALYTICS);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setParentalControlItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.PARENTAL_CONTROL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 由於 device 尚未支援 Parental Control, 該項目點下去的頁面先換成跟下方 menu bar 的 Member 項目相同的頁面
                if (mContext instanceof DashboardActivity) {
                    // TODO: 待支援 Parental Control 功能後, 改成開啟 ParentalControlActivity
                    startActivity(new Intent(mContext, MemberListActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof MemberListActivity) {
                    // TODO: 待支援 Parental Control 功能後, 改成判斷 ParentalControlActivity
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.PARENTAL_CONTROL);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setVoiceAssistanceItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.VOICE_ASSISTANCE, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, VoiceAssistanceActivity.class));
                    CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "false");
                    CommonUtils.setSharedPrefData(AppConstants.KEY_SELECTED_INDEX, "-1");
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof VoiceAssistanceActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.VOICE_ASSISTANCE);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setSecurityItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.SECURITY, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, SecurityActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof SecurityActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.SECURITY);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setAccountItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.ACCOUNT, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, AccountSettingActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof AccountSettingActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.ACCOUNT);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setTroubleshootingItem() {
        mMenuBar.setMainMenuItemClickListener(MenuBar.MainMenuItem.TROUBLESHOOTING, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mContext instanceof DashboardActivity) {
                    startActivity(new Intent(mContext, TroubleshootingActivity.class));
                    mMenuBar.closeMainMenu();
                } else if (mContext instanceof TroubleshootingActivity) {
                    mMenuBar.closeMainMenu();
                } else {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MAIN_MENU_ITEM, MenuBar.MainMenuItem.TROUBLESHOOTING);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
