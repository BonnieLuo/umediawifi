package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.AudioDevice;

import java.util.ArrayList;

public class
VoiceDeviceAdapter extends RecyclerView.Adapter<VoiceDeviceAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(AudioDevice audioDevice);

        void onMuteUnMuteClick(String muteUnMute, AudioDevice audioDevice);
    }

    private Context mContext;
    public static ArrayList<AudioDevice> mData = new ArrayList<AudioDevice>(); // "public static": global data for related pages(activities)
    //
    // 2. 宣告interface
    //
    private OnItemClickHandler mClickHandler;
    private int mMutePosition = -1;

    public VoiceDeviceAdapter(Context context, ArrayList<AudioDevice> data, OnItemClickHandler clickHandler) {
        mContext = context;
        //mData = data;
        mData.addAll(data);
        mClickHandler = clickHandler;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private ImageView imgIsAlexaConnect;
        private ImageView imgIsDeviceConnect;
        private TextView tvUMEDIAVoice;
        private TextView tvName;
        private ImageView imgIsMute;

        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            imgIsAlexaConnect = (ImageView) itemView.findViewById(R.id.imgIsAlexaConnect);
            imgIsDeviceConnect = (ImageView) itemView.findViewById(R.id.imgIsDeviceConnect); // 從view(list item)中取得TextView實體
            tvUMEDIAVoice = (TextView) itemView.findViewById(R.id.tvUMEDIAVoice);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            imgIsMute = (ImageView) itemView.findViewById(R.id.imgIsMute);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AudioDevice audioDevice = mData.get(getAdapterPosition());
                    //mSelectedPos = getAdapterPosition();
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(audioDevice);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });

            imgIsMute.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    AudioDevice audioDevice = mData.get(getAdapterPosition());
                    mMutePosition = getAdapterPosition();

                    // 4. 呼叫interface的method
                    if (audioDevice.getIsMute()) {
                        LogUtils.trace("Bonnie_mute", "[Mute -> UnMute]");
                        mClickHandler.onMuteUnMuteClick("0", audioDevice);

                        //imgIsMute.setImageResource(R.drawable.ic_voice_mic);
                    } else {
                        LogUtils.trace("Bonnie_mute", "[UnMute -> Mute]");
                        mClickHandler.onMuteUnMuteClick("1", audioDevice);

                        //imgIsMute.setImageResource(R.drawable.ic_voice_privacy);
                    }
                }
            });
        }
    }

    // binding with layout
    @Override // required
    public VoiceDeviceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_voice_device_list_item, viewGroup, false); //取得list的view
        return new ViewHolder(view);
    }

    // set data (according to the status of each device)
    @Override // required
    public void onBindViewHolder(@NonNull VoiceDeviceAdapter.ViewHolder viewHolder, int i) {

        if (mData.get(i).getIsDeviceConnect()) { // device is connect
            if (mData.get(i).getIsAlexaConnect()) { // Alexa is connect
                LogUtils.trace("status", "onboard");
                viewHolder.imgIsMute.setVisibility(View.VISIBLE);
                viewHolder.imgIsAlexaConnect.setImageResource(R.drawable.ic_alexa);
                if (mData.get(i).getIsMute())
                    viewHolder.imgIsMute.setImageResource(R.drawable.ic_voice_privacy);
                else
                    viewHolder.imgIsMute.setImageResource(R.drawable.ic_voice_mic);
            }
            else {
                LogUtils.trace("status", "not onboard");
            }
            viewHolder.imgIsDeviceConnect.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
            viewHolder.imgIsDeviceConnect.setImageAlpha(0xFF);
            viewHolder.tvUMEDIAVoice.setTextColor(ContextCompat.getColor(mContext, R.color.gray_828282));
            viewHolder.tvUMEDIAVoice.setText("UMEDIA Voice");//(mData.get(i).getName());
            viewHolder.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
            viewHolder.tvName.setText(mData.get(i).getLocation());

        } else {                                 // device is not connect
            viewHolder.imgIsDeviceConnect.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
            viewHolder.imgIsDeviceConnect.setImageAlpha(0x80);
            viewHolder.tvUMEDIAVoice.setTextColor(ContextCompat.getColor(mContext, R.color.gray_cccccc));
            viewHolder.tvUMEDIAVoice.setText("UMEDIA Voice");//viewHolder.tvUMEDIAVoice.setText(mData.get(i).getName());
            viewHolder.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.gray_cccccc));
            viewHolder.tvName.setText(mData.get(i).getLocation());
            viewHolder.imgIsAlexaConnect.setVisibility(View.INVISIBLE);
            viewHolder.imgIsMute.setVisibility(View.INVISIBLE);
        }
    }

    public int getMuteUnMutePos() {
        return mMutePosition;
    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public void setDevice(ArrayList<AudioDevice> data) {
        //通过System.identityHashCode(object)方法来间接的获取内存地址；
        //创建出来的对象，只要没被销毁，内存地址始终不变。
        //Adapter绑定的数据源集合要为同一个集合，notifyDataSetChanged()方法才有效，否则需要重新设置数据源
        //mData = data; <-錯誤寫法，address可能會變更
        mData.clear();
        mData.addAll(data);
        LogUtils.trace("status","address: "+System.identityHashCode(mData));
        notifyDataSetChanged();
    }
}
