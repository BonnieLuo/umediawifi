package com.onyx.wifi.view.activity.menubar.pause;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySelectFromMemberBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.model.item.pause.PauseProfile;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.pause.FamilyMemberListAdapter;
import com.onyx.wifi.view.interfaces.pause.OnMemberSelectedListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class SelectFromMemberActivity extends BaseMenuActivity implements OnMemberSelectedListener {

    private ActivitySelectFromMemberBinding mBinding;

    private FamilyMemberListAdapter mAdapter;

    private List<String> mTmpSelectedMemberList = new ArrayList<>();
    private List<String> mSelectedMemberList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_from_member);

        mAdapter = new FamilyMemberListAdapter(this);

        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            setData();
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Global Pause Profile");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataManager dataManager = DataManager.getInstance();
                dataManager.setSelectedClientList(null);
                PauseProfile pauseProfile = dataManager.getEditPauseProfile();

                /////////////////////////////////////

                if (mSelectedMemberList != null) {
                    mSelectedMemberList.clear();

                    if (mTmpSelectedMemberList != null)
                        mSelectedMemberList.addAll(mTmpSelectedMemberList);
                    //mSelectedMemberList = selectedList; //<-此寫法只要執行此行之後，兩個array就指向同一address，怎麼比都相同
                } else {
                    mSelectedMemberList = new ArrayList<String>(mTmpSelectedMemberList);
                }
                /////////////////////////////////////


                if (pauseProfile != null) {
                    pauseProfile.setExcludeMemberIdList(mSelectedMemberList);
                    pauseProfile.setExcludeCidList(null);
                }

                dataManager.setEditPauseProfile(pauseProfile);
                finish();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setData() {
        DataManager dataManager = DataManager.getInstance();

        MemberList memberList = dataManager.getMemberList();

        if (memberList == null) {
            return;
        }

        PauseProfile pauseProfile = dataManager.getEditPauseProfile();

        if (pauseProfile != null) {

            //mTmpSelectedMemberList = pauseProfile.getExcludeMemberIdList();
            mTmpSelectedMemberList.clear();
            mSelectedMemberList.clear();

            if (pauseProfile.getExcludeMemberIdList() != null) {
                mTmpSelectedMemberList.addAll(pauseProfile.getExcludeMemberIdList());
                mSelectedMemberList.addAll(pauseProfile.getExcludeMemberIdList());
            }
        }

        List<MemberModel> familyMemeberModelList = memberList.getFamilyMemberList();
        mAdapter.setData(familyMemeberModelList, mSelectedMemberList);
    }

    @Override
    public void OnMemberSelectedListener(List<String> selectedList) {
        boolean isChanged = isChanged(selectedList);

        if (isChanged) {
            if (mTmpSelectedMemberList != null) {
                mTmpSelectedMemberList.clear();

                if (selectedList != null) {
                    mTmpSelectedMemberList.addAll(selectedList);
                }
                //mSelectedMemberList = selectedList; //<-此寫法只要執行此行之後，兩個array就指向同一address，怎麼比都相同
            } else {
                mTmpSelectedMemberList = new ArrayList<String>(selectedList);
            }
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    public boolean isChanged(List<String> selectedList) {
        if (mSelectedMemberList == null && selectedList == null) {
            return false;
        }

        if (mSelectedMemberList == null && selectedList != null) {
            return true;
        }

        if (mSelectedMemberList != null && selectedList == null) {
            return true;
        }

        if (mSelectedMemberList.size() != selectedList.size()) {
            return true;
        }

        Collection<String> similar = new HashSet<String>(mSelectedMemberList);
        Collection<String> different = new HashSet<String>();
        different.addAll(mSelectedMemberList);
        different.addAll(selectedList);

        similar.retainAll(selectedList);
        different.removeAll(similar);

        return different.size() > 0;
    }
}
