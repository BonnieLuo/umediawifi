package com.onyx.wifi.view.fragment.setup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentDeviceSetLabelBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.dialog.InputDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.setup.DeviceSetupViewModel;

import java.util.ArrayList;
import java.util.Arrays;

public class DeviceSetLabelFragment extends BaseFragment {

    private FragmentDeviceSetLabelBinding mBinding;
    private DeviceSetupViewModel mViewModel;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_device_set_label, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewModel();
        setView();
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(DeviceSetupViewModel.class);
    }

    private void setView() {
        switch (DataManager.getInstance().getSetupMode()) {
            case ROUTER:
            case ROUTER_RESETUP:
                mBinding.tvSubTitle.setText(R.string.router_set_label_subtitle);
                mBinding.tvContent.setText(R.string.router_set_label_content);
                break;

            case REPEATER:
            default:
                mBinding.tvSubTitle.setText(R.string.repeater_set_label_subtitle);
                mBinding.tvContent.setText(R.string.repeater_set_label_content);
                break;
        }

        if (mActivity instanceof BaseDeviceSetupActivity) {
            ((BaseDeviceSetupActivity) mActivity).setDeviceImage(mViewModel.getSetupDevice(), mBinding.imgDevice);
        }
        // 預設不用顯示 device name
        mBinding.tvDeviceName.setVisibility(View.INVISIBLE);
        if (mViewModel.getSetupFlow() == AppConstants.SetupFlow.BLE &&
                mViewModel.getSetupMode() == AppConstants.SetupMode.REPEATER) {
            // 顯示 device name 的目的是幫助使用者知道自己連線到哪一台裝置
            // (1) 若走 WIFI setup 流程, 因為是使用者自己手動去連線要設定的裝置, 使用者本來就知道 device name 了 => 不用顯示
            // (2) 若是 BLE + router, 不論掃描到一台或多台 router, 都會有畫面幫助使用者確認是連線到哪一台裝置 => 不用顯示
            // (3) 若是 BLE + repeater, 因為若只掃描到一台 repeater 時會直接進入現在這頁, 使用者無法確認是連線到哪一台裝置 => 要顯示
            mBinding.tvDeviceName.setText(mViewModel.getSetupDevice().getName());
            mBinding.tvDeviceName.setVisibility(View.VISIBLE);
        }
        mBinding.btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final OptionListDialog optionListDialog = new OptionListDialog();
                String[] options = mActivity.getResources().getStringArray(R.array.location_label);
                final ArrayList<String> optionList = new ArrayList<>(Arrays.asList(options));
                optionListDialog.setOptionString(optionList);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i == 0) {
                            // Custom - Specify your own
                            final InputDialog inputDialog = new InputDialog();
                            inputDialog.setTitle(mActivity.getString(R.string.dialog_title_input_label));
                            inputDialog.setInputHint(mActivity.getString(R.string.dialog_hint_input_label));
                            inputDialog.setPositiveListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    setLabel(inputDialog.getInput());
                                    inputDialog.dismiss();
                                }
                            });
                            inputDialog.setNegativeListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    inputDialog.dismiss();
                                }
                            });
                            showDialog(inputDialog);
                        } else {
                            setLabel(optionList.get(i));
                        }
                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (DataManager.getInstance().getSetupMode()) {
                    case ROUTER:
                    case ROUTER_RESETUP:
                        mEventListener.onContinue(Code.Action.DeviceSetup.SETUP_WIFI);
                        break;

                    case REPEATER:
                    default:
                        mEventListener.onContinue(Code.Action.DeviceSetup.CONNECT_DEVICE);
                        break;
                }
            }
        });
        // 一進入頁面時沒有選擇任何 label, 所以 continue 的按鈕不能作用 (按鈕會是灰色的)
        mBinding.btnContinue.setEnabled(false);
        mEventListener.setSupportView(mBinding.tvSupport);
    }

    private void setLabel(String label) {
        mBinding.tvLabel.setText(label);
        mBinding.tvLabel.setTextColor(CommonUtils.getColor(mActivity, R.color.purple_4e1393));
        mBinding.btnContinue.setEnabled(true);
        mViewModel.setLocationLabel(label);
    }
}
