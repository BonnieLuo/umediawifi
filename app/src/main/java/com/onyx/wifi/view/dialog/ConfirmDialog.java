package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewConfirmDialogBinding;

public class ConfirmDialog extends BaseDialog {

    private ViewConfirmDialogBinding mBinding;

    private String mContent;
    private String mPositiveText;
    private String mNegativeText;
    private View.OnClickListener mPositiveListener;
    private View.OnClickListener mNegativeListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_confirm_dialog, null, false);
        mBinding.tvTitle.setText(mTitle);
        mBinding.tvContent.setText(mContent);
        mBinding.btnPositive.setText(mPositiveText);
        mBinding.btnPositive.setOnClickListener(mPositiveListener);
        mBinding.btnNegative.setText(mNegativeText);
        mBinding.btnNegative.setOnClickListener(mNegativeListener);
        builder.setView(mBinding.getRoot());
        return builder.create();
    }

    @Override
    public String toString() {
        return "ConfirmDialog{ " +
                "title = \"" + mTitle + "\"" +
                ", content = \"" + mContent + "\"" +
                " }";
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setPositiveButton(String text, View.OnClickListener listener) {
        mPositiveText = text;
        mPositiveListener = listener;
    }

    public void setNegativeButton(String text, View.OnClickListener listener) {
        mNegativeText = text;
        mNegativeListener = listener;
    }
}
