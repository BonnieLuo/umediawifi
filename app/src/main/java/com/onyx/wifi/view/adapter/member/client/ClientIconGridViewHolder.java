package com.onyx.wifi.view.adapter.member.client;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.view.interfaces.member.OnClientTypeClickListener;

public class ClientIconGridViewHolder extends RecyclerView.ViewHolder {

    private RecyclerView mRecyclerView;

    private ClientIconGridAdapter mAdapter;

    public ClientIconGridViewHolder(@NonNull View itemView) {
        super(itemView);

        mRecyclerView = this.itemView.findViewById(R.id.recyclerView);

        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(false);

            Context context = this.itemView.getContext();
            mRecyclerView.setLayoutManager(new GridLayoutManager(context, 4));

            int spacingInPixels = context.getResources().getDimensionPixelSize(R.dimen.recyclerview_member_device_spacing);
            mRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        }
    }

    public void setData(String[] clientTypeList, String selectedClientType, OnClientTypeClickListener onClientTypeClickListener) {
        mAdapter = new ClientIconGridAdapter(clientTypeList, selectedClientType, onClientTypeClickListener);

        mRecyclerView.setAdapter(mAdapter);
    }

    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space * 2;
            outRect.top = space * 2;
        }
    }

}
