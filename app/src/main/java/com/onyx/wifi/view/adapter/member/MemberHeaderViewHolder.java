package com.onyx.wifi.view.adapter.member;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.view.customized.CustomEditText;
import com.onyx.wifi.view.interfaces.member.OnMemberChangedListener;

public class MemberHeaderViewHolder extends RecyclerView.ViewHolder {

    private FamilyMember mMember;

    private ImageView mPhotoImageView;

    private ImageButton mButton;

    private CustomEditText mNameEditText;

    private OnMemberChangedListener mOnMemberChangedListener;

    public MemberHeaderViewHolder(@NonNull View itemView) {
        super(itemView);

        mPhotoImageView = this.itemView.findViewById(R.id.photoImageView);

        mButton = this.itemView.findViewById(R.id.button);

        mNameEditText = this.itemView.findViewById(R.id.editText);
        mNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String name = mNameEditText.getText().toString().trim();

                mMember.setTemporaryName(name);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String oldName = mMember.getName();
                String newName = mMember.getTemporaryName();

                if (!oldName.equalsIgnoreCase(newName)) {
                    if (mOnMemberChangedListener != null) {
                        mOnMemberChangedListener.onMemberChanged();
                    }

                    return;
                }

                if (mOnMemberChangedListener != null) {
                    mOnMemberChangedListener.onMemberRollback();
                }
            }
        });
    }

    public void setFamilyMember(FamilyMember member) {
        mMember = member;

        Context context = this.itemView.getContext();
        Bitmap photoBitmap = mMember.getPhoto(context);
        if (photoBitmap != null) {
            mPhotoImageView.setImageBitmap(photoBitmap);
        }

        Bitmap temporaryPhotoBitmap = mMember.getTemporaryPhoto();
        if (temporaryPhotoBitmap != null) {
            mPhotoImageView.setImageBitmap(temporaryPhotoBitmap);
        }

        String name = mMember.getName();

        if (name != null && name != "") {
            mNameEditText.setText(name);
        }

        String temporaryName = mMember.getTemporaryName();

        if (temporaryName != null) {
            mNameEditText.setText(temporaryName);
        }
    }

    public void setOnButtonClickListener(View.OnClickListener listener) {
        mButton.setOnClickListener(listener);
    }

    public void setOnMemberChangedListener(OnMemberChangedListener listener) {
        mOnMemberChangedListener = listener;
    }

    public void setIconCamera() {
        mButton.setImageResource(R.drawable.ic_sheet_camera);
    }

    public void setIconMoreAction() {
        mButton.setImageResource(R.drawable.ic_more);
    }

}
