package com.onyx.wifi.view.activity.menubar.pause;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityEditPauseProfileBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.advancewireless.dmz.DestinationClient;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.model.item.pause.PauseProfile;
import com.onyx.wifi.model.item.pause.PauseProfileModel;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.activity.ClientListActivity;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.pause.EditPauseProfileViewModel;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EditPauseProfileActivity extends BaseMenuActivity {

    private ActivityEditPauseProfileBinding mBinding;

    private EditPauseProfileViewModel mViewModel;

    private PauseProfile mPauseProfile;

    private List<ClientModel> mAllClientModelList;

    private boolean mIsCreateNewProfile = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) { // 如果只是update,bundle是null
                mIsCreateNewProfile = bundle.getBoolean("is_create_new_profile");
            }
        }

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_pause_profile);
        setViewModel();
        DataManager dataManager = DataManager.getInstance();
        dataManager.setSelectedClientList(null);
        mAllClientModelList = dataManager.getAllClientModelList();
        if (mAllClientModelList == null) {
            mViewModel.getAllClients();
        }

        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            // two cases: (1) create (2) update
            // (1) create
            if (mIsCreateNewProfile) {
                DataManager dataManager = DataManager.getInstance();

                dataManager.setEditPauseProfile(null);
                dataManager.setSelectedClientList(null);

                PauseProfileModel model = new PauseProfileModel();
                mPauseProfile = new PauseProfile(model);

                setData();
                mIsCreateNewProfile = false;

            } else { // (2) update
                DataManager dataManager = DataManager.getInstance();

                mPauseProfile = dataManager.getEditPauseProfile();

                if (mPauseProfile == null) {
                    PauseProfileModel model = new PauseProfileModel();
                    mPauseProfile = new PauseProfile(model);
                }

                // "Select From Devices"用dataManager.setSelectedClientList()儲存
                // "Select From Members"用以下方式存:
                //  if (pauseProfile != null) {
                //      pauseProfile.setExcludeMemberIdList(mSelectedMemberList);
                //      pauseProfile.setExcludeCidList(null);
                //  }
                //
                //  dataManager.setEditPauseProfile(pauseProfile);
                // 所以selected client list的部分需要額外做以下的動作
                List<DestinationClient> clientList = dataManager.getSelectedClientList();
                if (clientList != null) {
                    List<String> selectedCidList = new ArrayList<>();

                    for (DestinationClient client:clientList) {
                        String cid = client.getCid();
                        selectedCidList.add(cid);
                    }

                    mPauseProfile.setExcludeCidList(selectedCidList);
                    mPauseProfile.setExcludeMemberIdList(null);
                }

                // 顯示
                setData();
            }
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.customEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mPauseProfile != null) {
                    String name = mBinding.customEditText.getText().toString();
                    mPauseProfile.setName(name.trim());

                    checkDiff();
                }
            }
        });

        mBinding.editDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();

                String[] globalPauseDurationArray = getResources().getStringArray(R.array.globalPauseDurationArray);
                final ArrayList<String> options = new ArrayList<String>(Arrays.asList(globalPauseDurationArray));

                optionListDialog.setOptionString(options);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        mPauseProfile.setDuration(position);

                        setData();

                        optionListDialog.dismiss();

                        checkDiff();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();

                final ArrayList<String> options = new ArrayList<String>();
                options.add("Select From Devices");
                options.add("Select From Members");

                optionListDialog.setOptionString(options);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                        DataManager dataManager = DataManager.getInstance();

                        dataManager.setEditPauseProfile(mPauseProfile);

                        if (position == 0) {

                            List<DestinationClient> selectedClientList = new ArrayList<>();

                            List<String> excludeCidList = mPauseProfile.getExcludeCidList();

                            if (excludeCidList != null && excludeCidList.size() > 0) {
                                for (ClientModel model:mAllClientModelList) {
                                    String type = model.getClientType();
                                    String name = model.getName();
                                    String cid = model.getCid();
                                    String ip = model.getIp();

                                    DestinationClient client = new DestinationClient(type, name, cid, ip);

                                    if (excludeCidList.contains(cid)) {
                                        selectedClientList.add(client);
                                    }
                                }
                            }

                            dataManager.setSelectedClientList(selectedClientList);

                            String title = getString(R.string.global_pause_profiles_title);

                            Intent intent = new Intent(mContext, ClientListActivity.class);
                            intent.putExtra("title", title);
                            intent.putExtra("multi_select_enabled", true);
                            startActivity(intent);

                        }

                        if (position == 1) {
                            List<String> excludeMemberIdList = mPauseProfile.getExcludeCidList();

                            Intent intent = new Intent(mContext, SelectFromMemberActivity.class);
                            startActivity(intent);
                        }

                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.eraseProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConfirmDialog confirmDialog = new ConfirmDialog();

                confirmDialog.setTitle("Erase Profile");
                confirmDialog.setContent("Are you sure you want to erase this global pause profile?");
                confirmDialog.setPositiveButton("Erase", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewModel.removeGlobalPauseProfile(mPauseProfile);
                    }
                });
                confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmDialog.dismiss();
                    }
                });

                showDialog(confirmDialog);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        DataManager dataManager = DataManager.getInstance();
        dataManager.setEditPauseProfile(null);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.global_pause_profiles_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataManager dataManager = DataManager.getInstance();
                dataManager.setEditPauseProfile(null);

                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mPauseProfile.getName();
                if (!InputUtils.isNameValid(name)) {
                    String title = getString(R.string.member_name_invalid_name);
                    String message = getString(R.string.member_name_rule);

                    showMessageDialog(title, message);

                    return;
                }

                if (mPauseProfile.getDuration() == 0) {
                    String title = "Invalid Duration";
                    String message = "Please select the pause duation for this profile.";

                    showMessageDialog(title, message);

                    return;
                }

                String profileId = mPauseProfile.getProfileId();

                if (profileId.isEmpty()) {
                    mViewModel.createGlobalPauseProfile(mPauseProfile);
                } else {
                    mViewModel.updateGlobalPauseProfile(mPauseProfile);
                }
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(EditPauseProfileViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.GET_ALL_CLIENTS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get All Clients", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            JsonObject dataJson = json.getAsJsonObject("data");

                            JsonArray clientListJSON = dataJson.getAsJsonArray("client_list");

                            if (clientListJSON != null) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<ClientModel>>() {
                                }.getType();
                                List<ClientModel> allClientModelList = gson.fromJson(clientListJSON.toString(), listType);

                                DataManager dataManager = DataManager.getInstance();
                                dataManager.setAllClientModelList(allClientModelList);

                                dataManager.setSelectedClientList(null);
                                mAllClientModelList = dataManager.getAllClientModelList();
                            }

                        }
                    }
                    if (resultStatus.actionCode == Code.Action.Pause.CREATE_GLOBAL_PAUSE_PROFILE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Create Global Pause", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Pause.UPDATE_GLOBAL_PAUSE_PROFILE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Global Pause", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Pause.REMOVE_GLOBAL_PAUSE_PROFILE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Global Pause", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                }
            }
        });
    }

    private void setData() {
        mBinding.customEditText.setText(mPauseProfile.getName());

        String[] globalPauseDurationPresentArray = getResources().getStringArray(R.array.globalPauseDurationPresentArray);

        String presentDuration;
        switch (mPauseProfile.getDuration()) {
            case 15:
                presentDuration = globalPauseDurationPresentArray[0];
                break;
            case 30:
                presentDuration = globalPauseDurationPresentArray[1];
                break;
            case 60:
                presentDuration = globalPauseDurationPresentArray[2];
                break;
            case 120:
                presentDuration = globalPauseDurationPresentArray[3];
                break;
            case 240:
                presentDuration = globalPauseDurationPresentArray[4];
                break;
            case 480:
                presentDuration = globalPauseDurationPresentArray[5];
                break;
            case 720:
                presentDuration = globalPauseDurationPresentArray[6];
                break;
            default:
                presentDuration = "--";
                break;
        }

        mBinding.durationTextView.setText(presentDuration);

        String profileId = mPauseProfile.getProfileId();

        if (mIsCreateNewProfile) {//if (profileId.isEmpty()) {
            mBinding.eraseProfileButton.setEnabled(false);
        } else {
            mBinding.eraseProfileButton.setEnabled(true);
        }

        mBinding.noneTextView.setVisibility(View.VISIBLE);
        mBinding.memberLayout.setVisibility(View.GONE);
        mBinding.clientLayout.setVisibility(View.GONE);

        List<String> excludeMemberIdList = mPauseProfile.getExcludeMemberIdList();

        if (excludeMemberIdList != null) {
            setExcludeMember();
        }

        List<String> excludeCidList = mPauseProfile.getExcludeCidList();

        if (excludeCidList != null) {
            setExcludeClient();
        }

        checkDiff();
    }

    private void setExcludeMember() {
        List<String> excludeMemberIdList = mPauseProfile.getExcludeMemberIdList();

        if (excludeMemberIdList.isEmpty()) {
            mBinding.noneTextView.setVisibility(View.VISIBLE);
            mBinding.memberLayout.setVisibility(View.GONE);
            mBinding.clientLayout.setVisibility(View.GONE);
            return;
        }

        mBinding.noneTextView.setVisibility(View.INVISIBLE);
        mBinding.memberLayout.setVisibility(View.VISIBLE);

        String packageName = getPackageName();
        Resources resources = getResources();

        for(int i = 0; i < 10; i++) {
            int imageViewId = resources.getIdentifier("member" + i + "ImageView", "id", packageName);
            ImageView imageView = findViewById(imageViewId);

            int layoutId = resources.getIdentifier("member" + i, "id", packageName);
            FrameLayout layout = findViewById(layoutId);
            layout.setVisibility(View.GONE);

            if (excludeMemberIdList.size() > i) {
                String memberId = excludeMemberIdList.get(i);

                Bitmap bitmap = getPhoto(memberId);

                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                }

                layout.setVisibility(View.VISIBLE);
            }

        }
    }

    private void setExcludeClient() {
        List<String> excludeCidList = mPauseProfile.getExcludeCidList();

        if (excludeCidList.isEmpty()) {
            mBinding.noneTextView.setVisibility(View.VISIBLE);
            mBinding.memberLayout.setVisibility(View.GONE);
            mBinding.clientLayout.setVisibility(View.GONE);
            return;
        }

        mBinding.noneTextView.setVisibility(View.INVISIBLE);
        mBinding.clientLayout.setVisibility(View.VISIBLE);

        String packageName = getPackageName();
        Resources resources = getResources();

        for(int i = 0; i < 10; i++) {
            int imageViewId = resources.getIdentifier("client" + i + "ImageView", "id", packageName);
            ImageView imageView = findViewById(imageViewId);
            imageView.setVisibility(View.INVISIBLE);

            if (excludeCidList.size() > i) {
                String cid = excludeCidList.get(i);

                if (mAllClientModelList != null) {
                    for (ClientModel model : mAllClientModelList) {
                        String modelCid = model.getCid();
                        if (cid.equalsIgnoreCase(modelCid)) {
                            Client client = new Client(model);
                            int iconId = client.getClientIcon(ConnectionStatus.ONLINE);
                            imageView.setImageResource(iconId);
                            imageView.setVisibility(View.VISIBLE);

                            break;
                        }
                    }
                }
            }

        }

    }

    private Bitmap getPhoto(String memberId) {
        Bitmap bitmap = null;

        String avatarFileName = memberId + ".jpg";
        File avatarFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), avatarFileName);

        if (avatarFile.exists()) {
            String filePath = avatarFile.getAbsolutePath();

            bitmap = decodeSampledBitmapFromResource(filePath, 200, 200);
        }

        return bitmap;
    }

    private Bitmap decodeSampledBitmapFromResource(String filePath, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return getOrientationBitmap(filePath, options);

    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private void checkDiff() {
        boolean isChanged = mPauseProfile.isChanged();

        if (isChanged) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    public int resolveBitmapOrientation(File bitmapFile) throws IOException {
        ExifInterface exif = null;
        exif = new ExifInterface(bitmapFile.getAbsolutePath());

        return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
    }

    public Bitmap applyOrientation(Bitmap bitmap, int orientation) {
        int rotate = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            default:
                return bitmap;
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        mtx.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public Bitmap getOrientationBitmap(String filePath,BitmapFactory.Options options) {
        Bitmap srcBmp01=BitmapFactory.decodeFile(filePath,options);
        try {
            int orientation = resolveBitmapOrientation(new File(filePath));
            srcBmp01 = applyOrientation(srcBmp01, orientation);
        } catch (Exception ex) {
            ;
        }

        return srcBmp01;
    }

    public Bitmap getOrientationBitmap(String filePath) {
        Bitmap srcBmp01=BitmapFactory.decodeFile(filePath);

        if(srcBmp01!=null) {
            try {
                int orientation = resolveBitmapOrientation(new File(filePath));
                srcBmp01 = applyOrientation(srcBmp01, orientation);
            } catch (Exception ex) {
                ;
            }
        }

        return srcBmp01;
    }

}
