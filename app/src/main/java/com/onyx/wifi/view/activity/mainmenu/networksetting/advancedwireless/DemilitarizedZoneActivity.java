package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessDemilitarizedZoneBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfig;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.DmzInputDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.DmzViewModel;

import java.util.ArrayList;

public class DemilitarizedZoneActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessDemilitarizedZoneBinding mBinding;

    private DmzViewModel mViewModel;

    private DemilitarizedZoneConfig mDmzConfig;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setSelectedClientList(new ArrayList<>());

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_demilitarized_zone);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            DataManager dataManager = DataManager.getInstance();
            mDmzConfig = dataManager.getDmzConfig();

            setData();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.enableImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager dataManager = DataManager.getInstance();
                DemilitarizedZoneConfig dmzConfig = dataManager.getDmzConfig();
                dmzConfig.toggleEnable();

                setData();

                checkDiff();
            }
        });

        mBinding.clientEditImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ipAddress = mDmzConfig.getIpAddress();

                DmzInputDialog inputDialog = new DmzInputDialog();
                inputDialog.setTitle("DMZ");
                inputDialog.setDescription("Enter Destination IP Address.");

                if (ipAddress == null)
                    inputDialog.setIp("192.168.8.");
                else
                    inputDialog.setIp(ipAddress);

                // Cancel
                inputDialog.setNegativeListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inputDialog.dismiss();
                    }
                });
                // Apply
                inputDialog.setPositiveListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!InputUtils.isIpValid(inputDialog.getInput())) {
                            CommonUtils.toast(mContext, "The IP you entered does not work.");
                        } else {
                            String newIpAddress = inputDialog.getInput();
                            mDmzConfig.setIpAddress(newIpAddress);
                            setData();
                            inputDialog.dismiss();
                        }
                    }
                });
                showDialog(inputDialog);
            }
        });

    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("DMZ");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSettings();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(DmzViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.SET_DMZ) {
                        if (!resultStatus.success) {
                            showMessageDialog("Setup DMZ", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }
                }
            }
        });
    }

    private void setData() {
        if (mDmzConfig.isEnabled()) {
            mBinding.enableTextView.setText("Enabled");
            mBinding.enableImageView.setSelected(true);

            //mBinding.configLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.enableTextView.setText("Disabled");
            mBinding.enableImageView.setSelected(false);

            //mBinding.configLayout.setVisibility(View.GONE);
        }

        setIpAddress();
    }

    private void setIpAddress() {
        String ipAddress = mDmzConfig.getIpAddress();

        if (ipAddress == null) {
            mBinding.ipAddressTextView.setText("192.168.8._");

            return;
        }

        mBinding.ipAddressTextView.setText(ipAddress);

        checkDiff();
    }

    private void checkDiff() {
        DataManager dataManager = DataManager.getInstance();
        DemilitarizedZoneConfig dmzConfig = dataManager.getDmzConfig();
        boolean isChanged = dmzConfig.isChanged();

        if (isChanged) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void updateSettings() {
        DataManager dataManager = DataManager.getInstance();
        DemilitarizedZoneConfig dmzConfig = dataManager.getDmzConfig();
        mViewModel.setupDmz(dmzConfig);
    }

}
