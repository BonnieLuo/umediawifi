package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityGeneralQuestionBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.ListDisplayType;

public class GeneralQuestionActivity extends BaseMoreHelpActivity {

    private ActivityGeneralQuestionBinding mBinding;

    private ListDisplayType mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ3DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_general_question);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        //
        // set partial color of some TextView content
        //
        //enableSupportLink(mBinding.tv2Solution, mBinding.tv2Solution.getText().toString(), R.color.purple_7110b2);
        //enableSupportLink(mBinding.tv3Solution, mBinding.tv3Solution.getText().toString(), R.color.purple_7110b2);

        //
        // onclick handler
        //
        mBinding.ctv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ3DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ3DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mQ3DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ3DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        setBottomLayout(mBinding.btnReturnToSetup, mBinding.tvTrouble, mBinding.tvSupport, mBinding.menuBar, mBinding.clContent);
    }

    private void showByDisplayType() {
        // 1
        if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("+");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl1Solution.setVisibility(View.GONE);
        } else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("-");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl1Solution.setVisibility(View.VISIBLE);
        }

        // 2
        if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("+");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl2Solution.setVisibility(View.GONE);
        } else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("-");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl2Solution.setVisibility(View.VISIBLE);
        }

        // 3
        if (mQ3DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv3.setText("+");
            mBinding.tv3Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl3Solution.setVisibility(View.GONE);
        } else if (mQ3DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv3.setText("-");
            mBinding.tv3Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl3Solution.setVisibility(View.VISIBLE);
        }
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.more_help_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
