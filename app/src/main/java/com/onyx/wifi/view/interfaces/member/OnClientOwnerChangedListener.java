package com.onyx.wifi.view.interfaces.member;

public interface OnClientOwnerChangedListener {

    void onClientOwnerChanged(boolean isOwnerChanged);

}
