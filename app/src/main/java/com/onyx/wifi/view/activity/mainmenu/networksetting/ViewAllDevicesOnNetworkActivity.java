package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityViewAllDevicesOnNetworkBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientInfoList;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.menubar.member.client.ClientSettingsActivity;
import com.onyx.wifi.view.activity.menubar.member.list.MemberListActivity;
import com.onyx.wifi.view.adapter.ClientAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.ListDisplayType;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ViewAllDevicesOnNetworkActivity extends BaseMenuActivity implements ClientAdapter.OnItemClickHandler {
    public static final int mRecentListDefaultItemNum = 4;
    public static final int mLongInactiveListDefaultItemNum = 2;
    private SwipeRefreshLayout laySwipe;
    private ActivityViewAllDevicesOnNetworkBinding mBinding;
    private NetworkSettingViewModel mViewModel;
    private ClientAdapter mRecentClientAdapter;
    private ClientAdapter mLongInactiveClientAdapter;
    private ListDisplayType mRecentListDisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mLongInactiveListDisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private Timer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_all_devices_on_network);
        setViewModel();
        mViewModel.getClientInfoList();
        initClientList();
        setView();
    }

    private void setUpRecentClientRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding.rvRecentClient.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding.rvRecentClient.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        if (mRecentClientAdapter == null) {
            mRecentClientAdapter = new ClientAdapter(mContext, mViewModel.getmRecentClient(), this, false);
        }
        mBinding.rvRecentClient.setAdapter(mRecentClientAdapter);
    }

    private void setUpLongInactiveClientRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding.rvLongInactiveClient.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding.rvLongInactiveClient.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        if (mLongInactiveClientAdapter == null) {
            mLongInactiveClientAdapter = new ClientAdapter(mContext, mViewModel.getmLongInactiveClient(), this, false);
        }
        mBinding.rvLongInactiveClient.setAdapter(mLongInactiveClientAdapter);
    }

    void startTimer() {
        if (mTimer == null) {
            ViewAllDevicesOnNetworkActivity.GetListTimerTask getListTimerTask = new ViewAllDevicesOnNetworkActivity.GetListTimerTask();
            mTimer = new Timer();
            mTimer.schedule(getListTimerTask, 10L, 5000L);
        }
    }

    void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void initClientList() {
        startTimer();
        updateClientList();//之前放在startTimer()之前
    }

    @Override
    protected void onResume() {
        super.onResume();
        initClientList();
    }


    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
    }


    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.NetworkSetting.GET_CLIENT_INFO_LIST) {
                        if (resultStatus.success) {
                            //LogUtils.trace("NotifyMsg", "Get List success~~~~~~~~~~~~~~");
                            //(2)laySwipe.setRefreshing(false);
                            updateClientList();
                        } else {
                            showMessageDialog("Get Client List", resultStatus.errorMsg);
                        }
                    }
                    if (resultStatus.actionCode == Code.Action.Member.REMOVE_CLIENT) {
                        if (resultStatus.success) {
                            mViewModel.getClientInfoList();
                        } else {
                            showMessageDialog("Get Client List", resultStatus.errorMsg);
                        }
                    }

                }
            }
        });
    }

    private void updateClientList() {
        ArrayList<ClientModel> data = new ArrayList<ClientModel>();
        //////////////////////////////////////////////////
        if (mRecentClientAdapter != null) {
            if (mViewModel.getmRecentClient().size() <= mRecentListDefaultItemNum) { // item # <= 4, 隱藏More
                //LogUtils.trace("LongInactive3", "[RecentClient item # <= 4]" + data.toString());
                data.addAll(mViewModel.getmRecentClient());
            } else { // item # > 4
                if (mRecentListDisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    //LogUtils.trace("LongInactive3", "[RecentClient SHOW_DISPLAY_EXTENSION_BUTTON]" + data.toString());
                    mBinding.tvMoreRecent.setText("MORE");
                    mBinding.btnDisplayAllRecentClient.setImageResource(R.drawable.ic_member_more);
                    for (int i = 0; i < mRecentListDefaultItemNum; ++i) {
                        data.add(mViewModel.getmRecentClient().get(i));
                    }
                } else if (mRecentListDisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    //LogUtils.trace("LongInactive3", "[RecentClient SHOW_HIDE_EXTENSION_BUTTON]" + data.toString());
                    mBinding.tvMoreRecent.setText("HIDE");
                    mBinding.btnDisplayAllRecentClient.setImageResource(R.drawable.ic_member_hide);
                    data.addAll(mViewModel.getmRecentClient());
                }
            }
            mRecentClientAdapter.setDevice(data); //在此處直接用mViewModel.getFWStatusList()，在setDevice裡會變成null
            //LogUtils.trace("LongInactive3", "[RecentClient]" + data.toString());

            mBinding.rvRecentClient.setAdapter(mRecentClientAdapter);
        }

        data.clear();
        if (mLongInactiveClientAdapter != null) {
            if (mViewModel.getmLongInactiveClient().size() <= mLongInactiveListDefaultItemNum) { // item # <= 2, 隱藏More
                data.addAll(mViewModel.getmLongInactiveClient());
            } else { // item # > 2
                if (mLongInactiveListDisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mBinding.tvMoreLongInactive.setText("MORE");
                    mBinding.btnDisplayAllLongInactiveClient.setImageResource(R.drawable.ic_member_more);

                    for (int i = 0; i < mLongInactiveListDefaultItemNum; ++i) {
                        data.add(mViewModel.getmLongInactiveClient().get(i));
                    }
                } else if (mLongInactiveListDisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mBinding.tvMoreLongInactive.setText("HIDE");
                    mBinding.btnDisplayAllLongInactiveClient.setImageResource(R.drawable.ic_member_hide);
                    data.addAll(mViewModel.getmLongInactiveClient());
                }
            }
            mLongInactiveClientAdapter.setDevice(data); //在此處直接用mViewModel.getFWStatusList()，在setDevice裡會變成null
            //LogUtils.trace("LongInactive3", "[LongInactiveClient]" + data.toString());

            mBinding.rvLongInactiveClient.setAdapter(mLongInactiveClientAdapter);
        }

        // show/hide "No Device!"/Long inactive list
        if (mViewModel.getmRecentClient().size() == 0) {
            if (mViewModel.getmLongInactiveClient().size() == 0) {// Recent LongInactive: X  X
                showOrHideGroup(true, false, false, false);
            } else {                                              // Recent LongInactive: X  O
                if (mViewModel.getmLongInactiveClient().size() <= mLongInactiveListDefaultItemNum) { // item # <= 2, 隱藏More
                    showOrHideGroup(true, true, false, false);
                } else {
                    showOrHideGroup(true, true, false, true);

                }
            }
        } else {
            if (mViewModel.getmLongInactiveClient().size() == 0) {// Recent LongInactive: O  X
                if (mViewModel.getmRecentClient().size() <= mRecentListDefaultItemNum) { // item # <= 4, 隱藏More
                    showOrHideGroup(false, false, false, false);
                } else {
                    showOrHideGroup(false, false, true, false);
                }
            } else {                                              // Recent LongInactive: O  O

                if (mViewModel.getmRecentClient().size() <= mRecentListDefaultItemNum) { // item # <= 4, 隱藏More
                    if (mViewModel.getmLongInactiveClient().size() <= mLongInactiveListDefaultItemNum) { // item # <= 2, 隱藏More
                        showOrHideGroup(false, true, false, false);
                    } else {
                        showOrHideGroup(false, true, false, true);
                    }

                } else {

                    if (mViewModel.getmLongInactiveClient().size() <= mLongInactiveListDefaultItemNum) { // item # <= 2, 隱藏More
                        showOrHideGroup(false, true, true, false);
                    } else {
                        showOrHideGroup(false, true, true, true);
                    }
                }
            }
        }
        //////////////////////////////////////////////////
        /*if (mRecentClientAdapter != null) {
            data.addAll(mViewModel.getmRecentClient());
            mRecentClientAdapter.setDevice(data); //在此處直接用mViewModel.getFWStatusList()，在setDevice裡會變成null
            LogUtils.trace("LongInactive3", "[RecentClient]" + data.toString());

            mBinding.rvRecentClient.setAdapter(mRecentClientAdapter);
        }

        data.clear();

        if (mLongInactiveClientAdapter != null) {
            data.addAll(mViewModel.getmLongInactiveClient());
            mLongInactiveClientAdapter.setDevice(data); //在此處直接用mViewModel.getFWStatusList()，在setDevice裡會變成null
            LogUtils.trace("LongInactive3", "[LongInAcive]" + data.toString());
            mBinding.rvLongInactiveClient.setAdapter(mLongInactiveClientAdapter);
        }*/

    }

    private void showOrHideGroup(boolean isShowNoDevice, boolean isShowLongInactiveList, boolean isShowMoreRecent, boolean isShowMoreLongInactive) {
        if (isShowNoDevice) {
            mBinding.groupNoDevice.setVisibility(View.VISIBLE);
        } else {
            mBinding.groupNoDevice.setVisibility(View.INVISIBLE);
        }

        if (isShowLongInactiveList) {
            mBinding.groupLongInactive.setVisibility(View.VISIBLE);
        } else {
            mBinding.groupLongInactive.setVisibility(View.INVISIBLE);
        }

        if (isShowMoreRecent) {
            mBinding.groupMoreRecent.setVisibility(View.VISIBLE);
        } else {
            mBinding.groupMoreRecent.setVisibility(View.INVISIBLE);
        }

        if (isShowMoreLongInactive) {
            mBinding.groupMoreLongInactive.setVisibility(View.VISIBLE);
        } else {
            mBinding.groupMoreLongInactive.setVisibility(View.INVISIBLE);
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        // swipe mechanism
        /*(1)laySwipe = mBinding.laySwipe;

        laySwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                mViewModel.getClientInfoList();
            }
        });

        laySwipe.setColorSchemeResources(
                android.R.color.holo_red_light,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light);*/

        //
        // List
        //
        setUpRecentClientRecycler();
        setUpLongInactiveClientRecycler();

        //
        // Display all button
        //
        mBinding.btnDisplayAllRecentClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRecentListDisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mRecentListDisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mRecentListDisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mRecentListDisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                updateClientList(); // 先update再打cloud api重新update，才不會有視覺上的delay
                mViewModel.getClientInfoList();
            }
        });

        mBinding.btnDisplayAllLongInactiveClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LogUtils.trace("LongInactive3", "click btnDisplayAllLongInactiveClient");
                if (mLongInactiveListDisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mLongInactiveListDisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mLongInactiveListDisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mLongInactiveListDisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                updateClientList(); // 先update再打cloud api重新update，才不會有視覺上的delay
                mViewModel.getClientInfoList();
            }
        });

        mBinding.btnRemoveLongInactiveDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.removeClients(mViewModel.getmLongInactiveClient());
            }
        });


    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.view_all_devices_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemClick(ClientModel clientModel) {
        Client client = new Client(clientModel);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setManageClient(client);

        Intent intent = new Intent(ViewAllDevicesOnNetworkActivity.this, ClientSettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onViewClick(int pos) {

    }

    @Override
    public void onClientSelected() {

    }

    private class GetListTimerTask extends TimerTask {

        @Override
        public void run() {

            mViewModel.getClientInfoList();
        }
    }

}
