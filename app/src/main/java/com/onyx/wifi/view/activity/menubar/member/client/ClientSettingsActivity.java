package com.onyx.wifi.view.activity.menubar.member.client;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMemberClientSettingsBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.InfoDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.client.ClientSettingViewModel;

import java.util.ArrayList;
import java.util.List;

public class ClientSettingsActivity extends BaseMenuActivity {

    private ActivityMemberClientSettingsBinding mBinding;

    private ClientSettingViewModel mViewModel;

    private Client mClient;

    private Handler mHandler = new Handler();

    class PauseTask implements Runnable {
        private Client mClient;

        public void setClient(Client client) {
            mClient = client;
        }

        @Override
        public void run() {
            if (mViewModel == null) {
                return;
            }

            if (mClient == null) {
                return;
            }

            String cid = mClient.getCid();
            int duration = mClient.getPauseDuration();

            mViewModel.pauseClient(cid, duration);
        }
    }

    private PauseTask pauseTask = new PauseTask();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_client_settings);

        setView();

        setViewModel();

        DataManager dataManager = DataManager.getInstance();
        MemberList memberList = dataManager.getMemberList();

        if (memberList == null)
            mViewModel.getMemberList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_client_settings);

            setView();

            setData();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        DataManager dataManager = DataManager.getInstance();
        dataManager.setManageClient(null);
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.manageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showManageDialog();
            }
        });

        mBinding.nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newName = mBinding.nameEditText.getText().toString().trim();

                if (mClient != null)
                    mClient.setTemporaryName(newName);

                checkDiff();
            }
        });

        mBinding.outageMonitoringInfoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoDialog infoDialog = new InfoDialog();
                String title = mContext.getString(R.string.device_setting_outage_info_title);
                infoDialog.setTitle(title);

                String content = mContext.getString(R.string.device_setting_outage_info_content);
                infoDialog.setContent(content);

                BaseActivity baseActivity = (BaseActivity) mContext;
                baseActivity.showDialog(infoDialog);
            }
        });

        mBinding.outageMonitoringButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isDeviceOutageMonitoring = !mClient.isTemporaryDeviceOutageMonitoring();
                mClient.setTemporaryDeviceOutageMonitoring(isDeviceOutageMonitoring);

                setOutageMonitoring();

                checkDiff();
            }
        });

        mBinding.priorityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isHighPriority = !mClient.isTemporaryHighPriority();
                mClient.setTemporaryInternetPriority(isHighPriority);

                setPriority();

                checkDiff();
            }
        });

        mBinding.arrowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ClientOwnerActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.device_setting_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataManager dataManager = DataManager.getInstance();
                dataManager.setManageClient(null);

                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectionStatus connectionStatus = mClient.getConnectionStatus();
                if (connectionStatus == ConnectionStatus.OFFLINE) {
                    String title = "Error";
                    String message = "The device is offline.";

                    showMessageDialog(title, message);

                    return;
                }

                String name = mClient.getTemporaryName();//mClient.getName();
                if ((!InputUtils.isNameValid(name)) || (InputUtils.containsWhitespace(name))){
                    String title = getString(R.string.member_name_invalid_name);
                    String message = getString(R.string.member_name_rule);

                    showMessageDialog(title, message);

                    return;
                }
                mClient.setName(name);
                mClient.setDeviceOutageMonitoring(mClient.getTemporaryDeviceOutageMonitoring());
                mClient.setInternelPriority(mClient.getTemporaryInternetPriority());
                mClient.setClientType(mClient.getTemporaryClientType());
                mViewModel.updateDeviceSetting(mClient.getClientModel());
                mBinding.loading.setVisibility(View.VISIBLE);
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ClientSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Member.UPDATE_CLIENT_SETTINGS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Update Client Settings", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setManageClient(null);
                        mBinding.loading.setVisibility(View.INVISIBLE);
                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.BLOCK_CLIENT) {
                        if (!resultStatus.success) {
                            showMessageDialog("Block Client", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();// ?
                        ///////////////////////////////////
                        // 延遲關loading畫面
                        Handler mLoadingDelayHandler = new Handler(Looper.getMainLooper());
                        Runnable mLinkingTimeoutRunnable = new Runnable() {
                            @Override
                            public void run() {
                                //mViewModel.closeLoadingView();
                                mBinding.loadingBlockClient.setVisibility(View.GONE);
                                finish();
                            }
                        };
                        mLoadingDelayHandler.postDelayed(mLinkingTimeoutRunnable, 13000);

                    }

                    if (resultStatus.actionCode == Code.Action.Member.PAUSE_CLIENT) {
                        if (!resultStatus.success) {
                            showMessageDialog("Pause Client", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.RESUME_CLIENT) {
                        if (!resultStatus.success) {
                            showMessageDialog("Resume Client", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Member List", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList(); // TODO: why?
                    }

                }
            }
        });
    }

    private void setData() {
        DataManager dataManager = DataManager.getInstance();
        mClient = dataManager.getManageClient();

        setPicture();

        setName();

        setConnectionStatus();

        setConnectionType();

        setIp();

        setOutageMonitoring();

        setPriority();

        setOwnerLayout();

        setOwnerPhoto();

        checkDiff();
    }

    private void setPicture() {
        int pictureId = mClient.getClientPicture();
        mBinding.iconImageView.setImageResource(pictureId);

        String temporaryClientType = mClient.getTemporaryClientType();

        if (temporaryClientType != null) {
            int tempClientPictureId = mClient.getTemporaryClientPicture();
            mBinding.iconImageView.setImageResource(tempClientPictureId);
        }
    }

    private void setName() {
        String memberName = mClient.getName();
        mBinding.nameEditText.setText(memberName);
    }

    double normalize(double value, double min, double max) {
        return ((value - min) / (max - min));
    }

    private void setConnectionStatus() {
        int lightId = mClient.getConnectionLight();
        mBinding.lightImageView.setImageResource(lightId);

        List<PauseInfo> pauseInfoList = mClient.getPauseInfoList();

        if (!pauseInfoList.isEmpty()) {
            mBinding.statusTextView.setText("Paused");

            return;
        }

        String message = mClient.getConnectionMessage(this);
        mBinding.statusTextView.setText(message);
    }

    private void setConnectionType() {
        String connectionType = mClient.getConnectionType();
        mBinding.connectionTypeTextView.setText(connectionType);
    }

    private void setIp() {
        String ip = mClient.getIp();
        mBinding.ipTextView.setText(ip);
    }

    private void setOutageMonitoring() {
        boolean isOutageMonitoring = mClient.isTemporaryDeviceOutageMonitoring();

        if (isOutageMonitoring) {
            mBinding.outageMonitoringTextView.setText(R.string.device_setting_enable);

            mBinding.outageMonitoringButton.setSelected(true);
        } else {
            mBinding.outageMonitoringTextView.setText(R.string.device_setting_disable);

            mBinding.outageMonitoringButton.setSelected(false);
        }
    }

    private void setPriority() {
//        mBinding.priorityTextView.setText(R.string.device_setting_normal);
//
        mBinding.priorityInfoImageButton.setEnabled(false);

        boolean isHighPriority = mClient.isTemporaryHighPriority();

        if (isHighPriority) {
            mBinding.priorityTextView.setText("High");

            mBinding.priorityButton.setSelected(true);
        } else {
            mBinding.priorityTextView.setText(R.string.device_setting_normal);

            mBinding.priorityButton.setSelected(false);
        }
    }

    private void setOwnerLayout() {
        String ownerId = mClient.getTemporaryOwnerId();

        if (ownerId == null) {
            mBinding.ownerLayout.setVisibility(View.GONE);

            return;
        }

        mBinding.ownerLayout.setVisibility(View.VISIBLE);
    }

    private void setOwnerPhoto() {
        String ownerId = mClient.getOwnerId();

        Bitmap ownerPhoto = mClient.getOwnerPhoto(this, ownerId);

        if (ownerPhoto != null) {
            mBinding.ownerImageView.setImageBitmap(ownerPhoto);
        } else {
            mBinding.ownerImageView.setImageResource(R.drawable.ic_member_default);
        }
    }

    private void checkDiff() {
        boolean isClientChanged = isClientChanged();
        if (isClientChanged) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private boolean isClientChanged() {
        boolean isTypeChanged = !mClient.getTemporaryClientType().equalsIgnoreCase(mClient.getClientType());
        boolean isNameChanged = !mClient.getTemporaryName().equalsIgnoreCase(mClient.getName());
        boolean isDeviceOutageMonitoringChanged = mClient.isTemporaryDeviceOutageMonitoring() != mClient.isDeviceOutageMonitoring();
        boolean isHighPriority = mClient.isTemporaryHighPriority() != mClient.isHighPriority();
        boolean isOwnerChanged = mClient.isOwnerChanged();

        return isTypeChanged ||
                isNameChanged ||
                isDeviceOutageMonitoringChanged ||
                isHighPriority ||
                isOwnerChanged;
    }

    private void showManageDialog() {
        OptionListDialog optionListDialog = new OptionListDialog();

        final ArrayList<String> options = new ArrayList<>();
        options.add(getString(R.string.device_setting_change_icon));
        options.add(getString(R.string.device_setting_block_device));

        ArrayList<Integer> icons = new ArrayList<>();
        icons.add(R.drawable.ic_sheet_change);
        icons.add(R.drawable.ic_sheet_device);

        optionListDialog.setOptionString(options);
        optionListDialog.setOptionIconIds(icons);
        optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(ClientSettingsActivity.this, ClientTypeListActivity.class);
                    startActivity(intent);
                }

                if (position == 1) {
                    showRemoveDialog(optionListDialog);
                }

                optionListDialog.dismiss();
            }
        });
        showDialog(optionListDialog);
    }

    private void showRemoveDialog(OptionListDialog optionListDialo) {
        final ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.setTitle(getString(R.string.device_setting_block_device));
        confirmDialog.setContent(getString(R.string.device_setting_block_device_confirm));
        confirmDialog.setPositiveButton(getString(R.string.device_setting_btn_block), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.blockClient(mClient.getClientModel());
                mBinding.loadingBlockClient.setVisibility(View.VISIBLE);
                confirmDialog.dismiss();
            }
        });
        confirmDialog.setNegativeButton(getString(R.string.device_setting_btn_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
            }
        });
        showDialog(confirmDialog);
    }
}
