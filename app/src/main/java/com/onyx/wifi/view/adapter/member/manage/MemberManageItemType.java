package com.onyx.wifi.view.adapter.member.manage;

public enum MemberManageItemType {
    HEADER,
    SECTION,
    EMPTY,
    ITEM,
    CONTENT_FILTERS,
    INTERNET_SCHEDULE
}
