package com.onyx.wifi.view.fragment.setup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentMultipleDeviceFoundBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.view.adapter.BleDeviceAdapter;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;
import com.onyx.wifi.viewmodel.setup.BleScanViewModel;

import java.util.ArrayList;

public class MultipleDeviceFoundFragment extends BaseFragment {

    private FragmentMultipleDeviceFoundBinding mBinding;
    private BleScanViewModel mViewModel;

    private DeviceSetupListener mEventListener;

    private BleDeviceAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_multiple_device_found, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewModel();
        setView();
    }

    private void setView() {
        final ArrayList<SetupDevice> devicesList = mViewModel.getDeviceList();
        mAdapter = new BleDeviceAdapter(mActivity, devicesList);
        mBinding.gvBleDevices.setAdapter(mAdapter);
        mBinding.gvBleDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // 一開始所有 item 都沒有被選中, item 被選中後 view 長得不太一樣, 一旦選擇某個 item 後所有的 item view 要重畫
                mAdapter.setSelectedIndex(position);
                mAdapter.notifyDataSetChanged();
                // 選擇任何一個 device 後, continue 的按鈕就能按了
                mBinding.btnContinue.setEnabled(true);
                mViewModel.setSelectedIndex(position);
            }
        });
        mBinding.btnHelpProductCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onContinue(Code.Action.DeviceSetup.HELP_PRODUCT_CODE);
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (DataManager.getInstance().getSetupMode()) {
                    case ROUTER:
                    case ROUTER_RESETUP:
                        // 理論上 resetup router 時只能顯示原本那台 router, 不會進入這頁 multiple device found 才對
                        // 不過以防萬一這裡還是判斷一下
                        mEventListener.onContinue(Code.Action.DeviceSetup.SETUP_ROUTER);
                        break;

                    case REPEATER:
                    default:
                        mEventListener.onContinue(Code.Action.DeviceSetup.SETUP_REPEATER);
                        break;
                }
            }
        });
        // 一進入頁面時沒有選擇任何 device, 所以 continue 的按鈕不能作用 (按鈕會是灰色的)
        mBinding.btnContinue.setEnabled(false);
        mEventListener.setSupportView(mBinding.tvSupport);
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(BleScanViewModel.class);
    }
}
