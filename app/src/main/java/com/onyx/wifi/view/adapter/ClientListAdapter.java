package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.advancewireless.dmz.DestinationClient;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.interfaces.OnClientListChangedListener;

import java.util.ArrayList;
import java.util.List;

public class ClientListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private enum ListItemType {
        RECENTLY_SECTION,
        LONG_INACTIVE_SECTION,
        ITEM
    }

    private List<ListItem> mListItems = new ArrayList<>();

    private List<ClientModel> mClientModelList = new ArrayList<>();

    private List<ClientModel> mRecentlyClientModelList = new ArrayList<>();

    private List<ClientModel> mLongInactiveClientModelList = new ArrayList<>();

    private List<DestinationClient> mSelectedClientList = new ArrayList<>();

    private boolean mIsMultiSelectEnabled = false;

    private boolean mIsRecentlyExtend = false;
    private boolean mIsLongInactiveExtend = false;

    private OnClientListChangedListener mListener;

    public ClientListAdapter(boolean isMultiSelectEnabled, OnClientListChangedListener listener) {
        mIsMultiSelectEnabled = isMultiSelectEnabled;

        mListener = listener;

        setupListItems();
    }

    public void setData(List<ClientModel> clientModelList, List<DestinationClient> clientList) {
        mClientModelList = clientModelList;

        mSelectedClientList = clientList;

        setupClientModelList();

        setupListItems();
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<ListItemType, Object> listItem = mListItems.get(position);
        ListItemType type = listItem.getType();

        return type.ordinal();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        if (viewType != ListItemType.ITEM.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_client_list_section, parent, false);

            SectionViewHolder viewHolder = new SectionViewHolder(view);

            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_client_list_item, parent, false);

        ClientViewHolder viewHolder = new ClientViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<ListItemType, Object> listItem = mListItems.get(position);
        ListItemType type = listItem.getType();

        if (type != ListItemType.ITEM) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder)viewHolder;
            sectionViewHolder.setListItem(listItem);
            return;
        }

        ClientModel clientModel = (ClientModel) listItem.getItem();

        ClientViewHolder clientViewHolder = (ClientViewHolder)viewHolder;
        clientViewHolder.setClientModel(clientModel);
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    private void setupClientModelList() {
        mRecentlyClientModelList.clear();
        mLongInactiveClientModelList.clear();

        for (ClientModel model : mClientModelList) {
            long timestamp = model.getLastOnlineTimestamp();
            if (timestamp != 0) {
                mRecentlyClientModelList.add(model);
            } else {
                mLongInactiveClientModelList.add(model);
            }
        }

        mIsRecentlyExtend = mRecentlyClientModelList.size() < 4;

        mIsLongInactiveExtend = mLongInactiveClientModelList.size() < 4;
    }

    private void setupListItems() {
        mListItems.clear();

        ListItem<ListItemType, String> recentlySection = new ListItem(ListItemType.RECENTLY_SECTION, "Recently Active Devices");
        mListItems.add(recentlySection);

        for (ClientModel model: getRecentlyClientModelList()) {
            ListItem<ListItemType, ClientModel> item = new ListItem(ListItemType.ITEM, model);
            mListItems.add(item);
        }

        ListItem<ListItemType, String> longInactiveSection = new ListItem(ListItemType.LONG_INACTIVE_SECTION, "Long Inactive Devices");
        mListItems.add(longInactiveSection);

        for (ClientModel model: getLongInactiveClientModelList()) {
            ListItem<ListItemType, ClientModel> item = new ListItem(ListItemType.ITEM, model);
            mListItems.add(item);
        }

        notifyDataSetChanged();
    }

    private List<ClientModel> getRecentlyClientModelList() {
        if (mRecentlyClientModelList.size() <= 3) {
            return mRecentlyClientModelList;
        }

        if (!mIsRecentlyExtend) {
            return mRecentlyClientModelList.subList(0, 3);
        }

        return mRecentlyClientModelList;
    }

    private List<ClientModel> getLongInactiveClientModelList() {
        if (mLongInactiveClientModelList.size() <= 3) {
            return mLongInactiveClientModelList;
        }

        if (!mIsLongInactiveExtend) {
            return mLongInactiveClientModelList.subList(0, 3);
        }

        return mLongInactiveClientModelList;
    }

    class SectionViewHolder extends RecyclerView.ViewHolder {

        ListItem<ListItemType, Object> mListItem;

        TextView mTitleTextView;

        Button mButton;

        public SectionViewHolder(@NonNull View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.titleTextView);

            mButton = itemView.findViewById(R.id.extendButton);

            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListItemType type = mListItem.getType();

                    if (type == ListItemType.RECENTLY_SECTION) {
                        mIsRecentlyExtend = !mIsRecentlyExtend;
                    }

                    if (type == ListItemType.LONG_INACTIVE_SECTION) {
                        mIsLongInactiveExtend = !mIsLongInactiveExtend;
                    }

                    setupListItems();
                }
            });
        }

        public void setListItem(ListItem<ListItemType, Object> listItem) {
            mListItem = listItem;

            String title = (String) mListItem.getItem();
            mTitleTextView.setText(title);

            ListItemType type = mListItem.getType();

            if (type == ListItemType.RECENTLY_SECTION) {
                if (mIsRecentlyExtend) {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_hide_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_hide, 0);
                } else {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_more_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);
                }

                if (mRecentlyClientModelList.size() > 3) {
                    mButton.setVisibility(View.VISIBLE);
                } else {
                    mButton.setVisibility(View.INVISIBLE);
                }
            }

            if (type == ListItemType.LONG_INACTIVE_SECTION) {
                if (mIsLongInactiveExtend) {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_hide_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_hide, 0);
                } else {
                    Context context = itemView.getContext();
                    String buttonTitle = context.getString(R.string.member_display_more_device);
                    mButton.setText(buttonTitle);

                    mButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_member_more, 0);
                }

                if (mLongInactiveClientModelList.size() > 3) {
                    mButton.setVisibility(View.VISIBLE);
                } else {
                    mButton.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    class ClientViewHolder extends RecyclerView.ViewHolder {

        Client mClient;

        ImageView mIconImageView;
        TextView mNameTextView;
        TextView mDescriptionTextView;
        ImageView mDefaultUserImageView;
        CardView mCardView;
        ImageView mPhotoImageView;
        ImageView mLightImageView;
        ImageView mSelectedImageView;
        //CheckBox mcbSelected;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = itemView.findViewById(R.id.iconImageView);

            mNameTextView = itemView.findViewById(R.id.nameTextView);

            mDescriptionTextView = itemView.findViewById(R.id.descriptionTextView);

            mDefaultUserImageView = itemView.findViewById(R.id.defaultUserImageView);
            mCardView = itemView.findViewById(R.id.userCardView);
            mPhotoImageView = itemView.findViewById(R.id.photoImageView);

            mLightImageView = itemView.findViewById(R.id.lightImageView);

            mSelectedImageView = itemView.findViewById(R.id.selectedImageView);

//            mcbSelected = itemView.findViewById(R.id.cbSelected);
//            mcbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    int position = getAdapterPosition();
//                    if (isChecked) {
//                        if (mSelectedPos != position) {//(2) 当前选中的position和上次选中不是同一个position 执行
//                            cbIsAlexaConnect.setChecked(true);
//                            if (mSelectedPos != -1) {//判断是否有效 -1是初始值 即无效 第二个参数是Object 随便传个int 这里只是起个标志位
//                                notifyItemChanged(mSelectedPos, 0); //(2) 主要call 此function更新 (by onBindViewHolder)
//                            }
//                            mSelectedPos = position;//(2)
//                        }
//                        mClickHandler.enableButton(position);
//                    } else {
//                        //mSelectedPos = -1; // <=沒有checkbox被選中(for單選)，要記得恢復預設值
//
//                        mClickHandler.diableButton();
//                    }
//
//                }
//            });

//        }
            mSelectedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = mClient.getName();
                    String type = mClient.getClientType();
                    String currentMac = mClient.getCid();
                    String ip = mClient.getIp();
                    DestinationClient selectedClient = new DestinationClient(type, name, currentMac, ip);


                    if (mIsMultiSelectEnabled) {

                        int index = findInadeByMac(currentMac);

                        if (index == -1) {
                            mSelectedClientList.add(selectedClient);
                        } else {
                            mSelectedClientList.remove(index);
                        }

                    } else {
                        mSelectedClientList.clear();
                        mSelectedClientList.add(selectedClient);
                    }

                    notifyDataSetChanged();

                    if (mListener != null) {
                        mListener.onClientListChanged(mSelectedClientList);
                    }
                }
            });
        }

        private int findInadeByMac(String searchMac) {
            for (int index = 0; index < mSelectedClientList.size(); index++) {
                DestinationClient client = mSelectedClientList.get(index);
                String cid = client.getCid();

                if (searchMac.equalsIgnoreCase(cid)) {
                    return index;
                }
            }

            return -1;
        }

        public void setClientModel(ClientModel clientModel) {
            mClient = new Client(clientModel);

            setClientType();

            String name = mClient.getName();
            mNameTextView.setText(name);

            Context context = itemView.getContext();
            String connectionMessage = mClient.getLastOnlineTimeInfo();//.getConnectionMessage(context);

            mDescriptionTextView.setText(connectionMessage);

            setOwnerPhoto();

            setStatus();

            setSelected();
        }

        public void setClientType() {
            int iconId = mClient.getClientIcon();

            mIconImageView.setImageResource(iconId);
        }

        public void setOwnerPhoto() {
            String ownerId = mClient.getOwnerId();

            if (ownerId == null) {
                mDefaultUserImageView.setVisibility(View.INVISIBLE);
                mCardView.setVisibility(View.INVISIBLE);
                mPhotoImageView.setVisibility(View.INVISIBLE);

                return;
            }

            mDefaultUserImageView.setVisibility(View.VISIBLE);
            mCardView.setVisibility(View.VISIBLE);
            mPhotoImageView.setVisibility(View.VISIBLE);
            mPhotoImageView.setImageResource(0);

            Context context = itemView.getContext();
            Bitmap bitmap = mClient.getOwnerPhoto(context, ownerId);

            if (bitmap != null) {
                mPhotoImageView.setImageBitmap(bitmap);
            }
        }

        public void setStatus() {
            int light = mClient.getConnectionLight();

            mLightImageView.setImageResource(light);
        }

        public void setSelected() {
            mSelectedImageView.setSelected(false);

            String currentMac = mClient.getCid();

            for (DestinationClient client:mSelectedClientList) {
                String cid = client.getCid();

                if (currentMac.equalsIgnoreCase(cid)) {
                    mSelectedImageView.setSelected(true);
                    return;
                }
            }

        }
    }
}
