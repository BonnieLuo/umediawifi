package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.Language;

import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(Language lang);
        // 提供onItemRemove做為移除項目的事件
        //void onItemRemove(int dataCount, UserType userType, int position);
    }

    private Context mContext;
    public static List<Language> mData; // "public static": global data for related pages(activities)
    //
    // 2. 宣告interface
    //
    private OnItemClickHandler mClickHandler;
    public LanguageAdapter(Context context, List<Language> data, OnItemClickHandler clickHandler){
        mContext = context;
        mData = data;
        mClickHandler = clickHandler;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        //
        // list item所含的widgets
        //
        private TextView tvItemContent;

        ViewHolder(View itemView){
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            tvItemContent = (TextView)itemView.findViewById(R.id.tvItemContent);


            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Language lang = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(lang);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    @Override
    public LanguageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_string_list_item, viewGroup, false); //取得list的view
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tvItemContent.setText(mData.get(i).getLang());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
