package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityProductPlacementQuestionBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.customized.NoUnderlineSpan;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.ListDisplayType;

public class ProductPlacementQuestionActivity extends BaseMoreHelpActivity {

    private ActivityProductPlacementQuestionBinding mBinding;

    private ListDisplayType mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_placement_question);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        //
        // set partial color of some TextView content
        //
        //enableSupportLink(mBinding.tv1Solution, mBinding.tv1Solution.getText().toString(), R.color.purple_7110b2);
        //
        // onclick handler
        //
        mBinding.ctv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                }else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        setBottomLayout(mBinding.btnReturnToSetup, mBinding.tvTrouble, mBinding.tvSupport, mBinding.menuBar, mBinding.clContent);
    }

    private void showByDisplayType() {
        // 1
        if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("+");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl1Solution.setVisibility(View.GONE);
        } else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("-");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl1Solution.setVisibility(View.VISIBLE);
        }

        // 2
        if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("+");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl2Solution.setVisibility(View.GONE);
        } else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("-");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl2Solution.setVisibility(View.VISIBLE);
        }
    }
        private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.more_help_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
