package com.onyx.wifi.view.interfaces.member;

public interface OnMemberChangedListener {

    void onMemberChanged();

    void onMemberRollback();
    
}
