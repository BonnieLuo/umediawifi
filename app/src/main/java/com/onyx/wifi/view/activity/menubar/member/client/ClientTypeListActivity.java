package com.onyx.wifi.view.activity.menubar.member.client;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityClientTypeIconBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.member.client.ClientTypeListAdapter;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.interfaces.member.OnClientTypeClickListener;

public class ClientTypeListActivity extends BaseMenuActivity implements OnClientTypeClickListener {

    private ActivityClientTypeIconBinding mBinding;

    private ClientTypeListAdapter mAdapter;

    private String mSelectedClientType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_client_type_icon);

        mAdapter = new ClientTypeListAdapter(this, mSelectedClientType, this);

        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.device_icon_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedClientType != null) {
                    DataManager dataManager = DataManager.getInstance();
                    Client targetClient = dataManager.getManageClient();
                    targetClient.setTemporaryClientType(mSelectedClientType);
                }

                finish();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onClientTypeClick(String clientType) {
        if (mSelectedClientType == null || !clientType.equalsIgnoreCase(mSelectedClientType)) {
            mSelectedClientType = clientType;

            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mSelectedClientType = null;

            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }

        mAdapter.setSelectedClientType(mSelectedClientType);
    }
}
