package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewOptionItemBinding;

import java.util.ArrayList;

public class OptionListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> mOptionStrings;
    private ArrayList<Integer> mOptionIconIds;

    public OptionListAdapter(Context context, ArrayList<String> optionStrings, ArrayList<Integer> optionIcons) {
        mContext = context;
        mOptionStrings = optionStrings;
        mOptionIconIds = optionIcons;
    }

    @Override
    public int getCount() {
        return mOptionStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return mOptionStrings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OptionItemViewHolder holder;

        if (convertView == null) {
            ViewOptionItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.view_option_item, null, false);
            convertView = binding.getRoot();
            holder = new OptionItemViewHolder();
            holder.imgOptionIcon = binding.imgOptionIcon;
            holder.tvOption = binding.tvOption;
            convertView.setTag(holder);
        } else {
            holder = (OptionItemViewHolder) convertView.getTag();
        }

        // 透過 view holder 來為 item set data
        holder.tvOption.setText(mOptionStrings.get(position));
        if (mOptionIconIds != null) {
            // mOptionIconIds 不為 null 代表有設定 icon, 將圖示設定到 image view
            holder.imgOptionIcon.setImageResource(mOptionIconIds.get(position));
        } else {
            // 沒有設定 icon, 隱藏 image view
            holder.imgOptionIcon.setVisibility(View.GONE);
        }

        return convertView;
    }

    private class OptionItemViewHolder {
        ImageView imgOptionIcon;
        TextView tvOption;
    }
}
