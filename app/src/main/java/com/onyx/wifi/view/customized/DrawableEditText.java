package com.onyx.wifi.view.customized;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.interfaces.DrawableClickListener;

import static com.onyx.wifi.view.interfaces.DrawableClickListener.DrawablePosition.RIGHT;

// 尚未開發完成的元件, 因時程考量先暫停
public class DrawableEditText extends AppCompatEditText {

    private Context mContext;

    private Drawable mDrawableRight;
    private Drawable mDrawableLeft;
    private Drawable mDrawableTop;
    private Drawable mDrawableBottom;

    private int mActionX, mActionY;

    private DrawableClickListener mClickListener;

    public DrawableEditText(Context context, AttributeSet attrs) {
        // this Constructor is required when you are using this view in xml
        super(context, attrs);
        mContext = context;
    }

    public DrawableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        // 在 xml 裡面設定 drawableLeft/drawableRight/drawableTop/drawableBottom 屬性, 該 function 會被呼叫到
        // 若有設定 drawable 會被儲存下來, drawable 的點擊事件會使用到
        if (left != null) {
            mDrawableLeft = left;
        }
        if (right != null) {
            mDrawableRight = right;
        }
        if (top != null) {
            mDrawableTop = top;
        }
        if (bottom != null) {
            mDrawableBottom = bottom;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Rect bounds;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mActionX = (int) event.getX();
            mActionY = (int) event.getY();
            if (mDrawableBottom != null && mDrawableBottom.getBounds().contains(mActionX, mActionY)) {
                mClickListener.onClick(DrawableClickListener.DrawablePosition.BOTTOM);
                return super.onTouchEvent(event);
            }

            if (mDrawableTop != null && mDrawableTop.getBounds().contains(mActionX, mActionY)) {
                mClickListener.onClick(DrawableClickListener.DrawablePosition.TOP);
                return super.onTouchEvent(event);
            }

            // this works for left since container shares 0,0 origin with bounds
            if (mDrawableLeft != null) {
                bounds = mDrawableLeft.getBounds();

                int x, y;
                int extraTapArea = (int) (13 * getResources().getDisplayMetrics().density + 0.5);

                x = mActionX;
                y = mActionY;

                if (!bounds.contains(mActionX, mActionY)) {
                    /** Gives the +20 area for tapping. */
                    x = mActionX - extraTapArea;
                    y = mActionY - extraTapArea;

                    if (x <= 0) {
                        x = mActionX;
                    }
                    if (y <= 0) {
                        y = mActionY;
                    }

                    /** Creates square from the smallest value */
                    if (x < y) {
                        y = x;
                    }
                }

                if (bounds.contains(x, y) && mClickListener != null) {
                    mClickListener.onClick(DrawableClickListener.DrawablePosition.LEFT);
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return false;
                }
            }

            if (mDrawableRight != null) {
                bounds = mDrawableRight.getBounds();

                int x, y;
                int extraTapArea = 40;

                /**
                 * IF USER CLICKS JUST OUT SIDE THE RECTANGLE OF THE DRAWABLE
                 * THAN ADD X AND SUBTRACT THE Y WITH SOME VALUE SO THAT AFTER
                 * CALCULATING X AND Y CO-ORDINATE LIES INTO THE DRAWBABLE
                 * BOUND. - this process help to increase the tappable area of
                 * the rectangle.
                 */
                x = mActionX + extraTapArea;
                y = mActionY - extraTapArea;

                /**
                 * Since this is right drawable subtract the value of x from the width
                 * of view. so that width - tappedarea will result in x co-ordinate in drawable bound. 
                 */
                x = getWidth() - x;

                /**
                 * x can be negative if user taps at x co-ordinate just near the width.
                 * e.g views width = 300 and user taps 290. Then as per previous calculation
                 * 290 + 13 = 303. So subtract X from getWidth() will result in negative value.
                 * So to avoid this add the value previous added when x goes negative.
                 */

                if (x <= 0) {
                    x += extraTapArea;
                }

                /**
                 *  If result after calculating for extra tappable area is negative.
                 * assign the original value so that after subtracting
                 * extratapping area value doesn't go into negative value.
                 */

                if (y <= 0) {
                    y = mActionY;
                }

                /* If drawable bounds contains the x and y points then move ahead.*/
                if (bounds.contains(x, y) && mClickListener != null) {
                    mClickListener.onClick(RIGHT);
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return false;
                }
                return super.onTouchEvent(event);
            }

        }

        return super.onTouchEvent(event);
    }

    @Override
    protected void finalize() throws Throwable {
        mDrawableRight = null;
        mDrawableBottom = null;
        mDrawableLeft = null;
        mDrawableTop = null;
        super.finalize();
    }

    public void setDrawableClickListener(DrawableClickListener listener) {
        this.mClickListener = listener;
    }

    // for most use case: "edit" icon
    public void setDrawableEdit() {
        mClickListener = new DrawableClickListener() {
            @Override
            public void onClick(DrawablePosition position) {
                switch (position) {
                    case RIGHT:
                        CommonUtils.toast(mContext, "lalalalalala");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                setEditableState(true);
                            }
                        }, 100);
                        break;

                    default:
                        break;
                }
            }
        };
//        this.setOnFocusChangeListener(new OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                LogUtils.trace("focus: " + hasFocus);
//                if (!hasFocus) {
//                    setEditableState(false);
//                }
//            }
//        });
    }

    public void setEditableState(boolean isEditable) {
        if (isEditable) {
            setEnabled(true);
            setFocusableInTouchMode(true);
            setFocusable(true);
            requestFocus();
            CommonUtils.showKeyboard(mContext, this);
        } else {
            setEnabled(false);
            setFocusableInTouchMode(false);
            setFocusable(false);
            clearFocus();
        }
    }
}
