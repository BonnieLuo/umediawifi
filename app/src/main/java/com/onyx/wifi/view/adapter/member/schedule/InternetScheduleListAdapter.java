package com.onyx.wifi.view.adapter.member.schedule;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.ScheduleConfiguration;
import com.onyx.wifi.model.item.member.ScheduleConfigurationModel;
import com.onyx.wifi.view.activity.menubar.member.schedule.InternetScheduleActivity;

import java.util.ArrayList;
import java.util.List;

public class InternetScheduleListAdapter extends RecyclerView.Adapter<InternetScheduleListAdapter.MemberScheduleViewHolder> {

    List<ScheduleConfigurationModel> scheduleListList = new ArrayList<>();

    @NonNull
    @Override
    public MemberScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_member_schedule_item, parent, false);

        MemberScheduleViewHolder viewHolder = new MemberScheduleViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MemberScheduleViewHolder viewHolder, int position) {
        ScheduleConfigurationModel memberScheduleModel = scheduleListList.get(position);

        viewHolder.setMemberScheduleModel(memberScheduleModel);
    }

    @Override
    public int getItemCount() {
        return scheduleListList.size();
    }

    public void setScheduleListList(List<ScheduleConfigurationModel> newScheduleListList) {
        scheduleListList = newScheduleListList;

        notifyDataSetChanged();
    }

    public class MemberScheduleViewHolder extends RecyclerView.ViewHolder {

        private ScheduleConfiguration mScheduleConfig;

        private TextView titleTextView;

        private TextView enableTextView;

        private ImageView arrowImageView;

        public MemberScheduleViewHolder(@NonNull View itemView) {
            super(itemView);

            titleTextView = this.itemView.findViewById(R.id.titleTextView);

            enableTextView = this.itemView.findViewById(R.id.enableTextView);

            arrowImageView = this.itemView.findViewById(R.id.arrow);
            arrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataManager dataManager = DataManager.getInstance();
                    dataManager.setScheduleConfiguration(mScheduleConfig);

                    Context context = itemView.getContext();
                    Intent intent = new Intent(context, InternetScheduleActivity.class);
                    context.startActivity(intent);
                }
            });
        }

        public void setMemberScheduleModel(ScheduleConfigurationModel scheduleModel) {
            mScheduleConfig = new ScheduleConfiguration(scheduleModel);

            String title = mScheduleConfig.getName();
            titleTextView.setText(title);

            if (mScheduleConfig.isEnabled()) {
                enableTextView.setText("Enabled");
            } else {
                enableTextView.setText("Disable");
            }
        }
    }
}
