package com.onyx.wifi.view.customized;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewMenuBarBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.NodeSetInfo;

public class MenuBar extends FrameLayout {
    private Context mContext;
    private ViewMenuBarBinding mBinding;

    public enum MenuBarItem {
        HOME, MEMBER, MENU, PAUSE, NOTIFICATION
    }

    public enum MainMenuItem {
        NETWORK_SETTING, WIRELESS_SETTING, ANALYTICS, PARENTAL_CONTROL,
        VOICE_ASSISTANCE, SECURITY, ACCOUNT, TROUBLESHOOTING
    }

    private MenuBarItem mMenuBarFocusItem;

    public MenuBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.view_menu_bar, this, true);

        setView();
        setData();
    }

    private void setView() {
        mBinding.clMainMenu.setVisibility(View.INVISIBLE);
        mBinding.clMainMenu.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mBinding.clMainMenu.getVisibility() == View.VISIBLE) {
                    /* main menu 開啟時, 被覆蓋在頁面下層佈局的元件不可響應點擊事件.
                       由於 onTouch 的預設返回值是 false, 所以會繼續往下調用下層元件的點擊事件.
                       返回 true 代表 main menu 已處理了事件, 不用繼續往下調用事件. */
                    return true;
                }
                return false;
            }
        });

        mMenuBarFocusItem = null;
    }

    // menu bar 這些資料是來自 dashboard/wireless API, 所以若資料有更新的話, menu bar 也要更新
    public void setData() {
        mBinding.tvNetworkName.setText(DataManager.getInstance().getNetworkName());

        NodeSetInfo dashboardNodeSet = DataManager.getInstance().getDashboardNodeSet();
        int clientListCount = 0;
        if (dashboardNodeSet != null) {
            clientListCount = dashboardNodeSet.getClientListCount();
        }
        if (clientListCount > 0) {
            mBinding.tvClientCount.setVisibility(View.VISIBLE);
            mBinding.tvClientCount.setText(String.valueOf(clientListCount));
        } else {
            mBinding.tvClientCount.setVisibility(View.INVISIBLE);
        }

        // Notification 功能現在還沒做, 所以先隱藏 notification count
        mBinding.tvNotificationCount.setVisibility(View.INVISIBLE);
    }

    public void toggleMainMenu() {
        if (isMainMenuOpened()) {
            // main menu 原本是開啟的狀態, 就關閉
            mBinding.clMainMenu.setVisibility(View.INVISIBLE);
            setMenuBarItemFocus(MenuBarItem.MENU, false);
        } else {
            // 更新資料並開啟 main menu
            setData();
            mBinding.clMainMenu.setVisibility(View.VISIBLE);
            setMenuBarItemFocus(MenuBarItem.MENU, true);
        }
    }

    public void closeMainMenu() {
        mBinding.clMainMenu.setVisibility(View.INVISIBLE);
        setMenuBarItemFocus(MenuBarItem.MENU, false);
    }

    public ConstraintLayout getMenuTitleBar() {
        return mBinding.menuTitleBar;
    }

    public boolean isMainMenuOpened() {
        return mBinding.clMainMenu.getVisibility() == View.VISIBLE;
    }

    public MenuBarItem getMenuBarFocusItem() {
        return mMenuBarFocusItem;
    }

    // 設定下方 menu bar 項目的 focus 狀態
    // 平常狀態的 icon 線條是紫色, 被 focus 的話 icon 線條是白色
    public void setMenuBarItemFocus(MenuBarItem item, boolean isFocus) {
        resetAllMenuBarItemFocus();
        int drawableId;
        switch (item) {
            case HOME:
                drawableId = (isFocus) ? R.drawable.ic_bar_dashboard_1 : R.drawable.ic_bar_dashboard_0;
                mBinding.btnHome.setImageResource(drawableId);
                break;
            case MEMBER:
                drawableId = (isFocus) ? R.drawable.ic_bar_member_1 : R.drawable.ic_bar_member_0;
                mBinding.btnMember.setImageResource(drawableId);
                if (drawableId == R.drawable.ic_bar_member_1)
                    mBinding.tvClientCount.setBackgroundResource(R.drawable.shape_circle_white);
                else
                    mBinding.tvClientCount.setBackgroundResource(R.drawable.shape_circle_purple);
                break;
            case MENU:
                drawableId = (isFocus) ? R.drawable.ic_bar_menu_1 : R.drawable.ic_bar_menu_0;
                mBinding.btnMenu.setImageResource(drawableId);
                if (!isFocus && mMenuBarFocusItem != null) {
                    // 關閉 main menu 時, 原本 menu bar 被 focus 的項目要再次被 focus
                    setMenuBarItemFocus(mMenuBarFocusItem, true);
                }
                break;
            case PAUSE:
                drawableId = (isFocus) ? R.drawable.ic_bar_pause_1 : R.drawable.ic_bar_pause_0;
                mBinding.btnPause.setImageResource(drawableId);
                break;
            case NOTIFICATION:
                drawableId = (isFocus) ? R.drawable.ic_bar_notification_1 : R.drawable.ic_bar_notification_0;
                mBinding.btnNotification.setImageResource(drawableId);
                break;
        }
        if (isFocus && item != MenuBarItem.MENU) {
            // 紀錄原本被 focus 的項目, 若開啟 main menu 時, 原本項目的 focus 要消失, 關閉 main menu 時則要再次被 focus
            mMenuBarFocusItem = item;
        }
    }

    private void resetAllMenuBarItemFocus() {
        mBinding.btnHome.setImageResource(R.drawable.ic_bar_dashboard_0);
        mBinding.btnMember.setImageResource(R.drawable.ic_bar_member_0);
        mBinding.tvClientCount.setBackgroundResource(R.drawable.shape_circle_purple);
        mBinding.btnMenu.setImageResource(R.drawable.ic_bar_menu_0);
        mBinding.btnPause.setImageResource(R.drawable.ic_bar_pause_0);
        mBinding.btnNotification.setImageResource(R.drawable.ic_bar_notification_0);
    }

    public void setMenuBarItemClickListener(MenuBarItem item, View.OnClickListener listener) {
        ImageButton itemView = null;
        switch (item) {
            case HOME:
                itemView = mBinding.btnHome;
                break;
            case MEMBER:
                itemView = mBinding.btnMember;
                break;
            case MENU:
                itemView = mBinding.btnMenu;
                break;
            case PAUSE:
                itemView = mBinding.btnPause;
                break;
            case NOTIFICATION:
                itemView = mBinding.btnNotification;
                break;
            default:
                break;
        }
        if (itemView != null) {
            itemView.setOnClickListener(listener);
        }
    }

    public void setMainMenuItemClickListener(MainMenuItem item, View.OnClickListener listener) {
        Button itemView = null;
        switch (item) {
            case NETWORK_SETTING:
                itemView = mBinding.btnNetworkSetting;
                break;
            case WIRELESS_SETTING:
                itemView = mBinding.btnWirelessSetting;
                break;
            case ANALYTICS:
                itemView = mBinding.btnAnalytics;
                break;
            case PARENTAL_CONTROL:
                itemView = mBinding.btnParentalControl;
                break;
            case VOICE_ASSISTANCE:
                itemView = mBinding.btnVoiceAssistance;
                break;
            case SECURITY:
                itemView = mBinding.btnSecurity;
                break;
            case ACCOUNT:
                itemView = mBinding.btnAccount;
                break;
            case TROUBLESHOOTING:
                itemView = mBinding.btnTroubleshooting;
                break;
            default:
                break;
        }
        if (itemView != null) {
            itemView.setOnClickListener(listener);
        }
    }

    public void performClickMainMenuItem(MainMenuItem item) {
        Button itemView = null;
        switch (item) {
            case NETWORK_SETTING:
                itemView = mBinding.btnNetworkSetting;
                break;
            case WIRELESS_SETTING:
                itemView = mBinding.btnWirelessSetting;
                break;
            case ANALYTICS:
                itemView = mBinding.btnAnalytics;
                break;
            case PARENTAL_CONTROL:
                itemView = mBinding.btnParentalControl;
                break;
            case VOICE_ASSISTANCE:
                itemView = mBinding.btnVoiceAssistance;
                break;
            case SECURITY:
                itemView = mBinding.btnSecurity;
                break;
            case ACCOUNT:
                itemView = mBinding.btnAccount;
                break;
            case TROUBLESHOOTING:
                itemView = mBinding.btnTroubleshooting;
                break;
            default:
                break;
        }
        if (itemView != null) {
            itemView.performClick();
        }
    }
}
