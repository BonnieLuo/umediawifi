package com.onyx.wifi.view.adapter.pause;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.interfaces.pause.OnMemberSelectedListener;

import java.util.ArrayList;
import java.util.List;

public class FamilyMemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListItem> mListItems = new ArrayList<>();

    List<MemberModel> mFamilyMemberModelList = new ArrayList<>();

    private List<String> mSelectedMemberList = new ArrayList<>();

    private OnMemberSelectedListener mOnMemberSelectedListener;

    public FamilyMemberListAdapter(OnMemberSelectedListener listener) {
        super();

        mOnMemberSelectedListener = listener;
    }

    public void setData(List<MemberModel> familyMemeberModelList, List<String> selectedMemberList) {
        mFamilyMemberModelList = familyMemeberModelList;

        List<ListItem> listItems = new ArrayList<>();

        ListItem<SelectMemberItemType, String> section = new ListItem<>(SelectMemberItemType.SECTION, "Select Family Members");
        listItems.add(section);

        for (MemberModel model:mFamilyMemberModelList) {
            FamilyMember familyMember = new FamilyMember(model);
            ListItem<SelectMemberItemType, FamilyMember> item = new ListItem<>(SelectMemberItemType.ITEM, familyMember);
            listItems.add(item);
        }

        if (selectedMemberList != null) {
            mSelectedMemberList.clear();
            for (String memberId:selectedMemberList) {
                mSelectedMemberList.add(memberId);
            }
        }

        mListItems = listItems;

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<SelectMemberItemType, Object> listItem = mListItems.get(position);

        SelectMemberItemType type = listItem.getType();

        if (type == SelectMemberItemType.SECTION) {
            return SelectMemberItemType.SECTION.ordinal();
        }

        return SelectMemberItemType.ITEM.ordinal();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == SelectMemberItemType.SECTION.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_select_list_section, parent, false);

            MemberSectionViewHolder viewHolder = new MemberSectionViewHolder(view);

            return viewHolder;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_member_select_list_item, parent, false);

        MemberViewHolder viewHolder = new MemberViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<SelectMemberItemType, Object> listItem = mListItems.get(position);

        SelectMemberItemType type = listItem.getType();

        switch (type) {
            case SECTION:
                setHeader(listItem, viewHolder);

                break;
            case ITEM:
            default:
                setItem(listItem, viewHolder);

                break;
        }
    }

    private void setHeader(ListItem<SelectMemberItemType, Object> listItem, RecyclerView.ViewHolder viewHolder) {
        String title = (String) listItem.getItem();

        if (title != null) {

            MemberSectionViewHolder sectionViewHolder = (MemberSectionViewHolder) viewHolder;

            sectionViewHolder.setTitle(title);
            sectionViewHolder.isSelected();
        }
    }

    private void setItem(ListItem<SelectMemberItemType, Object> listItem, RecyclerView.ViewHolder viewHolder){
        FamilyMember member = (FamilyMember) listItem.getItem();

        if (member != null) {

            MemberViewHolder memberViewHolder = (MemberViewHolder) viewHolder;

            memberViewHolder.setMember(member);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    class MemberSectionViewHolder extends RecyclerView.ViewHolder {

        TextView mTitleTextView;
        ImageView mSelectedImageView;

        public MemberSectionViewHolder(@NonNull View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.titleTextView);

            mSelectedImageView = itemView.findViewById(R.id.selectedButton);
            mSelectedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectedMemberList != null) {
                        if (mSelectedMemberList.size() == mFamilyMemberModelList.size()) {
                            mSelectedMemberList.clear();
                        } else {
                            mSelectedMemberList.clear();
                            for (MemberModel model:mFamilyMemberModelList) {
                                String id = model.getMemberId();
                                mSelectedMemberList.add(id);
                            }
                        }
                    }

                    isSelected();

                    notifyDataSetChanged();

                    mOnMemberSelectedListener.OnMemberSelectedListener(mSelectedMemberList);
                }
            });
        }

        public void setTitle(String title) {
            mTitleTextView.setText(title);
        }

        public void isSelected() {
            if (mSelectedMemberList != null) {
                if (mSelectedMemberList.size() == mFamilyMemberModelList.size()) {
                    mSelectedImageView.setSelected(true);
                } else {
                    mSelectedImageView.setSelected(false);
                }
            }
        }

    }

    class MemberViewHolder extends RecyclerView.ViewHolder {

        FamilyMember mMember;

        ImageView mUserImageView;
        TextView mNameTextView;
        ImageButton mSelectedButton;

        public MemberViewHolder(@NonNull View itemView) {
            super(itemView);

            mUserImageView = itemView.findViewById(R.id.userImageView);

            mNameTextView = itemView.findViewById(R.id.textView);

            mSelectedButton = itemView.findViewById(R.id.assignButton);
            mSelectedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = mMember.getId();

                    if (mSelectedMemberList != null) {
                        if (mSelectedMemberList.contains(id)) {
                            mSelectedMemberList.remove(id);
                        } else {
                            mSelectedMemberList.add(id);
                        }
                    }

                    isSelected();

                    notifyDataSetChanged();

                    mOnMemberSelectedListener.OnMemberSelectedListener(mSelectedMemberList);
                }
            });
        }

        public void setMember(FamilyMember member) {
            mMember = member;

            Context context = this.itemView.getContext();
            Bitmap pictureBitmap = mMember.getPhoto(context);

            if (pictureBitmap != null) {
                mUserImageView.setImageBitmap(pictureBitmap);
            } else {
                mUserImageView.setImageResource(0);
            }

            String name = mMember.getName();
            mNameTextView.setText(name);
            mNameTextView.setTextColor(context.getResources().getColor(R.color.gray_676767));

            isSelected();
        }

        public boolean isSelected() {
            String memberId = mMember.getId();

            if (mSelectedMemberList.contains(memberId)) {
                Context context = this.itemView.getContext();
                mNameTextView.setTextColor(context.getResources().getColor(R.color.purple_4e1393));
                mSelectedButton.setSelected(true);
                return true;
            }

            Context context = this.itemView.getContext();
            mNameTextView.setTextColor(context.getResources().getColor(R.color.gray_676767));
            mSelectedButton.setSelected(false);
            return false;
        }

    }
}
