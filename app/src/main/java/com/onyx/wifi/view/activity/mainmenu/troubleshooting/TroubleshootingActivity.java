package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityTroubleshootingBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.NodeSetInfo;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.TroubleshootingViewModel;

public class TroubleshootingActivity extends BaseMenuActivity {

    private ActivityTroubleshootingBinding mBinding;
    private TroubleshootingViewModel mViewModel;

    private NodeSetInfo mNodeSetInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_troubleshooting);
        setView();
        setViewModel();

        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.EVENT_SPEED_TEST_UPDATE);
        intentFilter.addAction(AppConstants.EVENT_DEVICE_SPEED_TEST);
        registerReceiver(mEventReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.clInternetSpeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvRerunTest.setVisibility(View.INVISIBLE);
                mBinding.pbRerunTest.setVisibility(View.VISIBLE);
                mViewModel.startSpeedTest();
            }
        });

        mBinding.clNetworkSpeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, NetworkSpeedActivity.class));
            }
        });

        mBinding.clMoreHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, MoreHelpActivity.class));
            }
        });

        mBinding.clPlacementAssistant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PlacementAssistantActivity.class));
            }
        });

        mBinding.btnSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SupportActivity.class));
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.troubleshooting_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(TroubleshootingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Troubleshooting.START_SPEED_TEST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Speed Test", resultStatus.errorMsg);
                        }
                        // 按了 rerun test 後, 文字會變成一直轉圈圈的 progress bar
                        // 即使 speed test command fail 也要更新資料, 不然 progress bar 會一直存在
                        setData();
                    }
                }
            }
        });
    }

    private void setData() {
        String routerDid = DataManager.getInstance().getControllerDid();
        SpeedTest speedTest = DataManager.getInstance().getSpeedTest(routerDid);
        int status = speedTest.getStatus();
        if (status != 0) {   // not in progress
            int download = speedTest.getDownload();
            int upload = speedTest.getUpload();
            String down = (download == -1) ? "--" : String.valueOf(download);
            String up = (upload == -1) ? "--" : String.valueOf(upload);
            String result = down + "Mbps Down, " + up + "Mbps Up";
            mBinding.tvInternetSpeedValue.setText(result);
            mBinding.tvRerunTest.setVisibility(View.VISIBLE);
            mBinding.pbRerunTest.setVisibility(View.INVISIBLE);
        } else {    // in progress
            mBinding.tvInternetSpeedValue.setText(getText(R.string.speed_in_progress));
            mBinding.tvRerunTest.setVisibility(View.INVISIBLE);
            mBinding.pbRerunTest.setVisibility(View.VISIBLE);
        }
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace(action);
            if (action.equals(AppConstants.EVENT_SPEED_TEST_UPDATE) ||
                    action.equals(AppConstants.EVENT_DEVICE_SPEED_TEST)) {
                setData();
            }
        }
    };
}
