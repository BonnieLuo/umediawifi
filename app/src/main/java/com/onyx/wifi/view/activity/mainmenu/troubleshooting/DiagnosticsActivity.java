package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityRunDiagnosticsBinding;
import com.onyx.wifi.databinding.FragmentDiagnosticsAfterFixIssueBinding;
import com.onyx.wifi.databinding.FragmentListDiagnosticsBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.UMEDIADeviceAdapter;
import com.onyx.wifi.view.customized.TitleBar;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.FWStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.UMEDIADevice;

import java.util.ArrayList;

enum VIEW_TYPE {
    RUN_DIAGNOSTICS,
    LIST_DIAGNOSTICS,
    DIAGNOSTICS_AFTER_FIX_ISSUE,
    INIT
}


//
// This activity is no-use now. But it is a sample of how to switch different views/layouts in one activity.
//
public class DiagnosticsActivity extends BaseMenuActivity implements UMEDIADeviceAdapter.OnItemClickHandler {

    private ArrayList<UMEDIADevice> mUMEDIADevice = new ArrayList<>();
    private UMEDIADeviceAdapter mAdapter;

    private ActivityRunDiagnosticsBinding mBinding_runDiagnostics;
    private FragmentListDiagnosticsBinding mBinding_listDiagnostics;
    private FragmentDiagnosticsAfterFixIssueBinding mBinding_diagnosticsAfterFixIssue;

    View view_runDiagnostics = null;
    View view_listDiagnostics = null;
    View view_AfterFixIssue = null;

    VIEW_TYPE mCurrentView = VIEW_TYPE.INIT;

    boolean mIsInitRunDiagnosticsView = false;
    boolean mIsInitListDiagnosticsView = false;
    boolean mIsInitAfterFixIssueView = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        // 切換畫面時傳入View才能紀錄畫面之前的操作結果
        //
        LayoutInflater inflater = LayoutInflater.from(this);
        view_runDiagnostics = inflater.inflate(R.layout.activity_run_diagnostics, null);
        view_listDiagnostics = inflater.inflate(R.layout.fragment_list_diagnostics, null);
        view_AfterFixIssue = inflater.inflate(R.layout.fragment_diagnostics_after_fix_issue, null);

        //
        // 每個畫面有自己的binding
        //
        mBinding_runDiagnostics = DataBindingUtil.bind(view_runDiagnostics);
        mBinding_listDiagnostics = DataBindingUtil.bind(view_listDiagnostics);
        mBinding_diagnosticsAfterFixIssue = DataBindingUtil.bind(view_AfterFixIssue);

        setData(); // init adapter, data change時再更新(mAdapter.setDevice(mUMEDIADevice);)
        setView_runDiagnostics();
    }

    private void setData() {
        loadVoiceDevices();
        mAdapter = new UMEDIADeviceAdapter(mContext, mUMEDIADevice, this);
    }

    private void setUpUMEDIADeviceRecycler_DiagnosticsList() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding_listDiagnostics.rvDiagnosticsList.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding_listDiagnostics.rvDiagnosticsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        //setData();
        mBinding_listDiagnostics.rvDiagnosticsList.setAdapter(mAdapter);
    }

    private void setUpUMEDIADeviceRecycler_AfterFixIssue() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding_diagnosticsAfterFixIssue.rvDiagnosticsList.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding_diagnosticsAfterFixIssue.rvDiagnosticsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        //setData();
        mBinding_diagnosticsAfterFixIssue.rvDiagnosticsList.setAdapter(mAdapter);
    }


    private void loadVoiceDevices() {
        mUMEDIADevice.add(new UMEDIADevice(AppConstants.DeviceType.TOWER, FWStatus.UMEDIA_FW_UP_TO_DATE, "Garage", " | v1.2.4", false));
        mUMEDIADevice.add(new UMEDIADevice(AppConstants.DeviceType.DESKTOP, FWStatus.UMEDIA_FW_UPDATE_AVAILABLE, "Bedroom", " | v1.1.9", false));
        mUMEDIADevice.add(new UMEDIADevice(AppConstants.DeviceType.TOWER, FWStatus.UMEDIA_FW_UP_TO_DATE, "Main Office", " | v1.2.4", true));
        mUMEDIADevice.add(new UMEDIADevice(AppConstants.DeviceType.DESKTOP, FWStatus.UMEDIA_FW_UPDATE_AVAILABLE, "Kid's Room", " | v1.2.4", false));
        mUMEDIADevice.add(new UMEDIADevice(AppConstants.DeviceType.PLUG, FWStatus.UMEDIA_FW_UP_TO_DATE, "Kid's Room", " | v1.2.4", false));
        mUMEDIADevice.add(new UMEDIADevice(AppConstants.DeviceType.PLUG, FWStatus.UMEDIA_FW_UP_TO_DATE, "Kid's Room", " | v1.2.4", false));
    }

    /////////////////////////////////////////////////////
    private void setView_runDiagnostics() {
        setContentView(view_runDiagnostics);
        mCurrentView = VIEW_TYPE.RUN_DIAGNOSTICS;

        // set titlebar and menubar for current view
        setTitleBar();
        setMenuBarForCurView();

        if (!mIsInitRunDiagnosticsView) {
            mBinding_runDiagnostics.btnRunDiagnostics.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setView_listDiagnostics();
                }
            });
            mIsInitRunDiagnosticsView = true;
        }
    }

    private void setView_listDiagnostics() {
        setContentView(view_listDiagnostics);
        mCurrentView = VIEW_TYPE.LIST_DIAGNOSTICS;
        //
        // notify data change for adapter
        //
        mAdapter.setDevice(mUMEDIADevice);
        //
        // show status
        //
        if (IsAnyDeviceHasIssue()) {
            mBinding_listDiagnostics.tvSatusValue.setText("Failed");
            mBinding_listDiagnostics.tvSatusValue.setTextColor(ContextCompat.getColor(mContext, R.color.red_cf0c0a));
        } else {
            mBinding_listDiagnostics.tvSatusValue.setText("Healthy");
            mBinding_listDiagnostics.tvSatusValue.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
        }

        // set titlebar and menubar for current view
        setTitleBar();
        setMenuBarForCurView();

        if (!mIsInitListDiagnosticsView) {
            setUpUMEDIADeviceRecycler_DiagnosticsList();

            mBinding_listDiagnostics.btnFixIssueAutomatically.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    setView_diagnosticsAfterFixIssue();
                }
            });

            mBinding_listDiagnostics.tvSupport.setClickable(true);
            mBinding_listDiagnostics.tvSupport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(mContext, SupportActivity.class));
                }
            });
            mIsInitListDiagnosticsView = true;
        }
    }

    private boolean IsAnyDeviceHasIssue() {
        if (mUMEDIADevice == null)
            return false;
        for (UMEDIADevice umediaDevice : mUMEDIADevice) {
            if (umediaDevice.getIsHasIssue())
                return true;
        }
        return false;
    }

    private void setView_diagnosticsAfterFixIssue() {
        boolean isHasIssue = false;
        setContentView(view_AfterFixIssue);
        mCurrentView = VIEW_TYPE.DIAGNOSTICS_AFTER_FIX_ISSUE;

        // test for data changing
        mUMEDIADevice.set(2, new UMEDIADevice(AppConstants.DeviceType.PLUG, FWStatus.UMEDIA_FW_UP_TO_DATE, "Bonnie's Room", " | v1.0.0", false));
        mUMEDIADevice.set(1, new UMEDIADevice(AppConstants.DeviceType.PLUG, FWStatus.UMEDIA_FW_UP_TO_DATE, "Chris's Room", " | v1.0.0", true));

        // notify data change for adapter
        mAdapter.setDevice(mUMEDIADevice);

        // show status
        if (IsAnyDeviceHasIssue()) {
            mBinding_diagnosticsAfterFixIssue.tvSatusValue.setText("Failed");
            mBinding_diagnosticsAfterFixIssue.tvSatusValue.setTextColor(ContextCompat.getColor(mContext, R.color.red_cf0c0a));
        } else {
            mBinding_diagnosticsAfterFixIssue.tvSatusValue.setText("Healthy");
            mBinding_diagnosticsAfterFixIssue.tvSatusValue.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
        }

        // set titlebar and menubar for current view
        setTitleBar();
        setMenuBarForCurView();

        if (!mIsInitAfterFixIssueView) {
            setUpUMEDIADeviceRecycler_AfterFixIssue();
            mIsInitAfterFixIssueView = true;
        }
    }

    @Override
    public void onBackPressed() {
        if (mCurrentView == VIEW_TYPE.RUN_DIAGNOSTICS) {
            finish();
        }

        if (mCurrentView == VIEW_TYPE.LIST_DIAGNOSTICS) {
            setView_runDiagnostics();
        }

        if (mCurrentView == VIEW_TYPE.DIAGNOSTICS_AFTER_FIX_ISSUE) {
            setView_runDiagnostics();
        }
    }

    private void setMenuBarForCurView() {
        if (mCurrentView == VIEW_TYPE.RUN_DIAGNOSTICS) {
            setMenuBar(mBinding_runDiagnostics.menuBar);
        }

        if (mCurrentView == VIEW_TYPE.LIST_DIAGNOSTICS) {
            setMenuBar(mBinding_listDiagnostics.menuBar);
        }

        if (mCurrentView == VIEW_TYPE.DIAGNOSTICS_AFTER_FIX_ISSUE) {
            setMenuBar(mBinding_diagnosticsAfterFixIssue.menuBar);
        }
    }

    private void setTitleBar() {
        TitleBar titleBar = null;
        if (mCurrentView == VIEW_TYPE.RUN_DIAGNOSTICS) {
            titleBar = mBinding_runDiagnostics.titleBar;
        }

        if (mCurrentView == VIEW_TYPE.LIST_DIAGNOSTICS) {
            titleBar = mBinding_listDiagnostics.titleBar;
        }

        if (mCurrentView == VIEW_TYPE.DIAGNOSTICS_AFTER_FIX_ISSUE) {
            titleBar = mBinding_diagnosticsAfterFixIssue.titleBar;
        }
        setImmerseLayout(titleBar);
        titleBar.setTitle(R.string.troubleshooting_title);
        titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    // 3. 點擊事件
    @Override
    public void onItemClick(UMEDIADevice umediaDevice) {
        Toast.makeText(this,
                "click " + umediaDevice.getName(), Toast.LENGTH_SHORT).show();
    }
}
