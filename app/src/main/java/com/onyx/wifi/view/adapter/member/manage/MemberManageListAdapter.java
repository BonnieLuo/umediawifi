package com.onyx.wifi.view.adapter.member.manage;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.view.activity.menubar.member.blocking.BlockingSiteListActivity;
import com.onyx.wifi.view.activity.menubar.member.client.ClientSettingsActivity;
import com.onyx.wifi.view.activity.menubar.member.schedule.InternetScheduleListActivity;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.MemberHeaderViewHolder;
import com.onyx.wifi.view.adapter.member.SectionViewHolder;
import com.onyx.wifi.view.interfaces.member.OnMemberChangedListener;

import java.util.List;

public class MemberManageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private FamilyMember mMember;

    private List<ListItem> mListItems;

    private View.OnClickListener mOnButtonClickListener;

    private OnMemberChangedListener mOnMemberChangedListener;

    public MemberManageListAdapter(FamilyMember member, List<ListItem> listItems, View.OnClickListener listener, OnMemberChangedListener memberChangedListener) {
        super();

        mMember = member;

        mListItems = listItems;

        mOnButtonClickListener = listener;

        mOnMemberChangedListener = memberChangedListener;
    }

    @Override
    public int getItemViewType(int position) {
        ListItem<MemberManageItemType, Object> listItem = mListItems.get(position);

        MemberManageItemType type = listItem.getType();

        switch (type) {
            case HEADER:
                return MemberManageItemType.HEADER.ordinal();
            case SECTION:
                return MemberManageItemType.SECTION.ordinal();
            case EMPTY:
                return MemberManageItemType.EMPTY.ordinal();
            case ITEM:
                return MemberManageItemType.ITEM.ordinal();
            case CONTENT_FILTERS:
                return MemberManageItemType.CONTENT_FILTERS.ordinal();
            case INTERNET_SCHEDULE:
                return MemberManageItemType.INTERNET_SCHEDULE.ordinal();
            default:
                return -1;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == MemberManageItemType.HEADER.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_editor_header, parent, false);

            return new MemberHeaderViewHolder(view);
        }

        if (viewType == MemberManageItemType.SECTION.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_section, parent, false);

            return new SectionViewHolder(view);
        }

        if (viewType == MemberManageItemType.EMPTY.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_empty_section, parent, false);

            return new SectionViewHolder(view);
        }

        if (viewType == MemberManageItemType.CONTENT_FILTERS.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_content_filters, parent, false);

            return new ContentFilterViewHolder(view);
        }

        if (viewType == MemberManageItemType.INTERNET_SCHEDULE.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_member_internet_schedule, parent, false);

            return new InternetScheduleViewHolder(view);
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_member_client_list_item, parent, false);

        return new DeviceListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem<MemberManageItemType, Object> listItem = mListItems.get(position);

        MemberManageItemType type = listItem.getType();

        switch (type) {
            case HEADER:
                setHeader(listItem, viewHolder);

                break;
            case SECTION:
            case EMPTY:
                setSection(listItem, viewHolder);

                break;
            case ITEM:
            default:
                setItem(listItem, viewHolder);

                break;
        }
    }

    private void setHeader(ListItem<MemberManageItemType, Object> viewModel, RecyclerView.ViewHolder viewHolder){
        FamilyMember member = (FamilyMember) viewModel.getItem();

        if (member != null) {
            MemberHeaderViewHolder headerViewHolder = (MemberHeaderViewHolder) viewHolder;

            headerViewHolder.setFamilyMember(member);
            headerViewHolder.setIconMoreAction();
            headerViewHolder.setOnButtonClickListener(mOnButtonClickListener);
            headerViewHolder.setOnMemberChangedListener(mOnMemberChangedListener);
        }
    }

    private void setSection(ListItem<MemberManageItemType, Object> viewModel, RecyclerView.ViewHolder viewHolder){
        String title = (String) viewModel.getItem();

        if (title != null) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) viewHolder;

            sectionViewHolder.setTitle(title);
        }
    }

    private void setItem(ListItem<MemberManageItemType, Object> viewModel, RecyclerView.ViewHolder viewHolder){
        Object data = viewModel.getItem();

        if (data != null) {
            Client client = (Client)data;

            DeviceListViewHolder deviceListViewHolder = (DeviceListViewHolder) viewHolder;

            deviceListViewHolder.setClient(client);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    class DeviceListViewHolder extends RecyclerView.ViewHolder {

        private Client mClient;

        private ImageView mIconImageView;

        private TextView mNameTextView;

        private TextView mLastOnlineTextView;

        private FrameLayout mPauseLayout;

        private ImageView mPauseImageView;

        private ProgressBar mProgressBar;

        private TextView mDurationTextView;

        private ImageView mLightImageView;

        private ImageView mArrowImageView;

        public DeviceListViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = this.itemView.findViewById(R.id.iconImageView);

            mNameTextView = this.itemView.findViewById(R.id.nameTextView);

            mLastOnlineTextView = this.itemView.findViewById(R.id.descriptionTextView);

            mPauseLayout = this.itemView.findViewById(R.id.pauseLayout);
            mPauseLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pauseDuration = mClient.getPauseDuration();

                    if (pauseDuration == 0) {
                        pauseDuration = 30;
                    } else if (pauseDuration == 30) {
                        pauseDuration = 60;
                    } else if (pauseDuration == 60) {
                        pauseDuration = 120;
                    } else if (pauseDuration == 120) {
                        pauseDuration = 480;
                    } else if (pauseDuration == 480) {
                        pauseDuration = 525600;
                    } else if (pauseDuration == 525600) {
                        pauseDuration = 0;
                    }

                    mClient.setPauseDuration(pauseDuration);
                }
            });

            mPauseImageView = this.itemView.findViewById(R.id.pauseImageView);

            mProgressBar = this.itemView.findViewById(R.id.progressBar);

            mDurationTextView = this.itemView.findViewById(R.id.durationTextView);

            mLightImageView = this.itemView.findViewById(R.id.lightImageView);

            mArrowImageView = this.itemView.findViewById(R.id.arrowImageView);
            mArrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataManager dataManager = DataManager.getInstance();
                    dataManager.setManageClient(mClient);

                    Context context = DeviceListViewHolder.this.itemView.getContext();
                    Intent intent = new Intent(context, ClientSettingsActivity.class);
                    context.startActivity(intent);
                }
            });
        }

        public void setClient(Client client) {
            mClient = client;

            setupIcon();

            setupName();

            setupConnectionLight();

            setupConnectionMessage();

            //setupPause();
        }

        private void setupIcon() {
            int iconId = mClient.getClientIcon();
            mIconImageView.setImageResource(iconId);
        }

        private void setupName() {
            String name = mClient.getName();
            mNameTextView.setText(name);
        }

        private void setupConnectionLight() {
            int lightId = mClient.getConnectionLight();
            mLightImageView.setImageResource(lightId);
        }

        private void setupConnectionMessage() {
            Context context = this.itemView.getContext();
            //String message = mClient.getConnectionMessage(context);
            String message = mClient.getLastOnlineTimeInfo();//.getConnectionMessage(context);
            mLastOnlineTextView.setText(message);
        }
    }

    double normalize(double value, double min, double max) {
        return ((value - min) / (max - min));
    }

    class ContentFilterViewHolder extends RecyclerView.ViewHolder {
        private Context mContext;

        private ImageView arrowImageView;

        public ContentFilterViewHolder(@NonNull View itemView) {
            super(itemView);

            mContext = this.itemView.getContext();

            arrowImageView = this.itemView.findViewById(R.id.arrowImageView);
            arrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = ContentFilterViewHolder.this.itemView.getContext();
                    Intent intent = new Intent(context, BlockingSiteListActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }

    class InternetScheduleViewHolder extends RecyclerView.ViewHolder {
        private Context mContext;

        private ImageView arrowImageView;

        public InternetScheduleViewHolder(@NonNull View itemView) {
            super(itemView);

            mContext = this.itemView.getContext();

            arrowImageView = this.itemView.findViewById(R.id.arrowImageView);
            arrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = InternetScheduleViewHolder.this.itemView.getContext();
                    Intent intent = new Intent(context, InternetScheduleListActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }
}
