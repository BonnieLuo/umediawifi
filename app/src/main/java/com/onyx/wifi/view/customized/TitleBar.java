package com.onyx.wifi.view.customized;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewTitleBarBinding;

public class TitleBar extends RelativeLayout {
    private Context mContext;
    private ViewTitleBarBinding mBinding;

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.view_title_bar, this, true);

        setView();
    }

    private void setView() {
        mBinding.btnLeft.setVisibility(View.INVISIBLE);
        mBinding.btnRight.setVisibility(View.INVISIBLE);
        mBinding.btnSignOut.setVisibility(View.INVISIBLE);
    }

    public void setTitle(int resId) {
        mBinding.tvTitle.setText(resId);
        mBinding.tvTitle.setVisibility(View.VISIBLE);
        mBinding.imgDashboardTitle.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String title) {
        mBinding.tvTitle.setText(title);
        mBinding.tvTitle.setVisibility(View.VISIBLE);
        mBinding.imgDashboardTitle.setVisibility(View.INVISIBLE);
    }

    public void setDashboardTitle() {
        mBinding.tvTitle.setVisibility(View.INVISIBLE);
        mBinding.imgDashboardTitle.setVisibility(View.VISIBLE);
    }

    // 為左按鈕設置 click listener (左按鈕圖示預設是返回鍵 ic_back_white, 並且已在 TitleBar.setView() 內被預設成不可見)
    public void setLeftButtonListener(View.OnClickListener listener) {
        mBinding.btnLeft.setOnClickListener(listener);
    }

    // 為左按鈕設置 icon 及 click listener, 並且設置為可見
    // (若 UI 設計的左按鈕圖示不是返回鍵, 使用此 function 可更換 icon)
    public void setLeftButton(int drawableId, View.OnClickListener listener) {
        mBinding.btnLeft.setVisibility(View.VISIBLE);
        mBinding.btnLeft.setImageResource(drawableId);
        mBinding.btnLeft.setOnClickListener(listener);
    }

    public void setLeftButtonVisibility(int visibility) {
        mBinding.btnLeft.setVisibility(visibility);
    }

    public void setLeftButtonSignOut(View.OnClickListener listener) {
        mBinding.btnSignOut.setVisibility(View.VISIBLE);
        mBinding.btnSignOut.setOnClickListener(listener);
    }

    public void setLeftButtonSignOutInvisible() {
        mBinding.btnSignOut.setVisibility(View.INVISIBLE);
    }

    // 為右按鈕設置 click listener (右按鈕圖示預設是確認鍵 ic_check, 並且已在 TitleBar.setView() 內被預設成不可見)
    public void setRightButtonListener(View.OnClickListener listener) {
        mBinding.btnRight.setOnClickListener(listener);
    }

    // 為右按鈕設置 icon 及 click listener, 並且設置為可見
    // (若 UI 設計的右按鈕圖示不是確認鍵, 使用此 function 可更換 icon)
    public void setRightButton(int drawableId, View.OnClickListener listener) {
        mBinding.btnRight.setVisibility(View.VISIBLE);
        mBinding.btnRight.setImageResource(drawableId);
        mBinding.btnRight.setOnClickListener(listener);
    }

    public void setRightButtonVisibility(int visibility) {
        mBinding.btnRight.setVisibility(visibility);
    }
}
