package com.onyx.wifi.view.activity.menubar.dashboard;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;

import com.google.firebase.auth.FirebaseUser;
import com.onyx.wifi.BuildConfig;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityDashboardBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.NodeSetInfo;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.model.item.member.PauseInfoModel;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.setup.repeater.RepeaterSetupActivity;
import com.onyx.wifi.view.activity.setup.router.RouterSetupActivity;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.DashboardViewModel;

public class DashboardActivity extends BaseMenuActivity {

    private ActivityDashboardBinding mBinding;
    private DashboardViewModel mViewModel;

    private NodeSetInfo mNodeSetInfo;

    private MessageDialog mUpdateAppDialog;
    private ConfirmDialog mExitAppDialog;

    private double mDuration = 0;
    private CountDownTimer mTimer;

    private boolean isFromCreate = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        setView();
        setViewModel();

        initDialog();

        isFromCreate = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        FirebaseUser user = mAccountManager.getUser();
        if (user != null && DataManager.getInstance().getUserInfo() != null) {
            if (checkAppVersion()) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(AppConstants.EVENT_DASHBOARD_INFO_UPDATE);
                intentFilter.addAction(AppConstants.EVENT_WIRELESS_SETTING_UPDATE);
                intentFilter.addAction(AppConstants.EVENT_SPEED_TEST_UPDATE);
                intentFilter.addAction(AppConstants.EVENT_GLOBAL_PAUSE_INFO_UPDATE);
                intentFilter.addAction(AppConstants.EVENT_DEVICE_INTERNET_STATUS);
                intentFilter.addAction(AppConstants.EVENT_DEVICE_SPEED_TEST);
                registerReceiver(mEventReceiver, intentFilter);

                mNodeSetInfo = mViewModel.getSavedNodeSetInfo();
                if (mNodeSetInfo == null) {
                    // share preference 儲存的 node set info 是空的, 代表手機本地端還沒有 node set 的資料, 但不代表使用者雲端的資料也沒有 node set
                    // 有可能使用者已經用其他手機做完 initial setup 了, 才在這台手機登入, 這時候手機本地端沒有 node set 的資料, 但雲端有
                    // 故當手機本地端沒有 node set 的資料時, 必須先打 cloud API, 看雲端資料的結果才能判斷是否需要進入 initial setup 流程

                    // 先顯示預設資料
                    setInitialData();

                    // 打 cloud API get dashboard info (node set info 包含在裡面)
                    mViewModel.getDashboardInfo(true);
                } else {
                    // 先顯示 share preference 儲存的資料
                    setData();

                    // 打 cloud API 更新資料, 並根據不同情況顯示/不顯示 loading 畫面
                    if (DataManager.getInstance().getAfterInitialSetup()) {
                        // initial setup 完成
                        mViewModel.getDashboardInfo(true);
                    } else if (isFromCreate) {
                        // 退出 App 再重新開啟
                        mViewModel.getDashboardInfo(true);
                        isFromCreate = false;
                    } else {
                        // 從 App 其他頁面進到 Dashboard
                        mViewModel.getDashboardInfo(false);
                    }
                }
            }
        } else {
            LogUtils.trace("Firebase user is null: " + (user == null) + ", user info is null: " + (DataManager.getInstance().getUserInfo() == null));
            mAccountManager.signOut();
            startLoginHomeActivity(this);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        cancelTimer();
    }

    private void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        MenuBar.MainMenuItem item = (MenuBar.MainMenuItem) intent.getSerializableExtra(AppConstants.EXTRA_MAIN_MENU_ITEM);
        if (item != null) {
            mMenuBar.performClickMainMenuItem(item);
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.imgSpeedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.imgSpeedTest.setVisibility(View.INVISIBLE);
                mBinding.imgSpeedTestAnim.setVisibility(View.VISIBLE);
                mViewModel.startSpeedTest();
            }
        });
        mBinding.imgRouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NodeSettingActivity.class);
                intent.putExtra(AppConstants.EXTRA_DID, DataManager.getInstance().getControllerDid());
                startActivity(intent);
            }
        });
        mBinding.btnNetworkMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, NetworkMapActivity.class));
            }
        });
        mBinding.btnDeviceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, NodeListActivity.class));
            }
        });
        mBinding.btnAddDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RepeaterSetupActivity.class));
            }
        });

        // Task #8583 (https://umedia.plan.io/issues/8583)
        // remove "Resetup" function/button
//        mBinding.btnResetupNetwork.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mContext, RouterSetupActivity.class);
//                intent.putExtra(AppConstants.EXTRA_RESETUP_ROUTER, true);
//                startActivity(intent);
//            }
//        });

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.HOME, true);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setDashboardTitle();
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Dashboard.GET_DASHBOARD_INFO) {
                        // 若 get dashboard 結果是失敗的, UI 會做 error handle, 例如:
                        // (1) 直接引導至 initial setup => 會離開 dashboard 頁面, 所以不需要繼續往下執行
                        // (2) 讓使用者選擇重打一次 get dashboard API => 等待使用者選擇, 所以也不需要繼續往下執行
                        // 只有 get dashboard 成功了才要繼續往下執行, 會打其他 cloud API 拿資料
                        if (resultStatus.success) {
                            mViewModel.getWirelessSetting();
                        } else {
                            int serverErrorCode = resultStatus.serverErrorCode;
                            if (serverErrorCode == 4010 || serverErrorCode == 4011 || serverErrorCode == 4012) {
                                String errorMsg = serverErrorCode + ": " + resultStatus.errorMsg + "\nYou need to sign in again.";
                                handleDashboardErrorWithSignOut(errorMsg);
                            } else {
                                String errorMsg = resultStatus.errorMsg;
                                if (errorMsg.equals(getString(R.string.err_internet_not_available))) {
                                    ConfirmDialog confirmDialog = new ConfirmDialog();
                                    confirmDialog.setTitle("Get Dashboard Info");
                                    confirmDialog.setContent(errorMsg);
                                    confirmDialog.setPositiveButton(getString(R.string.btn_retry), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mViewModel.getDashboardInfo(true);
                                            confirmDialog.dismiss();
                                        }
                                    });
                                    confirmDialog.setNegativeButton(getString(R.string.settings), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            confirmDialog.dismiss();
                                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    });
                                    showDialog(confirmDialog);
                                } else if (errorMsg.equals(getString(R.string.err_252)) || errorMsg.equals(getString(R.string.err_259))) {
                                    startInitialSetup();
                                } else if (errorMsg.equals(getString(R.string.err_258)) || errorMsg.equals(getString(R.string.err_unknown))) {
                                    handleDashboardErrorWithRetry(errorMsg);
                                } else {
                                    handleDashboardErrorWithRetry(errorMsg);
                                }
                            }
                        }
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.GET_WIRELESS_SETTING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Wireless Setting", resultStatus.errorMsg);
                        }
                        // 不論成功或失敗都要繼續往下執行, 打其他 cloud API 拿資料
                        // 因為資料彼此之間是獨立的, 不應該因為這支 API 失敗了就不打其他支 API
                        mViewModel.getSpeedTest();
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.GET_SPEED_TEST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Speed Test", resultStatus.errorMsg);
                        }
                        mViewModel.getGlobalPauseList();
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.GET_GLOBAL_PAUSE_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Global Pause List", resultStatus.errorMsg);
                        }
                        setData();
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.START_SPEED_TEST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Speed Test", resultStatus.errorMsg);
                        }
                        // 按了 rerun test 後, 文字會變成一直轉圈圈的 progress bar
                        // 即使 speed test command fail 也要更新資料, 不然 progress bar 會一直存在
                        setData();
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.DISABLE_GLOBAL_PAUSE) {
                        if (!resultStatus.success) {
                            showMessageDialog("Disable Global Pause", resultStatus.errorMsg);
                        } else {
                            // 成功 disable global pause, 儲存 enable = 0 的資料到 share preference
                            DataManager.getInstance().setPauseInfoModel(new PauseInfoModel());
                        }
                        setData();
                    }
                }
            }
        });
    }

    private void initDialog() {
        mUpdateAppDialog = new MessageDialog();
        mUpdateAppDialog.setTitle("Information");
        mUpdateAppDialog.setContent("Please update the UMEDIA App to the latest version at the Google Play Store.");
        mUpdateAppDialog.setOkButton("Update", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUpdateAppDialog.dismiss();
                String appPackageName = BuildConfig.APPLICATION_ID;
                try {
                    // Open app with Google Play app
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException e) {
                    // Open Google Play website
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        mUpdateAppDialog.setCancelable(false);

        mExitAppDialog = new ConfirmDialog();
        mExitAppDialog.setTitle("Exit App");
        mExitAppDialog.setContent("Are you sure to exit UMEDIA App?");
        mExitAppDialog.setPositiveButton(getString(R.string.btn_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExitAppDialog.dismiss();
                finish();
            }
        });
        mExitAppDialog.setNegativeButton(getString(R.string.btn_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExitAppDialog.dismiss();
            }
        });
    }

    // 檢查使用者現在的 App 版本, 若小於某個版本, 訊息提示強制使用者更新 App
    private boolean checkAppVersion() {
        boolean isAppVersionPassed = true;
        DashboardInfo dashboardInfo = DataManager.getInstance().getDashboardInfo();
        if (dashboardInfo != null) {
            int androidMinVersion = -1;
            try {
                // check App version
                androidMinVersion = Integer.valueOf(dashboardInfo.getAndroidMinVer());
                if (BuildConfig.VERSION_CODE < androidMinVersion) {
                    if (mUpdateAppDialog != null && !mUpdateAppDialog.isAdded()) {
                        showDialog(mUpdateAppDialog);
                    }
                    isAppVersionPassed = false;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return isAppVersionPassed;
    }

    // dashboard 畫面呈現預設資料, 待打 cloud API get Dashboard 資料
    private void setInitialData() {
        setNetworkName();

        mBinding.imgRouter.setVisibility(View.INVISIBLE);

        setRouterOffline();

        mBinding.clPauseContainer.setVisibility(View.INVISIBLE);
    }

    private void setNetworkName() {
        // network name
        // 若 dashboard node set 是空的, 會從 user info 抓取預設的 network name 來顯示
        String networkName = DataManager.getInstance().getNetworkName();
        mBinding.tvNetworkName.setText(networkName);
        if (networkName.length() > 20) {
            mBinding.tvNetworkName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        } else {
            mBinding.tvNetworkName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        }
    }

    private void setData() {
        if (checkAppVersion()) {
            if (mMenuBar != null) {
                mMenuBar.setData();
            }

            setNetworkName();

            mNodeSetInfo = mViewModel.getSavedNodeSetInfo();
            if (mNodeSetInfo == null) {
                startInitialSetup();
            } else {
                // 使用者已設定過 router, 顯示 router 的資料

                // router image
                DeviceInfo routerDeviceInfo = mNodeSetInfo.getDeviceInfo();
                switch (routerDeviceInfo.getDeviceType()) {
                    case DESKTOP:
                        mBinding.imgRouter.setImageResource(R.drawable.pic_hrn22ac);
                        break;

                    case PLUG:
                        mBinding.imgRouter.setImageResource(R.drawable.pic_hex22acp);
                        break;

                    case TOWER:
                    default:
                        mBinding.imgRouter.setImageResource(R.drawable.pic_hna22ac);
                        break;
                }
                mBinding.imgRouter.setVisibility(View.VISIBLE);

                if (mNodeSetInfo.getControllerOnline()) {
                    // router internet status: online
                    mBinding.tvInternet.setText(R.string.internet);
                    mBinding.imgInternetStatus.setImageResource(R.drawable.ic_internet_status_online);
                    mBinding.imgInternetStatus.setVisibility(View.VISIBLE);

                    // speed test
                    String routerDid = DataManager.getInstance().getControllerDid();
                    SpeedTest speedTest = DataManager.getInstance().getSpeedTest(routerDid);
                    int status = speedTest.getStatus();
                    if (status != 0) {   // not in progress
                        int download = speedTest.getDownload();
                        int upload = speedTest.getUpload();
                        mBinding.tvDownstreamSpeedValue.setText((download == -1) ? "--" : String.valueOf(download));
                        mBinding.tvUpstreamSpeedValue.setText((upload == -1) ? "--" : String.valueOf(upload));
                        mBinding.imgSpeedTest.setVisibility(View.VISIBLE);
                        mBinding.imgSpeedTestAnim.setVisibility(View.INVISIBLE);
                        mBinding.tvSpeedTime.setText(DateUtils.getSpeedTestDiffTimeString(speedTest.getTimestamp()));
                        mBinding.tvSpeedTime.setVisibility(View.VISIBLE);
                    } else {    // in progress
                        mBinding.tvDownstreamSpeedValue.setText("--");
                        mBinding.tvUpstreamSpeedValue.setText("--");
                        mBinding.imgSpeedTest.setVisibility(View.INVISIBLE);
                        mBinding.imgSpeedTestAnim.setVisibility(View.VISIBLE);
                        mBinding.tvSpeedTime.setText(getText(R.string.speed_in_progress));
                        mBinding.tvSpeedTime.setVisibility(View.VISIBLE);
                    }

                    // Task #8583 (https://umedia.plan.io/issues/8583)
                    // remove "Resetup" function/button
//                    mBinding.btnAddDevice.setVisibility(View.VISIBLE);
//                    mBinding.btnResetupNetwork.setVisibility(View.INVISIBLE);

                    // 設定部份元件的不透明度/顏色
                    mBinding.imgRouter.setImageAlpha(255);
                    mBinding.imgSpeed.setImageAlpha(255);
                    mBinding.imgDownstream.setImageAlpha(255);
                    mBinding.imgUpstream.setImageAlpha(255);
                    mBinding.tvDownstreamSpeedValue.setAlpha(1.0f);
                    mBinding.tvDownstreamSpeedUnit.setAlpha(1.0f);
                    mBinding.tvUpstreamSpeedValue.setAlpha(1.0f);
                    mBinding.tvUpstreamSpeedUnit.setAlpha(1.0f);
                    mBinding.btnAddDevice.setTextColor(getResources().getColor(R.color.white));
                    mBinding.btnAddDevice.setAlpha(1.0f);
                    mBinding.btnAddDevice.setEnabled(true);
                } else {
                    setRouterOffline();
                }
            }

            // global pause
            updateGlobalPauseUi();

            // 資料已更新完成, 清除 flag
            DataManager.getInstance().setAfterInitialSetup(false);
        }
    }

    private void setRouterOffline() {
        // router internet status: offline
        mBinding.tvInternet.setText(R.string.offline);
        mBinding.imgInternetStatus.setImageResource(R.drawable.ic_internet_status_offline_purple);
        mBinding.imgInternetStatus.setVisibility(View.VISIBLE);

        // speed test
        mBinding.tvDownstreamSpeedValue.setText("--");
        mBinding.tvUpstreamSpeedValue.setText("--");

        // 設定 speed test 部份元件不可見
        mBinding.imgSpeedTest.setVisibility(View.INVISIBLE);
        mBinding.imgSpeedTestAnim.setVisibility(View.INVISIBLE);
        mBinding.tvSpeedTime.setVisibility(View.INVISIBLE);

        // Task #8583 (https://umedia.plan.io/issues/8583)
        // remove "Resetup" function/button
//        mBinding.btnAddDevice.setVisibility(View.INVISIBLE);
//        mBinding.btnResetupNetwork.setVisibility(View.VISIBLE);

        // 設定部份元件的不透明度/顏色
        mBinding.imgRouter.setImageAlpha(90);   // 35% => 256 * 0.35 = 89.6
        mBinding.imgSpeed.setImageAlpha(128);   // 50%
        mBinding.imgDownstream.setImageAlpha(128);
        mBinding.imgUpstream.setImageAlpha(128);
        mBinding.tvDownstreamSpeedValue.setAlpha(0.5f);
        mBinding.tvDownstreamSpeedUnit.setAlpha(0.5f);
        mBinding.tvUpstreamSpeedValue.setAlpha(0.5f);
        mBinding.tvUpstreamSpeedUnit.setAlpha(0.5f);
        mBinding.btnAddDevice.setTextColor(getResources().getColor(R.color.purple_e487ff));
        mBinding.btnAddDevice.setAlpha(0.4f);
        mBinding.btnAddDevice.setEnabled(false);    // 當 router offline 時, 不能新增 device
    }

    private void updateGlobalPauseUi() {
        PauseInfoModel pauseInfoModel = DataManager.getInstance().getPauseInfoModel();
        if (pauseInfoModel != null) {
            PauseInfo pauseInfo = new PauseInfo(pauseInfoModel);
            if ("1".equals(pauseInfo.getType()) && pauseInfo.isEnabled() && pauseInfo.getMinuteLeft() > 0) {
                mBinding.clPauseContainer.setVisibility(View.VISIBLE);
                mBinding.clPauseContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewModel.disableGlobalPause(pauseInfoModel.getProfileId(), pauseInfoModel.getRuleId());
                    }
                });

                mDuration = (double) pauseInfo.getDuration();
                int minuteLeft = pauseInfo.getMinuteLeft();
                updateGlobalPauseProgressBar(minuteLeft * 1000 * 60);

                mBinding.tvPauseDuration.setText(pauseInfo.getDurationString());

                String pauseMessage = String.format(getString(R.string.global_pause_message), pauseInfo.getDurationString());
                mBinding.tvPauseMessage.setText(pauseMessage);
            } else {
                mBinding.clPauseContainer.setVisibility(View.INVISIBLE);
                cancelTimer();
            }
        } else {
            mBinding.clPauseContainer.setVisibility(View.INVISIBLE);
            cancelTimer();
        }
    }

    private void updateGlobalPauseProgressBar(long minuteLeftInMillis) {
        cancelTimer();

        mTimer = new CountDownTimer(minuteLeftInMillis, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                double value = mDuration - (double) millisUntilFinished / 1000 / 60;
                double normalizeValue = normalize(value, 0, mDuration);
                int progress = (int) (normalizeValue * 85);
                mBinding.pbPauseProgress.setProgress(progress);
            }

            @Override
            public void onFinish() {
                // 時間到代表 global pause 結束了, 儲存 enable = 0 的資料到 share preference
                DataManager.getInstance().setPauseInfoModel(new PauseInfoModel());
                updateGlobalPauseUi();
            }
        };
        mTimer.start();
    }

    private double normalize(double value, double min, double max) {
        return ((value - min) / (max - min));
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace(action);
            if (action.equals(AppConstants.EVENT_DASHBOARD_INFO_UPDATE) ||
                    action.equals(AppConstants.EVENT_WIRELESS_SETTING_UPDATE) ||
                    action.equals(AppConstants.EVENT_SPEED_TEST_UPDATE) ||
                    action.equals(AppConstants.EVENT_GLOBAL_PAUSE_INFO_UPDATE) ||
                    action.equals(AppConstants.EVENT_DEVICE_INTERNET_STATUS) ||
                    action.equals(AppConstants.EVENT_DEVICE_SPEED_TEST)) {
                setData();
            }
        }
    };

    private void handleDashboardErrorWithRetry(String errorMsg) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.setTitle("Get Dashboard Info");
        confirmDialog.setContent(errorMsg);
        confirmDialog.setPositiveButton(getString(R.string.btn_retry), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.getDashboardInfo(true);
                confirmDialog.dismiss();
            }
        });
        confirmDialog.setNegativeButton(getString(R.string.initial_setup_title), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInitialSetup();
            }
        });
        showDialog(confirmDialog);
    }

    private void handleDashboardErrorWithSignOut(String errorMsg) {
        MessageDialog messageDialog = new MessageDialog();
        messageDialog.setTitle("Get Dashboard Info");
        messageDialog.setContent(errorMsg);
        messageDialog.setOkButton(getString(R.string.btn_sign_out), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAccountManager.signOut();
                startLoginHomeActivity(DashboardActivity.this);
                finish();
            }
        });
        showDialog(messageDialog);
    }

    private void startInitialSetup() {
        if (checkAppVersion()) {
            Intent intent = new Intent(mContext, RouterSetupActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (mMenuBar.isMainMenuOpened()) {
            // 若有開啟 main menu 時, 按返回鍵關閉 main menu 就好, 不要結束頁面
            mMenuBar.closeMainMenu();
        } else if (isNotInLoadingStatus()) {
            // 頁面不處於 loading 狀態時才允許結束頁面
            if (mExitAppDialog != null && !mExitAppDialog.isAdded()) {
                showDialog(mExitAppDialog);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (BuildConfig.DEBUG) {
                // 若需要快速測試一些小功能, 可以寫在這裡, 但要注意測試完最好就刪除, 測試的 code 儘量不要 commit 上去
//                DateUtils.testTimezone();
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            return showAppInfo();
        }
        return super.onKeyDown(keyCode, event);
    }
}
