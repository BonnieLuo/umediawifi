package com.onyx.wifi.view.activity.mainmenu.accountsetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityChangeEmailBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.AccountSettingViewModel;

public class ChangeEmailActivity extends BaseMenuActivity {
    private ActivityChangeEmailBinding mBinding;
    private AccountSettingViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_email);
        setViewModel();
        setView();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(AccountSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.UPDATE_EMAIL) {
                        if (resultStatus.success) {
                            CommonUtils.setSharedPrefData(AppConstants.KEY_TEMP_NEW_EMAIL, mViewModel.getAccountSettingUser().getEmail());
                            CommonUtils.toast(mContext, getString(R.string.msg_send_verify_code_success));
                            startActivity(new Intent(mContext, AccountEmailVerificationActivity.class));
                            finish();
                        } else {
                            showMessageDialog("Update Email", resultStatus.errorMsg);
                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        });
    }

    private void checkEmailDataFilled() {
        if (mViewModel.isEmailDataFilled()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        mBinding.etNewEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getAccountSettingUser().setEmail(editable.toString().trim());
                checkEmailDataFilled();
            }
        });
        mBinding.etConfirmNewEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getAccountSettingUser().setConfirmEmail(editable.toString().trim());
                checkEmailDataFilled();
            }
        });
    }

//    @Override
//    public void onBackPressed() {
//        startAccountSettingActivity(this);
//        finish();
//    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.account_setting_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.isEmailDataValid()) {
                    mViewModel.updateEmail();
                } else {
                    showMessageDialog("Update Email", mViewModel.getInputErrorMsg());
                    mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
