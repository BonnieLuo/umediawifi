package com.onyx.wifi.view.adapter.advancedwireless;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.dhcp.DhcpClient;
import com.onyx.wifi.model.item.advancewireless.dhcp.DhcpClientModel;
import com.onyx.wifi.model.item.member.ConnectionStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DhcpTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<DhcpClientModel> mClientList = new ArrayList<>();

    public void setClientList(List<DhcpClientModel> clientList) {
        mClientList = clientList;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_dhcp_client_list, parent, false);

        DhcpClientViewHolder viewHolder = new DhcpClientViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        DhcpClientModel client = mClientList.get(position);
        DhcpClientViewHolder dhcpClientViewHolder = (DhcpClientViewHolder) viewHolder;
        dhcpClientViewHolder.setClient(client);
    }

    @Override
    public int getItemCount() {
        return mClientList.size();
    }

    class DhcpClientViewHolder extends RecyclerView.ViewHolder {

        DhcpClient mClient;

        ImageView mIconImageView;
        TextView mNameTextView;
        TextView mDescriptionTextView;
        ImageView mConnectionTypeImageView;
        ImageView mLightImageView;

        public DhcpClientViewHolder(@NonNull View itemView) {
            super(itemView);

            mIconImageView = itemView.findViewById(R.id.iconImageView);

            mNameTextView = itemView.findViewById(R.id.nameTextView);

            mDescriptionTextView = itemView.findViewById(R.id.descriptionTextView);

            mConnectionTypeImageView = itemView.findViewById(R.id.connectionTypeImageView);

            mLightImageView = itemView.findViewById(R.id.lightImageView);
        }

        public void setClient(DhcpClientModel clientModel) {
            mClient = new DhcpClient(clientModel);

            String name = mClient.getName();
            mNameTextView.setText(name);

            if (mClient.getOnlineStatus() == ConnectionStatus.ONLINE) {
                mNameTextView.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.purple_4e1393));
                setClientType(true);
            }else {
                mNameTextView.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.gray_828282));
                setClientType(false);
            }

            String ip = mClient.getIp();
            String mac = mClient.getMac();
            String leastTime = mClient.getLeastTime();
            String description = null;
            if (mClient.getOnlineStatus() == ConnectionStatus.ONLINE)
                description = String.format("%s %s %s", ip, mac, leastTime);
            else
                description = String.format("%s %s", ip, mac);

            mDescriptionTextView.setText(description);

            setConnectionType();

            setStatus();
        }

        public void setClientType(boolean isOnline) {
            String clientType = mClient.getType();

            DataManager dataManager = DataManager.getInstance();
            Map<String, Integer> deviceIconOnlineMap = dataManager.getOnlineClientIconMap();
            Map<String, Integer> deviceIconOfflineMap = dataManager.getOfflineClientIconMap();

            String lowercaseDeviceType = clientType.toLowerCase();
            Integer deviceIconId = deviceIconOnlineMap.get(lowercaseDeviceType);

            if (isOnline) {
                //int iconId = mClient.getClientIcon(ConnectionStatus.ONLINE);
                mIconImageView.setImageResource(deviceIconOnlineMap.get(lowercaseDeviceType));
            } else {
                //int iconId = mClient.getClientIcon(ConnectionStatus.OFFLINE);
                mIconImageView.setImageResource(deviceIconOfflineMap.get(lowercaseDeviceType));
            }
//            if (deviceIconId != null)
//                mIconImageView.setImageResource(deviceIconId);
        }

        public void setConnectionType() {
            int connectionTypeIcon = mClient.getConnectionType();

            mConnectionTypeImageView.setImageResource(connectionTypeIcon);
        }

        public void setStatus() {
            int light = mClient.getConnectionLight();

            mLightImageView.setImageResource(light);
        }
    }
}
