package com.onyx.wifi.view.activity.base;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class BaseTitleActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 設置沉浸式狀態列（透明狀態列, App 畫面背景延伸至狀態列的感覺）
     *
     * @param titleBar activity 頂部元件(標題列)
     */
    protected void setImmerseLayout(View titleBar) {
        // 先將狀態列透明化
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // 獲取狀態列的高度
            int statusBarHeight = getStatusBarHeight();

            // 將頂部空間的 top padding 設置為和狀態列一樣的高度, 以此達到預期的效果
            titleBar.setPadding(0, statusBarHeight, 0, 0);
        }
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = mContext.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
