package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewBleDeviceBinding;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

import java.util.ArrayList;

public class BleDeviceAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SetupDevice> mSetupDevices;

    private int mSelectedIndex = -1;

    public BleDeviceAdapter(Context context, ArrayList<SetupDevice> setupDevices) {
        mContext = context;
        mSetupDevices = setupDevices;
    }

    @Override
    public int getCount() {
        if (mSetupDevices != null) {
            return mSetupDevices.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mSetupDevices != null) {
            return mSetupDevices.get(position);
        } else {
            return new SetupDevice();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BleDeviceViewHolder holder;

        if (convertView == null) {
            ViewBleDeviceBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.view_ble_device, null, false);
            convertView = binding.getRoot();
            holder = new BleDeviceViewHolder();
            holder.imgChoice = binding.imgChoice;
            holder.imgDevice = binding.imgDevice;
            holder.tvModelNumber = binding.tvModelNumber;
            holder.tvDeviceName = binding.tvDeviceName;
            convertView.setTag(holder);
        } else {
            holder = (BleDeviceViewHolder) convertView.getTag();
        }

        // 透過 view holder 來為 item set data
        SetupDevice setupDevice = (SetupDevice) getItem(position);
        holder.tvDeviceName.setText(setupDevice.getName());
        holder.tvModelNumber.setText(setupDevice.getModelNumber());
        switch (setupDevice.getDeviceType()) {
            case DESKTOP:
                holder.imgDevice.setImageResource(R.drawable.pic_hrn22ac);
                break;

            case PLUG:
                holder.imgDevice.setImageResource(R.drawable.pic_hex22acp);
                break;

            case TOWER:
                holder.imgDevice.setImageResource(R.drawable.pic_hna22ac);
                break;
        }
        // 有被選擇到的 item 外框是紫色且有一個打勾符號
        if (position == mSelectedIndex) {
            convertView.setBackgroundResource(R.drawable.rec_purple);
            holder.imgChoice.setImageResource(R.drawable.ic_option_on);
        } else {
            convertView.setBackgroundResource(R.drawable.rec_gray);
            holder.imgChoice.setImageResource(R.drawable.ic_option_off);
        }

        return convertView;
    }

    private class BleDeviceViewHolder {
        ImageView imgChoice;
        ImageView imgDevice;
        TextView tvModelNumber;
        TextView tvDeviceName;
    }

    // 一開始所有 item 都沒有被選中, item 被選中後 view 長得不太一樣, 一旦選擇某個 item 後所有的 item view 要重畫
    // adapter 必須記住被選中的 item index, 以此更新 item view
    public void setSelectedIndex(int index) {
        mSelectedIndex = index;
    }
}
