package com.onyx.wifi.view.adapter.member.client;

public enum IconViewType {
    SECTION,
    ITEM,
    COPYRIGHT
}
