package com.onyx.wifi.view.fragment.setup.router;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentSetStaticIpBinding;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.setup.DeviceSetupViewModel;

public class SetStaticIpFragment extends BaseFragment {

    private FragmentSetStaticIpBinding mBinding;
    private DeviceSetupViewModel mViewModel;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_set_static_ip, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
    }

    private void setView() {
        mBinding.etStaticIpAddr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getStaticIpInfo().setIp(editable.toString().trim());
                checkStaticIpDataFilled();
            }
        });
        mBinding.etNetworkMask.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getStaticIpInfo().setMask(editable.toString().trim());
                checkStaticIpDataFilled();
            }
        });
        mBinding.etDefaultGateway.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getStaticIpInfo().setGateway(editable.toString().trim());
                checkStaticIpDataFilled();
            }
        });
        mBinding.etPrimaryDns.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getStaticIpInfo().setDns1(editable.toString().trim());
                checkStaticIpDataFilled();
            }
        });
        mBinding.etSecondaryDns.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getStaticIpInfo().setDns2(editable.toString().trim());
                checkStaticIpDataFilled();
            }
        });
        mBinding.btnChangeType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onContinue(Code.Action.DeviceSetup.CHANGE_CONNECTION_TYPE);
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mViewModel.isStaticIpDataValid()) {
                    mEventListener.onContinue(Code.Action.DeviceSetup.SET_STATIC_IP);
                } else {
                    showMessageDialog("Set Static IP", mViewModel.getInputErrorMsg());
                }
            }
        });
        // 一進入頁面時沒有輸入任何值, 所以 continue 的按鈕不能作用 (按鈕會是灰色的)
        mBinding.btnContinue.setEnabled(false);
        mEventListener.setSupportView(mBinding.tvSupport);
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(DeviceSetupViewModel.class);
    }

    private void checkStaticIpDataFilled() {
        if (mViewModel.isStaticIpDataFilled()) {
            mBinding.btnContinue.setEnabled(true);
        } else {
            mBinding.btnContinue.setEnabled(false);
        }
    }
}
