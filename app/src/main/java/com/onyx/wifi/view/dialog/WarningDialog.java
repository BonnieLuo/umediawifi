package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewWarningDialogBinding;

public class WarningDialog extends BaseDialog {

    private ViewWarningDialogBinding mBinding;

    private String mContent;
    private String mYesText;
    private View.OnClickListener mYesListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_warning_dialog, null, false);
        mBinding.tvContent.setText(mContent);
        mBinding.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBinding.btnYes.setText(mYesText);
        mBinding.btnYes.setOnClickListener(mYesListener);
        builder.setView(mBinding.getRoot());
        return builder.create();
    }

    @Override
    public String toString() {
        return "WarningDialog{ " +
                "content = \"" + mContent + "\"" +
                " }";
    }

    // TODO: to be removed. 因為 warning dialog 的 title 只有一種, 且已在 layout 裡設定好了, 不需要再設定
    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setYesButton(String text, View.OnClickListener listener) {
        mYesText = text;
        mYesListener = listener;
    }
}
