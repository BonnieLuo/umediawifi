package com.onyx.wifi.view.interfaces.member;

public interface OnClientTypeClickListener {
    void onClientTypeClick(String clientType);
}
