package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.DateUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(ClientModel clientModel);

        void onViewClick(int pos);

        // 提供onItemRemove做為移除項目的事件
        //void onItemRemove(int dataCount, UserType userType, int position);
        void onClientSelected();
    }

    private Context mContext;
    private boolean mbSelectable = false;
    public ArrayList<ClientModel> mData = new ArrayList<ClientModel>(); // note! 在此不要設成static，因為同一畫面有多個list用這個Adapter
    private boolean[] flag = new boolean[1000];//此處新增一個boolean型別的陣列
    private boolean mhasChangedSelect = false;
    //
    // 2. 宣告interface
    //
    private ClientAdapter.OnItemClickHandler mClickHandler;

    public ClientAdapter(
            Context context,
            ArrayList<ClientModel> data,
            ClientAdapter.OnItemClickHandler clickHandler,
            boolean bSelectable) {
        mContext = context;
        mData.addAll(data);
        mClickHandler = clickHandler;
        mbSelectable = bSelectable;
        mhasChangedSelect = false;

        // update flag[1000]
        /*for (int i = 0; i < data.size(); ++i) {
            if ("1".equals(data.get(i).getInternetPriority())) {
                flag[i] = true;
            } else {
                flag[i] = false;
            }
        }*/

        // test
//        if (data.size() > 2) {
//            flag[2] = true;
//        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private ImageView imgClientType;
        private TextView tvName;
        private TextView tvLastOnlineTime;
        ImageView mDefaultUserImageView;
        CardView mCardView;
        ImageView mPhotoImageView;
        private ImageView imgClientOnlineStatus;
        private CheckBox cbSelectHighInternetPriority;
        private ImageButton btnView;

        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            imgClientType = (ImageView) itemView.findViewById(R.id.imgClientType);// 從view(list item)中取得TextView實體
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvLastOnlineTime = (TextView) itemView.findViewById(R.id.tvLastOnlineTimestamp);
            mDefaultUserImageView = itemView.findViewById(R.id.defaultUserImageView);
            mCardView = itemView.findViewById(R.id.userCardView);
            mPhotoImageView = itemView.findViewById(R.id.photoImageView);
            imgClientOnlineStatus = (ImageView) itemView.findViewById(R.id.imgClientOnlineStatus);
            cbSelectHighInternetPriority = (CheckBox) itemView.findViewById(R.id.cbSelectHighInternetPriority);
            btnView = (ImageButton) itemView.findViewById(R.id.btnView);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClientModel clientModel = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(clientModel);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });

            if (btnView != null && mbSelectable) {
                btnView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mClickHandler.onViewClick(getAdapterPosition());
                    }
                });
            }

            // (x) 解決checkbox因為timer定期重畫造成勾選消失的問題 <= 寫在此無法解決
            //再設定一次CheckBox的選中監聽器，當CheckBox的選中狀態發生改變時，把改變後的狀態儲存在陣列中
//            cbSelectHighInternetPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                    flag[getAdapterPosition()] = b;
//                }
//            });
        }

        // copy start
        public void setOwnerPhoto(ClientModel clientInfo) {
            String ownerId = clientInfo.getOwnerMemberId();

            if (ownerId == null) {
                mDefaultUserImageView.setVisibility(View.INVISIBLE);
                mCardView.setVisibility(View.INVISIBLE);
                mPhotoImageView.setVisibility(View.INVISIBLE);

                return;
            }

            mDefaultUserImageView.setVisibility(View.VISIBLE);
            mCardView.setVisibility(View.VISIBLE);
            mPhotoImageView.setVisibility(View.VISIBLE);
            mPhotoImageView.setImageResource(0);

            Context context = itemView.getContext();
            Bitmap bitmap = getOwnerPhoto(context, ownerId);

            mPhotoImageView.setImageBitmap(bitmap);
//            if (bitmap != null) {
//                mPhotoImageView.setImageBitmap(bitmap);
//            }
        }

        public Bitmap getOwnerPhoto(Context context, String ownerId) {
            Bitmap bitmap = null;

            String avatarFileName = ownerId + ".jpg";
            File avatarFile = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), avatarFileName);

            if (avatarFile.exists()) {
                String filePath = avatarFile.getAbsolutePath();

                bitmap = decodeSampledBitmapFromResource(filePath, 200, 200);
            }

            return bitmap;
        }

        private Bitmap decodeSampledBitmapFromResource(String filePath, int reqWidth, int reqHeight) {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return getOrientationBitmap(filePath, options);
        }

        private int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

        public int resolveBitmapOrientation(File bitmapFile) throws IOException {
            ExifInterface exif = null;
            exif = new ExifInterface(bitmapFile.getAbsolutePath());

            return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        }

        public Bitmap applyOrientation(Bitmap bitmap, int orientation) {
            int rotate = 0;
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                default:
                    return bitmap;
            }

            int w = bitmap.getWidth();
            int h = bitmap.getHeight();
            Matrix mtx = new Matrix();
            mtx.postRotate(rotate);
            return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
        }

        public Bitmap getOrientationBitmap(String filePath,BitmapFactory.Options options) {
            Bitmap srcBmp01=BitmapFactory.decodeFile(filePath,options);
            try {
                int orientation = resolveBitmapOrientation(new File(filePath));
                srcBmp01 = applyOrientation(srcBmp01, orientation);
            } catch (Exception ex) {
                ;
            }

            return srcBmp01;
        }
        // copy end
    }

    // binding with layout
    @Override // required
    public ClientAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_networksettings_client_list_item, viewGroup, false); //取得list的view
        return new ClientAdapter.ViewHolder(view);
    }

    // set data (according to the status of each device)
    @Override // required
    public void onBindViewHolder(@NonNull ClientAdapter.ViewHolder viewHolder, int i) {
        //
        // client type
        //
        ClientModel clientModel = (ClientModel)mData.get(i);
        Client client= new Client(clientModel);
        int iconId = client.getClientIcon();
        viewHolder.imgClientType.setImageResource(iconId);
//
//        mIconImageView.setImageResource(iconId);
        //if (mData.get(i).getStatus() == 2) {
//        if (mData.get(i).getOnlineStatus().equals("1")) {
//            viewHolder.imgClientType.setImageResource(R.drawable.ic_mobile);
//
//        } else if (mData.get(i).getOnlineStatus().equals("0")) {
//            viewHolder.imgClientType.setImageResource(R.drawable.ic_mobile_offline);
//
//        }

        //
        //  Name
        //
        viewHolder.tvName.setText(mData.get(i).getName());

        //
        // last online time
        //
        // (mData.get(i).getStatus() == 1 || mData.get(i).getStatus() == 2) {
        if (mData.get(i).getOnlineStatus()== ConnectionStatus.ONLINE) {
            viewHolder.tvLastOnlineTime.setText("online");
            //} else {
        } else if (mData.get(i).getOnlineStatus()== ConnectionStatus.OFFLINE) {
            viewHolder.tvLastOnlineTime.setText(DateUtils.getTimeDiffToCurrent(mData.get(i).getLastOnlineTimestamp()));
        }

        // status 燈號
        //if (mData.get(i).getStatus() == 1) {
        //    viewHolder.imgClientOnlineStatus.setImageResource(R.drawable.ic_internet_status_weak);
        //} else if (mData.get(i).getStatus() == 2) {
        if (mData.get(i).getOnlineStatus() == ConnectionStatus.ONLINE) {
            viewHolder.imgClientOnlineStatus.setImageResource(R.drawable.ic_internet_status_online);
            //} else { // 0 or others
        } else if (mData.get(i).getOnlineStatus()== ConnectionStatus.OFFLINE) {
            viewHolder.imgClientOnlineStatus.setImageResource(R.drawable.ic_internet_status_offline_gray);
        }

        //
        // member icon
        //
        ClientAdapter.ViewHolder ViewHolder = (ClientAdapter.ViewHolder)viewHolder;
        ViewHolder.setOwnerPhoto(mData.get(i));

        //
        // checkbox or view button
        //
        if (mbSelectable) {
            viewHolder.cbSelectHighInternetPriority.setVisibility(View.VISIBLE);
            viewHolder.btnView.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.cbSelectHighInternetPriority.setVisibility(View.INVISIBLE);
            viewHolder.btnView.setVisibility(View.VISIBLE);
        }
        // 解決checkbox因為timer定期重畫造成勾選消失的問題
        viewHolder.cbSelectHighInternetPriority.setOnCheckedChangeListener(null);//先設定一次CheckBox的選中監聽器，傳入引數null
        viewHolder.cbSelectHighInternetPriority.setChecked(flag[i]);//用陣列中的值設定CheckBox的選中狀態
        viewHolder.cbSelectHighInternetPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mhasChangedSelect = true;
                flag[i] = b;
                mClickHandler.onClientSelected();
            }
        });
    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public boolean[] getCheckBoxRecord() {
        return flag;
    }

    public void setDevice(ArrayList<ClientModel> data) {
        //通过System.identityHashCode(object)方法来间接的获取内存地址；
        //创建出来的对象，只要没被销毁，内存地址始终不变。
        //Adapter绑定的数据源集合要为同一个集合，notifyDataSetChanged()方法才有效，否则需要重新设置数据源
        //mData = data; <-錯誤寫法，address可能會變更
        mData.clear();
        mData.addAll(data);

        /////////////////////////////////////////////////////////////
        // update flag[1000]
        if (!mhasChangedSelect) {
            for (int i = 0; i < data.size(); ++i) {
                if ("1".equals(data.get(i).getInternetPriority())) {
                    flag[i] = true;
                } else {
                    flag[i] = false;
                }
            }
        }

//        // test
//        if (data.size() > 1) {
//            flag[1] = true;
//        }

        /////////////////////////////////////////////////////////////

        //LogUtils.trace("NotifyMsg", "[Adapter]:" + data.size() + " " + mData.size());
        for (ClientModel clientInfo : mData) {
            //LogUtils.trace("NotifyMsg",clientInfo.toString());
            //System.out.println(clientInfo);
        }
        //LogUtils.trace("status", "address: " + System.identityHashCode(mData));
        notifyDataSetChanged();
    }

}
