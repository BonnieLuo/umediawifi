package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessDhcpTableBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.dhcp.DhcpClientModel;
import com.onyx.wifi.model.item.advancewireless.dhcp.DhcpTable;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.advancedwireless.DhcpTableAdapter;
import com.onyx.wifi.viewmodel.menubar.advancewireless.AdvanceWirelessSettingsViewModel;

import java.util.List;

public class DhcpTableActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessDhcpTableBinding mBinding;

    private AdvanceWirelessSettingsViewModel mViewModel;

    private DhcpTableAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_dhcp_table);

        mAdapter = new DhcpTableAdapter();

        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            setData();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("DHCP Table");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setData(){
        DataManager dataManager = DataManager.getInstance();
        AdvanceWirelessSettings advanceWirelessSettings = dataManager.getAdvanceWirelessSettings();

        if (advanceWirelessSettings == null) {
            return;
        }

        DhcpTable dhcpTable = advanceWirelessSettings.getDhcpTable();
        if (dhcpTable == null) {
            return;
        }

        List<DhcpClientModel> clientList = dhcpTable.getmClientList();

        if (clientList != null) {
            mAdapter.setClientList(clientList);
        }
    }
}
