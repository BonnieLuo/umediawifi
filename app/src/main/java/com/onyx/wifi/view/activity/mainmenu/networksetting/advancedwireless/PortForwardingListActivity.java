package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessPortForwardingListBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingList;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRuleModel;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.advancedwireless.PortForwardingListAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.AdvanceWirelessSettingsViewModel;

import java.util.List;

public class PortForwardingListActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessPortForwardingListBinding mBinding;

    private AdvanceWirelessSettingsViewModel mViewModel;

    private PortForwardingListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_port_forwarding_list);

        mAdapter = new PortForwardingListAdapter();

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            mViewModel.getAdvanceWirelessSettings();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Port Forwarding");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_add_rule, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, PortForwardingRuleActivity.class));
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(AdvanceWirelessSettingsViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.GET_SETTINGS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Advance Wireless Settings", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            JsonObject jsonObject = json.getAsJsonObject("data");

                            Gson gson = new Gson();

                            AdvanceWirelessSettings advanceWirelessSettings = gson.fromJson(jsonObject, AdvanceWirelessSettings.class);

                            DataManager dataManager = DataManager.getInstance();
                            dataManager.setAdvanceWirelessSettings(advanceWirelessSettings);

                            setData();
                        }
                    }
                }
            }
        });
    }

    private void setData(){
        DataManager dataManager = DataManager.getInstance();
        AdvanceWirelessSettings settings = dataManager.getAdvanceWirelessSettings();
        PortForwardingList portForwardingList = settings.getPortForwardingList();
        List<PortForwardingRuleModel> ruleModelList = portForwardingList.getPortForwardingRule();

        if (ruleModelList == null) {
            mBinding.hintTextView.setVisibility(View.VISIBLE);

            return;
        }

        if (ruleModelList.size() > 0) {
            mBinding.hintTextView.setVisibility(View.INVISIBLE);
        }else {
            mBinding.hintTextView.setVisibility(View.VISIBLE);
        }

        mAdapter.setRuleModelList(ruleModelList);
    }

}
