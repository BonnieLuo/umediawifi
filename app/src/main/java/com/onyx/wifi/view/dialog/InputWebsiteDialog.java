package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewInputWebsiteDialogBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.InputUtils;

import java.net.MalformedURLException;
import java.net.URL;

public class InputWebsiteDialog extends BaseDialog {

    private ViewInputWebsiteDialogBinding mBinding;

    private String mInputHint;
    private View.OnClickListener mPositiveListener;
    private View.OnClickListener mNegativeListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_input_website_dialog, null, false);
        mBinding.tvTitle.setText(mTitle);
        mBinding.etInput.setHint(mInputHint);
        mBinding.etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String input = editable.toString().trim();
                // 輸入值符合限制才能按 OK
                if (InputUtils.isNotEmpty(input) && (InputUtils.isUrlValid(input) || InputUtils.isKeywordValid(input))) {
                    setPositiveButtonEnable(true);
                } else {
                    // 輸入的字串 trim 完的結果是空字串, 或是有不合法的字元, 都不能按 OK
                    setPositiveButtonEnable(false);
                    if (InputUtils.isNotEmpty(input)) {
                        // 原本沒有這個 if 判斷時, 使用者如果一直按空白鍵, 會一直持續出現提示, 很奇怪, 所以改成不是空字串時才顯示提示
                        //CommonUtils.toast(getContext(), InputUtils.getInvalidNameMessage(mInputHint));
                    }
                }
            }
        });

        mBinding.etInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    try {
                        mBinding.etInput.setText(getKeywordOfDomainName());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    CommonUtils.toast(getContext(), "press OK!");
                }

                return false;
            }
        });

    mBinding.btnPositive.setOnClickListener(mPositiveListener);
        // 一開始還沒輸入任何值時, OK 的按鈕不能按
        setPositiveButtonEnable(false);
        mBinding.btnNegative.setOnClickListener(mNegativeListener);
        builder.setView(mBinding.getRoot());
        return builder.create();
    }

    @Override
    public String toString() {
        return "InputDialog{ " +
                "title = \"" + mTitle + "\"" +
                ", inputHint = \"" + mInputHint + "\"" +
                " }";
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
        // 要延遲一點時間 (等待 dialog 創建完成), EditText 才能正確取得焦點並跳出鍵盤
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CommonUtils.showKeyboard(getContext(), mBinding.etInput);
            }
        }, 100);
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setInputHint(String inputHint) {
        mInputHint = inputHint;
    }

    public void setPositiveListener(View.OnClickListener listener) {
        mPositiveListener = listener;
    }

    public void setNegativeListener(View.OnClickListener listener) {
        mNegativeListener = listener;
    }

    public String getKeywordOfDomainName() throws MalformedURLException {
        String site = mBinding.etInput.getText().toString().trim();
        URL url = new URL(site);
        String host = url.getHost(); // get domain name

//        if (host != null) {
//            if (host.indexOf("www.") != -1) {
//                host = host.replace("www.","");
//            } else if (host.indexOf("m.") != -1) {
//                host = host.replace("m.","");
//            }
//        }
        return host;
    }

    public String getInput() throws MalformedURLException {
        return mBinding.etInput.getText().toString().trim();
    }

    public void resetInput(String url) { // for url blocking
        mBinding.etInput.setText(url);
    }

    private void setPositiveButtonEnable(boolean isEnable) {
        if (isEnable) {
            mBinding.btnPositive.setTextColor(CommonUtils.getColor(getActivity(), R.color.purple_4e1393));
            mBinding.btnPositive.setEnabled(true);
        } else {
            mBinding.btnPositive.setTextColor(CommonUtils.getColor(getActivity(), R.color.gray_cccccc));
            mBinding.btnPositive.setEnabled(false);
        }
    }
}
