package com.onyx.wifi.view.activity.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySignUpBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.fragment.login.CreateAccountFragment;
import com.onyx.wifi.view.fragment.login.CreatePasswordFragment;
import com.onyx.wifi.viewmodel.interfaces.login.SignUpListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.login.SignUpViewModel;

public class SignUpActivity extends BaseActivity implements SignUpListener {

    private CreateAccountFragment mCreateAccountFragment;
    private CreatePasswordFragment mCreatePasswordFragment;

    private ActivitySignUpBinding mBinding;
    private SignUpViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mCreateAccountFragment = (CreateAccountFragment) mFragmentManager.findFragmentByTag(CreateAccountFragment.class.getName());
            mCreatePasswordFragment = (CreatePasswordFragment) mFragmentManager.findFragmentByTag(CreatePasswordFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            // 顯示第一頁, 隱藏其他頁
            if (mCreatePasswordFragment != null) {
                fragmentTransaction.hide(mCreatePasswordFragment);
            }
            fragmentTransaction.show(mCreateAccountFragment);
            fragmentTransaction.commit();
        } else {
            mCreateAccountFragment = new CreateAccountFragment();
            addFragmentAsFirstPage(R.id.flContainer, mCreateAccountFragment, CreateAccountFragment.class.getName());
        }
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.SIGN_UP) {
                        if (resultStatus.success) {
                            // 註冊成功, 進行 email 認證程序
                            Intent intent = new Intent(mContext, EmailVerificationActivity.class);
                            intent.putExtra(AppConstants.EXTRA_AUTO_RESEND_VERIFY_CODE, false);
                            intent.putExtra(AppConstants.EXTRA_USER_EMAIL, mViewModel.getSignUpUser().getEmail());
                            intent.putExtra(AppConstants.EXTRA_USER_PASSWORD, mViewModel.getSignUpUser().getPassword());
                            startActivity(intent);
                            finish();
                        } else {
                            showMessageDialog("Sign up", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onAccountDataValid() {
        mCreatePasswordFragment = new CreatePasswordFragment();
        addFragment(R.id.flContainer, mCreatePasswordFragment, CreatePasswordFragment.class.getName());
    }

    @Override
    public void onPasswordDataValid() {
        mViewModel.signUp();
    }
}
