package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMoreHelpBinding;
import com.onyx.wifi.utility.AppConstants;

public class MoreHelpActivity extends BaseMoreHelpActivity {

    private ActivityMoreHelpBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_more_help);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        setBottomLayout(mBinding.btnReturnToSetup, mBinding.tvTrouble, mBinding.tvSupport, mBinding.menuBar, mBinding.clContent);

        mBinding.clSetupTroubleshooting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SetupTroubleshootingActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, mFromDeviceSetup);
                startActivity(intent);
            }
        });
        mBinding.clProductLed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ProductLedActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, mFromDeviceSetup);
                startActivity(intent);
            }
        });
        mBinding.clGeneralQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, GeneralQuestionActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, mFromDeviceSetup);
                startActivity(intent);
            }
        });
        mBinding.clWirelessPerformanceQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, WirelessPerformanceQuestionActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, mFromDeviceSetup);
                startActivity(intent);
            }
        });
        mBinding.clProductPlacementQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ProductPlacementQuestionActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, mFromDeviceSetup);
                startActivity(intent);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.more_help_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
