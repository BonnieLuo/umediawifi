package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityProductLedBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.ListDisplayType;

public class ProductLedActivity extends BaseMoreHelpActivity {

    private ActivityProductLedBinding mBinding;

    private ListDisplayType mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
    private ListDisplayType mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_led);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        //
        // set partial color of some TextView content
        //
        setTextPartialColor(mBinding.tv1Solution1, mBinding.tv1Solution1.getText().toString(), "“Purple”", R.color.purple_7110b2);
        setTextPartialColor(mBinding.tv1Solution2, mBinding.tv1Solution2.getText().toString(), "“Purple”", R.color.purple_7110b2);
        setTextPartialColor(mBinding.tv1Solution3, mBinding.tv1Solution3.getText().toString(), "“Amber”", R.color.orange_e16842);
        setTextPartialColor(mBinding.tv1Solution4, mBinding.tv1Solution4.getText().toString(), "“Blue”", R.color.blue_1e88ff);
        //setTextPartialColor(mBinding.tv1Solution5, mBinding.tv1Solution5.getText().toString(), "“Blue”", R.color.blue_1e88ff);

        //
        // onclick handler
        //
        mBinding.ctv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ1DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        mBinding.ctv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON;
                } else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
                    mQ2DisplayType = ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON;
                }
                showByDisplayType();
            }
        });

        setBottomLayout(mBinding.btnReturnToSetup, mBinding.tvTrouble, mBinding.tvSupport, mBinding.menuBar, mBinding.clContent);
    }

    private void showByDisplayType() {
        // 1
        if (mQ1DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("+");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl1Solution.setVisibility(View.GONE);
        } else if (mQ1DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv1.setText("-");
            mBinding.tv1Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl1Solution.setVisibility(View.VISIBLE);
        }

        // 2
        if (mQ2DisplayType == ListDisplayType.SHOW_DISPLAY_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("+");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            mBinding.cl2Solution.setVisibility(View.GONE);
        } else if (mQ2DisplayType == ListDisplayType.SHOW_HIDE_EXTENSION_BUTTON) {
            mBinding.ctv2.setText("-");
            mBinding.tv2Question.setTextColor(CommonUtils.getColor(mContext, R.color.purple_7110b2));
            mBinding.cl2Solution.setVisibility(View.VISIBLE);
        }
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.more_help_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
