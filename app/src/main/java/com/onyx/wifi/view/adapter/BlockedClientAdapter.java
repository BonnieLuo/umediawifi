package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.utility.DateUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class BlockedClientAdapter extends RecyclerView.Adapter<BlockedClientAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(ClientModel clientModel);
        void onDelete(ClientModel clientModel);

        void onViewClick(int pos);

        // 提供onItemRemove做為移除項目的事件
        //void onItemRemove(int dataCount, UserType userType, int position);
        void onClientSelected();
    }

    private Context mContext;
    //private boolean mbSelectable = false;
    public ArrayList<ClientModel> mData = new ArrayList<ClientModel>(); // note! 在此不要設成static，因為同一畫面有多個list用這個Adapter
    private boolean[] flag = new boolean[1000];//此處新增一個boolean型別的陣列
    private boolean mhasChangedSelect = false;
    //
    // 2. 宣告interface
    //
    private BlockedClientAdapter.OnItemClickHandler mClickHandler;

    public BlockedClientAdapter(
            Context context,
            ArrayList<ClientModel> data,
            BlockedClientAdapter.OnItemClickHandler clickHandler) {
        mContext = context;
        mData.addAll(data);
        mClickHandler = clickHandler;
        mhasChangedSelect = false;

        // update flag[1000]
        /*for (int i = 0; i < data.size(); ++i) {
            if ("1".equals(data.get(i).getInternetPriority())) {
                flag[i] = true;
            } else {
                flag[i] = false;
            }
        }*/

        // test
//        if (data.size() > 2) {
//            flag[2] = true;
//        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private ImageView imgClientType;
        private ImageButton btnDelete;
        private TextView tvName;
        private TextView tvDisallowedTimestamp;


        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            imgClientType = (ImageView) itemView.findViewById(R.id.imgClientType);// 從view(list item)中取得TextView實體
            btnDelete = (ImageButton) itemView.findViewById(R.id.btnDelete);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDisallowedTimestamp = (TextView) itemView.findViewById(R.id.tvDisallowedTimestamp);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClientModel clientModel = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(clientModel);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClientModel clientModel = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onDelete(clientModel);
                }
            });
        }
    }

    // binding with layout
    @Override // required
    public BlockedClientAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_blocked_client_list_item, viewGroup, false); //取得list的view
        return new BlockedClientAdapter.ViewHolder(view);
    }

    // set data (according to the status of each device)
    @Override // required
    public void onBindViewHolder(@NonNull BlockedClientAdapter.ViewHolder viewHolder, int i) {
        //
        // client type
        //
        ClientModel clientModel = (ClientModel) mData.get(i);
        Client client = new Client(clientModel);
        int iconId = client.getClientIcon();
        viewHolder.imgClientType.setImageResource(iconId);

        //
        //  Name
        //
        viewHolder.tvName.setText(mData.get(i).getName());

        //
        // disallowed time
        //
        viewHolder.tvDisallowedTimestamp.setText(DateUtils.getDisallowedTimeDescription(mData.get(i).getDisallowedTimestamp()));


    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public boolean[] getCheckBoxRecord() {
        return flag;
    }

    public void setDevice(ArrayList<ClientModel> data) {
        //通过System.identityHashCode(object)方法来间接的获取内存地址；
        //创建出来的对象，只要没被销毁，内存地址始终不变。
        //Adapter绑定的数据源集合要为同一个集合，notifyDataSetChanged()方法才有效，否则需要重新设置数据源
        //mData = data; <-錯誤寫法，address可能會變更
        mData.clear();
        mData.addAll(data);

        /////////////////////////////////////////////////////////////
        // update flag[1000]
        if (!mhasChangedSelect) {
            for (int i = 0; i < data.size(); ++i) {
                if ("1".equals(data.get(i).getInternetPriority())) {
                    flag[i] = true;
                } else {
                    flag[i] = false;
                }
            }
        }

//        // test
//        if (data.size() > 1) {
//            flag[1] = true;
//        }

        /////////////////////////////////////////////////////////////

        //LogUtils.trace("NotifyMsg", "[Adapter]:" + data.size() + " " + mData.size());
        for (ClientModel clientInfo : mData) {
            //LogUtils.trace("NotifyMsg",clientInfo.toString());
            //System.out.println(clientInfo);
        }
        //LogUtils.trace("status", "address: " + System.identityHashCode(mData));
        notifyDataSetChanged();
    }

}
