package com.onyx.wifi.view.adapter.advancedwireless;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRule;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRuleModel;
import com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless.PortTriggeringRuleActivity;

import java.util.ArrayList;
import java.util.List;

public class PortTriggeringListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<PortTriggeringRule> mRuleList = new ArrayList<>();

    public void setRuleModelList(List<PortTriggeringRuleModel> ruleModelList) {
        mRuleList.clear();

        for (PortTriggeringRuleModel model:ruleModelList) {
            PortTriggeringRule rule = new PortTriggeringRule(model);
            mRuleList.add(rule);
        }

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_port_triggering_list_item, parent, false);

        RuleViewHolder viewHolder = new RuleViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        PortTriggeringRule rule = mRuleList.get(position);

        RuleViewHolder ruleViewHolder = (RuleViewHolder)viewHolder;
        ruleViewHolder.setRule(rule);
    }

    @Override
    public int getItemCount() {
        return mRuleList.size();
    }

    class RuleViewHolder extends RecyclerView.ViewHolder {

        PortTriggeringRule mRule;

        TextView mRuleNameTextView;

        TextView mRuleTextView;

        TextView mEnableTextView;

        ImageView mArrowImageView;

        public RuleViewHolder(@NonNull View itemView) {
            super(itemView);

            mRuleNameTextView = itemView.findViewById(R.id.nameTextView);

            mRuleTextView = itemView.findViewById(R.id.ruleTextView);

            mEnableTextView = itemView.findViewById(R.id.enableTextView);

            mArrowImageView = itemView.findViewById(R.id.arrowImageView);
            mArrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataManager dataManager = DataManager.getInstance();
                    dataManager.setPortTriggeringRule(mRule);

                    Context context = itemView.getContext();
                    context.startActivity(new Intent(context, PortTriggeringRuleActivity.class));
                }
            });
        }

        public void setRule(PortTriggeringRule rule) {
            mRule = rule;

            String ruleName = mRule.getName();
            mRuleNameTextView.setText(ruleName);

            String ruleDescription = mRule.getRuleDescription();
            mRuleTextView.setText(ruleDescription);

            if (mRule.isEnabled()) {
                mEnableTextView.setText("Enabled");
            } else {
                mEnableTextView.setText("Disabled");
            }
        }

    }
}
