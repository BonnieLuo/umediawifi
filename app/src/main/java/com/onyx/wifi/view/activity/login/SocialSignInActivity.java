package com.onyx.wifi.view.activity.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySocialSignInBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseActivity;
import com.onyx.wifi.view.customized.NoUnderlineSpan;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.login.SocialSignInViewModel;

import java.util.Arrays;

public class SocialSignInActivity extends BaseActivity {

    private static final int REQUEST_CODE_SIGN_IN_GOOGLE = 1;
    private static final int REQUEST_CODE_SIGN_IN_FACEBOOK = 64206; // Defined by Facebook

    private ActivitySocialSignInBinding mBinding;
    private SocialSignInViewModel mViewModel;

    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_social_sign_in);
        setView();
        setViewModel();
    }

    private void setView() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.btnSignInGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPrivacy() && checkInternetStatus()) {
                    signInWithGoogle();
                }
            }
        });

        final LoginManager fbLoginManager = LoginManager.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        fbLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LogUtils.trace("Facebook:onSuccess: " + loginResult);
                mViewModel.firebaseSignInWithFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                LogUtils.trace("Facebook:onCancel");
                mAccountManager.signOut();
            }

            @Override
            public void onError(FacebookException e) {
                LogUtils.trace("Facebook:onError: " + e.toString());
                showMessageDialog("Facebook Sign in", e.getMessage());
                mAccountManager.signOut();
            }
        });
        mBinding.btnSignInFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPrivacy() && checkInternetStatus()) {
                    fbLoginManager.setLoginBehavior(LoginBehavior.WEB_ONLY);
                    fbLoginManager.logInWithReadPermissions(SocialSignInActivity.this, Arrays.asList("email", "public_profile"));
                }
            }
        });

        setClickMessage();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SocialSignInViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.SOCIAL_SIGN_IN) {
                        if (resultStatus.success) {
                            afterSignInDone(SocialSignInActivity.this);
                        } else {
                            // social sign in 分兩步驟: (1) 先使用 Firebase SDK 登入 social account, (2) 成功後打我們的 cloud API social sign up
                            // 過程中有任何失敗都是登入失敗, 需將 App 的帳號登出, 以免帳號狀態錯誤而不小心錯誤進入 dashboard 畫面
                            mAccountManager.signOut();

                            showMessageDialog("Social Sign in", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void setClickMessage() {
        SpannableString msgCheck = new SpannableString(getString(R.string.check_privacy_policy));
        String msgService = getString(R.string.terms_of_service);
        String msgPrivacy = getString(R.string.privacy_policy);
        int startIndex, endIndex;
        NoUnderlineSpan urlSpan;

        // set hyperlink for terms of service
        startIndex = msgCheck.toString().indexOf(msgService);
        endIndex = startIndex + msgService.length();
        urlSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
//                Uri uri = Uri.parse(AppConstants.URL_TERMS_OF_SERVICE);
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
            }
        };
        msgCheck.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgCheck.setSpan(new ForegroundColorSpan(CommonUtils.getColor(mContext, R.color.white)), startIndex, endIndex, 0);

        // set hyperlink for privacy policy
        startIndex = msgCheck.toString().indexOf(msgPrivacy);
        endIndex = startIndex + msgPrivacy.length();
        urlSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
//                Uri uri = Uri.parse(AppConstants.URL_PRIVACY_POLICY);
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
            }
        };
        msgCheck.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgCheck.setSpan(new ForegroundColorSpan(CommonUtils.getColor(mContext, R.color.white)), startIndex, endIndex, 0);

        mBinding.tvPrivacy.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvPrivacy.setText(msgCheck, TextView.BufferType.SPANNABLE);
        mBinding.tvPrivacy.setHighlightColor(Color.TRANSPARENT);
    }

    private boolean checkPrivacy() {
        if (!mBinding.cbPrivacy.isChecked()) {
            showMessageDialog("Social Sign in", getString(R.string.err_privacy_not_agree));
            return false;
        }
        return true;
    }

    private void signInWithGoogle() {
        String clientId = mContext.getString(R.string.default_web_client_id);

        AppConstants.CloudServerMode cloudServerMode = DataManager.getInstance().getCloudServerMode();
        if (cloudServerMode == AppConstants.CloudServerMode.TEST || cloudServerMode == AppConstants.CloudServerMode.RDQA) {
            // - 如果 Android App 要使用 Google sign in 的話, 在 Firebase console 需要設定 Android App 的:
            //      (1) package name
            //      (2) signing keystore 的 SHA-1 fingerprint
            // - 對世界上所有的 Firebase project 來說, (1) & (2) 形成的 value pair 必須是唯一的,
            //   所以無法為不同的 Firebase project 設定同樣的 (1) & (2) value
            // - 若要讓不同的 Firebase project 使用同樣的 (1) & (2) value 做 Google sign in,
            //   請參考: https://firebase.googleblog.com/2016/12/working-with-multiple-firebase-projects-in-an-android-app.html
            //   其中的 "Authenticating to two different Firebase Databases" 這段
            clientId = mContext.getString(R.string.firebase_project_web_client_id_rdqa);
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(clientId)
                .requestEmail()
                .build();
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(mContext, gso);
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_SIGN_IN_GOOGLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SIGN_IN_GOOGLE) {
            mViewModel.firebaseSignInWithGoogle(data);
        } else if (requestCode == REQUEST_CODE_SIGN_IN_FACEBOOK) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
