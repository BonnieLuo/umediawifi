package com.onyx.wifi.view.activity.mainmenu.accountsetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAccountEmailVerificationBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.view.interfaces.TextChangedListener;
import com.onyx.wifi.view.listener.PinCodeOnKeyListener;
import com.onyx.wifi.view.listener.PinCodeTextWatcher;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.AccountSettingViewModel;

public class AccountEmailVerificationActivity extends BaseMenuActivity {
    private ActivityAccountEmailVerificationBinding mBinding;
    private AccountSettingViewModel mViewModel;
    private EditText[] mEtDigits;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_account_email_verification);
        setViewModel();
        setView();
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(AccountSettingViewModel.class);
        // 需紀錄上個頁面的 new email
        mViewModel.getAccountSettingUser().setEmail(CommonUtils.getSharedPrefData(AppConstants.KEY_TEMP_NEW_EMAIL));
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.UserAccount.VERIFY_UPDATE_EMAIL) {
                        if (resultStatus.success) {
                            CommonUtils.toast(mContext, getString(R.string.msg_verify_code_success));
                            final MessageDialog messageDialog = new MessageDialog();
                            messageDialog.setTitle("Change Email Successfully");
                            messageDialog.setContent("Please sign in again.");
                            messageDialog.setOkButton(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    messageDialog.dismiss();
                                    mAccountManager.signOut();
                                    startLoginHomeActivity(AccountEmailVerificationActivity.this);
                                    finish();
                                }
                            });
                            showDialog(messageDialog);
                        } else {
                            showMessageDialog("Verify Updated Email", resultStatus.errorMsg);
                            mBinding.btnResend.setVisibility(View.VISIBLE);
                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        });
    }

    private TextChangedListener mTextChangedListener = new TextChangedListener() {
        @Override
        public void afterTextChangedDone() {
            String verificationCode = "";
            for (EditText etDigit : mEtDigits) {
                verificationCode = verificationCode + etDigit.getText().toString();
            }
            if (verificationCode.length() == 4) {
                mBinding.btnResend.setVisibility(View.INVISIBLE);

                mViewModel.setVerificationCode(verificationCode);
                LogUtils.trace("verification code = " + verificationCode);
                mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
//                mBinding.titleBar.setRightButton(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        mViewModel.verifyUpdateEmail();
//                    }
//                });

            } else {
                mBinding.btnResend.setVisibility(View.VISIBLE);
            }
        }
    };

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mEtDigits = new EditText[]{mBinding.etDigit1, mBinding.etDigit2, mBinding.etDigit3, mBinding.etDigit4};
        mBinding.etDigit1.addTextChangedListener(new PinCodeTextWatcher(AccountEmailVerificationActivity.this, mEtDigits, 0, mTextChangedListener));
        mBinding.etDigit2.addTextChangedListener(new PinCodeTextWatcher(AccountEmailVerificationActivity.this, mEtDigits, 1, mTextChangedListener));
        mBinding.etDigit3.addTextChangedListener(new PinCodeTextWatcher(AccountEmailVerificationActivity.this, mEtDigits, 2, mTextChangedListener));
        mBinding.etDigit4.addTextChangedListener(new PinCodeTextWatcher(AccountEmailVerificationActivity.this, mEtDigits, 3, mTextChangedListener));
        mBinding.etDigit1.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 0));
        mBinding.etDigit2.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 1));
        mBinding.etDigit3.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 2));
        mBinding.etDigit4.setOnKeyListener(new PinCodeOnKeyListener(mEtDigits, 3));

        mBinding.btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.updateEmail();
            }
        });

    }

//    @Override
//    public void onBackPressed() {
//        // 出現強制認證 email 頁面即代表 user 已經不合法了, 若退出該頁面則需將 user 登出.
//        startAccountSettingActivity(this);
//        finish();
//    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.account_setting_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewModel.verifyUpdateEmail();
            }
        });
    }
}
