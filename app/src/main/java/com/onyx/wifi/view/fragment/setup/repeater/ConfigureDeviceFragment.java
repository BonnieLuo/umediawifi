package com.onyx.wifi.view.fragment.setup.repeater;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentConfigureDeviceBinding;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;

public class ConfigureDeviceFragment extends BaseFragment {

    private FragmentConfigureDeviceBinding mBinding;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_configure_device, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // 一開始看不到進度, 到開始 get provision state 時再顯示進度
        mBinding.tvProgress.setText("");
    }

    public void updateProgress(int progress) {
        LogUtils.trace(progress + "%");
        mBinding.tvProgress.setText(progress + "%");
    }
}
