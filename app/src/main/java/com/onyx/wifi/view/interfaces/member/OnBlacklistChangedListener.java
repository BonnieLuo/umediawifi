package com.onyx.wifi.view.interfaces.member;

public interface OnBlacklistChangedListener {

    void onAddWebsiteBlocking();

    void onRemoveWebsiteBlocking(String site);

}
