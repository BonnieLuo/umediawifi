package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.AudioDevice;

import java.util.ArrayList;
import java.util.List;

public class DisconnectAlexaAdapter extends RecyclerView.Adapter<DisconnectAlexaAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(AudioDevice audioDevice);

        void enableButton(int position);

        void diableButton();

        void onMuteUnMuteClick(String muteUnMute, AudioDevice audioDevice);
    }

    private Context mContext;
    public static ArrayList<AudioDevice> mData; // "public static": global data for related pages(activities)
    private int mPosition = -1;
    private int mSelectedPos = -1;//(1) 保存当前选中的position 重点！
    private int mMutePosition = -1;
    private boolean[] flag = new boolean[1000];//此處新增一個boolean型別的陣列

    //
    // 2. 宣告interface
    //
    private DisconnectAlexaAdapter.OnItemClickHandler mClickHandler;

    public DisconnectAlexaAdapter(Context context, ArrayList<AudioDevice> data, DisconnectAlexaAdapter.OnItemClickHandler clickHandler) {
        mContext = context;
        mData = data;
        mClickHandler = clickHandler;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private CheckBox cbIsAlexaDisConnect;
        private ImageView imgIsDeviceConnect;
        private TextView tvUMEDIAVoice;
        private TextView tvName;
        private ImageView imgIsMute;

        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            cbIsAlexaDisConnect = (CheckBox) itemView.findViewById(R.id.cbIsAlexaConnect);
            imgIsDeviceConnect = (ImageView) itemView.findViewById(R.id.imgIsDeviceConnect); // 從view(list item)中取得TextView實體
            tvUMEDIAVoice = (TextView) itemView.findViewById(R.id.tvUMEDIAVoice);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            imgIsMute = (ImageView) itemView.findViewById(R.id.imgIsMute);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AudioDevice audioDevice = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(audioDevice);
                }
            });

            cbIsAlexaDisConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPosition = getAdapterPosition();
                    LogUtils.trace("Pos", "position" + mPosition);
                }
            });
            cbIsAlexaDisConnect.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = getAdapterPosition();
                    if (isChecked) {  // 至少有一個item被選取
                        if(mSelectedPos!=position) {//(2) 当前选中的position和上次选中不是同一个position 执行
                            cbIsAlexaDisConnect.setChecked(true);
                            if (mSelectedPos != -1) {//判断是否有效 -1是初始值 即无效 第二个参数是Object 随便传个int 这里只是起个标志位
                                notifyItemChanged(mSelectedPos, 0); //(2) 主要call 此function更新 (by onBindViewHolder)
                            }
                            mSelectedPos = position;//(2)
                        }
                        mClickHandler.enableButton(position);//getAdapterPosition()
                    } else {
                        //mSelectedPos = -1; // <=沒有checkbox被選中(for單選)，要記得恢復預設值

                        mClickHandler.diableButton();
                    }

                    LogUtils.trace("position: " + position + " " +(mSelectedPos==position));
                }
            });

            imgIsMute.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    AudioDevice audioDevice = mData.get(getAdapterPosition());
                    mMutePosition = getAdapterPosition();
                    // 4. 呼叫interface的method
                    if (audioDevice.getIsMute()) {
                        LogUtils.trace("Bonnie_mute", "[Mute -> UnMute]");
                        mClickHandler.onMuteUnMuteClick("0", audioDevice);
                        //imgIsMute.setImageResource(R.drawable.ic_voice_mic);
                    } else {
                        LogUtils.trace("Bonnie_mute", "[UnMute -> Mute]");
                        mClickHandler.onMuteUnMuteClick("1", audioDevice);
                        //imgIsMute.setImageResource(R.drawable.ic_voice_privacy);
                    }
                }
            });
        }
    }

    // binding with layout
    @Override // required
    public DisconnectAlexaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_disconnect_alexa_list_item, viewGroup, false); //取得list的view
        return new DisconnectAlexaAdapter.ViewHolder(view);
    }

     //
     // draw each item (according to the status of each device)
     //

    // required
    @Override
    public void onBindViewHolder(@NonNull DisconnectAlexaAdapter.ViewHolder viewHolder, int i) {
        // 解決checkbox因為timer定期重畫造成勾選消失的問題
        viewHolder.cbIsAlexaDisConnect.setOnCheckedChangeListener(null);//先設定一次CheckBox的選中監聽器，傳入引數null
        viewHolder.cbIsAlexaDisConnect.setChecked(flag[i]);//用陣列中的值設定CheckBox的選中狀態
        viewHolder.cbIsAlexaDisConnect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                flag[i] = b;
            }
        });
    }
    //
    // 為實作checkbox單選
    //
    @Override
    public void onBindViewHolder(@NonNull DisconnectAlexaAdapter.ViewHolder viewHolder, final int position,@NonNull List payloads) {
        if(payloads.isEmpty()){ //(3) payloads即有效负载，当首次加载或调用notifyDatasetChanged() ,notifyItemChange(int position)进行刷新时，payloads为empty 即空
            if (mData.get(position).getIsDeviceConnect()) { // device is connect
                if (mData.get(position).getIsAlexaConnect()) { // Alexa is connect
                    viewHolder.imgIsMute.setVisibility(View.VISIBLE);
                    viewHolder.cbIsAlexaDisConnect.setVisibility(View.VISIBLE);
                    if (mData.get(position).getIsMute())
                        viewHolder.imgIsMute.setImageResource(R.drawable.ic_voice_privacy);
                    else
                        viewHolder.imgIsMute.setImageResource(R.drawable.ic_voice_mic);
                }
                viewHolder.imgIsDeviceConnect.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
                viewHolder.tvUMEDIAVoice.setTextColor(ContextCompat.getColor(mContext, R.color.gray_828282));
                viewHolder.tvUMEDIAVoice.setText("UMEDIA Voice");//viewHolder.tvUMEDIAVoice.setText(mData.get(position).getName());
                viewHolder.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
                viewHolder.tvName.setText(mData.get(position).getLocation());

            } else {                                 // device is not connect
                viewHolder.imgIsDeviceConnect.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
                viewHolder.imgIsDeviceConnect.setImageAlpha(0x80);
                viewHolder.tvUMEDIAVoice.setTextColor(ContextCompat.getColor(mContext, R.color.gray_cccccc));
                viewHolder.tvUMEDIAVoice.setText("UMEDIA Voice");//viewHolder.tvUMEDIAVoice.setText(mData.get(position).getName());
                viewHolder.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.gray_cccccc));
                viewHolder.tvName.setText(mData.get(position).getLocation());
                viewHolder.imgIsMute.setVisibility(View.INVISIBLE);
            }
            viewHolder.cbIsAlexaDisConnect.setChecked(mSelectedPos==position); // (3 - 1) 記得加！！否則更新內容勾勾會消失
        } else { //当调用notifyItemChange(int position, Object payload)进行布局刷新时，payloads不会empty ，所以真正的布局刷新应该在这里实现 重点！
            viewHolder.cbIsAlexaDisConnect.setChecked(mSelectedPos==position); // (3 - 2)
            mClickHandler.enableButton(position); //記得加這個，否則checkbox跟button狀態不能sync
            LogUtils.trace("position: " + position + " " +(mSelectedPos==position));
        }
    }

    public int getMuteUnMutePos() {
        return mMutePosition;
    }

    //提供给外部Activity来获取当前checkBox选中的item，这样就不用去遍历了 重点！
    public int getSelectedPos(){
        return mPosition; //用setOnClickListener取位置，setOnCheckedChangeListener為了達到一次選一個的目標，位置並非準確
    }

    public void resetSelectedRecord() {
        mSelectedPos = -1;
        mPosition = -1;
    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public void setDevice(ArrayList<AudioDevice> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void setInit() {
        mPosition = -1;
        mSelectedPos = -1;
    }
}
