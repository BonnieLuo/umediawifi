package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.onyx.wifi.utility.StringUtils;

public class BaseDialog extends DialogFragment {

    protected String mTitle = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            if (this instanceof OptionListDialog) {
                // 若是 option list dialog, 可以點擊對話框以外的部份取消對話框
                dialog.setCanceledOnTouchOutside(true);
            } else {
                // 若是其他 dialog 則不能取消對話框, 以免使用者還沒看完訊息但卻不小心點到其他地方而關閉了對話框
                dialog.setCanceledOnTouchOutside(false);
            }

            Window window = dialog.getWindow();
            if (window != null) {
                // 設置背景透明
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    // 有時呼叫 DialogFragment.show() 會發生 IllegalStateException : Can not perform this action after onSaveInstanceSate
    // 為解決此問題, override show() function
    // 請參考: https://www.jianshu.com/p/f6570ce9e413
    @Override
    public void show(FragmentManager manager, String tag) {
        // 有時呼叫 BaseActivity.showDialog() 會發生 java.lang.IllegalStateException: Fragment already added
        // 為解決此問題, 通過 isAdded() 判斷 fragment 是否 add, 同時通過 tag 獲取 fragment, 判斷 Fragment 是否為空
        // 雙重判斷, 若符合條件才 add fragment
        // 請參考: https://blog.csdn.net/k393393/article/details/78875838
        if (!isAdded() && manager.findFragmentByTag(tag) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } else {
            try {
                // 在 add 前先 remove, 防止連續的 add
                manager.beginTransaction().remove(this).commit();
                super.show(manager, tag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getTitle() {
        if (StringUtils.isNullOrEmpty(mTitle)) {
            return "Dialog";
        } else {
            return mTitle;
        }
    }
}
