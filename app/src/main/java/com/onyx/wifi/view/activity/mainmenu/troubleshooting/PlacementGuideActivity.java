package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityPlacementGuideBinding;

public class PlacementGuideActivity extends BaseMoreHelpActivity {

    private ActivityPlacementGuideBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_placement_guide);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        setBottomLayout(mBinding.btnReturnToSetup, mBinding.tvTrouble, mBinding.tvSupport, mBinding.menuBar, mBinding.clContent);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Placement Assistant");
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
