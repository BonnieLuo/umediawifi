package com.onyx.wifi.view.fragment.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentCreateAccountBinding;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.login.SignUpListener;
import com.onyx.wifi.viewmodel.login.SignUpViewModel;

public class CreateAccountFragment extends BaseFragment {

    private FragmentCreateAccountBinding mBinding;
    private SignUpViewModel mViewModel;

    private SignUpListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignUpListener) {
            mEventListener = (SignUpListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement SignUpListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_account, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        mBinding.etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignUpUser().setFirstName(editable.toString().trim());
                checkAccountDataFilled();
            }
        });
        mBinding.etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignUpUser().setLastName(editable.toString().trim());
                checkAccountDataFilled();
            }
        });
        mBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignUpUser().setEmail(editable.toString().trim());
                checkAccountDataFilled();
            }
        });
        mBinding.etConfirmEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getSignUpUser().setConfirmEmail(editable.toString().trim());
                checkAccountDataFilled();
            }
        });
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onBack();
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mViewModel.isAccountDataValid()) {
                    mEventListener.onAccountDataValid();
                } else {
                    showMessageDialog("Create Account", mViewModel.getInputErrorMsg());
                }
            }
        });
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(SignUpViewModel.class);
    }

    private void checkAccountDataFilled() {
        if (mViewModel.isAccountDataFilled()) {
            mBinding.btnContinue.setVisibility(View.VISIBLE);
        } else {
            mBinding.btnContinue.setVisibility(View.INVISIBLE);
        }
    }

    private void setData() {
        mBinding.etFirstName.setText(mViewModel.getSignUpUser().getFirstName());
        mBinding.etLastName.setText(mViewModel.getSignUpUser().getLastName());
        mBinding.etEmail.setText(mViewModel.getSignUpUser().getEmail());
        mBinding.etConfirmEmail.setText(mViewModel.getSignUpUser().getConfirmEmail());
        checkAccountDataFilled();
    }
}
