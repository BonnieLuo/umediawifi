package com.onyx.wifi.view.fragment.setup.router;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentConnectToModemBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;

public class ConnectToModemFragment extends BaseFragment {

    private FragmentConnectToModemBinding mBinding;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_connect_to_modem, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
    }

    private void setView() {
        SpannableString msgSubTitle = new SpannableString(getString(R.string.connect_to_modem_subtitle));
        String msgYellow = "yellow";
        int startIndex = msgSubTitle.toString().indexOf(msgYellow);
        int endIndex = startIndex + msgYellow.length();
        msgSubTitle.setSpan(new ForegroundColorSpan(CommonUtils.getColor(mActivity, R.color.yellow_dfc231)), startIndex, endIndex, 0);
        mBinding.tvSubTitle.setText(msgSubTitle);

        mBinding.btnMoreHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onContinue(Code.Action.DeviceSetup.MORE_HELP);
            }
        });
        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEventListener.onContinue(Code.Action.DeviceSetup.PLUG_IN_MODEM);
            }
        });
        mEventListener.setSupportView(mBinding.tvSupport);
    }
}
