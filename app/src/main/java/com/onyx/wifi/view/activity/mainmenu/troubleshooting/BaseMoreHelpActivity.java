package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.setup.AlternativeSetupActivity;
import com.onyx.wifi.view.activity.setup.repeater.RepeaterSetupActivity;
import com.onyx.wifi.view.activity.setup.router.RouterSetupActivity;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.customized.NoUnderlineSpan;

public class BaseMoreHelpActivity extends BaseMenuActivity {

    protected boolean mFromDeviceSetup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* 有兩種路徑可開啟 More Help 頁面:
           (1) Troubleshooting 頁面的 More Help 項目、Support 頁面的 More Help 按鈕
                => 頁面下方 "不要顯示" Return to Setup 按鈕及 Click here for support 文字、"要顯示" menu bar
           (2) Device setup 過程中頁面的 More Help 按鈕
                => 頁面下方 "要顯示" Return to Setup 按鈕及 Click here for support 文字、"不要顯示" menu bar
           預設值 false 代表的是 (1) 的情況*/
        mFromDeviceSetup = getIntent().getBooleanExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, false);
        LogUtils.trace("from device setup = " + mFromDeviceSetup);
    }

    protected void setBottomLayout(Button btnReturnToSetup, TextView tvTrouble, TextView tvSupport,
                                   MenuBar menuBar, ConstraintLayout clContent) {
        if (mFromDeviceSetup) {
            btnReturnToSetup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDeviceSetup();
                }
            });
            setSupportClickMessage(tvSupport);

            menuBar.setVisibility(View.GONE);
            ConstraintLayout.LayoutParams newLayoutParams = (ConstraintLayout.LayoutParams) clContent.getLayoutParams();
            newLayoutParams.bottomMargin = 0;
            clContent.setLayoutParams(newLayoutParams);
        } else {
            btnReturnToSetup.setVisibility(View.GONE);
            tvTrouble.setVisibility(View.GONE);
            tvSupport.setVisibility(View.GONE);
        }
    }

    // 必須與 BaseDeviceSetupActivity 的 retryDeviceSetup() 內容相同
    protected void retryDeviceSetup() {
        Intent intent;
        switch (DataManager.getInstance().getSetupMode()) {
            case ROUTER:
                if (DataManager.getInstance().getSetupFlow() == AppConstants.SetupFlow.BLE) {
                    intent = new Intent(mContext, RouterSetupActivity.class);
                } else {
                    intent = new Intent(mContext, AlternativeSetupActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            case ROUTER_RESETUP:
                // resetup router 時只能透過 BLE 的方式
                intent = new Intent(mContext, RouterSetupActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            case REPEATER:
            default:
                if (DataManager.getInstance().getSetupFlow() == AppConstants.SetupFlow.BLE) {
                    intent = new Intent(mContext, RepeaterSetupActivity.class);
                } else {
                    intent = new Intent(mContext, AlternativeSetupActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    // 必須與 BaseDeviceSetupActivity 的 setSupportClickMessage() 內容相同
    protected void setSupportClickMessage(TextView textView) {
        textView.setClickable(true);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SupportActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    protected void setTextPartialColor(TextView textView, String totalString, String partialString, int id) {
        // Set hyperlink of forgot password
        SpannableString sTotalString = new SpannableString(totalString);
        String sPartialString = new String(partialString);
        int startIndex = sTotalString.toString().indexOf(sPartialString);
        int endIndex = startIndex + sPartialString.length();

        sTotalString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, id)), startIndex, endIndex, 0);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(sTotalString, TextView.BufferType.SPANNABLE);
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    protected void enableSupportLink(TextView textView, String totalString, int id) {
        SpannableString sTotalString = new SpannableString(totalString);
        final String strWebsite = getString(R.string.support_content_2_website);
        // website
        int startIndex = sTotalString.toString().indexOf(strWebsite);
        int endIndex = startIndex + strWebsite.length();
        NoUnderlineSpan urlSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(strWebsite);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        };
        sTotalString.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        sTotalString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, id)), startIndex, endIndex, 0);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(sTotalString, TextView.BufferType.SPANNABLE);
        textView.setHighlightColor(Color.TRANSPARENT);
    }

}
