package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.onyx.wifi.R;

import java.util.ArrayList;
import java.util.List;

public class SearchTimezoneAdapter extends RecyclerView.Adapter<SearchTimezoneAdapter.ViewHolder> implements Filterable {

    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(boolean anySelected, String selectedTimezone);
        // 提供onItemRemove做為移除項目的事件
        //void onItemRemove(int dataCount, UserType userType, int position);
    }

    private Context mContext;
    public static ArrayList<String> mData; // "public static": global data for related pages(activities)
    public static ArrayList<String> mDataFull;
    private int mSelected = -1;
    //
    // 2. 宣告interface
    //
    private SearchTimezoneAdapter.OnItemClickHandler mClickHandler;
    public SearchTimezoneAdapter(Context context, ArrayList<String> data, SearchTimezoneAdapter.OnItemClickHandler clickHandler){
        mContext = context;
        mData = data; // 根據搜索內容有所不同
        mDataFull = new ArrayList<>(data);
        mClickHandler = clickHandler;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        //
        // list item所含的widgets
        //
        private TextView tvItemContent;
        private CheckBox cbSelected;

        ViewHolder(View itemView){
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            tvItemContent = (TextView)itemView.findViewById(R.id.tvItemContent);
            cbSelected = (CheckBox)itemView.findViewById(R.id.cbSelected);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mSelected == getAdapterPosition()) {
                        mSelected = -1;
                        mClickHandler.onItemClick(false, null);
                    } else {
                        mSelected = getAdapterPosition();
                        String selectedTimezone = mData.get(getAdapterPosition());
                        mClickHandler.onItemClick(true, selectedTimezone);
                    }

                    notifyDataSetChanged();
                    // 4. 呼叫interface的method

                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    @Override
    public SearchTimezoneAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_string_list_item, viewGroup, false); //取得list的view
        return new SearchTimezoneAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchTimezoneAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tvItemContent.setText(mData.get(i));

        if(i == mSelected) {
            viewHolder.tvItemContent.setTextColor(ContextCompat.getColor(mContext, R.color.purple_4e1393));
            viewHolder.cbSelected.setVisibility(View.VISIBLE);
            viewHolder.cbSelected.setChecked(true);
        } else {
            viewHolder.tvItemContent.setTextColor(ContextCompat.getColor(mContext, R.color.gray_828282));
            viewHolder.cbSelected.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    ///////////////////////////////////////////////////
    // Search Filter
    ///////////////////////////////////////////////////

    @Override
    public Filter getFilter() {
        return dataFilter;
    }

    private Filter dataFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<String> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) { // no input in searchView
                filteredList.addAll(mDataFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (String item : mDataFull) {
                    if (item.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mData.clear();
            mData.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
