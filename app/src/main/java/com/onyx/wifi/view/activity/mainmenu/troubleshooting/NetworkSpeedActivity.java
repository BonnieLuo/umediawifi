package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNetworkSpeedBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.mainmenu.networksetting.ReleaseNotesActivity;
import com.onyx.wifi.view.adapter.NetworkSpeedTestAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.NodeSpeedTestInfo;
import com.onyx.wifi.viewmodel.mainmenu.TroubleshootingViewModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class NetworkSpeedActivity extends BaseMenuActivity implements NetworkSpeedTestAdapter.OnItemClickHandler {
    private NetworkSpeedTestAdapter mAdapter;
    private SwipeRefreshLayout laySwipe;
    private ActivityNetworkSpeedBinding mBinding;
    private TroubleshootingViewModel mViewModel;
    private Timer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_network_speed);
        setViewModel();
        //mViewModel.getFirmwareStatus();
        mViewModel.getNodeList();
        //mViewModel.getSpeedTestInfo();
        initNodeSpeedTestInfoList();
        setView();
    }

    private void setUpNodeSpeedTestInfoRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        mBinding.rvSpeedTestInfoList.setLayoutManager(new LinearLayoutManager(this));
        // 設置格線
        mBinding.rvSpeedTestInfoList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // 將資料交給adapter
        // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
        if (mAdapter == null) {
            //LogUtils.trace("AdapterList", "new Adapter" + mViewModel.getFWStatusList().size());
            mAdapter = new NetworkSpeedTestAdapter(mContext, mViewModel.getNodeSpeedTestList(), this);
        }
        mBinding.rvSpeedTestInfoList.setAdapter(mAdapter);
    }

    void startTimer() {
        if (mTimer == null) {
            GetListTimerTask getListTimerTask = new GetListTimerTask();
            mTimer = new Timer();
            mTimer.schedule(getListTimerTask, 10L, 2000L);
        }
    }

    void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void initNodeSpeedTestInfoList() {
        startTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initNodeSpeedTestInfoList();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.EVENT_DEVICE_FIRMWARE_UPDATE);
        intentFilter.addAction(AppConstants.EVENT_DEVICE_FIRMWARE_UPDATE_COMPLETED);
        registerReceiver(mEventReceiver, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(TroubleshootingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Troubleshooting.GET_NODE_LIST) {
                        if (resultStatus.success) {
                            LogUtils.trace("NotifyMsg", "Get List success~~~~~~~~~~~~~~");
                            updateList();

                        } else {
                            showMessageDialog("Get Firmware Status", resultStatus.errorMsg);
                        }
                    }
                    if (resultStatus.actionCode == Code.Action.Troubleshooting.GET_SPEED_TEST_INFO) {

                    }
                }
            }
        });
    }

    private void updateList() {
        if (mAdapter != null) {
            //LogUtils.trace("NotifyMsg", "[updateFWList]" + mViewModel.getFWStatusList().size());
            ArrayList<NodeSpeedTestInfo> data = new ArrayList<NodeSpeedTestInfo>();
            data.addAll(mViewModel.getNodeSpeedTestList());
            mAdapter.setDevice(data); //在此處直接用mViewModel.getFWStatusList()，在setDevice裡會變成null
            mBinding.rvSpeedTestInfoList.setAdapter(mAdapter);
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        //
        // List
        //
        setUpNodeSpeedTestInfoRecycler();

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Network Speed");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    // 3. 點擊事件
    @Override
    public void onItemClick(NodeSpeedTestInfo nodeSpeedTestInfo) {

    }

    private class GetListTimerTask extends TimerTask {

        @Override
        public void run() {

            // TODO
            mViewModel.getNodeList();
            //mViewModel.getSpeedTestInfo();
        }
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            } else if (action == AppConstants.EVENT_DEVICE_FIRMWARE_UPDATE) {
                String did = intent.getStringExtra("DID");
                String fwStatus = intent.getStringExtra("FW_STATUS");

//                mViewModel.UpdateFWStatusList(did, fwStatus, null, null);
//                updateFWList();
//
//                if (fwStatus.equals("1")) { // updating (0,2,3)以外的正常狀態，恢復timer <= 現在沒有timer
//                    startTimer();
//                    mViewModel.getFirmwareStatus();
//                }
//                if (fwStatus.equals("-1")) {
//                    showMessageDialog("Update Unsuccessfully", "Please retry.");
//                    LogUtils.trace("file corrupted");
//                    mViewModel.getFirmwareStatus();
//                } else if (fwStatus.equals("-2")) {
//                    showMessageDialog("Update Unsuccessfully", "Please retry.");
//                    LogUtils.trace("update fail");
//                    mViewModel.getFirmwareStatus();
//                }

            } else if (action == AppConstants.EVENT_DEVICE_FIRMWARE_UPDATE_COMPLETED) {
//                String currentFW = intent.getStringExtra("CURRENT_FW");
//                String updateFW = intent.getStringExtra("UPDATE_FW");
//                String did = intent.getStringExtra("DID");
//                String fwStatus = intent.getStringExtra("FW_STATUS");
//
//                mViewModel.UpdateFWStatusList(did, fwStatus, currentFW, updateFW);
//                updateFWList();
//
//                if (fwStatus.equals("1")) { // updating以外的狀態，恢復timer <= 現在沒有timer
//                    startTimer();
//                    mViewModel.getFirmwareStatus();
//                }
//                if (fwStatus.equals("-1")) {
//                    showMessageDialog("Update Unsuccessfully", "Please retry.");
//                    LogUtils.trace("file corrupted");
//                    mViewModel.getFirmwareStatus();
//                } else if (fwStatus.equals("-2")) {
//                    showMessageDialog("Update Unsuccessfully", "Please retry.");
//                    LogUtils.trace("update fail");
//                    mViewModel.getFirmwareStatus();
//                }

            }
        }
    };

}

