package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewNodeListItemBinding;
import com.onyx.wifi.model.item.DeviceInfo;

import java.util.ArrayList;

public class NodeAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<DeviceInfo> mNodeList = new ArrayList<>();

    public NodeAdapter(Context context) {
        mContext = context;
    }

    public void setNodeList(ArrayList<DeviceInfo> nodeList) {
        mNodeList = nodeList;
    }

    @Override
    public int getCount() {
        if (mNodeList != null) {
            return mNodeList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mNodeList != null) {
            return mNodeList.get(position);
        } else {
            return new DeviceInfo();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NodeViewHolder viewHolder;

        if (convertView == null) {
            ViewNodeListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.view_node_list_item, null, false);
            convertView = binding.getRoot();
            viewHolder = new NodeViewHolder();
            viewHolder.imgDevice = binding.imgDevice;
            viewHolder.imgInternetStatus = binding.imgInternetStatus;
            viewHolder.tvDeviceLabel = binding.tvDeviceLabel;
            viewHolder.tvDeviceType = binding.tvDeviceType;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NodeViewHolder) convertView.getTag();
        }

        // 透過 view holder 來為 item set data
        DeviceInfo node = (DeviceInfo) getItem(position);
        switch (node.getDeviceType()) {
            case DESKTOP:
                viewHolder.imgDevice.setImageResource(R.drawable.pic_hrn22ac);
                break;

            case PLUG:
                viewHolder.imgDevice.setImageResource(R.drawable.pic_hex22acp);
                break;

            case TOWER:
            default:
                viewHolder.imgDevice.setImageResource(R.drawable.pic_hna22ac);
                break;
        }
        viewHolder.tvDeviceType.setText(node.getDeviceType().toString());

        if (node.getOnline()) {
            viewHolder.imgInternetStatus.setImageResource(R.drawable.ic_internet_status_online);
        } else {
            viewHolder.imgInternetStatus.setImageResource(R.drawable.ic_internet_status_offline_purple);
        }

        viewHolder.tvDeviceLabel.setText(node.getDeviceLabel());

        return convertView;
    }

    private class NodeViewHolder {
        ImageView imgDevice;
        ImageView imgInternetStatus;
        TextView tvDeviceLabel;
        TextView tvDeviceType;
    }
}
