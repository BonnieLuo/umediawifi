package com.onyx.wifi.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.UMEDIAFWStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.FWStatus;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;

public class UMEDIAFWAdapter extends RecyclerView.Adapter<UMEDIAFWAdapter.ViewHolder> {
    //
    // 1. 建立interface，命名為OnItemClickHandler，並在裡面寫好我們要發生的事件
    //
    public interface OnItemClickHandler {
        // 提供onItemClick方法作為點擊事件，括號內為接受的參數
        void onItemClick(UMEDIAFWStatus umediafwStatus);

        void onViewClick(int pos);
        // 提供onItemRemove做為移除項目的事件
        //void onItemRemove(int dataCount, UserType userType, int position);
    }

    private Context mContext;
    public static ArrayList<UMEDIAFWStatus> mData = new ArrayList<UMEDIAFWStatus>(); // "public static": global data for related pages(activities)
    //
    // 2. 宣告interface
    //
    private UMEDIAFWAdapter.OnItemClickHandler mClickHandler;

    public UMEDIAFWAdapter(
            Context context,
            ArrayList<UMEDIAFWStatus> data,
            UMEDIAFWAdapter.OnItemClickHandler clickHandler) {
        mContext = context;
        mData = data;
        mClickHandler = clickHandler;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //
        // list item所含的widgets
        //
        private ImageView imgUMEDIADevice;
        private TextView tvName;
        private TextView tvFWVersion;
        private TextView tvFWStatus;
        private TextView tvViewUpdate;
        private ImageButton btnViewUpdate;
        private GifImageView pbUpdating;
        private TextView tvUpdating;

        ViewHolder(View itemView) {
            //
            // 檢查view(List item)是否合法
            //
            super(itemView);
            //
            // 從view(list item)取得所有widget(for control purpose)
            //
            imgUMEDIADevice = (ImageView) itemView.findViewById(R.id.imgUMEDIADevice);// 從view(list item)中取得TextView實體
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvFWVersion = (TextView) itemView.findViewById(R.id.tvFWVersion);
            tvFWStatus = (TextView) itemView.findViewById(R.id.tvFWStatus);
            tvViewUpdate = (TextView) itemView.findViewById(R.id.tvViewUpdate);
            btnViewUpdate = (ImageButton) itemView.findViewById(R.id.btnReleaseNote);
            pbUpdating = (GifImageView) itemView.findViewById(R.id.pbUpdating);
            tvUpdating = (TextView) itemView.findViewById(R.id.tvUpdating);

            // 點擊list item時
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UMEDIAFWStatus umediafwStatus = mData.get(getAdapterPosition());
                    // 4. 呼叫interface的method
                    mClickHandler.onItemClick(umediafwStatus);
                    //Toast.makeText(view.getContext(),
                    //        "click " +getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            });

            if (btnViewUpdate != null) {
                btnViewUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mClickHandler.onViewClick(getAdapterPosition());
                    }
                });
            }
        }
    }

    // binding with layout
    @Override // required
    public UMEDIAFWAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // 連結定義List item (view, UI component)的layout
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_onyx_fw_list_item, viewGroup, false); //取得list的view
        return new UMEDIAFWAdapter.ViewHolder(view);
    }

    // set data (according to the status of each device)
    @Override // required
    public void onBindViewHolder(@NonNull UMEDIAFWAdapter.ViewHolder viewHolder, int i) {
        //
        // device type
        //
        if (mData.get(i).getDeviceType() == AppConstants.DeviceType.DESKTOP) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hrn22ac); // image needs to be update
        }
        if (mData.get(i).getDeviceType() == AppConstants.DeviceType.PLUG) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hex22acp); // image needs to be update
        }
        if (mData.get(i).getDeviceType() == AppConstants.DeviceType.TOWER) {
            viewHolder.imgUMEDIADevice.setImageResource(R.drawable.pic_hna22ac); // image needs to be update
        }

        //
        //  Name
        //
        viewHolder.tvName.setText(mData.get(i).getDeviceLabel());

        //
        // FW version
        //
        viewHolder.tvFWVersion.setText(mData.get(i).getFWVersion());

        //
        // Status
        //
        if (mData.get(i).getFWStatus() == FWStatus.UMEDIA_FW_UP_TO_DATE) {
            viewHolder.tvFWStatus.setText("Up-to-date");
            viewHolder.tvViewUpdate.setVisibility(View.INVISIBLE);
            viewHolder.btnViewUpdate.setVisibility(View.INVISIBLE);
        }
        if (mData.get(i).getFWStatus() == FWStatus.UMEDIA_FW_UPDATE_AVAILABLE) {
            viewHolder.tvFWStatus.setText("Update Available");
            viewHolder.tvViewUpdate.setVisibility(View.VISIBLE);
            viewHolder.btnViewUpdate.setVisibility(View.VISIBLE);
        }
        if (mData.get(i).getFWStatus() == FWStatus.UMEDIA_FW_UPDATING) {
            viewHolder.tvFWStatus.setText("Updating");
            viewHolder.tvViewUpdate.setVisibility(View.INVISIBLE);
            viewHolder.btnViewUpdate.setVisibility(View.INVISIBLE);

            viewHolder.tvUpdating.setVisibility(View.VISIBLE);
            viewHolder.pbUpdating.setVisibility(View.VISIBLE);
        }
    }

    @Override // required
    public int getItemCount() {
        return mData.size();
    }

    public void setDevice(ArrayList<UMEDIAFWStatus> data) {
        //通过System.identityHashCode(object)方法来间接的获取内存地址；
        //创建出来的对象，只要没被销毁，内存地址始终不变。
        //Adapter绑定的数据源集合要为同一个集合，notifyDataSetChanged()方法才有效，否则需要重新设置数据源
        //mData = data; <-錯誤寫法，address可能會變更
        mData.clear();
        mData.addAll(data);
        //LogUtils.trace("NotifyMsg", "[Adapter]:" + data.size() + " " + mData.size());
        for (UMEDIAFWStatus umediafwStatus : mData) {
            //LogUtils.trace("NotifyMsg",umediafwStatus.toString());
            //System.out.println(umediafwStatus);
        }
        //LogUtils.trace("status", "address: " + System.identityHashCode(mData));
        notifyDataSetChanged();
    }

}

