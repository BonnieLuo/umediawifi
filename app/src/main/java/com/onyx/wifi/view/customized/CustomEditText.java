package com.onyx.wifi.view.customized;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static android.support.v4.content.ContextCompat.getSystemService;

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    private DrawableLeftListener mLeftListener;
    private DrawableRightListener mRightListener;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDrawableLeftListener(DrawableLeftListener listener) {
        this.mLeftListener = listener;
    }

    public void setDrawableRightListener(DrawableRightListener listener) {
        this.mRightListener = listener;
    }

    public interface DrawableLeftListener {
        public void onDrawableLeftClick(View view);
    }

    public interface DrawableRightListener {
        public void onDrawableRightClick(View view);
    }

    /**
     * 因為我們不能直接給EditText設定點選事件，所以我們用記住我們按下的位置來模擬點選事件
     * 當我們按下的位置 在  EditText的寬度 - 圖示到控制元件右邊的間距 - 圖示的寬度  和
     * EditText的寬度 - 圖示到控制元件右邊的間距之間我們就算點選了圖示，豎直方向就沒有考慮
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (mRightListener != null) {
                    if (getCompoundDrawables()[2] != null) {
                        boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight()) && (event.getX() < ((getWidth() - getPaddingRight())));
                        if (touchable) {
                            try {
                                //hideSoftInput();
                                mRightListener.onDrawableRightClick(this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //setEditable();
                        }
                    }
                }
                if (mLeftListener != null) {
                    if (getCompoundDrawables()[0] != null) {
                        boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight()) && (event.getX() < ((getWidth() - getPaddingRight())));
                        if (touchable) {
                            try {
                                //hideSoftInput();
                                mLeftListener.onDrawableLeftClick(this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //setEditable();
                        }
                    }
                }
                break;

            case MotionEvent.ACTION_DOWN:
                if (mRightListener != null) {
                    if (getCompoundDrawables()[2] != null) {
                        boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight()) && (event.getX() < ((getWidth() - getPaddingRight())));
                        if (touchable) {
                            try {
                                //hideSoftInput();
                                mRightListener.onDrawableRightClick(this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //setEditable();
                        }
                    }
                }
                if (mLeftListener != null) {
                    if (getCompoundDrawables()[0] != null) {
                        boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight()) && (event.getX() < ((getWidth() - getPaddingRight())));
                        if (touchable) {
                            try {
                                //hideSoftInput();
                                mLeftListener.onDrawableLeftClick(this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //setEditable();
                        }
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 設定點選EditText右側圖示EditText失去焦點，防止點選EditText右側圖示EditText獲得焦點軟鍵盤彈出
     * 如果軟鍵盤此刻為顯示狀態則強直性隱藏
     */
    /*private void hideSoftInput() {
        setFocusableInTouchMode(false);
        setFocusable(false);
        InputMethodManager imm = (InputMethodManager) MyApplication.getApp().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
    }*/

    /**
     * 設定點選EditText輸入區域，EditText請求焦點，軟鍵盤彈出，EditText可編輯
     */
    public void setEditable() {
        setFocusableInTouchMode(true);
        setFocusable(true);
        requestFocus();
    }

    public void setNotEditable() {
        setFocusable(false);
        setFocusableInTouchMode(false);
        clearFocus();
    }
}
