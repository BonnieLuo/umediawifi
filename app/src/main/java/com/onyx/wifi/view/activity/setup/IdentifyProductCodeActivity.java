package com.onyx.wifi.view.activity.setup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityIdentifyProductCodeBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;

public class IdentifyProductCodeActivity extends BaseDeviceSetupActivity {

    private ActivityIdentifyProductCodeBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_identify_product_code);
        setView();
    }

    private void setView() {
        setTitleBar();

        mBinding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setSupportClickMessage(mBinding.tvSupport);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);

        switch (DataManager.getInstance().getSetupMode()) {
            case ROUTER:
            case ROUTER_RESETUP:
                mBinding.titleBar.setTitle(R.string.initial_setup_title);
                break;

            case REPEATER:
            default:
                mBinding.titleBar.setTitle(R.string.umedia_device_setup_title);
                break;
        }

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
