package com.onyx.wifi.view.activity.mainmenu.troubleshooting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.BuildConfig;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySupportBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.UserInfo;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.NoUnderlineSpan;

import java.io.File;

public class SupportActivity extends BaseMenuActivity {

    private ActivitySupportBinding mBinding;

    private boolean mFromDeviceSetup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* 有兩種路徑可開啟 Support 頁面:
           (1) Troubleshooting 頁面的 support 按鈕 => 頁面下方要顯示 menu bar
           (2) Device setup 過程中頁面的 support 訊息 => 頁面下方不顯示 menu bar
           預設值 false 代表的是 (1) 的情況*/
        mFromDeviceSetup = getIntent().getBooleanExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, false);
        LogUtils.trace("from device setup = " + mFromDeviceSetup);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_support);
        setView();
        setData();
    }

    private void setData() {
        mBinding.tvAppVersion.setText(BuildConfig.VERSION_NAME);
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        setClickMessage();

        mBinding.btnMoreHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MoreHelpActivity.class);
                intent.putExtra(AppConstants.EXTRA_FROM_DEVICE_SETUP, mFromDeviceSetup);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        mBinding.btnEmailUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail(false);
            }
        });

        if (mFromDeviceSetup) {
            mBinding.menuBar.setVisibility(View.GONE);

            ConstraintLayout.LayoutParams newLayoutParams = (ConstraintLayout.LayoutParams) mBinding.clContent.getLayoutParams();
            newLayoutParams.bottomMargin = 0;
            mBinding.clContent.setLayoutParams(newLayoutParams);
        }

        if (BuildConfig.DEBUG || BuildConfig.IS_RDQA) {
            // 只有 Production Release 版本不符合判斷式
            mBinding.tvAppVersion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 開啟 developer 寄信模式
                    sendDeveloperEmail();
                }
            });
        }
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.support_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setClickMessage() {
        SpannableString msgContent = new SpannableString(getString(R.string.support_content));
        final String strEmail = getString(R.string.support_content_email);
        int startIndex = msgContent.toString().indexOf(strEmail);
        int endIndex = startIndex + strEmail.length();
        NoUnderlineSpan urlSpan = new NoUnderlineSpan() {
            @Override
            public void onClick(View view) {
                sendEmail(false);
            }
        };
        msgContent.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msgContent.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.purple_4e1393)), startIndex, endIndex, 0);
        mBinding.tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvContent.setText(msgContent, TextView.BufferType.SPANNABLE);
        mBinding.tvContent.setHighlightColor(Color.TRANSPARENT);

        // Support (舊的字串, 先保留, 以後若有新的 support 電話/email/網站, 需要設定文字變色和連結, 可以參考這裡的做法)
//        SpannableString msgContent1 = new SpannableString(getString(R.string.support_content_1));
//        final String strPhoneNumber = getString(R.string.support_content_1_phone_number);
//        // phone number
//        int startIndex = msgContent1.toString().indexOf(strPhoneNumber);
//        int endIndex = startIndex + strPhoneNumber.length();
//        NoUnderlineSpan urlSpan = new NoUnderlineSpan() {
//            @Override
//            public void onClick(@NonNull View view) {
//                Uri uri = Uri.parse("tel:" + strPhoneNumber);
//                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
//                startActivity(intent);
//            }
//        };
//        msgContent1.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        msgContent1.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.purple_4e1393)), startIndex, endIndex, 0);
//        mBinding.tvContent1.setMovementMethod(LinkMovementMethod.getInstance());
//        mBinding.tvContent1.setText(msgContent1, TextView.BufferType.SPANNABLE);
//        mBinding.tvContent1.setHighlightColor(Color.TRANSPARENT);
//
//        SpannableString msgContent2 = new SpannableString(getString(R.string.support_content_2));
//        final String strEmail = getString(R.string.support_content_2_email);
//        final String strWebsite = getString(R.string.support_content_2_website);
//        // email
//        startIndex = msgContent2.toString().indexOf(strEmail);
//        endIndex = startIndex + strEmail.length();
//        urlSpan = new NoUnderlineSpan() {
//            @Override
//            public void onClick(View view) {
//                sendEmail();
//            }
//        };
//        msgContent2.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        msgContent2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.purple_4e1393)), startIndex, endIndex, 0);
//        // website
//        startIndex = msgContent2.toString().indexOf(strWebsite);
//        endIndex = startIndex + strWebsite.length();
//        urlSpan = new NoUnderlineSpan() {
//            @Override
//            public void onClick(View view) {
//                Uri uri = Uri.parse(strWebsite);
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
//            }
//        };
//        msgContent2.setSpan(urlSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        msgContent2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.purple_4e1393)), startIndex, endIndex, 0);
//        mBinding.tvContent2.setMovementMethod(LinkMovementMethod.getInstance());
//        mBinding.tvContent2.setText(msgContent2, TextView.BufferType.SPANNABLE);
//        mBinding.tvContent2.setHighlightColor(Color.TRANSPARENT);
    }

    private void sendEmail(boolean isDeveloperMode) {
        String toEmail;
        if (!isDeveloperMode) {
            toEmail = getString(R.string.support_content_email);
        } else {
            toEmail = "app.developer.umedia@gmail.com";
        }

        String firstName = "";
        String lastName = "";
        String email = "";
        UserInfo userInfo = DataManager.getInstance().getUserInfo();
        if (userInfo != null) {
            firstName = userInfo.getFirstName();
            lastName = userInfo.getLastName();
            email = userInfo.getEmail();
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        // 收件者
        String toEmails[] = {toEmail};
        intent.putExtra(Intent.EXTRA_EMAIL, toEmails);
        // 標題
        intent.putExtra(Intent.EXTRA_SUBJECT, "UMEDIA Support");
        // 內容
        intent.putExtra(Intent.EXTRA_TEXT, "User Name : " + firstName + " " + lastName +
                "\nAccount Email : " + email +
                "\nPhone Model Number : " + Build.MODEL +
                "\nAndroid Version : " + Build.VERSION.RELEASE +
                "\nApp Version : " + BuildConfig.VERSION_NAME +
                "\n------------------------------------------------" +
                "\nQuestion : " + "\n\n");

        if (isDeveloperMode) {
            File logFile = LogUtils.getLogFile();
            Uri logFileUri;
            if (Build.VERSION.SDK_INT >= 24) {  // Android 7.0 以上
                logFileUri = FileProvider.getUriForFile(SupportActivity.this, BuildConfig.APPLICATION_ID.concat(".provider"), logFile);
            } else {
                logFileUri = Uri.fromFile(logFile);
            }

            // 任意的二進位數據
            intent.setType("application/octet-stream");
            // 附件
            intent.putExtra(Intent.EXTRA_STREAM, logFileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            intent.setType("plain/text");
        }

        startActivity(Intent.createChooser(intent, "Choose Email Client"));
    }

    private void sendDeveloperEmail() {
        sendEmail(true);
    }
}
