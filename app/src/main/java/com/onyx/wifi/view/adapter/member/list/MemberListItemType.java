package com.onyx.wifi.view.adapter.member.list;

public enum MemberListItemType {
    SECTION,
    ITEM
}
