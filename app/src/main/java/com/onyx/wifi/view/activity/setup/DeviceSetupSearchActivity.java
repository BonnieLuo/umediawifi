package com.onyx.wifi.view.activity.setup;

import android.arch.lifecycle.ViewModelProviders;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityRouterSetupSearchBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseDeviceSetupActivity;
import com.onyx.wifi.view.activity.setup.repeater.RepeaterProvisionActivity;
import com.onyx.wifi.view.activity.setup.router.RouterProvisionActivity;
import com.onyx.wifi.view.fragment.setup.DeviceNotFoundFragment;
import com.onyx.wifi.view.fragment.setup.DeviceSearchFragment;
import com.onyx.wifi.view.fragment.setup.MultipleDeviceFoundFragment;
import com.onyx.wifi.view.fragment.setup.router.RouterFoundFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.setup.BleScanViewModel;

public class DeviceSetupSearchActivity extends BaseDeviceSetupActivity implements DeviceSetupListener {

    private DeviceSearchFragment mDeviceSearchFragment;
    private RouterFoundFragment mRouterFoundFragment;
    private DeviceNotFoundFragment mDeviceNotFoundFragment;
    private MultipleDeviceFoundFragment mMultipleDeviceFoundFragment;

    private ActivityRouterSetupSearchBinding mBinding;

    private BleScanViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_router_setup_search);
        setView();
        setViewModel();

        // 為何這裡要判斷 savedInstanceState 並做不同處理, 請參考文章: https://www.jianshu.com/p/d9143a92ad94
        // 該文章中段 (Fragment重疊異常--正確使用hide、show的姿勢) 有提到以下做法及原因
        if (savedInstanceState != null) {
            mDeviceSearchFragment = (DeviceSearchFragment) mFragmentManager.findFragmentByTag(DeviceSearchFragment.class.getName());
            mRouterFoundFragment = (RouterFoundFragment) mFragmentManager.findFragmentByTag(RouterFoundFragment.class.getName());
            mDeviceNotFoundFragment = (DeviceNotFoundFragment) mFragmentManager.findFragmentByTag(DeviceNotFoundFragment.class.getName());
            mMultipleDeviceFoundFragment = (MultipleDeviceFoundFragment) mFragmentManager.findFragmentByTag(MultipleDeviceFoundFragment.class.getName());
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            // 顯示第一頁, 隱藏其他頁
            if (mRouterFoundFragment != null) {
                fragmentTransaction.hide(mRouterFoundFragment);
            }
            if (mDeviceNotFoundFragment != null) {
                fragmentTransaction.hide(mDeviceNotFoundFragment);
            }
            if (mMultipleDeviceFoundFragment != null) {
                fragmentTransaction.hide(mMultipleDeviceFoundFragment);
            }
            fragmentTransaction.show(mDeviceSearchFragment);
            fragmentTransaction.commit();
        } else {
            mDeviceSearchFragment = new DeviceSearchFragment();
            addFragmentAsFirstPage(R.id.flContainer, mDeviceSearchFragment, DeviceSearchFragment.class.getName());
        }
    }

    private void setView() {
        setTitleBar();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);

        switch (DataManager.getInstance().getSetupMode()) {
            case ROUTER:
            case ROUTER_RESETUP:
                mBinding.titleBar.setTitle(R.string.initial_setup_title);
                break;

            case REPEATER:
            default:
                mBinding.titleBar.setTitle(R.string.umedia_device_setup_title);
                break;
        }

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        // search 流程的第一頁 (藍牙掃描裝置) 不能返回, 故先隱藏按鈕
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(BleScanViewModel.class);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBleStateListener, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(mBleStateListener);
    }

    @Override
    public void onBackPressed() {
        if (mDeviceSearchFragment != null && mDeviceSearchFragment.isVisible()) {
            // do nothing
            // 在 search device 頁面時不允許執行返回功能退出頁面, 以免藍牙掃描流程出現問題
            return;
        }

        // 若是在其他頁面 (ex: 找不到裝置、找到一個/多個裝置...), 允許頁面返回功能
        super.onBackPressed();
    }

    @Override
    public void onContinue(int nextStep) {
        LogUtils.trace("next step = " + nextStep);
        switch (nextStep) {
            // 找到一台 router
            case Code.Action.DeviceSetup.ROUTER_FOUND:
                // 藍牙掃描動作已完成, 從後續頁面返回時不能再出現 device search 頁面, 故移除
                removeFragment(mDeviceSearchFragment);

                mRouterFoundFragment = new RouterFoundFragment();
                // device search 頁面已移除, 新的頁面是第一頁, 不用加入 back stack
                addFragmentAsFirstPage(R.id.flContainer, mRouterFoundFragment, RouterFoundFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            // 找不到裝置
            case Code.Action.DeviceSetup.DEVICE_NOT_FOUND:
                DataManager.getInstance().setSetupFailCount(DataManager.getInstance().getSetupFailCount() + 1);

                // 藍牙掃描動作已完成, 從後續頁面返回時不能再出現 device search 頁面, 故移除
                removeFragment(mDeviceSearchFragment);

                mDeviceNotFoundFragment = new DeviceNotFoundFragment();
                // device search 頁面已移除, 新的頁面是第一頁, 不用加入 back stack
                addFragmentAsFirstPage(R.id.flContainer, mDeviceNotFoundFragment, DeviceNotFoundFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            // 找到多台裝置
            case Code.Action.DeviceSetup.MULTIPLE_DEVICE_FOUND:
                // 藍牙掃描動作已完成, 從後續頁面返回時不能再出現 device search 頁面, 故移除
                removeFragment(mDeviceSearchFragment);

                mMultipleDeviceFoundFragment = new MultipleDeviceFoundFragment();
                // device search 頁面已移除, 新的頁面是第一頁, 不用加入 back stack
                addFragmentAsFirstPage(R.id.flContainer, mMultipleDeviceFoundFragment, MultipleDeviceFoundFragment.class.getName());
                mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
                break;

            case Code.Action.DeviceSetup.ALTERNATIVE_SETUP:
                startActivity(new Intent(mContext, AlternativeSetupActivity.class));
                finish();
                break;

            case Code.Action.DeviceSetup.MORE_HELP:
                startMoreHelpActivity();
                break;

            case Code.Action.DeviceSetup.RETRY:
                retryDeviceSetup();
                break;

            case Code.Action.DeviceSetup.HELP_PRODUCT_CODE:
                startActivity(new Intent(mContext, IdentifyProductCodeActivity.class));
                break;

            // 確定要設定的裝置後, 繼續下一步驟, 設定 router
            case Code.Action.DeviceSetup.SETUP_ROUTER:
                Intent intent = new Intent(mContext, RouterProvisionActivity.class);
                intent.putExtra(AppConstants.EXTRA_SETUP_FLOW, AppConstants.SetupFlow.BLE);
                intent.putExtra(AppConstants.EXTRA_SETUP_DEVICE, mViewModel.getTargetDevice());
                startActivity(intent);
                finish();
                break;

            // 找到一台 repeater
            case Code.Action.DeviceSetup.REPEATER_FOUND:
                mViewModel.setSelectedIndex(0);
                onContinue(Code.Action.DeviceSetup.SETUP_REPEATER);
                break;

            // 確定要設定的裝置後, 繼續下一步驟, 設定 repeater
            case Code.Action.DeviceSetup.SETUP_REPEATER:
                intent = new Intent(mContext, RepeaterProvisionActivity.class);
                intent.putExtra(AppConstants.EXTRA_SETUP_FLOW, AppConstants.SetupFlow.BLE);
                intent.putExtra(AppConstants.EXTRA_SETUP_DEVICE, mViewModel.getTargetDevice());
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void setSupportView(TextView textView) {
        setSupportClickMessage(textView);
    }
}
