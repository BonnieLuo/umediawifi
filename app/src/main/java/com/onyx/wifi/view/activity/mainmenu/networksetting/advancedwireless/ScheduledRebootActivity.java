package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.app.TimePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TimePicker;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessScheduledRebootBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.reboot.ScheduleRebootConfig;
import com.onyx.wifi.model.item.advancewireless.reboot.ScheduleRebootModel;
import com.onyx.wifi.model.item.member.ScheduleConfigurationModel;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.mainmenu.accountsetting.AccountSettingActivity;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.AdvanceWirelessSettingsViewModel;
import com.onyx.wifi.viewmodel.menubar.advancewireless.DmzViewModel;
import com.onyx.wifi.viewmodel.menubar.advancewireless.ScheduledRebootViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class ScheduledRebootActivity extends BaseMenuActivity {

    final String[] mDayOfWeekArray = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    private ActivityAdvancedWirelessScheduledRebootBinding mBinding;

    private ScheduledRebootViewModel mViewModel;

    private ScheduleRebootConfig mConfig;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_scheduled_reboot);

        setView();

        setViewModel();

        DataManager dataManager = DataManager.getInstance();

        AdvanceWirelessSettings settings = dataManager.getAdvanceWirelessSettings();

        ScheduleRebootModel model = settings.getScheduleRebootModel();

        mConfig = new ScheduleRebootConfig(model);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            setData();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.enableImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfig.toggleEnable();

                setEnable();

                checkDiff();
            }
        });

        mBinding.everyImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionListDialog optionListDialog = new OptionListDialog();


                final ArrayList<String> options = new ArrayList<String>(Arrays.asList(mDayOfWeekArray));

                optionListDialog.setOptionString(options);
                optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        mConfig.setDayOfWeek(position);

                        setDayOfWeek();

                        checkDiff();

                        optionListDialog.dismiss();
                    }
                });
                showDialog(optionListDialog);
            }
        });

        mBinding.timeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar calendar = Calendar.getInstance();

                if (mConfig != null) {
                    Date startTime = mConfig.getTime();

                    if (startTime != null) {
                        calendar.setTime(startTime);
                    }
                }

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, 0);

                                if (mConfig != null) {
                                    Date newStartTime = calendar.getTime();
                                    mConfig.setTime(newStartTime);

                                    setTime();

                                    checkDiff();
                                }

                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        mBinding.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ConfirmDialog confirmDialog = new ConfirmDialog();
                confirmDialog.setTitle("Are you sure you want to reboot now?");
                confirmDialog.setContent("Rebooting UMEDIA device will cause all connected devices to lose the internet connection. All devices will be reconnected again after 3 minutes.");
                confirmDialog.setPositiveButton("Reboot Now", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewModel.rebootAllNodes();
                        confirmDialog.dismiss();
                    }
                });
                confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmDialog.dismiss();
                    }
                });
                showDialog(confirmDialog);
            }
        });

    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Scheduled Reboot");
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setRightButton(R.drawable.ic_check, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.setupScheduledReboot(mConfig);
            }
        });

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ScheduledRebootViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.SET_SCHEDULED_REBOOT) {
                        if (!resultStatus.success) {
                            showMessageDialog("Scheduled Reboot", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }

                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.REMOVE_ALL_NODES) {
                        if (!resultStatus.success) {
                            showMessageDialog("Reboot Now", resultStatus.errorMsg);

                            return;
                        }

                        finish();
                    }
                }
            }
        });
    }

    private void setData(){
        if (mConfig == null) {
            return;
        }

        setEnable();

        setDayOfWeek();

        setTime();

        DataManager dataManager = DataManager.getInstance();
        String currentTimezone = dataManager.getDeviceInfo().getCity();

        mBinding.timezoneTextView.setText(currentTimezone);

    }

    private void setEnable(){
        if (mConfig.isEnabled()) {
            mBinding.enableTextView.setText("Enabled");
            mBinding.enableImageView.setSelected(true);

            mBinding.configLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.enableTextView.setText("Disabled");
            mBinding.enableImageView.setSelected(false);

            mBinding.configLayout.setVisibility(View.GONE);
        }
    }

    private void setDayOfWeek(){
        int dayOfWeekIndex = mConfig.getDayOfWeek();

        String dayOfWeekString = mDayOfWeekArray[dayOfWeekIndex];

        mBinding.everyTextView.setText(dayOfWeekString);
    }

    private void setTime(){
        String timeString = mConfig.getTimeString("hh:mm a");
        mBinding.timeTextView.setText(timeString);
    }

    private void checkDiff(){
        if (mConfig.isChanged()) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
        } else {
            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);

        }
    }

}
