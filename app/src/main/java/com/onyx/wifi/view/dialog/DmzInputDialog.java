package com.onyx.wifi.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ViewDmzInputDialogBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.InputUtils;

public class DmzInputDialog extends BaseDialog {

    private ViewDmzInputDialogBinding mBinding;

    private String mTitle;
    private String mDescription;
    private String mInputHint;
    private String mIp;
    private View.OnClickListener mPositiveListener;
    private View.OnClickListener mNegativeListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_dmz_input_dialog, null, false);
        mBinding.tvTitle.setText(mTitle);
        mBinding.tvDescription.setText(mDescription);
        mBinding.etInput.setText(mIp);
        mBinding.etInput.setSelection(mIp.length());
        mBinding.etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String input = editable.toString().trim();
                // 輸入值符合限制才能按 OK
                if (InputUtils.isNotEmpty(input) && InputUtils.isNameValid(input)) {
                    setPositiveButtonEnable(true);
                } else {
                    // 輸入的字串 trim 完的結果是空字串, 或是有不合法的字元, 都不能按 OK
                    setPositiveButtonEnable(false);
                    if (InputUtils.isNotEmpty(input)) {
                        // 原本沒有這個 if 判斷時, 使用者如果一直按空白鍵, 會一直持續出現提示, 很奇怪, 所以改成不是空字串時才顯示提示
                        CommonUtils.toast(getContext(), InputUtils.getInvalidNameMessage(mInputHint));
                    }
                }
            }
        });
        mBinding.btnPositive.setOnClickListener(mPositiveListener);
        // 一開始還沒輸入任何值時, OK 的按鈕不能按
        setPositiveButtonEnable(false);
        mBinding.btnNegative.setOnClickListener(mNegativeListener);
        builder.setView(mBinding.getRoot());
        return builder.create();
    }

    @Override
    public String toString() {
        return "InputDialog{ " +
                "title = \"" + mTitle + "\"" +
                ", inputHint = \"" + mInputHint + "\"" +
                " }";
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
        // 要延遲一點時間 (等待 dialog 創建完成), EditText 才能正確取得焦點並跳出鍵盤
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CommonUtils.showKeyboard(getContext(), mBinding.etInput);
            }
        }, 100);
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setIp(String ip) { mIp = ip; }

    public void setPositiveListener(View.OnClickListener listener) {
        mPositiveListener = listener;
    }

    public void setNegativeListener(View.OnClickListener listener) {
        mNegativeListener = listener;
    }

    public String getInput() {
        return mBinding.etInput.getText().toString().trim();
    }

    private void setPositiveButtonEnable(boolean isEnable) {
        if (isEnable) {
            mBinding.btnPositive.setTextColor(CommonUtils.getColor(getActivity(), R.color.purple_4e1393));
            mBinding.btnPositive.setEnabled(true);
        } else {
            mBinding.btnPositive.setTextColor(CommonUtils.getColor(getActivity(), R.color.gray_cccccc));
            mBinding.btnPositive.setEnabled(false);
        }
    }
}
