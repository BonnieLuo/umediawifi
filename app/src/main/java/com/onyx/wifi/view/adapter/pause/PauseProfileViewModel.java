package com.onyx.wifi.view.adapter.pause;

public class PauseProfileViewModel<T> {
    private PauseProfileType mPauseProfileType;

    private T mData;

    public PauseProfileViewModel(PauseProfileType pauseProfileType, T data) {
        mPauseProfileType = pauseProfileType;
        mData = data;
    }

    public PauseProfileType getPauseProfileType() {
        return mPauseProfileType;
    }

    public T getData() {
        return mData;
    }
}
