package com.onyx.wifi.view.adapter.pause;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.pause.PauseProfileModel;

import java.util.ArrayList;
import java.util.List;


//TODO:此檔案沒用到
public class GlobalPauseProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface RecyclerViewClickListener {
        void onPauseProfileClick(PauseProfileModel pauseProfile);

        void onPauseClick(PauseProfileModel pauseProfile);

        void onCreatePauseProfileClick();
    }

    private List<PauseProfileViewModel> mProfileList = new ArrayList<PauseProfileViewModel>();

    private RecyclerViewClickListener mItemClickListener;

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == PauseProfileType.FOOTER.ordinal()) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_global_pause_create_profile, parent, false);

            FooterViewHolder footer = new FooterViewHolder(view);
            return footer;
        }

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_global_pause_profile, parent, false);

        PauseProfileViewHolder viewHolder = new PauseProfileViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    private void setProfile(PauseProfileViewModel viewModel, RecyclerView.ViewHolder viewHolder){

    }

    public void setProfileList(List<PauseProfileViewModel> profileList) {
        mProfileList = profileList;
        notifyDataSetChanged();
    }

    public void setItemClickListener(RecyclerViewClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    class PauseProfileViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        private PauseProfileModel mPauseProfile;

        private TextView titleTextView;

        public PauseProfileViewHolder(@NonNull View itemView) {
            super(itemView);

            mContext = this.itemView.getContext();

            titleTextView = this.itemView.findViewById(R.id.titleTextView);
        }

        public void setPauseProfile(PauseProfileModel pauseProfile) {
            mPauseProfile = pauseProfile;

            String name = mPauseProfile.getName();
            titleTextView.setText(name);
        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        public FooterViewHolder(@NonNull View itemView) {
            super(itemView);

            mContext = this.itemView.getContext();

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onCreatePauseProfileClick();
                    }
                }
            });
        }

    }

}
