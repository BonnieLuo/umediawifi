package com.onyx.wifi.view.fragment.setup.router;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentSetWifiBinding;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.setup.WirelessInfo;
import com.onyx.wifi.viewmodel.setup.DeviceSetupViewModel;

public class SetWifiFragment extends BaseFragment {

    private FragmentSetWifiBinding mBinding;
    private DeviceSetupViewModel mViewModel;

    private DeviceSetupListener mEventListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_set_wifi, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
        setViewModel();
        setData();
    }

    private void setView() {
        mBinding.et2gUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getWirelessInfo().setSsid2g(editable.toString().trim());
                checkWifiDataFilled();
            }
        });
        mBinding.et2gPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getWirelessInfo().setPassword2g(editable.toString().trim());
                checkWifiDataFilled();
            }
        });
        mBinding.et5gUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getWirelessInfo().setSsid5g(editable.toString().trim());
                checkWifiDataFilled();
            }
        });
        mBinding.et5gPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.getWirelessInfo().setPassword5g(editable.toString().trim());
                checkWifiDataFilled();
            }
        });
        mBinding.cbUseSame.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mViewModel.getWirelessInfo().setUseSame(isChecked);
                if (isChecked) { // use same
                    mBinding.group5g.setVisibility(View.GONE);

                    // 設定成 same 時, 將 5G 的資料設定成和 2.4G 相同. 更新 WirelessInfo object 就好, 5G 的 UI 看不到了, 不用更新沒關係
                    mViewModel.getWirelessInfo().setSsid5g(mViewModel.getWirelessInfo().getSsid2g());
                    mViewModel.getWirelessInfo().setPassword5g(mViewModel.getWirelessInfo().getPassword2g());
                } else {
                    mBinding.group5g.setVisibility(View.VISIBLE);

                    // 本來是 same, 對使用者來說沒有 5G 的設定, 改成 different 時要為 5G 預設 SSID/password. 5G 的 UI 要更新, WirelessInfo object 也要更新
                    // 不過這裡不用呼叫 WirelessInfo.setSsid5g()/setPassword5g() 沒關係, 因為 setText 後會觸發對應的 afterTextChanged(), 裡面就有做了
                    String ssid = mViewModel.getWirelessInfo().getSsid2g();
                    mBinding.et5gUsername.setText(InputUtils.getDefault5gNetworkSsid(ssid));
                    mBinding.et5gPassword.setText(mViewModel.getWirelessInfo().getPassword2g());
                }
            }
        });
        mBinding.btnRegenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regeneratePassword();
            }
        });
        mBinding.btnCreateNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.isWirelessDataValid()) {
                    mEventListener.onContinue(Code.Action.DeviceSetup.CREATE_NETWORK);
                } else {
                    showMessageDialog("Wireless Network", mViewModel.getInputErrorMsg());
                }
            }
        });
        mEventListener.setSupportView(mBinding.tvSupport);
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(DeviceSetupViewModel.class);
    }

    private void setData() {
        // 隨機產生預設的 network SSID/password
        WirelessInfo wirelessInfo = mViewModel.getWirelessInfo();
        wirelessInfo.randomGenerate();
        mBinding.et2gUsername.setText(wirelessInfo.getSsid2g());
        mBinding.et2gPassword.setText(wirelessInfo.getPassword2g());
        mBinding.et5gUsername.setText(wirelessInfo.getSsid5g());
        mBinding.et5gPassword.setText(wirelessInfo.getPassword5g());

        // 預設 2.4G 和 5G 使用相同的 SSID/password
        mBinding.cbUseSame.setChecked(true);
    }

    private void regeneratePassword() {
        WirelessInfo wirelessInfo = mViewModel.getWirelessInfo();
        wirelessInfo.setPassword2g(InputUtils.generateNetworkPassword());
        wirelessInfo.setPassword5g(InputUtils.generateNetworkPassword());
        mBinding.et2gPassword.setText(wirelessInfo.getPassword2g());
        mBinding.et5gPassword.setText(wirelessInfo.getPassword5g());
    }

    private void checkWifiDataFilled() {
        if (mViewModel.isWirelessDataFilled()) {
            mBinding.btnCreateNetwork.setEnabled(true);
        } else {
            mBinding.btnCreateNetwork.setEnabled(false);
        }
    }
}
