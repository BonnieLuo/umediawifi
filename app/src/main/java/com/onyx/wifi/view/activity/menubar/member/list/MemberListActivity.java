package com.onyx.wifi.view.activity.menubar.member.list;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMemberListBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.Member;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.model.item.member.OtherMember;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.menubar.member.client.ClientSettingsActivity;
import com.onyx.wifi.view.activity.menubar.member.create.MemberCreateActivity;
import com.onyx.wifi.view.activity.menubar.member.manage.MemberManageActivity;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.list.MemberListAdapter;
import com.onyx.wifi.view.adapter.member.list.MemberListItemType;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.OptionListDialog;
import com.onyx.wifi.view.interfaces.member.OnMemberListEventListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.list.MemberListViewModel;

import java.util.ArrayList;
import java.util.List;

public class MemberListActivity extends BaseMenuActivity implements OnMemberListEventListener {

    private ActivityMemberListBinding mBinding;

    private MemberListViewModel mViewModel;

    private MemberListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_list);

        mAdapter = new MemberListAdapter();
        mAdapter.setMemberListEventListener(this);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            mViewModel.getMemberList();
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        mBinding.membersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.membersRecyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.member_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButton(R.drawable.ic_member_add, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MemberListActivity.this, MemberCreateActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(MemberListViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Member List", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        MemberList memberList = dataManager.getMemberList();
                        setData(memberList);
                    }

                    if (resultStatus.actionCode == Code.Action.Member.REMOVE_FAMILY_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Family Member", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setManageMember(null);
                        dataManager.setManageClient(null);

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.PAUSE_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Pause Family Member", resultStatus.errorMsg);

                            return;
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setManageMember(null);
                        dataManager.setManageClient(null);

                        mViewModel.getMemberList();
                    }

                    if (resultStatus.actionCode == Code.Action.Member.RESUME_MEMBER) {
                        if (!resultStatus.success) {
                            showMessageDialog("Resume Family Member", resultStatus.errorMsg);

                            return;
                        }

                        mViewModel.getMemberList();
                    }
                }
            }
        });
    }

    private void setData(MemberList memberList){
        if (memberList == null) {
            return;
        }

        List<ListItem> memberListItems = new ArrayList<>();

        setupFamilyMemberList(memberList, memberListItems);

        setupOtherMemberList(memberList, memberListItems);

        mAdapter.setListItems(memberListItems);
    }

    private void setupFamilyMemberList(MemberList memberList, List<ListItem> listItems) {
        List<MemberModel> familyMemberList = memberList.getFamilyMemberList();

        if (familyMemberList.size() == 0) {
            return;
        }

        String title = getString(R.string.member_family_members);

        ListItem<MemberListItemType, String> section = new ListItem<MemberListItemType, String>(MemberListItemType.SECTION, title);
        listItems.add(section);

        for (MemberModel memberModel : familyMemberList) {
            Member member = new FamilyMember(memberModel);
            ListItem<MemberListItemType, Member> item = new ListItem<MemberListItemType, Member>(MemberListItemType.ITEM, member);
            listItems.add(item);
        }
    }

    private void setupOtherMemberList(MemberList memberList, List<ListItem> listItems) {
        String title = getString(R.string.member_other_network_members);

        ListItem<MemberListItemType, String> section = new ListItem<MemberListItemType, String>(MemberListItemType.SECTION, title);
        listItems.add(section);

        // Home

        List<ClientModel> homeClientModelList = memberList.getHomeNetwork();

        Member home = new OtherMember(mContext, Member.MemberType.HOME, homeClientModelList);

        ListItem<MemberListItemType, Member> homeMemberListItem = new ListItem<MemberListItemType, Member>(MemberListItemType.ITEM, home);
        listItems.add(homeMemberListItem);

        // Guest

        List<ClientModel> guestClientModelList = memberList.getGuestNetwork();

        Member guest = new OtherMember(mContext, Member.MemberType.GUEST, guestClientModelList);

        ListItem<MemberListItemType, Member> guestMemberListItem = new ListItem<MemberListItemType, Member>(MemberListItemType.ITEM, guest);
        listItems.add(guestMemberListItem);

        // BnB

        List<ClientModel> bnbGuestClientModelList = memberList.getBnBGuestNetwork();

        Member bnbNetwork = new OtherMember(mContext, Member.MemberType.BNB, bnbGuestClientModelList);

        ListItem<MemberListItemType, Member> bnbMemberListItem = new ListItem<MemberListItemType, Member>(MemberListItemType.ITEM, bnbNetwork);
        listItems.add(bnbMemberListItem);
    }

    private void showManageDialog() {
        OptionListDialog optionListDialog = new OptionListDialog();

        final ArrayList<String> options = new ArrayList<>();
        options.add(getString(R.string.member_manage_family_member));
        options.add(getString(R.string.member_remove_family_member));

        ArrayList<Integer> icons = new ArrayList<>();
        icons.add(R.drawable.ic_sheet_member);
        icons.add(R.drawable.ic_sheet_remove);

        optionListDialog.setOptionString(options);
        optionListDialog.setOptionIconIds(icons);
        optionListDialog.setOptionClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(MemberListActivity.this, MemberManageActivity.class);
                    startActivity(intent);

                    optionListDialog.dismiss();
                }

                if (position == 1) {
                    final ConfirmDialog confirmDialog = new ConfirmDialog();
                    confirmDialog.setTitle(getString(R.string.member_remove_family_member_dialog_title));
                    confirmDialog.setContent(getString(R.string.member_remove_family_member_desc));
                    confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeMember();

                            confirmDialog.dismiss();
                        }
                    });
                    confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            confirmDialog.dismiss();
                        }
                    });
                    showDialog(confirmDialog);
                }


                optionListDialog.dismiss();
            }
        });
        showDialog(optionListDialog);
    }

    private void removeMember() {
        DataManager dataManager = DataManager.getInstance();
        FamilyMember member = dataManager.getManageMember();
        String memberId = member.getId();

        if (memberId != null) {
            mViewModel.removeMember(memberId);
        }
    }

    // OnMemberListEventListener

    @Override
    public void onManageMemberClick(Member member) {
        if (member instanceof FamilyMember) {
            DataManager dataManager = DataManager.getInstance();
            dataManager.setManageMember((FamilyMember) member);

            showManageDialog();
        }
    }

    @Override
    public void onMemberClick(Member member) {
        if (member instanceof FamilyMember) {
            DataManager dataManager = DataManager.getInstance();
            dataManager.setManageMember((FamilyMember) member);
            Intent intent = new Intent(MemberListActivity.this, MemberManageActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onPauseMemberClick(Member member) {
        if (member instanceof FamilyMember != true) {
            return;
        }

        FamilyMember familyMember = (FamilyMember) member;

        List<PauseInfo> pauseInfoList = familyMember.getPauseInfoList();
        if (pauseInfoList.isEmpty()) {
            String memberId = familyMember.getId();
            mViewModel.pauseMember(memberId);
        } else {
            for (PauseInfo pauseInfo: pauseInfoList) {
                if (pauseInfo.getType().equalsIgnoreCase("2")) {
                    String memberId = familyMember.getId();
                    String ruleId = pauseInfo.getRuleId();

                    mViewModel.resumeMember(memberId, ruleId);
                }
            }
        }
    }

    @Override
    public void onClientClick(ClientModel clientModel) {
        Client client = new Client(clientModel);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setManageClient(client);

        Intent intent = new Intent(MemberListActivity.this, ClientSettingsActivity.class);
        startActivity(intent);

    }

}
