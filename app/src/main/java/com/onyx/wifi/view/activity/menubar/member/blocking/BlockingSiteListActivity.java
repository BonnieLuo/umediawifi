package com.onyx.wifi.view.activity.menubar.member.blocking;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityMemberContentFiltersBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.member.ListItem;
import com.onyx.wifi.view.adapter.member.blocking.BlockingListItemType;
import com.onyx.wifi.view.adapter.member.blocking.BlockingSiteListAdapter;
import com.onyx.wifi.view.customized.MenuBar;
import com.onyx.wifi.view.dialog.ConfirmDialog;
import com.onyx.wifi.view.dialog.InputWebsiteDialog;
import com.onyx.wifi.view.interfaces.member.OnBlacklistChangedListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.member.blocking.BlockingSiteViewModel;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class BlockingSiteListActivity extends BaseMenuActivity implements OnBlacklistChangedListener {

    private ActivityMemberContentFiltersBinding mBinding;

    private BlockingSiteViewModel mViewModel;

    private BlockingSiteListAdapter mAdapter;

    private FamilyMember mMember;

    private List<String> mBlacklist;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_member_content_filters);

        mAdapter = new BlockingSiteListAdapter(this);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            DataManager dataManager = DataManager.getInstance();
            mMember = dataManager.getManageMember();
            String memberId = mMember.getId();
            mViewModel.getContentFiltersList(memberId);
        }
    }

    private void setView() {
        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.menuBar.setMenuBarItemFocus(MenuBar.MenuBarItem.MEMBER, true);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Site & Keyword Blocking");

        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);

        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(BlockingSiteViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Member.GET_MEMBER_BLOCKING_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Blocking Site List", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            setData(json);
                        }
                    }

                    if (resultStatus.actionCode == Code.Action.Member.CREATE_MEMBER_WEBSITE_BLOCKING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Create Site Blocking", resultStatus.errorMsg);

                            return;
                        }

                        String memberId = mMember.getId();
                        mViewModel.getContentFiltersList(memberId);
                    }

                    if (resultStatus.actionCode == Code.Action.Member.ADD_MEMBER_WEBSITE_BLOCKING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Add Site Blocking", resultStatus.errorMsg);

                            return;
                        }

                        String memberId = mMember.getId();
                        mViewModel.getContentFiltersList(memberId);
                    }

                    if (resultStatus.actionCode == Code.Action.Member.REMOVE_MEMBER_WEBSITE_BLOCKING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Remove Site Blocking", resultStatus.errorMsg);

                            return;
                        }

                        String memberId = mMember.getId();
                        mViewModel.getContentFiltersList(memberId);
                    }

                    if (resultStatus.actionCode == Code.Action.Member.CLEAN_MEMBER_WEBSITE_BLOCKING) {
                        if (!resultStatus.success) {
                            showMessageDialog("Clean Site Blocking", resultStatus.errorMsg);

                            return;
                        }

                        String memberId = mMember.getId();
                        mViewModel.getContentFiltersList(memberId);
                    }
                }
            }
        });
    }

    private void setData(JsonObject json){
        List<ListItem> listItems = new ArrayList<>();

        ListItem header = new ListItem(BlockingListItemType.HEADER, null);
        listItems.add(header);

        if (json != null) {
            Gson gson = new Gson();

            JsonElement dataJson = json.get("data");
            if (dataJson != null) {
                JsonObject data = (dataJson instanceof JsonNull) ? null : dataJson.getAsJsonObject();

                if (data != null) {
                    JsonArray blacklistArray = data.getAsJsonArray("keyword_blacklist");
                    Type listType = new TypeToken<List<String>>() {
                    }.getType();
                    mBlacklist = gson.fromJson(blacklistArray.toString(), listType);

                    if (mBlacklist != null && mBlacklist.size() != 0) {
                        for (String site : mBlacklist) {
                            ListItem item = new ListItem(BlockingListItemType.ITEM, site);
                            listItems.add(item);
                        }
                    }
                } else {
                    mBlacklist = new ArrayList<>();
                }
            }
        }

        ListItem footer = new ListItem(BlockingListItemType.FOOTER, null);
        listItems.add(footer);

        mAdapter.setListItems(listItems);
    }

    @Override
    public void onAddWebsiteBlocking() {
        InputWebsiteDialog inputWebsiteDialog = new InputWebsiteDialog();
        inputWebsiteDialog.setTitle("Add A Website");
        inputWebsiteDialog.setInputHint("URL");
        inputWebsiteDialog.setPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String site = null;
                try {
                    site = inputWebsiteDialog.getInput();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                if (!(InputUtils.isUrlValid(site) || InputUtils.isKeywordValid(site))) {
                    CommonUtils.toast(mContext, "The URL you entered does not work.");
                } else {
                    String memberId = mMember.getId();

                    if (mBlacklist.size() > 0) {
                        mViewModel.addContentFilters(memberId, site);


                    } else {
                        mViewModel.createContentFilters(memberId, site);
                    }
                }

                inputWebsiteDialog.dismiss();
            }
        });
        inputWebsiteDialog.setNegativeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputWebsiteDialog.dismiss();
            }
        });
        showDialog(inputWebsiteDialog);
    }

    @Override
    public void onRemoveWebsiteBlocking(String site) {
        final ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.setTitle("Remove Website");
        confirmDialog.setContent("Are you sure you want to remove this URL?");
        confirmDialog.setPositiveButton("Remove", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String memberId = mMember.getId();

                if (mBlacklist.size() > 1) {
                    mViewModel.removeContentFilters(memberId, site);


                } else {
                    mViewModel.cleanContentFilters(memberId);
                }

                confirmDialog.dismiss();
            }
        });
        confirmDialog.setNegativeButton("Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
            }
        });
        showDialog(confirmDialog);
    }
}
