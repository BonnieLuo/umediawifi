package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNetworkSettingBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless.AdvancedWirelessSettingActivity;
import com.onyx.wifi.view.customized.CustomEditText;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

import java.util.Timer;
import java.util.TimerTask;

public class NetworkSettingActivity extends BaseMenuActivity implements CustomEditText.DrawableRightListener {

    private ActivityNetworkSettingBinding mBinding;
    private NetworkSettingViewModel mViewModel;
    //
    // 假資料，需修改為從Cloud取得或從SharePreference or DB取得的資訊。
    // CheckBox的狀態也必須根據實際資訊顯示 (setCheck())
    // 3個CheckBox只要有一個更動(與原本的資料不同)，在右上角會顯示勾勾，否則右上角不會有圖示
    //
    private boolean bAutoBWMonitor = false;
    private boolean bAutoUpdateFW = true;
    //private boolean bLEDLight = true;
    private boolean bTmpAutoBWMonitor = false;
    private boolean bTmpAutoUpdateFW = true;
    //private boolean bTmpLEDLight = true;

    // 紀錄Check Box是否有更動
    private boolean bIsCBAutoBWMonitorChange = false;
    private boolean bIsCBAutoUpdateFWChange = false;
    //private boolean bIsCBLEDLightChange = false;
    private NetworkSetting mOriginalNetworkSetting;
    private NetworkSetting mTempNetworkSetting;

    private View view_networksetting = null;
    private Timer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = LayoutInflater.from(this);
        view_networksetting = inflater.inflate(R.layout.activity_network_setting, null);

        //
        // 透過View來取得binding (為了解決Custom EditText點擊屏幕應該取消焦點和關掉鍵盤的問題)
        //
        mBinding = DataBindingUtil.bind(view_networksetting);
        setContentView(view_networksetting);
        //mOriginalNetworkSetting = DataManager.getInstance().getNetworkSetting();
        mTempNetworkSetting = DataManager.getInstance().getNetworkSetting();

        setViewModel();
        setData();// remove
        initView();
        initUpdateableData(); // called in (1)onCreate() and (2)execute postHomeNetworkSetting successfully
        setView(); // handler setting; the initial value of UI is set in onResume (setData()) <= note from Account Settings UI

        // 先打 以防止太慢出現
        mViewModel.getFirmwareStatus();
    }

    void startTimer() {
        if (mTimer == null) {
            NetworkSettingActivity.GetListTimerTask getListTimerTask = new NetworkSettingActivity.GetListTimerTask();
            mTimer = new Timer();
            mTimer.schedule(getListTimerTask, 10L, 30000L);
        }
    }

    void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void initView() {
        startTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
    }

    private class GetListTimerTask extends TimerTask {

        @Override
        public void run() {

            // get fw status list ( for summary fw status)
            //mViewModel.getFirmwareStatus();
            // get uptime
            mViewModel.postUptime();
            ///////////////////////////////////
            // get dashboard info
            Handler mGetInfoHandler = new Handler(Looper.getMainLooper());
            Runnable mLinkingTimeoutRunnable = new Runnable() {
                @Override
                public void run() {
                    mViewModel.getDashboardInfo();
                }
            };
            mGetInfoHandler.postDelayed(mLinkingTimeoutRunnable, 3000);

        }
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.NetworkSetting.GET_DASHBOARD_INFO) {
                        if (resultStatus.success) {
                            //LogUtils.trace("CloudApi", "Get Dashboard Info success~~~~~~~~~~~~~~");
                            mViewModel.getFirmwareStatus();
                            setData();
                        } else {
                            showMessageDialog("Get Dashboard Info", resultStatus.errorMsg);
                        }
                    }else if (resultStatus.actionCode == Code.Action.NetworkSetting.POST_HOME_NETWORK_SETTING) {
                        if (resultStatus.success) {
                            //LogUtils.trace("CloudApi", "Post Home Network Setting success~~~~~~~~~~~~~~");
                            DataManager.getInstance().updateNetworkSetting(mTempNetworkSetting);
                            initUpdateableData(); // 更新比對用的三個boolean變數
                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                        } else {
                            showMessageDialog("Post Home Network Setting", resultStatus.errorMsg);
                        }
                        startTimer();
                    } else if (resultStatus.actionCode == Code.Action.NetworkSetting.GET_FIRMWARE_STATUS) {
                        if (resultStatus.success) {
                            if (mViewModel.IsUpdateAvailable()) {
                                mBinding.tvFWStatus.setText("Update Available");
                            } else {
                                mBinding.tvFWStatus.setText("Up-to-date");
                            }

                        } else {
                            showMessageDialog("Get Firmware Status", resultStatus.errorMsg);
                        }
                    }else if (resultStatus.actionCode == Code.Action.NetworkSetting.POST_UPTIME) {
                        if (resultStatus.success) {

                        } else {
                            showMessageDialog("POST UPTIME", resultStatus.errorMsg);
                        }
                    }

                }
            }

        });
    }

    private void initUpdateableData() {
        // 打postHomeNetwork前會更新mTempNetworkSetting
        bAutoBWMonitor = mTempNetworkSetting.getAutoBandwidth();
        bAutoUpdateFW = mTempNetworkSetting.getAutoFwEnable();
        //bLEDLight = mTempNetworkSetting.getLedLight();
        bTmpAutoBWMonitor = mTempNetworkSetting.getAutoBandwidth();
        bTmpAutoUpdateFW = mTempNetworkSetting.getAutoFwEnable();
        //bTmpLEDLight = mTempNetworkSetting.getLedLight();

        mBinding.cbAutoBWMonitor.setChecked(bTmpAutoBWMonitor);
        if (bTmpAutoBWMonitor) {
            mBinding.tvAutoBWMonitorStatus.setText("Enabled");
        } else {
            mBinding.tvAutoBWMonitorStatus.setText("Disabled");
        }

        mBinding.cbAutoUpdateFW.setChecked(bTmpAutoUpdateFW);
        if (bTmpAutoUpdateFW) {
            mBinding.tvAutoUpdateFWStatus.setText("Enabled");
        } else {
            mBinding.tvAutoUpdateFWStatus.setText("Disabled");
        }

//        mBinding.cbLEDLight.setChecked(bTmpLEDLight);
//        if (bTmpLEDLight) {
//            mBinding.tvLEDLightStatus.setText("Enabled");
//        } else {
//            mBinding.tvLEDLightStatus.setText("Disabled");
//        }
    }

    private boolean checkAnyChangeOccur() {
        if (bIsCBAutoBWMonitorChange || bIsCBAutoUpdateFWChange)// || bIsCBLEDLightChange)
            return true;
        return false;
    }

    private void hideKeyboardAndClearCustomEditTextFocus(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        mBinding.tilNetworkName.setFocusable(false);
        mBinding.tilNetworkName.setFocusableInTouchMode(false);
        mBinding.etNetworkName.setNotEditable();
        mBinding.etNetworkName.setLongClickable(false);
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        //
        // Automatic Bandwidth Montitoring
        //
        mBinding.cbAutoBWMonitor.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mBinding.tvAutoBWMonitorStatus.setText("Enabled");
                } else {
                    mBinding.tvAutoBWMonitorStatus.setText("Disabled");
                }

                bTmpAutoBWMonitor = isChecked;

                if (isChecked != bAutoBWMonitor) {
                    bIsCBAutoBWMonitorChange = true;
                } else {
                    bIsCBAutoBWMonitorChange = false;
                }

                if (!checkAnyChangeOccur()) {
                    mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                } else {
                    mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnAutoBWMonitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfoDialog("Auto Bandwidth Monitoring",
                        "When enabled, this feature will keep track of your internet connection speed provided by your ISP (Internet Service Provider). Use this to compare your actual speed against your ISP's advertised speed. This feature can slow network speed dramatically for around 30 seconds a few times a day.");
            }
        });

        //
        // Automatically Update Firmware
        //
        mBinding.cbAutoUpdateFW.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mBinding.tvAutoUpdateFWStatus.setText("Enabled");
                } else {
                    mBinding.tvAutoUpdateFWStatus.setText("Disabled");
                }

                bTmpAutoUpdateFW = isChecked;

                if (isChecked != bAutoUpdateFW) {
                    bIsCBAutoUpdateFWChange = true;
                } else {
                    bIsCBAutoUpdateFWChange = false;
                }

                if (!checkAnyChangeOccur()) {
                    mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                } else {
                    mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
                }
            }
        });

        //
        // Current Timezone
        //
        mBinding.clCurrentTimezone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TimezoneActivity.class);
                startActivity(intent);
            }
        });
//        mBinding.btnCurrentTimezone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mContext, NetworkTimezoneActivity.class);
//                startActivity(intent);
//            }
//        });

        //
        // LED Light
        //
//        mBinding.cbLEDLight.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    mBinding.tvLEDLightStatus.setText("Enable");
//                } else {
//                    mBinding.tvLEDLightStatus.setText("Disable");
//                }
//
//                bTmpLEDLight = isChecked;
//
//                if (isChecked != bLEDLight) {
//                    bIsCBLEDLightChange = true;
//                } else {
//                    bIsCBLEDLightChange = false;
//                }
//
//                if (!checkAnyChangeOccur()) {
//                    mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
//                } else {
//                    mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
//                }
//            }
//        });

        //
        // handle View botton
        //
        mBinding.clInternetIPAddr.setOnClickListener(new View.OnClickListener() {
            //mBinding.btnInternetIPAddr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InternetIPInfoActivity.class);
                startActivity(intent);
            }
        });

        mBinding.clRouterLANIP.setOnClickListener(new View.OnClickListener() {
            //mBinding.btnRouterLANIP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, LANIPInformationActivity.class);
                startActivity(intent);
            }
        });

        mBinding.clFW.setOnClickListener(new View.OnClickListener() {
            //mBinding.btnFW.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FirmwareActivity.class);
                startActivity(intent);
            }
        });

        mBinding.clHighInternetPriority.setOnClickListener(new View.OnClickListener() {
            //mBinding.btnHighInternetPriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, HighInternetPriorityActivity.class);
                startActivity(intent);
            }
        });

        mBinding.clAllDeviceOnNetwork.setOnClickListener(new View.OnClickListener() {
            //mBinding.btnAllDeviceOnNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.getClientInfoList();
                Intent intent = new Intent(mContext, ViewAllDevicesOnNetworkActivity.class);
                startActivity(intent);
            }
        });

        mBinding.clViewAdvancedNetworkSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AdvancedWirelessSettingActivity.class);
                startActivity(intent);
            }
        });

        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTimer();
                // update AutoBWMonitor & AutoUpdateFW to cloud - 成功後更改SharePreference
                mTempNetworkSetting = DataManager.getInstance().getNetworkSetting();
                mTempNetworkSetting.setAutoBandwidth(bTmpAutoBWMonitor);
                mTempNetworkSetting.setAutoFwEnable(bTmpAutoUpdateFW);
                //mTempNetworkSetting.setLedLight(bTmpLEDLight);
                mViewModel.postHomeNetworkSetting(DataManager.getInstance().getDeviceInfo().getCity(), mTempNetworkSetting);
                //LogUtils.trace("NotifyMsg", "press right button");

            }
        });

    }

    private void setData() {
        if (mMenuBar != null) {
            mMenuBar.setData();
        }

//        LogUtils.trace("CloudApi", "\nbAutoBWMonitor: " + bAutoBWMonitor +
//                "\nbAutoUpdateFW: " + bAutoUpdateFW +
//               // "\nbLEDLight: " + bLEDLight +
//                "\n------------------------------------------------");

        // 除了三個可以更動的boolean值，其他都從SharePreference get（每隔一段時間會透過getBoardInfo更新）
        // 那三個變數(CheckBox)則保持最近一次更改的結果：
        // 比對值：bAutoBWMonitor, bAutoUpdateFW, bLEDLight <= 打完api（會將新的值給cloud並存入SharePreference）才會透過initUpdateableData更新
        // 最新值：bTmpAutoBWMonitor, bTmpAutoUpdateFW, bTmpLEDLight　<=　跟著UI變動
        // (比對值:評判有沒有更動的標準)
        // mTempNetworkSetting：
        // (1)在onCreate時根據當下的SharePreference給這三個變數初值
        // (2)作為打api要存的內容
        // (3)打完api後將比對值對齊最新值
        // mOriginalNetworkSetting：
        // (1)always get最新的SharePreference的值讓UI顯示，那三個變數除外
        mOriginalNetworkSetting = DataManager.getInstance().getNetworkSetting();


        //
        // Uptime
        //
        int upTimeInSeconds = 0;
        try {
            upTimeInSeconds = Integer.parseInt(mOriginalNetworkSetting.getUptime());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        //
        // Internet Status
        //
        if (DataManager.getInstance().getDashboardNodeSet() != null &&
                DataManager.getInstance().getDashboardNodeSet().getControllerOnline()) {
            mBinding.imgInternetStatus.setImageResource(R.drawable.ic_internet_status_online);
            mBinding.tvInternetStatusValue.setText(getString(R.string.online));
            mBinding.tvUptimeValue.setText(DateUtils.secToTimeFormat(upTimeInSeconds));
        } else {
            mBinding.imgInternetStatus.setImageResource(R.drawable.ic_internet_status_offline_gray);
            mBinding.tvInternetStatusValue.setText(getString(R.string.offline));
            mBinding.tvUptimeValue.setText("--");
        }

        //
        // Internet IP Address
        //
        mBinding.tvInternetIPAddrValue.setText(mOriginalNetworkSetting.getWanIp());

        //
        // Router's LAN IP
        //
        mBinding.tvRouterLANIPValue.setText(mOriginalNetworkSetting.getLanIp());

        //
        // Automatic Bandwidth Montitoring
        //
//        mBinding.cbAutoBWMonitor.setChecked(bTmpAutoBWMonitor);
//        if (bTmpAutoBWMonitor) {
//            mBinding.tvAutoBWMonitorStatus.setText("Enabled");
//        } else {
//            mBinding.tvAutoBWMonitorStatus.setText("Disabled");
//        }

        //
        // Automatically Update Firmware
        //
//        mBinding.cbAutoUpdateFW.setChecked(bTmpAutoUpdateFW);
//        if (bTmpAutoUpdateFW) {
//            mBinding.tvAutoUpdateFWStatus.setText("Enabled");
//        } else {
//            mBinding.tvAutoUpdateFWStatus.setText("Disabled");
//        }
        /*if ("1".equals(mOriginalNetworkSetting.getFwStatusFromAllNode()))
            mBinding.tvFWStatus.setText("Update Available");
        else if ("2".equals(mOriginalNetworkSetting.getFwStatusFromAllNode()))
            mBinding.tvFWStatus.setText("Up-to-date");
        else
            mBinding.tvFWStatus.setText("Invalid Value");*/

        //
        // High Internet Priority
        //
        if ("0".equals(mOriginalNetworkSetting.getHighInternetPriority())) {
            mBinding.tvHighInternetPriorityStatus.setText("Disabled");
        } else if ("1".equals(mOriginalNetworkSetting.getHighInternetPriority())) {
            mBinding.tvHighInternetPriorityStatus.setText("Enabled");
        }

        //
        // Current Timezone
        //
        mBinding.tvCurrentTimezoneValue.setText(DataManager.getInstance().getDeviceInfo().getCity());

        //
        // LED Light
        //
//        mBinding.cbLEDLight.setChecked(bTmpLEDLight);
//        if (bTmpLEDLight) {
//            mBinding.tvLEDLightStatus.setText("Enabled");
//        } else {
//            mBinding.tvLEDLightStatus.setText("Disabled");
//        }

    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.network_setting_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    public void onDrawableRightClick(View view) {
        switch (view.getId()) {
            case R.id.etNetworkName:
                mBinding.etNetworkName.setEditable();
                mBinding.etNetworkName.setLongClickable(true);
                break;
        }
    }
}
