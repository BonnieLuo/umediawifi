package com.onyx.wifi.view.activity.menubar.dashboard;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNodeListBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.NodeSetInfo;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.adapter.NodeAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.NodeListViewModel;

import java.util.ArrayList;

public class NodeListActivity extends BaseMenuActivity {

    private ActivityNodeListBinding mBinding;
    private NodeListViewModel mViewModel;

    private NodeAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_node_list);
        setView();
        setViewModel();

        // 一開始先顯示上方 router 資料
        setRouterData(getDashboardRouterData());
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.EVENT_DEVICE_INTERNET_STATUS);
        registerReceiver(mEventReceiver, intentFilter);

        // 打 cloud API 以取得最新的資料
        mViewModel.getNodeListForConnTest();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(mEventReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);

        mBinding.imgRouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NodeSettingActivity.class);
                intent.putExtra(AppConstants.EXTRA_DID, DataManager.getInstance().getControllerDid());
                startActivity(intent);
            }
        });

        mAdapter = new NodeAdapter(mContext);
        mBinding.gvDevices.setAdapter(mAdapter);
        mBinding.gvDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DeviceInfo node = (DeviceInfo) mAdapter.getItem(position);
                String did = node.getDid();
                Intent intent = new Intent(mContext, NodeSettingActivity.class);
                intent.putExtra(AppConstants.EXTRA_DID, did);
                startActivity(intent);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setDashboardTitle();
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NodeListViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.Dashboard.GET_NODE_LIST_FOR_CONN_TEST) {
                        if (resultStatus.success) {
                            // 從 node list 取得所有 node did 去打 connection test
                            // (如果 device 的連線狀態有錯, 可以透過 connection test API 去修復)
                            DataManager dataManager = DataManager.getInstance();
                            ArrayList<DeviceInfo> nodeList = dataManager.getNodeList();
                            for (DeviceInfo node : nodeList) {
                                mViewModel.deviceConnectionTest(node.getDid());
                            }

                            // 打完 connection test 後等一下再去 get 資料
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mViewModel.getNodeList();
                                }
                            }, 3000);
                        } else {
                            // 打 get node list 失敗, 不繼續打 connection test, 直接 get 資料
                            mViewModel.getNodeList();
                        }
                    } else if (resultStatus.actionCode == Code.Action.Dashboard.GET_NODE_LIST) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Node List", resultStatus.errorMsg);
                        }
                        setData();
                    }
                }
            }
        });
    }

    private DeviceInfo getDashboardRouterData() {
        DeviceInfo routerDeviceInfo = new DeviceInfo();
        NodeSetInfo nodeSetInfo = mViewModel.getSavedNodeSetInfo();
        if (nodeSetInfo != null) {
            routerDeviceInfo = nodeSetInfo.getDeviceInfo();
            routerDeviceInfo.setOnline(nodeSetInfo.getControllerOnline());
        }
        return routerDeviceInfo;
    }

    private void setRouterData(DeviceInfo routerDeviceInfo) {
        // router image
        switch (routerDeviceInfo.getDeviceType()) {
            case DESKTOP:
                mBinding.imgRouter.setImageResource(R.drawable.pic_hrn22ac);
                break;

            case PLUG:
                mBinding.imgRouter.setImageResource(R.drawable.pic_hex22acp);
                break;

            case TOWER:
            default:
                mBinding.imgRouter.setImageResource(R.drawable.pic_hna22ac);
                break;
        }

        // router internet status
        mBinding.imgInternetStatus.setImageResource(routerDeviceInfo.getOnline() ? R.drawable.ic_internet_status_online : R.drawable.ic_internet_status_offline_purple);

        // router device label
        mBinding.tvDeviceLabel.setText(routerDeviceInfo.getDeviceLabel());
    }

    private void setData() {
        DataManager dataManager = DataManager.getInstance();

        ArrayList<DeviceInfo> nodeList = dataManager.getNodeList();
        ArrayList<DeviceInfo> nodeListWithoutRouter = new ArrayList<>();
        DeviceInfo routerDeviceInfo = new DeviceInfo();
        if (nodeList != null) {
            for (DeviceInfo node : nodeList) {
                if (node.getDid().equals(dataManager.getControllerDid())) {
                    routerDeviceInfo = node;
                } else {
                    nodeListWithoutRouter.add(node);
                }
            }
        }

        // 上半部 router 資訊
        setRouterData(routerDeviceInfo);

        // 下半部 node 資訊
        mAdapter.setNodeList(nodeListWithoutRouter);
        mAdapter.notifyDataSetChanged();
    }

    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace(action);
            if (action.equals(AppConstants.EVENT_DEVICE_INTERNET_STATUS)) {
                setData();
            }
        }
    };
}
