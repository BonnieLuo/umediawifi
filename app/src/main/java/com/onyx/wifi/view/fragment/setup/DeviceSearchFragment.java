package com.onyx.wifi.view.fragment.setup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.FragmentDeviceSearchBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.fragment.BaseFragment;
import com.onyx.wifi.viewmodel.interfaces.setup.DeviceSetupListener;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.setup.BleScanViewModel;

public class DeviceSearchFragment extends BaseFragment {

    private FragmentDeviceSearchBinding mBinding;
    private BleScanViewModel mViewModel;

    private DeviceSetupListener mEventListener;
    private Handler mHandler;

    // stops scanning after 5 seconds
    private static final long SCAN_PERIOD = 5000;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeviceSetupListener) {
            mEventListener = (DeviceSetupListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement DeviceSetupListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_device_search, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewModel();

        mHandler = new Handler();
    }

    private void setViewModel() {
        // 取得 Activity 範圍的 view model instance 以和依附的 Activity 及其他 Fragment 共用資料
        mViewModel = ViewModelProviders.of(mActivity).get(BleScanViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();

        mViewModel.startScan();
        mHandler.postDelayed(mScanTimeoutRunnable, SCAN_PERIOD);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mViewModel.isScanning()) {
            mViewModel.stopScan();
            mHandler.removeCallbacks(mScanTimeoutRunnable);
        }
    }

    private Runnable mScanTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            mViewModel.stopScan();

            int findDeviceNumber = mViewModel.getDeviceList().size();
            if (findDeviceNumber == 0) {
                LogUtils.trace("Find no device");
                mEventListener.onContinue(Code.Action.DeviceSetup.DEVICE_NOT_FOUND);
            } else if (findDeviceNumber == 1) {
                LogUtils.trace("Find one device");
                AppConstants.SetupMode setupMode = DataManager.getInstance().getSetupMode();
                if (setupMode == AppConstants.SetupMode.ROUTER || setupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
                    mEventListener.onContinue(Code.Action.DeviceSetup.ROUTER_FOUND);
                } else {
                    mEventListener.onContinue(Code.Action.DeviceSetup.REPEATER_FOUND);
                }
            } else {
                LogUtils.trace("Find multiple device");
                mEventListener.onContinue(Code.Action.DeviceSetup.MULTIPLE_DEVICE_FOUND);
            }
        }
    };
}
