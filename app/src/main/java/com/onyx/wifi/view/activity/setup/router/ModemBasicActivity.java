package com.onyx.wifi.view.activity.setup.router;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityModemBasicBinding;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.view.activity.base.BaseTitleActivity;

public class ModemBasicActivity extends BaseTitleActivity {

    private ActivityModemBasicBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_modem_basic);
        setView();
    }

    private void setView() {
        setTitleBar();

        mBinding.clModemTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleExpandableItem(mBinding.clModemTitle, mBinding.clModemContent, mBinding.tvModemTitle, mBinding.imgExpandModem);
            }
        });
        mBinding.clModemRouterTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleExpandableItem(mBinding.clModemRouterTitle, mBinding.clModemRouterContent, mBinding.tvModemRouterTitle, mBinding.imgExpandModemRouter);
            }
        });
        mBinding.clDslModemRouterTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleExpandableItem(mBinding.clDslModemRouterTitle, mBinding.clDslModemRouterContent, mBinding.tvDslModemRouterTitle, mBinding.imgExpandDslModemRouter);
            }
        });
        mBinding.clWiredHomeTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleExpandableItem(mBinding.clWiredHomeTitle, mBinding.clWiredHomeContent, mBinding.tvWiredHomeTitle, mBinding.imgExpandWiredHome);
            }
        });
        mBinding.clVerizonFiosTvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleExpandableItem(mBinding.clVerizonFiosTvTitle, mBinding.clVerizonFiosTvContent, mBinding.tvVerizonFiosTvTitle, mBinding.imgExpandVerizonFiosTv);
            }
        });
        mBinding.clStaticIpConnectionTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleExpandableItem(mBinding.clStaticIpConnectionTitle, mBinding.clStaticIpConnectionContent, mBinding.tvStaticIpConnectionTitle, mBinding.imgExpandStaticIpConnection);
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.modem_basic_title);
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    private void toggleExpandableItem(ConstraintLayout itemTitle, ConstraintLayout itemContent, final TextView tvItemTitle, ImageView imgExpandItem) {
        if (itemContent.getVisibility() == View.GONE) {
            tvItemTitle.setTextColor(CommonUtils.getColor(mContext, R.color.purple_4e1393));
            imgExpandItem.setImageResource(R.drawable.ic_chevrons_close);
            itemContent.setVisibility(View.VISIBLE);

            // 項目展開後有可能因為在螢幕下方超出顯示範圍而部份被擋住, 故捲動螢幕到展開的項目的上緣, 讓展開的內容可見
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    mBinding.scrollView.scrollTo(0, itemTitle.getTop());
                }
            });
        } else {
            tvItemTitle.setTextColor(CommonUtils.getColor(mContext, R.color.gray_676767));
            imgExpandItem.setImageResource(R.drawable.ic_chevrons_open);
            itemContent.setVisibility(View.GONE);
        }
    }
}
