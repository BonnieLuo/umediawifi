package com.onyx.wifi.view.activity.mainmenu.parentalcontrol;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityParentalControlBinding;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;

public class ParentalControlActivity extends BaseMenuActivity {

    private ActivityParentalControlBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_parental_control);
        setView();
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.parental_control_title);
        mBinding.titleBar.setLeftButtonVisibility(View.INVISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }
}
