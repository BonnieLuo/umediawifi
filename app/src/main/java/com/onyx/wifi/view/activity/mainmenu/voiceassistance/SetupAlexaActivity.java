package com.onyx.wifi.view.activity.mainmenu.voiceassistance;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.amazon.identity.auth.device.AuthError;
import com.amazon.identity.auth.device.api.authorization.AuthCancellation;
import com.amazon.identity.auth.device.api.authorization.AuthorizationManager;
import com.amazon.identity.auth.device.api.authorization.AuthorizeListener;
import com.amazon.identity.auth.device.api.authorization.AuthorizeRequest;
import com.amazon.identity.auth.device.api.authorization.AuthorizeResult;
import com.amazon.identity.auth.device.api.authorization.ScopeFactory;
import com.amazon.identity.auth.device.api.workflow.RequestContext;
import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivitySetupAlexaBinding;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.audio.AudioDeviceInfo;
import com.onyx.wifi.utility.audio.UdpReceiveProtocol;
import com.onyx.wifi.utility.audio.UdpSendProtocol;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.customized.TitleBar;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.DeviceInfo;
import com.onyx.wifi.viewmodel.mainmenu.VoiceAssistantViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class SetupAlexaActivity extends BaseMenuActivity {

    private ActivitySetupAlexaBinding mBinding;
    private VoiceAssistantViewModel mViewModel;

    private RequestContext mRequestContext; // Bonnie 2
    private UdpReceiveProtocol mUdpReceiveProtocol;  // use the one created in VoiceAssistanceActivity

    private Handler handler = null;
    //final ConfirmDialog mRequestAmazonDialog = new ConfirmDialog();
    //final WarningDialog mLinkingTimeoutDialog = new WarningDialog();
    final MessageDialog mLinkingTimeoutDialog = new MessageDialog();

    DeviceInfo mDeviceInfo = new DeviceInfo("", "", "", "", "", "", "");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_alexa);
        setViewModel();
        setView();
        // reset status value
        CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "false");
        CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, "");

        mLinkingTimeoutDialog.setTitle("Linking Timeout");
        mLinkingTimeoutDialog.setContent("You have to set up the device again.");
        mLinkingTimeoutDialog.setOkButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mViewModel.setUnRegister("0", CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_UDID));

                // (移到unregister成功之後)x
                mLinkingTimeoutDialog.dismiss();
                Intent intent = new Intent(mContext, VoiceAssistanceActivity.class);
                startActivity(intent);

                CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "true");

                finish();
            }
        });
        mLinkingTimeoutDialog.setCancelable(false);

        // Bonnie 2&4
        // 2. Initialize the RequestContext
        mRequestContext = RequestContext.create(this);
        // 4. Create an instance of AuthorizeListenerImpl and register it.
        //    Create an instance of your AuthorizeListenerImpl and register it with your RequestContext instance.
        mRequestContext.registerListener(new AuthorizeListenerImpl());

        //
        // Step 1&2: Send and Receive audio device info through UDP socket
        //
        sendDeviceInfoRequest();

        try {
            LogUtils.trace("Bonnie", "[create Receive Thread]");
            receiveDeviceInfo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //
        // Step 3-1: Send user consent
        //
        sendConfirmRequest();
    }

    //
    // step 1: Request Product Metadata
    //
    private void sendDeviceInfoRequest() {
        JsonObject requestJson = new JsonObject();
        requestJson.addProperty("State", "Hello");
        requestJson.addProperty("Mode", "Broadcast");
        new UdpSendProtocol().execute(requestJson.toString(), CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP));//"192.168.1.138");//"192.168.2.100");
        LogUtils.trace("Bonnie_ip", "sendDeviceInfoRequest: " + CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP));
    }

    //
    // step 2: Receive Product Metadata
    //
    private void receiveDeviceInfo() throws IOException {
        DeviceInfo deviceInfo = new DeviceInfo("", "", "", "", "", "", "");
        mViewModel.getNXPDeviceInfo().setValue(deviceInfo);
        if (mUdpReceiveProtocol == null) {
            mUdpReceiveProtocol = new UdpReceiveProtocol(AppConstants.UDP_PORT, (AudioDeviceInfo) new AudioDeviceInfo() {

                @Override
                public void updateAudioDeviceInfo(DeviceInfo deviceInfo) {
                    mViewModel.getNXPDeviceInfo().postValue(deviceInfo);
                    checkDeviceStatus();

                    LogUtils.trace("socket" + "(+)----------------------------------------------");
                    LogUtils.trace(mViewModel.getNXPDeviceInfo().getValue().toString());
                    LogUtils.trace("socket" + "(-)----------------------------------------------");

                }
            });

            mUdpReceiveProtocol.start();
        }
    }

    // step 3-1: User Consent
    private void sendConfirmRequest() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                JsonObject requestJson = new JsonObject();
                requestJson.addProperty("State", "ConfirmationRequest");
                new UdpSendProtocol().execute(requestJson.toString(), CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP));//"192.168.2.100");
                LogUtils.trace("Bonnie_ip", "sendConfirmRequest: " + CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP));
            }
        }, 500);
    }

    /**
     * stop UDP listener socket
     */
    private void stopReceiveSocket() {
        //mDisplayNotifications.dismissDialog()
        //removeHandler()
        LogUtils.trace("Bonnie", "stop receive socket!");
        if (mUdpReceiveProtocol != null) {
            mUdpReceiveProtocol.setRunning(false);
            mUdpReceiveProtocol = null;
        }
    }

    //----- Start of Linking Timeout Sett-----------------------------------------------------------
    private Handler mLinkingHandler;
    private Runnable mLinkingTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            stopReceiveSocket();// 執行unregistert成功再關socket(onboarding卡住的狀況unregister前關socket，audio fw會luck住，所以移到別的出口關)x
            LogUtils.trace("Time out dialog");

            CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "true"); //防止user操作dialog前把app滑掉
            unregister();
            if (mLinkingTimeoutDialog != null && !mLinkingTimeoutDialog.isAdded()) {
                showDialog(mLinkingTimeoutDialog);
            }
        }
    };

    private void removeLinkingHandler() {
        LogUtils.trace("Bonnie", "remove linking handler!!!");
        if (mLinkingHandler != null) {
            mLinkingHandler.removeCallbacks(mLinkingTimeoutRunnable);
            mLinkingTimeoutRunnable = null;
            mLinkingHandler = null;
        }
    }

    private void checkDeviceStatus() {
        // TODO
        String status = CommonUtils.getSharedPrefData(AppConstants.DEVICE_STATUS);
        //String status = mViewModel.getNXPDeviceInfo().getValue().getDeviceStatus();
        LogUtils.trace("Bonnie", "status: " + status);
        if (!status.equals("")) {

            if (status.equals(AppConstants.DEVICE_STATUS_CONNECTION_COMPLETE) ||  // <= conditions for stopping timeout
                //status.equals(AppConstants.DEVICE_STATUS_CONNECTION_ERROR) ) {// ||
                status.equals(AppConstants.DEVICE_AMAZON_REQUEST)) {   // !!!!! 為了不要因為user閒置website而timeout
                removeLinkingHandler();
                LogUtils.trace("Bonnie", "[ ! complete or amazon request] linking handler!!!");
            } else {
                // close last timer and start new timer
                if (mLinkingHandler != null) {
                    removeLinkingHandler();
                }
                mLinkingHandler = new Handler(Looper.getMainLooper());
                mLinkingTimeoutRunnable = new Runnable() {
                    @Override
                    public void run() {
                        stopReceiveSocket();//要加回來???
                        LogUtils.trace("Time out dialog");
                        CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "true"); //防止user操作dialog前把app滑掉
                        unregister();
                        if (mLinkingTimeoutDialog != null && !mLinkingTimeoutDialog.isAdded()) {
                            showDialog(mLinkingTimeoutDialog);
                        }
                    }
                };
                mLinkingHandler.postDelayed(mLinkingTimeoutRunnable, 50000);
                LogUtils.trace("Bonnie", "[ ! complete or amazon request] linking handler!!!");

            }
        }
        if (status == "" ||
                status.equals(AppConstants.DEVICE_STATUS_CONNECTION_COMPLETE) ||
                status.equals(AppConstants.DEVICE_STATUS_CONNECTION_ERROR)) {
            CommonUtils.setSharedPrefData(AppConstants.SHARED_KEY_LINKING_PROCESS, "false");
        } else {
            CommonUtils.setSharedPrefData(AppConstants.SHARED_KEY_LINKING_PROCESS, "true");
        }
    }
    //----- End of Linking Timeout -----------------------------------------------------------------

    private void unregister() {
        //int index = Integer.valueOf(CommonUtils.getSharedPrefData(AppConstants.KEY_SELECTED_INDEX)); //有取得list才執行unregister的動作

        if (CommonUtils.getSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA).equals("true")) {
           // LogUtils.trace("Bonnie_reset", "[unregister]" + " " + index + " " + mViewModel.getmAudioDeviceList().get(index).getIP());

            mViewModel.setUnRegister("alexa_register", CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_DID));
            CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "false");
        }
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(VoiceAssistantViewModel.class);

        setLoadingObserve(mViewModel, this, mBinding.linkAlexa);
        //MenuBar menuBar = mBinding.linkAlexa.findViewById(R.id.menuBar);
        //setMenuBar(menuBar);

        final TextView tvProgress = mBinding.linkAlexa.findViewById(R.id.tvProgress);
        mViewModel.getNXPDeviceInfo().observe(this, new Observer<DeviceInfo>() {
            @Override
            public void onChanged(@Nullable DeviceInfo deviceInfo) {
                LogUtils.trace("Observe Status" + deviceInfo.getDeviceStatus());

                if (!deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_STATUS_CONNECTION_COMPLETE)) {
                    if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_AMAZON_REQUEST)) {
                        tvProgress.setText("30%");
                    } else if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_AMAZON_RESPONSE)) {
                        tvProgress.setText("40%");
                    } else if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_SEND_AUTH_TO_PRODUCT)) {
                        tvProgress.setText("50%");
                    } else if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_STATUS_MQTT_SUBSCRIBED)) {
                        tvProgress.setText("60%");
                    } else if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_STATUS_REGISTER_REQUEST)) {
                        tvProgress.setText("80%");
                    } else if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_STATUS_REGISTER_RESPONSE)) {
                        tvProgress.setText("90%");
                    } else {
                        LogUtils.trace("##################" + deviceInfo.getDeviceStatus());
                        //if (!deviceInfo.getDeviceStatus().equals(""))
                        //   tvProgress.setText(AppConstants.DEVICE_STATUS_ERROR);
                    }
                } else {
                    //mViewModel.getIsLoading().setValue(false);
                    LogUtils.trace("Connection complete");
                    // 要做下面兩件事的時機:(即需要重新onboard的時機)
                    // (1) 按左鍵回audio device list
                    // (2) onboard完成
                    // (3) 離開此畫面
                    // 另外，timeout時stopReceiveSocket
                    removeLinkingHandler();
                    stopReceiveSocket();

                    Intent intent = new Intent(mContext, AlexaSetupFinishActivity.class);
                    startActivity(intent);
                    finish();
                }

                // linking timeout -> deregister
                /*mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
                    @Override
                    public void onChanged(@Nullable ResultStatus resultStatus) {
                        if (resultStatus != null) {
                            if (resultStatus.actionCode == Code.Action.VoiceAssistant.SET_UNREGISTER) {
                                if (resultStatus.success) {
                                    //mViewModel.getAudioDeviceInfoList();
                                    //updateVoiceDevicesList();
                                } else {
                                    showMessageDialog("Unregister", resultStatus.errorMsg);
                                }
                            }
                        }
                    }
                });*/


                ////////////////////
//                if (deviceInfo.getDeviceStatus().equals(AppConstants.DEVICE_STATUS_CONNECTION_COMPLETE)) {
//                    mViewModel.getIsLoading().setValue(true);
//                    LogUtils.trace("Connection complete");
//                    // 要做下面兩件事的時機:(即需要重新onboard的時機)
//                    // (1) 按左鍵回audio device list
//                    // (2) onboard完成
//                    // (3) 離開此畫面
//                    // 另外，timeout時stopReceiveSocket
//                    removeLinkingHandler();
//                    stopReceiveSocket();
//
//                    Intent intent = new Intent(mContext, AlexaSetupFinishActivity.class);
//                    startActivity(intent);
//                    finish();
//                }

            }
        });

//        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
//            @Override
//            public void onChanged(@Nullable ResultStatus resultStatus) {
//                if (resultStatus != null) {
//                    if (resultStatus.actionCode == Code.Action.VoiceAssistant.SET_UNREGISTER) {
//                        if (resultStatus.success) {
//                            stopReceiveSocket();
//                            // 移到unregister成功之後
//                            mLinkingTimeoutDialog.dismiss();
//                            Intent intent = new Intent(mContext, VoiceAssistanceActivity.class);
//                            startActivity(intent);
//
//                            CommonUtils.setSharedPrefData(AppConstants.KEY_UNREGISTER_ALEXA, "true");
//
//                            finish();
//                        } else {
//                            showMessageDialog("Get Audio Device", resultStatus.errorMsg);
//                        }
//                    }
//                }
//            }
//        });
    }

    private void setView() {
        setTitleBar();
        // only for loading
        TitleBar titleBar = mBinding.linkAlexa.findViewById(R.id.titleBar);
        titleBar.setTitle("Voice Assistant");
        setImmerseLayout(titleBar);
        setMenuBar(mBinding.menuBar);

        mBinding.btnSignInAmazon.setOnClickListener(new View.OnClickListener() {

            // Bonnie 6
            // step 3-2: Request Auth Code
            @Override
            public void onClick(View v) {
                // if onboarding成功，切到下一page
                //Intent intent = new Intent(mContext, SetAlexaLanguageActivity.class);
                //startActivity(intent);
                final JSONObject scopeData = new JSONObject();
                final JSONObject productInstanceAttributes = new JSONObject();

                mViewModel.getIsLoading().setValue(true);

                try {
                    // record status
                    CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, AppConstants.DEVICE_AMAZON_REQUEST);
                    mDeviceInfo.clear();
                    mDeviceInfo.setDeviceStatus(AppConstants.DEVICE_AMAZON_REQUEST);
                    mViewModel.getNXPDeviceInfo().postValue(mDeviceInfo);
                    LogUtils.trace("Bonnie", mViewModel.getNXPDeviceInfo().toString()); //到前景才更新所以get不到
                    // set timeout
                    checkDeviceStatus();

                    productInstanceAttributes.put("deviceSerialNumber", CommonUtils.getSharedPrefData(AppConstants.DEVICE_DSN));
                    scopeData.put("productInstanceAttributes", productInstanceAttributes);
                    scopeData.put("productID", CommonUtils.getSharedPrefData(AppConstants.DEVICE_PRODUCT_ID));

                    AuthorizationManager.authorize(new AuthorizeRequest.Builder(mRequestContext)
                            .addScopes(ScopeFactory.scopeNamed("alexa:voice_service:pre_auth"),
                                    ScopeFactory.scopeNamed("alexa:all", scopeData))
                            .forGrantType(AuthorizeRequest.GrantType.AUTHORIZATION_CODE)
                            .withProofKeyParameters(CommonUtils.getSharedPrefData(AppConstants.DEVICE_CODE_CHALLENGE), AppConstants.CODE_CHALLENGE_METHOD)
                            .build());
                    LogUtils.trace("Happy", "Bonnie 6 (step 3-2: Request Auth Code)");
                } catch (JSONException e) {
                    // handle exception here
                }
            }
        });

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRequestContext.onResume(); // Bonnie 5
        //swapBackToVoiceDeviceRecycler();
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.voice_assistance_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtils.trace("Bonnie", "[onDestroy]");
        removeLinkingHandler();
        stopReceiveSocket(); // (onboarding卡住的狀況unregister前關socket，audio fw會luck住，所以移到別的出口關)x
    }

    // Bonnie 3
    private class AuthorizeListenerImpl extends AuthorizeListener {

        /* Authorization was completed successfully. */
        @Override
        public void onSuccess(final AuthorizeResult authorizeResult) {
            // record status
            CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, AppConstants.DEVICE_AMAZON_RESPONSE);
            mDeviceInfo.clear();
            mDeviceInfo.setDeviceStatus(AppConstants.DEVICE_AMAZON_RESPONSE);
            mViewModel.getNXPDeviceInfo().postValue(mDeviceInfo);
            LogUtils.trace("Bonnie", mViewModel.getNXPDeviceInfo().toString());
            // set timeout
            checkDeviceStatus();

            handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // step 4: Receive Auth Code from LWA
                                        final String authorizationCode = authorizeResult.getAuthorizationCode();
                                        final String redirectUri = authorizeResult.getRedirectURI();
                                        final String clientId = authorizeResult.getClientId();

                                        // step 5: Send (1)Auth Code (2)Client ID (3)Redirect URI to audio device
                                        JsonObject requestJson = new JsonObject();
                                        requestJson.addProperty(AppConstants.DEVICE_AUTH_CODE, authorizationCode);
                                        requestJson.addProperty(AppConstants.DEVICE_REDIRECT_URL, redirectUri);
                                        requestJson.addProperty(AppConstants.DEVICE_CLIENT_ID, clientId);
                                        new UdpSendProtocol().execute(requestJson.toString(), CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP));//"192.168.2.100");
                                        LogUtils.trace("Bonnie_ip", "onSuccess: " + CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_IP));
                                        LogUtils.trace("Happy", "Bonnie 3 (step 5: Send (1)Auth Code (2)Client ID (3)Redirect URI to audio device)");
                                    }
                                }

                    , 200);
            //TODO
            // clear handler?

            // record status
            CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, AppConstants.DEVICE_SEND_AUTH_TO_PRODUCT);
            mDeviceInfo.clear();
            mDeviceInfo.setDeviceStatus(AppConstants.DEVICE_SEND_AUTH_TO_PRODUCT);
            mViewModel.getNXPDeviceInfo().postValue(mDeviceInfo);
            LogUtils.trace("Bonnie", mViewModel.getNXPDeviceInfo().toString());
            // set timeout
            checkDeviceStatus();
        }

        /* There was an error during the attempt to authorize the application. */
        @Override
        public void onError(final AuthError authError) {
            // record status
            CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, AppConstants.DEVICE_STATUS_CONNECTION_ERROR);
            mDeviceInfo.clear();
            mDeviceInfo.setDeviceStatus(AppConstants.DEVICE_STATUS_CONNECTION_ERROR);
            mViewModel.getNXPDeviceInfo().postValue(mDeviceInfo);
            LogUtils.trace("Bonnie", mViewModel.getNXPDeviceInfo().toString());
            // set timeout
            checkDeviceStatus();
        }

        /* Authorization was cancelled before it could be completed. */
        @Override
        public void onCancel(final AuthCancellation authCancellation) {
            // record status
            CommonUtils.setSharedPrefData(AppConstants.DEVICE_STATUS, AppConstants.DEVICE_STATUS_CONNECTION_ERROR);
            mDeviceInfo.clear();
            mDeviceInfo.setDeviceStatus(AppConstants.DEVICE_STATUS_CONNECTION_ERROR);
            mViewModel.getNXPDeviceInfo().postValue(mDeviceInfo);
            LogUtils.trace("Bonnie", mViewModel.getNXPDeviceInfo().toString());
            // set timeout
            checkDeviceStatus();
        }
    }

}
