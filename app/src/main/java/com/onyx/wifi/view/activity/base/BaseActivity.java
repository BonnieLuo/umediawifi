package com.onyx.wifi.view.activity.base;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;

import com.onyx.wifi.BuildConfig;
import com.onyx.wifi.R;
import com.onyx.wifi.model.AccountManager;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LauncherBadgeHelper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.login.EmailVerificationActivity;
import com.onyx.wifi.view.activity.login.LoginHomeActivity;
import com.onyx.wifi.view.activity.login.SignInActivity;
import com.onyx.wifi.view.activity.menubar.dashboard.DashboardActivity;
import com.onyx.wifi.view.dialog.BaseDialog;
import com.onyx.wifi.view.dialog.InfoDialog;
import com.onyx.wifi.view.dialog.MessageDialog;
import com.onyx.wifi.viewmodel.BaseViewModel;

public class BaseActivity extends AppCompatActivity {

    protected Context mContext;
    protected FragmentManager mFragmentManager;

    protected AccountManager mAccountManager;

    private View mLoadingView;

    private static int gNotifyID = 0; // 通知的識別號碼
    private BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            } else if (action == AppConstants.EVENT_DEVICE_ALL_NOTIFY) {
                String command = intent.getStringExtra("NOTIFICATION_CMD");
                String content = intent.getStringExtra("NOTIFICATION_CONTENT");
                CommonUtils.toast(mContext, command);

                //系統通知
                final Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); // 通知音效的URI，在這裡使用系統內建的通知音效
                //final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE); // 取得系統的通知服務
                //final Notification notification = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_toy).setContentTitle(command).setContentText(content).setSubText("cmd name: ").setSound(soundUri).build(); // 建立通知

                final Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle(); // 建立BigTextStyle
                bigTextStyle.setBigContentTitle(command); // 當BigTextStyle顯示時，用BigTextStyle的setBigContentTitle覆蓋setContentTitle的設定
                bigTextStyle.bigText(content); // 設定BigTextStyle的文字內容

                final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE); // 取得系統的通知服務
                final Notification notification = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_toy).setContentTitle("notify cmd #" + gNotifyID).setContentText(command).setStyle(bigTextStyle).setSound(soundUri).build(); // 建立通知

                // for桌面圖示右上角圖標
                LauncherBadgeHelper.setBadgeCount(mContext, gNotifyID);

                notificationManager.notify(gNotifyID++, notification); // 發送通知
                if (gNotifyID == 15)
                    gNotifyID = 0;

            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mFragmentManager = getSupportFragmentManager();
        mAccountManager = AccountManager.getInstance();

        final View rootView = getWindow().getDecorView().getRootView();
        // 為最外層的 root view 設定 touch 事件, 實現「點選 EditText 以外的地方即自動隱藏鍵盤」.
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                rootView.requestFocus();
                hideKeyboard();
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppConstants.CloudServerMode cloudServerMode = DataManager.getInstance().getCloudServerMode();
        if (cloudServerMode != AppConstants.CloudServerMode.PRODUCTION) {
            // register BroadcastReceiver
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(AppConstants.EVENT_DEVICE_ALL_NOTIFY);
            registerReceiver(mEventReceiver, intentFilter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppConstants.CloudServerMode cloudServerMode = DataManager.getInstance().getCloudServerMode();
        if (cloudServerMode != AppConstants.CloudServerMode.PRODUCTION) {
            // unregister BroadcastReceiver
            try {
                unregisterReceiver(mEventReceiver);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();

        int count = mFragmentManager.getBackStackEntryCount();
        if (count > 0) {
            mFragmentManager.popBackStackImmediate();
        } else if (isNotInLoadingStatus()) {
            // 頁面不處於 loading 狀態時才允許結束頁面
            super.onBackPressed();
            // animation function is pending
//            overridePendingTransition(0, R.anim.push_right_side_out);
        }
    }

    protected boolean isNotInLoadingStatus() {
        if (mLoadingView == null) {
            // 該頁面沒有 loading view, 所以不會處於 loading 狀態
            return true;
        } else if (mLoadingView.getVisibility() != View.VISIBLE) {
            // 該頁面有 loading view 但現在看不見, 所以不是處於 loading 狀態
            return true;
        }
        return false;
    }

    protected void startActivityWithAnimation(Class<?> activityClass) {
        Intent intent = new Intent(mContext, activityClass);
        startActivity(intent);
        overridePendingTransition(R.anim.push_right_side_in, 0);
    }

//    protected void startAccountSettingActivity(Activity activity) {
//        Intent intent = new Intent(activity, AccountSettingActivity.class);
//        startActivity(intent);
//    }

    // 新增 fragment 為第一頁, 不用加入 back stack
    // 新增為第一頁時, 有幾種可能的情況:
    // (1) 新開啟一個 Activity, layout container 是空白的 => 新增 fragment 後才看得到第一個 view
    // (2) Activity 的 layout container 已有 view 了 => 新增 fragment 會覆蓋原本的 view, 成為新的第一頁
    protected void addFragmentAsFirstPage(int id, Fragment fragment, String tag) {
        if (null == mFragmentManager) {
            mFragmentManager = getSupportFragmentManager();
        }
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(id, fragment, tag);
        fragmentTransaction.commitAllowingStateLoss();
    }

    // 新增 fragment 並將其加入 back stack
    protected void addFragment(int id, Fragment fragment, String tag) {
        if (null == mFragmentManager) {
            mFragmentManager = getSupportFragmentManager();
        }

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        // animation function is pending
//        fragmentTransaction.setCustomAnimations(
//                R.anim.push_right_side_in,
//                R.anim.push_right_side_out,
//                R.anim.push_right_side_in,
//                R.anim.push_right_side_out);
        fragmentTransaction.add(id, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void removeFragment(Fragment fragment) {
        if (null == mFragmentManager) {
            mFragmentManager = getSupportFragmentManager();
        }

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void hideKeyboard() {
        CommonUtils.hideKeyboard(this);
    }

    protected void showKeyboard(View view) {
        CommonUtils.showKeyboard(this, view);
    }

    protected void setLoadingObserve(BaseViewModel viewModel, LifecycleOwner owner, final View viewLoading) {
        mLoadingView = viewLoading;
        viewModel.getIsLoading().observe(owner, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (mLoadingView != null) {
                    if (aBoolean != null && aBoolean) {
                        mLoadingView.setVisibility(View.VISIBLE);
                    } else {
                        mLoadingView.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    protected void setForceEmailVerify(BaseViewModel viewModel, LifecycleOwner owner) {
        viewModel.getForceEmailVerify().observe(owner, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null && aBoolean) {
                    // 打 cloud API 收到回覆 4013 (unverified email), 需要強制 user 認證 email.
                    Intent intent = new Intent(mContext, EmailVerificationActivity.class);
                    intent.putExtra(AppConstants.EXTRA_AUTO_RESEND_VERIFY_CODE, true);
                    startActivity(intent);
                    if (mContext instanceof SignInActivity) {
                        /* 如果是在登入頁面發生此情況, 代表登入的動作已經完成, 所以結束自身頁面.
                           若是在其他頁面則不結束自身, 這樣 user 認證完 email 後會回到原本的頁面, 可以繼續剛剛的操作. */
                        finish();
                    }
                }
            }
        });
    }

    // 登入完成, 回到 LoginHome 頁面, 由其判斷畫面的跳轉
    public void afterSignInDone(Activity activity) {
        Intent intent = new Intent(activity, LoginHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    // 將使用者引導至登入畫面, 使用時機如下:
    // (1) 使用者登出 App
    // (2) 使用者修改 email or password 後, App 需讓使用者重新登入
    // (3) 一般頁面 onResume() 時若判斷到 current user == null, 代表現在 App 沒有登入中的使用者, 需回到登入頁面
    public void startLoginHomeActivity(Activity activity) {
        Intent intent = new Intent(activity, LoginHomeActivity.class);
        // 清空現在 history stack 裡面所有的 Activity, 開啟 LoginHomeActivity 並令其為第一個頁面
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void startDashboardActivity(Activity activity) {
        Intent intent = new Intent(activity, DashboardActivity.class);
        startActivity(intent);
    }

    // 檢查網路狀態, 若網路不通會出現提示訊息, 若網路有通則回傳 true, UI 繼續執行原本的動作
    protected boolean checkInternetStatus() {
        if (!CommonUtils.isInternetAvailable(mContext)) {
            showMessageDialog("Internet Is Not Available", getString(R.string.err_internet_not_available));
            return false;
        }
        return true;
    }

    // showDialog(BaseDialog dialog) 用來顯示 App 自定義的 dialog
    // message/info dialog 較簡單, 只要設定標題和內容就可以顯示, 故寫成 function. (showMessageDialog/showInfoDialog)
    // confirm/warning dialog 除了標題和內容之外還要設定 button 文字和 click 事件, 不適合寫成 function. (因參數會過多)
    // 若要使用 confirm/warning dialog, 或是要為 message dialog 設定自定義的點擊事件,
    // 要自己 new dialog 並做完各項設定後, 呼叫 showDialog() 來顯示.
    public void showDialog(BaseDialog dialog) {
        dialog.show(mFragmentManager, dialog.getTitle());
        LogUtils.trace(dialog.toString());
    }

    // showMessageDialog() 會建立一個 message dialog 並顯示, 其中 OK button 會設定預設的點擊事件: 關閉 dialog, 不會做其他動作.
    // 若要為 message dialog 設定自定義的點擊事件, 要自己 new dialog 並做完各項設定後, 呼叫 showDialog() 來顯示.
    public void showMessageDialog(String title, String msg) {
        // message dialog 也可以設定 OK button listener, 但不能寫成 function 當成參數傳入, click listener 裡面會需要呼叫 dialog.dismiss()
        // 但 function 裡面才會建立出 dialog, 所以 click listener 不可能從 function 外面當成參數傳入
        MessageDialog messageDialog = new MessageDialog();
        messageDialog.setTitle(title);
        messageDialog.setContent(msg);
        showDialog(messageDialog);
    }

    public void showInfoDialog(String title, String msg) {
        InfoDialog infoDialog = new InfoDialog();
        infoDialog.setTitle(title);
        infoDialog.setContent(msg);
        showDialog(infoDialog);
    }

    protected boolean isPermissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    protected void requestPermission(String[] permissions, int requestCode) {
        String allPermission = "";
        for (int i = 0; i < permissions.length; i++) {
            allPermission += permissions[i];
        }
        LogUtils.trace("request permission: " + allPermission);
        ActivityCompat.requestPermissions(BaseActivity.this, permissions, requestCode);
    }

    protected boolean isNeedShowPermissionGuide(String permission) {
        // 如果系統詢問權限時使用者選取 "Never ask again" 並選 deny, 之後當 App 再次要求權限時, 系統會直接說沒有要到權限,
        // 並且所有要求權限的系統視窗通通不會再出現了 (此種情況下 shouldShowRequestPermissionRationale 會回傳 false)
        // 但 App 還是需要權限才能繼續往下運作, 故必須顯示額外的說明, 引導使用者到系統的設定頁面手動給予 App 權限
        // 請參考: https://cire.pixnet.net/blog/post/45886196-%E5%A6%82%E4%BD%95%E5%9C%A8-android-m-%E8%99%95%E7%90%86-runtime-permissions
        return !ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
    }

    protected void guideToSettingsOfApp() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected boolean showAppInfo() {
        if (BuildConfig.DEBUG || BuildConfig.IS_RDQA) {
            String buildVariant = BuildConfig.FLAVOR + " " + BuildConfig.BUILD_TYPE;
            String version = BuildConfig.VERSION_NAME;
            AppConstants.CloudServerMode cloudServerMode = DataManager.getInstance().getCloudServerMode();
            showMessageDialog("App Info", "Build variant: " + buildVariant + "\n" + "App version: " + version + "\n" +
                    "Cloud server: " + cloudServerMode.name());
            return true;
        }
        return false;
    }
}
