package com.onyx.wifi.view.activity.mainmenu.networksetting;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityNetworkTimezoneBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.view.activity.mainmenu.voiceassistance.SetAlexaLanguageActivity;
import com.onyx.wifi.view.adapter.LanguageAdapter;
import com.onyx.wifi.view.adapter.TimezoneAdapter;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.mainmenu.NetworkSettingViewModel;

public class NetworkTimezoneActivity extends BaseMenuActivity implements TimezoneAdapter.OnItemClickHandler{
    private ActivityNetworkTimezoneBinding mBinding;
    private NetworkSettingViewModel mViewModel;

    private PopupWindow timezonePopupWindow;
    private RecyclerView mRvTimezoneList = null;
    private TimezoneAdapter mAdapter;
    private View popTimezoneView = null;
    private String mSelectedTimezone = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_network_timezone);

        setViewModel();
        showTimezonePopwindow(); //需透過viewmodel取得timezone array，所以必須在setViewModel()之後
        setView();
    }

    private void setUpTimezoneRecycler() {
        //
        // Set up Recycler View for Voice Devices
        //
        if (mRvTimezoneList != null) {

            mRvTimezoneList.setLayoutManager(new LinearLayoutManager(NetworkTimezoneActivity.this));
            // 設置格線
            //1.用系統提供的高度和顏色,不做自定義
            //mRvTimezoneList.addItemDecoration(new DividerItemDecoration(NetworkTimezoneActivity.this, DividerItemDecoration.VERTICAL));
            DividerItemDecoration dec = new DividerItemDecoration(NetworkTimezoneActivity.this, DividerItemDecoration.VERTICAL);
            Drawable drawable = ContextCompat.getDrawable(mContext,R.drawable.shape_list_divider);
            dec.setDrawable(drawable);

            mRvTimezoneList.addItemDecoration(dec);

            // 將資料交給adapter
            // (修改Adapter建立方式，this指此Activity，因為我們已經implement所以此Activity本身就是一個OnItemClickHandler)
            mAdapter = new TimezoneAdapter(NetworkTimezoneActivity.this, mViewModel.getTimezoneList(), this);
            mRvTimezoneList.setAdapter(mAdapter);
        }
    }

    /**
     * 设置页面的透明度
     * @param bgAlpha 1表示不透明
     */
    public static void setBackgroundAlpha(Activity activity, float bgAlpha) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        if (bgAlpha == 1) {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug
        } else {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug
        }
        activity.getWindow().setAttributes(lp);
    }


    /**
     * 显示popupWindow
     */
    private void showTimezonePopwindow() {
        //加载弹出框的布局
        popTimezoneView = LayoutInflater.from(NetworkTimezoneActivity.this).inflate(
                R.layout.view_pop_timezone, null);
        mRvTimezoneList = popTimezoneView.findViewById(R.id.rvTimezoneList);
        setUpTimezoneRecycler();
        timezonePopupWindow = new PopupWindow(popTimezoneView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        timezonePopupWindow.setFocusable(true);// 取得焦点
        //注意  要是点击外部空白处弹框消息  那么必须给弹框设置一个背景色  不然是不起作用的
        timezonePopupWindow.setBackgroundDrawable(null);
        //点击外部消失
        timezonePopupWindow.setOutsideTouchable(true);
        //设置可以点击
        timezonePopupWindow.setTouchable(true);
        //进入退出的动画，指定刚才定义的style
        timezonePopupWindow.setAnimationStyle(R.style.popwindow_anim_style);

        timezonePopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                setBackgroundAlpha(NetworkTimezoneActivity.this, 1f);
            }
        });

    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(NetworkSettingViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.NetworkSetting.POST_HOME_NETWORK_SETTING) {
                        if (resultStatus.success) {
                            // update Share Preference (Device Info)
                            // TODO: 改成getDashboardInfo(主頁原本背景就會定期call)or什麼都不做，但最兩行要留著
                            DeviceInfo deviceInfo = DataManager.getInstance().getDeviceInfo();
                            deviceInfo.setCity(mSelectedTimezone);
                            DataManager.getInstance().updateDeviceInfo(deviceInfo);

                            mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
                            finish();
                        } else {
                            showMessageDialog("Post Home Network Setting", resultStatus.errorMsg);
                        }
                    }
                }
            }
        });
    }

    private void setView() {
        setTitleBar();
        setMenuBar(mBinding.menuBar);
        mBinding.btnSelectTimezone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timezonePopupWindow.showAtLocation(popTimezoneView, Gravity.BOTTOM, 100, 0);
                setBackgroundAlpha(NetworkTimezoneActivity.this, 0.6f);
            }
        });

        // default timezone
        mBinding.tvSelectedTimezoneValue.setText(DataManager.getInstance().getDeviceInfo().getCity());

        mBinding.titleBar.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mBinding.titleBar.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update timezone to cloud - 成功後更改SharePreference
                mViewModel.postHomeNetworkSetting(mSelectedTimezone, DataManager.getInstance().getNetworkSetting());
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle(R.string.network_timezone_title);
        mBinding.titleBar.setLeftButtonVisibility(View.VISIBLE);
        mBinding.titleBar.setRightButtonVisibility(View.INVISIBLE);
    }


    @Override
    public void onItemClick(String selectedTimezone) {
        // 與sharepreference裡的值比較，不同時會在右上角顯示勾號
        if (!DataManager.getInstance().getDeviceInfo().getCity().equals(selectedTimezone)) {
            mBinding.titleBar.setRightButtonVisibility(View.VISIBLE);
            mSelectedTimezone = selectedTimezone;
        }
        mBinding.tvSelectedTimezoneValue.setText(mSelectedTimezone);
        timezonePopupWindow.dismiss();
        //CommonUtils.toast(mContext, selectedTimezone);
    }
}
