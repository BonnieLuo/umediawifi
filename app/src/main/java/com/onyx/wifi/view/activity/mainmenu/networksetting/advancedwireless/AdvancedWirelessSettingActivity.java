package com.onyx.wifi.view.activity.mainmenu.networksetting.advancedwireless;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.databinding.ActivityAdvancedWirelessSettingsBinding;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfig;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfigModel;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingList;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringList;
import com.onyx.wifi.model.item.advancewireless.reboot.ScheduleRebootModel;
import com.onyx.wifi.view.activity.base.BaseMenuActivity;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.menubar.advancewireless.AdvanceWirelessSettingsViewModel;

public class AdvancedWirelessSettingActivity extends BaseMenuActivity {

    private ActivityAdvancedWirelessSettingsBinding mBinding;

    private AdvanceWirelessSettingsViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_wireless_settings);

        setView();

        setViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAccountManager.getUser() == null) {
            startLoginHomeActivity(this);
            finish();
        } else {
            mViewModel.getAdvanceWirelessSettings();
        }
    }

    private void setView() {

        setTitleBar();

        setMenuBar(mBinding.menuBar);

        mBinding.ssidEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SsidSettingActivity.class));
                mMenuBar.closeMainMenu();
            }
        });

        mBinding.dhcpEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, DhcpTableActivity.class));
                mMenuBar.closeMainMenu();
            }
        });

        mBinding.dmzEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager dataManager = DataManager.getInstance();

                AdvanceWirelessSettings advanceWirelessSettings = dataManager.getAdvanceWirelessSettings();

                DemilitarizedZoneConfigModel dmzModel = advanceWirelessSettings.getDemilitarizedZoneConfigModel();
                DemilitarizedZoneConfig dmzConfig = new DemilitarizedZoneConfig(dmzModel);

                dataManager.setDmzConfig(dmzConfig);

                startActivity(new Intent(mContext, DemilitarizedZoneActivity.class));
                mMenuBar.closeMainMenu();
            }
        });

        mBinding.portForwardingEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PortForwardingListActivity.class));
                mMenuBar.closeMainMenu();
            }
        });

        mBinding.portTriggeringEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PortTriggeringListActivity.class));
                mMenuBar.closeMainMenu();
            }
        });

        mBinding.scheduledRebootEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, ScheduledRebootActivity.class));
                mMenuBar.closeMainMenu();
            }
        });
    }

    private void setTitleBar() {
        setImmerseLayout(mBinding.titleBar);
        mBinding.titleBar.setTitle("Advanced Network Settings");
        mBinding.titleBar.setLeftButton(R.drawable.ic_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.titleBar.setRightButtonVisibility(View.GONE);
    }

    private void setViewModel() {
        mViewModel = ViewModelProviders.of(this).get(AdvanceWirelessSettingsViewModel.class);
        setLoadingObserve(mViewModel, this, mBinding.loading);
        setForceEmailVerify(mViewModel, this);
        mViewModel.getResultStatus().observe(this, new Observer<ResultStatus>() {
            @Override
            public void onChanged(@Nullable ResultStatus resultStatus) {
                if (resultStatus != null) {
                    if (resultStatus.actionCode == Code.Action.AdvanceWirelessSettings.GET_SETTINGS) {
                        if (!resultStatus.success) {
                            showMessageDialog("Get Advance Wireless Settings", resultStatus.errorMsg);

                            return;
                        }

                        JsonObject json = (JsonObject) resultStatus.data;
                        if (json != null) {
                            setData(json);
                        }
                    }
                }
            }
        });
    }

    private void setData(JsonObject json){
        JsonObject jsonObject = json.getAsJsonObject("data");

        Gson gson = new Gson();

        AdvanceWirelessSettings advanceWirelessSettings = gson.fromJson(jsonObject, AdvanceWirelessSettings.class);

        DataManager dataManager = DataManager.getInstance();
        dataManager.setAdvanceWirelessSettings(advanceWirelessSettings);


        setDMZ(advanceWirelessSettings);

        setPortForwarding(advanceWirelessSettings);

        setPortTriggering(advanceWirelessSettings);

        setScheduledReboot(advanceWirelessSettings);

    }

    private void setDMZ(AdvanceWirelessSettings advanceWirelessSettings) {
        DemilitarizedZoneConfigModel dmzConfigModel = advanceWirelessSettings.getDemilitarizedZoneConfigModel();
        String enable = dmzConfigModel.getEnable();

        if ("1".equalsIgnoreCase(enable)) {
            mBinding.dmzStatusTextView.setText("Enabled");
        } else {
            mBinding.dmzStatusTextView.setText("Disabled");
        }
    }

    private void setPortForwarding(AdvanceWirelessSettings advanceWirelessSettings) {
        PortForwardingList portForwardingList = advanceWirelessSettings.getPortForwardingList();
        String enable = portForwardingList.getEnable();

        if (enable.equalsIgnoreCase("1")) {
            mBinding.portForwardingStatusTextView.setText("Enabled");
        } else {
            mBinding.portForwardingStatusTextView.setText("Disabled");
        }
    }

    private void setPortTriggering(AdvanceWirelessSettings advanceWirelessSettings) {
        PortTriggeringList portTriggeringList = advanceWirelessSettings.getPortTriggeringList();
        String enable = portTriggeringList.getEnable();

        if (enable.equalsIgnoreCase("1")) {
            mBinding.portTriggeringStatusTextView.setText("Enabled");
        } else {
            mBinding.portTriggeringStatusTextView.setText("Disabled");
        }
    }

    private void setScheduledReboot(AdvanceWirelessSettings advanceWirelessSettings) {
        ScheduleRebootModel scheduleRebootModel = advanceWirelessSettings.getScheduleRebootModel();

        if (scheduleRebootModel != null && "1".equals(scheduleRebootModel.getEnable())) {
            mBinding.scheduledRebootStatusTextView.setText("Enabled");
        } else {
            mBinding.scheduledRebootStatusTextView.setText("Disabled");
        }
    }

}
