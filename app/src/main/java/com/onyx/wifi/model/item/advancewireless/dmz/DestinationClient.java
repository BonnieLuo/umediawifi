package com.onyx.wifi.model.item.advancewireless.dmz;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.StringUtils;

import java.util.Map;

public class DestinationClient {

    public DestinationClient(String clientType, String name, String cid, String ip) {
        mClientType = clientType;
        mClientName = StringUtils.encodeToHexString(name);
        mCid = cid;
        mIp = ip;
    }

    @SerializedName("ip")
    private String mIp;

    @SerializedName("client_type")
    private String mClientType;

    @SerializedName("client_name")
    private String mClientName;

    @SerializedName("cid")
    private String mCid;

    public String getIp() {
        return mIp;
    }

    public String getType() {
        return mClientType;
    }

    public String getName() {
        return StringUtils.decodeHexString(mClientName);
    }

    public String getCid() {
        return mCid;
    }

    public int getClientIcon() {
        if (mClientType == null) {
            return 0;
        }

        DataManager dataManager = DataManager.getInstance();

        Map<String, Integer> deviceIconMap = dataManager.getOnlineClientIconMap();

        String lowerCaseClientType = mClientType.toLowerCase();
        if (!deviceIconMap.containsKey(lowerCaseClientType)) {
            return 0;
        }

        return deviceIconMap.get(lowerCaseClientType);
    }
}
