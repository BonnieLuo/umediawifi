package com.onyx.wifi.model.item.advancewireless.ssid;

import com.google.gson.annotations.SerializedName;

public class WirelessModel {

    @SerializedName("5G")
    private Wireless5GModel wirelss5G;

    @SerializedName("2G")
    private Wireless2GModel wirelss2G;

    public Wireless5GModel getWirelss5G() {
        return wirelss5G;
    }

    public Wireless2GModel getWirelss2G() {
        return wirelss2G;
    }

}
