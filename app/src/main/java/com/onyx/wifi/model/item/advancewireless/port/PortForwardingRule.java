package com.onyx.wifi.model.item.advancewireless.port;

import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.StringUtils;

public class PortForwardingRule {

    private final String[] mProtocolArray = {"TCP", "UDP", "TCP & UDP"};

    private PortForwardingRuleModel mModel;

    public PortForwardingRule(PortForwardingRuleModel model) {
        mModel = model;

        String id = mModel.getId();

        mId = id;

        mEnable = mModel.getEnable();

        String name = mModel.getName();
        mName = StringUtils.decodeHexString(name);

        String ip = mModel.getIp();
        mIp = ip;

        String externalPort = mModel.getExternalPort();
        String[] separatedExternalPort = externalPort.split("-");

        if (separatedExternalPort.length == 2) {
            mExternalPortStart = separatedExternalPort[0];
            mExternalPortEnd = separatedExternalPort[1];
        }

        String internalPort = mModel.getInternalPort();
        String[] separatedInternalPort = internalPort.split("-");

        if (separatedInternalPort.length == 2) {
            mInternalPortStart = separatedInternalPort[0];
            mInternalPortEnd = separatedInternalPort[1];
        }

        String protocol = mModel.getProtocol();
        mProtocolIndex = mModel.getProtocolIndex(protocol);
    }

    private String mId;

    public String getId() {
        return mId;
    }

    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getHexName() {
        return StringUtils.encodeToHexString(mName);
    }

    private String mEnable;

    public boolean isEnabled() {
        return mEnable.equalsIgnoreCase("1");
    }

    public void toggleEnable() {
        if (mEnable.equalsIgnoreCase("1")) {
            mEnable = "0";
        } else {
            mEnable = "1";
        }
    }

    private String mExternalPortStart;

    public String getExternalPortStart() {
        return mExternalPortStart;
    }

    public void setExternalPortStart(String startPort) {
        mExternalPortStart = startPort;
    }

    private String mExternalPortEnd;

    public String getExternalPortEnd() {
        return mExternalPortEnd;
    }

    public void setExternalPortEnd(String endPort) {
        mExternalPortEnd = endPort;
    }

    private String mInternalPortStart;

    public String getInternalPortStart() {
        return mInternalPortStart;
    }

    public void setInternalPortStart(String startPort) {
        mInternalPortStart = startPort;
    }

    private String mInternalPortEnd;

    public String getInternalPortEnd() {
        return mInternalPortEnd;
    }

    public void setInternalPortEnd(String endPort) {
        mInternalPortEnd = endPort;
    }

    private Integer mProtocolIndex;

    public String getProtocol() {
        if (mProtocolIndex == -1) {
            return "";
        }

        String protocol = mProtocolArray[mProtocolIndex];

        return protocol;
    }

    public void setProtocol(Integer index) {
        mProtocolIndex = index;
    }

    public String getRuleDescription() {
        String protocol = getProtocol();

        return String.format("%s-%s --> %s-%s %s", mExternalPortStart, mExternalPortEnd, mInternalPortStart, mInternalPortEnd, protocol);
    }

    private String mIp;

    private String mManualIp;

    public String getIp() {
        return mIp;
    }

    public void setIp(String ip) {
        mIp = ip;
        mManualIp = ip;
    }

    public void setManualIp(String ip) {
        mManualIp = ip;
    }

    public String getExternalPort() {
        return String.format("%s-%s", mExternalPortStart, mExternalPortEnd);
    }

    public String getInternalPort() {
        return String.format("%s-%s", mInternalPortStart, mInternalPortEnd);
    }

    public boolean isChanged() {
        String modelName = mModel.getDisplayName();
        boolean isNameChanged = !mName.equalsIgnoreCase(modelName);

        String modelEnable = mModel.getEnable();
        boolean isEnableChanged = !mEnable.equalsIgnoreCase(modelEnable);

        String modelIp = mModel.getIp();
        boolean isIpChanged = !mIp.equalsIgnoreCase(modelIp);

        boolean isManualIpChange = !modelIp.equalsIgnoreCase(mManualIp);


        String modelExternalPort = mModel.getExternalPort();
        String externalPort = String.format("%s-%s", mExternalPortStart, mExternalPortEnd);
        boolean isExternalPortChanged = !externalPort.equalsIgnoreCase(modelExternalPort);

        String modelInternalPort = mModel.getInternalPort();
        String internalPort = String.format("%s-%s", mInternalPortStart, mInternalPortEnd);
        boolean isInternalPortChanged = !internalPort.equalsIgnoreCase(modelInternalPort);

        String modelProtocol = mModel.getProtocol();
        int modelProtocolIndex = mModel.getProtocolIndex(modelProtocol);
        boolean isProtocolChanged = mProtocolIndex != modelProtocolIndex;

        return isNameChanged ||
                isEnableChanged ||
                isIpChanged ||
                isManualIpChange ||
                isExternalPortChanged ||
                isInternalPortChanged ||
                isProtocolChanged;
    }

    public boolean isNameValid() {
        return InputUtils.isNameValid(mName);
    }

    public boolean isDeviceValid() {
        return InputUtils.isIpValid(mIp);
    }

    public boolean isExternalPortRangeValid() {
        Integer startPort;
        Integer endPort;

        if (!InputUtils.isNotEmpty(mExternalPortStart) ||
                !InputUtils.isNotEmpty(mExternalPortEnd))
            return false;

        startPort = Integer.parseInt(mExternalPortStart);
        endPort = Integer.parseInt(mExternalPortEnd);

        return isPortValid(startPort) && isPortValid(endPort);
    }

    public boolean isExternalPortRuleValid() {
        Integer startPort = Integer.parseInt(mExternalPortStart);
        Integer endPort = Integer.parseInt(mExternalPortEnd);

        return endPort >= startPort;
    }

    public boolean isInternalPortRangeValid() {

        Integer startPort;
        Integer endPort;

        if (!InputUtils.isNotEmpty(mInternalPortStart) ||
                !InputUtils.isNotEmpty(mInternalPortEnd))
            return false;

        startPort = Integer.parseInt(mInternalPortStart);
        endPort = Integer.parseInt(mInternalPortEnd);

        return isPortValid(startPort) && isPortValid(endPort);
    }

    public boolean isInternalPortRuleValid() {
        Integer startPort = Integer.parseInt(mExternalPortStart);
        Integer endPort = Integer.parseInt(mExternalPortEnd);

        return endPort >= startPort;
    }

    public boolean isPortNumberValid() {
        Integer externalStartPort = Integer.parseInt(mExternalPortStart);
        Integer externalEndPort = Integer.parseInt(mExternalPortEnd);

        Integer internalStartPort = Integer.parseInt(mInternalPortStart);
        Integer internalEndPort = Integer.parseInt(mInternalPortEnd);

        return (externalEndPort - externalStartPort) == (internalEndPort - internalStartPort);
    }

    public boolean isProtocolValid() {
        return mProtocolIndex != -1;
    }

    private boolean isPortValid(Integer port) {
        return port >= 0 && port <= 65535;
    }
}
