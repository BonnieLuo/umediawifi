package com.onyx.wifi.model.item.member;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public abstract class Member {

    public enum MemberType {
        FAMILY,
        HOME,
        GUEST,
        BNB
    }

    MemberType mMemberType;

    String mName = "";

    boolean mExtend = false;

    List<Client> mClientList;
    List<Client> mAllowedClientList = new ArrayList<Client>();


    public MemberType getType() {
        return mMemberType;
    }

    public String getName() {
        return mName;
    };

    public abstract MemberModel getMemberModel();

    public abstract Bitmap getPhoto(Context context);

    public int getClientListSize() {
        return mClientList.size();
    }

    public List<Client> getAllClients() {
        return mClientList;
    }

    //  顯示client list時，disallowed client ("disallow":"1")不能顯示
    public List<Client> getAllAllowedClient() {
        mAllowedClientList.clear();
        for (Client client: mClientList) {
            if (!client.getClientModel().getDisallowed())
                mAllowedClientList.add(client);
        }
        return mAllowedClientList;
    }

    public List<Client> getFirstTreeClients() {
        if (mClientList.size() > 3) {
            return mClientList.subList(0,3);
        }

        return mClientList;
    }

    //  顯示client list時，disallowed client ("disallow":"1")不能顯示
    public List<Client> getFirstThreeAllowedClients() {
        getAllAllowedClient();
        if (mAllowedClientList.size() > 3) {
            return mAllowedClientList.subList(0,3);
        }

        return mAllowedClientList;
    }

    public boolean needExtend() {
        return mExtend;
    }

    public boolean extend() {
        mExtend = !mExtend;

        return mExtend;
    }

}
