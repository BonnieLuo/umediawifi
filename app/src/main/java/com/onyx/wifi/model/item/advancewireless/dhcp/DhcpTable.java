package com.onyx.wifi.model.item.advancewireless.dhcp;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.item.member.ConnectionStatus;

import java.util.ArrayList;
import java.util.List;

public class DhcpTable {

    @SerializedName("client_list")
    private List<DhcpClientModel> mClientList;
    private List<DhcpClientModel> mAllowedClientList = new ArrayList<DhcpClientModel>();

    // for顯示only, 移除disallowed client
    public List<DhcpClientModel> getmClientList() {
        mAllowedClientList.clear();
        for (DhcpClientModel client: mClientList) {
            if (!client.getDisallowed())
                mAllowedClientList.add(client);
        }

        classifyClientByOnlineStatus(mAllowedClientList);
        return mAllowedClientList;
    }

    private void classifyClientByOnlineStatus(List<DhcpClientModel> clientList) {
        ArrayList<DhcpClientModel> onlineList = new ArrayList<DhcpClientModel>();
        ArrayList<DhcpClientModel> offlineList = new ArrayList<DhcpClientModel>();

        onlineList.clear();
        offlineList.clear();

        for (DhcpClientModel clientInfo : clientList) {
            if (clientInfo.getOnlineStatus() == ConnectionStatus.ONLINE) { // online
                onlineList.add(clientInfo);
            } else {                                  // offline
                offlineList.add(clientInfo);
            }
        }

        clientList.clear();
        clientList.addAll(onlineList);
        clientList.addAll(offlineList);
    }

}
