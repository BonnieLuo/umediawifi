package com.onyx.wifi.model.item.member;

import com.google.gson.annotations.SerializedName;

public class PauseInfoModel {

    public PauseInfoModel() {
        mEnable = "0";
    }

    @SerializedName("enable")
    private String mEnable;

    public String getEnable() {
        return mEnable;
    }

    public void setEnable(String enable) {
        mEnable = enable;
    }

    @SerializedName("minute_left")
    private String mMinuteLeft;

    public String getMinuteLeft() {
        return mMinuteLeft;
    }

    @SerializedName("duration")
    private String mDuration;

    public String getDuration() {
        return mDuration;
    }

    public String getDurationString() {
        if (mDuration.equalsIgnoreCase("15")) {
            return "15m";
        }

        if (mDuration.equalsIgnoreCase("30")) {
            return "30m";
        }

        if (mDuration.equalsIgnoreCase("60")) {
            return "1hr";
        }

        if (mDuration.equalsIgnoreCase("120")) {
            return "2hr";
        }

        if (mDuration.equalsIgnoreCase("240")) {
            return "4hr";
        }

        if (mDuration.equalsIgnoreCase("480")) {
            return "8hr";
        }

        if (mDuration.equalsIgnoreCase("720")) {
            return "12hr";
        }

        if (mDuration.equalsIgnoreCase("525600")) {
            return "∞";
        }

        return "";
    }

    @SerializedName("rule_id")
    private String mRuleId;

    public String getRuleId() {
        return mRuleId;
    }

    @SerializedName("type")
    private String mType;

    public String getType() {
        return mType;
    }

    @SerializedName("profile_id")
    private String mProfileId;

    public String getProfileId() {
        return mProfileId;
    }

}
