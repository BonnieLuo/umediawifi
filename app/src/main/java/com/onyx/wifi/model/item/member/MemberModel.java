package com.onyx.wifi.model.item.member;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.item.ClientModel;

import java.util.ArrayList;
import java.util.List;

public class MemberModel {

    /*
    {
        "name": "Thomas Lee Test",
        "member_id": "5d8c329b997a350001681864",
        "pause_info": null,
        "assigned_client_list": []
    }
    */

    @SerializedName("name")
    private String mName;

    @SerializedName("member_id")
    private String mMemberId;

    @SerializedName("pause_info")
    private ArrayList<PauseInfoModel> mPauseInfoModelList;

    @SerializedName("assigned_client_list")
    private List<ClientModel> mClientModelList = new ArrayList<>();

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getMemberId() {
        return mMemberId;
    }

    public void setMemberId(String memberId) {
        mMemberId = memberId;
    }

    public ArrayList<PauseInfoModel> getPauseInfoModelList() {
        return mPauseInfoModelList;
    }

    public List<ClientModel> getClientList() {
        return mClientModelList;
    }

    public void setClientList(List<ClientModel> clientModelList) {
        mClientModelList = clientModelList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }

}
