package com.onyx.wifi.model.item.member;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleConfiguration {

    public ScheduleConfiguration(ScheduleConfigurationModel model) {
        mModel = model;

        mId = mModel.getScheduleId();

        mName = mModel.getName();

        String enable = mModel.getEnable();
        mEnable = !enable.equalsIgnoreCase("0");

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        String startTime = mModel.getStartTime();

        if (startTime != null) {
            try {
                mStartTime = dateFormat.parse(startTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String endTime = mModel.getEndTime();

        if (endTime != null) {
            try {
                mEndTime = dateFormat.parse(endTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String repeat = mModel.getRepeat();
        char[] chars = repeat.toCharArray();
        boolean[] repeatConfig = new boolean[chars.length];
        for (int i = 0; i < chars.length; i++) {
            char character = chars[i];
            repeatConfig[i] = character != '0';
        }

        mFrequency = new ScheduleFrequency(repeatConfig);
    }

    // Model

    private ScheduleConfigurationModel mModel;

    // ID

    private String mId;

    public String getId() {
        return mId;
    }

    // Name

    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String newName) {
        mName = newName;
    }

    // Enable

    private boolean mEnable;

    public boolean isEnabled() {
        return mEnable;
    }

    public void toggleEnable() {
        mEnable = !mEnable;
    }

    // Start Time

    private Date mStartTime;

    public Date getStartTime() {
        return mStartTime;
    }

    public void setStartTime(Date time) {
        mStartTime = time;
    }

    public String getStartTimeString(String format) {
        if (mStartTime == null) {
            return "--";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        String startTimeString = dateFormat.format(mStartTime);

        return startTimeString;
    }

    // End Time

    private Date mEndTime;

    public Date getEndTime() {
        return mEndTime;
    }

    public void setEndTime(Date time) {
        mEndTime = time;
    }

    public String getEndTimeString(String format) {
        if (mEndTime == null) {
            return "--";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        String endTimeString = dateFormat.format(mEndTime);

        return endTimeString;
    }

    // ScheduleFrequency

    private ScheduleFrequency mFrequency;

    public ScheduleFrequency getFrequency() {
        return mFrequency;
    }

    public boolean checkDiff() {
        boolean isNameChanged = !mName.equalsIgnoreCase(mModel.getName());

        String modelEnableString = mModel.getEnable();
        boolean modelEnable = !modelEnableString.equalsIgnoreCase("0");
        boolean isEnableChanged = mEnable != modelEnable;

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        String modelStartTime = mModel.getStartTime();

        boolean isStartTimeChanged = false;

        if (modelStartTime != null) {
            Date currentStartTime = getStartTime();
            String startTime = dateFormat.format(currentStartTime);

            isStartTimeChanged = !startTime.equalsIgnoreCase(modelStartTime);
        } else {
            Date currentStartTime = getStartTime();
            isStartTimeChanged = currentStartTime != null;
        }

        String modelEndTime = mModel.getEndTime();

        boolean isEndTimeChanged = false;

        if (modelEndTime != null) {
            Date currentEndTime = getEndTime();
            String endTime = dateFormat.format(currentEndTime);

            isEndTimeChanged = !endTime.equalsIgnoreCase(modelEndTime);
        } else {
            Date currentEndTime = getEndTime();
            isEndTimeChanged = currentEndTime != null;
        }

        boolean isFrequencyChanged = mFrequency.checkDiff();

        return isNameChanged ||
                isEnableChanged ||
                isStartTimeChanged ||
                isEndTimeChanged ||
                isFrequencyChanged;
    }

}
