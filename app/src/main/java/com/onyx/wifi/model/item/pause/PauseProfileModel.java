package com.onyx.wifi.model.item.pause;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PauseProfileModel {

    public PauseProfileModel() {
        mProfileId = "";

        mName = "";

        mType = "1";

        mDuration = 0;
    }

    @SerializedName("profile_id")
    private String mProfileId;

    public String getProfileId() { return mProfileId; }

    @SerializedName("name")
    private String mName;

    public String getName() { return mName; }

    @SerializedName("type")
    private String mType;

    public String getType() { return mType; }

    @SerializedName("duration")
    private int mDuration = 0;

    public String getDurationString() {
        if (mDuration == 15) {
            return "15m";
        }

        if (mDuration == 30) {
            return "30m";
        }

        if (mDuration == 60) {
            return "1hr";
        }

        if (mDuration == 120) {
            return "2hr";
        }

        if (mDuration == 240) {
            return "4hr";
        }

        if (mDuration == 480) {
            return "8hr";
        }

        if (mDuration == 720) {
            return "12hr";
        }

        if (mDuration == 525600) {
            return "∞";
        }

        return "";
    }

    public int getDuration() { return mDuration; }

    @SerializedName("exclude")
    private List<String> mExcludeCidList;

    public List<String> getExcludeCidList() { return mExcludeCidList; }

    @SerializedName("members")
    private List<String> mExcludeMemberIdList;

    public List<String> getExcludeMemberIdList() { return mExcludeMemberIdList; }

}