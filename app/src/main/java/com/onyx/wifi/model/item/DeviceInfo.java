package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.StringUtils;

public class DeviceInfo {

    // 有多支 API 都會回傳 device info, 其中大部份欄位都是共同的, 但有些欄位只有某些 API 才會回傳
    // 這個 DeviceInfo 資料結構整合所有 API 的欄位

    @SerializedName("boardid")
    private String boardId = "";

    @SerializedName("device_type")
    private String deviceType = "3";

    @SerializedName("iot_type")
    private String iotType = "";

    @SerializedName("device_label")
    private String deviceLabel = "";

    @SerializedName("city")
    private String city = "";

    @SerializedName("current_fw")
    private String currentFw = "";

    @SerializedName("update_fw")
    private String updateFw = "";

    @SerializedName("did")
    private String did = "";

    @SerializedName("fw_status")
    private String fwStatus = "1";
    // -2: file corrupted
    // -1: update fail
    // 0: updating
    // 1: normal
    // 2: update successful
    // 3: already up-to-date

    @SerializedName("ip")
    private String ip = "";

    @SerializedName("online")
    private String online = "0";

    @SerializedName("node_count")
    private int nodeCount = 0;

    @SerializedName("client_count")
    private int clientCount = 0;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public AppConstants.DeviceType getDeviceType() {
        int index = -1;
        try {
            index = Integer.parseInt(deviceType);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (index > 0 && index < AppConstants.DeviceType.values().length) {
            return AppConstants.DeviceType.values()[index];
        }
        return AppConstants.DeviceType.TOWER;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getIotType() {
        return iotType;
    }

    public void setIotType(String iotType) {
        this.iotType = iotType;
    }

    public String getDeviceLabel() {
        return StringUtils.decodeHexString(deviceLabel);
    }

    public void setDeviceLabel(String deviceLabel) {
        this.deviceLabel = StringUtils.encodeToHexString(deviceLabel);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCurrentFw() {
        return currentFw;
    }

    public void setCurrentFw(String currentFw) {
        this.currentFw = currentFw;
    }

    public String getUpdateFw() {
        return updateFw;
    }

    public void setUpdateFw(String updateFw) {
        this.updateFw = updateFw;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getFwStatus() {
        return fwStatus;
    }

    public void setFwStatus(String fwStatus) {
        this.fwStatus = fwStatus;
    }

    public String getIp() {
        return ip;
    }

    public void setIp() {
        this.ip = ip;
    }

    public boolean getOnline() {
        if (online == null) {
            return false;
        }
        return online.equals("1");
    }

    public void setOnline(boolean online) {
        this.online = (online) ? "1" : "0";
    }

    public int getClientCount() {
        return clientCount;
    }

    public void setClientCount(int clientCount) {
        this.clientCount = clientCount;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }
}
