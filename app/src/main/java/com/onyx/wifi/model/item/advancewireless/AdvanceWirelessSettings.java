package com.onyx.wifi.model.item.advancewireless;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.item.advancewireless.dhcp.DhcpTable;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfigModel;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingList;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringList;
import com.onyx.wifi.model.item.advancewireless.reboot.ScheduleRebootModel;
import com.onyx.wifi.model.item.advancewireless.ssid.WirelessModel;

public class AdvanceWirelessSettings {

    @SerializedName("dmz")
    private DemilitarizedZoneConfigModel mDemilitarizedZoneConfigModel;

    @SerializedName("schedule_reboot")
    private ScheduleRebootModel scheduleRebootModel;

    @SerializedName("dhcp_table")
    private DhcpTable dhcpTable;

    @SerializedName("port_triggering")
    private PortTriggeringList portTriggeringList;

    @SerializedName("port_forwarding")
    private PortForwardingList portForwardingList;

    @SerializedName("wireless")
    private WirelessModel wireless;

    public WirelessModel getWireless() {
        return wireless;
    }

    public DhcpTable getDhcpTable() {
        return dhcpTable;
    }

    public DemilitarizedZoneConfigModel getDemilitarizedZoneConfigModel() {
        return mDemilitarizedZoneConfigModel;
    }

    public PortForwardingList getPortForwardingList() {
        return portForwardingList;
    }

    public PortTriggeringList getPortTriggeringList() {
        return portTriggeringList;
    }

    public ScheduleRebootModel getScheduleRebootModel() {
        return scheduleRebootModel;
    }

}
