package com.onyx.wifi.model.item.advancewireless.port;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PortForwardingList {

    @SerializedName("enable")
    private String enable;

    @SerializedName("port_forwarding_rule")
    private List<PortForwardingRuleModel> portForwardingRule = null;

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public List<PortForwardingRuleModel> getPortForwardingRule() {
        return portForwardingRule;
    }

}
