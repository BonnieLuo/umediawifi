package com.onyx.wifi.model.item.member;

public class PauseInfo {

    private PauseInfoModel mModel;

    public PauseInfo(PauseInfoModel model) {
        mModel = model;

        mType = mModel.getType();

        mEnable = mModel.getEnable();

        String modelMinuteLeft = model.getMinuteLeft();
        if (modelMinuteLeft != null) {
            mMinuteLeft = Integer.parseInt(modelMinuteLeft);
        } else {
            mMinuteLeft = 0;
        }

        String modelDuration = model.getDuration();

        if (modelDuration != null) {
            mDuration = Integer.parseInt(modelDuration);
        } else {
            mDuration = 0;
        }

        mRuleId = mModel.getRuleId();
    }

    private String mEnable;

    public boolean isEnabled() {
        return mEnable.equalsIgnoreCase("1");
    }

    public void toggleEnable() {
        if (mEnable.equalsIgnoreCase("1")) {
            mEnable = "0";
        } else {
            mEnable = "1";
        }
    }

    private int mMinuteLeft;

    public int getMinuteLeft() {
        return mMinuteLeft;
    }

    private int mDuration;

    public int getDuration() {
        return mDuration;
    }

    public String getDurationString() {
        if (mDuration == 15) {
            return "15m";
        }

        if (mDuration == 30) {
            return "30m";
        }

        if (mDuration == 60) {
            return "1hr";
        }

        if (mDuration == 120) {
            return "2hr";
        }

        if (mDuration == 240) {
            return "4hr";
        }

        if (mDuration == 480) {
            return "8hr";
        }

        if (mDuration == 720) {
            return "12hr";
        }

        if (mDuration == 525600) {
            return "∞";
        }

        return "";
    }

    private String mRuleId;

    public String getRuleId() {
        return mRuleId;
    }

    private String mType;

    public String getType() {
        return mType;
    }

}
