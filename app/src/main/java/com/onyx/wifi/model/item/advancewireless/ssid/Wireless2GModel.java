package com.onyx.wifi.model.item.advancewireless.ssid;

import com.google.gson.annotations.SerializedName;

public class Wireless2GModel {

    @SerializedName("ch_set")//Bonnie("ch")
    private String ch;

    @SerializedName("bandwidth")
    private String bandwidth;

    @SerializedName("ssid_broadcast")
    private String ssidBroadcast;

    public String getChannel() {
        return ch;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public String isSsidBroadcast() {
        return ssidBroadcast;
    }

}
