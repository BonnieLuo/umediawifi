package com.onyx.wifi.model;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;

import java.util.Map;

public class FcmService extends FirebaseMessagingService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     * <p>
     * The registration token may change when:
     * <p>
     * - The app deletes Instance ID
     * - The app is restored on a new device
     * - The user uninstalls/reinstall the app
     * - The user clears app data.
     */
    @Override
    public void onNewToken(String token) {
        LogUtils.trace("new FCM token: " + token);
        // 收到的 token 不用先儲存下來, 因為可以用 FirebaseInstanceId 取得 (參考 AccountManager.registerFcmToken())
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        LogUtils.trace("From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            LogUtils.trace("Message data payload: " + remoteMessage.getData());
            /***************************
             recommended code template by Firebase Cloud Messaging Guide
             https://firebase.google.com/docs/cloud-messaging/android/receive
             ***************************/
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use WorkManager.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }

            // Handle message within 10 seconds
            handleMessageData(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            final String message = remoteMessage.getNotification().getBody();
            LogUtils.trace("Message Notification Body: " + message);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void handleMessageData(Map<String, String> messageData) {
        DataManager dataManager = DataManager.getInstance();

        String uid = dataManager.getUserId();
        String messageUid = messageData.get("uid");
        if (uid != null && uid.equals(messageUid)) {
            String command = messageData.get("command");
            String did = messageData.get("did");
            if (command == null || did == null) {
                return;
            }

            // 只要收到notification,在app上show toast並且發系統通知，以紀錄notification information
            Intent gIntent = new Intent(AppConstants.EVENT_DEVICE_ALL_NOTIFY);
            gIntent.putExtra("NOTIFICATION_CMD", command);
            gIntent.putExtra("NOTIFICATION_CONTENT", "data: " + messageData);
            sendBroadcast(gIntent);

            switch (command.toLowerCase()) {
                case "provision_completed":
                    // device provision 完成, 原本要打 cloud API 拿取資料
                    // 但實測發現此時打 cloud API 的結果, 常常拿到的 dashboard 資料都是空的, 推測有可能是 cloud 的資料尚未處理完成
                    // 因為拿不到正確的資料, 故這裡不打 cloud API
                    // (實際處理 provision 結果的地方是 device 的 ProvisionActivity)
                    break;

                case "client_count":
                    dataManager.updateClientCount(StringUtils.stringToInt(messageData.get("total")));
                    sendBroadcast(new Intent(AppConstants.EVENT_CLIENT_COUNT));
                    break;

                case "online":
                    dataManager.updateDeviceOnlineStatus(did, true);
                    sendBroadcast(new Intent(AppConstants.EVENT_DEVICE_INTERNET_STATUS));
                    break;

                case "offline":
                    dataManager.updateDeviceOnlineStatus(did, false);
                    sendBroadcast(new Intent(AppConstants.EVENT_DEVICE_INTERNET_STATUS));
                    break;

                case "speedtest_completed":
                    updateSpeedTest(did, messageData);
                    sendBroadcast(new Intent(AppConstants.EVENT_DEVICE_SPEED_TEST));
                    break;

                case "fw_update_inprogress":
                    // Message data payload:
                    // {
                    //  current_fw_version=L101.00.00.B57,
                    //  fw_status=0,
                    //  uid=v7EjTJ3alaZThJewJ2mRVRJ8gsX2,
                    //  did=bc329e00-1dd8-11b2-8601-0011e004c5a6,
                    //  timestamp=1568746441424,
                    //  command=fw_update_inprogress,
                    //  update_fw_version=L101.00.00.B57
                    // }
                    String fwStatus = messageData.get("fw_status");
                    Intent intent = new Intent(AppConstants.EVENT_DEVICE_FIRMWARE_UPDATE);
                    intent.putExtra("DID", did);
                    intent.putExtra("FW_STATUS", fwStatus);
                    LogUtils.trace("NotifyMsg", "[fw_update_inprogress] did: " + did + " status:" + fwStatus);
                    sendBroadcast(intent);
                    break;

                case "fw_update_completed":
//                {
//                    update_fw=L101.00.00.B87,
//                        current_fw=L101.00.00.B87,
//                        fw_status=1,
//                        did=5e1d2217dd50a3617b5cebe2,
//                        uid=p6dPBQ2289MMaKljqojrwAvm0WY2,
//                        udid=ONYX-SWHA42C000-9191AUTUS00062-1A-US-ECA5DE01D4FE,
//                        timestamp=1579003416748,
//                        command=FWUPDATE_COMPLETED
//                }
                    String completefwStatus = messageData.get("fw_status");
                    String currentFW = messageData.get("current_fw");
                    String updateFW = messageData.get("update_fw");
                    Intent completeIntent = new Intent(AppConstants.EVENT_DEVICE_FIRMWARE_UPDATE_COMPLETED);
                    completeIntent.putExtra("CURRENT_FW", currentFW);
                    completeIntent.putExtra("UPDATE_FW", updateFW);
                    completeIntent.putExtra("DID", did);
                    completeIntent.putExtra("FW_STATUS", completefwStatus);
                    sendBroadcast(completeIntent);
                    break;
            }
        } else {
            LogUtils.trace("uid is different, uid: " + uid + ", message uid: " + messageUid);
        }
    }

    private void updateSpeedTest(String did, Map<String, String> messageData) {
        SpeedTest speedTest = new SpeedTest();
        speedTest.setStatus(StringUtils.stringToInt(messageData.get("status")));
        speedTest.setUpload(StringUtils.stringToInt(messageData.get("upload")));
        speedTest.setDownload(StringUtils.stringToInt(messageData.get("download")));
        String timestamp = messageData.get("timestamp");
        if (timestamp != null) {
            // 從 cloud get speed test API 拿到的資料 (ex: "2020-02-12T13:41:33.698Z") 代表的是 GMT 時間
            // App 也要使用與 cloud 同樣的格式來儲存資料, 故將收到的 timestamp 轉換格式再儲存
            speedTest.setTimestamp(DateUtils.getDateStringWithGMTTimestamp(Long.valueOf(timestamp)));
        }
        DataManager.getInstance().updateSpeedTest(did, speedTest);
    }
}
