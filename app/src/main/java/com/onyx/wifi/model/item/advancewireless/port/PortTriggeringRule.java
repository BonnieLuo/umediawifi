package com.onyx.wifi.model.item.advancewireless.port;

import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.StringUtils;

public class PortTriggeringRule {

    private final String[] mProtocolArray = {"TCP", "UDP", "TCP & UDP"};

    private PortTriggeringRuleModel mModel;

    public PortTriggeringRule(PortTriggeringRuleModel model) {
        mModel = model;

        String ruleId = mModel.getRuleId();

        mId = ruleId;

        String ruleName = mModel.getRuleName();

        mName = StringUtils.decodeHexString(ruleName);

        mEnable = mModel.getEnable();

        String matchPort = mModel.getMatchPort();
        String[] separatedMatchPort = matchPort.split("-");

        if (separatedMatchPort.length == 2) {
            mMatchedPortStart = separatedMatchPort[0];
            mMatchedPortEnd = separatedMatchPort[1];
        }

        String matchProtocol = mModel.getMatchProtocol();
        mMatchedProtocolIndex = mModel.getProtocolIndex(matchProtocol);

        String triggerPort = mModel.getTriggerPort();
        String[] separatedTriggerPort= triggerPort.split("-");

        if (separatedTriggerPort.length == 2) {
            mTriggeredPortStart = separatedTriggerPort[0];
            mTriggeredPortEnd = separatedTriggerPort[1];
        }

        String triggerProtocol = mModel.getTriggerProtocol();
        mTriggeredProtocolIndex = mModel.getProtocolIndex(triggerProtocol);
    }

    private String mEnable;

    public boolean isEnabled() {
        return mEnable.equalsIgnoreCase("1");
    }

    public void toggleEnable() {
        if (mEnable.equalsIgnoreCase("1")) {
            mEnable = "0";
        } else {
            mEnable = "1";
        }
    }

    private String mId;

    public String getId() {
        return mId;
    }

    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String newName) {
        mName = newName;
    }

    public String getHexName() {
        return StringUtils.encodeToHexString(mName);
    }

    private String mMatchedPortStart;

    public String getMatchedPortStart() {
        return mMatchedPortStart;
    }

    public void setMatchedPortStart(String startPort) {
        mMatchedPortStart = startPort;
    }

    private String mMatchedPortEnd;

    public String getMatchedPortEnd() {
        return mMatchedPortEnd;
    }

    public void setMatchedPortEnd(String endPort) {
        mMatchedPortEnd = endPort;
    }

    public String getMatchPort() {
        return String.format("%s-%s", mMatchedPortStart, mMatchedPortEnd);
    }

    private Integer mMatchedProtocolIndex;

    public String getMatchedProtocol() {
        if (mMatchedProtocolIndex == 0) {
            return "tcp";
        }

        if (mMatchedProtocolIndex == 1) {
            return "udp";
        }

        if (mMatchedProtocolIndex == 2) {
            return "tcp udp";
        }

        return "";
    }

    public String getDisplayMatchedProtocol() {
        if (mMatchedProtocolIndex == -1) {
            return "";
        }

        String protocol = mProtocolArray[mMatchedProtocolIndex];

        return protocol;
    }

    public void setMatchedProtocol(Integer index) {
        mMatchedProtocolIndex = index;
    }

    private String mTriggeredPortStart;

    public String getTriggeredPortStart() {
        return mTriggeredPortStart;
    }

    public void setTriggeredPortStart(String startPort) {
        this.mTriggeredPortStart = startPort;
    }

    private String mTriggeredPortEnd;

    public String getTriggeredPortEnd() {
        return mTriggeredPortEnd;
    }

    public void setTriggeredPortEnd(String endPort) {
        mTriggeredPortEnd = endPort;
    }

    public String getTriggerPort() {
        return String.format("%s-%s", mTriggeredPortStart, mTriggeredPortEnd);
    }

    private Integer mTriggeredProtocolIndex;

    public String getTriggeredProtocol() {
        if (mTriggeredProtocolIndex == 0) {
            return "tcp";
        }

        if (mTriggeredProtocolIndex == 1) {
            return "udp";
        }

        if (mTriggeredProtocolIndex == 2) {
            return "tcp udp";
        }

        return "";
    }

    public String getDisplayTriggeredProtocol() {
        if (mTriggeredProtocolIndex == -1) {
            return "";
        }

        String protocol = mProtocolArray[mTriggeredProtocolIndex];

        return protocol;
    }

    public void setTriggeredProtocol(Integer index) {
        mTriggeredProtocolIndex = index;
    }

    public String getRuleDescription() {

        return String.format("%s-%s --> %s-%s", mMatchedPortStart, mMatchedPortEnd, mTriggeredPortStart, mTriggeredPortEnd);
    }

    public boolean isChanged() {
        String modelName = mModel.getRuleDisplayName();
        boolean isNameChanged = !mName.equalsIgnoreCase(modelName);

        String modelEnable = mModel.getEnable();
        boolean isEnableChanged = !mEnable.equalsIgnoreCase(modelEnable);

        String modelMatchPort = mModel.getMatchPort();

        String matchPort = String.format("%s-%s", mMatchedPortStart, mMatchedPortEnd);

        boolean isMatchedPortChanged = !matchPort.equalsIgnoreCase(modelMatchPort);

        String matchProtocol = mModel.getMatchProtocol();
        int modelMatchedProtocolIndex = mModel.getProtocolIndex(matchProtocol);
        boolean isMatchedProtocolChanged = mMatchedProtocolIndex != modelMatchedProtocolIndex;

        String modelTriggerPort = mModel.getTriggerPort();

        String triggerPort = String.format("%s-%s", mTriggeredPortStart, mTriggeredPortEnd);

        boolean isTriggerPortChanged = !triggerPort.equalsIgnoreCase(modelTriggerPort);

        String triggerProtocol = mModel.getTriggerProtocol();
        int modelTriggeredProtocolIndex = mModel.getProtocolIndex(triggerProtocol);
        boolean isTriggeredProtocolChanged = mTriggeredProtocolIndex != modelTriggeredProtocolIndex;

        return isNameChanged ||
                isEnableChanged ||
                isMatchedPortChanged ||
                isMatchedProtocolChanged ||
                isTriggerPortChanged ||
                isTriggeredProtocolChanged;
    }

    public boolean isNameValid() {
        return InputUtils.isNameValid(mName);
    }


    public boolean isMatchedPortRangeValid() {
        Integer startPort;
        Integer endPort;

        if (!InputUtils.isNotEmpty(mMatchedPortStart) ||
                !InputUtils.isNotEmpty(mMatchedPortEnd))
            return false;

        startPort = Integer.parseInt(mMatchedPortStart);
        endPort = Integer.parseInt(mMatchedPortEnd);

        return isPortValid(startPort) && isPortValid(endPort);
    }

    public boolean isMatchedPortRuleValid() {
        Integer startPort = Integer.parseInt(mMatchedPortStart);
        Integer endPort = Integer.parseInt(mMatchedPortEnd);

        return endPort >= startPort;
    }

    public boolean isMatchedProtocolValid() {
        return mMatchedProtocolIndex != -1;
    }

    public boolean isTriggeredRangeValid() {
        Integer startPort;
        Integer endPort;

        if (!InputUtils.isNotEmpty(mTriggeredPortStart) ||
                !InputUtils.isNotEmpty(mTriggeredPortEnd))
            return false;

        startPort = Integer.parseInt(mTriggeredPortStart);
        endPort = Integer.parseInt(mTriggeredPortEnd);

        return isPortValid(startPort) && isPortValid(endPort);
    }

    public boolean isTriggeredRuleValid() {
        Integer startPort = Integer.parseInt(mTriggeredPortStart);
        Integer endPort = Integer.parseInt(mTriggeredPortEnd);

        return endPort >= startPort;
    }

    public boolean isTriggeredProtocolValid() {
        return mTriggeredProtocolIndex != -1;
    }

    private boolean isPortValid(Integer port) {
        return port >= 0 && port <= 65535;
    }

}

