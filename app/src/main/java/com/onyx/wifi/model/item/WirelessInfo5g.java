package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.StringUtils;

public class WirelessInfo5g {

    @SerializedName("sta_mac")
    private String staMac = "";

    @SerializedName("ch")
    private String ch = "";

    @SerializedName("auth")
    private String auth = "";

    @SerializedName("bh")
    private String bh = "";     // 0: Fronthaul(給使用者連線用), 1: Backhaul(給 repeater 連線用)

    @SerializedName("enc")
    private String enc = "";

    @SerializedName("pwd")
    private String pwd = "";

    @SerializedName("ssid")
    private String ssid = "";

    @SerializedName("mac")
    private String mac = "";

    @SerializedName("ap_bssid")
    private String apBssid = "";

    @SerializedName("ch_set")
    private String chSet = "";

    public String getStaMac() {
        return staMac;
    }

    public void setStaMac(String staMac) {
        this.staMac = staMac;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getEnc() {
        return enc;
    }

    public void setEnc(String enc) {
        this.enc = enc;
    }

    public String getPwd() {
        return StringUtils.decodeHexString(pwd);
    }

    public void setPwd(String pwd) {
        this.pwd = StringUtils.encodeToHexString(pwd);
    }

    public String getSsid() {
        return StringUtils.decodeHexString(ssid);
    }

    public void setSsid(String ssid) {
        this.ssid = StringUtils.encodeToHexString(ssid);
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getApBssid() {
        return apBssid;
    }

    public void setApBssid(String apBssid) {
        this.apBssid = apBssid;
    }

    public String getChSet() {
        return chSet;
    }

    public void setChSet(String chSet) {
        this.chSet = chSet;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
