package com.onyx.wifi.model.item.advancewireless.dmz;

import com.google.gson.annotations.SerializedName;

public class DemilitarizedZoneConfigModel {

    @SerializedName("enable")
    private String mEnable;

    @SerializedName("ip_addr")
    private String mIpAddress;

    public String getEnable() {
        return mEnable;
    }

    public String getIpAddress() {
        return mIpAddress;
    }

}
