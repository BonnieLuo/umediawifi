package com.onyx.wifi.model.item.advancewireless.dmz;

public class DemilitarizedZoneConfig {

    private DemilitarizedZoneConfigModel mModel;

    public DemilitarizedZoneConfig(DemilitarizedZoneConfigModel model) {
        mModel = model;

        mEnable = mModel.getEnable();

        mIpAddress = mModel.getIpAddress();
    }

    private String mEnable;

    public boolean isEnabled() {
        return "1".equalsIgnoreCase(mEnable);//mEnable.equalsIgnoreCase("1");
    }

    public void toggleEnable() {
        if ("1".equalsIgnoreCase(mEnable)) {//(mEnable.equalsIgnoreCase("1")) {
            mEnable = "0";
        } else {
            mEnable = "1";
        }
    }

    private String mIpAddress;

    public String getIpAddress() {
        return mIpAddress;
    }

    public void setIpAddress(String newIpAddress) {
        mIpAddress = newIpAddress;
    }

    public boolean isChanged() {
        String modelEnable = mModel.getEnable();
        boolean isEnableChanged = false; //!mEnable.equalsIgnoreCase(modelEnable);

        String modelIpAddress = mModel.getIpAddress();
        boolean isIpAddressChanged = false;

        if (modelEnable != null)
            isEnableChanged = !mEnable.equalsIgnoreCase(modelEnable);
        if (modelIpAddress != null)
            isIpAddressChanged = !mIpAddress.equalsIgnoreCase(modelIpAddress);

        return isEnableChanged || isIpAddressChanged;
    }

}
