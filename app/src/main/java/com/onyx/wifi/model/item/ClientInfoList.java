package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.StringUtils;

import java.util.ArrayList;

//參考UserInfo，但getUserInfo是直接在AccountApi直接轉成class object，ClientInfoList的class object則是在ViewModel裡轉換的
public class ClientInfoList implements Cloneable{
    @SerializedName("client_list")
    private ArrayList<ClientModel> clientInfoList = new ArrayList<ClientModel>();

    public ArrayList<ClientModel> getClientInfoList() {
        return clientInfoList;
    }

    public void setClientInfoList(ArrayList<ClientModel> clientInfo) {
        this.clientInfoList = clientInfo;
    }

    @Override
    public Object clone() {
        ClientInfoList clientInfoList = null;
        try{
            clientInfoList = (ClientInfoList)super.clone();
        }catch(CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clientInfoList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }

    /*public class ClientModel implements Cloneable{
//                "client_id": null,
//                "cid": "FC:DB:B3:B7:61:9F",
//                "name": "FAE-Galaxy-S6",
//                "ip": "192.168.1.229",
//                "status": 2, (Int)//0 is gray offline, 1 is yellow online, 2 is green online
//                "client_type": "MOBILE",//0 is phone,1 is pad,etc
//                "connection_type": "Wireless",
//                "last_online_timestamp": 1569317140000,
//                "first_online_timestamp": 1569228627000,
//                "disallowed_timestamp": 0,
//                "device_outage_monitoring": false,
//                "from_node_did": "bc329e00-1dd8-11b2-8601-0011e004c5a6",
//                "owner_member_id": null,
//                "internet_pause_end_timestamp": 0

//        {
//            "client_id": null,
//                "mac": "FC:77:74:22:8B:2A",
//                "cid": "5e1d97d82b93b317bc356339",
//                "ip": "192.168.8.118",
//                "status": 0,
//                "online": "0",
//                "client_type": "MOBILE",
//                "connection_type": "Wireless",
//                "network_type": "01",
//                "last_online_timestamp": 1579142282301,
//                "first_online_timestamp": 0,
//                "disallowed_timestamp": 0,
//                "from_node_did": "5e1e90d38e4dfe5655fc631d",
//                "owner_member_id": null,
//                "internet_priority": null,
//                "pause_info": null,
//                "client_name": "4E6574776F726B5F446576696365"
//        },

        @SerializedName("client_id")
        private String clientId = "";

        @SerializedName("cid")
        private String cid = "";

        @SerializedName("client_name")
        private String name = "";

        @SerializedName("ip")
        private String ip = "";

        //status
        @SerializedName("status")
        private int status = 0;

        @SerializedName("online")
        private String online = "0";

        @SerializedName("internet_priority")
        private String internetPriority = "0";

        @SerializedName("client_type")
        private String clientType = "";

        @SerializedName("connection_type")
        private String connectionType = "";

        //last_online_timestamp
        @SerializedName("last_online_timestamp")
        private long lastOnlineTimestamp = 0L;

        //first_online_timestamp
        @SerializedName("first_online_timestamp")
        private long firstOnlineTimestamp = 0L;

        //disallowed_timestamp
        @SerializedName("disallowed_timestamp")
        private long disallowedTimestamp = 0L;

        //device_outage_monitoring
        @SerializedName("device_outage_monitoring")
        private boolean deviceOutageMonitoring = false;

        @SerializedName("from_node_did")
        private String fromNodeDid = "";

        @SerializedName("owner_member_id")
        private String ownerMemberId = "";

        //internet_pause_end_timestamp
        @SerializedName("internet_pause_end_timestamp")
        private long internetPauseEndTimestamp = 0L;

        //clientId
        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        //cid
        public String getCid() {
            return cid;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        // name
        public String getName() {
            return StringUtils.decodeHexString(name);
        }

        public void setName(String name) {
            this.name = StringUtils.encodeToHexString(name);
        }

        //ip
        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        //status
        public int getStatus() {
            return status;
        }

        //online
        public String getOnline() {
            // [error handling]
            if (online == null)
                online = "0";
            return online;
        }

        // Internet priority
        public void setInternetPriority(String internetPriority) {
            this.internetPriority = internetPriority;
        }

        public String getInternetPriority() {
            return internetPriority;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        //clientType
        public String getClientType() {
            return clientType;
        }

        public void setClientType(String clientType) {
            this.clientType = clientType;
        }

        //connectionType
        public String getConnectionType() {
            return connectionType;
        }

        public void setConnectionType(String connectionType) {
            this.connectionType = connectionType;
        }

        //lastOnlineTimestamp
        public long getLastOnlineTimestamp() {
            return lastOnlineTimestamp;
        }

        public void setLastOnlineTimestamp(Long lastOnlineTimestamp) {
            this.lastOnlineTimestamp = lastOnlineTimestamp;
        }

        //firstOnlineTimestamp
        public long getFirstOnlineTimestamp() {
            return firstOnlineTimestamp;
        }

        public void setFirstOnlineTimestamp(Long firstOnlineTimestamp) {
            this.firstOnlineTimestamp = firstOnlineTimestamp;
        }
        //disallowedTimestamp
        public long getDisallowedTimestamp() {
            return disallowedTimestamp;
        }

        public void setDisallowedTimestamp(Long disallowedTimestamp) {
            this.disallowedTimestamp = disallowedTimestamp;
        }
        //deviceOutageMonitoring
        public boolean getDeviceOutageMonitoring() {
            return deviceOutageMonitoring;
        }

        public void setDeviceOutageMonitoring(Boolean deviceOutageMonitoring) {
            this.deviceOutageMonitoring = deviceOutageMonitoring;
        }

        //fromNodeDid
        public String getFromNodeDid() {
            return fromNodeDid;
        }

        public void setFromNodeDid(String fromNodeDid) {
            this.fromNodeDid = fromNodeDid;
        }

        //ownerMemberId
        public String getOwnerMemberId() {
            return ownerMemberId;
        }

        public void setOwnerMemberId(String ownerMemberId) {
            this.ownerMemberId = ownerMemberId;
        }

        //internetPauseEndTimestamp
        public long getInternetPauseEndTimestamp() {
            return internetPauseEndTimestamp;
        }

        public void setInternetPauseEndTimestamp(Long internetPauseEndTimestamp) {
            this.internetPauseEndTimestamp = internetPauseEndTimestamp;
        }

        @Override
        public Object clone() {
            ClientModel clientInfo = null;
            try{
                clientInfo = (ClientModel)super.clone();
            }catch(CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return clientInfo;
        }

        @Override
        public String toString() {
            Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
            return this.getClass().getSimpleName() + gson.toJson(this);
        }
    }*/
}
