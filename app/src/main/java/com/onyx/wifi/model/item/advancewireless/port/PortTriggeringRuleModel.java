package com.onyx.wifi.model.item.advancewireless.port;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.StringUtils;

public class PortTriggeringRuleModel {

    public PortTriggeringRuleModel() {
        enable = "0";

        mName = "";

        mMatchPort = "-";

        mMatchProtocol = "";

        mTriggerPort = "-";

        mTriggerProtocol = "";
    }

    @SerializedName("enable")
    private String enable;

    @SerializedName("ptid")
    private String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("match_port")
    private String mMatchPort;

    @SerializedName("match_protocol")
    private String mMatchProtocol;

    @SerializedName("trigger_port")
    private String mTriggerPort;

    @SerializedName("trigger_protocol")
    private String mTriggerProtocol;

    public String getEnable() {
        return enable;
    }

    public String getRuleId() {
        return mId;
    }

    public String getRuleName() {
        return mName;
    }

    public String getRuleDisplayName() {
        return StringUtils.decodeHexString(mName);
    }

    public String getMatchPort() {
        return mMatchPort;
    }

    public String getMatchProtocol() {
        return mMatchProtocol;
    }

    public String getTriggerPort() {
        return mTriggerPort;
    }

    public String getTriggerProtocol() {
        return mTriggerProtocol;
    }

    public int getProtocolIndex(String protocol) {
        if (protocol.equalsIgnoreCase("tcp")) {
            return 0;
        }

        if (protocol.equalsIgnoreCase("udp")) {
            return 1;
        }

        if (protocol.equalsIgnoreCase("tcp udp")) {
            return 2;
        }

        return -1;

    }

}
