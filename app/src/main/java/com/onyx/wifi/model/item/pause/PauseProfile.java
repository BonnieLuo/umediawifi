package com.onyx.wifi.model.item.pause;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class PauseProfile {

    private PauseProfileModel mModel;

    public PauseProfile(PauseProfileModel model) {
        mModel = model;

        mProfileId = mModel.getProfileId();

        mName = mModel.getName();

        mDuration = mModel.getDuration();

        mExcludeCidList = mModel.getExcludeCidList();

        mExcludeMemberIdList = mModel.getExcludeMemberIdList();
    }

    private String mProfileId;

    public String getProfileId() { return mProfileId; }

    private String mName;

    public String getName() { return mName; }

    public void setName(String name) { mName = name; }

    private int mDuration = 0;

    public int getDuration() { return mDuration; }

    public void setDuration(int index) {
        int duration = 0;

        switch (index) {
            case 0:
                duration = 15;
                break;
            case 1:
                duration = 30;
                break;
            case 2:
                duration = 60;
                break;
            case 3:
                duration = 120;
                break;
            case 4:
                duration = 240;
                break;
            case 5:
                duration = 480;
                break;
            case 6:
                duration = 720;
                break;
            default:
                duration = 0;
                break;
        }

        mDuration = duration;
    }

    private List<String> mExcludeCidList;

    public List<String> getExcludeCidList() { return mExcludeCidList; }

    public void setExcludeCidList(List<String> excludeCidList) {
        mExcludeCidList = excludeCidList;
    }

    private boolean isExcludeCidListChanged() {
        List<String> modelExcludeCidList = mModel.getExcludeCidList();

        if (mExcludeCidList == null && modelExcludeCidList == null) {
            return false;
        }

        if (mExcludeCidList == null && modelExcludeCidList != null) {
            return true;
        }

        if (mExcludeCidList != null && modelExcludeCidList == null) {
            return true;
        }

        if (mExcludeCidList != null || modelExcludeCidList != null) {
            if (mExcludeCidList.size() != modelExcludeCidList.size()) {
                return true;
            }

            Collection<String> similar = new HashSet<String>( mExcludeCidList );
            Collection<String> different = new HashSet<String>();
            different.addAll( mExcludeCidList );
            different.addAll( modelExcludeCidList );

            similar.retainAll( modelExcludeCidList );
            different.removeAll( similar );

            return different.size() > 0;
        }

        return true;
    }

    private List<String> mExcludeMemberIdList;

    public List<String> getExcludeMemberIdList() { return mExcludeMemberIdList; }

    public void setExcludeMemberIdList(List<String> excludeMemberIdList) {
        mExcludeMemberIdList = excludeMemberIdList;
    }

    private boolean isExcludeMemberIdListChanged() {
        List<String> modelExcludeMemberIdList = mModel.getExcludeMemberIdList();

        if (mExcludeMemberIdList == null && modelExcludeMemberIdList == null) {
            return false;
        }

        if (mExcludeMemberIdList == null && modelExcludeMemberIdList != null) {
            return true;
        }

        if (mExcludeMemberIdList != null && modelExcludeMemberIdList == null) {
            return true;
        }

        if (mExcludeMemberIdList != null || modelExcludeMemberIdList != null) {
            if (mExcludeMemberIdList.size() != modelExcludeMemberIdList.size()) {
                return true;
            }

            Collection<String> similar = new HashSet<String>( mExcludeMemberIdList );
            Collection<String> different = new HashSet<String>();
            different.addAll( mExcludeMemberIdList );
            different.addAll( modelExcludeMemberIdList );

            similar.retainAll( modelExcludeMemberIdList );
            different.removeAll( similar );

            return different.size() > 0;
        }

        return true;
    }

    public JsonObject toJson() {

        JsonObject requestJson = new JsonObject();
        requestJson.addProperty("name", mName);
        if (!mProfileId.isEmpty()) {
            requestJson.addProperty("profile_id", mProfileId);
        }
        requestJson.addProperty("type", "1");
        requestJson.addProperty("duration", mDuration);

        if (mExcludeCidList != null && !mExcludeCidList.isEmpty()) {
            JsonArray macArray = new JsonArray();

            for (String cid :mExcludeCidList) {
                macArray.add(cid);
            }

            requestJson.add("exclude", macArray);
        }

        if (mExcludeMemberIdList != null && !mExcludeMemberIdList.isEmpty()) {
            JsonArray memberIdArray = new JsonArray();

            for (String memberId:mExcludeMemberIdList) {
                memberIdArray.add(memberId);
            }

            requestJson.add("members", memberIdArray);
        }

        return requestJson;
    }

    public boolean isChanged() {
        String modelName = mModel.getName();

        boolean isNameChanged = !mName.equalsIgnoreCase(modelName);

        int modelDuration = mModel.getDuration();
        boolean isDurationChanged = mDuration != modelDuration;

        boolean isAssignChanged = isExcludeCidListChanged() || isExcludeMemberIdListChanged();

        return isNameChanged || isDurationChanged || isAssignChanged;
    }
}