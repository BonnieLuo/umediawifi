package com.onyx.wifi.model.item.advancewireless.port;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PortTriggeringList {

    @SerializedName("enable")
    private String enable;

    @SerializedName("port_triggering_rule")
    private List<PortTriggeringRuleModel> portTriggeringRule = null;

    public String getEnable() {
        return enable;
    }

    public List<PortTriggeringRuleModel> getPortTriggeringRule() {
        return portTriggeringRule;
    }

}
