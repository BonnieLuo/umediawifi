package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class ProvisionToken {

    /**
     * {
     * "token" : "1a9c2e8af7a04c31959e15f51a9ddab",
     * "uid" : "T0kLQLnsJQO0x5etyQotInRSo963",
     * "valid_to" : 1555052773371,
     * "nodeset_id" : "5d5fcdf15289dc0001973e07"
     * }
     */

    @SerializedName("token")
    private String token = "";

    @SerializedName("uid")
    private String uid = "";

    @SerializedName("valid_to")
    private long validTo = -1;

    @SerializedName("nodeset_id")
    private String nodeSetId = "";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getValidTo() {
        return validTo;
    }

    public void setValidTo(long validTo) {
        this.validTo = validTo;
    }

    public String getNodeSetId() {
        return nodeSetId;
    }

    public void setNodeSetId(String nodeSetId) {
        this.nodeSetId = nodeSetId;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
