package com.onyx.wifi.model;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;

import com.onyx.wifi.model.interfaces.BleScanListener;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

public class BleScanModel {

    private Context mContext;
    private BluetoothAdapter mBluetoothAdapter;

    private BleScanListener mBleScanListener;

    private boolean mIsScanning = false;

    public BleScanModel(Context context, BleScanListener bleScanListener) {
        mContext = context;
        mBleScanListener = bleScanListener;

        BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    public boolean isScanning() {
        return mIsScanning;
    }

    public void startScan() {
        if (checkAndEnableBluetooth()) {
            // checkAndEnableBluetooth 回傳 true 代表由 App 自動開啟了藍牙, 這時藍牙狀態尚未穩定, 需先等待一下再掃描才能正確執行
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doScan();
                }
            }, 2000);
        } else {
            doScan();
        }
    }

    private void doScan() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scanLeDevice21(true);
        } else {
            scanLeDevice18(true);
        }
    }

    public void stopScan() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scanLeDevice21(false);
        } else {
            scanLeDevice18(false);
        }
    }

    // 檢查藍牙狀態, 若沒有開啟則自動開啟
    private boolean checkAndEnableBluetooth() {
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            LogUtils.trace("enable bluetooth");
            mBluetoothAdapter.enable();
            return true;
        }
        return false;
    }

    private void scanLeDevice18(boolean enable) {
        if (mBluetoothAdapter != null) {
            if (enable) {
                LogUtils.trace("start scan");
                mIsScanning = true;
                mBluetoothAdapter.startLeScan(mLeScanCallback18);
            } else {
                LogUtils.trace("stop scan");
                mIsScanning = false;
                mBluetoothAdapter.stopLeScan(mLeScanCallback18);
            }
        } else {
            LogUtils.trace("bluetooth adapter is null");
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback18 = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {
            String name = bluetoothDevice.getName();
            String macAddress = bluetoothDevice.getAddress();
            LogUtils.trace("find BLE device, name = " + name + ", mac = " + macAddress);

            SetupDevice setupDevice = new SetupDevice();
            setupDevice.setName(name);
            setupDevice.setMacAddress(macAddress);
            setupDevice.setRssi(rssi);
            setupDevice.setBleDevice(bluetoothDevice);
            mBleScanListener.getDevice(setupDevice);
        }
    };

    private ScanCallback mLeScanCallback21 = null;

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private void scanLeDevice21(boolean enable) {
        if (mBluetoothAdapter != null) {
            BluetoothLeScanner bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
            if (mLeScanCallback21 == null) {
                mLeScanCallback21 = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        super.onScanResult(callbackType, result);

                        BluetoothDevice bluetoothDevice = result.getDevice();
                        String name = bluetoothDevice.getName();
                        String macAddress = bluetoothDevice.getAddress();
                        LogUtils.trace("find BLE device, name = " + name + ", mac = " + macAddress);

                        SetupDevice setupDevice = new SetupDevice();
                        setupDevice.setName(name);
                        setupDevice.setMacAddress(macAddress);
                        setupDevice.setRssi(result.getRssi());
                        setupDevice.setBleDevice(bluetoothDevice);
                        mBleScanListener.getDevice(setupDevice);
                    }
                };
            }

            if (bluetoothLeScanner != null) {
                if (enable) {
                    LogUtils.trace("start scan");
                    mIsScanning = true;
                    bluetoothLeScanner.startScan(mLeScanCallback21);
                } else {
                    LogUtils.trace("stop scan");
                    mIsScanning = false;
                    bluetoothLeScanner.stopScan(mLeScanCallback21);
                }
            } else {
                LogUtils.trace("bluetooth le scanner is null");
            }
        } else {
            LogUtils.trace("bluetooth adapter is null");
        }
    }
}
