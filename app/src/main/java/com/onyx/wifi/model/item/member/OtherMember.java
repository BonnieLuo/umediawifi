package com.onyx.wifi.model.item.member;

import android.content.Context;
import android.graphics.Bitmap;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.ClientModel;

import java.util.ArrayList;
import java.util.List;

public class OtherMember extends Member {

    public OtherMember(Context context, MemberType memberType, List<ClientModel> clientModelList) {
        mMemberType = memberType;

        switch (mMemberType) {
            case HOME:
                mName = context.getResources().getString(R.string.member_home_network);
                break;
            case GUEST:
                mName = context.getResources().getString(R.string.member_guest_network);
                break;
            case BNB:
                mName = context.getResources().getString(R.string.member_bnb_guest_network);
                break;
        }

        mClientList = new ArrayList<>();

        for (ClientModel clientModel: clientModelList) {
            Client client = new Client(clientModel);
            mClientList.add(client);
        }
    }

    @Override
    public MemberModel getMemberModel() {
        return null;
    }

    @Override
    public Bitmap getPhoto(Context context) {
        return null;
    }
}
