package com.onyx.wifi.model.item.advancewireless.reboot;

import com.onyx.wifi.utility.InputUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleRebootConfig {

    ScheduleRebootModel mModel;

    public ScheduleRebootConfig(ScheduleRebootModel model) {
        mModel = model;

        mEnable = mModel.getEnable();

        String modelDayOfWeek = mModel.getWeek();
        if (InputUtils.isNotEmpty(modelDayOfWeek))
            mDayOfWeek = Integer.parseInt(modelDayOfWeek);
        else
            mDayOfWeek = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        String time = mModel.getTime();

        if (time != null) {
            try {
                mTime = dateFormat.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private String mEnable;

    public boolean isEnabled() {
        return mEnable.equalsIgnoreCase("1");
    }

    public void toggleEnable() {
        if (mEnable.equalsIgnoreCase("1")) {
            mEnable = "0";
        } else {
            mEnable = "1";
        }
    }

    // Time

    private Date mTime;

    public Date getTime() {
        return mTime;
    }

    public void setTime(Date time) {
        mTime = time;
    }

    public String getTimeString(String format) {
        if (mTime == null) {
            return "--";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        String startTimeString = dateFormat.format(mTime);

        return startTimeString;
    }

    private Integer mDayOfWeek;

    public Integer getDayOfWeek() {
        return mDayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        mDayOfWeek = dayOfWeek;
    }

    public boolean isChanged() {
        String modelEnable = mModel.getEnable();
        boolean isEnableChanged = !mEnable.equalsIgnoreCase(modelEnable);

        String week = mModel.getWeek();
        Integer weekOfDay = 0;
        if (InputUtils.isNotEmpty(week))
            weekOfDay = Integer.parseInt(week);
        boolean isWeekOfDayChanged = mDayOfWeek != weekOfDay;

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        String modelTimeString = mModel.getTime();
        String timeString = "";

        if (InputUtils.isNotEmpty(modelTimeString))
            timeString = dateFormat.format(mTime);

        boolean isTimeChanged = !timeString.equalsIgnoreCase(modelTimeString);

        return isEnableChanged ||
                isWeekOfDayChanged ||
                isTimeChanged;
    }

}
