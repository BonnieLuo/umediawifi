package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.StringUtils;

public class GuestNetworkSetting {

    @SerializedName("enable")
    private boolean enable = false;

    @SerializedName("ssid")
    private String ssid = "";

    @SerializedName("pwd")
    private String pwd = "";

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getSsid() {
        return StringUtils.decodeHexString(ssid);
    }

    public void setSsid(String ssid) {
        this.ssid = StringUtils.encodeToHexString(ssid);
    }

    public String getPwd() {
        return StringUtils.decodeHexString(pwd);
    }

    public void setPwd(String pwd) {
        this.pwd = StringUtils.encodeToHexString(pwd);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
