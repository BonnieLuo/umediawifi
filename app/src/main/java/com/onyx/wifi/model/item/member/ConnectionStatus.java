package com.onyx.wifi.model.item.member;

public enum ConnectionStatus {
    OFFLINE,
    WEAK,
    ONLINE;

    public static ConnectionStatus fromString(String online) {
        if (online != null && online.equalsIgnoreCase("1")) {
            return ONLINE;
        }

        return OFFLINE;
    }

    private static ConnectionStatus[] allValues = values();

    public static ConnectionStatus fromOrdinal(int n) {return allValues[n];}
}
