package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInfo {

    /**
     * {
     * "uid":"jMLqL54SXkVYpvHCS5tFuI8EXUI2",
     * "email":"your.email@somewhere.com",
     * "first_name":"hello",
     * "last_name":"world",
     * "register_date":1553670787000,
     * "email_verified":true,
     * "nodesets":[
     * {
     * "name":"hello's Network",
     * "masterDeviceInfoId":"5e1300b19ce39864ea256917",
     * "id":"5e12f20648102b0001ab2276",
     * }
     * ],
     * }
     */

    @SerializedName("uid")
    private String uid = "";

    @SerializedName("email")
    private String email = "";

    @SerializedName("first_name")
    private String firstName = "";

    @SerializedName("last_name")
    private String lastName = "";

    @SerializedName("register_date")
    private long registerDate = -1;

    @SerializedName("email_verified")
    private boolean emailVerified = false;

    @SerializedName("nodesets")
    private ArrayList<NodeSetInfo> nodeSets = new ArrayList<>();

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(long registerDate) {
        this.registerDate = registerDate;
    }

    public boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public ArrayList<NodeSetInfo> getNodeSets() {
        return nodeSets;
    }

    public void setNodeSets(ArrayList<NodeSetInfo> nodeSets) {
        this.nodeSets = nodeSets;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }

    public static class NodeSetInfo {
        // 一個 node set 代表一個 network (一台 router + 兩台 repeater), 而目前產品的整體系統架構 (device + cloud + App) 設計成一個使用者只會有一個 network
        // 所以理論上 user info 的 node set array list 只會有一個 element
        // 使用者操作 App 註冊完帳號後, cloud 會自動為使用者產生一個預設的 network, 有 id 和 name, 但沒有 controller did
        // 要等使用者操作 App 的 initial setup 流程, 將 router provision 完成後, 這個預設的 network 才會有 controller did

        /**
         * {
         * "name":"hello's Network",
         * "masterDeviceInfoId":"5e1300b19ce39864ea256917",
         * "id":"5e12f20648102b0001ab2276",
         * }
         */

        @SerializedName("id")
        private String id = "";

        @SerializedName("name")
        private String name = "";

        @SerializedName("masterDeviceInfoId")
        private String controllerDid = "";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getControllerDid() {
            return controllerDid;
        }

        public void setControllerDid(String controllerDid) {
            this.controllerDid = controllerDid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
            return this.getClass().getSimpleName() + gson.toJson(this);
        }
    }
}
