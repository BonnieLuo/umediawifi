package com.onyx.wifi.model;

import android.content.Context;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.onyx.wifi.MainApp;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.CloudManager;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.encrypt.EncryptUtils;
import com.onyx.wifi.viewmodel.item.login.SignInUser;

import java.util.List;

public class AccountManager {

    private static AccountManager mInstance;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    public static AccountManager getInstance() {
        if (mInstance == null) {
            mInstance = new AccountManager();
        }
        return mInstance;
    }

    public void reinitialize() {
        initializeFirebaseAuth();
    }

    private AccountManager() {
        // - App 關聯 Firebase project 是靠 google-services.json 這個設定檔 (在 Firebase console 下載)
        // - 現在有兩個 Firebase project, 一個是 TEST, 一個是 RDQA (之前還有一個 PRODUCTION, 不過現在關閉了)
        // - 利用 Android Studio 的 Build Variant 可以做到在 build App 時自動切換使用的 google-services.json 設定檔
        //      (1) 在 "app/src/rdqaDebug" 放置 TEST 的 google-services.json
        //      (2) 在 "app/src/rdqaRelease" 放置 RDQA 的 google-services.json

        // 不過上述方式是在 build 時就決定要使用的設定檔, 算是靜態切換, 沒有辦法在 App 打開後才動態切換設定檔
        // 若要讓 App 可以動態切換 Firebase project, 要用以下 FirebaseApp.initializeApp() 的作法
        // 請參考: https://firebase.google.com/docs/projects/multiprojects#use_multiple_projects_in_your_application

        // initialize Firebase for TEST project
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setProjectId("test-onyxwifi")
                .setApplicationId("1:700989438876:android:ed0a084c38198341f3a659")
                .setApiKey("AIzaSyC38_kPrnib1iUtgrObkYvicGgyf1SDCBU")
                .build();
        FirebaseApp.initializeApp(MainApp.getInstance(), options, AppConstants.FIREBASE_PROJECT_NAME_TEST);

        // initialize Firebase for RDQA project
        options = new FirebaseOptions.Builder()
                .setProjectId("rdqa-onyxwifi")
                .setApplicationId("1:163793842939:android:372416a3f3298fc9b0f65e")
                .setApiKey("AIzaSyCK4zve8_TNiK5l-Ja-uCHknCboClxAo_g")
                .build();
        FirebaseApp.initializeApp(MainApp.getInstance(), options, AppConstants.FIREBASE_PROJECT_NAME_RDQA);

        initializeFirebaseAuth();
    }

    private void initializeFirebaseAuth() {
        mAuth = FirebaseAuth.getInstance(getFirebaseApp());
    }

    private FirebaseApp getFirebaseApp() {
        AppConstants.CloudServerMode cloudServerMode = DataManager.getInstance().getCloudServerMode();
        FirebaseApp firebaseApp;
        if (cloudServerMode == AppConstants.CloudServerMode.TEST) {
            firebaseApp = FirebaseApp.getInstance(AppConstants.FIREBASE_PROJECT_NAME_TEST);
        } else if (cloudServerMode == AppConstants.CloudServerMode.RDQA) {
            firebaseApp = FirebaseApp.getInstance(AppConstants.FIREBASE_PROJECT_NAME_RDQA);
        } else {
            // use default config (google-services.json in "app/src/[Build Variant]")
            firebaseApp = FirebaseApp.getInstance();
        }
        return firebaseApp;
    }

    public FirebaseUser getUser() {
        if (mAuth != null) {
            mUser = mAuth.getCurrentUser();
        } else {
            mUser = null;
        }
        return mUser;
    }

    public void signOut() {
        // sign out 時判斷 user 不為 null 才執行要做的事情, 避免無謂的動作
        if (getUser() != null) {
            unregisterFcmToken();

            // Firebase 登出
            mAuth.signOut();
            LogUtils.trace("Firebase sign out");

            // Google 登出
            Context context = MainApp.getInstance();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(context.getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
            GoogleSignIn.getClient(context, gso).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    LogUtils.trace("Google sign out");
                }
            });

            // Facebook 登出
            LoginManager fbLoginManager = LoginManager.getInstance();
            fbLoginManager.logOut();
            LogUtils.trace("Facebook sign out");

            DataManager.getInstance().clearUserData();
            LogUtils.trace("Clear all user data");
        }
    }

    public void signIn(SignInUser user, OnCompleteListener<AuthResult> onCompleteListener) {
        LogUtils.trace("Firebase sign in: " + user);

        // 使用者註冊帳號時是透過我們的 Cloud API (Cloud 會去向 Firebase 註冊帳號), 而上傳的密碼會加密過, 所以 Firebase 系統內儲存的密碼是加密過的字串
        // 而登入時是呼叫 Firebase SDK, 透過 SDK 直接跟 Firebase 系統做登入, 所以需將密碼加密過, 才能正確登入
        mAuth.signInWithEmailAndPassword(user.getEmail(), EncryptUtils.shaEncrypt(user.getPassword())).addOnCompleteListener(onCompleteListener);
    }

    public void firebaseAuthWithGoogle(GoogleSignInAccount account, OnCompleteListener<AuthResult> onCompleteListener) {
        LogUtils.trace("Firebase Auth with Google: " + account.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(onCompleteListener);
    }

    public void firebaseAuthWithFacebook(AccessToken token, OnCompleteListener<AuthResult> onCompleteListener) {
        LogUtils.trace("Firebase Auth wth Facebook, access token: " + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(onCompleteListener);
    }

    public void refreshUserIdToken(boolean forceRefresh) {
        FirebaseUser user = getUser();
        if (user != null) {
            /* 帳號相關動作(ex: 登入, email 認證)完成後一定要重新 refresh token 比較保險.
               token 的有效期限是一個小時, 一小時內 getIdToken(false) 會得到相同的 token.
               若 token 過期, SDK 會自動更新 token 後再回傳. */
            user.getIdToken(forceRefresh).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        String idToken = task.getResult().getToken();
                        LogUtils.trace(idToken);
                    } else if (task.getException() != null) {
                        LogUtils.trace(task.getException().getMessage());
                    }
                }
            });
        }
    }

    public boolean isSocialAccount() {
//        boolean isSocialAccount = true;
//        FirebaseUser user = getUser();
//        if (user != null) {
//            List<? extends UserInfo> userInfos = user.getProviderData();
//            for (UserInfo userInfo : userInfos) {
//                String providerId = userInfo.getProviderId();
//                LogUtils.trace("providerId: " + providerId);
//                /* 以 email/password 方式註冊的帳號, provider ID 為 "password"
//                   以 Google 登入的帳號, provider ID 為 "google.com"
//                   以 Facebook 登入的帳號, provider ID 為 "facebook.com"
//                   若同樣的 email 有用 email/password 方式註冊過、也有用社群登入過, Firebase 會紀錄為同一個使用者, 但有不同的 provider ID.
//                   只要一個帳號有用 email/password 註冊過, 即使使用者這次是使用社群登入, 在 Account Setting 頁面也要能修改 email/password.
//                   而若是只有用社群登入過的帳號, 在 Account Setting 頁面不能 change email, 也看不到 password. */
//                if (providerId.equals("password")) {
//                    isSocialAccount = false;
//                }
//            }
//        }
//        return isSocialAccount;

        //if (CommonUtils.getSharedPrefData(AppConstants.IS_SOCIAL_SIGN_IN).equals("true"))
        if (DataManager.getInstance().getIsSocialSignIn())
            return true;
        return false;
    }

    public void registerFcmToken() {
        // 用 FirebaseInstanceId 取得對應 FirebaseApp 的 FCM token
        FirebaseInstanceId.getInstance(getFirebaseApp()).getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    LogUtils.trace("Firebase get instance ID task fail");
                    return;
                }

                InstanceIdResult result = task.getResult();
                if (result == null) {
                    LogUtils.trace("Firebase get instance ID task result is null");
                    return;
                }

                String fcmToken = result.getToken();
                LogUtils.trace("FCM token: " + fcmToken);

                // 註冊 Firebase Cloud Messaging token, 在背景執行就好, 前景(UI)不需任何反應, 故 ApiCallback 都不做事
                CloudManager cloudManager = CloudManager.getInstance();
                cloudManager.registerFcmToken(new ApiCallback() {
                    @Override
                    public void onInternetUnavailable() {

                    }

                    @Override
                    public void onSendRequest() {

                    }

                    @Override
                    public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

                    }
                }, fcmToken);
            }
        });
    }

    private void unregisterFcmToken() {
        // 用 FirebaseInstanceId 取得對應 FirebaseApp 的 FCM token
        FirebaseInstanceId.getInstance(getFirebaseApp()).getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    LogUtils.trace("Firebase get instance ID task fail");
                    return;
                }

                InstanceIdResult result = task.getResult();
                if (result == null) {
                    LogUtils.trace("Firebase get instance ID task result is null");
                    return;
                }

                String fcmToken = result.getToken();
                LogUtils.trace("FCM token: " + fcmToken);

                // 取消註冊 Firebase Cloud Messaging token, 在背景執行就好, 前景(UI)不需任何反應, 故 ApiCallback 都不做事
                // 有些情況下打 API 可能會失敗 (ex: email 尚未通過驗證), 目前不用特別處理
                CloudManager cloudManager = CloudManager.getInstance();
                cloudManager.unregisterFcmToken(new ApiCallback() {
                    @Override
                    public void onInternetUnavailable() {

                    }

                    @Override
                    public void onSendRequest() {

                    }

                    @Override
                    public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

                    }
                }, fcmToken);
            }
        });
    }

    public void testFcm() {
        // 測試 Firebase Cloud Messaging
        CloudManager cloudManager = CloudManager.getInstance();
        cloudManager.testFcm(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

            }
        }, "AppName", "UMEDIA");
    }
}
