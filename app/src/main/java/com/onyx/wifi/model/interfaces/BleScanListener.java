package com.onyx.wifi.model.interfaces;

import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

public interface BleScanListener {

    void getDevice(SetupDevice setupDevice);
}
