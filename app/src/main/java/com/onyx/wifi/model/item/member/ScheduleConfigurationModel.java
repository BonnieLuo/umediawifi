package com.onyx.wifi.model.item.member;

import com.google.gson.annotations.SerializedName;

public class ScheduleConfigurationModel {

    /*
    {
        "nodeset_id":"$id",
        "member_id":"$member",
        "name":"Bedtime",
        "start_time":""08:00",   //24 hour
        "end_time":"20:00",     //24 hour
        "enable":"0/1",
        "repeat":{
            "mon":"0/1",
            "tue":"0/1",
            "wed":"0/1",
            "thu":"0/1",
            "fri":"0/1",
            "sat":"0/1",
            "sun":"0/1",
        },
        "exclude":[macAddr ...]                          //optional
    }
    */

    @SerializedName("member_id")
    private String mMemberId;

    public String getMemberId() {
        return mMemberId;
    }

    @SerializedName("ruleId")
    private String mRuleId;

    public String getRuleId() {
        return mRuleId;
    }

    @SerializedName("schedule_id")
    private String mId;

    public String getScheduleId() {
        return mId;
    }

    @SerializedName("name")
    private String mName = "";

    public String getName() {
        return mName;
    }

    @SerializedName("start")
    private String mStartTime;

    public String getStartTime() {
        return mStartTime;
    }

    @SerializedName("end")
    private String mEndTime;

    public String getEndTime() {
        return mEndTime;
    }

    @SerializedName("enable")
    private String mIsEnable = "0";

    public String getEnable() {
        return mIsEnable;
    }

    @SerializedName("repeat")
    private String mRepeat = "0000000";

    public String getRepeat() {
        return mRepeat;
    }

}
