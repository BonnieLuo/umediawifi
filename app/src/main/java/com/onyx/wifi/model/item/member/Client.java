package com.onyx.wifi.model.item.member;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;

import com.onyx.wifi.R;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.utility.DateUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Client {

    private ClientModel mClientModel;

    private String mTemporaryClientType;

    private String mTemporaryName;

    private String mTemporaryDeviceOutageMonitoring;

    private String mTemporaryInternetPriority;

    private String mTemporaryOwnerId;

    public Client(ClientModel clientModel) {
        mClientModel = clientModel;

        mTemporaryClientType = mClientModel.getClientType();

        mTemporaryName = mClientModel.getName();

        mTemporaryDeviceOutageMonitoring = mClientModel.getDeviceOutageMonitoring();

        mTemporaryInternetPriority = mClientModel.getInternetPriority();

        mTemporaryOwnerId = mClientModel.getOwnerMemberId();

        ArrayList<PauseInfoModel> pauseInfoModelList = mClientModel.getPauseInfo();

        if (pauseInfoModelList != null) {
            mPauseInfoList = new ArrayList<>();
            for (PauseInfoModel pauseInfoModel : pauseInfoModelList) {
                PauseInfo pauseInfo = new PauseInfo(pauseInfoModel);
                mPauseInfoList.add(pauseInfo);
            }
        }
    }

    public ClientModel getClientModel() {
        return mClientModel;
    }

    // Id

    public String getId() {
        return mClientModel.getClientID();
    }

    // ClientType

    public String getClientType() {
        return mClientModel.getClientType();
    }

    public String getTemporaryClientType() {
        return mTemporaryClientType;
    }

    public void setTemporaryClientType(String clientType) {
        mTemporaryClientType = clientType;
    }

    public void setClientType(String clientType) {
        mClientModel.setClientType(clientType);
    }

    // Name

    public String getName() {
        return mClientModel.getName();
    }

    public void setName(String name) {
        mClientModel.setName(name);
    }

    public String getTemporaryName() {
        return mTemporaryName;
    }

    public void setTemporaryName(String newName) {
        this.mTemporaryName = newName;
    }

    // IP

    public String getIp() {
        return mClientModel.getIp();
    }

    // Cid

    public String getCid() {
        return mClientModel.getCid();
    }

    // LastOnlineTimestamp

    public long getLastOnlineTimestamp() {
        return mClientModel.getLastOnlineTimestamp();
    }


    // Pause

    private List<PauseInfo> mPauseInfoList;

    public List<PauseInfo> getPauseInfoList() {
        return mPauseInfoList;
    }

    private int mPauseDuration = 0;

    public int getPauseDuration() {
        return mPauseDuration;
    }

    public void setPauseDuration(int duration) {
        mPauseDuration = duration;
    }

    public String getPauseDurationString() {
        if (mPauseDuration == 15) {
            return "15m";
        }

        if (mPauseDuration == 30) {
            return "30m";
        }

        if (mPauseDuration == 60) {
            return "1hr";
        }

        if (mPauseDuration == 120) {
            return "2hr";
        }

        if (mPauseDuration == 240) {
            return "4hr";
        }

        if (mPauseDuration == 480) {
            return "8hr";
        }

        if (mPauseDuration == 720) {
            return "12hr";
        }

        if (mPauseDuration == 525600) {
            return "∞";
        }

        return "";
    }

    // DeviceOutageMonitoring
    public boolean isDeviceOutageMonitoring() {
        if ("1".equals(mClientModel.getDeviceOutageMonitoring()))
            return true;
        else
            return false;
    }

    public boolean isTemporaryDeviceOutageMonitoring() {
        if ("1".equals(mTemporaryDeviceOutageMonitoring))
            return true;
        else
            return false;
    }

    public void setTemporaryDeviceOutageMonitoring(boolean temporaryDeviceOutageMonitoring) {
        if (temporaryDeviceOutageMonitoring)
            mTemporaryDeviceOutageMonitoring = "1";
        else
            mTemporaryDeviceOutageMonitoring = "0";
    }

    public String getTemporaryDeviceOutageMonitoring() {
        return mTemporaryDeviceOutageMonitoring;
    }

    public void setDeviceOutageMonitoring(String outageMonitoring) {
        mClientModel.setmDeviceOutageMonitoring(outageMonitoring);
    }

    // Internet Priority
    public boolean isHighPriority() {
        if ("1".equals(mClientModel.getInternetPriority()))
            return true;
        else
            return false;
    }

    public void setInternelPriority(String internelPriority) {
        mClientModel.setInternetPriority(internelPriority);
    }

    public boolean isTemporaryHighPriority() {
        if ("1".equals(mTemporaryInternetPriority))
            return true;
        else
            return false;
    }

    public String getTemporaryInternetPriority() {
        return mTemporaryInternetPriority;
    }

    public void setTemporaryInternetPriority(boolean isHighPriority) {
        if (isHighPriority)
            mTemporaryInternetPriority = "1";
        else
            mTemporaryInternetPriority = "0";
    }

    // OwnerId

    public String getOwnerId() {
        return mClientModel.getOwnerMemberId();
    }

    public String getTemporaryOwnerId() {
        return mTemporaryOwnerId;
    }

    public void setTemporaryOwnerId(String temporaryOwnerId) {
        mTemporaryOwnerId = temporaryOwnerId;
    }

    public boolean isOwnerChanged() {
        String ownerId = getOwnerId();
        String tempOwnerId = getTemporaryOwnerId();

        if (ownerId == null && tempOwnerId == null) {
            return false;
        }

        if (ownerId == null && tempOwnerId != null) {
            return true;
        }

        if (ownerId != null && tempOwnerId == null) {
            return true;
        }

        return !tempOwnerId.equalsIgnoreCase(ownerId);
    }

    // ConnectionType

    public String getConnectionType() {
        return mClientModel.getConnectionType();
    }

    // ConnectionStatus

    public ConnectionStatus getConnectionStatus() {
        return mClientModel.getOnlineStatus();
    }

    public int getConnectionLight() {
        ConnectionStatus status = getConnectionStatus();

        switch (status) {
            case ONLINE:
                return R.drawable.ic_internet_status_online;
            case WEAK:
                return R.drawable.ic_internet_status_weak;
            default:
                return R.drawable.ic_internet_status_offline_gray;
        }
    }

    // ConnectionMessage

    public String getConnectionMessage(Context context) {
        ConnectionStatus status = getConnectionStatus();

        if (status == ConnectionStatus.OFFLINE) {
            return context.getString(R.string.client_offline);

        }

        return context.getString(R.string.client_online);
    }

    // show last online time description
    public String getLastOnlineTimeInfo() {
        ConnectionStatus status = getConnectionStatus();
        long lastOnlineTimestamp = getLastOnlineTimestamp();

        if (status == ConnectionStatus.OFFLINE) {
            return DateUtils.getTimeDiffToCurrent(lastOnlineTimestamp);

        } else {
            return "online";
        }
    }

    // ClientPicture

    public int getClientPicture() {
        String clientType = getClientType();

        if (clientType == null) {
            return -1;
        }

        String lowerCaseClientType = clientType.toLowerCase();
        DataManager dataManager = DataManager.getInstance();
        Map<String, Integer> devicePictureMap = dataManager.getClientPictureMap();

        if (!devicePictureMap.containsKey(lowerCaseClientType)) {
            String connectionType = getConnectionType();
            if (connectionType.equalsIgnoreCase("Wireless")) {
                return R.drawable.pic_generic_wifi;
            }

            if (connectionType.equalsIgnoreCase("Ethernet")) {
                return R.drawable.pic_generic_lan;
            }
        }

        int picId = devicePictureMap.get(lowerCaseClientType);
        return picId;
    }

    public int getTemporaryClientPicture() {
        String clientType = getTemporaryClientType();

        if (clientType == null) {
            return -1;
        }

        String lowerCaseClientType = clientType.toLowerCase();
        DataManager dataManager = DataManager.getInstance();
        Map<String, Integer> devicePictureMap = dataManager.getClientPictureMap();

        int picId = devicePictureMap.get(lowerCaseClientType);
        return picId;
    }

    // ClientIcon

    public int getClientIcon() {
        ConnectionStatus status = mClientModel.getOnlineStatus();
        return getClientIcon(status);
    }

    public int getClientIcon(ConnectionStatus status) {
        String clientType = mClientModel.getClientType();

        if (clientType == null) {
            return 0;
        }

        DataManager dataManager = DataManager.getInstance();

        Map<String, Integer> deviceIconMap;

        if (status == ConnectionStatus.OFFLINE) {
            deviceIconMap = dataManager.getOfflineClientIconMap();
        } else {
            deviceIconMap = dataManager.getOnlineClientIconMap();
        }

        String lowerCaseClientType = clientType.toLowerCase();
        if (!deviceIconMap.containsKey(lowerCaseClientType)) {
            String connectionType = mClientModel.getConnectionType();
            if (connectionType.equalsIgnoreCase("Wireless")) {
                return getWiFiIcon();
            }

            if (connectionType.equalsIgnoreCase("Ethernet")) {
                return getEthernetIcon();
            }
        }

        return deviceIconMap.get(lowerCaseClientType);
    }

    private int getWiFiIcon() {
        ConnectionStatus status = mClientModel.getOnlineStatus();
        if (status == ConnectionStatus.OFFLINE) {
            return R.drawable.ic_generic_wifi_offline;
        }

        return R.drawable.ic_generic_wifi;
    }

    private int getEthernetIcon() {
        ConnectionStatus status = mClientModel.getOnlineStatus();
        if (status == ConnectionStatus.OFFLINE) {
            return R.drawable.ic_generic_lan_offline;
        }

        return R.drawable.ic_generic_lan;
    }

    public Bitmap getOwnerPhoto(Context context) {
        String ownerId = getOwnerId();

        if (ownerId == null) {
            return null;
        }

        return getOwnerPhoto(context, ownerId);
    }

    public Bitmap getOwnerPhoto(Context context, String ownerId) {
        Bitmap bitmap = null;

        String avatarFileName = ownerId + ".jpg";
        File avatarFile = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), avatarFileName);

        if (avatarFile.exists()) {
            String filePath = avatarFile.getAbsolutePath();

            bitmap = decodeSampledBitmapFromResource(filePath, 200, 200);
        }

        return bitmap;
    }

    private Bitmap decodeSampledBitmapFromResource(String filePath, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return getOrientationBitmap(filePath, options);
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public int resolveBitmapOrientation(File bitmapFile) throws IOException {
        ExifInterface exif = null;
        exif = new ExifInterface(bitmapFile.getAbsolutePath());

        return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
    }

    public Bitmap applyOrientation(Bitmap bitmap, int orientation) {
        int rotate = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            default:
                return bitmap;
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        mtx.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public Bitmap getOrientationBitmap(String filePath, BitmapFactory.Options options) {
        Bitmap srcBmp01 = BitmapFactory.decodeFile(filePath, options);
        try {
            int orientation = resolveBitmapOrientation(new File(filePath));
            srcBmp01 = applyOrientation(srcBmp01, orientation);
        } catch (Exception ex) {
            ;
        }

        return srcBmp01;
    }

    public Bitmap getOrientationBitmap(String filePath) {
        Bitmap srcBmp01 = BitmapFactory.decodeFile(filePath);

        if (srcBmp01 != null) {
            try {
                int orientation = resolveBitmapOrientation(new File(filePath));
                srcBmp01 = applyOrientation(srcBmp01, orientation);
            } catch (Exception ex) {
                ;
            }
        }

        return srcBmp01;
    }

}
