package com.onyx.wifi.model.item.advancewireless.ssid;

public class Wireless5G {

    private Wireless5GModel mModel;

    public Wireless5G(Wireless5GModel model) {
        mModel = model;

        mChannel = mModel.getChannel();

        mBandwidth = mModel.getBandwidth();

        mSsidBroadcast = mModel.isSsidBroadcast();
    }

    private String mChannel;

    public String getChannel() {
        return mChannel;
    }

    public void setChannel(String channel) {
        mChannel = channel;
    }

    private String mBandwidth;

    public String getBandwidth() {
        return mBandwidth;
    }

    public void setBandwidth(String bandwidth) {
        mBandwidth = bandwidth;
    }

    private String mSsidBroadcast;

    public boolean isSsidBroadcast() {
        return "1".equalsIgnoreCase(mSsidBroadcast);//mSsidBroadcast.equalsIgnoreCase("1");
    }

    public void toggleSsidBroadcast() {
        if ("1".equalsIgnoreCase(mSsidBroadcast)) {//if (mSsidBroadcast.equalsIgnoreCase("1")) {
            mSsidBroadcast = "0";
        } else {
            mSsidBroadcast = "1";
        }
    }

    public boolean isChanged() {
        boolean isChannelChanged = !mChannel.equalsIgnoreCase(mModel.getChannel());
        boolean isBandwidthChanged = !mBandwidth.equalsIgnoreCase(mModel.getBandwidth());
        boolean isSsidBroadcastChanged = !mSsidBroadcast.equalsIgnoreCase(mModel.isSsidBroadcast());

        return isChannelChanged || isBandwidthChanged || isSsidBroadcastChanged;
    }

}
