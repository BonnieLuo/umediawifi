package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DashboardInfo {

    @SerializedName("androidMinVer")
    private String androidMinVer;

    @SerializedName("accTier")
    private String accTier;

    @SerializedName("nodesets")
    private ArrayList<NodeSetInfo> nodeSets;

    @SerializedName("code")
    private String code = "";

    public String getAndroidMinVer() {
        return androidMinVer;
    }

    public void setAndroidMinVer(String androidMinVer) {
        this.androidMinVer = androidMinVer;
    }

    public String getAccTier() {
        return accTier;
    }

    public void setAccTier(String accTier) {
        this.accTier = accTier;
    }

    public ArrayList<NodeSetInfo> getNodeSets() {
        return nodeSets;
    }

    public void setNodeSets(ArrayList<NodeSetInfo> nodeSets) {
        this.nodeSets = nodeSets;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
