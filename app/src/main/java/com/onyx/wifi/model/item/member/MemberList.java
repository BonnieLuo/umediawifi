package com.onyx.wifi.model.item.member;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.model.item.ClientModel;

import java.lang.reflect.Type;
import java.util.List;

public class MemberList {

    @SerializedName("member_list")
    private List<MemberModel> memberList;

    @SerializedName("home_network")
    private JsonObject homeNetwork;

    @SerializedName("guest_network")
    private JsonObject guestNetwork;

    @SerializedName("bnb_network")
    private JsonObject bnbNetwork;

    private List<MemberModel> mOtherNetworkMemberList;

    public List<MemberModel> getFamilyMemberList() {
        return memberList;
    }

    public List<ClientModel> getHomeNetwork() {
        if (homeNetwork == null) {
            return null;
        }

        JsonArray assignedClientListJSON = homeNetwork.getAsJsonArray("assigned_client_list");

        Gson gson = new Gson();
        Type listType = new TypeToken<List<ClientModel>>(){}.getType();
        List<ClientModel> assignedClientModelList =  gson.fromJson(assignedClientListJSON.toString(), listType);

        return assignedClientModelList;
    }

    public List<ClientModel> getGuestNetwork() {
        if (guestNetwork == null) {
            return null;
        }

        JsonArray assignedClientListJSON = guestNetwork.getAsJsonArray("assigned_client_list");

        Gson gson = new Gson();
        Type listType = new TypeToken<List<ClientModel>>(){}.getType();
        List<ClientModel> assignedClientModelList =  gson.fromJson(assignedClientListJSON.toString(), listType);

        return assignedClientModelList;
    }

    public List<ClientModel> getBnBGuestNetwork() {
        if (bnbNetwork == null) {
            return null;
        }

        JsonArray assignedClientListJSON = bnbNetwork.getAsJsonArray("assigned_client_list");

        Gson gson = new Gson();
        Type listType = new TypeToken<List<ClientModel>>(){}.getType();
        List<ClientModel> assignedClientModelList =  gson.fromJson(assignedClientListJSON.toString(), listType);

        return assignedClientModelList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
