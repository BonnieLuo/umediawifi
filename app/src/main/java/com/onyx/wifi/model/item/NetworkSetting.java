package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NetworkSetting {

    @SerializedName("wan_type")
    private String wanType = "";    // 1:DHCP, 2:PPPoE, 3:Static IP

    @SerializedName("pppoe_user")
    private String pppoeUser = "";

    @SerializedName("pppoe_password")
    private String pppoePassword = "";

    @SerializedName("wan_static_ip")
    private String wanStaticIp = "";

    @SerializedName("wan_static_mask")
    private String wanStaticMask = "";

    @SerializedName("wan_static_gw")
    private String wanStaticGw = "";

    @SerializedName("wan_static_dns1")
    private String wanStaticDns1 = "";

    @SerializedName("wan_static_dns2")
    private String wanStaticDns2 = "";

    @SerializedName("wan_ip")
    private String wanIp = "";

    @SerializedName("wan_mask")
    private String wanMask = "";

    @SerializedName("wan_gw")
    private String wanGw = "";

    @SerializedName("wan_dns1")
    private String wanDns1 = "";

    @SerializedName("wan_dns2")
    private String wanDns2 = "";

    @SerializedName("lan_ip")
    private String lanIp = "";

    @SerializedName("network_name")
    private String networkName = "";

    @SerializedName("dev_mac")
    private String devMac = "";

    @SerializedName("wan_mac")
    private String wanMac = "";

    @SerializedName("dhcp_range")
    private String dhcpRange = "";

    @SerializedName("netmask")
    private String netMask = "";

    @SerializedName("client_lease_time")
    private String clientLeaseTime = "";

    @SerializedName("led_light")
    private String ledLight = "0";    // 0: false, 1: true (為方便使用, getter/setter 提供 boolean 型式)

    @SerializedName("uptime")
    private String uptime = "0";

    @SerializedName("fw_status_from_all_node")
    private String fwStatusFromAllNode = "0";

    @SerializedName("high_internet_priority")
    private String highInternetPriority = "0";

    @SerializedName("auto_fw_enable")
    private String autoFwEnable = "0";    // 0: false, 1: true (為方便使用, getter/setter 提供 boolean 型式)

    @SerializedName("auto_bandwidth")
    private String autoBandwidth = "0";   // 0: false, 1: true (為方便使用, getter/setter 提供 boolean 型式)

    public String getWanType() {
        return wanType;
    }

    public void setWanType(String wanType) {
        this.wanType = wanType;
    }

    public String getPppoeUser() {
        return pppoeUser;
    }

    public void setPppoeUser(String pppoeUser) {
        this.pppoeUser = pppoeUser;
    }

    public String getPppoePassword() {
        return pppoePassword;
    }

    public void setPppoePassword(String pppoePassword) {
        this.pppoePassword = pppoePassword;
    }

    public String getWanStaticIp() {
        return wanStaticIp;
    }

    public void setWanStaticIp(String wanStaticIp) {
        this.wanStaticIp = wanStaticIp;
    }

    public String getWanStaticMask() {
        return wanStaticMask;
    }

    public void setWanStaticMask(String wanStaticMask) {
        this.wanStaticMask = wanStaticMask;
    }

    public String getWanStaticGw() {
        return wanStaticGw;
    }

    public void setWanStaticGw(String wanStaticGw) {
        this.wanStaticGw = wanStaticGw;
    }

    public String getWanStaticDns1() {
        return wanStaticDns1;
    }

    public void setWanStaticDns1(String wanStaticDns1) {
        this.wanStaticDns1 = wanStaticDns1;
    }

    public String getWanStaticDns2() {
        return wanStaticDns2;
    }

    public void setWanStaticDns2(String wanStaticDns2) {
        this.wanStaticDns2 = wanStaticDns2;
    }

    public String getWanIp() {
        return wanIp;
    }

    public void setWanIp(String wanIp) {
        this.wanIp = wanIp;
    }

    public String getWanMask() {
        return wanMask;
    }

    public void setWanMask(String wanMask) {
        this.wanMask = wanMask;
    }

    public String getWanGw() {
        return wanGw;
    }

    public void setWanGw(String wanGw) {
        this.wanGw = wanGw;
    }

    public String getWanDns1() {
        return wanDns1;
    }

    public void setWanDns1(String wanDns1) {
        this.wanDns1 = wanDns1;
    }

    public String getWanDns2() {
        return wanDns2;
    }

    public void setWanDns2(String wanDns2) {
        this.wanDns2 = wanDns2;
    }

    public String getLanIp() {
        return lanIp;
    }

    public void setLanIp(String lanIp) {
        this.lanIp = lanIp;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getDevMac() {
        return devMac;
    }

    public void setDevMac(String devMac) {
        this.devMac = devMac;
    }

    public String getWanMac() {
        return wanMac;
    }

    public void setWanMac(String wanMac) {
        this.wanMac = wanMac;
    }

    public String getDhcpRange() {
        return dhcpRange;
    }

    public void setDhcpRange(String dhcpRange) {
        this.dhcpRange = dhcpRange;
    }

    public String getNetMask() {
        return netMask;
    }

    public void setNetMask(String netMask) {
        this.netMask = netMask;
    }

    public String getClientLeaseTime() {
        return clientLeaseTime;
    }

    public void setClientLeaseTime(String clientLeaseTime) {
        this.clientLeaseTime = clientLeaseTime;
    }

    public boolean getLedLight() {
        if (ledLight == null) {
            return false;
        }
        return ledLight.equals("1");
    }

    public void setLedLight(boolean ledLight) {
        this.ledLight = (ledLight) ? "1" : "0";
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }

    public String getFwStatusFromAllNode() {
        if (fwStatusFromAllNode == null)
            fwStatusFromAllNode = "1";
        return fwStatusFromAllNode;
    }

    public void setFwStatusFromAllNode(String fwStatusFromAllNode) {
        this.fwStatusFromAllNode = fwStatusFromAllNode;
    }


    public String getHighInternetPriority() {
        return highInternetPriority;
    }

    public void setHighInternetPriority(String highInternetPriority) {
        this.highInternetPriority = highInternetPriority;
    }

    public boolean getAutoFwEnable() {
        if (autoFwEnable == null) {
            return false;
        }
        return autoFwEnable.equals("1");
    }

    public void setAutoFwEnable(boolean autoFwEnable) {
        this.autoFwEnable = (autoFwEnable) ? "1" : "0";
    }

    public boolean getAutoBandwidth() {
        if (autoBandwidth == null) {
            return false;
        }
        return autoBandwidth.equals("1");
    }

    public void setAutoBandwidth(boolean autoBandwidth) {
        this.autoBandwidth = (autoBandwidth) ? "1" : "0";
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
