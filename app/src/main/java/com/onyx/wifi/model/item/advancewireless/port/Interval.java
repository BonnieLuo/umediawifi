package com.onyx.wifi.model.item.advancewireless.port;

public class Interval {
    int start;
    int end;
    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
