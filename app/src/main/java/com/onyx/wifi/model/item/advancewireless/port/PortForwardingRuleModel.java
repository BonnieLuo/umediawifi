package com.onyx.wifi.model.item.advancewireless.port;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.StringUtils;

public class PortForwardingRuleModel {

    public PortForwardingRuleModel() {
        mEnable = "0";

        mName = "";

        mIp = "";

        mExternalPort = "-";

        mInternalPort = "-";

        mProtocol = "";
    }

    @SerializedName("enable")
    private String mEnable;

    @SerializedName("pfid")
    private String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("ip_addr")
    private String mIp;

    @SerializedName("external_port")
    private String mExternalPort;

    @SerializedName("internal_port")
    private String mInternalPort;

    @SerializedName("protocol")
    private String mProtocol;

    public String getEnable() {
        return mEnable;
    }

    public String getId() {
        return mId;
    }

    public String getName() { return mName; }

    public String getDisplayName() {
        return StringUtils.decodeHexString(mName);
    }

    public String getIp() {
        return mIp;
    }

    public String getExternalPort() {
        return mExternalPort;
    }

    public String getInternalPort() {
        return mInternalPort;
    }

    public String getProtocol() {
        return mProtocol;
    }

    public int getProtocolIndex(String protocol) {
        if (protocol.equalsIgnoreCase("tcp")) {
            return 0;
        }

        if (protocol.equalsIgnoreCase("udp")) {
            return 1;
        }

        if (protocol.equalsIgnoreCase("tcp & udp")) {
            return 2;
        }

        return -1;

    }

}
