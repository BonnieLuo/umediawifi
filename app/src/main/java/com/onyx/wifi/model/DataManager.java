package com.onyx.wifi.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.MainApp;
import com.onyx.wifi.R;
import com.onyx.wifi.model.item.BnbNetworkSetting;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.GuestNetworkSetting;
import com.onyx.wifi.model.item.NetworkMap;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.model.item.NodeSetInfo;
import com.onyx.wifi.model.item.ProvisionToken;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.model.item.UserInfo;
import com.onyx.wifi.model.item.WirelessInfo2g;
import com.onyx.wifi.model.item.WirelessInfo5g;
import com.onyx.wifi.model.item.WirelessSetting;
import com.onyx.wifi.model.item.advancewireless.AdvanceWirelessSettings;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfig;
import com.onyx.wifi.model.item.advancewireless.dmz.DestinationClient;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRule;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRule;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.PauseInfoModel;
import com.onyx.wifi.model.item.member.ScheduleConfiguration;
import com.onyx.wifi.model.item.pause.PauseProfile;
import com.onyx.wifi.model.item.pause.PauseProfileModel;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataManager {

    // DataManager 儲存及管理整個 App 全域的變數或資料, 應用情境如下:
    // (1) 需要跨 Activity 頁面判斷一些狀態之類的, 可將變數儲存在這裡
    // (2) 從 Cloud 抓取下來的資料, 有些需要儲存下來, 提供 App 顯示/判斷用

    private static final String SHARED_PREF_NAME = "app_data";

    private static final String KEY_CLOUD_SERVER_MODE = "cloud_server_mode";
    private static final String KEY_IS_SOCIAL_SIGN_IN = "is_social_sign_in";
    private static final String KEY_LOG_FILE_INDEX = "log_file_index";
    private static final String KEY_FIREBASE_REGISTRATION_TOKEN = "firebase_registration_token";
    private static final String KEY_USER_INFO = "user_info";
    private static final String KEY_DASHBOARD_INFO = "dashboard_info";
    private static final String KEY_WIRELESS_SETTING = "wireless_setting";
    private static final String KEY_GUEST_NETWORK_SETTING = "guest_network_setting";
    private static final String KEY_BNB_NETWORK_SETTING = "bnb_network_setting";
    private static final String KEY_ALL_SPEED_TEST = "all_speed_test";
    private static final String KEY_ALL_NODE_INFO = "all_node_info";    // for NodeSettingActivity
    private static final String KEY_PAUSE_INFO = "pause_info";  // for DashboardActivity global pause status

    private static DataManager mInstance = null;

    private Context mContext;

    private SharedPreferences mSharedPref;
    private JsonParserWrapper mJsonParser;

    private String mSignUpUid = "";
    private String mSignUpEmail = "";

    private Map<String, Integer> mClientPictureMap;
    private Map<String, Integer> mOnlineClientIconMap;
    private Map<String, Integer> mOfflineClientIconMap;

    private AppConstants.SetupFlow mSetupFlow = AppConstants.SetupFlow.NONE;
    private AppConstants.SetupMode mSetupMode = AppConstants.SetupMode.NONE;
    private ProvisionToken mDeviceProvisionToken = new ProvisionToken();
    private int mSetupFailCount = 0;
    private boolean mAfterInitialSetup = false;

    public static DataManager getInstance() {
        if (mInstance == null) {
            mInstance = new DataManager();
        }

        return mInstance;
    }

    private DataManager() {
        mContext = MainApp.getInstance();
        mSharedPref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        mJsonParser = new JsonParserWrapper();
    }

    public void clearUserData() {
        // 使用者登出 App 時, 要刪除 shared preference 的使用者資料, 以免登入另一個帳號時卻使用到上一個帳號的資料
        // 但有一些是 App 層級的資料 (不論哪個使用者都要共用這些資料), 這些資料就不能刪除, 除此之外的使用者資料都要刪除
        // (1) cloud server mode
        // (2) log file index

        // (1) cloud server mode
        AppConstants.CloudServerMode cloudServerMode = getCloudServerMode();

        // (2) log file index
        // 若登出時也將 log file index 清除, 會造成 log file index 變回 0, log 會從頭開始寫, 這樣會覆蓋掉之前的 log, 不理想
        // 故將 log file index 先保存起來, 等一下 clear all data 後再寫回去
        long logFileIndex = getLogFileIndex();

        // 使用 SharedPreferences.Editor.clear() 可以清除所有資料
        // 也可以依照 key 個別清除值 (將值設定為 null), 例如: setUserInfo(null); setDashboardInfo(null);
        // 不過這裡不使用依照 key 個別清除值的方式, 以免未來有新增 key, 但這裡忘記加上 set to null, 造成部份資料殘留
        mSharedPref.edit().clear().apply();

        // 清除使用者資料後, 將剛剛暫存的資料寫回去
        setCloudServerMode(cloudServerMode);
        setLogFileIndex(logFileIndex);
    }

    public void setCloudServerMode(AppConstants.CloudServerMode cloudServerMode) {
        setData(KEY_CLOUD_SERVER_MODE, cloudServerMode.ordinal());
    }

    public AppConstants.CloudServerMode getCloudServerMode() {
        AppConstants.CloudServerMode cloudServerMode = AppConstants.CloudServerMode.NONE;
        try {
            String ordinal = getData(KEY_CLOUD_SERVER_MODE);
            cloudServerMode = AppConstants.CloudServerMode.values()[Integer.parseInt(ordinal)];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cloudServerMode;
    }

    public void setSignUpUid(String signUpUid) {
        mSignUpUid = signUpUid;
    }

    public void setSignUpEmail(String signUpEmail) {
        mSignUpEmail = signUpEmail;
    }

    public void setSetupFlow(AppConstants.SetupFlow setupFlow) {
        mSetupFlow = setupFlow;
    }

    public AppConstants.SetupFlow getSetupFlow() {
        LogUtils.trace("setup flow = " + mSetupFlow.name());
        return mSetupFlow;
    }

    public void setSetupMode(AppConstants.SetupMode setupMode) {
        mSetupMode = setupMode;
    }

    public AppConstants.SetupMode getSetupMode() {
        LogUtils.trace("setup mode = " + mSetupMode.name());
        return mSetupMode;
    }

    public void setDeviceProvisionToken(ProvisionToken provisionToken) {
        mDeviceProvisionToken = provisionToken;
    }

    public ProvisionToken getDeviceProvisionToken() {
        return mDeviceProvisionToken;
    }

    public void setSetupFailCount(int setupFailCount) {
        mSetupFailCount = setupFailCount;
    }

    public int getSetupFailCount() {
        return mSetupFailCount;
    }

    public void setAfterInitialSetup(boolean afterInitialSetup) {
        LogUtils.trace("set mAfterInitialSetup = " + afterInitialSetup);
        this.mAfterInitialSetup = afterInitialSetup;
    }

    public boolean getAfterInitialSetup() {
        LogUtils.trace("get mAfterInitialSetup = " + mAfterInitialSetup);
        return mAfterInitialSetup;
    }

    private void setData(String key, String value) {
        mSharedPref.edit().putString(key, value).apply();
    }

    private void setData(String key, Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        setData(key, json);
    }

    private String getData(String key) {
        return mSharedPref.getString(key, "");
    }

    public void setLogFileIndex(long index) {
        setData(KEY_LOG_FILE_INDEX, String.valueOf(index));
    }

    public long getLogFileIndex() {
        long length = 0;
        try {
            length = Integer.valueOf(getData(KEY_LOG_FILE_INDEX));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return length;
    }

    public void setIsSocialSignIn(boolean isSocialSignIn) {

        if (isSocialSignIn)
            setData(KEY_IS_SOCIAL_SIGN_IN, "true");
        else
            setData(KEY_IS_SOCIAL_SIGN_IN, "false");
    }

    public boolean getIsSocialSignIn() {
        if (getData(KEY_IS_SOCIAL_SIGN_IN).equals("true"))
            return true;
        return false;
    }

    public void setUserInfo(UserInfo userInfo) {
//        LogUtils.trace("user info: " + userInfo);
        setData(KEY_USER_INFO, userInfo);
    }

    public UserInfo getUserInfo() {
        String json = getData(KEY_USER_INFO);
        return mJsonParser.jsonToObject(json, UserInfo.class);
    }

    // 取得現在登入的使用者的 uid
    public String getUserId() {
        FirebaseUser firebaseUser = AccountManager.getInstance().getUser();
        if (firebaseUser != null) {
            LogUtils.trace("Firebase user uid: " + firebaseUser.getUid());
            return firebaseUser.getUid();
        }

        // 使用者如果成功登入 App 時, App 會打 cloud API 取得 user info 並儲存下來, user info 就不會是 null
        // 取得儲存下來的 user info 並判斷
        UserInfo userInfo = getUserInfo();
        if (userInfo != null) {
            LogUtils.trace("user info uid: " + userInfo.getUid());
            return userInfo.getUid();
        }
        // 理論上 user info == null 只有在使用者剛註冊完、第一次進入 email 認證頁面時
        LogUtils.trace("sign up uid: " + mSignUpUid);
        return mSignUpUid;
    }

    // 取得現在登入的使用者的 email
    public String getUserEmail() {
        FirebaseUser firebaseUser = AccountManager.getInstance().getUser();
        if (firebaseUser != null) {
            LogUtils.trace("Firebase user email: " + firebaseUser.getEmail());
            return firebaseUser.getEmail();
        }

        // 使用者如果成功登入 App 時, App 會打 cloud API 取得 user info 並儲存下來, user info 就不會是 null
        // 取得儲存下來的 user info 並判斷
        UserInfo userInfo = getUserInfo();
        if (userInfo != null) {
            LogUtils.trace("user info email: " + userInfo.getEmail());
            return userInfo.getEmail();
        }
        // 理論上 user info == null 只有在使用者剛註冊完、第一次進入 email 認證頁面時
        LogUtils.trace("sign up email: " + mSignUpEmail);
        return mSignUpEmail;
    }

    public void setDashboardInfo(DashboardInfo dashboardInfo) {
//        LogUtils.trace("dashboard info: " + dashboardInfo);
        setData(KEY_DASHBOARD_INFO, dashboardInfo);
    }

    public DashboardInfo getDashboardInfo() {
        String json = getData(KEY_DASHBOARD_INFO);
        return mJsonParser.jsonToObject(json, DashboardInfo.class);
    }

    public NodeSetInfo getDashboardNodeSet() {
        NodeSetInfo nodeSetInfo = null;
        DashboardInfo dashboardInfo = getDashboardInfo();
        if (dashboardInfo != null) {
            ArrayList<NodeSetInfo> dashboardNodeSets = dashboardInfo.getNodeSets();
            if (dashboardNodeSets != null && dashboardNodeSets.size() > 0) {
                // 取第一個 node set, 因為目前產品的整體系統架構 (device + cloud + App) 設計成一個使用者只會有一個 network (node set)
                nodeSetInfo = dashboardNodeSets.get(0);
            }
        }
        return nodeSetInfo;
    }

    public void setDashboardNodeSet(NodeSetInfo nodeSetInfo) {
        DashboardInfo dashboardInfo = getDashboardInfo();
        if (dashboardInfo != null) {
            ArrayList<NodeSetInfo> dashboardNodeSets = dashboardInfo.getNodeSets();
            if (dashboardNodeSets != null && dashboardNodeSets.size() > 0) {
                // update dashboard info
                dashboardNodeSets.set(0, nodeSetInfo);
                dashboardInfo.setNodeSets(dashboardNodeSets);

                // save data to share preference
                setDashboardInfo(dashboardInfo);
            }
        }
    }

    public String getNodeSetId() {
        String nodeSetId = "";

        // 目前產品的整體系統架構 (device + cloud + App) 設計成一個使用者只會有一個 network (node set), user info 和 dashboard info 都會儲存 node set 資訊
        // user info 儲存 network 簡化的資訊 (只有 node set id, controller id, network name), dashboard info 儲存全部的資訊

        // 先從 dashboard info 拿取 node set id 資訊
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            nodeSetId = dashboardNodeSet.getNodeSetId();
        }

        // 若 dashboard info 拿不到 node set id, 再從 user info 拿取
        if (nodeSetId.isEmpty()) {
            LogUtils.trace("get node set id from user info");
            UserInfo userInfo = getUserInfo();
            ArrayList<UserInfo.NodeSetInfo> nodeSets = userInfo.getNodeSets();
            if (nodeSets != null && nodeSets.size() > 0) {
                nodeSetId = nodeSets.get(0).getId();
            }
        }

        return nodeSetId;
    }

    public String getNetworkName() {
        // 客戶定義 Dashboard 的 network name 要顯示的是 2.4G SSID
        WirelessInfo2g wirelessInfo2g = getWirelessSetting().getWireless2g();
        String networkName = wirelessInfo2g.getSsid();
        if (networkName.isEmpty()) {
            UserInfo userInfo = getUserInfo();
            if (userInfo != null && userInfo.getNodeSets().size() > 0) {
                // default network name
                networkName = userInfo.getNodeSets().get(0).getName();
            }
        }
        return networkName;
    }

    public String getControllerDid() {
        String controllerDid = "";
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            DeviceInfo deviceInfo = dashboardNodeSet.getDeviceInfo();
            if (deviceInfo != null) {
                controllerDid = deviceInfo.getDid();
            }
        } else {
            UserInfo userInfo = getUserInfo();
            if (userInfo != null) {
                controllerDid = userInfo.getNodeSets().get(0).getControllerDid();
            }
        }
        return controllerDid;
    }

    public NetworkSetting getNetworkSetting() {
        NetworkSetting networkSetting = null;
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            networkSetting = dashboardNodeSet.getNetworkSetting();
        }
        if (networkSetting == null) {
            networkSetting = new NetworkSetting();
        }
        return networkSetting;
    }

    public void updateNetworkSetting(NetworkSetting networkSetting) {
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            LogUtils.trace("network setting: " + networkSetting);
            dashboardNodeSet.setNetworkSetting(networkSetting);
            setDashboardNodeSet(dashboardNodeSet);
        }
    }

    public void updateDeviceLabel(String did, String deviceLabel) {
        // 更新對應 did 的 node info
        DeviceInfo nodeInfo = getNodeInfo(did);
        nodeInfo.setDeviceLabel(deviceLabel);
        setNodeInfo(did, nodeInfo);
    }

    public void updateClientCount(int total) {
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            dashboardNodeSet.setClientListCount(total);
            setDashboardNodeSet(dashboardNodeSet);
        }
    }

    // 收到 cloud notify 時, 更新資料
    public void updateDeviceOnlineStatus(String did, boolean isOnline) {
        // 更新對應 did 的 node info
        DeviceInfo nodeInfo = getNodeInfo(did);
        nodeInfo.setOnline(isOnline);
        setNodeInfo(did, nodeInfo);
    }

    // 主動 get speed test result 或是收到 cloud notify 時, 更新資料
    public void updateSpeedTest(String did, SpeedTest speedTest) {
        JsonObject allSpeedTest = mJsonParser.jsonToObject(getData(KEY_ALL_SPEED_TEST), JsonObject.class);
        if (allSpeedTest == null) {
            allSpeedTest = new JsonObject();
        }
        allSpeedTest.addProperty(did, speedTest.toJsonString());
        setData(KEY_ALL_SPEED_TEST, allSpeedTest);
    }

    // 打 cloud API 做 speed test, 收到 API 回覆 InProgress, 更新狀態
    public void updateSpeedTestInProgress(String did) {
        SpeedTest speedTest;
        JsonObject allSpeedTest = mJsonParser.jsonToObject(getData(KEY_ALL_SPEED_TEST), JsonObject.class);
        speedTest = mJsonParser.jsonToObject(mJsonParser.jsonGetString(allSpeedTest, did), SpeedTest.class);
        if (speedTest == null) {
            speedTest = new SpeedTest();
        }
        speedTest.setStatus(0);
        allSpeedTest.addProperty(did, speedTest.toJsonString());
        setData(KEY_ALL_SPEED_TEST, allSpeedTest);
    }

    public SpeedTest getSpeedTest(String did) {
        SpeedTest speedTest;
        JsonObject allSpeedTest = mJsonParser.jsonToObject(getData(KEY_ALL_SPEED_TEST), JsonObject.class);
        speedTest = mJsonParser.jsonToObject(mJsonParser.jsonGetString(allSpeedTest, did), SpeedTest.class);
        if (speedTest == null) {
            speedTest = new SpeedTest();
        }
        return speedTest;
    }

    public void setWirelessSetting(WirelessSetting wirelessSetting) {
        setData(KEY_WIRELESS_SETTING, wirelessSetting);
    }

    public WirelessSetting getWirelessSetting() {
        WirelessSetting wirelessSetting = mJsonParser.jsonToObject(getData(KEY_WIRELESS_SETTING), WirelessSetting.class);
        if (wirelessSetting == null) {
            wirelessSetting = new WirelessSetting();
        }
        return wirelessSetting;
    }

    public WirelessInfo2g getWirelessInfo2g() {
        WirelessInfo2g wirelessInfo = null;
        WirelessSetting wirelessSetting = getWirelessSetting();
        if (wirelessSetting != null) {
            wirelessInfo = wirelessSetting.getWireless2g();
        }
        if (wirelessInfo == null) {
            wirelessInfo = new WirelessInfo2g();
        }
        return wirelessInfo;
    }

    public WirelessInfo5g getWirelessInfo5gFronthaul() {
        return getWirelessInfo5g("0");
    }

    public WirelessInfo5g getWirelessInfo5gBackhaul() {
        return getWirelessInfo5g("1");
    }

    private WirelessInfo5g getWirelessInfo5g(String bh) {
        WirelessInfo5g wirelessInfo = null;
        WirelessSetting wirelessSetting = getWirelessSetting();
        if (wirelessSetting != null) {
            WirelessInfo5g wirelessInfo5g1 = wirelessSetting.getWireless5g();
            WirelessInfo5g wirelessInfo5g2 = wirelessSetting.getWireless5g2();
            if (wirelessInfo5g1.getBh().equals(bh)) {
                wirelessInfo = wirelessInfo5g1;
            } else if (wirelessInfo5g2.getBh().equals(bh)) {
                wirelessInfo = wirelessInfo5g2;
            }
        }
        if (wirelessInfo == null) {
            wirelessInfo = new WirelessInfo5g();
            wirelessInfo.setBh(bh);
        }
        return wirelessInfo;
    }

    public void setGuestNetworkSetting(GuestNetworkSetting guestNetworkSetting) {
//        LogUtils.trace(guestNetworkSetting.toString());
        setData(KEY_GUEST_NETWORK_SETTING, guestNetworkSetting);
    }

    public GuestNetworkSetting getGuestNetworkSetting() {
        return mJsonParser.jsonToObject(getData(KEY_GUEST_NETWORK_SETTING), GuestNetworkSetting.class);
    }

    public void setBnbNetworkSetting(BnbNetworkSetting bnbNetworkSetting) {
//        LogUtils.trace(bnbNetworkSetting.toString());
        setData(KEY_BNB_NETWORK_SETTING, bnbNetworkSetting);
    }

    public BnbNetworkSetting getBnbNetworkSetting() {
        return mJsonParser.jsonToObject(getData(KEY_BNB_NETWORK_SETTING), BnbNetworkSetting.class);
    }

    public void setNodeInfo(String did, DeviceInfo nodeInfo) {
//        LogUtils.trace("did: " + did + ", node info: " + nodeInfo);
        JsonObject allNodeInfo = mJsonParser.jsonToObject(getData(KEY_ALL_NODE_INFO), JsonObject.class);
        if (allNodeInfo == null) {
            allNodeInfo = new JsonObject();
        }
        allNodeInfo.addProperty(did, nodeInfo.toJsonString());
        setData(KEY_ALL_NODE_INFO, allNodeInfo);

        // 如果是 controller(router) did, 將資料寫回 dashboard info
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            DeviceInfo routerDeviceInfo = dashboardNodeSet.getDeviceInfo();
            if (routerDeviceInfo != null) {
                String controllerDid = routerDeviceInfo.getDid();
                if (did.equals(controllerDid)) {
                    dashboardNodeSet.setControllerOnline(nodeInfo.getOnline());

                    // 只有 dashboard API 回傳的 router device info 會有 timezone 資訊
                    // 而傳進來的 router device info 可能來自其他 API, 不一定有 timezone 資訊
                    // 所以將傳進來的 router device info 寫回 dashboard info 時, 原本的 timezone 資料要保留
                    String timezone = routerDeviceInfo.getCity();
                    nodeInfo.setCity(timezone);
                    dashboardNodeSet.setDeviceInfo(nodeInfo);

                    setDashboardNodeSet(dashboardNodeSet);
                }
            }
        }
    }

    public DeviceInfo getNodeInfo(String did) {
        DeviceInfo nodeInfo;
        JsonObject allNodeInfo = mJsonParser.jsonToObject(getData(KEY_ALL_NODE_INFO), JsonObject.class);
        nodeInfo = mJsonParser.jsonToObject(mJsonParser.jsonGetString(allNodeInfo, did), DeviceInfo.class);
        if (nodeInfo == null) {
            nodeInfo = new DeviceInfo();
        }
        return nodeInfo;
    }

    // save @GET("user/devices") API response data
    public void setNodeList(ArrayList<DeviceInfo> nodeList) {
        // get node list API 取得的資料是當下最新的所有 node 資料
        // 與原本的 share preference 資料相比, add/update/delete node 都有可能, 故先清除原本的資料, 再將新的資料全部寫進去
        setData(KEY_ALL_NODE_INFO, "");
        for (DeviceInfo nodeInfo : nodeList) {
            setNodeInfo(nodeInfo.getDid(), nodeInfo);
        }
    }

    public ArrayList<DeviceInfo> getNodeList() {
        ArrayList<DeviceInfo> nodeList = new ArrayList<>();

        JsonObject allNodeInfo = mJsonParser.jsonToObject(getData(KEY_ALL_NODE_INFO), JsonObject.class);
        if (allNodeInfo != null) {
            for (String did : allNodeInfo.keySet()) {
                DeviceInfo nodeInfo = mJsonParser.jsonToObject(mJsonParser.jsonGetString(allNodeInfo, did), DeviceInfo.class);
                nodeList.add(nodeInfo);
            }
        }

        return nodeList;
    }

    public void setNodeInfoFromNetworkMap(NetworkMap networkMap) {
        // 進入 node setting 頁面時一開始顯示的資料會從 share preference 取得
        // 故將 network map 取得的 node 資料更新回去
        DeviceInfo rootNode = networkMap.getRootDevice();
        setNodeInfo(rootNode.getDid(), rootNode);

        ArrayList<DeviceInfo> nodeList = networkMap.getNodeList();
        for (DeviceInfo nodeInfo : nodeList) {
            setNodeInfo(nodeInfo.getDid(), nodeInfo);
        }
    }

    public void setPauseInfoModel(PauseInfoModel pauseInfoModel) {
        setData(KEY_PAUSE_INFO, pauseInfoModel);
    }

    public PauseInfoModel getPauseInfoModel() {
        return mJsonParser.jsonToObject(getData(KEY_PAUSE_INFO), PauseInfoModel.class);
    }

    // Member
    private MemberList mMemberList = null;

    public MemberList getMemberList() {
        return mMemberList;
    }

    public void setMemberList(MemberList memberList) {
        mMemberList = memberList;
    }

    // Member
    private FamilyMember mManageMember = null;

    public FamilyMember getManageMember() {
        return mManageMember;
    }

    public void setManageMember(FamilyMember member) {
        mManageMember = member;
    }

    // Client

    private Client mManageClient = null;

    public Client getManageClient() {
        return mManageClient;
    }

    public void setManageClient(Client manageClient) {
        mManageClient = manageClient;
    }

    // Recently Active Clients

    private List<Client> mRecentlyActiveClients = null;
    private List<Client> mRecentlyActiveAllowedClients = new ArrayList<Client>();

    // for顯示only, 移除disallowed client
    public List<Client> getRecentlyActiveAllowedClients() {
        mRecentlyActiveAllowedClients.clear();
        for (Client client : mRecentlyActiveClients) {
            if (!client.getClientModel().getDisallowed())
                mRecentlyActiveAllowedClients.add(client);
        }
        return mRecentlyActiveAllowedClients;
    }

    public void setRecentlyActiveClients(List<Client> recentlyActiveClients) {
        mRecentlyActiveClients = recentlyActiveClients;
    }

    // Manage Member Schedule

    private ScheduleConfiguration mScheduleConfiguration = null;

    public ScheduleConfiguration getScheduleConfiguration() {
        return mScheduleConfiguration;
    }

    public void setScheduleConfiguration(ScheduleConfiguration scheduleConfig) {
        mScheduleConfiguration = scheduleConfig;
    }

    // Selected Client List
    private List<DestinationClient> mSelectedClientList;

    public List<DestinationClient> getSelectedClientList() {
        return mSelectedClientList;
    }

    public void setSelectedClientList(List<DestinationClient> clientList) {
        mSelectedClientList = clientList;
    }


    // Advance Wireless Settings
    private AdvanceWirelessSettings mAdvanceWirelessSettings = null;

    public AdvanceWirelessSettings getAdvanceWirelessSettings() {
        return mAdvanceWirelessSettings;
    }

    public void setAdvanceWirelessSettings(AdvanceWirelessSettings advanceWirelessSettings) {
        mAdvanceWirelessSettings = advanceWirelessSettings;
    }

    // DMZ

    private DemilitarizedZoneConfig mDmzConfig = null;

    public DemilitarizedZoneConfig getDmzConfig() {
        return mDmzConfig;
    }

    public void setDmzConfig(DemilitarizedZoneConfig dmzConfig) {
        mDmzConfig = dmzConfig;
    }

    // Port Forwarding

    private PortForwardingRule mPortForwardingRule = null;

    public PortForwardingRule getPortForwardingRule() {
        return mPortForwardingRule;
    }

    public void setPortForwardingRule(PortForwardingRule rule) {
        mPortForwardingRule = rule;
    }

    private String mSelectedClientIp;

    public String getSelectedClientIp() {
        return mSelectedClientIp;
    }

    public void setSelectedClientIp(String ip) {
        mSelectedClientIp = ip;
    }

    // Port Triggering

    private PortTriggeringRule mPortTriggeringRule = null;

    public PortTriggeringRule getPortTriggeringRule() {
        return mPortTriggeringRule;
    }

    public void setPortTriggeringRule(PortTriggeringRule rule) {
        mPortTriggeringRule = rule;
    }

    // Pause

    private List<PauseProfileModel> mPauseProfileList;

    public List<PauseProfileModel> getPauseProfileList() {
        return mPauseProfileList;
    }

    public void setPauseProfileList(List<PauseProfileModel> pauseProfileList) {
        mPauseProfileList = pauseProfileList;
    }

    private PauseProfile mEditPauseProfile;

    public PauseProfile getEditPauseProfile() {
        return mEditPauseProfile;
    }

    public void setEditPauseProfile(PauseProfile editPauseProfile) {
        mEditPauseProfile = editPauseProfile;
    }

    private List<ClientModel> mAllClientModelList;

    public List<ClientModel> getAllClientModelList() {
        return mAllClientModelList;
    }

    public void setAllClientModelList(List<ClientModel> allClientModelList) {
        mAllClientModelList = allClientModelList;

    }

    // Client Icon
    public Map<String, Integer> getClientPictureMap() {
        if (mClientPictureMap == null) {
            mClientPictureMap = generateClientPictureMap();
        }

        return mClientPictureMap;
    }

    public Map<String, Integer> getOnlineClientIconMap() {
        if (mOnlineClientIconMap == null) {
            mOnlineClientIconMap = generateOnlineClientIconMap();
        }

        return mOnlineClientIconMap;
    }

    public Map<String, Integer> getOfflineClientIconMap() {
        if (mOfflineClientIconMap == null) {
            mOfflineClientIconMap = generateOfflineClientIconMap();
        }

        return mOfflineClientIconMap;
    }

    public Map<String, Integer> generateClientPictureMap() {
        Map<String, Integer> map = new HashMap<>();

        // mobile

        map.put("generic wi-fi", R.drawable.pic_generic_wifi);
        map.put("generic lan", R.drawable.pic_generic_lan);
        map.put("mobile", R.drawable.pic_mobile);
        map.put("tablet", R.drawable.pic_tablet);
        map.put("mp3 player", R.drawable.pic_ipod);
        map.put("ebook reader", R.drawable.pic_ereader);
        map.put("smart watch", R.drawable.pic_watch);
        map.put("wearable", R.drawable.pic_wearable);
        map.put("car", R.drawable.pic_car);

        // audio & video

        map.put("media player", R.drawable.pic_media_player);
        map.put("television", R.drawable.pic_television);
        map.put("game console", R.drawable.pic_game_console);
        map.put("streaming dongle", R.drawable.pic_streaming_dongle);
        map.put("speaker/amp", R.drawable.pic_loudspeaker);
        map.put("av receiver", R.drawable.pic_sound_system);
        map.put("cable box", R.drawable.pic_stb);
        map.put("disc player", R.drawable.pic_disc_player);
        map.put("satellite", R.drawable.pic_satellite);
        map.put("audio player", R.drawable.pic_music);
        map.put("remote control", R.drawable.pic_remote_control);
        map.put("radio", R.drawable.pic_radio);
        map.put("photo camera", R.drawable.pic_photo_camera);
        map.put("photo display", R.drawable.pic_photos);
        map.put("mic", R.drawable.pic_mic);
        map.put("projector", R.drawable.pic_projector);

        // home & office

        map.put("computer", R.drawable.pic_computer);
        map.put("laptop", R.drawable.pic_laptop);
        map.put("desktop", R.drawable.pic_desktop);
        map.put("printer", R.drawable.pic_print);
//        map.put("fax", R.drawable.pic_);
        map.put("ip phone", R.drawable.pic_phone);
        map.put("scanner", R.drawable.pic_scanner);
        map.put("point of sale", R.drawable.pic_pos);
        map.put("clock", R.drawable.pic_clock);
        map.put("barcode scanner", R.drawable.pic_barcode);

        // smart home

        map.put("automation ip camera", R.drawable.pic_surveillance_camera);
        map.put("smart device", R.drawable.pic_smart_home);
        map.put("smart plug", R.drawable.pic_smart_plug);
        map.put("light", R.drawable.pic_light);
        map.put("voice control", R.drawable.pic_voice_control);
        map.put("thermostat", R.drawable.pic_thermostat);
        map.put("power system", R.drawable.pic_power_system);
        map.put("solar panel", R.drawable.pic_solar_panel);
        map.put("smart meter", R.drawable.pic_smart_meter);
        map.put("hvac", R.drawable.pic_heating);
        map.put("smart appliance", R.drawable.pic_appliance);
        map.put("smart washer", R.drawable.pic_washer);
        map.put("smart fridge", R.drawable.pic_fridge);
        map.put("smart cleaner", R.drawable.pic_cleaner);
        map.put("sleep tech", R.drawable.pic_sleep);
        map.put("garage door", R.drawable.pic_garage);
        map.put("sprinkler", R.drawable.pic_sprinkler);
        map.put("electric", R.drawable.pic_electric);
        map.put("doorbell", R.drawable.pic_bell);
        map.put("smart lock", R.drawable.pic_key_lock);
        map.put("touch panel", R.drawable.pic_control_panel);
        map.put("controller", R.drawable.pic_smart_controller);
        map.put("scale", R.drawable.pic_scale);
        map.put("toy", R.drawable.pic_toy);
        map.put("robot", R.drawable.pic_robot);
        map.put("weather station", R.drawable.pic_weather);
        map.put("health monitor", R.drawable.pic_health_monitor);
        map.put("baby monitor", R.drawable.pic_baby_monitor);
        map.put("pet monitor", R.drawable.pic_pet_monitor);
        map.put("alarm", R.drawable.pic_alarm);
        map.put("motion detector", R.drawable.pic_motion_detector);
        map.put("smoke detector", R.drawable.pic_smoke);
        map.put("water sensor", R.drawable.pic_humidity);
        map.put("sensor", R.drawable.pic_sensor);
        map.put("fingbox", R.drawable.pic_fingbox);
        map.put("domotz box", R.drawable.pic_domotz_box);

        // network

        map.put("router", R.drawable.pic_router);
        map.put("wi-fi", R.drawable.pic_wifi);
        map.put("wi-fi extender", R.drawable.pic_wifi_extender);
        map.put("nas", R.drawable.pic_nas_storage);
        map.put("modem", R.drawable.pic_modem);
        map.put("switch", R.drawable.pic_switch);
        map.put("gateway", R.drawable.pic_gateway);
        map.put("firewall", R.drawable.pic_firewall);
        map.put("vpn", R.drawable.pic_vpn);
        map.put("poe switch", R.drawable.pic_poe_plug);
        map.put("usb", R.drawable.pic_usb);
        map.put("small cell", R.drawable.pic_small_cell);
        map.put("cloud", R.drawable.pic_cloud);
        map.put("ups", R.drawable.pic_battery);
        map.put("network appliance", R.drawable.pic_network_appliance);

        // server

        map.put("virtual machine", R.drawable.pic_virtual_machine);
        map.put("server", R.drawable.pic_server);
        map.put("terminal", R.drawable.pic_terminal);
        map.put("mail server", R.drawable.pic_mail_server);
        map.put("file server", R.drawable.pic_file_server);
        map.put("proxy server", R.drawable.pic_proxy_server);
        map.put("web server", R.drawable.pic_web_server);
        map.put("domain server", R.drawable.pic_domain_server);
        map.put("communication", R.drawable.pic_communication);
        map.put("database", R.drawable.pic_database);

        // engineering

        map.put("raspberry", R.drawable.pic_raspberry);
        map.put("arduino", R.drawable.pic_arduino);
        map.put("processing", R.drawable.pic_processor);
        map.put("circuit board", R.drawable.pic_circuit_card);
        map.put("rfid tag", R.drawable.pic_rfid);

        return map;
    }

    public Map<String, Integer> generateOnlineClientIconMap() {
        Map<String, Integer> map = new HashMap<>();

        // mobile

        map.put("generic wi-fi", R.drawable.ic_generic_wifi);
        map.put("generic lan", R.drawable.ic_generic_lan);
        map.put("mobile", R.drawable.ic_mobile);
        map.put("tablet", R.drawable.ic_tablet);
        map.put("mp3 player", R.drawable.ic_ipod);
        map.put("ebook reader", R.drawable.ic_ereader);
        map.put("smart watch", R.drawable.ic_watch);
        map.put("wearable", R.drawable.ic_wearable);
        map.put("car", R.drawable.ic_car);

        // audio & video

        map.put("media player", R.drawable.ic_media_player);
        map.put("television", R.drawable.ic_television);
        map.put("game console", R.drawable.ic_game_console);
        map.put("streaming dongle", R.drawable.ic_streaming_dongle);
        map.put("speaker/amp", R.drawable.ic_loudspeaker);
        map.put("av receiver", R.drawable.ic_sound_system);
        map.put("cable box", R.drawable.ic_stb);
        map.put("disc player", R.drawable.ic_disc_player);
        map.put("satellite", R.drawable.ic_satellite);
        map.put("audio player", R.drawable.ic_music);
        map.put("remote control", R.drawable.ic_remote_control);
        map.put("radio", R.drawable.ic_radio);
        map.put("photo camera", R.drawable.ic_photo_camera);
        map.put("photo display", R.drawable.ic_photos);
        map.put("mic", R.drawable.ic_mic);
        map.put("projector", R.drawable.ic_projector);

        // home & office

        map.put("computer", R.drawable.ic_computer);
        map.put("laptop", R.drawable.ic_laptop);
        map.put("desktop", R.drawable.ic_desktop);
        map.put("printer", R.drawable.ic_print);
//        map.put("fax", R.drawable.ic_);
        map.put("ip phone", R.drawable.ic_phone);
        map.put("scanner", R.drawable.ic_scanner);
        map.put("point of sale", R.drawable.ic_pos);
        map.put("clock", R.drawable.ic_clock);
        map.put("barcode scanner", R.drawable.ic_barcode);

        // smart home

        map.put("automation ip camera", R.drawable.ic_surveillance_camera);
        map.put("smart device", R.drawable.ic_smart_home);
        map.put("smart plug", R.drawable.ic_smart_plug);
        map.put("light", R.drawable.ic_light);
        map.put("voice control", R.drawable.ic_voice_control);
        map.put("thermostat", R.drawable.ic_thermostat);
        map.put("power system", R.drawable.ic_power_system);
        map.put("solar panel", R.drawable.ic_solar_panel);
        map.put("smart meter", R.drawable.ic_smart_meter);
        map.put("hvac", R.drawable.ic_heating);
        map.put("smart appliance", R.drawable.ic_appliance);
        map.put("smart washer", R.drawable.ic_washer);
        map.put("smart fridge", R.drawable.ic_fridge);
        map.put("smart cleaner", R.drawable.ic_cleaner);
        map.put("sleep tech", R.drawable.ic_sleep);
        map.put("garage door", R.drawable.ic_garage);
        map.put("sprinkler", R.drawable.ic_sprinkler);
        map.put("electric", R.drawable.ic_electric);
        map.put("doorbell", R.drawable.ic_bell);
        map.put("smart lock", R.drawable.ic_key_lock);
        map.put("touch panel", R.drawable.ic_control_panel);
        map.put("controller", R.drawable.ic_smart_controller);
        map.put("scale", R.drawable.ic_scale);
        map.put("toy", R.drawable.ic_toy);
        map.put("robot", R.drawable.ic_robot);
        map.put("weather station", R.drawable.ic_weather);
        map.put("health monitor", R.drawable.ic_health_monitor);
        map.put("baby monitor", R.drawable.ic_baby_monitor);
        map.put("pet monitor", R.drawable.ic_pet_monitor);
        map.put("alarm", R.drawable.ic_alarm);
        map.put("motion detector", R.drawable.ic_motion_detector);
        map.put("smoke detector", R.drawable.ic_smoke);
        map.put("water sensor", R.drawable.ic_humidity);
        map.put("sensor", R.drawable.ic_sensor);
        map.put("fingbox", R.drawable.ic_fingbox);
        map.put("domotz box", R.drawable.ic_domotz_box);

        // network

        map.put("router", R.drawable.ic_router);
        map.put("wi-fi", R.drawable.ic_wifi);
        map.put("wi-fi extender", R.drawable.ic_wifi_extender);
        map.put("nas", R.drawable.ic_nas_storage);
        map.put("modem", R.drawable.ic_modem);
        map.put("switch", R.drawable.ic_switch);
        map.put("gateway", R.drawable.ic_gateway);
        map.put("firewall", R.drawable.ic_firewall);
        map.put("vpn", R.drawable.ic_vpn);
        map.put("poe switch", R.drawable.ic_poe_plug);
        map.put("usb", R.drawable.ic_usb);
        map.put("small cell", R.drawable.ic_small_cell);
        map.put("cloud", R.drawable.ic_cloud);
        map.put("ups", R.drawable.ic_battery);
        map.put("network appliance", R.drawable.ic_network_appliance);

        // server

        map.put("virtual machine", R.drawable.ic_virtual_machine);
        map.put("server", R.drawable.ic_server);
        map.put("terminal", R.drawable.ic_terminal);
        map.put("mail server", R.drawable.ic_mail_server);
        map.put("file server", R.drawable.ic_file_server);
        map.put("proxy server", R.drawable.ic_proxy_server);
        map.put("web server", R.drawable.ic_web_server);
        map.put("domain server", R.drawable.ic_domain_server);
        map.put("communication", R.drawable.ic_communication);
        map.put("database", R.drawable.ic_database);

        // engineering

        map.put("raspberry", R.drawable.ic_raspberry);
        map.put("arduino", R.drawable.ic_arduino);
        map.put("processing", R.drawable.ic_processor);
        map.put("circuit board", R.drawable.ic_circuit_card);
        map.put("rfid tag", R.drawable.ic_rfid);

        return map;
    }

    public Map<String, Integer> generateOfflineClientIconMap() {
        Map<String, Integer> map = new HashMap<>();

        // mobile

        map.put("generic wi-fi", R.drawable.ic_generic_wifi_offline);
        map.put("generic lan", R.drawable.ic_generic_lan_offline);
        map.put("mobile", R.drawable.ic_mobile_offline);
        map.put("tablet", R.drawable.ic_tablet_offline);
        map.put("mp3 player", R.drawable.ic_ipod_offline);
        map.put("ebook reader", R.drawable.ic_ereader_offline);
        map.put("smart watch", R.drawable.ic_watch_offline);
        map.put("wearable", R.drawable.ic_wearable_offline);
        map.put("car", R.drawable.ic_car_offline);

        // audio & video

        map.put("media player", R.drawable.ic_media_player_offline);
        map.put("television", R.drawable.ic_television_offline);
        map.put("game console", R.drawable.ic_game_console_offline);
        map.put("streaming dongle", R.drawable.ic_streaming_dongle_offline);
        map.put("speaker/amp", R.drawable.ic_loudspeaker_offline);
        map.put("av receiver", R.drawable.ic_sound_system_offline);
        map.put("cable box", R.drawable.ic_stb_offline);
        map.put("disc player", R.drawable.ic_disc_player_offline);
        map.put("satellite", R.drawable.ic_satellite_offline);
        map.put("audio player", R.drawable.ic_music_offline);
        map.put("remote control", R.drawable.ic_remote_control_offline);
        map.put("radio", R.drawable.ic_radio_offline);
        map.put("photo camera", R.drawable.ic_photo_camera_offline);
        map.put("photo display", R.drawable.ic_photos_offline);
        map.put("mic", R.drawable.ic_mic_offline);
        map.put("projector", R.drawable.ic_projector_offline);

        // home & office

        map.put("computer", R.drawable.ic_computer_offline);
        map.put("laptop", R.drawable.ic_laptop_offline);
        map.put("desktop", R.drawable.ic_desktop_offline);
        map.put("printer", R.drawable.ic_print_offline);
//        map.put("fax", R.drawable.ic__offline);
        map.put("ip phone", R.drawable.ic_phone_offline);
        map.put("scanner", R.drawable.ic_scanner_offline);
        map.put("point of sale", R.drawable.ic_pos_offline);
        map.put("clock", R.drawable.ic_clock_offline);
        map.put("barcode scanner", R.drawable.ic_barcode_offline);

        // smart home

        map.put("automation ip camera", R.drawable.ic_surveillance_camera_offline);
        map.put("smart device", R.drawable.ic_smart_home_offline);
        map.put("smart plug", R.drawable.ic_smart_plug_offline);
        map.put("light", R.drawable.ic_light_offline);
        map.put("voice control", R.drawable.ic_voice_control_offline);
        map.put("thermostat", R.drawable.ic_thermostat_offline);
        map.put("power system", R.drawable.ic_power_system_offline);
        map.put("solar panel", R.drawable.ic_solar_panel_offline);
        map.put("smart meter", R.drawable.ic_smart_meter_offline);
        map.put("hvac", R.drawable.ic_heating_offline);
        map.put("smart appliance", R.drawable.ic_appliance_offline);
        map.put("smart washer", R.drawable.ic_washer_offline);
        map.put("smart fridge", R.drawable.ic_fridge_offline);
        map.put("smart cleaner", R.drawable.ic_cleaner_offline);
        map.put("sleep tech", R.drawable.ic_sleep_offline);
        map.put("garage door", R.drawable.ic_garage_offline);
        map.put("sprinkler", R.drawable.ic_sprinkler_offline);
        map.put("electric", R.drawable.ic_electric_offline);
        map.put("doorbell", R.drawable.ic_bell_offline);
        map.put("smart lock", R.drawable.ic_key_lock_offline);
        map.put("touch panel", R.drawable.ic_control_panel_offline);
        map.put("controller", R.drawable.ic_smart_controller_offline);
        map.put("scale", R.drawable.ic_scale_offline);
        map.put("toy", R.drawable.ic_toy_offline);
        map.put("robot", R.drawable.ic_robot_offline);
        map.put("weather station", R.drawable.ic_weather_offline);
        map.put("health monitor", R.drawable.ic_health_monitor_offline);
        map.put("baby monitor", R.drawable.ic_baby_monitor_offline);
        map.put("pet monitor", R.drawable.ic_pet_monitor_offline);
        map.put("alarm", R.drawable.ic_alarm_offline);
        map.put("motion detector", R.drawable.ic_motion_detector_offline);
        map.put("smoke detector", R.drawable.ic_smoke_offline);
        map.put("water sensor", R.drawable.ic_humidity_offline);
        map.put("sensor", R.drawable.ic_sensor_offline);
        map.put("fingbox", R.drawable.ic_fingbox_offline);
        map.put("domotz box", R.drawable.ic_domotz_box_offline);

        // network

        map.put("router", R.drawable.ic_router_offline);
        map.put("wi-fi", R.drawable.ic_wifi_offline);
        map.put("wi-fi extender", R.drawable.ic_wifi_extender_offline);
        map.put("nas", R.drawable.ic_nas_storage_offline);
        map.put("modem", R.drawable.ic_modem_offline);
        map.put("switch", R.drawable.ic_switch_offline);
        map.put("gateway", R.drawable.ic_gateway_offline);
        map.put("firewall", R.drawable.ic_firewall_offline);
        map.put("vpn", R.drawable.ic_vpn_offline);
        map.put("poe switch", R.drawable.ic_poe_plug_offline);
        map.put("usb", R.drawable.ic_usb_offline);
        map.put("small cell", R.drawable.ic_small_cell_offline);
        map.put("cloud", R.drawable.ic_cloud_offline);
        map.put("ups", R.drawable.ic_battery_offline);
        map.put("network appliance", R.drawable.ic_network_appliance_offline);

        // server

        map.put("virtual machine", R.drawable.ic_virtual_machine_offline);
        map.put("server", R.drawable.ic_server_offline);
        map.put("terminal", R.drawable.ic_terminal_offline);
        map.put("mail server", R.drawable.ic_mail_server_offline);
        map.put("file server", R.drawable.ic_file_server_offline);
        map.put("proxy server", R.drawable.ic_proxy_server_offline);
        map.put("web server", R.drawable.ic_web_server_offline);
        map.put("domain server", R.drawable.ic_domain_server_offline);
        map.put("communication", R.drawable.ic_communication_offline);
        map.put("database", R.drawable.ic_database_offline);

        // engineering

        map.put("raspberry", R.drawable.ic_raspberry_offline);
        map.put("arduino", R.drawable.ic_arduino_offline);
        map.put("processing", R.drawable.ic_processor_offline);
        map.put("circuit board", R.drawable.ic_circuit_card_offline);
        map.put("rfid tag", R.drawable.ic_rfid_offline);

        return map;
    }

    public DeviceInfo getDeviceInfo() {
        DeviceInfo deviceInfo = null;
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            deviceInfo = dashboardNodeSet.getDeviceInfo();
        }
        if (deviceInfo == null) {
            deviceInfo = new DeviceInfo();
        }
        return deviceInfo;
    }

    // update device info in dashboard info
    // - dashboard info 裡面的 device info 是 router 的, 裡面儲存 timezone 資訊 ("city" 欄位)
    // - 目前下面這個 function 只用在 update timezone
    public void updateDeviceInfo(DeviceInfo deviceInfo) {
        NodeSetInfo dashboardNodeSet = getDashboardNodeSet();
        if (dashboardNodeSet != null) {
            LogUtils.trace("device info: " + deviceInfo);
            dashboardNodeSet.setDeviceInfo(deviceInfo);
            setDashboardNodeSet(dashboardNodeSet);
        }
    }
}
