package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NetworkMap {

    @SerializedName("root")
    private DeviceInfo rootDevice;

    @SerializedName("node_list")
    private ArrayList<DeviceInfo> nodeList;

    @SerializedName("client_list")
    private ArrayList<ClientModel> clientList;

    public DeviceInfo getRootDevice() {
        return rootDevice;
    }

    public void setRootDevice(DeviceInfo rootDevice) {
        this.rootDevice = rootDevice;
    }

    public ArrayList<DeviceInfo> getNodeList() {
        return nodeList;
    }

    public void setNodeList(ArrayList<DeviceInfo> nodeList) {
        this.nodeList = nodeList;
    }

    public ArrayList<ClientModel> getClientList() {
        return clientList;
    }

    public void setClientList(ArrayList<ClientModel> clientList) {
        this.clientList = clientList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
