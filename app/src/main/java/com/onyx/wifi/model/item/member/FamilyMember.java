package com.onyx.wifi.model.item.member;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;

import com.onyx.wifi.model.item.ClientModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class FamilyMember extends Member {

    public enum PauseConfig {
        PAUSE_NONE,
        PAUSE_30_MINS,
        PAUSE_1_HR,
        PAUSE_2_HR,
        PAUSE_4_HR,
        PAUSE_FOREVER
    }

    private MemberModel mMember;

    private String mTemporaryName = "";

    private String mTemporaryPhotoPath;

    private List<Client> mTemporaryClientList;

    public FamilyMember() {
        mMemberType = MemberType.FAMILY;

        mClientList = new ArrayList<>();

        mTemporaryClientList = new ArrayList<>();
    }

    public FamilyMember(MemberModel member) {
        mMember = member;

        mMemberType = MemberType.FAMILY;

        mName = member.getName();

        mTemporaryName = member.getName();

        mClientList = new ArrayList<>();

        mTemporaryClientList = new ArrayList<>();

        for (ClientModel clientModel: mMember.getClientList()) {
            Client client = new Client(clientModel);
            mClientList.add(client);
            mTemporaryClientList.add(client);
        }

        ArrayList<PauseInfoModel> pauseInfoModelList = mMember.getPauseInfoModelList();

        if (pauseInfoModelList != null) {
            mPauseInfoList = new ArrayList<>();
            for (PauseInfoModel pauseInfoModel:pauseInfoModelList) {
                PauseInfo pauseInfo = new PauseInfo(pauseInfoModel);
                mPauseInfoList.add(pauseInfo);
            }
        }
    }

    public MemberModel getMemberModel() {
        return mMember;
    }

    public String getId() {
        return mMember.getMemberId();
    }

    public Bitmap getPhoto(Context context) {
        if (mMember == null) {
            return null;
        }

        Bitmap bitmap = null;

        String memberId = mMember.getMemberId();
        String avatarFileName = memberId + ".jpg";
        File avatarFile = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), avatarFileName);

        if (avatarFile.exists()) {
            String filePath = avatarFile.getAbsolutePath();

            bitmap = decodeSampledBitmapFromResource(filePath, 200, 200);
        }

        return bitmap;
    }

    public Bitmap getTemporaryPhoto() {
        if (mTemporaryPhotoPath == null) {
            return null;
        }

        Bitmap bitmap = null;

        File avatarFile = new File(mTemporaryPhotoPath);

        if (avatarFile.exists()) {
            String filePath = avatarFile.getAbsolutePath();

            bitmap = decodeSampledBitmapFromResource(filePath, 200, 200);
        }

        return bitmap;
    }

    public String getTemporaryName() {
        return mTemporaryName;
    }

    public void setTemporaryName(String name) {
        mTemporaryName = name;
    }

    public String getTemporaryPhotoPath() {
        return mTemporaryPhotoPath;
    }

    public void setTemporaryPhotoPath(String imageFilePath) {
        mTemporaryPhotoPath = imageFilePath;
    }

    public void setClienModeltList(List<ClientModel> clientModelList) {
        mMember.setClientList(clientModelList);
    }

    public List<Client> getTemporaryClientList() {
        return mTemporaryClientList;
    }

    public boolean checkClientListChanged() {
        if (mClientList.size() != mTemporaryClientList.size()) {
            return true;
        }

        Collection<Client> similar = new HashSet<Client>( mClientList );
        Collection<Client> different = new HashSet<Client>();
        different.addAll( mClientList );
        different.addAll( mTemporaryClientList );

        similar.retainAll( mTemporaryClientList );
        different.removeAll( similar );

        return different.size() > 0;
    }

    private List<PauseInfo> mPauseInfoList;

    public List<PauseInfo> getPauseInfoList() {
        return mPauseInfoList;
    }

    private int mPauseDuration = 0;

    public int getPauseDuration() {
        return mPauseDuration;
    }

    public void setPauseDuration(int duration) {
        mPauseDuration = duration;
    }

    public String getPauseDurationString() {
        if (mPauseDuration == 15) {
            return "15m";
        }

        if (mPauseDuration == 30) {
            return "30m";
        }

        if (mPauseDuration == 60) {
            return "1hr";
        }

        if (mPauseDuration == 120) {
            return "2hr";
        }

        if (mPauseDuration == 240) {
            return "4hr";
        }

        if (mPauseDuration == 480) {
            return "8hr";
        }

        if (mPauseDuration == 720) {
            return "12hr";
        }

        if (mPauseDuration == 525600) {
            return "∞";
        }

        return "";
    }

    private Bitmap decodeSampledBitmapFromResource(String filePath, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return getOrientationBitmap(filePath, options);
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public int resolveBitmapOrientation(File bitmapFile) throws IOException {
        ExifInterface exif = null;
        exif = new ExifInterface(bitmapFile.getAbsolutePath());

        return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
    }

    public Bitmap applyOrientation(Bitmap bitmap, int orientation) {
        int rotate = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            default:
                return bitmap;
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        mtx.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public Bitmap getOrientationBitmap(String filePath,BitmapFactory.Options options) {
        Bitmap srcBmp01=BitmapFactory.decodeFile(filePath,options);
        try {
            int orientation = resolveBitmapOrientation(new File(filePath));
            srcBmp01 = applyOrientation(srcBmp01, orientation);
        } catch (Exception ex) {
            ;
        }

        return srcBmp01;
    }

    public Bitmap getOrientationBitmap(String filePath) {
        Bitmap srcBmp01=BitmapFactory.decodeFile(filePath);

        if(srcBmp01!=null) {
            try {
                int orientation = resolveBitmapOrientation(new File(filePath));
                srcBmp01 = applyOrientation(srcBmp01, orientation);
            } catch (Exception ex) {
                ;
            }
        }

        return srcBmp01;
    }
}
