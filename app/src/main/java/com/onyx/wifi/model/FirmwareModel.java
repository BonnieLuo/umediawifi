package com.onyx.wifi.model;

import com.google.gson.JsonObject;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class FirmwareModel {

    // device firmware WIFI API URL
    public static class ApiUrl {
        public static final String SET_USER_ID = "Dev/Info/UID";
        public static final String SET_TIMEZONE = "Dev/Time/TimeZone";
        public static final String SET_PROVISION_TOKEN = "Dev/Provision/Token";
        public static final String GET_DEVICE_BOARD_ID = "Dev/Info/BoardID";
        public static final String SET_DEVICE_OPERATION_MODE = "Dev/OpMode";

        // site survey 是未來給 repeater 連其他家 router 用的, 目前的流程用不到
        public static final String EXEC_AP_SITE_SURVEY = "Dev/WiFi/ApSiteSurvey";
        public static final String GET_AP_LIST = "Dev/WiFi/ApList";

        public static final String SET_2G_AP_CLIENT_WIFI = "Dev/WiFi0/Station";
        public static final String SET_2G_AP_CLIENT_WIFI_KEY = "Dev/WiFi0/StationKey";
        public static final String SET_2G_AP_CLIENT_WIFI_SEC = "Dev/WiFi0/StationSecurity";
        public static final String SET_5G_AP_CLIENT_WIFI = "Dev/WiFi1/Station";
        public static final String SET_5G_AP_CLIENT_WIFI_KEY = "Dev/WiFi1/StationKey";
        public static final String SET_5G_AP_CLIENT_WIFI_SEC = "Dev/WiFi1/StationSecurity";
        public static final String SET_AP_CLIENT_WIFI_ACTION = "Dev/WiFi/StationCommit";
        public static final String GET_2G_AP_CLIENT_CONN_STATUS = "Dev/WiFi0/Station/Connected";
        public static final String GET_5G_AP_CLIENT_CONN_STATUS = "Dev/WiFi1/Station/Connected";
        public static final String DETECT_WAN_TYPE = "Dev/Wan/TypeDetect";
        public static final String GET_WAN_TYPE = "Dev/Wan/TypeDetect";
        public static final String SET_WAN_DHCP = "Dev/Wan/Dhcp";
        public static final String SET_WAN_PPPOE = "Dev/Wan/PPPoE";
        public static final String SET_WAN_STATIC_IP = "Dev/Wan/Static";
        public static final String GET_INTERNET_CONN_STATUS = "Dev/Wan/Connected";
        public static final String GET_DEVICE_TOKEN_STATUS = "Dev/WiFi/DeviceToken";
        public static final String SET_LOCATION = "Dev/Mgnt/Location";
        public static final String SET_2G_WIFI = "Dev/WiFi0/Ap";
        public static final String SET_2G_WIFI_KEY = "Dev/WiFi0/ApKey";
        public static final String SET_2G_WIFI_SEC = "Dev/WiFi0/ApSecurity";
        public static final String SET_5G_WIFI = "Dev/WiFi1/Ap";
        public static final String SET_5G_WIFI_KEY = "Dev/WiFi1/ApKey";
        public static final String SET_5G_WIFI_SEC = "Dev/WiFi1/ApSecurity";
        public static final String SETUP_COMPLETE = "Dev/Setup/Complete";
        public static final String SET_BLE_ENABLE = "Dev/Ble/Enable";
        public static final String GET_DEVICE_MAC = "Dev/Info/MAC";
        public static final String SET_ADMIN_PASSWORD = "Dev/Mgnt/AdminPwd";
    }

    public static class BleRequestCommand {
        public static final String SET_USER_ID = AC_PUT + ApiUrl.SET_USER_ID;
        public static final String SET_TIMEZONE = AC_PUT + ApiUrl.SET_TIMEZONE;
        public static final String SET_PROVISION_TOKEN = AC_PUT + ApiUrl.SET_PROVISION_TOKEN;
        public static final String GET_DEVICE_BOARD_ID = AC_GET + ApiUrl.GET_DEVICE_BOARD_ID;
        public static final String SET_DEVICE_OPERATION_MODE = AC_PUT + "Dev/Info/OpMode";
        public static final String EXEC_AP_SITE_SURVEY = AC_PUT + ApiUrl.EXEC_AP_SITE_SURVEY;
        public static final String GET_AP_LIST = AC_GET + ApiUrl.GET_AP_LIST;
        public static final String SET_2G_AP_CLIENT_WIFI = AC_PUT + ApiUrl.SET_2G_AP_CLIENT_WIFI;
        public static final String SET_2G_AP_CLIENT_WIFI_KEY = AC_PUT + ApiUrl.SET_2G_AP_CLIENT_WIFI_KEY;
        public static final String SET_2G_AP_CLIENT_WIFI_SEC = AC_PUT + ApiUrl.SET_2G_AP_CLIENT_WIFI_SEC;
        public static final String SET_5G_AP_CLIENT_WIFI = AC_PUT + ApiUrl.SET_5G_AP_CLIENT_WIFI;
        public static final String SET_5G_AP_CLIENT_WIFI_KEY = AC_PUT + ApiUrl.SET_5G_AP_CLIENT_WIFI_KEY;
        public static final String SET_5G_AP_CLIENT_WIFI_SEC = AC_PUT + ApiUrl.SET_5G_AP_CLIENT_WIFI_SEC;
        public static final String SET_AP_CLIENT_WIFI_ACTION = AC_PUT + ApiUrl.SET_AP_CLIENT_WIFI_ACTION;
        public static final String GET_2G_AP_CLIENT_CONN_STATUS = AC_GET + ApiUrl.GET_2G_AP_CLIENT_CONN_STATUS;
        public static final String GET_5G_AP_CLIENT_CONN_STATUS = AC_GET + ApiUrl.GET_5G_AP_CLIENT_CONN_STATUS;
        public static final String DETECT_WAN_TYPE = AC_PUT + ApiUrl.DETECT_WAN_TYPE;
        public static final String GET_WAN_TYPE = AC_GET + ApiUrl.GET_WAN_TYPE;
        public static final String SET_WAN_DHCP = AC_PUT + ApiUrl.SET_WAN_DHCP;
        public static final String SET_WAN_PPPOE = AC_PUT + ApiUrl.SET_WAN_PPPOE;
        public static final String SET_WAN_STATIC_IP = AC_PUT + ApiUrl.SET_WAN_STATIC_IP;
        public static final String GET_INTERNET_CONN_STATUS = AC_GET + ApiUrl.GET_INTERNET_CONN_STATUS;
        public static final String GET_DEVICE_TOKEN_STATUS = AC_GET + ApiUrl.GET_DEVICE_TOKEN_STATUS;
        public static final String SET_LOCATION = AC_PUT + ApiUrl.SET_LOCATION;
        public static final String SET_2G_WIFI = AC_PUT + ApiUrl.SET_2G_WIFI;
        public static final String SET_2G_WIFI_KEY = AC_PUT + ApiUrl.SET_2G_WIFI_KEY;
        public static final String SET_2G_WIFI_SEC = AC_PUT + ApiUrl.SET_2G_WIFI_SEC;
        public static final String SET_5G_WIFI = AC_PUT + ApiUrl.SET_5G_WIFI;
        public static final String SET_5G_WIFI_KEY = AC_PUT + ApiUrl.SET_5G_WIFI_KEY;
        public static final String SET_5G_WIFI_SEC = AC_PUT + ApiUrl.SET_5G_WIFI_SEC;
        public static final String SETUP_COMPLETE = AC_PUT + ApiUrl.SETUP_COMPLETE;
        public static final String SET_BLE_ENABLE = AC_PUT + ApiUrl.SET_BLE_ENABLE;
        public static final String GET_DEVICE_MAC = AC_GET + ApiUrl.GET_DEVICE_MAC;
        public static final String SET_ADMIN_PASSWORD = AC_PUT + ApiUrl.SET_ADMIN_PASSWORD;
    }

    public static class BleResponseCommand {
        public static final String SET_USER_ID = PUT + ApiUrl.SET_USER_ID;
        public static final String SET_TIMEZONE = PUT + ApiUrl.SET_TIMEZONE;
        public static final String SET_PROVISION_TOKEN = PUT + ApiUrl.SET_PROVISION_TOKEN;
        public static final String GET_DEVICE_BOARD_ID = GET + ApiUrl.GET_DEVICE_BOARD_ID;
        public static final String SET_DEVICE_OPERATION_MODE = PUT + ApiUrl.SET_DEVICE_OPERATION_MODE;
        public static final String EXEC_AP_SITE_SURVEY = PUT + ApiUrl.EXEC_AP_SITE_SURVEY;
        public static final String GET_AP_LIST = GET + ApiUrl.GET_AP_LIST;
        public static final String SET_2G_AP_CLIENT_WIFI = PUT + ApiUrl.SET_2G_AP_CLIENT_WIFI;
        public static final String SET_2G_AP_CLIENT_WIFI_KEY = PUT + ApiUrl.SET_2G_AP_CLIENT_WIFI_KEY;
        public static final String SET_2G_AP_CLIENT_WIFI_SEC = PUT + ApiUrl.SET_2G_AP_CLIENT_WIFI_SEC;
        public static final String SET_5G_AP_CLIENT_WIFI = PUT + ApiUrl.SET_5G_AP_CLIENT_WIFI;
        public static final String SET_5G_AP_CLIENT_WIFI_KEY = PUT + ApiUrl.SET_5G_AP_CLIENT_WIFI_KEY;
        public static final String SET_5G_AP_CLIENT_WIFI_SEC = PUT + ApiUrl.SET_5G_AP_CLIENT_WIFI_SEC;
        public static final String SET_AP_CLIENT_WIFI_ACTION = PUT + ApiUrl.SET_AP_CLIENT_WIFI_ACTION;
        public static final String GET_2G_AP_CLIENT_CONN_STATUS = GET + ApiUrl.GET_2G_AP_CLIENT_CONN_STATUS;
        public static final String GET_5G_AP_CLIENT_CONN_STATUS = GET + ApiUrl.GET_5G_AP_CLIENT_CONN_STATUS;
        public static final String DETECT_WAN_TYPE = PUT + ApiUrl.DETECT_WAN_TYPE;
        public static final String GET_WAN_TYPE = GET + ApiUrl.GET_WAN_TYPE;
        public static final String SET_WAN_DHCP = PUT + ApiUrl.SET_WAN_DHCP;
        public static final String SET_WAN_PPPOE = PUT + ApiUrl.SET_WAN_PPPOE;
        public static final String SET_WAN_STATIC_IP = PUT + ApiUrl.SET_WAN_STATIC_IP;
        public static final String GET_INTERNET_CONN_STATUS = GET + ApiUrl.GET_INTERNET_CONN_STATUS;
        public static final String GET_DEVICE_TOKEN_STATUS = GET + ApiUrl.GET_DEVICE_TOKEN_STATUS;
        public static final String SET_LOCATION = PUT + ApiUrl.SET_LOCATION;
        public static final String SET_2G_WIFI = PUT + ApiUrl.SET_2G_WIFI;
        public static final String SET_2G_WIFI_KEY = PUT + ApiUrl.SET_2G_WIFI_KEY;
        public static final String SET_2G_WIFI_SEC = PUT + ApiUrl.SET_2G_WIFI_SEC;
        public static final String SET_5G_WIFI = PUT + ApiUrl.SET_5G_WIFI;
        public static final String SET_5G_WIFI_KEY = PUT + ApiUrl.SET_5G_WIFI_KEY;
        public static final String SET_5G_WIFI_SEC = PUT + ApiUrl.SET_5G_WIFI_SEC;
        public static final String SETUP_COMPLETE = PUT + "Dev/WiFi/setupComplete";
        public static final String SET_BLE_ENABLE = PUT + ApiUrl.SET_BLE_ENABLE;
        public static final String GET_DEVICE_MAC = GET + ApiUrl.GET_DEVICE_MAC;
        public static final String SET_ADMIN_PASSWORD = PUT + ApiUrl.SET_ADMIN_PASSWORD;
    }

    private static final String AC = "AC ";
    private static final String PUT = "Put ";
    private static final String GET = "Get ";
    private static final String AC_GET = AC + GET;
    private static final String AC_PUT = AC + PUT;

    public FirmwareModel() {
        // do nothing
    }

    public byte[] getCommand(String requestCommand, JsonObject requestData) {
        String command = requestCommand + " " + requestData.toString();
        return getBleByteArray(command);
    }

    private byte[] getBleByteArray(String input) {
        byte bleEndChar = 0x0d;
        byte[] srcByteArray = input.getBytes(StandardCharsets.UTF_8);
        byte[] destByteArray = Arrays.copyOf(srcByteArray, srcByteArray.length + 1);
        destByteArray[srcByteArray.length] = bleEndChar;
        return destByteArray;
    }
}
