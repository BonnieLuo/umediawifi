package com.onyx.wifi.model;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;

import com.onyx.wifi.MainApp;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class BleControlModel {

    private static BleControlModel mInstance = null;

    private static final String UUID_UMEDIA_SETUP_SERVICE = "0000FFF0-0000-1000-8000-00805F9B34FB";
    private static final String UUID_UMEDIA_SETUP_FUNCTION_ONE = "0000FFF1-0000-1000-8000-00805F9B34FB";
    private static final String UUID_CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR = "00002902-0000-1000-8000-00805F9B34FB";

    private Context mContext;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothGattCharacteristic mBluetoothGattCharacteristic;
    private BluetoothGattDescriptor mBluetoothGattDescriptor;

    public static BleControlModel getInstance() {
        if (mInstance == null) {
            mInstance = new BleControlModel(MainApp.getInstance());
        }

        return mInstance;
    }

    private BleControlModel(Context context) {
        mContext = context;
    }

    public void connectDevice(BluetoothDevice bluetoothDevice) {
        LogUtils.trace("device bond state: " + bluetoothDevice.getBondState());
        mBluetoothGatt = bluetoothDevice.connectGatt(mContext, false, mBluetoothGattCallback);
    }

    public void disconnectDevice() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.disconnect();
            mBluetoothGatt.close();
        }
    }

    private BluetoothGattCallback mBluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt.discoverServices();
                LogUtils.trace("Connected to GATT server. Attempting to start service discovery.");
                Intent intent = new Intent(AppConstants.INTENT_BLE_ACTION_GATT_CONNECTED);
                mContext.sendBroadcast(intent);
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                gatt.close();
                LogUtils.trace("Disconnected from GATT server.");
                Intent intent = new Intent(AppConstants.INTENT_BLE_ACTION_GATT_DISCONNECTED);
                mContext.sendBroadcast(intent);
            }
        }

        @Override
        // New services discovered
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            boolean isDiscovered = false;
            if (status == BluetoothGatt.GATT_SUCCESS) {
                List<BluetoothGattService> gattServices = gatt.getServices();
                for (BluetoothGattService service : gattServices) {
                    String serviceUuid = service.getUuid().toString().toUpperCase();
                    LogUtils.trace("service uuid: " + serviceUuid);
                    // find service
                    if (serviceUuid.equals(UUID_UMEDIA_SETUP_SERVICE)) {
                        List<BluetoothGattCharacteristic> gattCharacteristics = service.getCharacteristics();
                        for (BluetoothGattCharacteristic characteristic : gattCharacteristics) {
                            String characteristicUuid = characteristic.getUuid().toString().toUpperCase();
                            LogUtils.trace("characteristic uuid: " + characteristicUuid);
                            // find characteristic
                            if (characteristicUuid.equals(UUID_UMEDIA_SETUP_FUNCTION_ONE)) {
                                mBluetoothGattCharacteristic = characteristic;
                                enableNotify(characteristic);
                                isDiscovered = true;
                                break;
                            }
                        }
                    }
                }
            } else {
                LogUtils.trace("onServicesDiscovered status: " + status);
            }
            Intent intent = new Intent(AppConstants.INTENT_BLE_ACTION_GATT_DISCOVERED);
            intent.putExtra(AppConstants.EXTRA_IS_DISCOVERED, isDiscovered);
            mContext.sendBroadcast(intent);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            LogUtils.trace("char uuid: " + characteristic.getUuid() + ", status: " + status + ", value: " + new String(characteristic.getValue(), StandardCharsets.UTF_8));
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (characteristic.getUuid().toString().toUpperCase().equals(UUID_UMEDIA_SETUP_FUNCTION_ONE)) {
                LogUtils.trace("value: " + new String(characteristic.getValue(), StandardCharsets.UTF_8));
            } else {
                LogUtils.trace("unknown characteristic: " + characteristic.getUuid());
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            if (characteristic.getUuid().toString().toUpperCase().equals(UUID_UMEDIA_SETUP_FUNCTION_ONE)) {
                LogUtils.trace("value: " + new String(characteristic.getValue(), StandardCharsets.UTF_8));
                Intent intent = new Intent(AppConstants.INTENT_BLE_ACTION_DATA_CHANGE);
                intent.putExtra(AppConstants.EXTRA_BLE_DATA, characteristic.getValue());
                mContext.sendBroadcast(intent);
            } else {
                LogUtils.trace("unknown characteristic: " + characteristic.getUuid());
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            LogUtils.trace("descriptor uuid: " + descriptor.getUuid() + ", status: " + status + ", value: " + new String(descriptor.getValue(), StandardCharsets.UTF_8));
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            boolean isEnable = false;
            String uuid = descriptor.getUuid().toString().toUpperCase();
            LogUtils.trace("descriptor uuid: " + uuid);
            if (uuid.equals(UUID_CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR)) {
                isEnable = Arrays.equals(descriptor.getValue(), BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            }
            Intent intent = new Intent(AppConstants.INTENT_BLE_ACTION_GATT_NOTIFY);
            intent.putExtra(AppConstants.EXTRA_IS_ENABLE, isEnable);
            mContext.sendBroadcast(intent);
        }
    };

    private void enableNotify(BluetoothGattCharacteristic gattCharacteristic) {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.setCharacteristicNotification(gattCharacteristic, true);
            mBluetoothGattDescriptor = gattCharacteristic.getDescriptor(UUID.fromString(UUID_CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR));
            mBluetoothGattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(mBluetoothGattDescriptor);
        }
    }

    public void writePayload(byte[] payload) {
        if (mBluetoothGatt != null && mBluetoothGattCharacteristic != null) {
            mBluetoothGattCharacteristic.setValue(payload);
            mBluetoothGatt.writeCharacteristic(mBluetoothGattCharacteristic);
        }
    }
}
