package com.onyx.wifi.model.item.advancewireless.dhcp;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.StringUtils;

public class DhcpClientModel {
    @SerializedName("client_type")
    private String mType;

    @SerializedName("client_name")
    private String mName;

    @SerializedName("ip")
    private String mIp;

    @SerializedName("mac")
    private String mMac;

    @SerializedName("lease_time")
    private String mLeastTime;

    @SerializedName("connection_type")
    private String mConnectionType;

    @SerializedName("status")
    private int mStatus;

    @SerializedName("online")
    private String mOnline;

    @SerializedName("disallowed")
    private String mDisallowed;

    public String getType() {
        return mType;
    }

    public String getName() {
        return StringUtils.decodeHexString(mName);
    }

    public String getIp() {
        return mIp;
    }

    public String getMac() {
        return mMac;
    }

    public String getLeastTime() {
        return mLeastTime;
    }

    public String getConnectionType() {
        return mConnectionType;
    }

    public int getStatus() {
        return mStatus;
    }

    public ConnectionStatus getOnlineStatus() {
        return ConnectionStatus.fromString(mOnline);
    }

    public boolean getDisallowed() {
        if (("1").equals(mDisallowed))
            return true;
        else
            return false;
    }

    public void setDisallowed(boolean isDisallowed) {
        if (isDisallowed)
            mDisallowed = "1";
        else
            mDisallowed = "0";
    }
}
