package com.onyx.wifi.model.item.member;

import com.google.gson.JsonObject;

public class ScheduleFrequency {

    public ScheduleFrequency(boolean[] config) {
        mConfig = config;

        mSunday = mConfig[0];
        mMonday = mConfig[1];
        mTuesday = mConfig[2];
        mWednesday = mConfig[3];
        mThursday = mConfig[4];
        mFriday = mConfig[5];
        mSaturday = mConfig[6];
    }

    private boolean[] mConfig;

    private boolean mSunday;

    public boolean isSundayEnable() {
        return mSunday;
    }

    public void toggleSunday() {
        mSunday = !mSunday;
    }

    private boolean mMonday;

    public boolean isMondayEnable() {
        return mMonday;
    }

    public void toggleMonday() {
        mMonday = !mMonday;
    }

    private boolean mTuesday;

    public boolean isTuesdayEnable() {
        return mTuesday;
    }

    public void toggleTuesday() {
        mTuesday = !mTuesday;
    }

    private boolean mWednesday;

    public boolean isWednesdayEnable() {
        return mWednesday;
    }

    public void toggleWednesday() {
        mWednesday = !mWednesday;
    }

    private boolean mThursday;

    public boolean isThursdayEnable() {
        return mThursday;
    }

    public void toggleThursday() {
        mThursday = !mThursday;
    }

    private boolean mFriday;

    public boolean isFridayEnable() {
        return mFriday;
    }

    public void toggleFriday() {
        mFriday = !mFriday;
    }

    private boolean mSaturday;

    public boolean isSaturdayEnable() {
        return mSaturday;
    }

    public void toggleSaturday() {
        mSaturday = !mSaturday;
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("sun", toString(mSunday));
        jsonObject.addProperty("mon", toString(mMonday));
        jsonObject.addProperty("tue", toString(mTuesday));
        jsonObject.addProperty("wed", toString(mWednesday));
        jsonObject.addProperty("thu", toString(mThursday));
        jsonObject.addProperty("fri", toString(mFriday));
        jsonObject.addProperty("sat", toString(mSaturday));

        return jsonObject;
    }

    private String toString(boolean config) {
        if (config) {
            return "1";
        }

        return "0";
    }

    public boolean checkDiff() {
        return mSunday != mConfig[0] ||
                mMonday != mConfig[1] ||
                mTuesday != mConfig[2] ||
                mWednesday != mConfig[3] ||
                mThursday != mConfig[4] ||
                mFriday != mConfig[5] ||
                mSaturday != mConfig[6];
    }

    public boolean isEmpty() {
        return !mSunday &&
                !mMonday &&
                !mTuesday &&
                !mWednesday &&
                !mThursday &&
                !mFriday &&
                !mSaturday;
    }
}

