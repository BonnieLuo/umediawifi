package com.onyx.wifi.model.item.advancewireless.dhcp;

import com.onyx.wifi.R;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.DateUtils;

public class DhcpClient {

    private DhcpClientModel mClientModel;

    public DhcpClient(DhcpClientModel model) {
        mClientModel = model;
    }

    public String getType() {
        return mClientModel.getType();
    }

    public String getName() {
        return mClientModel.getName();
    }

    public String getIp() {
        String ip = mClientModel.getIp();

        if (ip == null) {
            return "";
        }

        return ip;
    }

    public String getMac() {
        return mClientModel.getMac();
    }

    public String getLeastTime() {
        /*String modelLeastTime = mClientModel.getLeastTime();

        if (modelLeastTime == null) {
            return "0d:0h:0m";
        }

//        long leastTimestamp = Long.parseLong(modelLeastTime);

        return "0d:0h:0m";*/
        String leaseTime = mClientModel.getLeastTime();
        if (leaseTime != null) {
            int leaseTimeInSeconds = Integer.parseInt(leaseTime);
            return DateUtils.secToClientLeaseTimeFormat(leaseTimeInSeconds);
        } else {
            return "";
        }
    }

    public int getConnectionType() {
        String connectionType = mClientModel.getConnectionType();
        if (connectionType != null && connectionType.equalsIgnoreCase("Ethernet")) {
            return getEthernetIcon();
        }

        return getWiFiIcon();
    }

    public ConnectionStatus getOnlineStatus() {
//        int status = mClientModel.getStatus();
//        return ConnectionStatus.fromOrdinal(status);
        return mClientModel.getOnlineStatus();
    }

    public int getConnectionLight() {
        ConnectionStatus status = getOnlineStatus();

        if (status == ConnectionStatus.OFFLINE) {
            return R.drawable.ic_internet_status_offline_gray;
        } else {
            return R.drawable.ic_internet_status_online;
        }
    }

    private int getWiFiIcon() {
        ConnectionStatus status = getOnlineStatus();
        if (status == ConnectionStatus.OFFLINE) {
            return R.drawable.ic_generic_wifi_offline;
        }

        return R.drawable.ic_generic_wifi;
    }

    private int getEthernetIcon() {
        ConnectionStatus status = getOnlineStatus();
        if (status == ConnectionStatus.OFFLINE) {
            return R.drawable.ic_generic_lan_offline;
        }

        return R.drawable.ic_generic_lan;
    }
}
