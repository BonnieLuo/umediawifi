package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class NodeSetInfo {

    @SerializedName("name")
    private String name = "";

    @SerializedName("nodeset_id")
    private String nodeSetId = "";

    @SerializedName("controllerOnline")
    private boolean controllerOnline = false;

    @SerializedName("networkSetting")
    private NetworkSetting networkSetting = new NetworkSetting();

    @SerializedName("clientListCount")
    private int clientListCount = 0;

    // controller(router) 的 device info
    @SerializedName("deviceInfo")
    private DeviceInfo deviceInfo = new DeviceInfo();

    @SerializedName("pauseInfo")
    private Object pauseInfo = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNodeSetId() {
        return nodeSetId;
    }

    public void setNodeSetId(String nodeSetId) {
        this.nodeSetId = nodeSetId;
    }

    public boolean getControllerOnline() {
        return controllerOnline;
    }

    public void setControllerOnline(boolean controllerOnline) {
        this.controllerOnline = controllerOnline;
    }

    public NetworkSetting getNetworkSetting() {
        return networkSetting;
    }

    public void setNetworkSetting(NetworkSetting networkSetting) {
        this.networkSetting = networkSetting;
    }

    public int getClientListCount() {
        return clientListCount;
    }

    public void setClientListCount(int clientListCount) {
        this.clientListCount = clientListCount;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public Object getPauseInfo() {
        return pauseInfo;
    }

    public void setPauseInfo(Object pauseInfo) {
        this.pauseInfo = pauseInfo;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
