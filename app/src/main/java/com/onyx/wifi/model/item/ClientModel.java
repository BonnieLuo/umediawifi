package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.model.item.member.PauseInfoModel;
import com.onyx.wifi.utility.StringUtils;

import java.util.ArrayList;

public class ClientModel {

    /*
    {
        "client_id": null,
        "mac": "FC:77:74:22:8B:2A",
        "cid": "5e1d97d82b93b317bc356339",
        "ip": "192.168.8.118",
        "status": 0,
        "online": "1",
        "client_type": "MOBILE",
        "connection_type": "Wireless",
        "network_type": "01",
        "last_online_timestamp": 1579672465408,
        "first_online_timestamp": 0,
        "disallowed_timestamp": 0,
        "from_node_did": "5e1e90d38e4dfe5655fc631d",
        "owner_member_id": "5e2125b2860ea000014d9536",
        "internet_priority": "0",
        "pause_info": [],
        "client_name": "4E6574776F726B5F446576696365"

    }
    */

    @SerializedName("client_id")
    private String mClientId;

    @SerializedName("cid")
    private String mCid;

    @SerializedName("client_name")
    private String mName;

    @SerializedName("ip")
    private String mIp;

    @SerializedName("online")
    private String mOnline = "0";

    @SerializedName("internet_priority")
    private String internetPriority = "0";

    @SerializedName("client_type")
    private String mClientType;

    @SerializedName("connection_type")
    private String mConnectionType = "Wireless";

    @SerializedName("last_online_timestamp")
    private long mLastOnlineTimestamp = -1;

    @SerializedName("first_online_timestamp")
    private long mFirstOnlineTimestamp = -1;

    @SerializedName("disallowed")
    private String mDisallowed;

    @SerializedName("disallowed_timestamp")
    private long mDisallowedTimestamp = -1;

    @SerializedName("outage_monitor")
    private String mDeviceOutageMonitoring = "0";

    @SerializedName("from_node_did")
    private String mFromNodeDid;

    @SerializedName("owner_member_id")
    private String mOwnerMemberId;

    @SerializedName("pause_info")
    private ArrayList<PauseInfoModel> mPauseInfoModel;

    public String getClientID() {
        return mClientId;
    }

    public String getCid() {
        return mCid;
    }

    public void setName(String name) {
        this.mName = StringUtils.encodeToHexString(name);
    }

    public String getName() {
        return StringUtils.decodeHexString(mName);
    }

    public String getIp() {
        return mIp;
    }

    public ConnectionStatus getOnlineStatus() {
        return ConnectionStatus.fromString(mOnline);
    }

    // Internet priority
    public void setInternetPriority(String internetPriority) {
        this.internetPriority = internetPriority;
    }

    public String getInternetPriority() {
        return internetPriority;
    }

    public String getClientType() {
        return mClientType;
    }

    public void setClientType(String clientType) {
        mClientType = clientType;
    }

    public String getConnectionType() {
        // [error handling]
        if (StringUtils.isNullOrEmpty(mConnectionType)) {
            mConnectionType = "Wireless";
        } else if (!(mConnectionType.equals("Ethernet") || mConnectionType.equals("Wireless"))) {
            mConnectionType = "Wireless";
        }
        return mConnectionType;
    }

    public long getLastOnlineTimestamp() {
        return mLastOnlineTimestamp;
    }

    public long getFirstOnlineTimestamp() {
        return mFirstOnlineTimestamp;
    }

    public boolean getDisallowed() {
        if (("1").equals(mDisallowed))
            return true;
        else
            return false;
    }

    public void setDisallowed(boolean isDisallowed) {
        if (isDisallowed)
            mDisallowed = "1";
        else
            mDisallowed = "0";
    }

    public long getDisallowedTimestamp() {
        return mDisallowedTimestamp;
    }

    public String getDeviceOutageMonitoring() {
        if (mDeviceOutageMonitoring == null)
            return "0";
        else
            return mDeviceOutageMonitoring;
    }

    public void setmDeviceOutageMonitoring(String outageMonitoring) {
        mDeviceOutageMonitoring = outageMonitoring;
    }

    public String getFromNodeDid() {
        return mFromNodeDid;
    }

    public String getOwnerMemberId() {
        return mOwnerMemberId;
    }

    public ArrayList<PauseInfoModel> getPauseInfo() {
        return mPauseInfoModel;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
