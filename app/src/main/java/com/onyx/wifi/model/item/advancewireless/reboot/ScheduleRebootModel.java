package com.onyx.wifi.model.item.advancewireless.reboot;

import com.google.gson.annotations.SerializedName;

public class ScheduleRebootModel {

    @SerializedName("enable")
    private String mEnable;

    @SerializedName("time")
    private String mTime;

    @SerializedName("week")
    private String mWeek;

    public String getEnable() {
        return mEnable;
    }

    public String getTime() {
        if (mTime == null)
            return "";
        else
            return mTime;
    }

    public String getWeek() {
        if (mWeek == null)
            return "";
        else
            return mWeek;
    }

}
