package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.utility.StringUtils;

public class BnbNetworkSetting {

    @SerializedName("enable")
    private boolean enable = false;

    @SerializedName("ssid")
    private String ssid = "";

    @SerializedName("pwd")
    private String pwd = "";

    @SerializedName("start")
    private int start = 0;  // start time, Unix timestamp, seconds

    @SerializedName("end")
    private int end = 0;    // end time, Unix timestamp, seconds

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getSsid() {
        return StringUtils.decodeHexString(ssid);
    }

    public void setSsid(String ssid) {
        this.ssid = StringUtils.encodeToHexString(ssid);
    }

    public String getPwd() {
        return StringUtils.decodeHexString(pwd);
    }

    public void setPwd(String pwd) {
        this.pwd = StringUtils.encodeToHexString(pwd);
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
