package com.onyx.wifi.model.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class WirelessSetting {

    @SerializedName("wifi_same")
    private String wifiSameEnabled = "1";

    @SerializedName("client_online_notification")
    private String clientOnlineNotification = "0";

    @SerializedName("2G")
    private WirelessInfo2g wireless2g = new WirelessInfo2g();

    @SerializedName("5G")
    private WirelessInfo5g wireless5g = new WirelessInfo5g();

    @SerializedName("5G2")
    private WirelessInfo5g wireless5g2 = new WirelessInfo5g();

    public boolean getWifiSameEnabled() {
        return wifiSameEnabled.equals("1");
    }

    public void setWifiSameEnabled(boolean wifiSameEnabled) {
        this.wifiSameEnabled = (wifiSameEnabled) ? "1" : "0";
    }

    public boolean getClientOnlineNotification() {
        return clientOnlineNotification.equals("1");
    }

    public void setClientOnlineNotification(boolean clientOnlineNotification) {
        this.clientOnlineNotification = (clientOnlineNotification) ? "1" : "0";
    }

    public WirelessInfo2g getWireless2g() {
        return wireless2g;
    }

    public void setWireless2g(WirelessInfo2g wireless2g) {
        this.wireless2g = wireless2g;
    }

    public WirelessInfo5g getWireless5g() {
        return wireless5g;
    }

    public void setWireless5g(WirelessInfo5g wireless5g) {
        this.wireless5g = wireless5g;
    }

    public WirelessInfo5g getWireless5g2() {
        return wireless5g2;
    }

    public void setWireless5g2(WirelessInfo5g wireless5g2) {
        this.wireless5g2 = wireless5g2;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
