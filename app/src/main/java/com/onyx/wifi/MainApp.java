package com.onyx.wifi;

import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Intent;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.cloud.CloudManager;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.AccountManager;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.model.item.WirelessSetting;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.model.item.member.PauseInfoModel;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.List;

public class MainApp extends Application {

    private static MainApp mInstance;

    private AppLifecycleObserver mAppLifecycleObserver;

    public static MainApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        DataManager dataManager = DataManager.getInstance();
        AppConstants.CloudServerMode cloudServerMode = dataManager.getCloudServerMode();
        if (cloudServerMode == AppConstants.CloudServerMode.NONE) {
            // 判斷 share preference 是否有儲存 cloud server mode, 若沒有的話, 就依據 product flavor 設定 cloud server mode
            if (BuildConfig.IS_RDQA) { // product flavor: rdqa
                if (BuildConfig.DEBUG) {
                    // cloud 的 source code 最先上到 test domain, App 開發及整合測試使用 test domain
                    dataManager.setCloudServerMode(AppConstants.CloudServerMode.TEST);
                } else {
                    // cloud 給 RDQA 測試的版本會上到 rdqa domain, App 發佈給 RDQA 測試的版本也要使用 rdqa domain
                    dataManager.setCloudServerMode(AppConstants.CloudServerMode.RDQA);
                }
            } else { // product flavor: production
                // cloud server 使用 production domain, build type 可分為 debug or release
                // (1) 要正式上架的版本 or 提供給客戶測試的版本, 都是要 release build
                // (2) App 開發過程中若需要在 production domain 測試及 debug, 就要用 debug build, 才看得到 log
                dataManager.setCloudServerMode(AppConstants.CloudServerMode.PRODUCTION);
            }
            CloudManager.getInstance().initialCloudApiBaseUrl();
        }

        if (mAppLifecycleObserver == null) {
            mAppLifecycleObserver = new AppLifecycleObserver();
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(mAppLifecycleObserver);

        final Thread.UncaughtExceptionHandler originalHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                StringWriter writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                LogUtils.traceOnlyFile(writer.toString());

                originalHandler.uncaughtException(t, e);
            }
        });
    }

    class AppLifecycleObserver implements LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        void onEnterForeground() {
            LogUtils.trace("App enter foreground");
            if (AccountManager.getInstance().getUser() != null && DataManager.getInstance().getUserInfo() != null) {
                // App 回到前景時打 cloud API 取得 Dashboard 的最新資料, 更新到 share preference
                getDashboardInfo();
                getWirelessSetting();
                getSpeedTest();
                getGlobalPauseList();
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        void onEnterBackground() {
            LogUtils.trace("App enter background");
        }

        private void getDashboardInfo() {
            CloudManager.getInstance().getDashboardInfo(new ApiCallback() {
                @Override
                public void onInternetUnavailable() {

                }

                @Override
                public void onSendRequest() {

                }

                @Override
                public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                    if (apiResponse.httpStatusCode == 200) {
                        DataManager.getInstance().setDashboardInfo((DashboardInfo) apiResponse.body);
                        sendBroadcast(new Intent(AppConstants.EVENT_DASHBOARD_INFO_UPDATE));
                    }
                }
            });
        }

        private void getWirelessSetting() {
            CloudManager.getInstance().getWirelessSetting(new ApiCallback() {
                @Override
                public void onInternetUnavailable() {

                }

                @Override
                public void onSendRequest() {

                }

                @Override
                public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                    if (apiResponse.httpStatusCode == 200) {
                        JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                        JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, AppConstants.KEY_DATA);
                        JsonObject settingsJson = jsonParserWrapper.jsonGetJsonObject(dataJson, "settings");
                        WirelessSetting wirelessSetting = jsonParserWrapper.jsonToObject(settingsJson, WirelessSetting.class);
                        DataManager.getInstance().setWirelessSetting(wirelessSetting);
                        sendBroadcast(new Intent(AppConstants.EVENT_WIRELESS_SETTING_UPDATE));
                    }
                }
            });
        }

        private void getSpeedTest() {
            CloudManager.getInstance().getSpeedTest(new ApiCallback() {
                @Override
                public void onInternetUnavailable() {

                }

                @Override
                public void onSendRequest() {

                }

                @Override
                public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                    if (apiResponse.httpStatusCode == 200) {
                        JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                        JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                        SpeedTest speedTest = jsonParserWrapper.jsonToObject(dataJson, SpeedTest.class);
                        DataManager.getInstance().updateSpeedTest(DataManager.getInstance().getControllerDid(), speedTest);
                        sendBroadcast(new Intent(AppConstants.EVENT_SPEED_TEST_UPDATE));
                    }
                }
            });
        }

        private void getGlobalPauseList() {
            CloudManager.getInstance().getGlobalPauseList(new ApiCallback() {
                @Override
                public void onInternetUnavailable() {

                }

                @Override
                public void onSendRequest() {

                }

                @Override
                public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                    if (apiResponse.httpStatusCode == 200) {
                        JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                        JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                        JsonArray pauseStatusArray = jsonParserWrapper.jsonGetJsonArray(dataJson, "pause_status");

                        if (pauseStatusArray != null) {
                            Type pauseInfoListType = new TypeToken<List<PauseInfoModel>>() {
                            }.getType();
                            List<PauseInfoModel> pauseInfoList = new JsonParserWrapper().jsonToObject(pauseStatusArray.toString(), pauseInfoListType);
                            boolean find = false;
                            for (PauseInfoModel pauseInfoModel : pauseInfoList) {
                                PauseInfo pauseInfo = new PauseInfo(pauseInfoModel);
                                String type = pauseInfo.getType();
                                boolean isEnabled = pauseInfo.isEnabled();
                                int minuteLeft = pauseInfo.getMinuteLeft();
                                // type = 1 是 global pause
                                if ("1".equals(type) && isEnabled && minuteLeft > 0) {
                                    // 找到第一筆 enable = 1 且 left minute > 0 的 profile, 存到 share preference
                                    DataManager.getInstance().setPauseInfoModel(pauseInfoModel);
                                    find = true;
                                    break;
                                }
                            }
                            if (!find) {
                                // 如果沒有找到符合上述條件的 pause info, 代表 global pause 是 disable 的狀態
                                // 儲存 enable = 0 的資料到 share preference
                                DataManager.getInstance().setPauseInfoModel(new PauseInfoModel());
                            }

                            sendBroadcast(new Intent(AppConstants.EVENT_GLOBAL_PAUSE_INFO_UPDATE));
                        }
                    }
                }
            });
        }
    }
}
