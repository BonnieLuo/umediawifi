package com.onyx.wifi.cloud.item;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class ErrorResult {

    // There are different content of error may be responded by cloud server.

    /**
     * {
     * "code": 4013,
     * "message": "Unverified email."
     * }
     */

    /**
     * {
     * "cause": "The email was taken.",
     * "message": "The email was taken.",
     * "type": "javax.servlet.ServletException"
     * }
     */

    /**
     * {
     * "timestamp": "2019-05-03T14:22:48.674+0000",
     * "status": 500,
     * "error": "Internal Server Error",
     * "message": "Missing or invalid Authorization header.",
     * "path": "/update"
     * }
     */

    @SerializedName("code")
    private int code = -1;

    @SerializedName("message")
    private String message = "";

    @SerializedName("cause")
    private String cause = "";

    @SerializedName("type")
    private String type = "";

    @SerializedName("timestamp")
    private String timestamp = "";

    @SerializedName("status")
    private int status = -1;

    @SerializedName("error")
    private String error = "";

    @SerializedName("path")
    private String path = "";

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
