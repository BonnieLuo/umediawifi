package com.onyx.wifi.cloud;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.onyx.wifi.MainApp;
import com.onyx.wifi.cloud.interfaces.AccountApi;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.interfaces.AudioApi;
import com.onyx.wifi.cloud.interfaces.GetTokenCallback;
import com.onyx.wifi.cloud.interfaces.InternetPauseApi;
import com.onyx.wifi.cloud.interfaces.MemberApi;
import com.onyx.wifi.cloud.interfaces.UserApi;
import com.onyx.wifi.cloud.interfaces.WirelessSettingApi;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.AccountManager;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.BnbNetworkSetting;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.GuestNetworkSetting;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfig;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRule;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRule;
import com.onyx.wifi.model.item.advancewireless.reboot.ScheduleRebootConfig;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless2G;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless5G;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.pause.PauseProfile;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.utility.encrypt.EncryptUtils;
import com.onyx.wifi.utility.encrypt.RNCryptorUtils;
import com.onyx.wifi.viewmodel.item.login.ForgotPasswordUser;
import com.onyx.wifi.viewmodel.item.login.SignUpUser;
import com.onyx.wifi.viewmodel.item.mainmenu.accountsetting.AccountSettingUser;
import com.onyx.wifi.viewmodel.item.mainmenu.wirelesssetting.WirelessSettingData;

import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CloudManager {

    private final String VERSION = "v2/";
    private final String TEST_SERVER_URL = RNCryptorUtils.getTestServerUrl() + VERSION;
    //    private final String STAGE_SERVER_URL = RNCryptorUtils.getStageServerUrl() + VERSION;
    private final String RDQA_SERVER_URL = RNCryptorUtils.getRdqaServerUrl() + VERSION;
    private final String PRODUCTION_SERVER_URL = RNCryptorUtils.getProductionServerUrl() + VERSION;
    private final int TIMEOUT = 30;

    private static CloudManager mInstance = null;

    private Context mContext;

    private AccountApi mAccountApi;
    private UserApi mUserApi;
    private AudioApi mAudioApi;
    private MemberApi mMemberApi;
    private InternetPauseApi mInternetPauseApi;
    private WirelessSettingApi mWirelessSettingApi;

    public static CloudManager getInstance() {
        if (mInstance == null) {
            mInstance = new CloudManager();
        }

        return mInstance;
    }

    private CloudManager() {
        mContext = MainApp.getInstance();

        initialRetrofit();
    }

    public void initialCloudApiBaseUrl() {
        initialRetrofit();
    }

    private void initialRetrofit() {
        String baseUrl;
        switch (DataManager.getInstance().getCloudServerMode()) {
            case TEST:
                baseUrl = TEST_SERVER_URL;
                break;

//            case STAGE:
//                baseUrl = STAGE_SERVER_URL;
//                break;

            case RDQA:
                baseUrl = RDQA_SERVER_URL;
                break;

            case PRODUCTION:
            default:
                baseUrl = PRODUCTION_SERVER_URL;
                break;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)   // 設置 baseUrl
                .client(getOkHttpClient())   // 將 okHttpClient 加入連線基底
                .addConverterFactory(GsonConverterFactory.create(getGson())) // addConverterFactory 用 Gson 作為資料處理 Converter
                .build();

        mAccountApi = retrofit.create(AccountApi.class);
        mUserApi = retrofit.create(UserApi.class);
        mAudioApi = retrofit.create(AudioApi.class);
        mMemberApi = retrofit.create(MemberApi.class);
        mInternetPauseApi = retrofit.create(InternetPauseApi.class);
        mWirelessSettingApi = retrofit.create(WirelessSettingApi.class);
    }

    private Gson getGson() {
        return new GsonBuilder().serializeNulls().create();
    }

    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient().newBuilder();
        okHttpClientBuilder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.addInterceptor(new RequestInterceptor());
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                LogUtils.trace(message);
            }
        });
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        okHttpClientBuilder.addInterceptor(logging);

        // get unsafe OKHttp client
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            okHttpClientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            okHttpClientBuilder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return okHttpClientBuilder.build();
    }

    private boolean isInternetAvailable() {
        return CommonUtils.isInternetAvailable(mContext);
    }

    // 取得 Firebase user 的 token
    private void getToken(final GetTokenCallback callback) {
        FirebaseUser user = AccountManager.getInstance().getUser();
        if (user != null) {
            user.getIdToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    String idToken = "";
                    try {
                        if (task.getResult() != null) {
                            idToken = task.getResult().getToken();
                            LogUtils.traceOnlyDebug("user token: " + idToken);
                        }
                    } catch (RuntimeExecutionException e) {
                        LogUtils.trace(e.getMessage());
                    }
                    callback.onComplete(idToken);
                }
            });
        } else {
            callback.onComplete("");
        }
    }

    /* 使用 Firebase SDK 登入取得 token 後, 將其加入 Http Header 中的 Authorization 屬性.
       Authorization 前置詞為"Bearer". */
    private String getAuthorizeHeader(String token) {
        return "Bearer " + token;
    }

    private String getNodeSetId() {
        return DataManager.getInstance().getNodeSetId();
    }

    private String getControllerDid() {
        return DataManager.getInstance().getControllerDid();
    }

    private <T> void sendRequest(final ApiCallback apiCallback, Call<T> call) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                apiCallback.onReceiveResponse(new ApiResponse<>(response));
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                if (!call.isCanceled()) {
                    call.cancel();
                }
                apiCallback.onReceiveResponse(new ApiResponse<>(t));
            }
        });
    }

    // 為了安全性, 需將密碼加密過後再上傳 (若傳輸的資料被窺探, 至少看到的密碼是加密過後的而不是明文)
    private String getEncryptPassword(String password) {
        return EncryptUtils.shaEncrypt(password);
    }

    // ************************
    // Account API
    // ************************
    // O (Row 5)
    public void signUp(final ApiCallback apiCallback, SignUpUser signUpUser) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            JsonObject requestJson = new JsonObject();
            requestJson.addProperty("email", signUpUser.getEmail());
            requestJson.addProperty("password", getEncryptPassword(signUpUser.getPassword()));
            requestJson.addProperty("first_name", signUpUser.getFirstName());
            requestJson.addProperty("last_name", signUpUser.getLastName());
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mAccountApi.signUp(requestJson));
        }
    }

    // O (Row 7)
    public void verifyEmail(final ApiCallback apiCallback, String verificationCode, String uid) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            JsonObject requestJson = new JsonObject();
            requestJson.addProperty("code", verificationCode);
            requestJson.addProperty("uid", uid);
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mAccountApi.verifyEmail(requestJson));
        }
    }

    // O (Row 8)
    public void resendEmailValidation(final ApiCallback apiCallback, String email) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            JsonObject requestJson = new JsonObject();
            requestJson.addProperty("email", email);
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mAccountApi.resendEmailValidation(requestJson));
        }
    }

    // O (Row 13)
    public void forgotPassword(final ApiCallback apiCallback, String email) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            JsonObject requestJson = new JsonObject();
            requestJson.addProperty("email", email);
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mAccountApi.forgotPassword(requestJson));
        }
    }

    // O (Row 14)
    public void verifyResetPassword(final ApiCallback apiCallback, ForgotPasswordUser forgotPasswordUser) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            JsonObject requestJson = new JsonObject();
            requestJson.addProperty("code", forgotPasswordUser.getResetCode());
            requestJson.addProperty("email", forgotPasswordUser.getEmail());
            requestJson.addProperty("new_password", getEncryptPassword(forgotPasswordUser.getPassword()));
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mAccountApi.verifyResetPassword(requestJson));
        }
    }

    // O (Row 15)
    public void getUserInfo(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mAccountApi.getUserInfo(getAuthorizeHeader(token)));
                }
            });
        }
    }

    // apiCallback is implemented in corresponding viewmodel
    // O (Row 16)
    public void updateUserInfo(final ApiCallback apiCallback, final AccountSettingUser accountSettingUser) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("first_name", accountSettingUser.getFirstName());
                    requestJson.addProperty("last_name", accountSettingUser.getLastName());
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAccountApi.updateUserInfo(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // O (Row 12)
    public void updatePassword(final ApiCallback apiCallback, final String newPassword) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("new_password", getEncryptPassword(newPassword));
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAccountApi.updatePassword(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // apiCallback is implemented in corresponding viewmodel
    // O (Row 9)
    public void updateEmail(final ApiCallback apiCallback, final String newEmail) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("new_email", newEmail);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAccountApi.updateEmail(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // apiCallback is implemented in corresponding viewmodel
    // O (Row 10)
    public void verifyUpdateEmail(final ApiCallback apiCallback, final String verificationCode, final String newEmail) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("code", verificationCode);
                    requestJson.addProperty("new_email", newEmail);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAccountApi.verifyUpdateEmail(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // O (Row 18)
    public void socialSignUp(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mAccountApi.socialSignUp(getAuthorizeHeader(token)));
                }
            });
        }
    }

    // O (Row 17)
    public void deleteUser(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mAccountApi.deleteUser(getAuthorizeHeader(token)));
                }
            });
        }
    }

    // O (Row 19)
    public void registerFcmToken(final ApiCallback apiCallback, final String fcmToken) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("token", fcmToken);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAccountApi.registerFcmToken(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // O (Row 20)
    public void unregisterFcmToken(final ApiCallback apiCallback, final String fcmToken) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mAccountApi.unregisterFcmToken(getAuthorizeHeader(token), fcmToken));
                }
            });
        }
    }

    // Deprecated
    public void testFcm(final ApiCallback apiCallback, final String anyKey, final String anyValue) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty(anyKey, anyValue);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAccountApi.testFcm(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    //
    // Audio api
    //
    // 107
    public void setMuteUnMute(final ApiCallback apiCallback, final String command, final String mute, final String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("command", command);
                    requestJson.addProperty("mute", mute);
                    requestJson.addProperty("did", did);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAudioApi.setMuteUnMute(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 108
    public void setUnRegister(final ApiCallback apiCallback, final String command, final String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("command", command);
                    requestJson.addProperty("register", "0");
                    requestJson.addProperty("did", did);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mAudioApi.setUnRegister(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    public void getAudioDeviceInfo(final ApiCallback apiCallback, final String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mAudioApi.getAudioDeviceInfo(getAuthorizeHeader(token), did));
                }
            });
        }
    }

    // 110
    //	1. 少了did
    //  2. audio_ip值是空的
    public void getAudioDeviceInfoList(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mAudioApi.getAudioDeviceInfoList(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // ************************
    // User API
    // ************************
    // O (Row 23)
    public void getProvisionToken(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getProvisionToken(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // O (Row 24)
    public void getProvisionState(final ApiCallback apiCallback, final String provisionToken) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getProvisionState(getAuthorizeHeader(token), provisionToken, getNodeSetId()));
                }
            });
        }
    }

    // 39
    // fw_status_from_all_node沒東西
    public void getDashboardInfo(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getDashboardInfo(getAuthorizeHeader(token)));
                }
            });
        }
    }

    // O (Row 77)
    public void getSpeedTest(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getSpeedTest(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // O (Row 78)
    // 目前 cloud API 只有支援 router 的 speed test, extender 還沒有
    // 未來如果要支援 extender 的 speed test, 可能是 request json 要再多帶 did 參數, 或是有其他 API
    // TODO: extender speed test
    public void startSpeedTest(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.startSpeedTest(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // O (Row 35)
    public void getNodeInfo(final ApiCallback apiCallback, String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getNodeInfo(getAuthorizeHeader(token), getNodeSetId(), did));
                }
            });
        }
    }

    // O (Row 36)
    public void changeDeviceLabel(final ApiCallback apiCallback, String did, String deviceLabel) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("did", did);
                    JsonObject dataJson = new JsonObject();
                    dataJson.addProperty("device_label", StringUtils.encodeToHexString(deviceLabel));
                    requestJson.add("data", dataJson);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.changeDeviceLabel(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // O (Row 38)
    public void removeDevice(final ApiCallback apiCallback, String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.removeDevice(getAuthorizeHeader(token), getNodeSetId(), did));
                }
            });
        }
    }

    // O (Row 57)
    public void rebootDevice(final ApiCallback apiCallback, String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("did", did);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.rebootDevice(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // O (Row 104)
    public void deviceConnectionTest(final ApiCallback apiCallback, String did) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("did", did);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.deviceConnectionTest(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // Firmware update
    // 74
    public void firmwareUpdateAll(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", getNodeSetId());
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.firmwareUpdateAll(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 75
    //
    public void getFirmwareStatus(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    LogUtils.trace("FWList", getControllerDid());
                    sendRequest(apiCallback, mUserApi.getFirmwareStatus(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // 30
    // @GET("user/client") => @GET("user/clients") 
    // not ready 原來的url才有資料
    public void getClientInfoList(final ApiCallback apiCallback, Map<String, String> options) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getClientInfoList(getAuthorizeHeader(token), options));
                }
            });
        }
    }

    // network setting
    // 59
    // not ready
    public void postHomeNetworkSetting(final ApiCallback apiCallback, String city, NetworkSetting networkSetting) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    // nodeset_id
                    requestJson.addProperty("nodeset_id", DataManager.getInstance().getNodeSetId());

                    // city
                    requestJson.addProperty("city", city);

                    // auto_fw_enable
                    if (networkSetting.getAutoFwEnable()) {
                        requestJson.addProperty("auto_fw_enable", "1");
                    } else {
                        requestJson.addProperty("auto_fw_enable", "0");
                    }

                    // auto_bandwidth
                    if (networkSetting.getAutoBandwidth()) {
                        requestJson.addProperty("auto_bandwidth", "1");
                    } else {
                        requestJson.addProperty("auto_bandwidth", "0");
                    }

                    // network_name
                    requestJson.addProperty("network_name", networkSetting.getNetworkName());

                    // daylight_saving
                    //requestJson.addProperty("daylight_saving", city);

                    // led_light
                    /*if (networkSetting.getLedLight()) {
                        requestJson.addProperty("led_light", "1");
                    } else {
                        requestJson.addProperty("led_light", "0");
                    }*/

                    // high_internet_priority
                    //requestJson.addProperty("high_internet_priority", city);

                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.postHomeNetworkSetting(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // post uptime
    public void postUptime(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    LogUtils.trace("request json: " + requestJson.toString());

                    sendRequest(apiCallback, mUserApi.postUptime(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // put internet priority
    public void putInternetPriority(final ApiCallback apiCallback, ArrayList<String> cidList) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    JsonArray cidArray = new JsonArray();

                    for (String cid : cidList) {
                        cidArray.add(cid);
                    }
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.add("internet_priority", cidArray);

                    sendRequest(apiCallback, mUserApi.putInternetPriority(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // update wireless setting
    // O (Row 65)
    public void updateWirelessSetting(final ApiCallback apiCallback, WirelessSettingData wirelessSettingData) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    switch (wirelessSettingData.getSettingType()) {
                        case INFO_2G:
                            JsonObject info = new JsonObject();
                            info.addProperty("ssid", StringUtils.encodeToHexString(wirelessSettingData.getSsid2g()));
                            info.addProperty("pwd", StringUtils.encodeToHexString(wirelessSettingData.getPassword2g()));
                            requestJson.add("2G", info);
                            break;

                        case INFO_5G:
                            info = new JsonObject();
                            info.addProperty("ssid", StringUtils.encodeToHexString(wirelessSettingData.getSsid5g()));
                            info.addProperty("pwd", StringUtils.encodeToHexString(wirelessSettingData.getPassword5g()));
                            requestJson.add("5G", info);
                            break;

                        case BOTH:
                            info = new JsonObject();
                            info.addProperty("ssid", StringUtils.encodeToHexString(wirelessSettingData.getSsid2g()));
                            info.addProperty("pwd", StringUtils.encodeToHexString(wirelessSettingData.getPassword2g()));
                            requestJson.add("2G", info);
                            info = new JsonObject();
                            info.addProperty("ssid", StringUtils.encodeToHexString(wirelessSettingData.getSsid5g()));
                            info.addProperty("pwd", StringUtils.encodeToHexString(wirelessSettingData.getPassword5g()));
                            requestJson.add("5G", info);
                            break;
                    }
                    requestJson.addProperty("wifi_same", wirelessSettingData.getWifiSame() ? "1" : "0");
                    requestJson.addProperty("client_online_notification", wirelessSettingData.getClientOnlineNotification() ? "1" : "0");
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.updateWirelessSetting(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // get wireless setting
    // O (Row 66)
    public void getWirelessSetting(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getWirelessSetting(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // update guest network
    // O (Row 67)
    public void updateGuestNetwork(final ApiCallback apiCallback, GuestNetworkSetting guestNetworkSetting) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("enable", guestNetworkSetting.getEnable() ? "1" : "0");
                    requestJson.addProperty("ssid", StringUtils.encodeToHexString(guestNetworkSetting.getSsid()));
                    requestJson.addProperty("pwd", StringUtils.encodeToHexString(guestNetworkSetting.getPwd()));
                    LogUtils.trace("guestNetwork", "request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.updateGuestNetwork(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // update BnB network
    // O (Row 68)
    public void updateBnbNetwork(final ApiCallback apiCallback, BnbNetworkSetting bnbNetworkSetting) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("enable", bnbNetworkSetting.getEnable() ? "1" : "0");
                    requestJson.addProperty("ssid", StringUtils.encodeToHexString(bnbNetworkSetting.getSsid()));
                    requestJson.addProperty("pwd", StringUtils.encodeToHexString(bnbNetworkSetting.getPwd()));
                    requestJson.addProperty("start", bnbNetworkSetting.getStart());
                    requestJson.addProperty("end", bnbNetworkSetting.getEnd());
                    LogUtils.trace("bnbNetwork", "request json: " + requestJson.toString());
                    sendRequest(apiCallback, mUserApi.updateBnbNetwork(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // get guest network
    // O (Row 69)
    public void getGuestNetwork(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getGuestNetwork(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // get BnB network
    // O (Row 70)
    public void getBnbNetwork(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getBnbNetwork(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // get node list
    // O (Row 34)
    public void getNodeList(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    sendRequest(apiCallback, mUserApi.getNodeList(getAuthorizeHeader(token), getNodeSetId()));
                }
            });
        }
    }

    // get network map
    // O (Row 29)
    public void getNetworkMap(final ApiCallback apiCallback, String nodeDid) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    Map<String, String> options = new HashMap<>();
                    options.put("nodeset_id", getNodeSetId());
                    options.put("scope", "node");
                    options.put("did", nodeDid);
                    sendRequest(apiCallback, mUserApi.getNetworkMap(getAuthorizeHeader(token), options));
                }
            });
        }
    }

    // MemberModel

    // 81
    // @GET("user/member_list") =>@GET("user/members")
    // 1. spec寫錯: nodeset_id -> nid
    public void getMemberList(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    sendRequest(apiCallback, mMemberApi.getMemberList(getAuthorizeHeader(token), nodesetId));
                }
            });
        }
    }

    // 86
    // OK
    public void createMember(final String memberName, List<Client> clientList, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", getNodeSetId());
                    requestJson.addProperty("member_name", memberName);

                    JsonArray array = new JsonArray();
                    if (clientList.size() > 0) {
                        for (Client client : clientList) {
                            String cid = client.getCid();
                            array.add(new JsonPrimitive(cid));
                        }
                    }
                    requestJson.add("cids", array);

                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.createMember(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 88
    // 	1. spec少了member_name
    //  2. Cloud not ready
    public void updateMember(final String memberId, final String memberName, List<Client> clientList, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", getNodeSetId());// "nodeset_id"
                    requestJson.addProperty("mid", memberId);// "member_id"
                    requestJson.addProperty("member_name", memberName);
                    JsonArray array = new JsonArray();
                    if (clientList.size() > 0) {
                        for (Client client : clientList) {
                            String cid = client.getCid();
                            array.add(new JsonPrimitive(cid));
                        }
                    }
                    requestJson.add("cids", array);// "assigned_client_list"
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.updateMember(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 91
    // key name與spec不符
    public void removeMember(final String memberId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();
                    sendRequest(apiCallback, mMemberApi.removeMember(getAuthorizeHeader(token), nodeSetId, memberId));
                }
            });
        }
    }

    // 87
    // OK
    public void assignClientToMember(String newOwnerId, Client client, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();
                    String cid = client.getCid();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", nodeSetId);
                    requestJson.addProperty("mid", newOwnerId);
                    requestJson.addProperty("cid", cid);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.assignClientToMember(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 89
    // OK
    public void removeClientFromMember(String ownerId, Client client, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();
                    String cid = client.getCid();

                    sendRequest(apiCallback, mMemberApi.removeClientFromMember(getAuthorizeHeader(token), nodeSetId, ownerId, cid));
                }
            });
        }
    }

    // 48
    // not ready
    public void pauseMember(String memberId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("member_id", memberId);
                    requestJson.addProperty("enable", "1");
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseMember(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 48
    // not ready
    public void resumeMember(String memberId, String ruleId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("member_id", memberId);
                    requestJson.addProperty("rule_id", ruleId);
                    requestJson.addProperty("enable", "0");
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseMember(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 47
    // not ready
    //    nodeset_id -> nid ?
    //    cloud 還未從mac -> cid
    public void pasueClient(String cid, int minutes, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("cid", cid);
                    requestJson.addProperty("enable", "1");
                    requestJson.addProperty("duration", String.valueOf(minutes));
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseClient(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 47
    // not ready
    public void resumeClient(String cid, String ruleId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("cid", cid);
                    requestJson.addProperty("rule_id", ruleId);
                    requestJson.addProperty("enable", "0");
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseClient(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // not used now
    public void pauseHomeNetwork(int minutes, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("enable", 1);
                    requestJson.addProperty("duration", String.valueOf(minutes));
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseHomeNetwork(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // not used now
    public void pauseGusetNetwork(int minutes, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("enable", 1);
                    requestJson.addProperty("duration", String.valueOf(minutes));
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseGuestNetwork(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // not used now
    public void pauseBnBGuestNetwork(int minutes, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    requestJson.addProperty("enable", 1);
                    requestJson.addProperty("duration", String.valueOf(minutes));
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseBnBGuestNetwork(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 32
    // not ready
    //   1. Device_outage_monitoring -> outage_monitor
    //   2. app的internet_priority還沒加
    public void updateClient(ClientModel clientModel, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", getNodeSetId());
                    requestJson.addProperty("cid", clientModel.getCid());
                    requestJson.addProperty("client_name", StringUtils.encodeToHexString(clientModel.getName()));
                    requestJson.addProperty("outage_monitor", clientModel.getDeviceOutageMonitoring());
                    requestJson.addProperty("client_type", clientModel.getClientType());
                    requestJson.addProperty("internet_priority", clientModel.getInternetPriority()); // TODO: 尚未加此功能
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.updateClientSetting(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 33
    // not ready
    // 原本api拿掉改成#33，cid array裡塞單筆資訊(cid)
    public void removeClients(ArrayList<ClientModel> clientModelList, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();

                    /////////////////////////////////
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", getNodeSetId());// "nodeset_id"
                    JsonArray array = new JsonArray();
                    for (ClientModel clientInfo : clientModelList) {
                        array.add(new JsonPrimitive(clientInfo.getCid()));
                    }


                    requestJson.add("remove_cid_list", array);// "assigned_client_list"
                    /////////////////////////////////
                    sendRequest(apiCallback, mMemberApi.removeClients(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    public void blockClient(ClientModel clientModel, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();
                    String cid = clientModel.getCid();
                    /////////////////////////////////
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", nodeSetId);// "nodeset_id"
                    requestJson.addProperty("cid", cid);

                    /////////////////////////////////
                    sendRequest(apiCallback, mMemberApi.blockClient(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    public void unblockClient(ClientModel clientModel, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();
                    String cid = clientModel.getCid();
                    /////////////////////////////////
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nid", nodeSetId);// "nodeset_id"
                    requestJson.addProperty("cid", cid);

                    /////////////////////////////////
                    sendRequest(apiCallback, mMemberApi.unblockClient(getAuthorizeHeader(token), requestJson));//, nodeSetId, cid));
                }
            });
        }
    }


    // Content Filters-Website Blocking
    // 95
    public void getContentFiltersList(final String memberId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    sendRequest(apiCallback, mMemberApi.getContentFiltersList(getAuthorizeHeader(token), nodesetId, memberId));
                }
            });
        }
    }

    // 94
    // not ready
    public void createContentFilters(final String memberId, final String site, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("member_id", memberId);
                    requestJson.addProperty("enable", "1");
                    JsonArray array = new JsonArray();
                    array.add(new JsonPrimitive(site));
                    requestJson.add("keyword_blacklist", array);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.createContentFilters(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 97 (enable: 1)
    // can not test
    public void addContentFilters(final String memberId, final String site, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("member_id", memberId);
                    requestJson.addProperty("enable", "1");
                    JsonArray addArray = new JsonArray();
                    addArray.add(new JsonPrimitive(site));
                    requestJson.add("add", addArray);
                    JsonArray removeArray = new JsonArray();
                    requestJson.add("remove", removeArray);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.addContentFilters(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 97 (enable: 0)
    // can not test
    public void removeContentFilters(final String memberId, final String site, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("member_id", memberId);
                    requestJson.addProperty("enable", "1");
                    JsonArray addArray = new JsonArray();
                    requestJson.add("add", addArray);
                    JsonArray removeArray = new JsonArray();
                    removeArray.add(new JsonPrimitive(site));
                    requestJson.add("remove", removeArray);
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.removeContentFilters(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 96
    // can not test
    public void cleanContentFilters(final String memberId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    sendRequest(apiCallback, mMemberApi.cleanContentFilters(getAuthorizeHeader(token), nodesetId, memberId));
                }
            });
        }
    }


    // Member Schedule

    public void getMemberScheduleList(final String memberId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    sendRequest(apiCallback, mMemberApi.getMemberScheduleList(getAuthorizeHeader(token), nodesetId, memberId));
                }
            });
        }
    }

    // 54
    // 少了很多欄位
    public void createMemberSchedule(final JsonObject requestJson, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.createMemberSchedule(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 56
    // can not test
    public void updateMemberSchedule(final JsonObject requestJson, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    requestJson.addProperty("nodeset_id", getNodeSetId());
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mMemberApi.updateMemberSchedule(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 55
    public void removeMemberSchedule(final String memberId, final String scheduleId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodeSetId = getNodeSetId();

                    sendRequest(apiCallback, mMemberApi.removeMemberSchedule(getAuthorizeHeader(token), nodeSetId, memberId, scheduleId));
                }
            });
        }
    }

    // Global Pause
    // 42
    // OK
    public void getGlobalPauseList(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    sendRequest(apiCallback, mInternetPauseApi.getGlobalPauseList(getAuthorizeHeader(token), nodesetId));
                }
            });
        }
    }


    // 43
    // OK
    public void createGlobalPauseProfile(PauseProfile pauseProfile, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();

                    JsonObject requestJson = pauseProfile.toJson();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    sendRequest(apiCallback, mInternetPauseApi.createGlobalPauseProfile(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 45
    // OK
    public void updateGlobalPauseProfile(PauseProfile pauseProfile, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();

                    JsonObject requestJson = pauseProfile.toJson();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    sendRequest(apiCallback, mInternetPauseApi.updateGlobalPauseProfile(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 44
    // OK
    public void removeGlobalPauseProfile(PauseProfile pauseProfile, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    String profileId = pauseProfile.getProfileId();
                    sendRequest(apiCallback, mInternetPauseApi.removeGlobalPauseProfile(getAuthorizeHeader(token), nodesetId, profileId));
                }
            });
        }
    }

    // 46
    // app not ready to test
    public void enableGlobalPause(String profileId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("profile_id", profileId);
                    requestJson.addProperty("enable", "1");
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseGlobal(getAuthorizeHeader(token), requestJson));

                }
            });
        }
    }

    // 46
    // app not ready to test
    public void disableGlobalPause(String profileId, String ruleId, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("profile_id", profileId);
                    requestJson.addProperty("rule_id", ruleId);
                    requestJson.addProperty("enable", "0");
                    LogUtils.trace("request json: " + requestJson.toString());
                    sendRequest(apiCallback, mInternetPauseApi.pauseGlobal(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // Member
    // 112
    // OK
    public void getAdvanceWirelessSettings(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    sendRequest(apiCallback, mWirelessSettingApi.getAdvanceWirelessSettings(getAuthorizeHeader(token), nodesetId));
                }
            });
        }
    }

    // 114
    // not ready
    public void setupSSID(Wireless5G wireless5G, Wireless2G wireless2G, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {

                    // 5G
                    String wireless5GChannel = wireless5G.getChannel();
                    String wireless5GBandwidth = wireless5G.getBandwidth();
                    String wireless5GEnable = wireless5G.isSsidBroadcast() ? "1" : "0";

                    JsonObject wireless5GJson = new JsonObject();
                    wireless5GJson.addProperty("ch", wireless5GChannel);
                    wireless5GJson.addProperty("bandwidth", wireless5GBandwidth);
                    wireless5GJson.addProperty("ssid_broadcast", wireless5GEnable);

                    // 2.4G
                    String channel = wireless2G.getChannel();
                    String bandwidth = wireless2G.getBandwidth();
                    String enable = wireless2G.isSsidBroadcast() ? "1" : "0";

                    JsonObject wirelessJson = new JsonObject();
                    wirelessJson.addProperty("ch", channel);
                    wirelessJson.addProperty("bandwidth", bandwidth);
                    wirelessJson.addProperty("ssid_broadcast", enable);

                    String nodesetId = getNodeSetId();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.add("5G", wireless5GJson);
                    requestJson.add("2G", wirelessJson);

                    sendRequest(apiCallback, mWirelessSettingApi.setupSSID(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 115
    // not ready(404)
    public void setupDmz(DemilitarizedZoneConfig dmzConfig, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();

                    String enable = dmzConfig.isEnabled() ? "1" : "0";
                    String ipAddress = dmzConfig.getIpAddress();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("enable", enable);
                    requestJson.addProperty("ip_addr", ipAddress);

                    sendRequest(apiCallback, mWirelessSettingApi.setupDmz(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 116
    // Cloud not ready
    public void updatePortForwardingRule(PortForwardingRule rule, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {

                    JsonArray dataArray = new JsonArray();

                    String enable = rule.isEnabled() ? "1" : "0";
                    String id = rule.getId();
                    String hexName = rule.getHexName();
                    String ip = rule.getIp();
                    String externalPort = rule.getExternalPort();
                    String internalPort = rule.getInternalPort();
                    String protocol = rule.getProtocol();

                    JsonObject ruleJson = new JsonObject();

                    ruleJson.addProperty("enable", enable);
                    ruleJson.addProperty("name", hexName);
                    ruleJson.addProperty("ip_addr", ip);
                    ruleJson.addProperty("external_port", externalPort);
                    ruleJson.addProperty("internal_port", internalPort);
                    ruleJson.addProperty("protocol", protocol);

                    if (id != null) {
                        ruleJson.addProperty("pfid", id);
                    }

                    dataArray.add(ruleJson);

                    String nodesetId = getNodeSetId();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.add("data", dataArray);

                    sendRequest(apiCallback, mWirelessSettingApi.updatePortForwardingRule(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // 117
    // Cloud not ready
    public void removePortForwardingRule(PortForwardingRule rule, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    String id = rule.getId();

                    sendRequest(apiCallback, mWirelessSettingApi.removePortForwardingRule(getAuthorizeHeader(token), nodesetId, id));
                }
            });
        }
    }

    // 118
    // app is not ready to test
    public void updatePortTriggeringRule(PortTriggeringRule rule, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {

                    JsonArray dataArray = new JsonArray();

                    String enable = rule.isEnabled() ? "1" : "0";
                    String id = rule.getId();
                    String hexName = rule.getHexName();
                    String matchPort = rule.getMatchPort();
                    String matchProtocol = rule.getMatchedProtocol();
                    String trigger_port = rule.getTriggerPort();
                    String triggerProtocol = rule.getTriggeredProtocol();

                    JsonObject ruleJson = new JsonObject();
                    ruleJson.addProperty("enable", enable);
                    if (id != null) {
                        ruleJson.addProperty("ptid", id);
                    }
                    ruleJson.addProperty("name", hexName);
                    ruleJson.addProperty("match_port", matchPort);
                    ruleJson.addProperty("match_protocol", matchProtocol);
                    ruleJson.addProperty("trigger_port", trigger_port);
                    ruleJson.addProperty("trigger_protocol", triggerProtocol);

                    dataArray.add(ruleJson);

                    String nodesetId = getNodeSetId();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);// 不需要
                    requestJson.add("data", dataArray);

                    sendRequest(apiCallback, mWirelessSettingApi.updatePortTriggeringRule(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }


    // 119
    // cannot test
    public void removePortTriggeringRule(PortTriggeringRule rule, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    String id = rule.getId();

                    sendRequest(apiCallback, mWirelessSettingApi.removePortTriggeringRule(getAuthorizeHeader(token), nodesetId, id));
                }
            });
        }
    }

    // 113
    // cannot test
    public void setupScheduledReboot(ScheduleRebootConfig config, final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();

                    String enable = config.isEnabled() ? "1" : "0";

                    Integer dayOfWeekIndex = config.getDayOfWeek();

                    String week = String.valueOf(dayOfWeekIndex);
                    String time = config.getTimeString("HH:mm");

                    JsonObject rebootJson = new JsonObject();
                    rebootJson.addProperty("enable", enable);
                    rebootJson.addProperty("week", week);
                    rebootJson.addProperty("time", time);

//                    JsonObject dataJson = new JsonObject();
//                    dataJson.add("reboot", rebootJson);

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.add("data", rebootJson);

                    sendRequest(apiCallback, mWirelessSettingApi.updateScheduledReboot(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    public void rebootNow(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();

                    String did = getControllerDid();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);
                    requestJson.addProperty("did", did);

                    sendRequest(apiCallback, mUserApi.rebootDevice(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }

    // Schedule reboot (network reboot #58)
    public void rebootAllNodes(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();

                    String did = getControllerDid();

                    JsonObject requestJson = new JsonObject();
                    requestJson.addProperty("nodeset_id", nodesetId);

                    sendRequest(apiCallback, mUserApi.rebootAllNodes(getAuthorizeHeader(token), requestJson));
                }
            });
        }
    }


    // Client List
    // 30
    // @GET("user/client") => @GET("user/clients") 
    public void getAllClients(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();

            getToken(new GetTokenCallback() {
                @Override
                public void onComplete(String token) {
                    String nodesetId = getNodeSetId();
                    String scope = "all";

                    sendRequest(apiCallback, mUserApi.getAllClients(getAuthorizeHeader(token), nodesetId, scope));
                }
            });
        }
    }

}
