package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonObject;
import com.onyx.wifi.utility.AppConstants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface WirelessSettingApi {

    String HEADER_NAME = AppConstants.CLOUD_API_HEADER_NAME;

    @GET("user/wireless/advance")
    Call<JsonObject> getAdvanceWirelessSettings(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId);

    @POST("user/wireless/advance")
    Call<JsonObject> setupSSID(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/home_network/dmz")
    Call<JsonObject> setupDmz(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PUT("user/home_network/port/forward")
    Call<JsonObject> updatePortForwardingRule(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/home_network/port/forward")
    Call<JsonObject> removePortForwardingRule(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("pfid") String pfid);

    @PUT("user/home_network/port/trigger")
    Call<JsonObject> updatePortTriggeringRule(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/home_network/port/trigger")
    Call<JsonObject> removePortTriggeringRule(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("ptid") String ptid);

    @PUT("user/home_network/reboot")
    Call<JsonObject> updateScheduledReboot(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

//    @POST("user/action/reboot")
//    Call<JsonObject> rebootNow(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

}
