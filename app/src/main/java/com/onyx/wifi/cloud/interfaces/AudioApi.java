package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonObject;
import com.onyx.wifi.utility.AppConstants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AudioApi {

    String HEADER_NAME = AppConstants.CLOUD_API_HEADER_NAME;

    @POST("user/alexa/audio")
    Call<JsonObject> setMuteUnMute(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/alexa/register")
    Call<JsonObject> setUnRegister(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @GET("user/alexa/status") // no use
    Call<JsonObject> getAudioDeviceInfo(@Header(HEADER_NAME) String authKey, @Query("did") String did);

    @GET("user/alexa/statuses")
    Call<JsonObject> getAudioDeviceInfoList(@Header(HEADER_NAME) String authKey, @Query("nid") String nodeSetId);
}
