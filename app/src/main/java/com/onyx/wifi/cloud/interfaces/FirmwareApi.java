package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonObject;
import com.onyx.wifi.model.FirmwareModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface FirmwareApi {

    @PUT(FirmwareModel.ApiUrl.SET_USER_ID)
    Call<JsonObject> setUserId(@Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_TIMEZONE)
    Call<JsonObject> setTimezone(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_PROVISION_TOKEN)
    Call<JsonObject> setProvisionToken(@Query("uid") String uid, @Body JsonObject requestJson);

    @GET(FirmwareModel.ApiUrl.GET_DEVICE_BOARD_ID)
    Call<JsonObject> getDeviceBoardId(@Query("uid") String uid);

    @PUT(FirmwareModel.ApiUrl.SET_DEVICE_OPERATION_MODE)
    Call<JsonObject> setDeviceOperationMode(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_5G_AP_CLIENT_WIFI)
    Call<JsonObject> set5gApClientWifiSsid(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_5G_AP_CLIENT_WIFI_KEY)
    Call<JsonObject> set5gApClientWifiPassword(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_5G_AP_CLIENT_WIFI_SEC)
    Call<JsonObject> set5gApClientWifiSecurity(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_AP_CLIENT_WIFI_ACTION)
    Call<JsonObject> setApClientWifiAction(@Query("uid") String uid);

    @GET(FirmwareModel.ApiUrl.GET_5G_AP_CLIENT_CONN_STATUS)
    Call<JsonObject> get5gApClientConnectionStatus(@Query("uid") String uid, @Query("radio") String radio);

    @PUT(FirmwareModel.ApiUrl.DETECT_WAN_TYPE)
    Call<JsonObject> detectWanType(@Query("uid") String uid);

    @GET(FirmwareModel.ApiUrl.GET_WAN_TYPE)
    Call<JsonObject> getWanType(@Query("uid") String uid);

    @PUT(FirmwareModel.ApiUrl.SET_WAN_DHCP)
    Call<JsonObject> setWanDhcp(@Query("uid") String uid);

    @PUT(FirmwareModel.ApiUrl.SET_WAN_PPPOE)
    Call<JsonObject> setPppoe(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_WAN_STATIC_IP)
    Call<JsonObject> setStaticIp(@Query("uid") String uid, @Body JsonObject requestJson);

    @GET(FirmwareModel.ApiUrl.GET_INTERNET_CONN_STATUS)
    Call<JsonObject> getInternetConnectionStatus(@Query("uid") String uid);

    @GET(FirmwareModel.ApiUrl.GET_DEVICE_TOKEN_STATUS)
    Call<JsonObject> getDeviceTokenStatus(@Query("uid") String uid);

    @PUT(FirmwareModel.ApiUrl.SET_LOCATION)
    Call<JsonObject> setLocation(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_2G_WIFI)
    Call<JsonObject> set2gWifiSsid(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_2G_WIFI_KEY)
    Call<JsonObject> set2gWifiPassword(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_5G_WIFI)
    Call<JsonObject> set5gWifiSsid(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_5G_WIFI_KEY)
    Call<JsonObject> set5gWifiPassword(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SET_BLE_ENABLE)
    Call<JsonObject> setBleEnable(@Query("uid") String uid, @Body JsonObject requestJson);

    @PUT(FirmwareModel.ApiUrl.SETUP_COMPLETE)
    Call<JsonObject> setupComplete(@Query("uid") String uid);
}
