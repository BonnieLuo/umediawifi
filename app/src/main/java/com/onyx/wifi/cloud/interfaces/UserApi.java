package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonObject;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.ProvisionToken;
import com.onyx.wifi.utility.AppConstants;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface UserApi {

    String HEADER_NAME = AppConstants.CLOUD_API_HEADER_NAME;

    // Device provision
    @GET("user/provision/token")
    Call<ProvisionToken> getProvisionToken(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodeSetId);

    @GET("user/provision/state")
    Call<JsonObject> getProvisionState(@Header(HEADER_NAME) String authKey, @Query("token") String provisionToken, @Query("nodeset_id") String nodeSetId);

    // Dashboard
    @GET("user/dashboard")
    Call<DashboardInfo> getDashboardInfo(@Header(HEADER_NAME) String authKey);

    @GET("user/test/speed")
    Call<JsonObject> getSpeedTest(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodeSetId);

    @POST("user/test/speed")
    Call<JsonObject> startSpeedTest(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // Node setting
    @GET("user/device")
    Call<JsonObject> getNodeInfo(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodeSetId, @Query("did") String did);

    @PUT("user/device")
    Call<JsonObject> changeDeviceLabel(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/device")
    Call<JsonObject> removeDevice(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodeSetId, @Query("did") String did);

    @POST("user/action/reboot")
    Call<JsonObject> rebootDevice(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/reboot/network")
    Call<JsonObject> rebootAllNodes(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // MQTT connection test
    @POST("user/device/conn")
    Call<JsonObject> deviceConnectionTest(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // Firmware update
    @POST("user/nodeset/firmware/update")
    Call<JsonObject> firmwareUpdateAll(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @GET("user/nodeset/firmware/status")
    Call<JsonObject> getFirmwareStatus(@Header(HEADER_NAME) String authKey, @Query("nid") String nodeSetId);

    // Client list
    // 帶的參數部分是optional，所以使用@QueryMap
    @GET("user/clients")//@GET("user/client")
    Call<JsonObject> getClientInfoList(@Header(HEADER_NAME) String authKey, @QueryMap Map<String, String> options);

    @GET("user/clients")//@GET("user/client")
    Call<JsonObject> getAllClients(@Header(HEADER_NAME) String authKey, @Query("nid") String nodesetId, @Query("scope") String scope);


    // Network setting
    @POST("user/home_network/setting")
    Call<JsonObject> postHomeNetworkSetting(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/get_uptime")
    Call<JsonObject> postUptime(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PUT("user/home_network/setting/internet_priority")
    Call<JsonObject> putInternetPriority(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // update wireless setting
    @POST("user/wireless")
    Call<JsonObject> updateWirelessSetting(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // get wireless setting
    @GET("user/wireless")
    Call<JsonObject> getWirelessSetting(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId);

    // update guest network
    @POST("user/guest_network")
    Call<JsonObject> updateGuestNetwork(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // update BnB network
    @POST("user/bnb_network")
    Call<JsonObject> updateBnbNetwork(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // get guest network
    @GET("user/guest_network")
    Call<JsonObject> getGuestNetwork(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId);

    // get BnB network
    @GET("user/bnb_network")
    Call<JsonObject> getBnbNetwork(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId);

    // Node list
    @GET("user/devices")
    Call<JsonObject> getNodeList(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodeSetId);

    // Network Map
    // 帶的參數部分是 optional, 所以使用 @QueryMap
    @GET("user/network/map")
    Call<JsonObject> getNetworkMap(@Header(HEADER_NAME) String authKey, @QueryMap Map<String, String> options);
}
