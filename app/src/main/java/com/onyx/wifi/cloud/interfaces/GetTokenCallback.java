package com.onyx.wifi.cloud.interfaces;

public interface GetTokenCallback {
    void onComplete(String token);
}
