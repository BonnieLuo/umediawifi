package com.onyx.wifi.cloud.item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onyx.wifi.MainApp;
import com.onyx.wifi.R;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;

import retrofit2.Response;

/**
 * Common class used by API responses.
 *
 * @param <T>
 */
public class ApiResponse<T> {

    public final int httpStatusCode;

    public final T body;

    public final String errorMsg;

    public final ErrorResult errorResult;

    public ApiResponse(Throwable error) {
        httpStatusCode = 500;
        body = null;
        errorResult = null;

        error.printStackTrace();
        LogUtils.trace(error.getMessage());
        if (error instanceof java.net.ConnectException
                || error instanceof java.net.UnknownHostException) {
            errorMsg = MainApp.getInstance().getString(R.string.err_internet_connection_fail);
        } else if (error instanceof java.net.SocketTimeoutException) {
            errorMsg = MainApp.getInstance().getString(R.string.err_internet_timeout);
        } else if (error instanceof java.net.SocketException) {
            errorMsg = MainApp.getInstance().getString(R.string.err_socket_exception);
        } else {
            errorMsg = MainApp.getInstance().getString(R.string.err_unknown) + "\n\n" + error.getMessage();
        }
    }

    public ApiResponse(Response<T> response) {
        LogUtils.trace(response.toString());
        httpStatusCode = response.code();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
                LogUtils.trace(gson.toJson(response.body()));
            }
            body = response.body();
            errorMsg = null;
            errorResult = null;
        } else {
            body = null;

            ErrorResult error = new ErrorResult();
            if (response.errorBody() != null) {
                try {
                    String json = response.errorBody().string();
                    error = new JsonParserWrapper().jsonToObject(json, ErrorResult.class);
                } catch (Exception e) {
                    LogUtils.trace(e.getMessage());
                }
            }
            LogUtils.trace(error.toString());
            errorResult = error;
            // 將 error message 提取出來, 方便 UI 層直接抓取來顯示
            if (!StringUtils.isNullOrEmpty(errorResult.getMessage())) {
                errorMsg = errorResult.getMessage();
            } else if (errorResult.getCode() != -1) {
                errorMsg = "Server error code: " + errorResult.getCode();
            } else if (!StringUtils.isNullOrEmpty(errorResult.getError())) {
                errorMsg = "Server error: " + errorResult.getError();
            } else if (!StringUtils.isNullOrEmpty(errorResult.getType())) {
                errorMsg = "Server error: " + errorResult.getType();
            } else {
                errorMsg = "Http status code: " + httpStatusCode;
            }
        }
    }
}
