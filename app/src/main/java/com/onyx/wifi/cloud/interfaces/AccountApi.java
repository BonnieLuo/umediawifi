package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.onyx.wifi.model.item.UserInfo;
import com.onyx.wifi.utility.AppConstants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AccountApi {

    String HEADER_NAME = AppConstants.CLOUD_API_HEADER_NAME;

    @POST("account/register")
    Call<JsonObject> signUp(@Body JsonObject requestJson);

    @POST("account/email/verify")
    Call<JsonObject> verifyEmail(@Body JsonObject requestJson);

    @POST("account/email/resend")
    Call<JsonObject> resendEmailValidation(@Body JsonObject requestJson);

    @POST("account/password/forgot")
    Call<JsonObject> forgotPassword(@Body JsonObject requestJson);

    @POST("account/password/verify")
    Call<JsonObject> verifyResetPassword(@Body JsonObject requestJson);

    @GET("account/profile")
    Call<UserInfo> getUserInfo(@Header(HEADER_NAME) String authKey);

    @POST("account/profile")
    Call<JsonObject> updateUserInfo(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("account/password/change")
    Call<JsonObject> updatePassword(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("account/email")
    Call<JsonObject> updateEmail(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("account/email/change/verify")
    Call<JsonObject> verifyUpdateEmail(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @GET("account/social")
    Call<UserInfo> socialSignUp(@Header(HEADER_NAME) String authKey);

    @DELETE("account/profile")
    Call<JsonObject> deleteUser(@Header(HEADER_NAME) String authKey);

    @POST("account/mobile/token")
    Call<JsonObject> registerFcmToken(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("account/mobile/token")
    Call<JsonObject> unregisterFcmToken(@Header(HEADER_NAME) String authKey, @Query("token") String token);

    @POST("account/notify/test")
    Call<JsonArray> testFcm(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);
}
