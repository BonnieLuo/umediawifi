package com.onyx.wifi.cloud.interfaces;

import com.onyx.wifi.cloud.item.ApiResponse;

public interface ApiCallback {
    /* view model 負責呼叫 CloudManager 打 cloud API, 回傳的結果透過 ApiCallback 反應給 view model,
       view model 再根據結果做相應處理, 並設定對應的 live data 以反應給 UI 層. */

    // 打 cloud API 前會先檢查網路狀態, 若沒有網路連線時 UI 層要有對應顯示
    void onInternetUnavailable();

    // 確定有網路連線, 要開始打 cloud API, UI 層要先顯示 loading 畫面.
    // 配合 BaseViewModel.mIsLoading 的設計:
    // (1) 要顯示 loading 畫面時: mIsLoading.setValue(true);
    // (2) 若不需要顯示 (例如: 在背景打 API, 或是 UI 已顯示 loading 畫面了), 則 onSendRequest() 裡面不做事即可, 這樣 mIsLoading 才不會被改動到
    //     因為同一個頁面的 loading 畫面通常是共用的, 若前景和背景都有在打 API, mIsLoading 的值可能會互相影響到
    void onSendRequest();

    // 收到 cloud API 回傳的結果
    <T> void onReceiveResponse(ApiResponse<T> apiResponse);
}
