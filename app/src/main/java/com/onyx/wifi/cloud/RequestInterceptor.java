package com.onyx.wifi.cloud;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        /*
        chain.request() returns original request that you can work with(modify, rewrite)
        */

        Request.Builder builder = chain.request().newBuilder();
        builder.addHeader("Connection", "close");

        /*
        chain.proceed(request) is the call which will initiate the HTTP work. This call invokes the
        request and returns the response as per the request.
        */

        Request newRequest = builder.build();
        Response response = chain.proceed(newRequest);

        return response;
    }
}
