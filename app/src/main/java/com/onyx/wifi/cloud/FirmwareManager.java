package com.onyx.wifi.cloud;

import android.content.Context;

import com.google.gson.JsonObject;
import com.onyx.wifi.MainApp;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.interfaces.FirmwareApi;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.encrypt.EncryptUtils;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FirmwareManager {

    private final String BASE_URL = "https://setup.u-media.com/api/";
    private final int TIMEOUT = 10;

    private static FirmwareManager mInstance = null;

    private Context mContext;

    private FirmwareApi mFirmwareApi;

    private String mUid;    // 以 MD5 加密 uid

    public static FirmwareManager getInstance() {
        if (mInstance == null) {
            mInstance = new FirmwareManager();
        }

        return mInstance;
    }

    private FirmwareManager() {
        mContext = MainApp.getInstance();

        initialRetrofit(BASE_URL);

        mUid = EncryptUtils.md5Encrypt(DataManager.getInstance().getUserId());
    }

    private void initialRetrofit(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)   // 設置 baseUrl
                .client(provideOkHttpClient())   // 將 okHttpClient 加入連線基底
                .addConverterFactory(GsonConverterFactory.create()) // addConverterFactory 用 Gson 作為資料處理 Converter
                .build();

        mFirmwareApi = retrofit.create(FirmwareApi.class);
    }

    public void changeFirmwareApiBaseUrl(String ip) {
        String url = "https://" + ip + "/api/";
        initialRetrofit(url);
    }

    public void restoreToDefaultBaseUrl() {
        initialRetrofit(BASE_URL);
    }

    private OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient().newBuilder();
        okHttpClientBuilder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.addInterceptor(new RequestInterceptor());
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                LogUtils.trace(message);
            }
        });
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        okHttpClientBuilder.addInterceptor(logging);

        // get unsafe OKHttp client
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            okHttpClientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            okHttpClientBuilder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return okHttpClientBuilder.build();
    }

    private boolean isInternetAvailable() {
        return CommonUtils.isInternetAvailable(mContext);
    }

    private <T> void sendRequest(final ApiCallback apiCallback, Call<T> call) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                apiCallback.onReceiveResponse(new ApiResponse<>(response));
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                apiCallback.onReceiveResponse(new ApiResponse<>(t));
            }
        });
    }

    public void setUserId(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setUserId(requestJson));
        }
    }

    public void setTimezone(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setTimezone(mUid, requestJson));
        }
    }

    public void setProvisionToken(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setProvisionToken(mUid, requestJson));
        }
    }

    public void getDeviceBoardId(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.getDeviceBoardId(mUid));
        }
    }

    public void setDeviceOperationMode(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setDeviceOperationMode(mUid, requestJson));
        }
    }

    public void set5gApClientWifiSsid(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set5gApClientWifiSsid(mUid, requestJson));
        }
    }

    public void set5gApClientWifiPassword(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set5gApClientWifiPassword(mUid, requestJson));
        }
    }

    public void set5gApClientWifiSecurity(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set5gApClientWifiSecurity(mUid, requestJson));
        }
    }

    public void setApClientWifiAction(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.setApClientWifiAction(mUid));
        }
    }

    public void get5gApClientConnectionStatus(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.get5gApClientConnectionStatus(mUid, "5"));
        }
    }

    public void detectWanType(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.detectWanType(mUid));
        }
    }

    public void getWanType(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.getWanType(mUid));
        }
    }

    public void setWanDhcp(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.setWanDhcp(mUid));
        }
    }

    public void setPppoe(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setPppoe(mUid, requestJson));
        }
    }

    public void setStaticIp(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setStaticIp(mUid, requestJson));
        }
    }

    public void getInternetConnectionStatus(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.getInternetConnectionStatus(mUid));
        }
    }

    public void getDeviceTokenStatus(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.getDeviceTokenStatus(mUid));
        }
    }

    public void setLocation(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setLocation(mUid, requestJson));
        }
    }

    public void set2gWifiSsid(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set2gWifiSsid(mUid, requestJson));
        }
    }

    public void set2gWifiPassword(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set2gWifiPassword(mUid, requestJson));
        }
    }

    public void set5gWifiSsid(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set5gWifiSsid(mUid, requestJson));
        }
    }

    public void set5gWifiPassword(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.set5gWifiPassword(mUid, requestJson));
        }
    }

    public void setBleEnable(final ApiCallback apiCallback, JsonObject requestJson) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            LogUtils.trace("request json: " + requestJson.toString());
            sendRequest(apiCallback, mFirmwareApi.setBleEnable(mUid, requestJson));
        }
    }

    public void setupComplete(final ApiCallback apiCallback) {
        if (!isInternetAvailable()) {
            apiCallback.onInternetUnavailable();
        } else {
            apiCallback.onSendRequest();
            sendRequest(apiCallback, mFirmwareApi.setupComplete(mUid));
        }
    }
}
