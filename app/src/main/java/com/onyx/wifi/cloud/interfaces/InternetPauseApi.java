package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonObject;
import com.onyx.wifi.utility.AppConstants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface InternetPauseApi {

    String HEADER_NAME = AppConstants.CLOUD_API_HEADER_NAME;

    @POST("user/action/pause/client")
    Call<JsonObject> pauseClient(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/pause/member")
    Call<JsonObject> pauseMember(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/pause/profile")//@POST("user/action/action/pause/profile")
    Call<JsonObject> pauseGlobal(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/pause/home_network")
    Call<JsonObject> pauseHomeNetwork(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/pause/guest_network")
    Call<JsonObject> pauseGuestNetwork(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/action/pause/bnb_network")
    Call<JsonObject> pauseBnBGuestNetwork(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @GET("user/pause/summary")
    Call<JsonObject> getGlobalPauseList(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId);

    @POST("user/pause/profile")
    Call<JsonObject> createGlobalPauseProfile(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PUT("user/pause/profile")
    Call<JsonObject> updateGlobalPauseProfile(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/pause/profile")
    Call<JsonObject> removeGlobalPauseProfile(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("profile_id") String profileId);

}
