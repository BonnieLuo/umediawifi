package com.onyx.wifi.cloud.interfaces;

import com.google.gson.JsonObject;
import com.onyx.wifi.utility.AppConstants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface MemberApi {

    String HEADER_NAME = AppConstants.CLOUD_API_HEADER_NAME;

    @GET("user/members")
    Call<JsonObject> getMemberList(@Header(HEADER_NAME) String authKey, @Query("nid") String nodesetId);

    @PUT("user/member/client")
    Call<JsonObject> updateMember(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/member")
    Call<JsonObject> removeMember(@Header(HEADER_NAME) String authKey, @Query("nid") String nodesetId, @Query("mid") String memberId);

    @POST("user/member/client")
    Call<JsonObject> createMember(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PATCH("user/member/client")
    Call<JsonObject> assignClientToMember(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/member/client")
    Call<JsonObject> removeClientFromMember(@Header(HEADER_NAME) String authKey, @Query("nid") String nodesetId, @Query("member_id") String memberId, @Query("cid") String cid);

    @PUT("user/client")
    Call<JsonObject> updateClientSetting(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @POST("user/client/remove")//@DELETE("user/client")
    Call<JsonObject> removeClients(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    // block client
    @POST("user/client/disallow")//@DELETE("user/client")
    Call<JsonObject> blockClient(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    //@DELETE("user/client/disallow")//@DELETE("user/client")
    @HTTP(method = "DELETE", path = "user/client/disallow", hasBody = true)
    Call<JsonObject> unblockClient(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);//@Query("nid") String nodesetId, @Query("cid") String cid);

    // Content Filters
    @GET("user/security/website")
    Call<JsonObject> getContentFiltersList(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("member_id") String memberId);

    @POST("user/security/website")
    Call<JsonObject> createContentFilters(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PATCH("user/security/website")
    Call<JsonObject> addContentFilters(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PATCH("user/security/website")
    Call<JsonObject> removeContentFilters(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/security/website")
    Call<JsonObject> cleanContentFilters(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("member_id") String memberId);

    // Internet Schedule
    @GET("user/member/schedule")
    Call<JsonObject> getMemberScheduleList(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("member_id") String memberId);

    @POST("user/member/schedule")
    Call<JsonObject> createMemberSchedule(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @PUT("user/member/schedule")
    Call<JsonObject> updateMemberSchedule(@Header(HEADER_NAME) String authKey, @Body JsonObject requestJson);

    @DELETE("user/member/schedule")
    Call<JsonObject> removeMemberSchedule(@Header(HEADER_NAME) String authKey, @Query("nodeset_id") String nodesetId, @Query("member_id") String memberId, @Query("schedule_id") String scheduleId);

}
