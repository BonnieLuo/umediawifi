package com.onyx.wifi.viewmodel.mainmenu;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.NodeSpeedTestInfo;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class TroubleshootingViewModel extends BaseViewModel {

    private ArrayList<NodeSpeedTestInfo> mNodeSpeedTestList = new ArrayList<NodeSpeedTestInfo>();
    public TroubleshootingViewModel(@NonNull Application application) {
        super(application);
    }

    public ArrayList<NodeSpeedTestInfo> getNodeSpeedTestList() {
        return mNodeSpeedTestList;
    }

    public void startSpeedTest() {
        mActionCode = Code.Action.Troubleshooting.START_SPEED_TEST;
        mCloudManager.startSpeedTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    String status = new JsonParserWrapper().jsonGetString((JsonObject) apiResponse.body, AppConstants.KEY_STATUS);
                    if (status.equals(AppConstants.VALUE_IN_PROGRESS)) {
                        mDataManager.updateSpeedTestInProgress(mDataManager.getControllerDid());
                    }
                }
                handleDeviceResponse(apiResponse);
            }
        });
    }

    public void getNodeList() {
        mActionCode = Code.Action.Troubleshooting.GET_NODE_LIST;
        mCloudManager.getNodeList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    handleNodeListData((JsonObject) apiResponse.body);
                }
                handleResponse(apiResponse);
            }
        });
    }

    private void handleNodeListData(JsonObject apiResponseBody) {
        if (apiResponseBody != null) {
            JsonArray dataJson = apiResponseBody.getAsJsonArray(AppConstants.KEY_DATA);
            Type listType = new TypeToken<ArrayList<DeviceInfo>>() {
            }.getType();
            ArrayList<DeviceInfo> nodeList = new JsonParserWrapper().jsonToObject(dataJson.toString(), listType);
            mDataManager.setNodeList(nodeList);
        }

        // TODO: 以下是暫時寫的code
        ArrayList<DeviceInfo> nodeList = mDataManager.getNodeList();

        mNodeSpeedTestList.clear();
        for (DeviceInfo deviceInfo: nodeList) {
            NodeSpeedTestInfo nodeSpeedTestInfo = new NodeSpeedTestInfo();
            nodeSpeedTestInfo.setDeviceInfo(deviceInfo);
            mNodeSpeedTestList.add(nodeSpeedTestInfo);
        }
    }
}
