package com.onyx.wifi.viewmodel.item;

public class Code {

    public static final int NONE = -1;

    public static class Action {
        public static class UserAccount {
            public static final int SIGN_UP = 1000;
            public static final int SIGN_IN = 1001;
            public static final int SOCIAL_SIGN_IN = 1002;
            public static final int VERIFY_EMAIL_VERIFY_CODE = 1003;
            public static final int VERIFY_EMAIL_RESEND_CODE = 1004;
            public static final int FORGOT_PASSWORD_SEND_CODE = 1005;
            public static final int FORGOT_PASSWORD_RESEND_CODE = 1006;
            public static final int FORGOT_PASSWORD_VERIFY_RESET_PASSWORD = 1007;
            public static final int GET_USER_INFO = 1008;
            public static final int UPDATE_USER_INFO = 1009;
            public static final int UPDATE_PASSWORD = 1010;
            public static final int UPDATE_EMAIL = 1011;
            public static final int VERIFY_UPDATE_EMAIL = 1012;
            public static final int DELETE_USER = 1013;
            public static final int GET_ALL_CLIENTS = 1014;
        }

        public static class DeviceSetup {
            public static final int MORE_ABOUT_MODEM = 1100;
            public static final int UNPLUG_MODEM = 1101;
            public static final int CONNECT_TO_MODEM = 1102;
            public static final int PLUG_IN_MODEM = 1103;
            public static final int WAIT_FOR_LED = 1104;
            public static final int FIND_THE_BUTTON = 1105;
            public static final int FIND_THE_BUTTON_GOT_IT = 1106;
            public static final int GET_PROVISION_TOKEN = 1107;
            public static final int SEARCH_DEVICE = 1108;
            public static final int ROUTER_FOUND = 1109;
            public static final int DEVICE_NOT_FOUND = 1110;
            public static final int MULTIPLE_DEVICE_FOUND = 1111;
            public static final int ALTERNATIVE_SETUP = 1112;
            public static final int MORE_HELP = 1113;
            public static final int RETRY = 1114;
            public static final int HELP_PRODUCT_CODE = 1115;
            public static final int SETUP_ROUTER = 1116;
            public static final int CONNECT_DEVICE = 1117;
            public static final int CHECK_CABLE = 1118;
            public static final int CHECK_CABLE_RETRY = 1119;
            public static final int CHANGE_CONNECTION_TYPE = 1120;
            public static final int SELECT_PPPOE = 1121;
            public static final int SELECT_STATIC_IP = 1122;
            public static final int SET_DHCP = 1123;
            public static final int SET_PPPOE = 1124;
            public static final int SET_STATIC_IP = 1125;
            public static final int SETUP_LOCATION = 1126;
            public static final int SETUP_WIFI = 1127;
            public static final int CREATE_NETWORK = 1128;
            public static final int GET_PROVISION_STATE = 1129;
            public static final int PROVISION_SUCCESS = 1130;
            public static final int ADD_ANOTHER_DEVICE = 1131;
            public static final int FINISH_SETUP = 1132;
            public static final int CHECK_SETUP_WIFI = 1133;
            public static final int GET_WIRELESS_SETTING = 1134;
            public static final int REPEATER_FOUND = 1135;
            public static final int SETUP_REPEATER = 1136;
            public static final int PLACEMENT_ASSISTANT = 1137;
        }

        public static class DeviceFirmwareCommand {
            public static final int SET_USER_ID = 1200;
            public static final int SET_TIMEZONE = 1201;
            public static final int SET_PROVISION_TOKEN = 1202;
            public static final int GET_DEVICE_BOARD_ID = 1203;
            public static final int SET_DEVICE_OPERATION_MODE = 1204;
            public static final int EXEC_AP_SITE_SURVEY = 1205;
            public static final int GET_AP_LIST = 1206;
            public static final int SET_2G_AP_CLIENT_WIFI = 1207;
            public static final int SET_2G_AP_CLIENT_WIFI_KEY = 1208;
            public static final int SET_2G_AP_CLIENT_WIFI_SEC = 1209;
            public static final int SET_5G_AP_CLIENT_WIFI = 1210;
            public static final int SET_5G_AP_CLIENT_WIFI_KEY = 1211;
            public static final int SET_5G_AP_CLIENT_WIFI_SEC = 1212;
            public static final int SET_AP_CLIENT_WIFI_ACTION = 1213;
            public static final int GET_2G_AP_CLIENT_CONN_STATUS = 1214;
            public static final int GET_5G_AP_CLIENT_CONN_STATUS = 1215;
            public static final int DETECT_WAN_TYPE = 1216;
            public static final int GET_WAN_TYPE = 1217;
            public static final int SET_WAN_DHCP = 1218;
            public static final int SET_WAN_PPPOE = 1219;
            public static final int SET_WAN_STATIC_IP = 1220;
            public static final int GET_INTERNET_CONN_STATUS = 1221;
            public static final int GET_DEVICE_TOKEN_STATUS = 1222;
            public static final int SET_LOCATION = 1223;
            public static final int SET_2G_WIFI = 1224;
            public static final int SET_2G_WIFI_KEY = 1225;
            public static final int SET_2G_WIFI_SEC = 1226;
            public static final int SET_5G_WIFI = 1227;
            public static final int SET_5G_WIFI_KEY = 1228;
            public static final int SET_5G_WIFI_SEC = 1229;
            public static final int SETUP_COMPLETE = 1230;
            public static final int SET_BLE_ENABLE = 1231;
            public static final int GET_DEVICE_MAC = 1232;
            public static final int SET_ADMIN_PASSWORD = 1233;
        }

        public static class NetworkSetting {
            public static final int GET_FIRMWARE_STATUS = 1300;
            public static final int FIRMWARE_UPDATE_ALL = 1301;
            public static final int GET_CLIENT_INFO_LIST = 1302;
            public static final int POST_HOME_NETWORK_SETTING = 1303;
            public static final int GET_DASHBOARD_INFO = 1304;
            public static final int POST_UPTIME = 1305;
            public static final int PUT_INTERNET_PRIORITY = 1306;
        }

        public static class VoiceAssistant {
            public static final int SET_MUTE_UNMUTE = 1400;
            public static final int SET_UNREGISTER = 1401;
            public static final int GET_AUDIO_DEVICE_INFO = 1402;
            public static final int GET_AUDIO_DEVICE_INFO_LIST = 1403;
            public static final int CHECK_IF_HOME_NETWORK = 1404;
        }

        public static class Troubleshooting {
            public static final int START_SPEED_TEST = 1500;
            public static final int GET_NODE_LIST = 1501;
            public static final int GET_SPEED_TEST_INFO = 1502;
        }

        public static class Dashboard {
            public static final int GET_DASHBOARD_INFO = 1600;
            public static final int GET_WIRELESS_SETTING = 1601;
            public static final int GET_SPEED_TEST = 1602;
            public static final int GET_GLOBAL_PAUSE_LIST = 1603;
            public static final int START_SPEED_TEST = 1604;
            public static final int DISABLE_GLOBAL_PAUSE = 1605;
            public static final int GET_NODE_LIST = 1606;
            public static final int GET_NETWORK_MAP = 1607;
            public static final int GET_NODE_LIST_FOR_CONN_TEST = 1608;
        }

        public static class WirelessSetting {
            public static final int GET_WIRELESS_SETTING = 1700;
            public static final int GET_GUEST_NETWORK_SETTING = 1701;
            public static final int GET_BNB_NETWORK_SETTING = 1702;
            public static final int UPDATE_WIRELESS_SETTING = 1703;
            public static final int UPDATE_GUEST_NETWORK_SETTING = 1704;
            public static final int UPDATE_BNB_NETWORK_SETTING = 1705;
        }

        public static class Member {
            public static final int GET_MEMBER_LIST = 1800;
            public static final int CREATE_FAMILY_MEMBER = 1801;
            public static final int UPDATE_FAMILY_MEMBER = 1802;
            public static final int REMOVE_FAMILY_MEMBER = 1803;
            public static final int ASSIGN_CLIENT_TO_MEMBER = 1804;
            public static final int REMOVE_CLIENT_FROM_MEMBER = 1805;
            public static final int UPDATE_CLIENT_SETTINGS = 1806;
            public static final int REMOVE_CLIENT = 1807;
            public static final int GET_MEMBER_SCHEDULE_LIST = 1808;
            public static final int CREATE_MEMBER_SCHEDULE = 1809;
            public static final int UPDATE_MEMBER_SCHEDULE = 1810;
            public static final int REMOVE_MEMBER_SCHEDULE = 1811;
            public static final int GET_MEMBER_BLOCKING_LIST = 1812;
            public static final int CREATE_MEMBER_WEBSITE_BLOCKING = 1813;
            public static final int ADD_MEMBER_WEBSITE_BLOCKING = 1814;
            public static final int REMOVE_MEMBER_WEBSITE_BLOCKING = 1815;
            public static final int CLEAN_MEMBER_WEBSITE_BLOCKING = 1816;
            public static final int PAUSE_MEMBER = 1817;
            public static final int RESUME_MEMBER = 1818;
            public static final int PAUSE_CLIENT = 1819;
            public static final int RESUME_CLIENT = 1820;
            public static final int BLOCK_CLIENT = 1821;
            public static final int UNBLOCK_CLIENT = 1822;
        }

        public static class Pause {
            public static final int GET_GLOBAL_PAUSE_LIST = 1900;
            public static final int CREATE_GLOBAL_PAUSE_PROFILE = 1901;
            public static final int UPDATE_GLOBAL_PAUSE_PROFILE = 1902;
            public static final int REMOVE_GLOBAL_PAUSE_PROFILE = 1903;
            public static final int ENABLE_GLOBAL_PAUSE = 1904;
            public static final int DISABLE_GLOBAL_PAUSE = 1905;
            public static final int GET_PAUSE_STATUS = 1906;
        }

        // 20xx for Advanced Network Setting
        public static class AdvanceWirelessSettings {
            public static final int GET_SETTINGS = 2000;
            public static final int SET_SSID = 2001;
            public static final int SET_DMZ = 2002;
            public static final int UPDATE_PORT_FORWARDING = 2003;
            public static final int REMOVE_PORT_FORWARDING = 2004;
            public static final int UPDATE_PORT_TRIGGERING = 2005;
            public static final int REMOVE_PORT_TRIGGERING = 2006;
            public static final int SET_SCHEDULED_REBOOT = 2007;
            public static final int REMOVE_ALL_NODES = 2008;
        }

        public static class NodeSetting {
            public static final int GET_NODE_INFO = 2100;
            public static final int GET_SPEED_TEST = 2101;
            public static final int GET_CLIENT_LIST = 2102;
            public static final int UPDATE_DEVICE_LABEL = 2103;
            public static final int REMOVE_DEVICE = 2104;
            public static final int REBOOT_DEVICE = 2105;
        }
    }

    public static class Error {
        public static class DeviceSetup {
            // BLE
            public static final String STATUS_615 = "615";  // cable unplugged
            public static final String STATUS_631 = "631";  // signal too weak, can not connect to ap
            public static final String STATUS_632 = "632";  // incorrect key, can not connect to ap
            public static final String STATUS_641 = "641";  // unconnected
            public static final String STATUS_651 = "651";  // incorrect pwd or acct, can not connect to ISP
            public static final String STATUS_652 = "652";  // exceed connection limit, can not connect to ISP
            public static final String STATUS_653 = "653";  // timeout, can not connect to ISP
            public static final String STATUS_654 = "654";  // no modem, can not connect to ISP
            public static final String STATUS_661 = "661";  // can not get ip
            public static final String STATUS_671 = "671";  // no provision token
            public static final String STATUS_672 = "672";  // not get device token
            public static final String STATUS_691 = "691";  // station no ssid

            // WIFI
            public static final String STATUS_715 = "715";  // cable unplugged
            public static final String STATUS_731 = "731";  // signal too weak, can not connect to ap
            public static final String STATUS_732 = "732";  // incorrect key, can not connect to ap
            public static final String STATUS_741 = "741";  // unconnected
            public static final String STATUS_751 = "751";  // incorrect pwd or acct, can not connect to ISP
            public static final String STATUS_752 = "752";  // exceed connection limit, can not connect to ISP
            public static final String STATUS_753 = "753";  // timeout, can not connect to ISP
            public static final String STATUS_754 = "754";  // no modem, can not connect to ISP
            public static final String STATUS_761 = "761";  // can not get ip
            public static final String STATUS_771 = "771";  // no provision token
            public static final String STATUS_772 = "772";  // not get device token
            public static final String STATUS_791 = "791";  // station no ssid
        }
    }
}
