package com.onyx.wifi.viewmodel.interfaces.setup;

import android.widget.TextView;

public interface DeviceSetupListener {
    void onContinue(int nextStep);
    void setSupportView(TextView textView);
}
