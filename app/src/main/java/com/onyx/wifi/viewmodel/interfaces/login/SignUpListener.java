package com.onyx.wifi.viewmodel.interfaces.login;

public interface SignUpListener {
    void onBack();
    void onAccountDataValid();
    void onPasswordDataValid();
}

