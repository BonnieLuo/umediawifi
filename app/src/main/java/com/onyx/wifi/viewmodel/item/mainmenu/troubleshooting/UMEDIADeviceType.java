package com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting;

public enum UMEDIADeviceType {
    UMEDIA_DEVICE_PLUG,
    UMEDIA_DEVICE_DESKTOP,
    UMEDIA_DEVICE_TOWER //audio (3)
}
