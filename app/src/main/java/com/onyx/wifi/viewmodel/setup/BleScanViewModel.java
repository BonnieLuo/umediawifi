package com.onyx.wifi.viewmodel.setup;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.model.BleScanModel;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.interfaces.BleScanListener;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class BleScanViewModel extends BaseViewModel {

    private BleScanModel mBleScanModel;

    private ArrayList<SetupDevice> mDeviceList;

    private int mSelectedIndex = -1;

    private AppConstants.SetupMode mSetupMode;

    public BleScanViewModel(@NonNull Application application) {
        super(application);

        mBleScanModel = new BleScanModel(mContext, mBleScanListener);

        mSetupMode = DataManager.getInstance().getSetupMode();
    }

    private BleScanListener mBleScanListener = new BleScanListener() {
        @Override
        public void getDevice(SetupDevice setupDevice) {
            String deviceName = setupDevice.getName();
            if (!StringUtils.isNullOrEmpty(deviceName) && isTargetDevice(deviceName)) {
                if (deviceName.length() == 20) {
                    // example device name: "UMEDIA_YYYYYYYY_XXXX", XXXX 是 MAC address 最後四碼, Y 是 model number
                    // model number: 8
                    // digits length, “0” to fill up empty digit.
                    String[] deviceNameTokens = deviceName.split("_");
                    if (deviceNameTokens[1].equals("SWHA42C0")) {
                        setupDevice.setDeviceType(AppConstants.DeviceType.TOWER);
                        setupDevice.setModelNumber("SWHA42C");
                        setupDevice.setName("UMEDIA_Setup_" + deviceNameTokens[2]);
                    } else {
                        setupDevice.setDeviceType(AppConstants.DeviceType.TOWER);
                        setupDevice.setModelNumber("SWHA42C");
                        setupDevice.setName("UMEDIA_Setup_" + deviceNameTokens[2]);
                    }
                }

                // 根據 setup mode 來判斷是否要將掃描到的 device 加入 list (在 list 內的 device 最後會顯示給使用者看)
                if (mSetupMode == AppConstants.SetupMode.ROUTER) {
                    addToDeviceList(setupDevice);
                } else if (mSetupMode == AppConstants.SetupMode.REPEATER) {
                    // 當要新增 repeater 時, 若掃描到的 device 是使用者已經 provision 完成的 router, 要過濾掉
                    if (!isRouterDevice(setupDevice)) {
                        addToDeviceList(setupDevice);
                    }
                } else if (mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
                    // 當要 resetup router 時, 過濾掃描到的 device, 目標 device 只能是 router
                    if (isRouterDevice(setupDevice)) {
                        addToDeviceList(setupDevice);
                    }
                }
            }
        }
    };

    public boolean isScanning() {
        return mBleScanModel.isScanning();
    }

    public void startScan() {
        if (mDeviceList == null) {
            mDeviceList = new ArrayList<>();
        } else {
            mDeviceList.clear();
        }
        mBleScanModel.startScan();
    }

    public void stopScan() {
        mBleScanModel.stopScan();
    }

    public ArrayList<SetupDevice> getDeviceList() {
        return mDeviceList;
    }

    public SetupDevice getTargetDevice() {
        if (mSelectedIndex < 0 || mSelectedIndex > (mDeviceList.size() - 1)) {
            return null;
        }
        return mDeviceList.get(mSelectedIndex);
    }

    public void setSelectedIndex(int selectedIndex) {
        mSelectedIndex = selectedIndex;
    }

    private boolean isTargetDevice(String deviceName) {
        boolean isTargetDevice = false;
        if (deviceName != null && deviceName.contains("UMEDIA_")) {
            isTargetDevice = true;
        }
        return isTargetDevice;
    }

    private void addToDeviceList(SetupDevice setupDevice) {
        Iterator<SetupDevice> iterator = mDeviceList.iterator();
        while (iterator.hasNext()) {
            SetupDevice existDevice = iterator.next();
            if (setupDevice.getMacAddress().equals(existDevice.getMacAddress())
                    && setupDevice.getName().equals(existDevice.getName())) {
                // 已在 list 內的裝置不用再新增一次
                return;
            }
        }
        LogUtils.trace("add BLE device: " + setupDevice.toString());
        mDeviceList.add(setupDevice);
        sortDeviceList(mDeviceList);
    }

    private void sortDeviceList(ArrayList<SetupDevice> setupDeviceList) {
        Comparator<SetupDevice> comparator = new Comparator<SetupDevice>() {
            @Override
            public int compare(SetupDevice device1, SetupDevice device2) {
                int rssi1 = device1.getRssi();
                int rssi2 = device2.getRssi();
                if (rssi1 < rssi2) {
                    return -1;
                } else if (rssi1 > rssi2) {
                    return 1;
                }
                return 0;   // rssi1 equals rssi2
            }
        };
        Collections.sort(setupDeviceList, comparator);
    }

    // 比對掃描到的裝置和 router 的 device MAC 末四碼, 判斷掃描到的裝置是不是 router
    private boolean isRouterDevice(SetupDevice setupDevice) {
        NetworkSetting networkSetting = DataManager.getInstance().getNetworkSetting();
        if (networkSetting != null) {
            String routerDeviceMac = networkSetting.getDevMac();
            LogUtils.trace("router device MAC: " + routerDeviceMac);
            String[] routerDeviceMacTokens = routerDeviceMac.split(":");
            int tokenLength = routerDeviceMacTokens.length;
            String routerMacLastTwo = "";
            if (tokenLength > 2) {
                routerMacLastTwo = routerDeviceMacTokens[tokenLength - 2] + routerDeviceMacTokens[tokenLength - 1];
            }
            LogUtils.trace("router device MAC last two fields: " + routerMacLastTwo);
            String setupDeviceName = setupDevice.getName();
            String setupDeviceMacLastTwo = setupDeviceName.substring(setupDeviceName.length() - 4);
            LogUtils.trace("setup device MAC last two fields: " + setupDeviceMacLastTwo);
            boolean isRouterDevice = setupDeviceMacLastTwo.equals(routerMacLastTwo);
            LogUtils.trace("the scanned device is router? " + isRouterDevice);
            return isRouterDevice;
        }
        return false;
    }
}
