package com.onyx.wifi.viewmodel.item;

import com.google.gson.GsonBuilder;

public class ResultStatus<T> {
    public int actionCode;
    public boolean success;
    public String errorMsg;
    public int serverErrorCode;
    public T data;

    public ResultStatus() {
        actionCode = Code.NONE;
        success = false;
        errorMsg = "";
        serverErrorCode = -1;
        data = null;
    }

    public int getActionCode() {
        return actionCode;
    }

    public void setActionCode(int actionCode) {
        this.actionCode = actionCode;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public int getServerErrorCode() {
        return serverErrorCode;
    }

    public void setServerErrorCode(int serverErrorCode) {
        this.serverErrorCode = serverErrorCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
