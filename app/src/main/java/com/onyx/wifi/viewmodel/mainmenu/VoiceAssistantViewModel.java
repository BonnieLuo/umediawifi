package com.onyx.wifi.viewmodel.mainmenu;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.FirmwareManager;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.AudioDevice;
import com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance.DeviceInfo;

import java.util.ArrayList;

public class VoiceAssistantViewModel extends BaseViewModel {

    private MutableLiveData<DeviceInfo> mNXPDeviceInfo; // got from nxp FW for onboarding
    private ArrayList<AudioDevice> mAudioDeviceList = new ArrayList<AudioDevice>(); // got from cloud for listing audio devices
    private ArrayList<AudioDevice> mTmpAudioDeviceList = new ArrayList<>();
    private JsonParserWrapper mJsonParserWrapper = null;
    private boolean mIsListTheSame = false;

    public VoiceAssistantViewModel(@NonNull Application application) {
        super(application);

        DeviceInfo deviceInfo = new DeviceInfo("", "", "", "", "", "", "");
        mNXPDeviceInfo = new MutableLiveData<>();
        mNXPDeviceInfo.setValue(deviceInfo);
        mJsonParserWrapper = new JsonParserWrapper();
    }

    public boolean isListTheSame() {
        return mIsListTheSame;
    }

    public MutableLiveData<DeviceInfo> getNXPDeviceInfo() {
        return mNXPDeviceInfo;
    }

    public ArrayList<AudioDevice> getmAudioDeviceList() {
        return mAudioDeviceList;
    }

    public void setMuteUnMute(final String command, final String mute, final String did) {
        mActionCode = Code.Action.VoiceAssistant.SET_MUTE_UNMUTE;
        mCloudManager.setMuteUnMute(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    LogUtils.trace("Bonnie_mute", "setMuteUnMute -- Success" + " " + command + " " + mute);
                }
                handleDeviceResponse(apiResponse);
            }
        }, command, mute, did);
    }

    public void setUnRegister(final String command, final String did) {
        mActionCode = Code.Action.VoiceAssistant.SET_UNREGISTER;
        mCloudManager.setUnRegister(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                LogUtils.trace("Bonnie_unregister", "[setUnRegister]" + " " + command);
                if (apiResponse.httpStatusCode == 200) {
                    LogUtils.trace("Bonnie_unregister", "setUnRegister -- Success" + " " + command);
                }
                handleDeviceResponse(apiResponse);
            }
        }, command, did);
    }

    public void getAudioDeviceInfoList() {
        LogUtils.trace("AudioList", "getAudioDeviceInfoList");
        mTmpAudioDeviceList.clear();
        mTmpAudioDeviceList.addAll(mAudioDeviceList);
        mActionCode = Code.Action.VoiceAssistant.GET_AUDIO_DEVICE_INFO_LIST;
        mCloudManager.getAudioDeviceInfoList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                LogUtils.trace("AudioList", "onInternetUnavailable");
                //handleInternetUnavailableError(); //<--在background thread處理，裡面有呼叫setValue，會當掉
            }

            @Override
            public void onSendRequest() {
                LogUtils.trace("AudioList", "onSendRequest");
                //mIsLoading.postValue(false);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                LogUtils.trace("AudioList", "onReceiveResponse");

                if (apiResponse.httpStatusCode == 200) {
                    /* 註冊完成後會開啟 email 認證頁面, 進入 email 認證程序.
                       而 email 認證程序相關的 cloud API 會需要用到 uid 和 email 兩項資訊, 故先儲存起來. */

                    mAudioDeviceList.clear();

                    JsonArray audioDeviceInfo = mJsonParserWrapper.jsonGetJsonArray((JsonObject) apiResponse.body, AppConstants.KEY_DATA);
                    LogUtils.trace("AudioList", "getAudioDeviceInfoList -- Success" + " " + audioDeviceInfo.size());
                    LogUtils.trace("AudioList", "(got from cloud: (original packet) List size:" + " " + audioDeviceInfo.size());
                    //for (int i = 0; i < audioDeviceInfo.size(); i++) {
                    int i = 0;
                    for (JsonElement value : audioDeviceInfo) {

                        JsonObject jsonObject = value.getAsJsonObject();
                        // 是audio device
                        if (mJsonParserWrapper.jsonGetString(jsonObject, "device_type").equals("3")) {

                            // parse
                            AudioDevice audioDevice = new AudioDevice(
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    false,
                                    "",
                                    false,
                                    false);

                            audioDevice.setUdid(mJsonParserWrapper.jsonGetString(jsonObject, "udid"));
                            audioDevice.setDid(mJsonParserWrapper.jsonGetString(jsonObject, "did"));
                            String hexString = mJsonParserWrapper.jsonGetString(jsonObject, "device_label");
                            audioDevice.setLocation(new String(StringUtils.hexString2Bytes(hexString)));
                            audioDevice.setType(mJsonParserWrapper.jsonGetString(jsonObject, "device_type"));
                            audioDevice.setName(mJsonParserWrapper.jsonGetString(jsonObject, "device_name"));
                            audioDevice.setSN(mJsonParserWrapper.jsonGetString(jsonObject, "audio_sn"));
                            audioDevice.setFWVersion(mJsonParserWrapper.jsonGetString(jsonObject, "audio_fw"));
                            audioDevice.setMac(mJsonParserWrapper.jsonGetString(jsonObject, "audio_mac"));
                            audioDevice.setIP(mJsonParserWrapper.jsonGetString(jsonObject, "audio_ip"));
                            audioDevice.setDeviceIP(mJsonParserWrapper.jsonGetString(jsonObject, "device_ip"));

                            if ((mJsonParserWrapper.jsonGetString(jsonObject, "audio_mute")).equals("1"))
                                audioDevice.setIsMute(true);
                            else
                                audioDevice.setIsMute(false);
                            audioDevice.setLang(mJsonParserWrapper.jsonGetString(jsonObject, "audio_lang"));

                            if ((mJsonParserWrapper.jsonGetString(jsonObject, "audio_registered")).equals("1"))
                                audioDevice.setIsAlexaConnect(true);
                            else
                                audioDevice.setIsAlexaConnect(false);
                            if ((mJsonParserWrapper.jsonGetString(jsonObject, "audio_status")).equals("1")) {
                                // if mac is empty => offline
                                if (audioDevice.getCid().equals("")) {
                                    audioDevice.setIsDeviceConnect(false);
                                } else {
                                    audioDevice.setIsDeviceConnect(true);
                                }
                            } else {
                                audioDevice.setIsDeviceConnect(false);
                            }
                            mAudioDeviceList.add(audioDevice);

                            LogUtils.trace("AudioList", "index: " + "(" + i++ + ")" + audioDevice.toString());
                            LogUtils.trace("AudioList", "(got from cloud) list size: " + mAudioDeviceList.size());
                        }
                    }
                    //}
                    // 塞假資料
                    /*mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365",
                            "3",
                            "ART2200A",//
                            "m5vjY9IJJQk",
                            "1.0.12",//
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            false,
                            "",
                            false,
                            false));                    // offline v


                    mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365",
                            "3",
                            "ART2200A",
                            "m5vjY9IJJQk",
                            "1.0.12",
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            true,
                            "",
                            true,
                            true));                    // onlineOnboardIsMute v

                    mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365dddddddd",
                            "3",
                            "ART2200A",
                            "m5vjY9IJJQk",
                            "1.0.12",
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            true,
                            "",
                            true,
                            false));                    // offline v
                    mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365",
                            "3",
                            "ART2200A",//
                            "m5vjY9IJJQk",
                            "1.0.12",//
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            false,
                            "",
                            true,
                            true));                    // onlineOnboardNotMute

                    mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365",
                            "3",
                            "ART2200A",
                            "m5vjY9IJJQk",
                            "1.0.12",
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            true,
                            "",
                            false,
                            true));                    // onlineNotOnboard v
                    mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365ddddddddttttttttttttttttttttttttttttttttttttt",
                            "3",
                            "ART2200A",//
                            "m5vjY9IJJQk",
                            "1.0.12",//
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            false,
                            "",
                            false,
                            false));                    // offline v

                    mAudioDeviceList.add(new AudioDevice(
                            "bc329e00-1dd8-11b2-8601-0011e004c7b7",
                            "4f6666696365",
                            "3",
                            "ART2200A",
                            "m5vjY9IJJQk",
                            "1.0.12",
                            "00:04:9F:05:1C:81",
                            "192.168.1.138",
                            "",
                            true,
                            "",
                            true,
                            true));       */             // onlineOnboardIsMute v

                    mIsListTheSame = compareAudioDevice(mTmpAudioDeviceList, mAudioDeviceList);

                    sortAudioList();

                }
                LogUtils.trace("AudioList", "here");

                //handleResponse(apiResponse);
                if (apiResponse.httpStatusCode == 401) {
                    // handleUnauthorized(apiResponse);
                    LogUtils.trace("AudioList", "401");
                    return;
                }

                ResultStatus resultStatus = new ResultStatus();
                resultStatus.actionCode = mActionCode;
                if (apiResponse.httpStatusCode == 200) {
                    resultStatus.success = true;
                    resultStatus.data = apiResponse.body;
                } else {
                    resultStatus.success = false;
                    resultStatus.errorMsg = apiResponse.errorMsg;
                }
                // 設置 API 回傳的結果讓 UI 響應
                mResultStatus.setValue(resultStatus);
            }
        });
    }

    private void sortAudioList() {
        ArrayList<AudioDevice> onLineOnBoardNotMute = new ArrayList<AudioDevice>();
        ArrayList<AudioDevice> onLineOnBoardIsMute = new ArrayList<AudioDevice>();
        ArrayList<AudioDevice> onLineNotOnBoard = new ArrayList<AudioDevice>();
        ArrayList<AudioDevice> offLineNotOnBoard = new ArrayList<AudioDevice>();

        for (AudioDevice audioDevice : mAudioDeviceList) {
            if (audioDevice.getIsDeviceConnect()) {
                if (audioDevice.getIsAlexaConnect()) {
                    if (!audioDevice.getIsMute()) {
                        onLineOnBoardNotMute.add(audioDevice);
                    } else {
                        onLineOnBoardIsMute.add(audioDevice);
                    }

                } else {
                    onLineNotOnBoard.add(audioDevice);
                }
            } else {
                offLineNotOnBoard.add(audioDevice);
            }
        }

        mAudioDeviceList.clear();
        if (onLineOnBoardNotMute.size() > 0) {
            mAudioDeviceList.addAll(onLineOnBoardNotMute);
        }
        if (onLineOnBoardIsMute.size() > 0) {
            mAudioDeviceList.addAll(onLineOnBoardIsMute);
        }
        if (onLineNotOnBoard.size() > 0) {
            mAudioDeviceList.addAll(onLineNotOnBoard);
        }
        if (offLineNotOnBoard.size() > 0) {
            mAudioDeviceList.addAll(offLineNotOnBoard);
        }

    }

    public boolean compareAudioDevice(ArrayList<AudioDevice> audioDeviceList1, ArrayList<AudioDevice> audioDeviceList2) {

        if (audioDeviceList1 == null || audioDeviceList2 == null) {
            if (audioDeviceList1 == null && audioDeviceList2 == null)
                return true;
            return false;
        }

        if (audioDeviceList1.size() != audioDeviceList2.size())
            return false;

        for (int i = 0; i < audioDeviceList1.size(); ++i) {
            if (!audioDeviceList1.get(i).getDid().equals(audioDeviceList2.get(i).getDid()) ||
                    !audioDeviceList1.get(i).getLocation().equals(audioDeviceList2.get(i).getLocation()) ||
                    !audioDeviceList1.get(i).getType().equals(audioDeviceList2.get(i).getType()) ||
                    !audioDeviceList1.get(i).getName().equals(audioDeviceList2.get(i).getName()) ||
                    !audioDeviceList1.get(i).getSN().equals(audioDeviceList2.get(i).getSN()) ||
                    !audioDeviceList1.get(i).getFWVersion().equals(audioDeviceList2.get(i).getFWVersion()) ||
                    !audioDeviceList1.get(i).getCid().equals(audioDeviceList2.get(i).getCid()) ||
                    !audioDeviceList1.get(i).getIP().equals(audioDeviceList2.get(i).getIP()) ||
                    !audioDeviceList1.get(i).getDeviceIP().equals(audioDeviceList2.get(i).getDeviceIP()) ||
                    !(audioDeviceList1.get(i).getIsMute() == audioDeviceList2.get(i).getIsMute()) ||
                    !audioDeviceList1.get(i).getLang().equals(audioDeviceList2.get(i).getLang()) ||
                    !(audioDeviceList1.get(i).getIsAlexaConnect() == audioDeviceList2.get(i).getIsAlexaConnect()) ||
                    !(audioDeviceList1.get(i).getIsDeviceConnect() == audioDeviceList2.get(i).getIsDeviceConnect())) {
                return false;
            }
        }
        return true;

    }


    public void checkIsHomeNetwork() {
        // 打 WIFI API 檢測手機是否有連線上 device 預設的 setup WIFI SSID
        mActionCode = Code.Action.VoiceAssistant.CHECK_IF_HOME_NETWORK;

        // 打 device WIFI API 需要以 audio device IP (不固定的值) 為 base URL 才會通, 打完 API 之後要回復成原本的 base URL (有兩個地方)
        FirmwareManager.getInstance().changeFirmwareApiBaseUrl(CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_DEVICE_IP));
        FirmwareManager.getInstance().getDeviceBoardId(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                setError();

                // (1) 網路不通, API 沒有打出去, 回復原本的 base URL
                FirmwareManager.getInstance().restoreToDefaultBaseUrl();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                    JsonObject responseData = (JsonObject) apiResponse.body;
                    String deviceUdid = CommonUtils.getSharedPrefData(AppConstants.KEY_AUDIO_DEVICE_UDID);
                    String udid = mJsonParserWrapper.jsonGetString(responseData, "udid");

                    LogUtils.trace("udid", deviceUdid + "\n" + udid);
                    if (mJsonParserWrapper.jsonGetString(responseData, "status").equals("719") && deviceUdid.equals(udid)) {
                        //if (new JsonParserWrapper().jsonGetString(responseData, "status").equals("719")) {
                        // 若 device 已經 provision 完成, 第二次再進入 WIFI setup 流程, 打 API 會回傳 status 719
                        ResultStatus resultStatus = new ResultStatus();
                        resultStatus.actionCode = mActionCode;
                        resultStatus.success = true;
                        mResultStatus.setValue(resultStatus);
                    } else {
                        setError();
                    }
                } else {
                    setError();
                }
                mIsLoading.setValue(false);

                // (2) 打完 API 也處理完了, 回復原本的 base URL
                FirmwareManager.getInstance().restoreToDefaultBaseUrl();
            }
        });
    }

    private void setError() {
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        resultStatus.success = false;
        resultStatus.errorMsg = "Not in home network";    // TBD
        mResultStatus.setValue(resultStatus);
    }

}
