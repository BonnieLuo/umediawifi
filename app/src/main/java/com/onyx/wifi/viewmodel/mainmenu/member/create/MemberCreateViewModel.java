package com.onyx.wifi.viewmodel.mainmenu.member.create;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.util.List;

public class MemberCreateViewModel extends BaseViewModel {

    public MemberCreateViewModel(@NonNull Application application) {
        super(application);
    }

    public void createMember(String memberName, List<Client> clientList) {
        mActionCode = Code.Action.Member.CREATE_FAMILY_MEMBER;
        mCloudManager.createMember(memberName, clientList, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
