package com.onyx.wifi.viewmodel.item.mainmenu.accountsetting;

import com.onyx.wifi.utility.encrypt.EncryptUtils;

public class AccountSettingUser {

    private String firstName;
    private String lastName;
    private String email;
    private String confirmEmail;
    private String password;
    private String confirmPassword;

    public AccountSettingUser() {
        firstName = "";
        lastName = "";
        email = "";
        confirmEmail = "";
        password = "";
        confirmPassword = "";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return "AccountSettingUser{ " +
                "firstName = \"" + firstName + "\"" +
                ", lastName = \"" + lastName + "\"" +
                ", email = \"" + email + "\"" +
                ", confirmEmail = \"" + confirmEmail + "\"" +
                // 為了安全性, 打印出來的密碼需加密過, 不能以明文顯示
                ", password = \"" + EncryptUtils.shaEncrypt(password) + "\"" +
                ", confirmPassword = \"" + EncryptUtils.shaEncrypt(confirmPassword) + "\"" +
                " }";
    }

}
