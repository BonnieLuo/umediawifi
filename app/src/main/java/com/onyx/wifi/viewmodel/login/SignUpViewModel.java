package com.onyx.wifi.viewmodel.login;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.login.SignUpUser;

public class SignUpViewModel extends BaseViewModel {

    private SignUpUser mSignUpUser = new SignUpUser();

    private String mInputErrorMsg = "";

    public SignUpViewModel(@NonNull Application application) {
        super(application);
    }

    public SignUpUser getSignUpUser() {
        return mSignUpUser;
    }

    public String getInputErrorMsg() {
        return mInputErrorMsg;
    }

    public boolean isAccountDataFilled() {
        if (mSignUpUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mSignUpUser.getFirstName());
            filled &= InputUtils.isNotEmpty(mSignUpUser.getLastName());
            filled &= InputUtils.isNotEmpty(mSignUpUser.getEmail());
            filled &= InputUtils.isNotEmpty(mSignUpUser.getConfirmEmail());
            return filled;
        }
    }

    public boolean isAccountDataValid() {
        if (mSignUpUser == null) {
            return false;
        } else {
            if (!mSignUpUser.getConfirmEmail().equals(mSignUpUser.getEmail())) {
                mInputErrorMsg = mContext.getString(R.string.err_email_not_the_same);
                return false;
            }
            if (!InputUtils.isEmailValid(mSignUpUser.getEmail())) {
                mInputErrorMsg = mContext.getString(R.string.err_email_not_valid);
                return false;
            }
            return true;
        }
    }

    public boolean isPasswordDataFilled() {
        if (mSignUpUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mSignUpUser.getPassword());
            filled &= InputUtils.isNotEmpty(mSignUpUser.getConfirmPassword());
            filled &= mSignUpUser.getCheckPrivacy();
            return filled;
        }
    }

    public boolean isPasswordDataValid() {
        if (mSignUpUser == null) {
            return false;
        } else {
            if (!InputUtils.isPasswordLengthEnough(mSignUpUser.getPassword())) {
                mInputErrorMsg = mContext.getString(R.string.err_pwd_too_short);
                return false;
            }
            if (!mSignUpUser.getConfirmPassword().equals(mSignUpUser.getPassword())) {
                mInputErrorMsg = mContext.getString(R.string.err_pwd_not_the_same);
                return false;
            }
            return true;
        }
    }

    public void signUp() {
        mActionCode = Code.Action.UserAccount.SIGN_UP;
        mCloudManager.signUp(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    /* 註冊完成後會開啟 email 認證頁面, 進入 email 認證程序.
                       而 email 認證程序相關的 cloud API 會需要用到 uid 和 email 兩項資訊, 故先儲存起來. */
                    String uid = new JsonParserWrapper().jsonGetString((JsonObject) apiResponse.body, "uid");
                    mDataManager.setSignUpUid(uid);
                    mDataManager.setSignUpEmail(mSignUpUser.getEmail());
                }

                handleResponse(apiResponse);
            }
        }, mSignUpUser);
    }
}
