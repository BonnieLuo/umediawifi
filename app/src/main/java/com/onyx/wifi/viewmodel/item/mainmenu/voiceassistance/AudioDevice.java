package com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AudioDevice {
    private String mUdid;
    private String mDid;
    private String mLocation;
    private String mType;
    private String mName;
    private String mSN;
    private String mFWVersion;
    private String mMac;
    private String mIP;
    private String mDeviceIP;
    private boolean mIsMute;
    private String mLang;
    private boolean mIsAlexaConnect;
    private boolean mIsDeviceConnect;

    public AudioDevice(
            String udid,
            String did,
            String location,
            String type,
            String name,
            String SN,
            String FWVersion,
            String mac,
            String ip, // audio_ip
            String device_ip,
            Boolean isMute,
            String lang,
            Boolean isAlexaConnect,
            Boolean isDeviceConnect) {
        mUdid = udid;
        mDid = did;
        mLocation = location;
        mType = type;
        mName = name;
        mSN = SN;
        mFWVersion = FWVersion;
        mMac = mac;
        mIP = ip;
        mDeviceIP = device_ip;
        mIsMute = isMute;
        mLang = lang;
        mIsAlexaConnect = isAlexaConnect;
        mIsDeviceConnect = isDeviceConnect;
    }

    // udid
    public String getUdid() {
        return mUdid;
    }

    public void setUdid(String udid) {
        this.mUdid = udid;
    }

    // did
    public String getDid() {
        return mDid;
    }

    public void setDid(String did) {
        this.mDid = did;
    }

    // location
    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        this.mLocation = location;
    }

    // type
    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    // name
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    //SN
    public String getSN() {
        return mSN;
    }

    public void setSN(String SN) {
        this.mSN = SN;
    }

    //FWVersion
    public String getFWVersion() {
        return mFWVersion;
    }

    public void setFWVersion(String FWVersion) {
        this.mFWVersion = FWVersion;
    }

    //mac
    public String getCid() {
        return mMac;
    }

    public void setMac(String mac) {
        this.mMac = mac;
    }

    //ip
    public String getIP() {
        return mIP;
    }

    public void setIP(String IP) {
        this.mIP = IP;
    }

    //device_ip
    public String getDeviceIP() {
        return mDeviceIP;
    }

    public void setDeviceIP(String deviceIP) {
        this.mDeviceIP = deviceIP;
    }

    // isMute
    public boolean getIsMute() {
        return mIsMute;
    }

    public void setIsMute(boolean isMute) {
        this.mIsMute = isMute;
    }

    // lang
    public String getLang() {
        return mLang;
    }

    public void setLang(String lang) {
        this.mLang = lang;
    }

    // isAlexaConnect
    public boolean getIsAlexaConnect() {
        return mIsAlexaConnect;
    }

    public void setIsAlexaConnect(boolean isAlexaConnect) {
        this.mIsAlexaConnect = isAlexaConnect;
    }

    // isDeviceConnect
    public boolean getIsDeviceConnect() {
        return mIsDeviceConnect;
    }

    public void setIsDeviceConnect(boolean isDeviceConnect) {
        this.mIsDeviceConnect = isDeviceConnect;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
