package com.onyx.wifi.viewmodel.item.setup;

import com.google.gson.GsonBuilder;

public class StaticIpInfo {

    /**
     * ip : 192.168.70.22
     * mask : 255.255.255.0
     * gateway : 192.168.70.1
     * dns1 : 8.8.8.8
     * dns2 : 8.8.8.8
     */

    private String ip;
    private String mask;
    private String gateway;
    private String dns1;
    private String dns2;

    public StaticIpInfo() {
        ip = "";
        mask = "";
        gateway = "";
        dns1 = "";
        dns2 = "";
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getDns1() {
        return dns1;
    }

    public void setDns1(String dns1) {
        this.dns1 = dns1;
    }

    public String getDns2() {
        return dns2;
    }

    public void setDns2(String dns2) {
        this.dns2 = dns2;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
