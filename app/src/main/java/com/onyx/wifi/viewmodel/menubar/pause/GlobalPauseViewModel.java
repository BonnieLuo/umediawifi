package com.onyx.wifi.viewmodel.menubar.pause;

import android.app.Application;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;

import java.util.ArrayList;
import java.util.List;

public class GlobalPauseViewModel extends BaseViewModel {

    public GlobalPauseViewModel(@NonNull Application application) {
        super(application);
    }

    public void getGlobalPauseList() {
        mActionCode = Code.Action.Pause.GET_GLOBAL_PAUSE_LIST;
        mCloudManager.getGlobalPauseList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

                //mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                }
                handleResponse(apiResponse);
            }
        });
    }

    public void enableGlobalPause(String profileId) {
        mActionCode = Code.Action.Pause.ENABLE_GLOBAL_PAUSE;
        mCloudManager.enableGlobalPause(profileId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        handleDeviceResponse(apiResponse);
                    }
                }, 3000);
                //handleResponse(apiResponse);
            }
        });
    }

    public void disableGlobalPause(String profileId, String ruleId) {
        mActionCode = Code.Action.Pause.DISABLE_GLOBAL_PAUSE;
        mCloudManager.disableGlobalPause(profileId, ruleId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getMemberList() {
        mActionCode = Code.Action.Member.GET_MEMBER_LIST;
        mCloudManager.getMemberList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject json = (JsonObject) apiResponse.body;
                    if (json != null) {
                        JsonObject dataJSON = json.getAsJsonObject("data");

                        Gson gson = new Gson();
                        MemberList memberList = gson.fromJson(dataJSON, MemberList.class);

                        List<ClientModel> homeClientModelList = memberList.getHomeNetwork();

                        List<Client> recentlyActiveClients = new ArrayList<>();
                        for (ClientModel clientModel:homeClientModelList) {
                            Client client = new Client(clientModel);
                            recentlyActiveClients.add(client);
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setMemberList(memberList);
                        dataManager.setRecentlyActiveClients(recentlyActiveClients);
                    }
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getAllClients() {
        mActionCode = Code.Action.UserAccount.GET_ALL_CLIENTS;
        mCloudManager.getAllClients(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }
}
