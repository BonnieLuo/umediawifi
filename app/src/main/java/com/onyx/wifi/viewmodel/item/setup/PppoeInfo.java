package com.onyx.wifi.viewmodel.item.setup;

import com.onyx.wifi.utility.encrypt.EncryptUtils;

public class PppoeInfo {

    private String username;
    private String password;

    public PppoeInfo() {
        username = "";
        password = "";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PppoeInfo{ " +
                "username = \"" + username + "\"" +
                // 為了安全性, 打印出來的密碼需加密過, 不能以明文顯示
                ", password = \"" + EncryptUtils.shaEncrypt(password) + "\"" +
                " }";

    }
}
