package com.onyx.wifi.viewmodel.menubar.advancewireless;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class AdvanceWirelessSettingsViewModel extends BaseViewModel {

    public AdvanceWirelessSettingsViewModel(@NonNull Application application) {
        super(application);
    }

    public void getAdvanceWirelessSettings() {
        mActionCode = Code.Action.AdvanceWirelessSettings.GET_SETTINGS;
        mCloudManager.getAdvanceWirelessSettings(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
