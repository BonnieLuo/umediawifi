package com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting;

public enum FWStatus {
    UMEDIA_FW_UP_TO_DATE,
    UMEDIA_FW_UPDATE_AVAILABLE,
    UMEDIA_FW_UPDATING
}
