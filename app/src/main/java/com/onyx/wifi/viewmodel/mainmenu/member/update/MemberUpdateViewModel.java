package com.onyx.wifi.viewmodel.mainmenu.member.update;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.util.ArrayList;
import java.util.List;

public class MemberUpdateViewModel extends BaseViewModel {

    public MemberUpdateViewModel(@NonNull Application application) {
        super(application);
    }

    public void getMemberList() {
        mActionCode = Code.Action.Member.GET_MEMBER_LIST;
        mCloudManager.getMemberList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject json = (JsonObject) apiResponse.body;
                    if (json != null) {
                        JsonObject dataJSON = json.getAsJsonObject("data");

                        Gson gson = new Gson();
                        MemberList memberList = gson.fromJson(dataJSON, MemberList.class);

                        DataManager dataManager = DataManager.getInstance();

                        List<ClientModel> homeClientModelList = memberList.getHomeNetwork();

                        List<Client> recentlyActiveClients = new ArrayList<>();
                        for (ClientModel clientModel:homeClientModelList) {
                            Client client = new Client(clientModel);
                            recentlyActiveClients.add(client);
                        }

                        List<MemberModel> familyMemberList = memberList.getFamilyMemberList();
                        for (MemberModel memberModel:familyMemberList) {
                            String memberId = memberModel.getMemberId();

                            FamilyMember currentMember = dataManager.getManageMember();

                            if (currentMember != null) {
                                String currentMemberId = currentMember.getId();

                                if (memberId.equalsIgnoreCase(currentMemberId)) {
                                    dataManager.setManageMember(new FamilyMember(memberModel));
                                }
                            }
                        }

                        dataManager.setMemberList(memberList);
                        dataManager.setRecentlyActiveClients(recentlyActiveClients);
                    }
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void updateMember(FamilyMember member) {
        String memberId = member.getId();
        String memberName = member.getTemporaryName();
        List<Client> clientList = member.getTemporaryClientList();

        mActionCode = Code.Action.Member.UPDATE_FAMILY_MEMBER;
        mCloudManager.updateMember(memberId, memberName, clientList, new ApiCallback() {
            @Override
            public void onInternetUnavailable() { }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
