package com.onyx.wifi.viewmodel.menubar.pause;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.pause.PauseProfile;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class EditPauseProfileViewModel extends BaseViewModel {

    public EditPauseProfileViewModel(@NonNull Application application) {
        super(application);
    }

    public void createGlobalPauseProfile(PauseProfile pauseProfile) {
        mActionCode = Code.Action.Pause.CREATE_GLOBAL_PAUSE_PROFILE;
        mCloudManager.createGlobalPauseProfile(pauseProfile, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                }
                handleResponse(apiResponse);
            }
        });
    }

    public void updateGlobalPauseProfile(PauseProfile pauseProfile) {
        mActionCode = Code.Action.Pause.UPDATE_GLOBAL_PAUSE_PROFILE;
        mCloudManager.updateGlobalPauseProfile(pauseProfile, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                }
                handleResponse(apiResponse);
            }
        });
    }

    public void removeGlobalPauseProfile(PauseProfile pauseProfile) {
        mActionCode = Code.Action.Pause.REMOVE_GLOBAL_PAUSE_PROFILE;
        mCloudManager.removeGlobalPauseProfile(pauseProfile ,new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getAllClients() {
        mActionCode = Code.Action.UserAccount.GET_ALL_CLIENTS;
        mCloudManager.getAllClients(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
