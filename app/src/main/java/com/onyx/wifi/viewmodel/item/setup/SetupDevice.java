package com.onyx.wifi.viewmodel.item.setup;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.GsonBuilder;
import com.onyx.wifi.utility.AppConstants;

public class SetupDevice implements Parcelable {

    // SetupDevice is the device which is scanned by BLE during the device setup flow

    private String name;
    private String macAddress;
    private String modelNumber;
    private int rssi;
    private AppConstants.DeviceType deviceType;
    private BluetoothDevice bleDevice;

    public SetupDevice() {
        name = "";
        macAddress = "";
        rssi = 0;
        deviceType = AppConstants.DeviceType.NONE;
        bleDevice = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public AppConstants.DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(AppConstants.DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public BluetoothDevice getBleDevice() {
        return bleDevice;
    }

    public void setBleDevice(BluetoothDevice bleDevice) {
        this.bleDevice = bleDevice;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

    /******************** Parcelable ********************/

    // 給 createFromParcel 裡面用
    protected SetupDevice(Parcel in) {
        name = in.readString();
        macAddress = in.readString();
        rssi = in.readInt();
        deviceType = AppConstants.DeviceType.values()[in.readInt()];
        bleDevice = in.readParcelable(BluetoothDevice.class.getClassLoader());
    }

    public static final Creator<SetupDevice> CREATOR = new Creator<SetupDevice>() {
        /**
         *
         * @param in
         * @return
         * createFromParcel() 方法中我們要去讀取剛才寫出的所有欄位, 並創建一個物件進行返回,
         * 其中的欄位都是調用 Parcel 的 readXxx() 方法讀取到的, 注意這裡讀取的順序一定要和剛才寫出的順序完全相同.
         * 讀取的工作我們利用一個構造函數幫我們完成了
         */
        @Override
        public SetupDevice createFromParcel(Parcel in) {
            return new SetupDevice(in); // 在構造函數裡面完成了讀取的工作
        }

        // 供反序列化本類陣列時調用的
        @Override
        public SetupDevice[] newArray(int size) {
            return new SetupDevice[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;  // 內容介面描述, 預設返回 0 即可。
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(macAddress);
        dest.writeInt(rssi);
        dest.writeInt(deviceType.ordinal());
        dest.writeParcelable(bleDevice, flags);
    }
}
