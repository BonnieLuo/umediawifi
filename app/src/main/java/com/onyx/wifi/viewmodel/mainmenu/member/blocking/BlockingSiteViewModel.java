package com.onyx.wifi.viewmodel.mainmenu.member.blocking;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class BlockingSiteViewModel extends BaseViewModel {

    public BlockingSiteViewModel(@NonNull Application application) {
        super(application);
    }

    // Content Filters

    public void getContentFiltersList(final String memberId) {
        mActionCode = Code.Action.Member.GET_MEMBER_BLOCKING_LIST;
        mCloudManager.getContentFiltersList(memberId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void createContentFilters(final String memberId, final String site) {
        mActionCode = Code.Action.Member.CREATE_MEMBER_WEBSITE_BLOCKING;
        mCloudManager.createContentFilters(memberId, site, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void addContentFilters(final String memberId, final String site) {
        mActionCode = Code.Action.Member.ADD_MEMBER_WEBSITE_BLOCKING;
        mCloudManager.addContentFilters(memberId, site, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void removeContentFilters(final String memberId, final String site) {
        mActionCode = Code.Action.Member.REMOVE_MEMBER_WEBSITE_BLOCKING;
        mCloudManager.removeContentFilters(memberId, site, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void cleanContentFilters(final String memberId) {
        mActionCode = Code.Action.Member.CLEAN_MEMBER_WEBSITE_BLOCKING;
        mCloudManager.cleanContentFilters(memberId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
