package com.onyx.wifi.viewmodel.menubar.advancewireless;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.advancewireless.dmz.DemilitarizedZoneConfig;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class DmzViewModel extends BaseViewModel {

    public DmzViewModel(@NonNull Application application) {
        super(application);
    }

    public void setupDmz(DemilitarizedZoneConfig dmzConfig) {
        mActionCode = Code.Action.AdvanceWirelessSettings.SET_DMZ;
        mCloudManager.setupDmz(dmzConfig, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
