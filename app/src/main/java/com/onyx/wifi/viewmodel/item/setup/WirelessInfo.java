package com.onyx.wifi.viewmodel.item.setup;

import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.encrypt.EncryptUtils;

public class WirelessInfo {

    private String ssid2g = "";
    private String password2g = "";
    private String ssid5g = "";
    private String password5g = "";
    private boolean useSame = true;

    public String getSsid2g() {
        return ssid2g;
    }

    public void setSsid2g(String ssid2g) {
        this.ssid2g = ssid2g;
    }

    public String getPassword2g() {
        return password2g;
    }

    public void setPassword2g(String password2g) {
        this.password2g = password2g;
    }

    public String getSsid5g() {
        return ssid5g;
    }

    public void setSsid5g(String ssid5g) {
        this.ssid5g = ssid5g;
    }

    public String getPassword5g() {
        return password5g;
    }

    public void setPassword5g(String password5g) {
        this.password5g = password5g;
    }

    public boolean getUseSame() {
        return useSame;
    }

    public void setUseSame(boolean useSame) {
        this.useSame = useSame;
    }

    @Override
    public String toString() {
        return "WirelessInfo{ " +
                "ssid2g = \"" + ssid2g + "\"" +
                // 為了安全性, 打印出來的密碼需加密過, 不能以明文顯示
                ", password2g = \"" + EncryptUtils.shaEncrypt(password2g) + "\"" +
                ", ssid5g = \"" + ssid5g + "\"" +
                ", password5g = \"" + EncryptUtils.shaEncrypt(password5g) + "\"" +
                ", useSame = " + useSame +
                " }";
    }

    public void randomGenerate() {
        ssid2g = InputUtils.generateNetworkSsid();
        password2g = InputUtils.generateNetworkPassword();
        ssid5g = ssid2g + "_5GHz";
        password5g = InputUtils.generateNetworkPassword();
    }
}
