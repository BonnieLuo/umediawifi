package com.onyx.wifi.viewmodel.mainmenu;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.R;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.mainmenu.accountsetting.AccountSettingUser;

public class AccountSettingViewModel extends BaseViewModel {

    private AccountSettingUser mAccountSettingUser = new AccountSettingUser();
    private String mVerificationCode;

    private String mInputErrorMsg = "";

    public AccountSettingViewModel(@NonNull Application application) {
        super(application);
    }

    public AccountSettingUser getAccountSettingUser() {
        return mAccountSettingUser;
    }

    public void setVerificationCode(String verificationCode) {
        mVerificationCode = verificationCode;
    }

    public String getInputErrorMsg() {
        return mInputErrorMsg;
    }

    //
    // check if the following are filled: first name, last name
    //
    public boolean isNameDataFilled() {
        if (mAccountSettingUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mAccountSettingUser.getFirstName());
            filled &= InputUtils.isNotEmpty(mAccountSettingUser.getLastName());
            return filled;
        }
    }

    public boolean isNameChanged() {
        if (mAccountSettingUser == null) {
            return false;
        } else {
            boolean filled = !(mAccountSettingUser.getFirstName()).equals(CommonUtils.getSharedPrefData(AppConstants.KEY_FIRST_NAME));
            filled |= !(mAccountSettingUser.getLastName()).equals(CommonUtils.getSharedPrefData(AppConstants.KEY_LAST_NAME));
            return filled;
        }
    }

    public boolean isEmailDataFilled() {
        if (mAccountSettingUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mAccountSettingUser.getEmail());
            filled &= InputUtils.isNotEmpty(mAccountSettingUser.getConfirmEmail());
            return filled;
        }
    }

    //
    // check if email is equal to confirm email (rename?)
    //
    public boolean isEmailDataValid() {
        if (mAccountSettingUser == null) {
            return false;
        } else {
            if (!mAccountSettingUser.getConfirmEmail().equals(mAccountSettingUser.getEmail())) {
                mInputErrorMsg = mContext.getString(R.string.err_email_not_the_same);
                return false;
            }
            if (!InputUtils.isEmailValid(mAccountSettingUser.getEmail())) {
                mInputErrorMsg = mContext.getString(R.string.err_email_not_valid);
                return false;
            }
            return true;
        }
    }

    //
    //  check if the following are filled: password, confirm password
    //
    public boolean isPasswordDataFilled() {
        if (mAccountSettingUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mAccountSettingUser.getPassword());
            filled &= InputUtils.isNotEmpty(mAccountSettingUser.getConfirmPassword());
            return filled;
        }
    }

    //
    // check if password is valid: (1) password == confirm password? (2) length >= 8
    //
    public boolean isPasswordDataValid() {
        if (mAccountSettingUser == null) {
            return false;
        } else {
            // the password length shall >= 8
            if (!InputUtils.isPasswordLengthEnough(mAccountSettingUser.getPassword())) {
                mInputErrorMsg = mContext.getString(R.string.err_pwd_too_short);
                return false;
            }
            if (!mAccountSettingUser.getConfirmPassword().equals(mAccountSettingUser.getPassword())) {
                mInputErrorMsg = mContext.getString(R.string.err_pwd_not_the_same);
                return false;
            }
            return true;
        }
    }

    //
    // interfaces to call Cloud API
    //
    public void updateUserInfo() {
        mActionCode = Code.Action.UserAccount.UPDATE_USER_INFO;
        mCloudManager.updateUserInfo(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) { // success
                    // 在ViewModel裡實作data set/get 在UI層僅顯示(暫時以shareprefrence紀錄登入的account資訊)
                    CommonUtils.setSharedPrefData(AppConstants.KEY_FIRST_NAME, mAccountSettingUser.getFirstName());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_LAST_NAME, mAccountSettingUser.getLastName());
                }
                handleResponse(apiResponse);
            }
        }, mAccountSettingUser);
    }

    public void updatePassword() {
        mActionCode = Code.Action.UserAccount.UPDATE_PASSWORD;
        mCloudManager.updatePassword(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleResponse(apiResponse);
            }
        }, mAccountSettingUser.getPassword());
    }

    public void updateEmail() {
        mActionCode = Code.Action.UserAccount.UPDATE_EMAIL;
        mCloudManager.updateEmail(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleResponse(apiResponse);
            }
        }, mAccountSettingUser.getEmail());
    }

    public void verifyUpdateEmail() {
        mActionCode = Code.Action.UserAccount.VERIFY_UPDATE_EMAIL;
        mCloudManager.verifyUpdateEmail(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) { // success
                    CommonUtils.setSharedPrefData(AppConstants.KEY_EMAIL, mAccountSettingUser.getEmail());
                }
                handleResponse(apiResponse);
            }
        }, mVerificationCode, mAccountSettingUser.getEmail());
    }

    public void deleteUser() {
        mActionCode = Code.Action.UserAccount.DELETE_USER;
        mCloudManager.deleteUser(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

                handleResponse(apiResponse);
            }
        });
    }
}
