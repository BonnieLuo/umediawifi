package com.onyx.wifi.viewmodel.mainmenu;

import android.app.Application;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.BnbNetworkSetting;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.GuestNetworkSetting;
import com.onyx.wifi.model.item.WirelessInfo5g;
import com.onyx.wifi.model.item.WirelessSetting;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.wirelesssetting.WirelessSettingData;

public class WirelessSettingViewModel extends BaseViewModel {

    public WirelessSettingViewModel(@NonNull Application application) {
        super(application);
    }

    public WirelessSettingData getWirelessSettingData() {
        WirelessSetting wirelessSetting = mDataManager.getWirelessSetting();
        WirelessSettingData wirelessSettingData = new WirelessSettingData();
        wirelessSettingData.setWifiSame(wirelessSetting.getWifiSameEnabled());
        wirelessSettingData.setSsid2g(wirelessSetting.getWireless2g().getSsid());
        wirelessSettingData.setPassword2g(wirelessSetting.getWireless2g().getPwd());
        wirelessSettingData.setSsid5g(getWirelessInfo5gFronthaul(wirelessSetting).getSsid());
        wirelessSettingData.setPassword5g(getWirelessInfo5gFronthaul(wirelessSetting).getPwd());
        wirelessSettingData.setClientOnlineNotification(wirelessSetting.getClientOnlineNotification());
        return wirelessSettingData;
    }

    public GuestNetworkSetting getGuestNetworkSettingData() {
        return mDataManager.getGuestNetworkSetting();
    }

    public BnbNetworkSetting getBnbNetworkSettingData() {
        return mDataManager.getBnbNetworkSetting();
    }

    private WirelessInfo5g getWirelessInfo5gFronthaul(WirelessSetting wirelessSetting) {
        WirelessInfo5g wirelessInfo = new WirelessInfo5g();
        if (wirelessSetting != null) {
            WirelessInfo5g wirelessInfo5g1 = wirelessSetting.getWireless5g();
            WirelessInfo5g wirelessInfo5g2 = wirelessSetting.getWireless5g2();
            if (wirelessInfo5g1.getBh().equals("0")) {
                wirelessInfo = wirelessInfo5g1;
            } else if (wirelessInfo5g2.getBh().equals("0")) {
                wirelessInfo = wirelessInfo5g2;
            }
        }
        return wirelessInfo;
    }

    public void getWirelessSetting() {
        mActionCode = Code.Action.WirelessSetting.GET_WIRELESS_SETTING;
        mCloudManager.getWirelessSetting(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 打第一支 API 要顯示 loading 畫面
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    JsonObject settingsJson = jsonParserWrapper.jsonGetJsonObject(dataJson, "settings");
                    WirelessSetting wirelessSetting = jsonParserWrapper.jsonToObject(settingsJson, WirelessSetting.class);
                    mDataManager.setWirelessSetting(wirelessSetting);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void getGuestNetworkSetting() {
        mActionCode = Code.Action.WirelessSetting.GET_GUEST_NETWORK_SETTING;
        mCloudManager.getGuestNetwork(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    GuestNetworkSetting guestNetworkSetting = jsonParserWrapper.jsonToObject(dataJson, GuestNetworkSetting.class);
                    mDataManager.setGuestNetworkSetting(guestNetworkSetting);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void getBnbNetworkSetting() {
        mActionCode = Code.Action.WirelessSetting.GET_BNB_NETWORK_SETTING;
        mCloudManager.getBnbNetwork(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    BnbNetworkSetting bnbNetworkSetting = jsonParserWrapper.jsonToObject(dataJson, BnbNetworkSetting.class);
                    mDataManager.setBnbNetworkSetting(bnbNetworkSetting);
                }
                // 最後一支 API, handle response 時要結束 loading 狀態
                handleResponse(apiResponse);
            }
        });
    }

    public void updateWirelessSetting(WirelessSettingData wirelessSettingData, boolean isNeedToDelay) {
        mActionCode = Code.Action.WirelessSetting.UPDATE_WIRELESS_SETTING;
        mCloudManager.updateWirelessSetting(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (isNeedToDelay) {
                    // 需要 delay 的情況是在 WirelessInfoSettingActivity 打完 API
                    // 由於回去上一頁 (WirelessSettingActivity) 會打 get wireless setting 取得最新值
                    // 故等待一下確保 cloud 的值更新完成, 比較保險

                    // 需要等待一下子 cloud 的值才會更新完成, 故延遲 3 秒再通知 UI 結果
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            handleDeviceResponse(apiResponse);
                        }
                    }, 3000);
                } else {
                    // 不需要 delay 的情況是在 WirelessSettingActivity 打完 API
                    // 由於不會重新 get wireless setting, 而是將資料寫回 share preference, 故不需要 delay
                    handleDeviceResponse(apiResponse);
                }
            }
        }, wirelessSettingData);
    }

    // update guest network
    public void updateGuestNetwork(GuestNetworkSetting guestNetworkSetting) {
        mActionCode = Code.Action.WirelessSetting.UPDATE_GUEST_NETWORK_SETTING;
        mCloudManager.updateGuestNetwork(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                LogUtils.trace("device_status", "[updateGuestNetwork]" + " " + guestNetworkSetting.toString());
                if (apiResponse.httpStatusCode == 200) {
                    LogUtils.trace("device_status", "updateGuestNetwork -- Success");
                }
                // 需要等待一下子 cloud 的值才會更新完成, 故延遲 3 秒再通知 UI 結果
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        handleDeviceResponse(apiResponse);
                    }
                }, 3000);
            }
        }, guestNetworkSetting);
    }

    // update BnB network
    public void updateBnbNetwork(BnbNetworkSetting bnbNetworkSetting) {
        mActionCode = Code.Action.WirelessSetting.UPDATE_BNB_NETWORK_SETTING;
        mCloudManager.updateBnbNetwork(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                LogUtils.trace("device_status", "[updateBnbNetwork]" + " " + bnbNetworkSetting.toString());
                if (apiResponse.httpStatusCode == 200) {
                    LogUtils.trace("device_status", "updateBnbNetwork -- Success");
                }
                // 需要等待一下子 cloud 的值才會更新完成, 故延遲 3 秒再通知 UI 結果
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        handleDeviceResponse(apiResponse);
                    }
                }, 3000);
            }
        }, bnbNetworkSetting);
    }

    public void getDashboardInfo() {
        mActionCode = Code.Action.NetworkSetting.GET_DASHBOARD_INFO;
        mCloudManager.getDashboardInfo(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    mDataManager.setDashboardInfo((DashboardInfo) apiResponse.body);
                }
                if (apiResponse.httpStatusCode == 401) {
                    // handleUnauthorized(apiResponse);
                    LogUtils.trace("NotifyMsg", "401");
                    return;
                }

                ResultStatus resultStatus = new ResultStatus();
                resultStatus.actionCode = mActionCode;
                if (apiResponse.httpStatusCode == 200) {
                    resultStatus.success = true;
                    resultStatus.data = apiResponse.body;
                } else {
                    resultStatus.success = false;
                    resultStatus.errorMsg = apiResponse.errorMsg;
                }
                // 設置 API 回傳的結果讓 UI 響應
                mResultStatus.setValue(resultStatus);

            }
        });
    }
}
