package com.onyx.wifi.viewmodel.login;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.login.SignInUser;

public class EmailVerificationViewModel extends BaseSignInViewModel {

    private String mVerificationCode;

    private SignInUser mSignInUser = new SignInUser();

    private boolean mIsNeedAutoSignIn;

    public EmailVerificationViewModel(@NonNull Application application) {
        super(application);
    }

    public void setVerificationCode(String verificationCode) {
        mVerificationCode = verificationCode;
    }

    public void setSignInUser(String email, String password) {
        mSignInUser.setEmail(email);
        mSignInUser.setPassword(password);
    }

    public void setIsNeedAutoSignIn(boolean isNeedAutoSignIn) {
        mIsNeedAutoSignIn = isNeedAutoSignIn;
    }

    public void verifyCode() {
        mActionCode = Code.Action.UserAccount.VERIFY_EMAIL_VERIFY_CODE;
        mCloudManager.verifyEmail(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    if (mIsNeedAutoSignIn) {
                        firebaseSignIn(mSignInUser);
                    } else {
                        getUserInfo();
                    }
                } else {
                    handleResponse(apiResponse);
                }
            }
        }, mVerificationCode, mDataManager.getUserId());
    }

    public void resendCode() {
        mActionCode = Code.Action.UserAccount.VERIFY_EMAIL_RESEND_CODE;
        mCloudManager.resendEmailValidation(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleResponse(apiResponse);
            }
        }, mDataManager.getUserEmail());
    }
}
