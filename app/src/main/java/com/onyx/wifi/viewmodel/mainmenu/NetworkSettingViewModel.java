package com.onyx.wifi.viewmodel.mainmenu;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.ClientInfoList;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.NetworkSetting;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.DateUtils;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.networksetting.UMEDIAFWStatus;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.FWStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NetworkSettingViewModel extends BaseViewModel {
    private ArrayList<String> mTimezoneList = null;
    private ArrayList<UMEDIAFWStatus> mFWStatusList = new ArrayList<UMEDIAFWStatus>(); // got from cloud for listing audio devices
    private String mUpdateFWDid = null;
    private String mUpdateFWStatus = null;
    private Map<String, String> mOptions = new HashMap<>();
    private ClientInfoList mClientInfoList = new ClientInfoList();
    private ArrayList<ClientModel> mRecentClient = new ArrayList<ClientModel>();
    private ArrayList<ClientModel> mLongInactiveClient = new ArrayList<ClientModel>();
    private ArrayList<ClientModel> mBlockedClient = new ArrayList<ClientModel>();
    private boolean mbIsEnableUpdateButton = false;
    long mJudgeTimeInMillis = 4 * 24 * 60 * 60 * 1000;


    private JsonParserWrapper mJsonParserWrapper = null;

    public NetworkSettingViewModel(@NonNull Application application) {
        super(application);
        mOptions.put("nid", mDataManager.getNodeSetId());
        mOptions.put("scope", "all");
        mJsonParserWrapper = new JsonParserWrapper();
    }

    public ArrayList<UMEDIAFWStatus> getFWStatusList() {
        //LogUtils.trace("AdapterList", "getFWStatusList" + mFWStatusList.size());
        for (UMEDIAFWStatus umediafwStatus : mFWStatusList) {
            //LogUtils.trace("AdapterList", umediafwStatus.toString());
        }

        return mFWStatusList;
    }

    public ArrayList<UMEDIAFWStatus> ifAnyUpdating() {
        //LogUtils.trace("AdapterList", "getFWStatusList" + mFWStatusList.size());
        for (UMEDIAFWStatus umediafwStatus : mFWStatusList) {
            //LogUtils.trace("AdapterList", umediafwStatus.toString());
        }

        return mFWStatusList;
    }

    public boolean IsUpdateAvailable() {
        return mbIsEnableUpdateButton;
    }

    // Firmware update
    public void UpdateFWStatusList(
            String updateFWDid,
            String updateFWStatus,
            String currentFW,
            String updateFW) {
        int i = 0;
        UMEDIAFWStatus umediafwStatus = null;
        String currentFWVersion = null;
        String updateFWVersion = null;


        currentFWVersion = currentFW;
        updateFWVersion = updateFW;

        for (i = 0; i < mFWStatusList.size(); ++i) {
            String did = mFWStatusList.get(i).getDid();
            umediafwStatus = mFWStatusList.get(i);
            if (currentFW == null) {
                currentFWVersion = umediafwStatus.getFWVersion();
            }

            if (updateFW == null) {
                updateFWVersion = umediafwStatus.getFWNewestVerion();
            }

            if (did.equals(updateFWDid)) {
                // fw status
                if (updateFWStatus.equals("1")) {
                    umediafwStatus.setFWVersion(currentFWVersion);
                    if (currentFWVersion.equals(updateFWVersion)) {
                        umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UP_TO_DATE);
                    } else {
                        umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UPDATE_AVAILABLE);
                    }
                } else if (updateFWStatus.equals("0") || updateFWStatus.equals("2") || updateFWStatus.equals("3")) {

                    umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UPDATING);
                    mFWStatusList.set(i, umediafwStatus);
                } else if (updateFWStatus.equals("-1")) {

                } else if (updateFWStatus.equals("-2")) {

                }/*else {// else if (updateFWStatus.equals("1")) {// 2: update successful (ready to reboot), 3: already up-to-date
                    if (updateFWStatus.equals("2") || updateFWStatus.equals("3")) {
                        umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UP_TO_DATE);
                    } else { //1: update available(normal)
                        //umediafwStatus = mFWStatusList.get(i);
                        if (umediafwStatus.getFWVersion().equals(umediafwStatus.getFWNewestVerion())) {
                            umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UP_TO_DATE);
                        } else {
                            umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UPDATE_AVAILABLE);
                        }
                        mFWStatusList.set(i, umediafwStatus);
                    }

                }*//*else {// else if (updateFWStatus.equals("1")) {//1: update available(normal), 2: update successful (ready to reboot), 3: already up-to-date
                    umediafwStatus = mFWStatusList.get(i);
                    if (umediafwStatus.getFWVersion().equals(umediafwStatus.getFWNewestVerion())) {
                        umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UP_TO_DATE);
                    } else {
                        umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UPDATE_AVAILABLE);
                    }
                    mFWStatusList.set(i, umediafwStatus);
                }*/
            }
            //LogUtils.trace("NotifyMsg", mFWStatusList.get(i).toString());
        }
    }

    public void postHomeNetworkSetting(String city, NetworkSetting networkSetting) {
        mActionCode = Code.Action.NetworkSetting.POST_HOME_NETWORK_SETTING;
        //LogUtils.trace("NotifyMsg", "[postHomeNetworkSetting]");
        mCloudManager.postHomeNetworkSetting(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                //LogUtils.trace("CloudApi", "onReceiveResponse");

                if (apiResponse.httpStatusCode == 200) {
                    //LogUtils.trace("CloudApi", "success " + apiResponse.httpStatusCode);
                } else {
                    //LogUtils.trace("CloudApi", "errorCode = " + apiResponse.httpStatusCode);
                }

                handleDeviceResponse(apiResponse);
            }
        }, city, networkSetting);

    }

    public void postUptime() {
        mActionCode = Code.Action.NetworkSetting.POST_UPTIME;
        mCloudManager.postUptime(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                //mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                //LogUtils.trace("CloudApi", "onReceiveResponse");

                if (apiResponse.httpStatusCode == 200) {
                    //LogUtils.trace("CloudApi", "success " + apiResponse.httpStatusCode);
                } else {
                    //LogUtils.trace("CloudApi", "errorCode = " + apiResponse.httpStatusCode);
                }

                handleResponse(apiResponse);
            }
        });
    }

    public void putInternetPriority(ArrayList<String> cidList) {
        mActionCode = Code.Action.NetworkSetting.PUT_INTERNET_PRIORITY;
        mCloudManager.putInternetPriority(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    //LogUtils.trace("CloudApi", "success " + apiResponse.httpStatusCode);
                } else {
                    //LogUtils.trace("CloudApi", "errorCode = " + apiResponse.httpStatusCode);
                }

                handleDeviceResponse(apiResponse);
            }
        }, cidList);
    }

    public void firmwareUpdateAll() {
        mActionCode = Code.Action.NetworkSetting.FIRMWARE_UPDATE_ALL;
        mCloudManager.firmwareUpdateAll(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                //LogUtils.trace("NotifyMsg", "[FW updateAll:httpStatusCode]" + " " + apiResponse.httpStatusCode);
                if (apiResponse.httpStatusCode == 200) {
                    //LogUtils.trace("NotifyMsg", "firmwareUpdateAll -- Success" );
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getFirmwareStatus() {
        mActionCode = Code.Action.NetworkSetting.GET_FIRMWARE_STATUS;
        mCloudManager.getFirmwareStatus(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {

                //mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {

                    // init data
                    mFWStatusList.clear();
                    mbIsEnableUpdateButton = false;

                    JsonObject fwStatusdata = mJsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");

                    JsonArray fwStatusInfo = mJsonParserWrapper.jsonGetJsonArray(fwStatusdata, "node_list");
                    //LogUtils.trace("AdapterList", "getFirmwareStatus -- Success" + " " + fwStatusInfo.size());
                    //LogUtils.trace("FWList", "(got from cloud: (original packet) List size:" + " " + fwStatusInfo.size());

                    int i = 0;
                    for (JsonElement value : fwStatusInfo) {
                        JsonObject jsonObject = value.getAsJsonObject();
                        UMEDIAFWStatus umediafwStatus = new UMEDIAFWStatus(
                                AppConstants.DeviceType.NONE,
                                "",
                                "",
                                FWStatus.UMEDIA_FW_UPDATE_AVAILABLE,
                                "",
                                "");

                        // did
                        umediafwStatus.setDid(mJsonParserWrapper.jsonGetString(jsonObject, "did"));
                        // current FW version
                        umediafwStatus.setFWVersion(mJsonParserWrapper.jsonGetString(jsonObject, "current_fw"));
                        // newest FW version
                        umediafwStatus.setFWNewestVerion(mJsonParserWrapper.jsonGetString(jsonObject, "update_fw"));
                        // device label
                        String hexString = mJsonParserWrapper.jsonGetString(jsonObject, "device_label");
                        umediafwStatus.setDeviceLabel(new String(StringUtils.hexString2Bytes(hexString)));
                        // device type
                        String deviceType = mJsonParserWrapper.jsonGetString(jsonObject, "device_type");
                        if (deviceType.equals("3")) {
                            umediafwStatus.setDeviceType(AppConstants.DeviceType.TOWER);
                        } else if (deviceType.equals("2")) {
                            umediafwStatus.setDeviceType(AppConstants.DeviceType.PLUG);
                        } else if (deviceType.equals("1")) {
                            umediafwStatus.setDeviceType(AppConstants.DeviceType.DESKTOP);
                        }
                        // fw status
                        if ((mJsonParserWrapper.jsonGetString(jsonObject, "fw_status")).equals("0")) {
                            umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UPDATING);
                        } else {// if ((mJsonParserWrapper.jsonGetString(jsonObject, "fw_status")).equals("1")) {
                            if (umediafwStatus.getFWVersion().equals(umediafwStatus.getFWNewestVerion())) {
                                umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UP_TO_DATE);
                            } else {
                                umediafwStatus.setFWStatus(FWStatus.UMEDIA_FW_UPDATE_AVAILABLE);
                                mbIsEnableUpdateButton = true;
                            }
                        }
                        //LogUtils.trace("FWList", umediafwStatus.toString());

                        mFWStatusList.add(umediafwStatus);

                    }
                }

                //handleResponse(apiResponse);
                if (apiResponse.httpStatusCode == 401) {
                    // handleUnauthorized(apiResponse);
                    //LogUtils.trace("AudioList", "401");
                    return;
                }

                ResultStatus resultStatus = new ResultStatus();
                resultStatus.actionCode = mActionCode;
                if (apiResponse.httpStatusCode == 200) {
                    resultStatus.success = true;
                    resultStatus.data = apiResponse.body;
                } else {
                    resultStatus.success = false;
                    resultStatus.errorMsg = apiResponse.errorMsg;
                }
                // 設置 API 回傳的結果讓 UI 響應
                mResultStatus.setValue(resultStatus);

            }
        });
    }

    // Timezone
    public ArrayList<String> getTimezoneList() {
        loadTimezoneList();
        return mTimezoneList;
    }

    private void loadTimezoneList() {
        String[] timezones = mContext.getResources().getStringArray(R.array.timezone);
        mTimezoneList = new ArrayList<String>(Arrays.asList(timezones));
    }

    // Client List
    public void getClientInfoList() {
        mActionCode = Code.Action.NetworkSetting.GET_CLIENT_INFO_LIST;
        mCloudManager.getClientInfoList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {

                //mIsLoading.postValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject fwStatusInfo = mJsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    //LogUtils.trace("ClientInfoList", "jsonObject: " + fwStatusInfo.toString());

                    mClientInfoList = mJsonParserWrapper.jsonToObject(fwStatusInfo, ClientInfoList.class);
                    //LogUtils.trace("ClientInfoList", "mClientInfoList: " + mClientInfoList.getClientInfoList().toString());
                    classifyClient();
                }
                //handleResponse(apiResponse);
                if (apiResponse.httpStatusCode == 401) {
                    // handleUnauthorized(apiResponse);
                    //LogUtils.trace("AudioList", "401");
                    return;
                }

                ResultStatus resultStatus = new ResultStatus();
                resultStatus.actionCode = mActionCode;
                if (apiResponse.httpStatusCode == 200) {
                    resultStatus.success = true;
                    resultStatus.data = apiResponse.body;
                } else {
                    resultStatus.success = false;
                    resultStatus.errorMsg = apiResponse.errorMsg;
                }
                // 設置 API 回傳的結果讓 UI 響應
                mResultStatus.setValue(resultStatus);

            }
        }, mOptions);
    }

    private void classifyClient() {
        ClientInfoList clientList = (ClientInfoList) mClientInfoList.clone();

        // (1)分類recent和long inactive
        mRecentClient.clear();
        mLongInactiveClient.clear();
        mBlockedClient.clear();

        for (ClientModel clientInfo : clientList.getClientInfoList()) {
            //LogUtils.trace("ClientInfoList1", clientInfo.toString());
            //if (clientInfo.getStatus() == 2) { //<=原本的判斷有漏
            if (!clientInfo.getDisallowed()) {
                // recent / long inactive
                if (clientInfo.getOnlineStatus() == ConnectionStatus.ONLINE) {
                    mRecentClient.add(clientInfo);

                    //以下是假資料
//                mRecentClient.add(clientInfo);
//                mRecentClient.add(clientInfo);
//                mRecentClient.add(clientInfo);
//                mRecentClient.add(clientInfo);
//                mLongInactiveClient.add(clientInfo);
//                mLongInactiveClient.add(clientInfo);
//                mLongInactiveClient.add(clientInfo);
//                mLongInactiveClient.add(clientInfo);
//                mLongInactiveClient.add(clientInfo);

                } else {
                    if (DateUtils.IsLongInactiveClient(clientInfo.getLastOnlineTimestamp())) {
                        mLongInactiveClient.add(clientInfo);
                        //以下是假資料
                        //mRecentClient.add(clientInfo);
                        //mLongInactiveClient.add(clientInfo);
                    } else {
                        mRecentClient.add(clientInfo);
                        //以下是假資料
                        //mRecentClient.add(clientInfo);
                    }
                }

//                // blocked (disallowed)
//                if (clientInfo.getDisallowed())

            } else {
                mBlockedClient.add(clientInfo);
            }

        }

        // 分類online和offline
        classifyClientByOnlineStatus();
    }

    private void classifyClientByOnlineStatus() {
        ArrayList<ClientModel> onlineList = new ArrayList<ClientModel>();
        ArrayList<ClientModel> offlineList = new ArrayList<ClientModel>();

        onlineList.clear();
        offlineList.clear();

        for (ClientModel clientInfo : mRecentClient) {
            if (clientInfo.getOnlineStatus() == ConnectionStatus.ONLINE) { // online
                onlineList.add(clientInfo);
            } else {                                  // offline
                offlineList.add(clientInfo);
            }
        }

        mRecentClient.clear();
        mRecentClient.addAll(onlineList);
        mRecentClient.addAll(offlineList);

        onlineList.clear();
        offlineList.clear();

        for (ClientModel clientInfo : mLongInactiveClient) {
            if (clientInfo.getOnlineStatus() == ConnectionStatus.ONLINE) { // online
                onlineList.add(clientInfo);
            } else {                                  // offline
                offlineList.add(clientInfo);
            }
        }

        mLongInactiveClient.clear();
        mLongInactiveClient.addAll(onlineList);
        mLongInactiveClient.addAll(offlineList);
    }

    public ArrayList<ClientModel> getmRecentClient() {
        return mRecentClient;
    }

    public ArrayList<ClientModel> getmLongInactiveClient() {
        return mLongInactiveClient;
    }

    public ArrayList<ClientModel> getmBlockedClient() {return mBlockedClient;}

    public void getDashboardInfo() {
        mActionCode = Code.Action.NetworkSetting.GET_DASHBOARD_INFO;
        mCloudManager.getDashboardInfo(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    mDataManager.setDashboardInfo((DashboardInfo) apiResponse.body);
                }
                if (apiResponse.httpStatusCode == 401) {
                    // handleUnauthorized(apiResponse);
                    LogUtils.trace("NotifyMsg", "401");
                    return;
                }

                ResultStatus resultStatus = new ResultStatus();
                resultStatus.actionCode = mActionCode;
                if (apiResponse.httpStatusCode == 200) {
                    resultStatus.success = true;
                    resultStatus.data = apiResponse.body;
                } else {
                    resultStatus.success = false;
                    resultStatus.errorMsg = apiResponse.errorMsg;
                }
                // 設置 API 回傳的結果讓 UI 響應
                mResultStatus.setValue(resultStatus);

            }
        });
    }

    // unblock client
    public void unblockClient(ClientModel clientModel) {
        mActionCode = Code.Action.Member.UNBLOCK_CLIENT;
        mCloudManager.unblockClient(clientModel, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                //handleResponse(apiResponse);
                handleResponseExceptLoadingStatus(apiResponse);
                ///////////////////////////////////
                // 延遲關loading畫面
                Handler mLoadingDelayHandler = new Handler(Looper.getMainLooper());
                Runnable mLinkingTimeoutRunnable = new Runnable() {
                    @Override
                    public void run() {
                        mIsLoading.setValue(false);
                    }
                };
                mLoadingDelayHandler.postDelayed(mLinkingTimeoutRunnable, 13000);
            }
        });
    }
    // remove client <= 改成block client
    public void removeClients(ArrayList<ClientModel> clientModelList) {
        mActionCode = Code.Action.Member.REMOVE_CLIENT;
        mCloudManager.removeClients(clientModelList, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
