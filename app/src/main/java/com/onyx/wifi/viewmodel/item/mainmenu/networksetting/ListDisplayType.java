package com.onyx.wifi.viewmodel.item.mainmenu.networksetting;

public enum ListDisplayType {
    NO_EXTENSION_BUTTON,
    SHOW_HIDE_EXTENSION_BUTTON,
    SHOW_DISPLAY_EXTENSION_BUTTON
}
