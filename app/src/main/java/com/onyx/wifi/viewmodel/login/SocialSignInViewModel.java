package com.onyx.wifi.viewmodel.login;

import android.app.Application;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.Code;

public class SocialSignInViewModel extends BaseSignInViewModel {

    public SocialSignInViewModel(@NonNull Application application) {
        super(application);
    }

    public void firebaseSignInWithGoogle(Intent data) {
        mActionCode = Code.Action.UserAccount.SOCIAL_SIGN_IN;

        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null) {
                LogUtils.trace("Google account: " + account.getEmail());
            }
            mIsLoading.setValue(true);
            mAccountManager.firebaseAuthWithGoogle(account, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        LogUtils.trace("Firebase sign in with Google credential: success");
                        socialSignUp();
                    } else {
                        LogUtils.trace("Firebase sign in with Google credential: failed");
                        String errorMsg = (task.getException() != null) ? task.getException().getMessage() : "";
                        if (errorMsg.equals(mContext.getString(R.string.err_firebase_sdk_network_error_msg))) {
                            errorMsg = mContext.getString(R.string.err_internet_connection_fail);
                        }
                        handleError(errorMsg);
                    }
                }
            });
        } else if (result.getStatus().getStatusCode() != 12501) {
            /* https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInStatusCodes
               12501: SIGN_IN_CANCELLED
               code 12501 代表使用者取消選擇 Google 帳號, 這種情況下不需要顯示 error message, 其他類型的錯誤則要顯示 */
            LogUtils.trace("Google sign in failed, status code: " + result.getStatus().getStatusCode());
            if (result.getStatus().getStatusCode() == 7) {
                // https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes.html#NETWORK_ERROR
                handleError(mContext.getString(R.string.err_internet_connection_fail));
            } else {
                handleError(mContext.getString(R.string.err_google_sign_in_fail));
            }
        }
    }

    public void firebaseSignInWithFacebook(AccessToken token) {
        mActionCode = Code.Action.UserAccount.SOCIAL_SIGN_IN;

        mIsLoading.setValue(true);
        // Firebase authenticate with Facebook credential
        mAccountManager.firebaseAuthWithFacebook(token, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    LogUtils.trace("Firebase sign in with Facebook credential: success");
                    socialSignUp();
                } else {
                    LogUtils.trace("Firebase sign in with Facebook credential: failed");
                    String errorMsg = (task.getException() != null) ? task.getException().getMessage() : "";
                    if (errorMsg.equals(mContext.getString(R.string.err_firebase_sdk_network_error_msg))) {
                        errorMsg = mContext.getString(R.string.err_internet_connection_fail);
                    }
                    handleError(errorMsg);
                }
            }
        });
    }

    private void socialSignUp() {
        // 使用 Firebase SDK 社群登入成功, 向 cloud 註冊帳號.
        mCloudManager.socialSignUp(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // mIsLoading 在前面使用 Firebase SDK 做社群登入時已經設定過了, do nothing
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    // 打 cloud API 取得 user info
                    getUserInfo();
                } else {
                    handleResponse(apiResponse);
                }
            }
        });
    }
}
