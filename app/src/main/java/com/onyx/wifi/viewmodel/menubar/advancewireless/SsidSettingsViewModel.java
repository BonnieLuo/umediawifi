package com.onyx.wifi.viewmodel.menubar.advancewireless;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless2G;
import com.onyx.wifi.model.item.advancewireless.ssid.Wireless5G;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class SsidSettingsViewModel extends BaseViewModel {

    public SsidSettingsViewModel(@NonNull Application application) {
        super(application);
    }

    public void updateSSID(Wireless5G wireless5G, Wireless2G wireless2G) {
        mActionCode = Code.Action.AdvanceWirelessSettings.SET_SSID;
        mCloudManager.setupSSID(wireless5G, wireless2G, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
