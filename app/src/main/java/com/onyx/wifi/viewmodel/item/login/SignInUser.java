package com.onyx.wifi.viewmodel.item.login;

import com.onyx.wifi.utility.encrypt.EncryptUtils;

public class SignInUser {

    private String email;
    private String password;

    public SignInUser() {
        email = "";
        password = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "SignInUser{ " +
                "email = \"" + email + "\"" +
                // 為了安全性, 打印出來的密碼需加密過, 不能以明文顯示
                ", password = \"" + EncryptUtils.shaEncrypt(password) + "\"" +
                " }";
    }
}
