package com.onyx.wifi.viewmodel.mainmenu.member.schedule;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class ScheduleConfigurationListViewModel extends BaseViewModel {

    public ScheduleConfigurationListViewModel(@NonNull Application application) {
        super(application);
    }

    public void getMemberScheduleList(final String memberId) {
        mActionCode = Code.Action.Member.GET_MEMBER_SCHEDULE_LIST;
        mCloudManager.getMemberScheduleList(memberId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
