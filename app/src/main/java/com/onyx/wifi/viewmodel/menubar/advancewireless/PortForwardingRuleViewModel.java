package com.onyx.wifi.viewmodel.menubar.advancewireless;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.advancewireless.port.PortForwardingRule;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class PortForwardingRuleViewModel extends BaseViewModel {

    public PortForwardingRuleViewModel(@NonNull Application application) {
        super(application);
    }

    public void updatePortForwardingRule(PortForwardingRule rule) {
        mActionCode = Code.Action.AdvanceWirelessSettings.UPDATE_PORT_FORWARDING;
        mCloudManager.updatePortForwardingRule(rule, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void removePortForwardingRule(PortForwardingRule rule) {
        mActionCode = Code.Action.AdvanceWirelessSettings.REMOVE_PORT_FORWARDING;
        mCloudManager.removePortForwardingRule(rule, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getAdvanceWirelessSettings() {
        mActionCode = Code.Action.AdvanceWirelessSettings.GET_SETTINGS;
        mCloudManager.getAdvanceWirelessSettings(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
