package com.onyx.wifi.viewmodel.item.menubar.dashboard;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MapItem {

    public enum ItemType {
        NODE, CLIENT
    }

    private ItemType itemType;
    private Object itemData;

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Object getItemData() {
        return itemData;
    }

    public void setItemData(Object itemData) {
        this.itemData = itemData;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
