package com.onyx.wifi.viewmodel.mainmenu.member.list;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.util.ArrayList;
import java.util.List;

public class MemberListViewModel extends BaseViewModel {

    public MemberListViewModel(@NonNull Application application) {
        super(application);
    }

    public void getMemberList() {
        mActionCode = Code.Action.Member.GET_MEMBER_LIST;
        mCloudManager.getMemberList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject json = (JsonObject) apiResponse.body;
                    if (json != null) {
                        JsonObject dataJSON = json.getAsJsonObject("data");

                        Gson gson = new Gson();
                        MemberList memberList = gson.fromJson(dataJSON, MemberList.class);

                        List<ClientModel> homeClientModelList = memberList.getHomeNetwork();

                        List<Client> recentlyActiveClients = new ArrayList<>();
                        for (ClientModel clientModel:homeClientModelList) {
                            Client client = new Client(clientModel);
                            recentlyActiveClients.add(client);
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setMemberList(memberList);
                        dataManager.setRecentlyActiveClients(recentlyActiveClients);
                    }
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void removeMember(String memberId) {
        mActionCode = Code.Action.Member.REMOVE_FAMILY_MEMBER;
        mCloudManager.removeMember(memberId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    // Pause

    public void pauseMember(String memberId) {
        mActionCode = Code.Action.Member.PAUSE_MEMBER;
        mCloudManager.pauseMember(memberId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void resumeMember(String memberId, String ruleId) {
        mActionCode = Code.Action.Member.RESUME_MEMBER;
        mCloudManager.resumeMember(memberId, ruleId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
