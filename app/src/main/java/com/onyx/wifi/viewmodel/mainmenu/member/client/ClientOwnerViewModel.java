package com.onyx.wifi.viewmodel.mainmenu.member.client;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.FamilyMember;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.model.item.member.MemberModel;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.util.ArrayList;
import java.util.List;

public class ClientOwnerViewModel extends BaseViewModel {

    public ClientOwnerViewModel(@NonNull Application application) {
        super(application);
    }

    public void getMemberList() {
        mActionCode = Code.Action.Member.GET_MEMBER_LIST;
        mCloudManager.getMemberList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject json = (JsonObject) apiResponse.body;
                    if (json != null) {
                        JsonObject dataJSON = json.getAsJsonObject("data");

                        Gson gson = new Gson();
                        MemberList memberList = gson.fromJson(dataJSON, MemberList.class);

                        DataManager dataManager = DataManager.getInstance();

                        List<ClientModel> homeClientModelList = memberList.getHomeNetwork();

                        Client currentClient = dataManager.getManageClient();
                        String currentClientMac = currentClient.getCid();

                        List<Client> recentlyActiveClients = new ArrayList<>();
                        for (ClientModel clientModel:homeClientModelList) {
                            Client client = new Client(clientModel);
                            recentlyActiveClients.add(client);

                            String clientModelMac = clientModel.getCid();
                            if (clientModelMac.equalsIgnoreCase(currentClientMac)) {
                                Client updateClient = new Client(clientModel);
                                dataManager.setManageClient(updateClient);
                            }
                        }

                        List<MemberModel> familyMemberList = memberList.getFamilyMemberList();
                        for (MemberModel memberModel:familyMemberList) {
                            String memberId = memberModel.getMemberId();

                            FamilyMember currentMember = dataManager.getManageMember();

                            if (currentMember != null) {
                                String currentMemberId = currentMember.getId();

                                if (memberId.equalsIgnoreCase(currentMemberId)) {
                                    dataManager.setManageMember(new FamilyMember(memberModel));
                                }
                            }

                            List<ClientModel> clientList = memberModel.getClientList();
                            for (ClientModel clientModel:clientList) {
                                String clientModelMac = clientModel.getCid();
                                if (clientModelMac.equalsIgnoreCase(currentClientMac)) {
                                    Client updateClient = new Client(clientModel);
                                    dataManager.setManageClient(updateClient);
                                }
                            }
                        }

                        dataManager.setMemberList(memberList);
                        dataManager.setRecentlyActiveClients(recentlyActiveClients);
                    }
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void assignClientToMember(String newOwnerId, Client client) {
        mActionCode = Code.Action.Member.ASSIGN_CLIENT_TO_MEMBER;
        mCloudManager.assignClientToMember(newOwnerId, client, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void removeClientFromMember(String ownerId, Client client) {
        mActionCode = Code.Action.Member.REMOVE_CLIENT_FROM_MEMBER;
        mCloudManager.removeClientFromMember(ownerId, client, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
