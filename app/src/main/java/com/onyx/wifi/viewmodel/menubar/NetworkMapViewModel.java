package com.onyx.wifi.viewmodel.menubar;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.NetworkMap;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class NetworkMapViewModel extends BaseViewModel {

    private NetworkMap mNetworkMapData = new NetworkMap();

    public NetworkMapViewModel(@NonNull Application application) {
        super(application);
    }

    public void getNodeListForConnTest(boolean showLoadingStatus) {
        mActionCode = Code.Action.Dashboard.GET_NODE_LIST_FOR_CONN_TEST;
        // 執行 connection test 要帶 did 為參數, 所以先打 get node list 取得有哪些 device
        mCloudManager.getNodeList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                if (showLoadingStatus) {
                    mIsLoading.setValue(true);
                }
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject json = (JsonObject) apiResponse.body;
                    if (json != null) {
                        JsonArray dataJson = json.getAsJsonArray(AppConstants.KEY_DATA);
                        Type listType = new TypeToken<ArrayList<DeviceInfo>>() {
                        }.getType();
                        ArrayList<DeviceInfo> nodeList = new JsonParserWrapper().jsonToObject(dataJson.toString(), listType);
                        mDataManager.setNodeList(nodeList);
                    }
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void deviceConnectionTest(String did) {
        mCloudManager.deviceConnectionTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

            }
        }, did);
    }

    public void getNetworkMap(String nodeDid) {
        mActionCode = Code.Action.Dashboard.GET_NETWORK_MAP;
        mCloudManager.getNetworkMap(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 呼叫 get network map 時, 有時是在前景有時是在背景
                //  - 在前景被呼叫時已經先由 getNodeListForConnTest() 設定了 loading 狀態了
                //  - 在背景被呼叫時不需要顯示 loading 狀態
                // 故這裡不做事, 讓 loading status 維持原本的狀態
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    NetworkMap networkMap = jsonParserWrapper.jsonToObject(dataJson, NetworkMap.class);
                    mDataManager.setNodeInfoFromNetworkMap(networkMap);
                    mNetworkMapData = networkMap;
                }

                handleResponse(apiResponse);
            }
        }, nodeDid);
    }

    public NetworkMap getNetworkMapData() {
        return mNetworkMapData;
    }
}
