package com.onyx.wifi.viewmodel.menubar;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class NodeListViewModel extends BaseViewModel {

    public NodeListViewModel(@NonNull Application application) {
        super(application);
    }

    public void getNodeListForConnTest() {
        mActionCode = Code.Action.Dashboard.GET_NODE_LIST_FOR_CONN_TEST;
        // 執行 connection test 要帶 did 為參數, 所以先打 get node list 取得有哪些 device
        mCloudManager.getNodeList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    handleNodeListData((JsonObject) apiResponse.body);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void deviceConnectionTest(String did) {
        mCloudManager.deviceConnectionTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

            }
        }, did);
    }

    public void getNodeList() {
        mActionCode = Code.Action.Dashboard.GET_NODE_LIST;
        mCloudManager.getNodeList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    handleNodeListData((JsonObject) apiResponse.body);
                }
                handleResponse(apiResponse);
            }
        });
    }

    private void handleNodeListData(JsonObject apiResponseBody) {
        if (apiResponseBody != null) {
            JsonArray dataJson = apiResponseBody.getAsJsonArray(AppConstants.KEY_DATA);
            Type listType = new TypeToken<ArrayList<DeviceInfo>>() {
            }.getType();
            ArrayList<DeviceInfo> nodeList = new JsonParserWrapper().jsonToObject(dataJson.toString(), listType);
            mDataManager.setNodeList(nodeList);
        }
    }
}
