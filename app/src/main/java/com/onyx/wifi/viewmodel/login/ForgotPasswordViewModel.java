package com.onyx.wifi.viewmodel.login;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.R;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.login.ForgotPasswordUser;
import com.onyx.wifi.viewmodel.item.login.SignInUser;

public class ForgotPasswordViewModel extends BaseSignInViewModel {

    private ForgotPasswordUser mForgetPasswordUser = new ForgotPasswordUser();
    private String mInputErrorMsg = "";

    public ForgotPasswordViewModel(@NonNull Application application) {
        super(application);
    }

    public ForgotPasswordUser getForgetPasswordUser() {
        return mForgetPasswordUser;
    }

    public String getInputErrorMsg() {
        return mInputErrorMsg;
    }

    public boolean isEmailDataFilled() {
        if (mForgetPasswordUser == null) {
            return false;
        } else {
            return InputUtils.isNotEmpty(mForgetPasswordUser.getEmail());
        }
    }

    public boolean isEmailDataValid() {
        if (mForgetPasswordUser == null) {
            return false;
        } else {
            if (!InputUtils.isEmailValid(mForgetPasswordUser.getEmail())) {
                mInputErrorMsg = mContext.getString(R.string.err_email_not_valid);
                return false;
            }
            return true;
        }
    }

    public boolean isPasswordDataFilled() {
        if (mForgetPasswordUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mForgetPasswordUser.getPassword());
            filled &= InputUtils.isNotEmpty(mForgetPasswordUser.getConfirmPassword());
            return filled;
        }
    }

    public boolean isPasswordDataValid() {
        if (mForgetPasswordUser == null) {
            return false;
        } else {
            if (!InputUtils.isPasswordLengthEnough(mForgetPasswordUser.getPassword())) {
                mInputErrorMsg = mContext.getString(R.string.err_pwd_too_short);
                return false;
            }
            if (!mForgetPasswordUser.getConfirmPassword().equals(mForgetPasswordUser.getPassword())) {
                mInputErrorMsg = mContext.getString(R.string.err_pwd_not_the_same);
                return false;
            }
            return true;
        }
    }

    public void sendCode() {
        mActionCode = Code.Action.UserAccount.FORGOT_PASSWORD_SEND_CODE;
        requestSendCode();
    }

    public void resendCode() {
        mActionCode = Code.Action.UserAccount.FORGOT_PASSWORD_RESEND_CODE;
        requestSendCode();
    }

    private void requestSendCode() {
        mCloudManager.forgotPassword(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 404) {
                    // Bug #7619 (https://umedia.plan.io/issues/7619)
                    handleError(mContext.getString(R.string.err_email_or_password_invalid));
                } else {
                    handleResponse(apiResponse);
                }
            }
        }, mForgetPasswordUser.getEmail());
    }

    public void verifyResetPassword() {
        mActionCode = Code.Action.UserAccount.FORGOT_PASSWORD_VERIFY_RESET_PASSWORD;
        mCloudManager.verifyResetPassword(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    // Firebase sign in
                    SignInUser signInUser = new SignInUser();
                    signInUser.setEmail(mForgetPasswordUser.getEmail());
                    signInUser.setPassword(mForgetPasswordUser.getPassword());
                    firebaseSignIn(signInUser);
                } else {
                    handleResponse(apiResponse);
                }
            }
        }, mForgetPasswordUser);
    }
}
