package com.onyx.wifi.viewmodel.mainmenu.member.client;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.member.Client;
import com.onyx.wifi.model.item.member.MemberList;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.util.ArrayList;
import java.util.List;

public class ClientSettingViewModel extends BaseViewModel {

    public ClientSettingViewModel(@NonNull Application application) {
        super(application);
    }

    public void updateDeviceSetting(ClientModel clientModel) {
        mActionCode = Code.Action.Member.UPDATE_CLIENT_SETTINGS;

        mCloudManager.updateClient(clientModel, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    // block client
    public void blockClient(ClientModel clientModel) {
        mActionCode = Code.Action.Member.BLOCK_CLIENT;
        mCloudManager.blockClient(clientModel, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
                // 需要在Activity裡切換畫面，以結束loading的動作移至Activity裡判斷打cloud api成功之後
//                handleResponseExceptLoadingStatus(apiResponse);
//                ///////////////////////////////////
//                // 延遲關loading畫面
//                Handler mLoadingDelayHandler = new Handler(Looper.getMainLooper());
//                Runnable mLinkingTimeoutRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        mIsLoading.setValue(false);
//                    }
//                };
//                mLoadingDelayHandler.postDelayed(mLinkingTimeoutRunnable, 13000);
            }
        });
    }

    public void closeLoadingView() {
        mIsLoading.setValue(false);
    }

    public void pauseClient(String cid, int minutes) {
        mActionCode = Code.Action.Member.PAUSE_CLIENT;
        mCloudManager.pasueClient(cid, minutes, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void resumeClient(String cid, String ruleId) {
        mActionCode = Code.Action.Member.RESUME_CLIENT;
        mCloudManager.resumeClient(cid, ruleId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getMemberList() {
        mActionCode = Code.Action.Member.GET_MEMBER_LIST;
        mCloudManager.getMemberList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject json = (JsonObject) apiResponse.body;
                    if (json != null) {
                        JsonObject dataJSON = json.getAsJsonObject("data");

                        Gson gson = new Gson();
                        MemberList memberList = gson.fromJson(dataJSON, MemberList.class);

                        List<ClientModel> homeClientModelList = memberList.getHomeNetwork();

                        List<Client> recentlyActiveClients = new ArrayList<>();
                        for (ClientModel clientModel:homeClientModelList) {
                            Client client = new Client(clientModel);
                            recentlyActiveClients.add(client);
                        }

                        DataManager dataManager = DataManager.getInstance();
                        dataManager.setMemberList(memberList);
                        dataManager.setRecentlyActiveClients(recentlyActiveClients);
                    }
                }
                handleResponse(apiResponse);
            }
        });
    }

}
