package com.onyx.wifi.viewmodel.menubar.advancewireless;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.advancewireless.port.PortTriggeringRule;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class PortTriggeringRuleViewModel extends BaseViewModel {

    public PortTriggeringRuleViewModel(@NonNull Application application) {
        super(application);
    }

    public void updatePortTriggeringRule(PortTriggeringRule rule) {
        mActionCode = Code.Action.AdvanceWirelessSettings.UPDATE_PORT_TRIGGERING;
        mCloudManager.updatePortTriggeringRule(rule, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void removePortTriggeringRule(PortTriggeringRule rule) {
        mActionCode = Code.Action.AdvanceWirelessSettings.REMOVE_PORT_TRIGGERING;
        mCloudManager.removePortTriggeringRule(rule, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void getAdvanceWirelessSettings() {
        mActionCode = Code.Action.AdvanceWirelessSettings.GET_SETTINGS;
        mCloudManager.getAdvanceWirelessSettings(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
