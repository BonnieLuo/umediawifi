package com.onyx.wifi.viewmodel.item.login;

import com.onyx.wifi.utility.encrypt.EncryptUtils;

public class ForgotPasswordUser {

    private String email;
    private String resetCode;
    private String password;
    private String confirmPassword;

    public ForgotPasswordUser() {
        email = "";
        resetCode = "";
        password = "";
        confirmPassword = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return "ForgotPasswordUser{ " +
                "email = \"" + email + "\"" +
                ", resetCode = \"" + resetCode + "\"" +
                // 為了安全性, 打印出來的密碼需加密過, 不能以明文顯示
                ", password = \"" + EncryptUtils.shaEncrypt(password) + "\"" +
                ", confirmPassword = \"" + EncryptUtils.shaEncrypt(confirmPassword) + "\"" +
                " }";
    }
}
