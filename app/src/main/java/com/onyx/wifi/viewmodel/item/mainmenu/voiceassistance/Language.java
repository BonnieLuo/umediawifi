package com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance;

public class Language {
    private String mLang;

    public Language(String sLang) {
        mLang = sLang;
    }

    public String getLang() {
        return mLang;
    }

    public void setLang(String sLang) {
        this.mLang = sLang;
    }

    @Override
    public String toString() {
        return "Language{ " +
                "language = \"" + mLang + "\"" +
                " }";
    }
}
