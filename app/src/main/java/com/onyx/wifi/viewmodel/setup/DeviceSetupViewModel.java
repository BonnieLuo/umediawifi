package com.onyx.wifi.viewmodel.setup;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.FirmwareManager;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.BleControlModel;
import com.onyx.wifi.model.FirmwareModel;
import com.onyx.wifi.model.item.ProvisionToken;
import com.onyx.wifi.model.item.WirelessInfo2g;
import com.onyx.wifi.model.item.WirelessInfo5g;
import com.onyx.wifi.model.item.WirelessSetting;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.utility.StringUtils;
import com.onyx.wifi.utility.encrypt.EncryptUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;
import com.onyx.wifi.viewmodel.item.setup.PppoeInfo;
import com.onyx.wifi.viewmodel.item.setup.SetupDevice;
import com.onyx.wifi.viewmodel.item.setup.StaticIpInfo;
import com.onyx.wifi.viewmodel.item.setup.WirelessInfo;

import java.nio.charset.StandardCharsets;
import java.util.TimeZone;

/******************************************************************************
 *  Important!!
 *  FW team 提供的 device setup 流程圖:
 *      https://www.draw.io/#G17IYCM0adhhI4Q_W8kZHD73Ic9CEdrTh5
 *      (App team 公用 Google 帳號 (app.developer.umedia) 有權限可以開啟)
 ******************************************************************************/

public class DeviceSetupViewModel extends BaseViewModel {

    private final int TIMEOUT_10 = 10000;   // 10 seconds
    private final int TIMEOUT_20 = 20000;   // 20 seconds
    private final int TIMEOUT_30 = 30000;   // 30 seconds

    private BleControlModel mBleControlModel;
    private FirmwareModel mFirmwareModel;
    private FirmwareManager mFirmwareManager;

    private JsonParserWrapper mJsonParser;

    private Handler mHandler;

    private boolean mIsBleFlow;
    private boolean mIsRouterMode;
    private boolean mIsFirstTimeSetProvisionToken;
    private boolean mIsBleEnabled;

    private String mUid;    // 以 MD5 加密 uid
    private String mLocation;
    private String mProvisionToken;

    private AppConstants.SetupFlow mSetupFlow;
    private AppConstants.SetupMode mSetupMode;
    private SetupDevice mSetupDevice;

    private int mCommandRetryCount = 0;

    // for router
    private PppoeInfo mPppoeInfo = new PppoeInfo();    // 使用者在 UI 上輸入的資料
    private StaticIpInfo mStaticIpInfo = new StaticIpInfo();    // 使用者在 UI 上輸入的資料
    private WirelessInfo mWirelessInfo = new WirelessInfo();    // 使用者在 UI 上輸入的資料

    // for repeater
    private WirelessSetting mWirelessSetting;   // 從 cloud 拿到的資料 (App 儲存下來的)
    private WirelessInfo2g mWirelessInfo2g;   // 從 cloud 拿到的資料 (App 儲存下來的)
    private WirelessInfo5g mWirelessInfo5gBh0;   // 從 cloud 拿到的資料 (App 儲存下來的)
    private WirelessInfo5g mWirelessInfo5gBh1;   // 從 cloud 拿到的資料 (App 儲存下來的)

    private String mInputErrorMsg = "";

    public DeviceSetupViewModel(@NonNull Application application) {
        super(application);

        mBleControlModel = BleControlModel.getInstance();
        mFirmwareModel = new FirmwareModel();
        mFirmwareManager = FirmwareManager.getInstance();

        mHandler = new Handler();
        mJsonParser = new JsonParserWrapper();

        mUid = EncryptUtils.md5Encrypt(mDataManager.getUserId());
    }

    public AppConstants.SetupFlow getSetupFlow() {
        return mSetupFlow;
    }

    public void setSetupFlow(AppConstants.SetupFlow setupFlow) {
        mSetupFlow = setupFlow;

        switch (mSetupFlow) {
            case BLE:
                mIsBleFlow = true;
                break;

            case WIFI:
                mIsBleFlow = false;
                break;
        }
    }

    public AppConstants.SetupMode getSetupMode() {
        return mSetupMode;
    }

    public void setSetupMode(AppConstants.SetupMode setupMode) {
        mSetupMode = setupMode;

        switch (mSetupMode) {
            case ROUTER:
            case ROUTER_RESETUP:
                mIsRouterMode = true;
                break;

            case REPEATER:
                mIsRouterMode = false;
                break;
        }
    }

    public void setSetupDevice(SetupDevice setupDevice) {
        mSetupDevice = setupDevice;
    }

    public SetupDevice getSetupDevice() {
        return mSetupDevice;
    }

    public void getProvisionToken(boolean firstTime) {
        mActionCode = Code.Action.DeviceSetup.GET_PROVISION_TOKEN;
        mCloudManager.getProvisionToken(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (firstTime) {
                    if (apiResponse.httpStatusCode == 200) {
                        mDataManager.setDeviceProvisionToken((ProvisionToken) apiResponse.body);
                    }
                    if (mIsRouterMode) {
                        handleResponse(apiResponse);
                    } else {
                        // setup repeater 時, 打完 get provision token 還要再打 get wireless setting, 所以維持 loading 狀態
                        handleResponseExceptLoadingStatus(apiResponse);
                    }
                } else {
                    if (apiResponse.httpStatusCode == 200) {
                        mDataManager.setDeviceProvisionToken((ProvisionToken) apiResponse.body);
                        mIsFirstTimeSetProvisionToken = false;
                        setProvisionToken();
                    } else {
                        handleResponse(apiResponse);
                    }
                }
            }
        });
    }

    public void getWirelessSetting() {
        mActionCode = Code.Action.DeviceSetup.GET_WIRELESS_SETTING;
        mCloudManager.getWirelessSetting(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    JsonObject settingsJson = jsonParserWrapper.jsonGetJsonObject(dataJson, "settings");
                    WirelessSetting wirelessSetting = jsonParserWrapper.jsonToObject(settingsJson, WirelessSetting.class);
                    mDataManager.setWirelessSetting(wirelessSetting);
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void connectDevice() {
        // 只有 BLE setup flow 才需要執行藍牙的 connect/disconnect device
        if (mIsBleFlow && mSetupDevice != null && mSetupDevice.getBleDevice() != null) {
            mBleControlModel.connectDevice(mSetupDevice.getBleDevice());
            mHandler.postDelayed(mConnectionTimeoutRunnable, TIMEOUT_30);
        }
    }

    public void disconnectDevice() {
        // 只有 BLE setup flow 才需要執行藍牙的 connect/disconnect device
        if (mIsBleFlow) {
            mBleControlModel.disconnectDevice();
        }
    }

    public void registerBleActionReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppConstants.INTENT_BLE_ACTION_GATT_CONNECTED);
        filter.addAction(AppConstants.INTENT_BLE_ACTION_GATT_DISCONNECTED);
        filter.addAction(AppConstants.INTENT_BLE_ACTION_GATT_DISCOVERED);
        filter.addAction(AppConstants.INTENT_BLE_ACTION_GATT_NOTIFY);
        filter.addAction(AppConstants.INTENT_BLE_ACTION_DATA_CHANGE);
        mContext.registerReceiver(mActionReceiver, filter);
    }

    public void unregisterBleActionReceiver() {
        mContext.unregisterReceiver(mActionReceiver);
    }

    private Runnable mConnectionTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            LogUtils.trace("BLE connection timeout");
            disconnectDevice();
            setErrorMessage(Code.Action.DeviceSetup.CONNECT_DEVICE, "Cannot connect to device.");
        }
    };

    private Runnable mCommandTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            LogUtils.trace("BLE command timeout");
            disconnectDevice();
            setErrorMessage(mActionCode, "BLE command timeout.");
        }
    };

    private BroadcastReceiver mActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            LogUtils.trace("action: " + action);
            if (action.equals(AppConstants.INTENT_BLE_ACTION_GATT_CONNECTED)) {
                // connect 成功後的動作在 BleControlModel 的 onConnectionStateChange() 執行
                mHandler.removeCallbacks(mConnectionTimeoutRunnable);
                mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
                // 連線成功, 代表 device BLE 的功能是正常運作的
                mIsBleEnabled = true;
            } else if (action.equals(AppConstants.INTENT_BLE_ACTION_GATT_DISCONNECTED)) {
                disconnectDevice();
                LogUtils.trace("mIsBleEnabled: " + mIsBleEnabled);
                if (mIsBleEnabled) {
                    // setup 流程最後會將 device BLE 功能關閉, 就會收到 BLE gatt disconnect 的狀態, 此時是正常的斷線, 不需要通知 UI
                    // 反之若是 device BLE 功能正常運作的情況下收到 BLE gatt disconnect 的狀態, 是不正常的斷線, 需通知 UI
                    setErrorMessage(Code.Action.DeviceSetup.CONNECT_DEVICE, "Device disconnected.");
                }
            } else if (action.equals(AppConstants.INTENT_BLE_ACTION_GATT_DISCOVERED)) {
                mHandler.removeCallbacks(mCommandTimeoutRunnable);
                boolean isDiscovered = intent.getBooleanExtra(AppConstants.EXTRA_IS_DISCOVERED, false);
                LogUtils.trace("isDiscovered = " + isDiscovered);
                if (isDiscovered) {
                    // discover 成功後的動作在 BleControlModel 的 onServicesDiscovered() 執行
                    mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
                } else {
                    setErrorMessage(Code.Action.DeviceSetup.CONNECT_DEVICE, "Discovered service failed.");
                }
            } else if (action.equals(AppConstants.INTENT_BLE_ACTION_GATT_NOTIFY)) {
                mHandler.removeCallbacks(mCommandTimeoutRunnable);
                boolean isEnable = intent.getBooleanExtra(AppConstants.EXTRA_IS_ENABLE, false);
                LogUtils.trace("isEnable = " + isEnable);
                if (isEnable) {
                    mActionCode = Code.Action.DeviceSetup.CONNECT_DEVICE;
                    setSuccess(null);
                } else {
                    setErrorMessage(Code.Action.DeviceSetup.CONNECT_DEVICE, "Enable notify failed.");
                }
            } else if (action.equals(AppConstants.INTENT_BLE_ACTION_DATA_CHANGE)) {
                mHandler.removeCallbacks(mCommandTimeoutRunnable);
                byte[] data = intent.getByteArrayExtra(AppConstants.EXTRA_BLE_DATA);
                parseCommandResponse(data);
            }
        }
    };

    private <T> void setSuccess(T data) {
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        resultStatus.success = true;
        resultStatus.data = data;
        mResultStatus.setValue(resultStatus);
    }

    // 設定 error message 讓 UI 層可以直接顯示出來給使用者看
    // (應用情境: FW 回傳的 error message 直接顯示給使用者看)
    private void setErrorMessage(int actionCode, String errorMsg) {
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = actionCode;
        resultStatus.success = false;
        resultStatus.errorMsg = errorMsg + "\nPlease try again.";
        mResultStatus.setValue(resultStatus);
    }

    // 設定 error code 和 error message 讓 UI 層可以判斷要如何處理
    // (應用情境: FW 回傳的 error code 要經過判斷, 顯示自定義的 error message 或畫面給使用者看)
    private void setErrorResult(String errorCode, String errorMsg) {
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        resultStatus.success = false;
        resultStatus.errorMsg = errorMsg;
        resultStatus.data = errorCode;
        mResultStatus.setValue(resultStatus);
    }

    private void setWifiInternetError() {
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        resultStatus.success = false;
        resultStatus.errorMsg = "You are not connected to the device. Please follow the steps to connect to the setup WiFi network and try again.";
        mResultStatus.setValue(resultStatus);
    }

    private void setWifiApiError(String errorMsg) {
        if (errorMsg.equals(mContext.getString(R.string.err_internet_connection_fail))
                || errorMsg.equals(mContext.getString(R.string.err_internet_connection_fail))
                || errorMsg.equals(mContext.getString(R.string.err_internet_timeout))) {
            // 打 WIFI API 因為網路連線錯誤而失敗時, error message 有可能出現上述訊息
            // 如果出現這種連線錯誤的情況, 代表網路不通, 意即手機沒有連線上 device 預設的 setup WIFI SSID, 將要顯示給使用者看的訊息換成比較容易理解的內容
            setWifiInternetError();
        } else {
            // 若是其他種錯誤, 直接顯示原本的訊息內容
            setErrorMessage(mActionCode, errorMsg);
        }
    }

    private JsonObject getRequestDataWithUid(JsonObject jsonObject) {
        // 在 request data JsonObject 內加入 "uid" key/value
        // 若傳入的 JsonObject 是 null, 則回傳一個只有 "uid" 的 JsonObject
        JsonObject result = (jsonObject == null) ? new JsonObject() : jsonObject;
        result.addProperty("uid", mUid);
        return result;
    }

    public void startDeviceSetup() {
        if (mSetupMode == AppConstants.SetupMode.REPEATER) {
            // 後面 setup repeater 的步驟需要 wireless setting 及 wireless info 資料
            mWirelessSetting = mDataManager.getWirelessSetting();
            mWirelessInfo2g = mDataManager.getWirelessInfo2g();
            mWirelessInfo5gBh0 = mDataManager.getWirelessInfo5gFronthaul();
            mWirelessInfo5gBh1 = mDataManager.getWirelessInfo5gBackhaul();
        }

        setUserId();
    }

    private void setUserId() {
        LogUtils.trace("set user ID");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_USER_ID;

        JsonObject requestData = getRequestDataWithUid(null);
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_USER_ID, requestData));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setUserId(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_USER_ID), requestData);
        }
    }

    private void setTimezone() {
        LogUtils.trace("set timezone");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_TIMEZONE;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("city", getMatchedTimezone());
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_TIMEZONE, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setTimezone(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_TIMEZONE), requestData);
        }
    }

    /**
     * Device Provision Flow:
     * 1. App 與 Cloud 取得一個暫時性的 user_provision_token (此 token 於 Cloud 上只能取得 device token)
     * 2. App 將暫時性的 user_provision_token 於 setup device 時, 傳遞給 device
     * 3. 當 Device 設定好可以連上 Internet 後, 利用暫時性的 user_provision_token 與 Device 的 udid 向 Cloud 取得 device token
     * 4. Device 已跟 Cloud 取得 device token 後, 就會開始進行 Device Provision 的程序
     */
    private void setProvisionToken() {
        LogUtils.trace("set provision token");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_PROVISION_TOKEN;

        JsonObject requestData = new JsonObject();
        mProvisionToken = mDataManager.getDeviceProvisionToken().getToken();
        requestData.addProperty("ProvisionToken", mProvisionToken);
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_PROVISION_TOKEN, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setProvisionToken(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_PROVISION_TOKEN), requestData);
        }
    }

    private void getDeviceBoardId() {
        LogUtils.trace("get device board ID");
        mActionCode = Code.Action.DeviceFirmwareCommand.GET_DEVICE_BOARD_ID;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.GET_DEVICE_BOARD_ID, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.getDeviceBoardId(getWifiApiCallback(FirmwareModel.BleResponseCommand.GET_DEVICE_BOARD_ID));
        }
    }

    private void setDeviceOperationMode() {
        LogUtils.trace("set device operation mode");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_DEVICE_OPERATION_MODE;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("OpMode", (mIsRouterMode) ? "router" : "repeater");
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_DEVICE_OPERATION_MODE, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setDeviceOperationMode(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_DEVICE_OPERATION_MODE), requestData);
        }
    }

    private void set5gApClientWifiSsid() {
        LogUtils.trace("set 5G ApClient Wifi SSID");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "5");
        requestData.addProperty("ssid", StringUtils.encodeToHexString(mWirelessInfo5gBh1.getSsid()));
        requestData.addProperty("bssid", mWirelessInfo5gBh1.getMac());
        requestData.addProperty("ch", mWirelessInfo5gBh1.getCh());
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_5G_AP_CLIENT_WIFI, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set5gApClientWifiSsid(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_5G_AP_CLIENT_WIFI), requestData);
        }
    }

    private void set5gApClientWifiPassword() {
        LogUtils.trace("set 5G ApClient Wifi password");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI_KEY;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "5");
        requestData.addProperty("key", StringUtils.encodeToHexString(mWirelessInfo5gBh1.getPwd()));
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_5G_AP_CLIENT_WIFI_KEY, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set5gApClientWifiPassword(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_5G_AP_CLIENT_WIFI_KEY), requestData);
        }
    }

    private void set5gApClientWifiSecurity() {
        LogUtils.trace("set 5G ApClient Wifi security");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI_SEC;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "5");
        requestData.addProperty("auth", mWirelessInfo5gBh1.getAuth());
        requestData.addProperty("enc", mWirelessInfo5gBh1.getEnc());
        requestData.addProperty("wep_dkey", "");
        requestData.addProperty("wep_keytype", "");
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_5G_AP_CLIENT_WIFI_SEC, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set5gApClientWifiSecurity(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_5G_AP_CLIENT_WIFI_SEC), requestData);
        }
    }

    private void setApClientWifiAction() {
        LogUtils.trace("set ApClient Wifi action");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_AP_CLIENT_WIFI_ACTION;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_AP_CLIENT_WIFI_ACTION, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setApClientWifiAction(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_AP_CLIENT_WIFI_ACTION));
        }
    }

    private void get5gApClientConnectionStatus() {
        LogUtils.trace("set 5G ApClient connection status");
        mActionCode = Code.Action.DeviceFirmwareCommand.GET_5G_AP_CLIENT_CONN_STATUS;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "5");
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.GET_5G_AP_CLIENT_CONN_STATUS, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.get5gApClientConnectionStatus(getWifiApiCallback(FirmwareModel.BleResponseCommand.GET_5G_AP_CLIENT_CONN_STATUS));
        }
    }

    public void detectWanType() {
        LogUtils.trace("detect WAN type");
        mActionCode = Code.Action.DeviceFirmwareCommand.DETECT_WAN_TYPE;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.DETECT_WAN_TYPE, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.detectWanType(getWifiApiCallback(FirmwareModel.BleResponseCommand.DETECT_WAN_TYPE));
        }
    }

    private void getWanType() {
        LogUtils.trace("get WAN type");
        mActionCode = Code.Action.DeviceFirmwareCommand.GET_WAN_TYPE;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.GET_WAN_TYPE, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.getWanType(getWifiApiCallback(FirmwareModel.BleResponseCommand.GET_WAN_TYPE));
        }
    }

    public void setWanDhcp() {
        LogUtils.trace("set WAN DHCP");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_WAN_DHCP;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_WAN_DHCP, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_20);
        } else {
            mFirmwareManager.setWanDhcp(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_WAN_DHCP));
        }
    }

    public void setPppoe() {
        LogUtils.trace("set PPPoE");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_WAN_PPPOE;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("user", StringUtils.encodeToHexString(mPppoeInfo.getUsername()));
        requestData.addProperty("pw", StringUtils.encodeToHexString(mPppoeInfo.getPassword()));
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_WAN_PPPOE, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_20);
        } else {
            mFirmwareManager.setPppoe(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_WAN_PPPOE), requestData);
        }
    }

    public void setStaticIp() {
        LogUtils.trace("set static IP");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_WAN_STATIC_IP;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("ip", mStaticIpInfo.getIp());
        requestData.addProperty("mask", mStaticIpInfo.getMask());
        requestData.addProperty("gw", mStaticIpInfo.getGateway());
        requestData.addProperty("dns1", mStaticIpInfo.getDns1());
        requestData.addProperty("dns2", mStaticIpInfo.getDns2());
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_WAN_STATIC_IP, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_20);
        } else {
            mFirmwareManager.setStaticIp(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_WAN_STATIC_IP), requestData);
        }
    }

    private void getInternetConnectionStatus() {
        LogUtils.trace("get Internet connection status");
        mActionCode = Code.Action.DeviceFirmwareCommand.GET_INTERNET_CONN_STATUS;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.GET_INTERNET_CONN_STATUS, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.getInternetConnectionStatus(getWifiApiCallback(FirmwareModel.BleResponseCommand.GET_INTERNET_CONN_STATUS));
        }
    }

    private void getDeviceTokenStatus() {
        LogUtils.trace("get device token status");
        mActionCode = Code.Action.DeviceFirmwareCommand.GET_DEVICE_TOKEN_STATUS;

        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.GET_DEVICE_TOKEN_STATUS, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.getDeviceTokenStatus(getWifiApiCallback(FirmwareModel.BleResponseCommand.GET_DEVICE_TOKEN_STATUS));
        }
    }

    public void setLocation() {
        LogUtils.trace("set location");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_LOCATION;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("Location", StringUtils.encodeToHexString(mLocation));
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_LOCATION, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setLocation(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_LOCATION), requestData);
        }
    }

    private void set2gWifiSsid() {
        LogUtils.trace("set 2.4G Wifi SSID");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_2G_WIFI;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "2");
        if (mSetupMode == AppConstants.SetupMode.ROUTER || mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
            requestData.addProperty("ssid", StringUtils.encodeToHexString(mWirelessInfo.getSsid2g()));
            requestData.addProperty("bandsteer", mWirelessInfo.getUseSame() ? "1" : "0");
        } else {
            requestData.addProperty("ssid", StringUtils.encodeToHexString(mWirelessInfo2g.getSsid()));
            requestData.addProperty("bandsteer", mWirelessSetting.getWifiSameEnabled() ? "1" : "0");
        }
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_2G_WIFI, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set2gWifiSsid(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_2G_WIFI), requestData);
        }
    }

    private void set2gWifiPassword() {
        LogUtils.trace("set 2.4G Wifi password");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_2G_WIFI_KEY;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "2");
        if (mSetupMode == AppConstants.SetupMode.ROUTER || mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
            requestData.addProperty("key", StringUtils.encodeToHexString(mWirelessInfo.getPassword2g()));
        } else {
            requestData.addProperty("key", StringUtils.encodeToHexString(mWirelessInfo2g.getPwd()));
        }
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_2G_WIFI_KEY, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set2gWifiPassword(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_2G_WIFI_KEY), requestData);
        }
    }

    private void set5gWifiSsid() {
        LogUtils.trace("set 5G Wifi SSID");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_5G_WIFI;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "5");
        if (mSetupMode == AppConstants.SetupMode.ROUTER || mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
            if (mWirelessInfo.getUseSame()) {
                // 5G 使用和 2.4G 一樣的 SSID/password
                requestData.addProperty("ssid", StringUtils.encodeToHexString(mWirelessInfo.getSsid2g()));
            } else {
                requestData.addProperty("ssid", StringUtils.encodeToHexString(mWirelessInfo.getSsid5g()));
            }
            requestData.addProperty("bandsteer", mWirelessInfo.getUseSame() ? "1" : "0");
        } else {
            requestData.addProperty("ssid", StringUtils.encodeToHexString(mWirelessInfo5gBh0.getSsid()));
            requestData.addProperty("bandsteer", mWirelessSetting.getWifiSameEnabled() ? "1" : "0");
        }
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_5G_WIFI, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set5gWifiSsid(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_5G_WIFI), requestData);
        }
    }

    private void set5gWifiPassword() {
        LogUtils.trace("set 5G Wifi password");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_5G_WIFI_KEY;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("radio", "5");
        if (mSetupMode == AppConstants.SetupMode.ROUTER || mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
            if (mWirelessInfo.getUseSame()) {
                // 5G 使用和 2.4G 一樣的 SSID/password
                requestData.addProperty("key", StringUtils.encodeToHexString(mWirelessInfo.getPassword2g()));
            } else {
                requestData.addProperty("key", StringUtils.encodeToHexString(mWirelessInfo.getPassword5g()));
            }
        } else {
            requestData.addProperty("key", StringUtils.encodeToHexString(mWirelessInfo5gBh0.getPwd()));
        }
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_5G_WIFI_KEY, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.set5gWifiPassword(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_5G_WIFI_KEY), requestData);
        }
    }

    // setup 流程最後要下指令將 BLE 的功能關閉
    private void setBleEnable() {
        LogUtils.trace("set BLE enable/disable");
        mActionCode = Code.Action.DeviceFirmwareCommand.SET_BLE_ENABLE;

        JsonObject requestData = new JsonObject();
        requestData.addProperty("enable", "0"); // 1: Enabled, 0: Disabled
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SET_BLE_ENABLE, getRequestDataWithUid(requestData)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setBleEnable(getWifiApiCallback(FirmwareModel.BleResponseCommand.SET_BLE_ENABLE), requestData);
        }
    }

    private void setupComplete() {
        LogUtils.trace("setup complete");
        mActionCode = Code.Action.DeviceFirmwareCommand.SETUP_COMPLETE;
        if (mIsBleFlow) {
            sendCommand(mFirmwareModel.getCommand(FirmwareModel.BleRequestCommand.SETUP_COMPLETE, getRequestDataWithUid(null)));
            mHandler.postDelayed(mCommandTimeoutRunnable, TIMEOUT_10);
        } else {
            mFirmwareManager.setupComplete(getWifiApiCallback(FirmwareModel.BleResponseCommand.SETUP_COMPLETE));
        }
    }

    private ApiCallback getWifiApiCallback(final String requestCommand) {
        return new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                setWifiInternetError();
            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    // 不論 setup 是走 BLE or WIFI 流程, 都統一由 parseCommandResponse() 判斷及處理結果
                    // BLE response JSON data 內會有 "cmd" 屬性標示是哪一個指令的結果, 而 WIFI response JSON data 裡面沒有,
                    // 故 WIFI response 需手動加上 "cmd" 屬性
                    JsonObject responseData = (JsonObject) apiResponse.body;
                    responseData.addProperty("cmd", requestCommand);
                    if (!responseData.has("status")) {
                        // 有些 WIFI API 打成功時, 回傳的內容沒有 "status" 屬性, 但同一支 API 的 BLE API 有回傳 "status" 屬性
                        // 由於要統一由 parseCommandResponse() 處理結果, 故手動加上 "status" 屬性
                        responseData.addProperty("status", 0);
                    }
                    parseCommandResponse(responseData.toString().getBytes());
                } else {
                    setWifiApiError(apiResponse.errorMsg);
                }
            }
        };
    }

    private void sendCommand(byte[] command) {
        mBleControlModel.writePayload(command);
    }

    private void parseCommandResponse(byte[] data) {
        JsonObject responseData = mJsonParser.parseJson(new String(data, StandardCharsets.UTF_8));
        String cmd = mJsonParser.jsonGetString(responseData, "cmd");
        // command status 回傳 "0" 代表成功 (大多數, 有些不一定)
        String status = mJsonParser.jsonGetString(responseData, "status");
        LogUtils.trace("cmd: " + cmd + ", status: " + status);

        String errorMsg = "Command failed. cmd: \"" + cmd + "\", status: " + status + ".";    // default error message
        String errorMsgNotMatch = "Response command doesn't match request command. cmd: \"" + cmd + "\", status: " + status + ".";

        switch (cmd) {
            // Set User ID
            case FirmwareModel.BleResponseCommand.SET_USER_ID:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_USER_ID) {
                    if (status.equals("0")) {
                        setTimezone();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set Timezone
            case FirmwareModel.BleResponseCommand.SET_TIMEZONE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_TIMEZONE) {
                    if (status.equals("0")) {
                        mIsFirstTimeSetProvisionToken = true;
                        setProvisionToken();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set Provision Token
            case FirmwareModel.BleResponseCommand.SET_PROVISION_TOKEN:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_PROVISION_TOKEN) {
                    if (status.equals("0")) {
                        // mIsFirstTimeSetProvisionToken = true 是第一次設定 provision token, 照著流程下一步是 get device board ID
                        // mIsFirstTimeSetProvisionToken = false 是第二次設定 provision token, 是因為 check token status 發現有問題,
                        // 重新向 cloud 再要一次 provision token 後設定給 device, 設定完之後要再一次 check device token status
                        // (provision token 和 device token 是不一樣的概念, 詳見 set provision token() 的說明)
                        if (mIsFirstTimeSetProvisionToken) {
                            getDeviceBoardId();
                        } else {
                            getDeviceTokenStatus();
                        }
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Get Device Board ID
            case FirmwareModel.BleResponseCommand.GET_DEVICE_BOARD_ID:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.GET_DEVICE_BOARD_ID) {
                    if (status.equals("0")) {
                        String boardId = mJsonParser.jsonGetString(responseData, "boardId");
                        // 0x56002: extender, 0x55001: router, 0x57001: Audio high, 0x57000: Audio low
                        // Audio 的產品可以做為 router 也可以做為 extender(repeater) 運作
                        if (boardId.equals("55001") && mIsRouterMode) {
                            // 產品是 router 而且 App 現在在 router setup flow
                            setDeviceOperationMode();
                        } else if (boardId.equals("56002") && !mIsRouterMode) {
                            // 產品是 extender 而且 App 現在在 repeater setup flow
                            setDeviceOperationMode();
                        } else if (boardId.equals("57000") || boardId.equals("57001")) {
                            // 產品是 Audio type
                            setDeviceOperationMode();
                        } else {
                            errorMsg = mContext.getString(R.string.err_board_id_not_support);
                            setErrorMessage(mActionCode, errorMsg);
                        }
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set Device Operation Mode
            case FirmwareModel.BleResponseCommand.SET_DEVICE_OPERATION_MODE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_DEVICE_OPERATION_MODE) {
                    if (status.equals("0")) {
                        if (mSetupMode == AppConstants.SetupMode.ROUTER || mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
                            detectWanType();
                        } else {
                            setLocation();
                        }
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 5G ApClient WiFi
            case FirmwareModel.BleResponseCommand.SET_5G_AP_CLIENT_WIFI:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI) {
                    if (status.equals("0")) {
                        set5gApClientWifiPassword();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 5G ApClient WiFi key(password)
            case FirmwareModel.BleResponseCommand.SET_5G_AP_CLIENT_WIFI_KEY:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI_KEY) {
                    if (status.equals("0")) {
                        set5gApClientWifiSecurity();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 5G ApClient WiFi Security
            case FirmwareModel.BleResponseCommand.SET_5G_AP_CLIENT_WIFI_SEC:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_5G_AP_CLIENT_WIFI_SEC) {
                    if (status.equals("0")) {
                        setApClientWifiAction();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set ApClient WiFi action
            case FirmwareModel.BleResponseCommand.SET_AP_CLIENT_WIFI_ACTION:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_AP_CLIENT_WIFI_ACTION) {
                    if (status.equals("0")) {
                        get5gApClientConnectionStatus();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Get 5G ApClient connection status
            case FirmwareModel.BleResponseCommand.GET_5G_AP_CLIENT_CONN_STATUS:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.GET_5G_AP_CLIENT_CONN_STATUS) {
                    // 0: connecting, 1: success
                    if (status.equals("1")) {
                        set2gWifiSsid();
                    } else if (status.equals("0")) {
                        // 還在連線中, 稍後再 get 一次狀態
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                get5gApClientConnectionStatus();
                            }
                        }, 3000);
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_631)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_631, mContext.getString(R.string.err_status_31));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_632)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_632, mContext.getString(R.string.err_status_32));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_691)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_691, mContext.getString(R.string.err_status_91));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_731)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_731, mContext.getString(R.string.err_status_31));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_732)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_732, mContext.getString(R.string.err_status_32));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_791)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_791, mContext.getString(R.string.err_status_91));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                }
                break;

            // Detect WAN Type
            case FirmwareModel.BleResponseCommand.DETECT_WAN_TYPE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.DETECT_WAN_TYPE) {
                    if (status.equals("0")) {
                        getWanType();
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Get WAN Type
            case FirmwareModel.BleResponseCommand.GET_WAN_TYPE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.GET_WAN_TYPE) {
                    if (status.equals("0")) {
                        String type = mJsonParser.jsonGetString(responseData, "type");
                        if (type.equals("detecting")) {
                            // 還在偵測中, 稍後再 get 一次狀態
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getWanType();
                                }
                            }, 3000);
                        } else if (type.equals("dhcp") || type.equals("pppoe") || type.equals("unknown")) {
                            // set success 結果 (回傳 data: WAN type) 給 UI 判斷下一步動作
                            setSuccess(type);
                        } else {
                            errorMsg = mContext.getString(R.string.err_wan_type_not_support);
                            setErrorMessage(mActionCode, errorMsg);
                        }
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set WAN Dhcp
            case FirmwareModel.BleResponseCommand.SET_WAN_DHCP:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_WAN_DHCP) {
                    if (status.equals("0")) {
                        getInternetConnectionStatus();
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_661)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_661, mContext.getString(R.string.err_status_61));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_761)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_761, mContext.getString(R.string.err_status_61));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set PPPoE
            case FirmwareModel.BleResponseCommand.SET_WAN_PPPOE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_WAN_PPPOE) {
                    if (status.equals("0")) {
                        getInternetConnectionStatus();
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_651)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_651, mContext.getString(R.string.err_status_51));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_652)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_652, mContext.getString(R.string.err_status_52));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_653)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_653, mContext.getString(R.string.err_status_53));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_654)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_654, mContext.getString(R.string.err_status_54));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_751)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_751, mContext.getString(R.string.err_status_51));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_752)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_752, mContext.getString(R.string.err_status_52));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_753)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_753, mContext.getString(R.string.err_status_53));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_754)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_754, mContext.getString(R.string.err_status_54));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set Static IP
            case FirmwareModel.BleResponseCommand.SET_WAN_STATIC_IP:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_WAN_STATIC_IP) {
                    if (status.equals("0")) {
                        getInternetConnectionStatus();
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Get Internet connection status
            case FirmwareModel.BleResponseCommand.GET_INTERNET_CONN_STATUS:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.GET_INTERNET_CONN_STATUS) {
                    // 0: connecting, 1: connected
                    if (status.equals("1")) {
                        mCommandRetryCount = 0;
                        getDeviceTokenStatus();
                    } else if (status.equals("0")) {
                        // 還在連線中, 稍後再 get 一次狀態
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getInternetConnectionStatus();
                            }
                        }, 3000);
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_641)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_641, mContext.getString(R.string.err_status_41));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_741)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_741, mContext.getString(R.string.err_status_41));
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Get device token status
            // (根據 FW 提供的 device setup 流程圖, router 和 repeater 都會 set provision token, 但只有 router 會呼叫 get device token status)
            case FirmwareModel.BleResponseCommand.GET_DEVICE_TOKEN_STATUS:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.GET_DEVICE_TOKEN_STATUS) {
                    if (status.equals("0")) {
                        // set success 結果 (不用回傳其他 data) 給 UI 判斷下一步動作
                        setSuccess(null);
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_615)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_615, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_671)
                            || (status.equals(Code.Error.DeviceSetup.STATUS_672))) {
                        mCommandRetryCount++;
                        if (mCommandRetryCount <= 5) {
                            getProvisionToken(false);
                        } else {
                            setErrorMessage(mActionCode, errorMsg);
                        }
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_715)) {
                        setErrorResult(Code.Error.DeviceSetup.STATUS_715, mContext.getString(R.string.err_status_15));
                    } else if (status.equals(Code.Error.DeviceSetup.STATUS_771)
                            || (status.equals(Code.Error.DeviceSetup.STATUS_772))) {
                        mCommandRetryCount++;
                        if (mCommandRetryCount <= 5) {
                            getProvisionToken(false);
                        } else {
                            setErrorMessage(mActionCode, errorMsg);
                        }
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set Location
            case FirmwareModel.BleResponseCommand.SET_LOCATION:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_LOCATION) {
                    if (status.equals("0")) {
                        if (mSetupMode == AppConstants.SetupMode.ROUTER || mSetupMode == AppConstants.SetupMode.ROUTER_RESETUP) {
                            set2gWifiSsid();
                        } else {
                            set5gApClientWifiSsid();
                        }
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 2.4G WiFi
            case FirmwareModel.BleResponseCommand.SET_2G_WIFI:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_2G_WIFI) {
                    if (status.equals("0")) {
                        set2gWifiPassword();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 2.4G WiFi key(password)
            case FirmwareModel.BleResponseCommand.SET_2G_WIFI_KEY:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_2G_WIFI_KEY) {
                    if (status.equals("0")) {
                        set5gWifiSsid();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 5G WiFi
            case FirmwareModel.BleResponseCommand.SET_5G_WIFI:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_5G_WIFI) {
                    if (status.equals("0")) {
                        set5gWifiPassword();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set 5G WiFi key(password)
            case FirmwareModel.BleResponseCommand.SET_5G_WIFI_KEY:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_5G_WIFI_KEY) {
                    if (status.equals("0")) {
                        setBleEnable();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Set BLE disable
            case FirmwareModel.BleResponseCommand.SET_BLE_ENABLE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SET_BLE_ENABLE) {
                    if (status.equals("0")) {
                        // set BLE disable 成功, BLE 的功能被關閉了, 稍後 device 會斷線, 此時的斷線是正常的, 不需要通知 UI, 所以用 flag 來判斷
                        mIsBleEnabled = false;
                        setupComplete();
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            // Setup complete
            case FirmwareModel.BleResponseCommand.SETUP_COMPLETE:
                if (mActionCode == Code.Action.DeviceFirmwareCommand.SETUP_COMPLETE) {
                    if (status.equals("0")) {
                        // set success 結果 (不用回傳其他 data) 給 UI 判斷下一步動作
                        setSuccess(null);
                    } else {
                        setErrorMessage(mActionCode, errorMsg);
                    }
                } else {
                    setErrorMessage(mActionCode, errorMsgNotMatch);
                }
                break;

            default:
                errorMsg = "Unknown command: " + cmd + ".";
                setErrorMessage(mActionCode, errorMsg);
                break;
        }
    }

    // device firmware 定義了 timezone list, 需將手機系統抓取到的 timezone 對應到已定義好的字串, 傳送給 device firmware
    private String getMatchedTimezone() {
        int index = -1;
        // device firmware 定義的 timezone list
        String[] timezones = mContext.getResources().getStringArray(R.array.timezone);
        // 手機系統抓取到的 timezone ID
        String timezoneId = TimeZone.getDefault().getID().replace("_", " ");
        LogUtils.trace("timezone ID: " + timezoneId);
        for (int i = 0; i < timezones.length; i++) {
            String timezone = timezones[i];
            if (timezone.equals(timezoneId)) {
                index = i;
                break;
            }
        }
        String definedTimezone;
        if (index != -1) {  // timezone 字串有匹配到
            definedTimezone = timezones[index];
        } else {
            definedTimezone = "America/New York";
        }
        return definedTimezone;
    }

    public void getProvisionState() {
        mActionCode = Code.Action.DeviceSetup.GET_PROVISION_STATE;
        mCloudManager.getProvisionState(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleResponse(apiResponse);
            }
        }, mProvisionToken);
    }

    public void checkIsConnectedToDevice() {
        // 打 WIFI API 檢測手機是否有連線上 device 預設的 setup WIFI SSID
        mActionCode = Code.Action.DeviceSetup.CHECK_SETUP_WIFI;
        mFirmwareManager.getDeviceBoardId(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                setWifiInternetError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                // 若有連線上 device 預設的 setup WIFI SSID, 打 API 才會通, 故成功回應即代表有連線上 device
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject responseData = (JsonObject) apiResponse.body;
                    if (mJsonParser.jsonGetString(responseData, "status").equals("719")) {
                        // 若 device 已經 provision 完成, 第二次再進入 WIFI setup 流程, 打 API 會回傳 status 719
                        setErrorMessage(mActionCode, "Please press setup button of device.");
                    } else {
                        ResultStatus resultStatus = new ResultStatus();
                        resultStatus.actionCode = mActionCode;
                        // 判斷產品型號回傳給 UI 使其可以往下傳遞資訊, 因流程進入下一頁面 (xxxxxProvisionActivity) 時, UI 需依據產品型號顯示圖片
                        // 預設 device type 是 TOWER
                        String boardId = mJsonParser.jsonGetString(responseData, "boardId");
                        switch (boardId) {
                            // 目前實際生產的產品, board ID 是 57000
                            // 其他 board ID 與 device type 的對應都不確定, 也還未有實際產品, 只是先寫起來
                            case "55001":
                                resultStatus.success = true;
                                resultStatus.data = AppConstants.DeviceType.PLUG;
                                break;

                            case "56002":
                                resultStatus.success = true;
                                resultStatus.data = AppConstants.DeviceType.DESKTOP;
                                break;

                            case "57000":
                            case "57001":
                                resultStatus.success = true;
                                resultStatus.data = AppConstants.DeviceType.TOWER;
                                break;

                            default:
                                resultStatus.success = false;
                                resultStatus.errorMsg = mContext.getString(R.string.err_board_id_not_support);
                                break;
                        }
                        mResultStatus.setValue(resultStatus);
                    }
                } else {
                    setWifiApiError(apiResponse.errorMsg);
                }
                mIsLoading.setValue(false);
            }
        });
    }

    // device provision 成功後, 需打 API 讓 device 更新連線狀態
    // (因為有發生 provision 完後 device 是 offline 的情況)
    public void deviceConnectionTest(String did) {
        mCloudManager.deviceConnectionTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {

            }
        }, did);
    }

    public PppoeInfo getPppoeInfo() {
        return mPppoeInfo;
    }

    public StaticIpInfo getStaticIpInfo() {
        return mStaticIpInfo;
    }

    public WirelessInfo getWirelessInfo() {
        return mWirelessInfo;
    }

    public String getInputErrorMsg() {
        return mInputErrorMsg;
    }

    public boolean isPppoeDataFilled() {
        if (mPppoeInfo == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mPppoeInfo.getUsername());
            filled &= InputUtils.isNotEmpty(mPppoeInfo.getPassword());
            return filled;
        }
    }

    public boolean isPppoeDataValid() {
        if (mPppoeInfo == null) {
            return false;
        } else {
            if (!InputUtils.isPppoeUsernameValid(mPppoeInfo.getUsername())) {
                mInputErrorMsg = InputUtils.getInvalidPppoeUsernameMessage();
                return false;
            } else if (!InputUtils.isPppoePasswordValid(mPppoeInfo.getPassword())) {
                mInputErrorMsg = InputUtils.getInvalidPppoePasswordMessage();
                return false;
            }
            return true;
        }
    }

    public boolean isStaticIpDataFilled() {
        if (mStaticIpInfo == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mStaticIpInfo.getIp());
            filled &= InputUtils.isNotEmpty(mStaticIpInfo.getMask());
            filled &= InputUtils.isNotEmpty(mStaticIpInfo.getGateway());
            filled &= InputUtils.isNotEmpty(mStaticIpInfo.getDns1());
            filled &= InputUtils.isNotEmpty(mStaticIpInfo.getDns2());
            return filled;
        }
    }

    public boolean isStaticIpDataValid() {
        if (mStaticIpInfo == null) {
            return false;
        } else {
            if (!InputUtils.isIpValid(mStaticIpInfo.getIp())) {
                mInputErrorMsg = mContext.getString(R.string.err_static_ip_not_valid);
                return false;
            } else if (!InputUtils.isIpValid(mStaticIpInfo.getMask())) {
                mInputErrorMsg = mContext.getString(R.string.err_network_mask_not_valid);
                return false;
            } else if (!InputUtils.isIpValid(mStaticIpInfo.getGateway())) {
                mInputErrorMsg = mContext.getString(R.string.err_default_gateway_not_valid);
                return false;
            } else if (!InputUtils.isIpValid(mStaticIpInfo.getDns1())) {
                mInputErrorMsg = mContext.getString(R.string.err_dns_not_valid);
                return false;
            } else if (!InputUtils.isIpValid(mStaticIpInfo.getDns2())) {
                mInputErrorMsg = mContext.getString(R.string.err_dns_not_valid);
                return false;
            }
            return true;
        }
    }

    public void setLocationLabel(String location) {
        mLocation = location;
    }

    public boolean isWirelessDataFilled() {
        if (mWirelessInfo == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mWirelessInfo.getSsid2g());
            filled &= InputUtils.isNotEmpty(mWirelessInfo.getPassword2g());
            filled &= InputUtils.isNotEmpty(mWirelessInfo.getSsid5g());
            filled &= InputUtils.isNotEmpty(mWirelessInfo.getPassword5g());
            return filled;
        }
    }

    public boolean isWirelessDataValid() {
        if (mWirelessInfo == null) {
            return false;
        } else {
            // 檢查設定的 2.4G SSID, password 是否合法
            if (!InputUtils.isWifiSsidValid(mWirelessInfo.getSsid2g())) {
                mInputErrorMsg = InputUtils.getInvalidWifiSsidMessage("2.4G SSID");
                return false;
            } else if (!InputUtils.isWifiPasswordValid(mWirelessInfo.getPassword2g())) {
                mInputErrorMsg = InputUtils.getInvalidWifiPasswordMessage("2.4G password");
                return false;
            } else if (InputUtils.containsWhitespace(mWirelessInfo.getSsid2g())) {
                // TODO: constraint to be removed if Bug #7076 is solved
                // https://umedia.plan.io/issues/7076?pn=1#change-28750
                mInputErrorMsg = mContext.getString(R.string.err_ssid_invalid_space);
                return false;
            }
            // 若 2.4G 和 5G 使用不同設定, 再檢查 5G 的設定
            if (!mWirelessInfo.getUseSame()) {
                if (!InputUtils.isWifiSsidValid(mWirelessInfo.getSsid5g())) {
                    mInputErrorMsg = InputUtils.getInvalidWifiSsidMessage("5G SSID");
                    return false;
                } else if (!InputUtils.isWifiPasswordValid(mWirelessInfo.getPassword5g())) {
                    mInputErrorMsg = InputUtils.getInvalidWifiPasswordMessage("5G password");
                    return false;
                } else if (InputUtils.containsWhitespace(mWirelessInfo.getSsid5g())) {
                    // TODO: constraint to be removed if Bug #7076 is solved
                    // https://umedia.plan.io/issues/7076?pn=1#change-28750
                    mInputErrorMsg = mContext.getString(R.string.err_ssid_invalid_space);
                    return false;
                }
            }
            return true;
        }
    }
}
