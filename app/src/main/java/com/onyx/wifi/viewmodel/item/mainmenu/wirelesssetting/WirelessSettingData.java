package com.onyx.wifi.viewmodel.item.mainmenu.wirelesssetting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onyx.wifi.utility.StringUtils;

public class WirelessSettingData {

    public enum SettingType {
        INFO_2G, INFO_5G, BOTH
    }

    private String ssid2g = "";
    private String password2g = "";
    private String ssid5g = "";
    private String password5g = "";
    private boolean wifiSame = true;
    private boolean clientOnlineNotification = false;
    private SettingType settingType = SettingType.BOTH;

    public String getSsid2g() {
        return StringUtils.decodeHexString(ssid2g);
    }

    public void setSsid2g(String ssid2g) {
        this.ssid2g = StringUtils.encodeToHexString(ssid2g);
    }

    public String getPassword2g() {
        return StringUtils.decodeHexString(password2g);
    }

    public void setPassword2g(String password2g) {
        this.password2g = StringUtils.encodeToHexString(password2g);
    }

    public String getSsid5g() {
        return StringUtils.decodeHexString(ssid5g);
    }

    public void setSsid5g(String ssid5g) {
        this.ssid5g = StringUtils.encodeToHexString(ssid5g);
    }

    public String getPassword5g() {
        return StringUtils.decodeHexString(password5g);
    }

    public void setPassword5g(String password5g) {
        this.password5g = StringUtils.encodeToHexString(password5g);
    }

    public boolean getWifiSame() {
        return wifiSame;
    }

    public void setWifiSame(boolean wifiSame) {
        this.wifiSame = wifiSame;
    }

    public boolean getClientOnlineNotification() {
        return clientOnlineNotification;
    }

    public void setClientOnlineNotification(boolean clientOnlineNotification) {
        this.clientOnlineNotification = clientOnlineNotification;
    }

    public SettingType getSettingType() {
        return settingType;
    }

    public void setSettingType(SettingType settingType) {
        this.settingType = settingType;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }

    public String toJsonString() {
        return new Gson().toJson(this);
    }
}
