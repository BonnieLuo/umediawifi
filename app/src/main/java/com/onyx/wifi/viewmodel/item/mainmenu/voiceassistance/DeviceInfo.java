package com.onyx.wifi.viewmodel.item.mainmenu.voiceassistance;

public class DeviceInfo {
    private String mProductID;
    private String mDsn;
    private String mCodeChallenge;
    private String mState;
    private String mDeviceAddress;
    private String mDeviceStatus;
    private String mMetadata;

    public DeviceInfo(
            String productID,
            String dsn,
            String codeChallenge,
            String state,
            String deviceAddress,
            String deviceStatus,
            String metadata) {
        mProductID = productID;
        mDsn = dsn;
        mCodeChallenge = codeChallenge;
        mState = state;
        mDeviceAddress = deviceAddress;
        mDeviceStatus = deviceStatus;
        mMetadata = metadata;
    }

    public void clear() {
        mProductID = "";
        mDsn = "";
        mCodeChallenge = "";
        mState = "";
        mDeviceAddress = "";
        mDeviceStatus = "";
        mMetadata = "";
    }


    public String getProductID() {
        return mProductID;
    }

    public void setProductID(String productID) {
        this.mProductID = productID;
    }

    public String getDsn() {
        return mDsn;
    }

    public void setDsn(String dsn) {
        this.mDsn = dsn;
    }

    public String getCodeChallenge() {
        return mCodeChallenge;
    }

    public void setCodeChallenge(String codeChallenge) {
        this.mCodeChallenge = codeChallenge;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        this.mState = state;
    }

    public String getDeviceAddress() {
        return mDeviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.mDeviceAddress = deviceAddress;
    }

    public String getDeviceStatus() {
        return mDeviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.mDeviceStatus = deviceStatus;
    }

    public String getMetadata() {
        return mMetadata;
    }

    public void setMetadata(String metadata) {
        this.mMetadata = metadata;
    }

    @Override
    public String toString() {
        return "DeviceInfo{ " +
                "\nproductID = " + mProductID +
                "\ndsn = " + mDsn +
                "\ncodeChallenge = " + mCodeChallenge +
                "\nstate = " + mState +
                "\ndeviceAddress = " + mDeviceAddress +
                "\ndeviceStatus = " + mDeviceStatus +
                "\nmetadata = " + mMetadata +
                " }";
    }
}
