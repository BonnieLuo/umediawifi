package com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting;

import com.google.gson.annotations.SerializedName;
import com.onyx.wifi.model.item.DeviceInfo;

public class NodeSpeedTestInfo {

    @SerializedName("did")
    private String did = "0";

    @SerializedName("status")
    private int status = 1;

    @SerializedName("upload")
    private int upload = 0;

    @SerializedName("download")
    private int download = 0;

    @SerializedName("timestamp")
    private String timestamp = "";

    DeviceInfo deviceInfo;

    public String getDid() {
        return did;
    }

    public int getStatus() {
        return status;
    }

    public int getUpload() {
        return upload;
    }

    public int getDownload() {
        return download;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
