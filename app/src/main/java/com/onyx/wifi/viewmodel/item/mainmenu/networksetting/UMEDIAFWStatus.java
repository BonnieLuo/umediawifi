package com.onyx.wifi.viewmodel.item.mainmenu.networksetting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting.FWStatus;

public class UMEDIAFWStatus {
    private AppConstants.DeviceType mDeviceType;
    private String mDid;
    private String mDeviceLabel;
    private FWStatus mFWStatus;
    private String mFWVersion;
    private String mFWNewestVerion;

    public UMEDIAFWStatus(
            AppConstants.DeviceType deviceType,
            String sDid,
            String sDeviceLabel,
            FWStatus FWStatus,
            String sFWVersion,
            String sNewestFWVersion) {
        mDeviceType = deviceType;
        mDid = sDid;
        mDeviceLabel = sDeviceLabel;
        mFWStatus = FWStatus;
        mFWVersion = sFWVersion;
        mFWNewestVerion = sNewestFWVersion;
    }

    public AppConstants.DeviceType getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(AppConstants.DeviceType deviceType) {
        this.mDeviceType = deviceType;
    }

    public String getDid() {
        return mDid;
    }

    public void setDid(String sDid) {
        this.mDid = sDid;
    }

    public String getDeviceLabel() {
        return mDeviceLabel;
    }

    public void setDeviceLabel(String sDeviceLabel) {
        this.mDeviceLabel = sDeviceLabel;
    }

    public FWStatus getFWStatus() {
        return mFWStatus;
    }

    public void setFWStatus(FWStatus FWStatus) {
        this.mFWStatus = FWStatus;
    }

    public String getFWVersion() {
        return mFWVersion;
    }

    public void setFWVersion(String sFWVersion) {
        this.mFWVersion = sFWVersion;
    }

    public String getFWNewestVerion() {
        return mFWNewestVerion;
    }

    public void setFWNewestVerion(String sFWNewestVerion) {
        this.mFWNewestVerion = sFWNewestVerion;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return this.getClass().getSimpleName() + gson.toJson(this);
    }
}
