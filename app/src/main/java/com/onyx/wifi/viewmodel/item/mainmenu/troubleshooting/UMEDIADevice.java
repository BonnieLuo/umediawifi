package com.onyx.wifi.viewmodel.item.mainmenu.troubleshooting;

import com.onyx.wifi.utility.AppConstants;

public class UMEDIADevice {
    private AppConstants.DeviceType mDeviceType;
    private FWStatus mFWStatus;
    private String mName;
    private String mFWVersion;
    private boolean mIsHasIssue;

    public UMEDIADevice(
            AppConstants.DeviceType deviceType,
            FWStatus FWStatus,
            String sName,
            String sFWVersion,
            Boolean bIsHasIssue)
    {
        mDeviceType = deviceType;
        mFWStatus = FWStatus;
        mName = sName;
        mFWVersion = sFWVersion;
        mIsHasIssue = bIsHasIssue;
    }

    public AppConstants.DeviceType getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(AppConstants.DeviceType deviceType) {
        this.mDeviceType = deviceType;
    }

    public FWStatus getFWStatus() {
        return mFWStatus;
    }

    public void setFWStatus(FWStatus FWStatus) {
        this.mFWStatus = FWStatus;
    }

    public String getName() {
        return mName;
    }

    public void setName(String sName) {
        this.mName = sName;
    }

    public String getFWVersion() {
        return mFWVersion;
    }

    public void setFWVersion(String sFWVersion) {
        this.mFWVersion = sFWVersion;
    }

    public boolean getIsHasIssue() {
        return mIsHasIssue;
    }

    public void setIsHasIssue(boolean bIsHasIssue) {
        this.mIsHasIssue = bIsHasIssue;
    }

    @Override
    public String toString() {
        return "UMEDIADevice{ " +
                "DeviceType = \"" + mDeviceType + "\"" +
                ", Name = \"" + mName + "\"" +
                ", FWVersion = \"" + mFWVersion + "\"" +
                ", IsHasIssue = \"" + mIsHasIssue + "\"" +
                " }";
    }
}
