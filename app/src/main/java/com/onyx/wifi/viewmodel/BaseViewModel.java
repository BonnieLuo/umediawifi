package com.onyx.wifi.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.CloudManager;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.AccountManager;
import com.onyx.wifi.model.DataManager;
import com.onyx.wifi.model.item.DashboardInfo;
import com.onyx.wifi.model.item.NodeSetInfo;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.utility.LogUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.ResultStatus;

public class BaseViewModel extends AndroidViewModel {
    protected Application mContext;
    protected AccountManager mAccountManager;
    protected CloudManager mCloudManager;
    protected DataManager mDataManager;

    // is loading flag 是 loading 畫面的開關, 若需時間等待回應的動作, 例如打 cloud API, 就要顯示 loading 畫面 (有預設畫面)
    // 若 UI 層不需要顯示預設的 loading 畫面, 或當時顯示的畫面已有表達 loading 狀態了, 就不用設置 is loading flag
    // 配合 ApiCallback.onSendRequest() 的設計:
    // (1) 要顯示 loading 畫面時: mIsLoading.setValue(true);
    // (2) 若不需要顯示 (例如: 在背景打 API, 或是 UI 已顯示 loading 畫面了), 則 onSendRequest() 裡面不做事即可, 這樣 mIsLoading 才不會被改動到
    //     因為同一個頁面的 loading 畫面通常是共用的, 若前景和背景都有在打 API, mIsLoading 的值可能會互相影響到
    protected MutableLiveData<Boolean> mIsLoading;

    // 若 user email 尚未認證, 打 cloud API 會失敗, 此時需顯示強制認證 email 頁面
    protected MutableLiveData<Boolean> mForceEmailVerify;

    /**
     * result status 儲存等待之後得到的回應資料, 例如 response code, data, error message 等
     * <p>
     * 有些 view model 繼承 BaseViewModel 之後, 會再額外定義一個 error message (例如: input error message)
     * 雖然 result status 的 error message 與 input error message 看似都是 error message, 但運作的概念不一樣所以不能共用
     * (1) result status 儲存的資料是需要時間等待處理才能得到回應, 所以使用 MutableLiveData 的概念儲存
     * UI 層 observe result status 資料, 資料有改變時會收到通知, 再做相應的 UI 顯示
     * (2) input error message 則是經檢查後馬上就可以得到結果的, 不需要時間等待處理, 所以用一般的 String 變數就好, 不需要 MutableLiveData
     * 經檢查後若發現使用者輸入的資料錯誤, 則將訊息寫在 input error message 變數, UI 層取得這個訊息來顯示
     */
    protected MutableLiveData<ResultStatus> mResultStatus;

    protected int mActionCode = Code.NONE;

    private String mInternetUnavailable;

    public BaseViewModel(@NonNull Application application) {
        super(application);

        mContext = application;
        mAccountManager = AccountManager.getInstance();
        mCloudManager = CloudManager.getInstance();
        mDataManager = DataManager.getInstance();

        mIsLoading = new MutableLiveData<>();
        mForceEmailVerify = new MutableLiveData<>();
        mResultStatus = new MutableLiveData<>();

        mInternetUnavailable = mContext.getString(R.string.err_internet_not_available);
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return mIsLoading;
    }

    public MutableLiveData<Boolean> getForceEmailVerify() {
        return mForceEmailVerify;
    }

    public MutableLiveData<ResultStatus> getResultStatus() {
        return mResultStatus;
    }

    protected void handleInternetUnavailableError() {
        handleError(mInternetUnavailable);
    }

    protected void handleError(String errorMsg) {
        LogUtils.trace(errorMsg);
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        resultStatus.success = false;
        resultStatus.errorMsg = errorMsg;
        mResultStatus.postValue(resultStatus);
        mIsLoading.postValue(false);
    }

    protected void handleError(String errorMsg, int code) {
        LogUtils.trace(errorMsg);
        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        resultStatus.success = false;
        resultStatus.errorMsg = errorMsg;
        resultStatus.serverErrorCode = code;
        mResultStatus.postValue(resultStatus);
        mIsLoading.postValue(false);
    }

    private void handleUnauthorized(ApiResponse apiResponse) {
        int code = apiResponse.errorResult.getCode();
        String errorMsg = apiResponse.errorResult.getMessage();
        if (code == 4010 || code == 4011) {
            // 4010: Missing or invalid Authorization header.
            // 4011: Invalid token.
            handleError(errorMsg, code);
        } else if (code == 4012) {
            // 4012: Unregister account.
            handleError(errorMsg, code);
        } else if (code == 4013) {
            // 4013: Unverified email.
            mForceEmailVerify.setValue(true);
            setLoadingStatus(false);
        } else {
            handleError(errorMsg, code);
        }
    }

    // 設置 cloud API 回傳的結果讓 UI 響應, 此 function 為預設的處理
    // 若有需要做其他事情 (例如 success 時先將資料儲存下來), 必須在呼叫此 function 前先執行 (參考 SignUpViewModel 裡的做法)
    // 因必須等待所有該做的事情處理完成後, 才能設置結果讓 UI 響應
    protected <T> void handleResponse(ApiResponse<T> apiResponse) {
        handleResponseExceptLoadingStatus(apiResponse);
        // 讓 UI 結束 loading 狀態
        setLoadingStatus(false);
    }

    // 當在前景需要連續打 cloud API 時, 打第一支時要顯示 loading 畫面, 等到最後一支 API 處理完才能結束 loading 狀態
    // 最後一支 API 呼叫上面的 handleResponse(), 其他前面的 API 都呼叫此 function (不會讓 UI 結束 loading 狀態)
    // 使用範例詳見: DashboardActivity & DashboardViewModel
    protected <T> void handleResponseExceptLoadingStatus(ApiResponse<T> apiResponse) {
        if (apiResponse.httpStatusCode == 401) {
            handleUnauthorized(apiResponse);
            return;
        }

        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        if (apiResponse.httpStatusCode == 200) {
            resultStatus.success = true;
            resultStatus.data = apiResponse.body;
        } else {
            resultStatus.success = false;
            resultStatus.errorMsg = apiResponse.errorMsg;
        }
        // 設置 cloud API 回傳的結果讓 UI 響應
        mResultStatus.setValue(resultStatus);
    }

    protected <T> void handleDeviceResponse(ApiResponse<T> apiResponse) {
        handleDeviceResponseExceptLoadingStatus(apiResponse);
        setLoadingStatus(false);
    }

    // 使用概念同 handleResponseExceptLoadingStatus()
    // 使用範例詳見: WirelessSettingActivity & WirelessSettingViewModel
    protected <T> void handleDeviceResponseExceptLoadingStatus(ApiResponse<T> apiResponse) {
        if (apiResponse.httpStatusCode == 401) {
            handleUnauthorized(apiResponse);
            return;
        }

        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;
        if (apiResponse.httpStatusCode == 200) {
            String status;
            JsonParserWrapper jsonParser = new JsonParserWrapper();
            // cloud API 回傳的 response json 裡的 device status 有不同的格式:
            // (1) {
            //      "code":"200",
            //      "data":{
            //          "track_id":"djPx52R",
            //          "track_status":"InProgress",
            //          "status":"InProgress"
            //      },
            //      "message":""
            //     }
            // (2) {
            //      "mqtt_status":"success",
            //      "status":"InProgress"
            //     }
            JsonObject jsonObject = jsonParser.jsonGetJsonObject((JsonObject) apiResponse.body, AppConstants.KEY_DATA);
            status = jsonParser.jsonGetString(jsonObject, AppConstants.KEY_STATUS); // (1)
            if (status.isEmpty()) {
                status = jsonParser.jsonGetString((JsonObject) apiResponse.body, AppConstants.KEY_STATUS); // (2)
            }
            if (status.equals(AppConstants.VALUE_COMPLETED) ||
                    status.equals(AppConstants.VALUE_IN_PROGRESS) ||
                    status.equals(AppConstants.VALUE_ACTIVATED) ||
                    status.equals(AppConstants.VALUE_CANCEL)) {
                // Cancel/Activated 在 Pause 功能會出現, 意義同 Completed
                LogUtils.trace("device_status", status);
                resultStatus.success = true;
                resultStatus.data = apiResponse.body;
            } else {
                resultStatus.success = false;
                if (status.equals(AppConstants.VALUE_PENDING)) {
                    resultStatus.errorMsg = "Pending.";
                } else if (status.equals(AppConstants.VALUE_TIMEOUT)) {
                    resultStatus.errorMsg = "This device does not have response. Please check your device's Internet connection.";
                } else if (status.equals(AppConstants.VALUE_FAILED)) {
                    resultStatus.errorMsg = "Command fail.";
                } else {
                    resultStatus.errorMsg = "Unknown error occurred.";
                }
                LogUtils.trace("device_status", resultStatus.errorMsg);
            }
        } else {
            resultStatus.success = false;
            resultStatus.errorMsg = apiResponse.errorMsg;
        }
        // 設置 cloud API 回傳的結果讓 UI 響應
        mResultStatus.setValue(resultStatus);
    }

    // initial setup 後, 需呼叫 cloud API get dashboard info 以取得 nodeset 的資訊, 但有可能 cloud 資料尚未處理完成,
    // 造成拿到的 nodeset 是空的, App 需根據 cloud 回傳的 code 做 error handling, 必要時需重做 initial setup
    protected <T> void handleDashboardResponseExceptLoadingStatus(ApiResponse<T> apiResponse) {
        if (apiResponse.httpStatusCode == 401) {
            handleUnauthorized(apiResponse);
            return;
        }

        ResultStatus resultStatus = new ResultStatus();
        resultStatus.actionCode = mActionCode;

        if (apiResponse.httpStatusCode == 200) {
            DashboardInfo dashboardInfo = (DashboardInfo) apiResponse.body;
            mDataManager.setDashboardInfo(dashboardInfo);
            if (dashboardInfo != null) {
                String code = dashboardInfo.getCode();
                if (code.equals("200")) {   // success
                    resultStatus.success = true;
                    resultStatus.data = apiResponse.body;
                } else if (code.equals("258")) {
                    // defined by cloud: NETWORKSTATUSMISSING = "258" -> no data is received from device yet.
                    resultStatus.success = false;
                    resultStatus.errorMsg = mContext.getString(R.string.err_258);
                } else if (code.equals("259")) {
                    // defined by cloud: IOTFAIL = "259" -> device is not register correctly, please re-setup
                    resultStatus.success = false;
                    resultStatus.errorMsg = mContext.getString(R.string.err_259);
                } else if (code.equals("252")) {
                    // defined by cloud: NOTFOUND = "252" -> missing controller, please re-setup
                    resultStatus.success = false;
                    resultStatus.errorMsg = mContext.getString(R.string.err_252);
                } else {
                    resultStatus.success = false;
                    resultStatus.errorMsg = mContext.getString(R.string.err_unknown);
                }
            } else {
                resultStatus.success = false;
                resultStatus.errorMsg = mContext.getString(R.string.err_unknown);
            }
        } else {
            resultStatus.success = false;
            resultStatus.errorMsg = apiResponse.errorMsg;
        }
        // 設置 cloud API 回傳的結果讓 UI 響應
        mResultStatus.setValue(resultStatus);
    }

    protected void setLoadingStatus(boolean isLoading) {
        // true: UI 進入/維持 loading 狀態
        // false: UI 結束 loading 狀態
        mIsLoading.setValue(isLoading);
    }

    public NodeSetInfo getSavedNodeSetInfo() {
        return mDataManager.getDashboardNodeSet();
    }
}
