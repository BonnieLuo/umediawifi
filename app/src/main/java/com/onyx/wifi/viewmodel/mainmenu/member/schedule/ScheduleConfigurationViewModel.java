package com.onyx.wifi.viewmodel.mainmenu.member.schedule;

import android.app.Application;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.member.ScheduleConfiguration;
import com.onyx.wifi.model.item.member.ScheduleFrequency;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class ScheduleConfigurationViewModel extends BaseViewModel {

    public ScheduleConfigurationViewModel(@NonNull Application application) {
        super(application);
    }

    public void createSchedule(String memberId, ScheduleConfiguration scheduleConfig) {
        String scheduleName = scheduleConfig.getName();
        String startTime = scheduleConfig.getStartTimeString("HH:mm");
        String endTime = scheduleConfig.getEndTimeString("HH:mm");
        String isEnabled = scheduleConfig.isEnabled() ? "1" : "0";
        ScheduleFrequency frequency = scheduleConfig.getFrequency();

        JsonObject requestJson = new JsonObject();

        requestJson.addProperty("member_id", memberId);

        requestJson.addProperty("schedule_name", scheduleName);
        requestJson.addProperty("start_time", startTime);
        requestJson.addProperty("end_time", endTime);
        requestJson.addProperty("enable", isEnabled);

        JsonObject frequencyJson = frequency.toJson();
        requestJson.add("repeat", frequencyJson);

        mActionCode = Code.Action.Member.CREATE_MEMBER_SCHEDULE;
        mCloudManager.createMemberSchedule(requestJson, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void updateSchedule(String memberId, ScheduleConfiguration scheduleConfig) {
        String scheduleId = scheduleConfig.getId();
        String scheduleName = scheduleConfig.getName();
        String startTime = scheduleConfig.getStartTimeString("HH:mm");
        String endTime = scheduleConfig.getEndTimeString("HH:mm");
        String isEnabled = scheduleConfig.isEnabled() ? "1" : "0";
        ScheduleFrequency frequency = scheduleConfig.getFrequency();

        JsonObject requestJson = new JsonObject();

        requestJson.addProperty("member_id", memberId);
        requestJson.addProperty("schedule_id", scheduleId);
        requestJson.addProperty("schedule_name", scheduleName);
        requestJson.addProperty("start_time", startTime);
        requestJson.addProperty("end_time", endTime);
        requestJson.addProperty("enable", isEnabled);

        JsonObject frequencyJson = frequency.toJson();
        requestJson.add("repeat", frequencyJson);

        mActionCode = Code.Action.Member.UPDATE_MEMBER_SCHEDULE;
        mCloudManager.updateMemberSchedule(requestJson, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
//                if (apiResponse.httpStatusCode == 200) {
//                }
//                handleResponse(apiResponse);

                if (true) {
                    // delay for the time that fw responeses to cloud
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            handleDeviceResponse(apiResponse);
                        }
                    }, 3000);
                } else {
                    // 不需要 delay 的情況是在 WirelessSettingActivity 打完 API
                    // 由於不會重新 get wireless setting, 而是將資料寫回 share preference, 故不需要 delay
                    handleDeviceResponse(apiResponse);
                }
            }
        });
    }

    public void removeSchedule(String memberId, String scheduleId) {
        mActionCode = Code.Action.Member.REMOVE_MEMBER_SCHEDULE;
        mCloudManager.removeMemberSchedule(memberId, scheduleId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
