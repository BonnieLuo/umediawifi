package com.onyx.wifi.viewmodel.login;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.onyx.wifi.R;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.UserInfo;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.CommonUtils;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.login.SignInUser;

public class BaseSignInViewModel extends BaseViewModel {

    public BaseSignInViewModel(@NonNull Application application) {
        super(application);
    }

    protected void firebaseSignIn(SignInUser signInUser) {
        // Firebase sign in
        mAccountManager.signIn(signInUser, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    // Firebase sign in 成功後, 打 cloud API 取得 user info
                    // (user info 會回傳 cloud 產生的 nodeset id 資訊, 登入後若 user 還沒有 router 裝置,
                    // App 會引導至 initial setup, setup 過程中打 cloud API 參數會需要傳 nodeset id)
                    getUserInfo();
                } else {
                    String errorMsg = (task.getException() != null) ? task.getException().getMessage() : "";

                    // 使用 Firebase SDK 登入失敗, 取得 SDK 的 error message
                    // 但有些情況 Firebase 回傳的 error message 較難理解 or 洩漏太多資訊, 故轉換訊息內容後再回傳給 UI
                    if (errorMsg.equals("The password is invalid or the user does not have a password.")
                            || errorMsg.equals("There is no user record corresponding to this identifier. The user may have been deleted.")) {
                        // 使用者輸入的 email 沒有註冊過, 或是輸入的 password 錯誤
                        // Bug #7619 (https://umedia.plan.io/issues/7619)
                        errorMsg = mContext.getString(R.string.err_email_or_password_invalid);
                    } else if (errorMsg.equals(mContext.getString(R.string.err_firebase_sdk_network_error_msg))) {
                        // Firebase 回傳的 error message 裡面有 error code, 請見下列網址的定義
                        // https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes.html#NETWORK_ERROR
                        errorMsg = mContext.getString(R.string.err_internet_connection_fail);
                    }
                    handleError(errorMsg);
                }
            }
        });
    }

    protected void getUserInfo() {
        mCloudManager.getUserInfo(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    // 暫時以shareprefrence紀錄登入的account資訊
                    CommonUtils.setSharedPrefData(AppConstants.KEY_FIRST_NAME, ((UserInfo) apiResponse.body).getFirstName());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_LAST_NAME, ((UserInfo) apiResponse.body).getLastName());
                    CommonUtils.setSharedPrefData(AppConstants.KEY_EMAIL, ((UserInfo) apiResponse.body).getEmail());

                    // 由 DataManager 統一管理所有 global 的資料是比較好的做法
                    // 待 AccountSetting 那邊拿取資料換成從 DataManager 拿資料後, 就刪除上述 share preference 的做法
                    mDataManager.setUserInfo((UserInfo) apiResponse.body);

                    // 使用者登入成功, 註冊 Firebase Cloud Messaging token 以接收 Cloud 訊息
                    mAccountManager.registerFcmToken();
                }
                handleResponse(apiResponse);
            }
        });
    }
}
