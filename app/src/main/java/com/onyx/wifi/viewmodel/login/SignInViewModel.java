package com.onyx.wifi.viewmodel.login;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.utility.InputUtils;
import com.onyx.wifi.viewmodel.item.Code;
import com.onyx.wifi.viewmodel.item.login.SignInUser;

public class SignInViewModel extends BaseSignInViewModel {

    private SignInUser mSignInUser = new SignInUser();

    public SignInViewModel(@NonNull Application application) {
        super(application);
    }

    public SignInUser getSignInUser() {
        return mSignInUser;
    }

    public boolean isDataFilled() {
        if (mSignInUser == null) {
            return false;
        } else {
            boolean filled = InputUtils.isNotEmpty(mSignInUser.getEmail());
            filled &= InputUtils.isNotEmpty(mSignInUser.getPassword());
            return filled;
        }
    }

    public void signIn() {
        mActionCode = Code.Action.UserAccount.SIGN_IN;

        mIsLoading.setValue(true);

        firebaseSignIn(mSignInUser);
    }
}
