package com.onyx.wifi.viewmodel.interfaces.login;

public interface ForgotPasswordListener {
    void onBack();
    void onResetCodeContinue();
}

