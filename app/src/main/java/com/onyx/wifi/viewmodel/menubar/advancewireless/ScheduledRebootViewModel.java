package com.onyx.wifi.viewmodel.menubar.advancewireless;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.advancewireless.reboot.ScheduleRebootConfig;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

public class ScheduledRebootViewModel extends BaseViewModel {

    public ScheduledRebootViewModel(@NonNull Application application) {
        super(application);
    }

    public void setupScheduledReboot(ScheduleRebootConfig config) {
        mActionCode = Code.Action.AdvanceWirelessSettings.SET_SCHEDULED_REBOOT;
        mCloudManager.setupScheduledReboot(config, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

    public void rebootAllNodes() {
        mActionCode = Code.Action.AdvanceWirelessSettings.REMOVE_ALL_NODES;
        mCloudManager.rebootAllNodes(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
