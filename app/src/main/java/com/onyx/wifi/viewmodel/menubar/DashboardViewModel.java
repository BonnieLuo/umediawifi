package com.onyx.wifi.viewmodel.menubar;

import android.app.Application;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.model.item.WirelessSetting;
import com.onyx.wifi.model.item.member.PauseInfo;
import com.onyx.wifi.model.item.member.PauseInfoModel;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.lang.reflect.Type;
import java.util.List;

public class DashboardViewModel extends BaseViewModel {

    public DashboardViewModel(@NonNull Application application) {
        super(application);
    }

    public void getDashboardInfo(boolean showLoadingStatus) {
        mActionCode = Code.Action.Dashboard.GET_DASHBOARD_INFO;
        mCloudManager.getDashboardInfo(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                if (showLoadingStatus) {
                    // 打第一支 API 要顯示 loading 畫面
                    mIsLoading.setValue(true);
                }
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleDashboardResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void getWirelessSetting() {
        mActionCode = Code.Action.Dashboard.GET_WIRELESS_SETTING;
        mCloudManager.getWirelessSetting(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    JsonObject settingsJson = jsonParserWrapper.jsonGetJsonObject(dataJson, "settings");
                    WirelessSetting wirelessSetting = jsonParserWrapper.jsonToObject(settingsJson, WirelessSetting.class);
                    mDataManager.setWirelessSetting(wirelessSetting);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void getSpeedTest() {
        mActionCode = Code.Action.Dashboard.GET_SPEED_TEST;
        mCloudManager.getSpeedTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    SpeedTest speedTest = jsonParserWrapper.jsonToObject(dataJson, SpeedTest.class);
                    mDataManager.updateSpeedTest(mDataManager.getControllerDid(), speedTest);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void getGlobalPauseList() {
        mActionCode = Code.Action.Dashboard.GET_GLOBAL_PAUSE_LIST;
        mCloudManager.getGlobalPauseList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonParserWrapper jsonParserWrapper = new JsonParserWrapper();
                    JsonObject dataJson = jsonParserWrapper.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    JsonArray pauseStatusArray = jsonParserWrapper.jsonGetJsonArray(dataJson, "pause_status");

                    if (pauseStatusArray != null) {
                        Type pauseInfoListType = new TypeToken<List<PauseInfoModel>>() {
                        }.getType();
                        List<PauseInfoModel> pauseInfoList = new JsonParserWrapper().jsonToObject(pauseStatusArray.toString(), pauseInfoListType);
                        boolean find = false;
                        for (PauseInfoModel pauseInfoModel : pauseInfoList) {
                            PauseInfo pauseInfo = new PauseInfo(pauseInfoModel);
                            String type = pauseInfo.getType();
                            boolean isEnabled = pauseInfo.isEnabled();
                            int minuteLeft = pauseInfo.getMinuteLeft();
                            // type = 1 是 global pause
                            if ("1".equals(type) && isEnabled && minuteLeft > 0) {
                                // 找到第一筆 enable = 1 且 left minute > 0 的 profile, 存到 share preference
                                mDataManager.setPauseInfoModel(pauseInfoModel);
                                find = true;
                                break;
                            }
                        }
                        if (!find) {
                            // 如果沒有找到符合上述條件的 pause info, 代表 global pause 是 disable 的狀態
                            // 儲存 enable = 0 的資料到 share preference
                            mDataManager.setPauseInfoModel(new PauseInfoModel());
                        }
                    }
                }
                // 最後一支 API, handle response 時要結束 loading 狀態
                handleResponse(apiResponse);
            }
        });
    }

    public void startSpeedTest() {
        mActionCode = Code.Action.Dashboard.START_SPEED_TEST;
        mCloudManager.startSpeedTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {

            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    String status = new JsonParserWrapper().jsonGetString((JsonObject) apiResponse.body, AppConstants.KEY_STATUS);
                    if (status.equals(AppConstants.VALUE_IN_PROGRESS)) {
                        mDataManager.updateSpeedTestInProgress(mDataManager.getControllerDid());
                    }
                }
                handleDeviceResponse(apiResponse);
            }
        });
    }

    public void disableGlobalPause(String profileId, String ruleId) {
        mActionCode = Code.Action.Dashboard.DISABLE_GLOBAL_PAUSE;
        mCloudManager.disableGlobalPause(profileId, ruleId, new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleDeviceResponse(apiResponse);
            }
        });
    }
}
