package com.onyx.wifi.viewmodel.menubar;

import android.app.Application;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.model.item.ClientInfoList;
import com.onyx.wifi.model.item.ClientModel;
import com.onyx.wifi.model.item.DeviceInfo;
import com.onyx.wifi.model.item.SpeedTest;
import com.onyx.wifi.model.item.member.ConnectionStatus;
import com.onyx.wifi.utility.AppConstants;
import com.onyx.wifi.utility.JsonParserWrapper;
import com.onyx.wifi.viewmodel.BaseViewModel;
import com.onyx.wifi.viewmodel.item.Code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NodeSettingViewModel extends BaseViewModel {

    private String mDid;

    private ArrayList<ClientModel> mOnlineClients = new ArrayList<>();

    private JsonParserWrapper mJsonParser;

    public NodeSettingViewModel(@NonNull Application application) {
        super(application);

        mJsonParser = new JsonParserWrapper();
    }

    // 現在頁面上呈現的 node 的 did
    public void setDeviceDid(String did) {
        mDid = did;
    }

    public DeviceInfo getSavedNodeInfo(String did) {
        return mDataManager.getNodeInfo(did);
    }

    public ArrayList<ClientModel> getOnlineClients() {
        return mOnlineClients;
    }

    public void getNodeInfo() {
        mActionCode = Code.Action.NodeSetting.GET_NODE_INFO;
        mCloudManager.getNodeInfo(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject jsonObject = mJsonParser.jsonGetJsonObject((JsonObject) apiResponse.body, AppConstants.KEY_DATA);
                    DeviceInfo nodeInfo = mJsonParser.jsonToObject(jsonObject, DeviceInfo.class);
                    mDataManager.setNodeInfo(mDid, nodeInfo);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        }, mDid);
    }

    public void getSpeedTest() {
        mActionCode = Code.Action.NodeSetting.GET_SPEED_TEST;
        mCloudManager.getSpeedTest(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject dataJson = mJsonParser.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    SpeedTest speedTest = mJsonParser.jsonToObject(dataJson, SpeedTest.class);
                    mDataManager.updateSpeedTest(mDid, speedTest);
                }
                // 需要繼續打其他 API, 所以 handle response 時不能改變 loading 狀態
                handleResponseExceptLoadingStatus(apiResponse);
            }
        });
    }

    public void getClientList() {
        mActionCode = Code.Action.NodeSetting.GET_CLIENT_LIST;
        Map<String, String> options = new HashMap<>();
        options.put("nid", mDataManager.getNodeSetId());
        options.put("scope", "node");
        options.put("did", mDid);
        mCloudManager.getClientInfoList(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                // 不做事 => loading 狀態不會被改動到
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                    JsonObject dataJson = mJsonParser.jsonGetJsonObject((JsonObject) apiResponse.body, "data");
                    ClientInfoList clientInfoList = mJsonParser.jsonToObject(dataJson, ClientInfoList.class);

                    mOnlineClients.clear();
                    for (ClientModel clientModel : clientInfoList.getClientInfoList()) {
                        if (!clientModel.getDisallowed() && clientModel.getOnlineStatus() == ConnectionStatus.ONLINE) {
                            mOnlineClients.add(clientModel);
                        }
                    }
                } else {
                    mOnlineClients.clear();
                }
                // 最後一支 API, handle response 時要結束 loading 狀態
                handleResponse(apiResponse);
            }
        }, options);
    }

    public void updateDeviceLabel(String deviceLabel) {
        mActionCode = Code.Action.NodeSetting.UPDATE_DEVICE_LABEL;
        mCloudManager.changeDeviceLabel(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleDeviceResponse(apiResponse);
            }
        }, mDid, deviceLabel);
    }

    public void removeDevice() {
        mActionCode = Code.Action.NodeSetting.REMOVE_DEVICE;
        mCloudManager.removeDevice(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                // 需要等待一下子 device 才會完全移除, 故延遲 3 秒再通知 UI 結果
                // 因為有發生在 extender page remove device, 結果回到上一頁 NodeList/NetworkMap 打 API 拿到的資料還是有那台 extender
                // 所以加一些 delay 避免這情況
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        handleResponse(apiResponse);
                    }
                }, 3000);
            }
        }, mDid);
    }

    public void rebootDevice() {
        mActionCode = Code.Action.NodeSetting.REBOOT_DEVICE;
        mCloudManager.rebootDevice(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {
                handleInternetUnavailableError();
            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                handleDeviceResponse(apiResponse);
            }
        }, mDid);
    }
}
