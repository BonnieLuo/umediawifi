package com.onyx.wifi.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;

import com.onyx.wifi.cloud.interfaces.ApiCallback;
import com.onyx.wifi.cloud.item.ApiResponse;
import com.onyx.wifi.viewmodel.item.Code;

public class ClientListViewModel extends BaseViewModel {

    public ClientListViewModel(@NonNull Application application) {
        super(application);
    }

    public void getAllClients() {
        mActionCode = Code.Action.UserAccount.GET_ALL_CLIENTS;
        mCloudManager.getAllClients(new ApiCallback() {
            @Override
            public void onInternetUnavailable() {

            }

            @Override
            public void onSendRequest() {
                mIsLoading.setValue(true);
            }

            @Override
            public <T> void onReceiveResponse(ApiResponse<T> apiResponse) {
                if (apiResponse.httpStatusCode == 200) {
                }
                handleResponse(apiResponse);
            }
        });
    }

}
